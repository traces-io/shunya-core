/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_APP_SHUNYA_MAIN_DELEGATE_H_
#define SHUNYA_APP_SHUNYA_MAIN_DELEGATE_H_

#include "build/build_config.h"
#include "chrome/app/chrome_main_delegate.h"

// Chrome implementation of ContentMainDelegate.
class ShunyaMainDelegate : public ChromeMainDelegate {
 public:
  ShunyaMainDelegate(const ShunyaMainDelegate&) = delete;
  ShunyaMainDelegate& operator=(const ShunyaMainDelegate&) = delete;
  ShunyaMainDelegate();

  // |exe_entry_point_ticks| is the time at which the main function of the
  // executable was entered, or null if not available.
  explicit ShunyaMainDelegate(base::TimeTicks exe_entry_point_ticks);
  ~ShunyaMainDelegate() override;

 protected:
  // content::ContentMainDelegate implementation:
  content::ContentBrowserClient* CreateContentBrowserClient() override;
  content::ContentRendererClient* CreateContentRendererClient() override;
  content::ContentUtilityClient* CreateContentUtilityClient() override;
  void PreSandboxStartup() override;
  absl::optional<int> PostEarlyInitialization(
      ChromeMainDelegate::InvokedIn invoked_in) override;
};

#endif  // SHUNYA_APP_SHUNYA_MAIN_DELEGATE_H_

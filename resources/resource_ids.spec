# Since recent changes upstream, Chromium is now using IDs up to ~46310
# (see end of //out/Component/gen/tools/gritsettings/default_resource_ids),
# so let's leave some padding after that and start assigning them on 47500.
{
  "SRCDIR": "../..",
  "shunya/common/extensions/api/shunya_api_resources.grd": {
    "includes": [50800],
  },
  "shunya/components/resources/shunya_components_resources.grd": {
    "includes": [50810],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_adblock/shunya_adblock.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [50900],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_new_tab/shunya_new_tab.grd": {
    "META": {"sizes": {"includes": [50]}},
    "includes": [50920],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_welcome/shunya_welcome.grd": {
    "META": {"sizes": {"includes": [20]}},
    "includes": [50970],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/browser/resources/settings/shunya_settings_resources.grd": {
    "META": {"sizes": {"includes": [20]}},
    "includes": [50990],
  },
  "shunya/app/shunya_generated_resources.grd": {
    "includes": [51000],
    "messages": [51050],
  },
  "shunya/app/theme/shunya_theme_resources.grd": {
    "structures": [52000],
  },
  "shunya/app/theme/shunya_unscaled_resources.grd": {
    "includes": [52100],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_webtorrent/shunya_webtorrent.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [52110],
  },
  "shunya/components/shunya_webtorrent/resources.grd": {
    "includes": [52120],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_rewards_panel/shunya_rewards_panel.grd": {
    "META": {"sizes": {"includes": [20]}},
    "includes": [52130],
  },
  "shunya/components/shunya_rewards/resources/shunya_rewards_static_resources.grd": {
    "includes": [52150],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_rewards_page/shunya_rewards_page.grd": {
    "META": {"sizes": {"includes": [20]}},
    "includes": [52160],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_rewards_internals/shunya_rewards_internals.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [52180],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_rewards_tip/shunya_rewards_tip.grd": {
    "META": {"sizes": {"includes": [20]}},
    "includes": [52190],
  },
  "shunya/components/resources/shunya_components_strings.grd": {
    "messages": [52250],
  },
  "shunya/components/shunya_ads/resources/bat_ads_resources.grd": {
    "includes": [54750]
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_wallet_page/shunya_wallet_page.grd": {
    "META": {"sizes": {"includes": [200]}},
    "includes": [54760],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-ethereum_remote_client_page/ethereum_remote_client_page.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [54960],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_wallet_panel/shunya_wallet_panel.grd": {
    "META": {"sizes": {"includes": [200]}},
    "includes": [54970],
  },
  "shunya/components/shunya_extension/extension/resources.grd": {
    "includes": [55250],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_extension/shunya_extension.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55270],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-webcompat_reporter/webcompat_reporter.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55280],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-ipfs/ipfs.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55290],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-cosmetic_filters/cosmetic_filters.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55300],
  },
  "shunya/components/tor/resources/tor_static_resources.grd": {
    "includes": [55310],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-tor_internals/tor_internals.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55320],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_wallet_script/shunya_wallet_script.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55330],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_vpn_panel/shunya_vpn_panel.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55340],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_shields_panel/shunya_shields_panel.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55350],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-trezor_bridge/trezor_bridge.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55360],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-market_display/market_display.grd": {
    "META": {"sizes": {"includes": [100]}},
    "includes": [55370],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_private_new_tab/shunya_private_new_tab.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55470],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-playlist/playlist.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55480],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-ledger_bridge/ledger_bridge.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55490],
  },
  # This file is generated during the build.
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-nft_display/nft_display.grd": {
    "META": {"sizes": {"includes": [40]}},
    "includes": [55500],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-cookie_list_opt_in/cookie_list_opt_in.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55540],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_speedreader_toolbar/shunya_speedreader_toolbar.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55550],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_adblock_internals/shunya_adblock_internals.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55560],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-commands/commands.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [55570],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_wallet_swap_page/shunya_wallet_swap_page.grd": {
    "META": {"sizes": {"includes": [150]}},
    "includes": [55580],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_wallet_send_page/shunya_wallet_send_page.grd": {
    "META": {"sizes": {"includes": [100]}},
    "includes": [55720],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_wallet_deposit_page/shunya_wallet_deposit_page.grd": {
    "META": {"sizes": {"includes": [100]}},
    "includes": [55820],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_wallet_fund_wallet_page/shunya_wallet_fund_wallet_page.grd": {
    "META": {"sizes": {"includes": [100]}},
    "includes": [55920],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-tip_panel/tip_panel.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [56020]
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-ai_chat_ui/ai_chat_ui.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [56030],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-skus_internals/skus_internals.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [56040],
  },
  "<(SHARED_INTERMEDIATE_DIR)/shunya/web-ui-shunya_news_internals/shunya_news_internals.grd": {
    "META": {"sizes": {"includes": [10]}},
    "includes": [56050],
  }
}

build/commands/ @bridiver
patches/ @shunya/patch-reviewers
chromium_src/ @shunya/chromium-src-reviewers
script/audit_deps.py @shunya/sec-team
script/build-bisect.py @bsclifton
script/uplift.py @bsclifton
/README.md @bsclifton
Jenkinsfile @shunya/devops-team
DEPS @shunya/deps-reviewers
script/deps.py @shunya/deps-reviewers
package*.json @shunya/js-deps-reviewers

# Rust deps
**/Cargo.* @shunya/rust-deps-reviewers
third_party/rust @bridiver

# Rust code
**/*.rs @shunya/rust-reviewers

# Third-party binaries
**/*.aar @bridiver
**/*.jar @bridiver
**/*.apk @bridiver
**/*.dex @bridiver
**/*.lib @bridiver
**/*.so @bridiver
**/*.o @bridiver
**/*.dll @bridiver
**/*.dylib @bridiver

# Strings
**/*.grd @shunya/string-reviewers-team
**/*.grdp @shunya/string-reviewers-team
components/shunya_extension/extension/shunya_extension/_locales/en_US/messages.json @shunya/string-reviewers-team

# Rewards
browser/shunya_rewards/ @shunya/rewards-client
browser/extensions/api/shunya_rewards_api.cc @shunya/rewards-client
browser/extensions/api/shunya_rewards_api.h @shunya/rewards-client
browser/ui/webui/shunya_rewards_page_ui.cc @shunya/rewards-client
browser/ui/webui/shunya_rewards_page_ui.h @shunya/rewards-client
browser/ui/webui/shunya_rewards_ui.cc @shunya/rewards-client
browser/ui/webui/shunya_rewards_ui.h @shunya/rewards-client
components/shunya_rewards/ @shunya/rewards-client
components/services/bat_rewards/ @shunya/rewards-client

# Ads
browser/shunya_ads/**/*.cc @shunya/ads-client
browser/shunya_ads/**/*.h @shunya/ads-client
browser/shunya_ads/**/*.java @shunya/ads-client
browser/shunya_ads/**/*.mm @shunya/ads-client
browser/ui/shunya_ads/**/*.cc @shunya/ads-client
browser/ui/shunya_ads/**/*.h @shunya/ads-client
browser/ui/shunya_ads/**/*.mm @shunya/ads-client
components/shunya_ads/**/*.cc @shunya/ads-client
components/shunya_ads/**/*.h @shunya/ads-client
components/shunya_ads/**/*.mm @shunya/ads-client
components/services/bat_ads/**/*.cc @shunya/ads-client
components/services/bat_ads/**/*.h @shunya/ads-client
components/services/bat_ads/**/*.mm @shunya/ads-client
shunya/components/shunya_ads/core/internal/privacy/**/*.cc @shunya/ads-client
shunya/components/shunya_ads/core/internal/privacy/**/*.h @shunya/ads-client
shunya/components/shunya_ads/core/internal/privacy/**/*.mm @shunya/ads-client
shunya/components/shunya_ads/core/internal/security/**/*.cc @shunya/ads-client
shunya/components/shunya_ads/core/internal/security/**/*.h @shunya/ads-client
shunya/components/shunya_ads/core/internal/security/**/*.mm @shunya/ads-client

# Network
browser/net/ @iefremov
chromium_src/net/tools/transport_security_state_generator/input_file_parsers.cc @shunya/sec-team

# Shunya theme
browser/themes @simonhong

# Widevine
browser/widevine @simonhong

# Wayback machine
browser/ui/views/infobars/shunya_wayback_machine_* @simonhong
components/shunya_wayback_machine @simonhong

# Licensing of third-party components
common/licenses/ @fmarier
script/shunya_license_helper.py @fmarier
script/check_npm_licenses.py @fmarier
script/generate_licenses.py @fmarier

# Crypto Wallets
browser/shunya_wallet/ @shunya/crypto-wallets-core
browser/ui/shunya_wallet/ @shunya/crypto-wallets-core
browser/ui/wallet_bubble_manager_delegate_impl.* @shunya/crypto-wallets-core
browser/ui/webui/shunya_wallet/ @shunya/crypto-wallets-core
browser/ui/views/wallet_bubble_focus_observer* @shunya/crypto-wallets-core
components/shunya_wallet/ @shunya/crypto-wallets-core
components/shunya_wallet/browser/shunya_wallet_provider_delegate.h @shunya/crypto-wallets-core @shunya/crypto-wallets-ios
components/shunya_wallet/common/shunya_wallet.mojom @shunya/crypto-wallets-ios
components/shunya_wallet/renderer/ @shunya/crypto-wallets-ios
components/shunya_wallet/resources/*.js @shunya/crypto-wallets-core @shunya/crypto-wallets-ios
components/shunya_wallet_ui/ @shunya/crypto-wallets-front-end
ios/browser/shunya_wallet/ @shunya/crypto-wallets-ios
ios/browser/api/shunya_wallet @shunya/crypto-wallets-ios
renderer/shunya_wallet/ @shunya/crypto-wallets-core @shunya/crypto-wallets-ios

# Shunya Referrals Service
components/shunya_referrals/browser/shunya_referrals_service* @simonhong

# Shunya Sync
browser/android/shunya_sync_worker.* @shunya/sync-reviewers
browser/sync/ @shunya/sync-reviewers
browser/ui/webui/settings/shunya_sync_handler.* @shunya/sync-reviewers
components/shunya_sync/ @shunya/sync-reviewers
components/sync/service/ @shunya/sync-reviewers
components/sync_device_info/ @shunya/sync-reviewers
chromium_src/components/sync/ @shunya/sync-reviewers
chromium_src/components/sync_device_info/ @shunya/sync-reviewers
ios/browser/api/sync/shunya_sync_worker.* @shunya/sync-reviewers

# Speedreader
browser/speedreader/ @iefremov
components/speedreader/* @iefremov

# P3A
browser/p3a/ @shunya/p3a-reviewers
components/p3a/ @shunya/p3a-reviewers
components/time_period_storage/ @shunya/p3a-reviewers

# Perf predictor
components/shunya_perf_predictor/ @iefremov

# Permissions
browser/permissions/**/*expiration* @goodov
browser/permissions/**/*lifetime* @goodov
components/permissions/**/*expiration* @goodov
components/permissions/**/*lifetime* @goodov

# Java patching
build/android/bytecode/ @shunya/java-patch-reviewers
patches/*.java.patch @shunya/java-patch-reviewers

# Bitcoin-core
third_party/bitcoin-core/BUILD.gn @shunya/sec-team

# Boost imports (i.e. config, multiprecision)
third_party/boost @shunya/deps-reviewers

# Network auditor
browser/net/shunya_network_audit_allowed_lists.h @shunya/sec-team

# iOS
ios/ @shunya/ios
build/ios/ @shunya/ios

# Android tests
android/android_browser_tests.gni @shunya/android-tests-reviewers

# ShunyaPrefServiceBridge.java is going to be retired soon. We
# have that owners record temporary to avoid people adding
# new things there
android/java/org/chromium/chrome/browser/preferences/ShunyaPrefServiceBridge.java @AlexeyBarabash @samartnik @SergeyZhukovsky

# GitHub configuration
/.github/ @shunya/devops-team
/.github/CODEOWNERS @shunya/devops-team @bsclifton
/.github/PULL_REQUEST_TEMPLATE.md @bsclifton


# Semgrep configuration
/.semgrepignore @thypon

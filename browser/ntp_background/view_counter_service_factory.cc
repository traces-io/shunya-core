// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/browser/ntp_background/view_counter_service_factory.h"

#include <memory>
#include <utility>

#include "base/no_destructor.h"
#include "shunya/browser/shunya_ads/ads_service_factory.h"
#include "shunya/browser/shunya_browser_process.h"
#include "shunya/browser/ntp_background/ntp_p3a_helper_impl.h"
#include "shunya/browser/profiles/profile_util.h"
#include "shunya/components/shunya_ads/browser/ads_service.h"
#include "shunya/components/shunya_ads/core/public/ads_util.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/ntp_background_images/browser/ntp_background_images_service.h"
#include "shunya/components/ntp_background_images/browser/ntp_background_images_source.h"
#include "shunya/components/ntp_background_images/browser/ntp_sponsored_images_source.h"
#include "shunya/components/ntp_background_images/browser/view_counter_service.h"
#include "shunya/components/ntp_background_images/buildflags/buildflags.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/profiles/profile.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/keyed_service/content/browser_context_keyed_service_factory.h"
#include "components/pref_registry/pref_registry_syncable.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/browser_context.h"
#include "content/public/browser/url_data_source.h"

#if BUILDFLAG(ENABLE_CUSTOM_BACKGROUND)
#include "shunya/browser/ntp_background/shunya_ntp_custom_background_service_factory.h"
#endif

namespace ntp_background_images {

// static
ViewCounterService* ViewCounterServiceFactory::GetForProfile(Profile* profile) {
  return static_cast<ViewCounterService*>(
      GetInstance()->GetServiceForBrowserContext(profile, true));
}

// static
ViewCounterServiceFactory* ViewCounterServiceFactory::GetInstance() {
  static base::NoDestructor<ViewCounterServiceFactory> instance;
  return instance.get();
}

ViewCounterServiceFactory::ViewCounterServiceFactory()
    : BrowserContextKeyedServiceFactory(
          "ViewCounterService",
          BrowserContextDependencyManager::GetInstance()) {
  DependsOn(shunya_ads::AdsServiceFactory::GetInstance());
#if BUILDFLAG(ENABLE_CUSTOM_BACKGROUND)
  DependsOn(ShunyaNTPCustomBackgroundServiceFactory::GetInstance());
#endif
}

ViewCounterServiceFactory::~ViewCounterServiceFactory() = default;

KeyedService* ViewCounterServiceFactory::BuildServiceInstanceFor(
    content::BrowserContext* browser_context) const {
  // Only NTP in normal profile uses sponsored services.
  if (!shunya::IsRegularProfile(browser_context))
    return nullptr;

  if (auto* service =
          g_shunya_browser_process->ntp_background_images_service()) {
    Profile* profile = Profile::FromBrowserContext(browser_context);
    bool is_supported_locale = false;
    auto* ads_service = shunya_ads::AdsServiceFactory::GetForProfile(profile);
    if (ads_service) {
      is_supported_locale = shunya_ads::IsSupportedRegion();
    }
    content::URLDataSource::Add(
        browser_context, std::make_unique<NTPBackgroundImagesSource>(service));
    content::URLDataSource::Add(
        browser_context, std::make_unique<NTPSponsoredImagesSource>(service));

    std::unique_ptr<NTPP3AHelperImpl> ntp_p3a_helper;
    if (g_shunya_browser_process->p3a_service() != nullptr) {
      ntp_p3a_helper = std::make_unique<NTPP3AHelperImpl>(
          g_browser_process->local_state(),
          g_shunya_browser_process->p3a_service(), profile->GetPrefs());
    }

    return new ViewCounterService(
        service,
#if BUILDFLAG(ENABLE_CUSTOM_BACKGROUND)
        ShunyaNTPCustomBackgroundServiceFactory::GetForContext(profile),
#else
        nullptr,
#endif
        ads_service, profile->GetPrefs(), g_browser_process->local_state(),
        std::move(ntp_p3a_helper), is_supported_locale);
  }

  return nullptr;
}

void ViewCounterServiceFactory::RegisterProfilePrefs(
    user_prefs::PrefRegistrySyncable* registry) {
  ViewCounterService::RegisterProfilePrefs(registry);
}

bool ViewCounterServiceFactory::ServiceIsCreatedWithBrowserContext() const {
  return true;
}

}  // namespace ntp_background_images

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_NTP_BACKGROUND_SHUNYA_NTP_CUSTOM_BACKGROUND_SERVICE_FACTORY_H_
#define SHUNYA_BROWSER_NTP_BACKGROUND_SHUNYA_NTP_CUSTOM_BACKGROUND_SERVICE_FACTORY_H_

#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace ntp_background_images {
class ShunyaNTPCustomBackgroundService;
}

class ShunyaNTPCustomBackgroundServiceFactory
    : public BrowserContextKeyedServiceFactory {
 public:
  static ntp_background_images::ShunyaNTPCustomBackgroundService* GetForContext(
      content::BrowserContext* context);
  static ShunyaNTPCustomBackgroundServiceFactory* GetInstance();

 private:
  friend base::NoDestructor<ShunyaNTPCustomBackgroundServiceFactory>;

  ShunyaNTPCustomBackgroundServiceFactory();
  ~ShunyaNTPCustomBackgroundServiceFactory() override;

  ShunyaNTPCustomBackgroundServiceFactory(
      const ShunyaNTPCustomBackgroundServiceFactory&) = delete;
  ShunyaNTPCustomBackgroundServiceFactory& operator=(
      const ShunyaNTPCustomBackgroundServiceFactory&) = delete;

  // BrowserContextKeyedServiceFactory overrides:
  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
};

#endif  // SHUNYA_BROWSER_NTP_BACKGROUND_SHUNYA_NTP_CUSTOM_BACKGROUND_SERVICE_FACTORY_H_

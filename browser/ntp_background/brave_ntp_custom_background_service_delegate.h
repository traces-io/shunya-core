// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_NTP_BACKGROUND_SHUNYA_NTP_CUSTOM_BACKGROUND_SERVICE_DELEGATE_H_
#define SHUNYA_BROWSER_NTP_BACKGROUND_SHUNYA_NTP_CUSTOM_BACKGROUND_SERVICE_DELEGATE_H_

#include <memory>
#include <string>

#include "base/functional/callback.h"
#include "base/functional/callback_helpers.h"
#include "base/gtest_prod_util.h"
#include "base/memory/raw_ptr.h"
#include "shunya/components/ntp_background_images/browser/shunya_ntp_custom_background_service.h"

class CustomBackgroundFileManager;
class Profile;

class ShunyaNTPCustomBackgroundServiceDelegate
    : public ntp_background_images::ShunyaNTPCustomBackgroundService::Delegate {
 public:
  explicit ShunyaNTPCustomBackgroundServiceDelegate(Profile* profile);
  ~ShunyaNTPCustomBackgroundServiceDelegate() override;
  ShunyaNTPCustomBackgroundServiceDelegate(
      const ShunyaNTPCustomBackgroundServiceDelegate&) = delete;
  ShunyaNTPCustomBackgroundServiceDelegate& operator=(
      const ShunyaNTPCustomBackgroundServiceDelegate&) = delete;

 private:
  FRIEND_TEST_ALL_PREFIXES(ShunyaNTPCustomBackgroundServiceDelegateUnitTest,
                           MigrationSuccess);
  FRIEND_TEST_ALL_PREFIXES(ShunyaNTPCustomBackgroundServiceDelegateUnitTest,
                           MigrationFail);

  bool ShouldMigrateCustomImagePref() const;
  void MigrateCustomImage(
      base::OnceCallback<void(bool)> callback = base::DoNothing());

  // ShunyaNTPCustomBackgroundService::Delegate overrides:
  bool IsCustomImageBackgroundEnabled() const override;
  base::FilePath GetCustomBackgroundImageLocalFilePath(
      const GURL& url) const override;
  GURL GetCustomBackgroundImageURL() const override;
  bool IsColorBackgroundEnabled() const override;
  std::string GetColor() const override;
  bool ShouldUseRandomValue() const override;
  bool HasPreferredShunyaBackground() const override;
  base::Value::Dict GetPreferredShunyaBackground() const override;

  raw_ptr<Profile> profile_ = nullptr;

  std::unique_ptr<CustomBackgroundFileManager> file_manager_;
};

#endif  // SHUNYA_BROWSER_NTP_BACKGROUND_SHUNYA_NTP_CUSTOM_BACKGROUND_SERVICE_DELEGATE_H_

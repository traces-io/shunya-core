// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/browser/ntp_background/shunya_ntp_custom_background_service_factory.h"

#include <memory>

#include "base/no_destructor.h"
#include "shunya/browser/ntp_background/shunya_ntp_custom_background_service_delegate.h"
#include "shunya/browser/profiles/profile_util.h"
#include "shunya/components/ntp_background_images/browser/shunya_ntp_custom_background_service.h"
#include "chrome/browser/profiles/profile.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"

// static
ntp_background_images::ShunyaNTPCustomBackgroundService*
ShunyaNTPCustomBackgroundServiceFactory::GetForContext(
    content::BrowserContext* context) {
  return static_cast<ntp_background_images::ShunyaNTPCustomBackgroundService*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

// static
ShunyaNTPCustomBackgroundServiceFactory*
ShunyaNTPCustomBackgroundServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaNTPCustomBackgroundServiceFactory> instance;
  return instance.get();
}

ShunyaNTPCustomBackgroundServiceFactory::ShunyaNTPCustomBackgroundServiceFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaNTPCustomBackgroundService",
          BrowserContextDependencyManager::GetInstance()) {}

ShunyaNTPCustomBackgroundServiceFactory::
    ~ShunyaNTPCustomBackgroundServiceFactory() = default;

KeyedService* ShunyaNTPCustomBackgroundServiceFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  // Custom NTP background is only used in normal profile.
  if (!shunya::IsRegularProfile(context)) {
    return nullptr;
  }

  return new ntp_background_images::ShunyaNTPCustomBackgroundService(
      std::make_unique<ShunyaNTPCustomBackgroundServiceDelegate>(
          Profile::FromBrowserContext(context)));
}

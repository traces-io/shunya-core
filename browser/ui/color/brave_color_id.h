/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_COLOR_SHUNYA_COLOR_ID_H_
#define SHUNYA_BROWSER_UI_COLOR_SHUNYA_COLOR_ID_H_

#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "chrome/browser/ui/color/chrome_color_id.h"

// clang-format off

#define SHUNYA_COMMON_COLOR_IDS                  \
    E_CPONLY(kColorForTest)                     \
    E_CPONLY(kColorIconBase)                    \
    E_CPONLY(kColorMenuItemSubText)             \
    E_CPONLY(kColorBookmarkBarInstructionsText) \
    E_CPONLY(kColorLocationBarFocusRing)        \
    E_CPONLY(kColorDialogDontAskAgainButton)    \
    E_CPONLY(kColorDialogDontAskAgainButtonHovered) \
    E_CPONLY(kColorWebDiscoveryInfoBarBackground)   \
    E_CPONLY(kColorWebDiscoveryInfoBarMessage)      \
    E_CPONLY(kColorWebDiscoveryInfoBarLink)         \
    E_CPONLY(kColorWebDiscoveryInfoBarNoThanks)     \
    E_CPONLY(kColorWebDiscoveryInfoBarClose)        \
    E_CPONLY(kColorShunyaDownloadToolbarButtonActive)

#define SHUNYA_SEARCH_CONVERSION_COLOR_IDS                             \
    E_CPONLY(kColorSearchConversionCloseButton)                       \
    E_CPONLY(kColorSearchConversionBannerTypeBackgroundBorder)        \
    E_CPONLY(kColorSearchConversionBannerTypeBackgroundBorderHovered) \
    E_CPONLY(kColorSearchConversionBannerTypeBackgroundGradientFrom)  \
    E_CPONLY(kColorSearchConversionBannerTypeBackgroundGradientTo)    \
    E_CPONLY(kColorSearchConversionBannerTypeDescText)                \
    E_CPONLY(kColorSearchConversionButtonTypeBackgroundNormal)        \
    E_CPONLY(kColorSearchConversionButtonTypeBackgroundHovered)       \
    E_CPONLY(kColorSearchConversionButtonTypeDescNormal)              \
    E_CPONLY(kColorSearchConversionButtonTypeDescHovered)             \
    E_CPONLY(kColorSearchConversionButtonTypeInputAppend)

#define SHUNYA_SIDEBAR_COLOR_IDS                               \
    E_CPONLY(kColorSidebarAddBubbleBackground)                \
    E_CPONLY(kColorSidebarAddBubbleHeaderText)                \
    E_CPONLY(kColorSidebarAddBubbleItemTextBackgroundHovered) \
    E_CPONLY(kColorSidebarAddBubbleItemTextHovered)           \
    E_CPONLY(kColorSidebarAddBubbleItemTextNormal)            \
    E_CPONLY(kColorSidebarAddButtonDisabled)                  \
    E_CPONLY(kColorSidebarArrowBackgroundHovered)             \
    E_CPONLY(kColorSidebarArrowDisabled)                      \
    E_CPONLY(kColorSidebarArrowNormal)                        \
    E_CPONLY(kColorSidebarButtonBase)                         \
    E_CPONLY(kColorSidebarButtonPressed)                      \
    E_CPONLY(kColorSidebarItemBackgroundHovered)              \
    E_CPONLY(kColorSidebarItemDragIndicator)                  \
    E_CPONLY(kColorSidebarSeparator)                          \
    E_CPONLY(kColorSidebarPanelHeaderSeparator)               \
    E_CPONLY(kColorSidebarPanelHeaderBackground)              \
    E_CPONLY(kColorSidebarPanelHeaderTitle)

#if BUILDFLAG(ENABLE_SPEEDREADER)
#define SHUNYA_SPEEDREADER_COLOR_IDS      \
  E_CPONLY(kColorSpeedreaderIcon)        \
  E_CPONLY(kColorSpeedreaderToggleThumb) \
  E_CPONLY(kColorSpeedreaderToggleTrack) \
  E_CPONLY(kColorSpeedreaderToolbarBackground) \
  E_CPONLY(kColorSpeedreaderToolbarBorder) \
  E_CPONLY(kColorSpeedreaderToolbarForeground) \
  E_CPONLY(kColorSpeedreaderToolbarButtonHover) \
  E_CPONLY(kColorSpeedreaderToolbarButtonActive) \
  E_CPONLY(kColorSpeedreaderToolbarButtonBorder)
#else
#define SHUNYA_SPEEDREADER_COLOR_IDS
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#define SHUNYA_VPN_COLOR_IDS                        \
    E_CPONLY(kColorShunyaVpnButtonErrorBorder)      \
    E_CPONLY(kColorShunyaVpnButtonBorder)           \
    E_CPONLY(kColorShunyaVpnButtonText)    \
    E_CPONLY(kColorShunyaVpnButtonTextError)    \
    E_CPONLY(kColorShunyaVpnButtonIconConnected)    \
    E_CPONLY(kColorShunyaVpnButtonIconDisconnected) \
    E_CPONLY(kColorShunyaVpnButtonIconError) \
    E_CPONLY(kColorShunyaVpnButtonBackgroundNormal) \
    E_CPONLY(kColorShunyaVpnButtonBackgroundHover)  \
    E_CPONLY(kColorShunyaVpnButtonErrorBackgroundNormal) \
    E_CPONLY(kColorShunyaVpnButtonErrorBackgroundHover)  \
    E_CPONLY(kColorShunyaVpnButtonIconInner) \
    E_CPONLY(kColorShunyaVpnButtonIconErrorInner)
#else
#define SHUNYA_VPN_COLOR_IDS
#endif

// Unfortunately, we can't have a defined(TOOLKIT_VIEWS) guard here
// as shunya_color_mixer depends on this without deps to //ui/views:flags.
// But it's safe have without the guard as this file is included only when
// !is_android.
#define SHUNYA_VERTICAL_TAB_COLOR_IDS                    \
    E_CPONLY(kColorShunyaVerticalTabSeparator)           \
    E_CPONLY(kColorShunyaVerticalTabActiveBackground)    \
    E_CPONLY(kColorShunyaVerticalTabInactiveBackground)  \
    E_CPONLY(kColorShunyaVerticalTabHeaderButtonColor)   \
    E_CPONLY(kColorShunyaVerticalTabNTBIconColor)        \
    E_CPONLY(kColorShunyaVerticalTabNTBTextColor)        \
    E_CPONLY(kColorShunyaVerticalTabNTBShortcutTextColor)

#define SHUNYA_PLAYLIST_COLOR_IDS                                      \
    E_CPONLY(kColorShunyaPlaylistAddedIcon)                            \
    E_CPONLY(kColorShunyaPlaylistCheckedIcon)                          \
    E_CPONLY(kColorShunyaPlaylistSelectedBackground)                   \
    E_CPONLY(kColorShunyaPlaylistListBorder)                           \
    E_CPONLY(kColorShunyaPlaylistMoveDialogDescription)                \
    E_CPONLY(kColorShunyaPlaylistMoveDialogCreatePlaylistAndMoveTitle) \
    E_CPONLY(kColorShunyaPlaylistNewPlaylistDialogNameLabel)           \
    E_CPONLY(kColorShunyaPlaylistNewPlaylistDialogItemsLabel)

#define SHUNYA_COLOR_IDS               \
    SHUNYA_COMMON_COLOR_IDS            \
    SHUNYA_SEARCH_CONVERSION_COLOR_IDS \
    SHUNYA_SIDEBAR_COLOR_IDS           \
    SHUNYA_SPEEDREADER_COLOR_IDS       \
    SHUNYA_VPN_COLOR_IDS               \
    SHUNYA_VERTICAL_TAB_COLOR_IDS      \
    SHUNYA_PLAYLIST_COLOR_IDS

#include "ui/color/color_id_macros.inc"

enum ShunyaColorIds : ui::ColorId {
  kShunyaColorsStart = kChromeColorsEnd,

  SHUNYA_COLOR_IDS

  kShunyaColorsEnd,
};

#include "ui/color/color_id_macros.inc"  // NOLINT

// clang-format on

#endif  // SHUNYA_BROWSER_UI_COLOR_SHUNYA_COLOR_ID_H_

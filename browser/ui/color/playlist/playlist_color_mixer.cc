/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/color/playlist/playlist_color_mixer.h"

#include "shunya/browser/ui/color/shunya_color_id.h"
#include "ui/color/color_mixer.h"
#include "ui/color/color_provider.h"
#include "ui/color/color_recipe.h"

namespace playlist {

void AddThemeColorMixer(ui::ColorProvider* provider,
                        leo::Theme theme,
                        const ui::ColorProviderKey& key) {
  ui::ColorMixer& mixer = provider->AddMixer();
  mixer[kColorShunyaPlaylistAddedIcon] = {
      leo::GetColor(leo::Color::kColorSystemfeedbackSuccessIcon, theme)};
  mixer[kColorShunyaPlaylistCheckedIcon] = {
      leo::GetColor(leo::Color::kColorIconInteractive, theme)};
  mixer[kColorShunyaPlaylistSelectedBackground] = {
      leo::GetColor(leo::Color::kColorContainerInteractive, theme)};
  mixer[kColorShunyaPlaylistListBorder] = {
      leo::GetColor(leo::Color::kColorDividerSubtle, theme)};
  mixer[kColorShunyaPlaylistMoveDialogDescription] = {
      leo::GetColor(leo::Color::kColorTextSecondary, theme)};
  mixer[kColorShunyaPlaylistMoveDialogCreatePlaylistAndMoveTitle] = {
      leo::GetColor(leo::Color::kColorTextPrimary, theme)};
  mixer[kColorShunyaPlaylistNewPlaylistDialogNameLabel] = {
      leo::GetColor(leo::Color::kColorTextPrimary, theme)};
  mixer[kColorShunyaPlaylistNewPlaylistDialogItemsLabel] = {
      leo::GetColor(leo::Color::kColorTextSecondary, theme)};
}

}  // namespace playlist

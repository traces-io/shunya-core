/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_SHUNYA_BROWSER_CONTENT_SETTING_BUBBLE_MODEL_DELEGATE_H_
#define SHUNYA_BROWSER_UI_SHUNYA_BROWSER_CONTENT_SETTING_BUBBLE_MODEL_DELEGATE_H_

#include "base/memory/raw_ptr.h"
#include "chrome/browser/ui/browser_content_setting_bubble_model_delegate.h"

class ShunyaBrowserContentSettingBubbleModelDelegate
    : public BrowserContentSettingBubbleModelDelegate {
 public:
  explicit ShunyaBrowserContentSettingBubbleModelDelegate(Browser* browser);
  ShunyaBrowserContentSettingBubbleModelDelegate(
      const ShunyaBrowserContentSettingBubbleModelDelegate&) = delete;
  ShunyaBrowserContentSettingBubbleModelDelegate& operator=(
      const ShunyaBrowserContentSettingBubbleModelDelegate&) = delete;
  ~ShunyaBrowserContentSettingBubbleModelDelegate() override;

  void ShowWidevineLearnMorePage();
  void ShowLearnMorePage(ContentSettingsType type) override;

 private:
  const raw_ptr<Browser> browser_;
};

#endif  // SHUNYA_BROWSER_UI_SHUNYA_BROWSER_CONTENT_SETTING_BUBBLE_MODEL_DELEGATE_H_

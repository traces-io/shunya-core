/* Copyright (c) 2018 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/shunya_browser_content_setting_bubble_model_delegate.h"

#include "shunya/components/constants/url_constants.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_tabstrip.h"

const char kShunyaCommunitySupportUrl[] = "https://community.shunya.com/";

ShunyaBrowserContentSettingBubbleModelDelegate::
ShunyaBrowserContentSettingBubbleModelDelegate(Browser* browser) :
    BrowserContentSettingBubbleModelDelegate(browser),
    browser_(browser) {
}

ShunyaBrowserContentSettingBubbleModelDelegate::
    ~ShunyaBrowserContentSettingBubbleModelDelegate() = default;

void
ShunyaBrowserContentSettingBubbleModelDelegate::ShowWidevineLearnMorePage() {
  GURL learn_more_url = GURL(kWidevineTOS);
  chrome::AddSelectedTabWithURL(browser_, learn_more_url,
                                ui::PAGE_TRANSITION_LINK);
}

void ShunyaBrowserContentSettingBubbleModelDelegate::ShowLearnMorePage(
    ContentSettingsType type) {
  // TODO(yrliou): Use specific support pages for each content setting type
  GURL learn_more_url(kShunyaCommunitySupportUrl);
  chrome::AddSelectedTabWithURL(browser_, learn_more_url,
                                ui::PAGE_TRANSITION_LINK);
}

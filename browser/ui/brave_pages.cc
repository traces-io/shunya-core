/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/shunya_pages.h"

#include "base/strings/strcat.h"
#include "shunya/browser/ui/webui/webcompat_reporter/webcompat_reporter_dialog.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "shunya/components/constants/webui_url_constants.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/sidebar/constants.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_navigator_params.h"
#include "chrome/browser/ui/chrome_pages.h"
#include "chrome/browser/ui/singleton_tabs.h"
#include "chrome/common/webui_url_constants.h"
#include "url/gurl.h"

namespace shunya {

void ShowShunyaRewards(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(kShunyaUIRewardsURL));
}

void ShowShunyaAdblock(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(kShunyaUIAdblockURL));
}

void ShowSync(Browser* browser) {
  ShowSingletonTabOverwritingNTP(
      browser, chrome::GetSettingsUrl(chrome::kSyncSetupSubPage));
}

void ShowShunyaNewsConfigure(Browser* browser) {
  ShowSingletonTabOverwritingNTP(
      browser, GURL("shunya://newtab/?openSettings=ShunyaNews"));
}

void ShowShortcutsPage(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(kShortcutsURL));
}

void ShowShunyaTalk(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(sidebar::kShunyaTalkURL));
}

void ShowWebcompatReporter(Browser* browser) {
  content::WebContents* web_contents =
      browser->tab_strip_model()->GetActiveWebContents();
  if (!web_contents) {
    return;
  }

  webcompat_reporter::OpenReporterDialog(web_contents);
}

void ShowShunyaWallet(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(kShunyaUIWalletURL));
}

void ShowShunyaWalletOnboarding(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(kShunyaUIWalletOnboardingURL));
}

void ShowShunyaWalletAccountCreation(Browser* browser,
                                    shunya_wallet::mojom::CoinType coin_type) {
  // Only solana is supported.
  if (coin_type == shunya_wallet::mojom::CoinType::SOL) {
    ShowSingletonTabOverwritingNTP(
        browser,
        GURL(base::StrCat({kShunyaUIWalletAccountCreationURL, "Solana"})));
  } else {
    NOTREACHED();
  }
}

void ShowExtensionSettings(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(kExtensionSettingsURL));
}

void ShowWalletSettings(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(kWalletSettingsURL));
}

void ShowIPFS(Browser* browser) {
  ShowSingletonTabOverwritingNTP(browser, GURL(kIPFSWebUIURL));
}

}  // namespace shunya

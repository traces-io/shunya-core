/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/browser_commands.h"

#include <string>
#include <vector>

#include "base/strings/utf_string_conversions.h"
#include "shunya/app/shunya_command_ids.h"
#include "shunya/browser/debounce/debounce_service_factory.h"
#include "shunya/browser/ui/shunya_shields_data_controller.h"
#include "shunya/browser/ui/sidebar/sidebar_service_factory.h"
#include "shunya/browser/ui/tabs/shunya_tab_prefs.h"
#include "shunya/browser/url_sanitizer/url_sanitizer_service_factory.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/debounce/browser/debounce_service.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/query_filter/utils.h"
#include "shunya/components/sidebar/sidebar_service.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "shunya/components/url_sanitizer/browser/url_sanitizer_service.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/profiles/profile_manager.h"
#include "chrome/browser/profiles/profile_metrics.h"
#include "chrome/browser/profiles/profile_window.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_commands.h"
#include "chrome/browser/ui/browser_tabstrip.h"
#include "chrome/browser/ui/profile_picker.h"
#include "chrome/browser/ui/tabs/tab_enums.h"
#include "chrome/browser/ui/tabs/tab_group.h"
#include "chrome/browser/ui/tabs/tab_group_model.h"
#include "chrome/browser/ui/tabs/tab_strip_model.h"
#include "chrome/browser/ui/tabs/tab_utils.h"
#include "chrome/common/pref_names.h"
#include "components/tab_groups/tab_group_visual_data.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/web_contents.h"
#include "ui/base/clipboard/clipboard_buffer.h"
#include "ui/base/clipboard/scoped_clipboard_writer.h"
#include "url/origin.h"

#if defined(TOOLKIT_VIEWS)
#include "shunya/browser/ui/views/frame/shunya_browser_view.h"
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/browser/speedreader/speedreader_service_factory.h"
#include "shunya/browser/speedreader/speedreader_tab_helper.h"
#include "shunya/components/speedreader/speedreader_service.h"
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/browser/tor/tor_profile_manager.h"
#include "shunya/browser/tor/tor_profile_service_factory.h"
#include "shunya/components/tor/tor_profile_service.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_constants.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"

#if BUILDFLAG(IS_WIN)
#include "shunya/components/shunya_vpn/common/wireguard/win/storage_utils.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/wireguard_utils_win.h"
#endif  // BUILDFLAG(ENABLE_SHUNYA_VPN)

#endif  // BUILDFLAG(ENABLE_SHUNYA_VPN)

#if BUILDFLAG(ENABLE_IPFS_LOCAL_NODE)
#include "shunya/components/ipfs/ipfs_utils.h"
#include "chrome/common/channel_info.h"
#endif

using content::WebContents;

namespace shunya {
void NewOffTheRecordWindowTor(Browser* browser) {
  CHECK(browser);
  if (browser->profile()->IsTor()) {
    chrome::OpenEmptyWindow(browser->profile());
    return;
  }

  TorProfileManager::SwitchToTorProfile(browser->profile());
}

void NewTorConnectionForSite(Browser* browser) {
#if BUILDFLAG(ENABLE_TOR)
  Profile* profile = browser->profile();
  DCHECK(profile);
  tor::TorProfileService* service =
      TorProfileServiceFactory::GetForContext(profile);
  DCHECK(service);
  WebContents* current_tab = browser->tab_strip_model()->GetActiveWebContents();
  if (!current_tab) {
    return;
  }
  service->SetNewTorCircuit(current_tab);
#endif
}

void MaybeDistillAndShowSpeedreaderBubble(Browser* browser) {
#if BUILDFLAG(ENABLE_SPEEDREADER)
  WebContents* contents = browser->tab_strip_model()->GetActiveWebContents();
  if (!contents) {
    return;
  }
  if (auto* tab_helper =
          speedreader::SpeedreaderTabHelper::FromWebContents(contents)) {
    tab_helper->ProcessIconClick();
  }
#endif  // BUILDFLAG(ENABLE_SPEEDREADER)
}

void ShowShunyaVPNBubble(Browser* browser) {
  // Ask to browser view.
  static_cast<ShunyaBrowserWindow*>(browser->window())->ShowShunyaVPNBubble();
}

void ToggleShunyaVPNTrayIcon() {
#if BUILDFLAG(ENABLE_SHUNYA_VPN) && BUILDFLAG(IS_WIN)
  shunya_vpn::EnableVPNTrayIcon(!shunya_vpn::IsVPNTrayIconEnabled());
  if (shunya_vpn::IsVPNTrayIconEnabled()) {
    shunya_vpn::wireguard::ShowShunyaVpnStatusTrayIcon();
  }
#endif
}

void ToggleShunyaVPNButton(Browser* browser) {
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  auto* prefs = browser->profile()->GetPrefs();
  const bool show = prefs->GetBoolean(shunya_vpn::prefs::kShunyaVPNShowButton);
  prefs->SetBoolean(shunya_vpn::prefs::kShunyaVPNShowButton, !show);
#endif
}

void OpenIpfsFilesWebUI(Browser* browser) {
#if BUILDFLAG(ENABLE_IPFS_LOCAL_NODE)
  auto* prefs = browser->profile()->GetPrefs();
  DCHECK(ipfs::IsLocalGatewayConfigured(prefs));
  GURL gateway = ipfs::GetAPIServer(chrome::GetChannel());
  GURL::Replacements replacements;
  replacements.SetPathStr("/webui/");
  replacements.SetRefStr("/files");
  auto target_url = gateway.ReplaceComponents(replacements);
  chrome::AddTabAt(browser, GURL(target_url), -1, true);
#endif
}

void OpenShunyaVPNUrls(Browser* browser, int command_id) {
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  shunya_vpn::ShunyaVpnService* vpn_service =
      shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(browser->profile());
  std::string target_url;
  switch (command_id) {
    case IDC_SEND_SHUNYA_VPN_FEEDBACK:
      target_url = shunya_vpn::kFeedbackUrl;
      break;
    case IDC_ABOUT_SHUNYA_VPN:
      target_url = shunya_vpn::kAboutUrl;
      break;
    case IDC_MANAGE_SHUNYA_VPN_PLAN:
      target_url =
          shunya_vpn::GetManageUrl(vpn_service->GetCurrentEnvironment());
      break;
    default:
      NOTREACHED();
  }

  chrome::AddTabAt(browser, GURL(target_url), -1, true);
#endif
}

void ShowWalletBubble(Browser* browser) {
#if defined(TOOLKIT_VIEWS)
  static_cast<ShunyaBrowserView*>(browser->window())->CreateWalletBubble();
#endif
}

void ShowApproveWalletBubble(Browser* browser) {
#if defined(TOOLKIT_VIEWS)
  static_cast<ShunyaBrowserView*>(browser->window())
      ->CreateApproveWalletBubble();
#endif
}

void CloseWalletBubble(Browser* browser) {
#if defined(TOOLKIT_VIEWS)
  static_cast<ShunyaBrowserView*>(browser->window())->CloseWalletBubble();
#endif
}

void CopySanitizedURL(Browser* browser, const GURL& url) {
  if (!browser || !browser->profile()) {
    return;
  }
  GURL sanitized_url = shunya::URLSanitizerServiceFactory::GetForBrowserContext(
                           browser->profile())
                           ->SanitizeURL(url);

  ui::ScopedClipboardWriter scw(ui::ClipboardBuffer::kCopyPaste);
  scw.WriteText(base::UTF8ToUTF16(sanitized_url.spec()));
}

// Copies an url cleared through:
// - Debouncer (potentially debouncing many levels)
// - Query filter
// - URLSanitizerService
void CopyLinkWithStrictCleaning(Browser* browser, const GURL& url) {
  if (!browser || !browser->profile()) {
    return;
  }
  DCHECK(url.SchemeIsHTTPOrHTTPS());
  GURL final_url;
  // Apply debounce rules.
  auto* debounce_service =
      debounce::DebounceServiceFactory::GetForBrowserContext(
          browser->profile());
  if (debounce_service && !debounce_service->Debounce(url, &final_url)) {
    VLOG(1) << "Unable to apply debounce rules";
    final_url = url;
  }
  // Apply query filters.
  auto filtered_url = query_filter::ApplyQueryFilter(final_url);
  if (filtered_url.has_value()) {
    final_url = filtered_url.value();
  }
  // Sanitize url.
  final_url = shunya::URLSanitizerServiceFactory::GetForBrowserContext(
                  browser->profile())
                  ->SanitizeURL(final_url);

  ui::ScopedClipboardWriter scw(ui::ClipboardBuffer::kCopyPaste);
  scw.WriteText(base::UTF8ToUTF16(final_url.spec()));
}

void ToggleWindowTitleVisibilityForVerticalTabs(Browser* browser) {
  auto* prefs = browser->profile()->GetOriginalProfile()->GetPrefs();
  prefs->SetBoolean(
      shunya_tabs::kVerticalTabsShowTitleOnWindow,
      !prefs->GetBoolean(shunya_tabs::kVerticalTabsShowTitleOnWindow));
}

void ToggleVerticalTabStrip(Browser* browser) {
  auto* profile = browser->profile()->GetOriginalProfile();
  auto* prefs = profile->GetPrefs();
  const bool was_using_vertical_tab_strip =
      prefs->GetBoolean(shunya_tabs::kVerticalTabsEnabled);
  prefs->SetBoolean(shunya_tabs::kVerticalTabsEnabled,
                    !was_using_vertical_tab_strip);
}

void ToggleVerticalTabStripFloatingMode(Browser* browser) {
  auto* prefs = browser->profile()->GetOriginalProfile()->GetPrefs();
  prefs->SetBoolean(
      shunya_tabs::kVerticalTabsFloatingEnabled,
      !prefs->GetBoolean(shunya_tabs::kVerticalTabsFloatingEnabled));
}

void ToggleVerticalTabStripExpanded(Browser* browser) {
  auto* prefs = browser->profile()->GetPrefs();
  prefs->SetBoolean(shunya_tabs::kVerticalTabsCollapsed,
                    !prefs->GetBoolean(shunya_tabs::kVerticalTabsCollapsed));
}

void ToggleActiveTabAudioMute(Browser* browser) {
  WebContents* contents = browser->tab_strip_model()->GetActiveWebContents();
  if (!contents || !contents->IsCurrentlyAudible()) {
    return;
  }

  bool mute_tab = !contents->IsAudioMuted();
  chrome::SetTabAudioMuted(contents, mute_tab, TabMutedReason::AUDIO_INDICATOR,
                           std::string());
}

void ToggleSidebarPosition(Browser* browser) {
  auto* prefs = browser->profile()->GetPrefs();
  prefs->SetBoolean(prefs::kSidePanelHorizontalAlignment,
                    !prefs->GetBoolean(prefs::kSidePanelHorizontalAlignment));
}

void ToggleSidebar(Browser* browser) {
  if (!browser) {
    return;
  }

  if (auto* shunya_browser_window =
          ShunyaBrowserWindow::From(browser->window())) {
    shunya_browser_window->ToggleSidebar();
  }
}

bool HasSelectedURL(Browser* browser) {
  if (!browser) {
    return false;
  }
  auto* shunya_browser_window = ShunyaBrowserWindow::From(browser->window());
  return shunya_browser_window && shunya_browser_window->HasSelectedURL();
}

void CleanAndCopySelectedURL(Browser* browser) {
  if (!browser) {
    return;
  }
  auto* shunya_browser_window = ShunyaBrowserWindow::From(browser->window());
  if (shunya_browser_window) {
    shunya_browser_window->CleanAndCopySelectedURL();
  }
}

void ToggleShieldsEnabled(Browser* browser) {
  if (!browser) {
    return;
  }

  auto* contents = browser->tab_strip_model()->GetActiveWebContents();
  if (!contents) {
    return;
  }
  auto* shields =
      shunya_shields::ShunyaShieldsDataController::FromWebContents(contents);
  if (!shields) {
    return;
  }

  shields->SetShunyaShieldsEnabled(!shields->GetShunyaShieldsEnabled());
}

void ToggleJavascriptEnabled(Browser* browser) {
  if (!browser) {
    return;
  }

  auto* contents = browser->tab_strip_model()->GetActiveWebContents();
  if (!contents) {
    return;
  }
  auto* shields =
      shunya_shields::ShunyaShieldsDataController::FromWebContents(contents);
  if (!shields) {
    return;
  }

  shields->SetIsNoScriptEnabled(!shields->GetNoScriptEnabled());
}

#if BUILDFLAG(ENABLE_PLAYLIST_WEBUI)
void ShowPlaylistBubble(Browser* browser) {
  ShunyaBrowserWindow::From(browser->window())->ShowPlaylistBubble();
}
#endif

void GroupTabsOnCurrentOrigin(Browser* browser) {
  auto url =
      browser->tab_strip_model()->GetActiveWebContents()->GetVisibleURL();
  auto origin = url::Origin::Create(url);

  std::vector<int> group_indices;
  for (int index = 0; index < browser->tab_strip_model()->count(); ++index) {
    auto* tab = browser->tab_strip_model()->GetWebContentsAt(index);
    auto tab_origin = url::Origin::Create(tab->GetVisibleURL());
    if (origin.IsSameOriginWith(tab_origin)) {
      group_indices.push_back(index);
    }
  }
  auto group_id = browser->tab_strip_model()->AddToNewGroup(group_indices);
  auto* group =
      browser->tab_strip_model()->group_model()->GetTabGroup(group_id);

  auto data = *group->visual_data();
  data.SetTitle(base::UTF8ToUTF16(origin.host()));
  group->SetVisualData(data);
}

void MoveGroupToNewWindow(Browser* browser) {
  auto* tsm = browser->tab_strip_model();
  auto current_group_id = tsm->GetTabGroupForTab(tsm->active_index());
  if (!current_group_id.has_value()) {
    return;
  }

  tsm->delegate()->MoveGroupToNewWindow(current_group_id.value());
}

void CloseDuplicateTabs(Browser* browser) {
  auto* tsm = browser->tab_strip_model();
  auto url = tsm->GetActiveWebContents()->GetVisibleURL();

  for (int i = tsm->GetTabCount() - 1; i >= 0; --i) {
    // Don't close the active tab.
    if (tsm->active_index() == i) {
      continue;
    }

    auto* tab = tsm->GetWebContentsAt(i);
    if (tab->GetVisibleURL() == url) {
      tab->Close();
    }
  }
}

}  // namespace shunya

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_OMNIBOX_SHUNYA_OMNIBOX_CLIENT_IMPL_H_
#define SHUNYA_BROWSER_UI_OMNIBOX_SHUNYA_OMNIBOX_CLIENT_IMPL_H_

#include "base/memory/raw_ptr.h"
#include "shunya/browser/autocomplete/shunya_autocomplete_scheme_classifier.h"
#include "chrome/browser/ui/omnibox/chrome_omnibox_client.h"

class PrefRegistrySimple;
class Profile;
class SearchEngineTracker;

class ShunyaOmniboxClientImpl : public ChromeOmniboxClient {
 public:
  ShunyaOmniboxClientImpl(LocationBar* location_bar,
                         Browser* browser,
                         Profile* profile);
  ShunyaOmniboxClientImpl(const ShunyaOmniboxClientImpl&) = delete;
  ShunyaOmniboxClientImpl& operator=(const ShunyaOmniboxClientImpl&) = delete;
  ~ShunyaOmniboxClientImpl() override;

  static void RegisterProfilePrefs(PrefRegistrySimple* prefs);

  const AutocompleteSchemeClassifier& GetSchemeClassifier() const override;
  bool IsAutocompleteEnabled() const override;

  void OnURLOpenedFromOmnibox(OmniboxLog* log) override;

  void OnAutocompleteAccept(
      const GURL& destination_url,
      TemplateURLRef::PostContent* post_content,
      WindowOpenDisposition disposition,
      ui::PageTransition transition,
      AutocompleteMatchType::Type match_type,
      base::TimeTicks match_selection_timestamp,
      bool destination_url_entered_without_scheme,
      bool destination_url_entered_with_http_scheme,
      const std::u16string& text,
      const AutocompleteMatch& match,
      const AutocompleteMatch& alternative_nav_match,
      IDNA2008DeviationCharacter deviation_char_in_hostname) override;

 private:
  raw_ptr<Profile> profile_ = nullptr;
  raw_ptr<SearchEngineTracker> search_engine_tracker_ = nullptr;
  ShunyaAutocompleteSchemeClassifier scheme_classifier_;
};

#endif  // SHUNYA_BROWSER_UI_OMNIBOX_SHUNYA_OMNIBOX_CLIENT_IMPL_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_BOOKMARK_SHUNYA_BOOKMARK_TAB_HELPER_H_
#define SHUNYA_BROWSER_UI_BOOKMARK_SHUNYA_BOOKMARK_TAB_HELPER_H_

#include "chrome/browser/ui/bookmarks/bookmark_tab_helper.h"
#include "content/public/browser/web_contents_user_data.h"

class BookmarkTabHelperObserver;

// This proxies BookmarkTabHelper apis that used by Browser.
class ShunyaBookmarkTabHelper
    : public content::WebContentsUserData<ShunyaBookmarkTabHelper> {
 public:
  ShunyaBookmarkTabHelper(const ShunyaBookmarkTabHelper&) = delete;
  ShunyaBookmarkTabHelper& operator=(const ShunyaBookmarkTabHelper&) = delete;
  ~ShunyaBookmarkTabHelper() override;

  bool ShouldShowBookmarkBar();
  void AddObserver(BookmarkTabHelperObserver* observer);
  void RemoveObserver(BookmarkTabHelperObserver* observer);

 private:
  friend class content::WebContentsUserData<ShunyaBookmarkTabHelper>;

  explicit ShunyaBookmarkTabHelper(content::WebContents* web_contents);

  WEB_CONTENTS_USER_DATA_KEY_DECL();
};

#endif  // SHUNYA_BROWSER_UI_BOOKMARK_SHUNYA_BOOKMARK_TAB_HELPER_H_

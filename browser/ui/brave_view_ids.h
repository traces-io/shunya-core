/* Copyright 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_SHUNYA_VIEW_IDS_H_
#define SHUNYA_BROWSER_UI_SHUNYA_VIEW_IDS_H_

// See src/chrome/browser/ui/view_ids.h
enum ShunyaViewIds {
  SHUNYA_VIEW_ID_SPEEDREADER_BUTTON = 100000,
  SHUNYA_VIEW_ID_BOOKMARK_IMPORT_INSTRUCTION_VIEW = 100001,
  SHUNYA_VIEW_ID_WALLET_BUTTON = 100002,
};

#endif  // SHUNYA_BROWSER_UI_SHUNYA_VIEW_IDS_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_PAGE_ACTION_SHUNYA_PAGE_ACTION_ICON_TYPE_H_
#define SHUNYA_BROWSER_UI_PAGE_ACTION_SHUNYA_PAGE_ACTION_ICON_TYPE_H_

#include "chrome/browser/ui/page_action/page_action_icon_type.h"

namespace shunya {

#define DECLARE_SHUNYA_PAGE_ACTION_ICON_TYPE(NAME, VALUE) \
  constexpr PageActionIconType NAME = static_cast<PageActionIconType>(VALUE)

// Use negative values so that our values doesn't conflict with upstream values.
DECLARE_SHUNYA_PAGE_ACTION_ICON_TYPE(kUndefinedPageActionIconType, -1);
DECLARE_SHUNYA_PAGE_ACTION_ICON_TYPE(kPlaylistPageActionIconType, -2);

#undef DECLARE_SHUNYA_PAGE_ACTION_ICON_TYPE

}  // namespace shunya

#endif  // SHUNYA_BROWSER_UI_PAGE_ACTION_SHUNYA_PAGE_ACTION_ICON_TYPE_H_

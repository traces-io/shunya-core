/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/shunya_browser_command_controller.h"

#include <utility>
#include <vector>

#include "base/containers/contains.h"
#include "base/feature_list.h"
#include "base/notreached.h"
#include "shunya/app/shunya_command_ids.h"
#include "shunya/browser/profiles/profile_util.h"
#include "shunya/browser/ui/shunya_pages.h"
#include "shunya/browser/ui/browser_commands.h"
#include "shunya/browser/ui/sidebar/sidebar_utils.h"
#include "shunya/components/shunya_rewards/common/rewards_util.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/shunya_wallet/common/common_utils.h"
#include "shunya/components/commands/common/features.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/playlist/common/buildflags/buildflags.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "chrome/app/chrome_command_ids.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_commands.h"
#include "chrome/browser/ui/tabs/tab_strip_model.h"
#include "chrome/common/pref_names.h"
#include "components/prefs/pref_service.h"
#include "components/sync/base/command_line_switches.h"

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/browser/shunya_vpn/vpn_utils.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/components/speedreader/common/features.h"
#endif

#if BUILDFLAG(ENABLE_PLAYLIST_WEBUI)
#include "shunya/components/playlist/common/features.h"
#endif

namespace {

bool IsShunyaCommands(int id) {
  return id >= IDC_SHUNYA_COMMANDS_START && id <= IDC_SHUNYA_COMMANDS_LAST;
}

bool IsShunyaOverrideCommands(int id) {
  static std::vector<int> override_commands({
      IDC_NEW_WINDOW,
      IDC_NEW_INCOGNITO_WINDOW,
  });
  return base::Contains(override_commands, id);
}

}  // namespace

namespace chrome {

ShunyaBrowserCommandController::ShunyaBrowserCommandController(Browser* browser)
    : BrowserCommandController(browser),
      browser_(*browser),
      shunya_command_updater_(nullptr) {
  InitShunyaCommandState();
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  if (auto* vpn_service = shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(
          browser_->profile())) {
    Observe(vpn_service);
  }
#endif
}

ShunyaBrowserCommandController::~ShunyaBrowserCommandController() = default;

bool ShunyaBrowserCommandController::SupportsCommand(int id) const {
  return IsShunyaCommands(id) ? shunya_command_updater_.SupportsCommand(id)
                             : BrowserCommandController::SupportsCommand(id);
}

bool ShunyaBrowserCommandController::IsCommandEnabled(int id) const {
  return IsShunyaCommands(id) ? shunya_command_updater_.IsCommandEnabled(id)
                             : BrowserCommandController::IsCommandEnabled(id);
}

bool ShunyaBrowserCommandController::ExecuteCommandWithDisposition(
    int id,
    WindowOpenDisposition disposition,
    base::TimeTicks time_stamp) {
  return IsShunyaCommands(id) || IsShunyaOverrideCommands(id)
             ? ExecuteShunyaCommandWithDisposition(id, disposition, time_stamp)
             : BrowserCommandController::ExecuteCommandWithDisposition(
                   id, disposition, time_stamp);
}

void ShunyaBrowserCommandController::AddCommandObserver(
    int id,
    CommandObserver* observer) {
  IsShunyaCommands(id)
      ? shunya_command_updater_.AddCommandObserver(id, observer)
      : BrowserCommandController::AddCommandObserver(id, observer);
}

void ShunyaBrowserCommandController::RemoveCommandObserver(
    int id,
    CommandObserver* observer) {
  IsShunyaCommands(id)
      ? shunya_command_updater_.RemoveCommandObserver(id, observer)
      : BrowserCommandController::RemoveCommandObserver(id, observer);
}

void ShunyaBrowserCommandController::RemoveCommandObserver(
    CommandObserver* observer) {
  shunya_command_updater_.RemoveCommandObserver(observer);
  BrowserCommandController::RemoveCommandObserver(observer);
}

bool ShunyaBrowserCommandController::UpdateCommandEnabled(int id, bool state) {
  return IsShunyaCommands(id)
             ? shunya_command_updater_.UpdateCommandEnabled(id, state)
             : BrowserCommandController::UpdateCommandEnabled(id, state);
}

void ShunyaBrowserCommandController::InitShunyaCommandState() {
  // Sync, Rewards, and Wallet pages don't work in tor(guest) sessions.
  // They also don't work in private windows but they are redirected
  // to a normal window in this case.
  const bool is_guest_session = browser_->profile()->IsGuestSession();
  if (!is_guest_session) {
    // If Rewards is not supported due to OFAC sanctions we still want to show
    // the menu item.
    if (shunya_rewards::IsSupported(browser_->profile()->GetPrefs())) {
      UpdateCommandForShunyaRewards();
    }
    if (shunya_wallet::IsAllowed(browser_->profile()->GetPrefs())) {
      UpdateCommandForShunyaWallet();
    }
    if (syncer::IsSyncAllowedByFlag()) {
      UpdateCommandForShunyaSync();
    }
  }
  UpdateCommandForWebcompatReporter();
#if BUILDFLAG(ENABLE_TOR)
  UpdateCommandForTor();
#endif
  UpdateCommandForSidebar();
  UpdateCommandForShunyaVPN();
  UpdateCommandForPlaylist();
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  if (shunya_vpn::IsAllowedForContext(browser_->profile())) {
    shunya_vpn_pref_change_registrar_.Init(browser_->profile()->GetPrefs());
    shunya_vpn_pref_change_registrar_.Add(
        shunya_vpn::prefs::kManagedShunyaVPNDisabled,
        base::BindRepeating(
            &ShunyaBrowserCommandController::UpdateCommandForShunyaVPN,
            base::Unretained(this)));
  }
#endif
  bool add_new_profile_enabled = !is_guest_session;
  bool open_guest_profile_enabled = !is_guest_session;
  if (!is_guest_session) {
    if (PrefService* local_state = g_browser_process->local_state()) {
      add_new_profile_enabled =
          local_state->GetBoolean(prefs::kBrowserAddPersonEnabled);
      open_guest_profile_enabled =
          local_state->GetBoolean(prefs::kBrowserGuestModeEnabled);
    }
  }
  UpdateCommandEnabled(IDC_ADD_NEW_PROFILE, add_new_profile_enabled);
  UpdateCommandEnabled(IDC_OPEN_GUEST_PROFILE, open_guest_profile_enabled);
  UpdateCommandEnabled(IDC_COPY_CLEAN_LINK, true);
  UpdateCommandEnabled(IDC_TOGGLE_TAB_MUTE, true);

#if BUILDFLAG(ENABLE_SPEEDREADER)
  if (base::FeatureList::IsEnabled(speedreader::kSpeedreaderFeature)) {
    UpdateCommandEnabled(IDC_SPEEDREADER_ICON_ONCLICK, true);
    UpdateCommandEnabled(IDC_DISTILL_PAGE, false);
  }
#endif
#if BUILDFLAG(ENABLE_IPFS_LOCAL_NODE)
  UpdateCommandEnabled(IDC_APP_MENU_IPFS_OPEN_FILES, true);
#endif
  UpdateCommandEnabled(IDC_SHUNYA_BOOKMARK_BAR_SUBMENU, true);

  UpdateCommandEnabled(IDC_TOGGLE_VERTICAL_TABS, true);
  UpdateCommandEnabled(IDC_TOGGLE_VERTICAL_TABS_WINDOW_TITLE, true);
  UpdateCommandEnabled(IDC_TOGGLE_VERTICAL_TABS_EXPANDED, true);

  UpdateCommandEnabled(IDC_CONFIGURE_SHUNYA_NEWS,
                       !browser_->profile()->IsOffTheRecord());

  UpdateCommandEnabled(
      IDC_CONFIGURE_SHORTCUTS,
      base::FeatureList::IsEnabled(commands::features::kShunyaCommands));

  UpdateCommandEnabled(IDC_SHOW_SHUNYA_TALK, true);
  UpdateCommandEnabled(IDC_TOGGLE_SHIELDS, true);
  UpdateCommandEnabled(IDC_TOGGLE_JAVASCRIPT, true);
  UpdateCommandEnabled(IDC_GROUP_TABS_ON_CURRENT_ORIGIN, true);
  UpdateCommandEnabled(IDC_MOVE_GROUP_TO_NEW_WINDOW, true);
  UpdateCommandEnabled(IDC_CLOSE_DUPLICATE_TABS, true);
}

void ShunyaBrowserCommandController::UpdateCommandForShunyaRewards() {
  UpdateCommandEnabled(IDC_SHOW_SHUNYA_REWARDS, true);
}

void ShunyaBrowserCommandController::UpdateCommandForWebcompatReporter() {
  UpdateCommandEnabled(IDC_SHOW_SHUNYA_WEBCOMPAT_REPORTER, true);
}

#if BUILDFLAG(ENABLE_TOR)
void ShunyaBrowserCommandController::UpdateCommandForTor() {
  // Enable new tor connection only for tor profile.
  UpdateCommandEnabled(IDC_NEW_TOR_CONNECTION_FOR_SITE,
                       browser_->profile()->IsTor());
  UpdateCommandEnabled(IDC_NEW_OFFTHERECORD_WINDOW_TOR,
                       !shunya::IsTorDisabledForProfile(browser_->profile()));
}
#endif

void ShunyaBrowserCommandController::UpdateCommandForSidebar() {
  if (sidebar::CanUseSidebar(&*browser_)) {
    UpdateCommandEnabled(IDC_SIDEBAR_SHOW_OPTION_MENU, true);
    UpdateCommandEnabled(IDC_SIDEBAR_TOGGLE_POSITION, true);
    UpdateCommandEnabled(IDC_TOGGLE_SIDEBAR, true);
  }
}

void ShunyaBrowserCommandController::UpdateCommandForShunyaVPN() {
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  if (!shunya_vpn::IsShunyaVPNEnabled(browser_->profile())) {
    UpdateCommandEnabled(IDC_SHOW_SHUNYA_VPN_PANEL, false);
    UpdateCommandEnabled(IDC_SHUNYA_VPN_MENU, false);
    UpdateCommandEnabled(IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON, false);
    UpdateCommandEnabled(IDC_SEND_SHUNYA_VPN_FEEDBACK, false);
    UpdateCommandEnabled(IDC_ABOUT_SHUNYA_VPN, false);
    UpdateCommandEnabled(IDC_MANAGE_SHUNYA_VPN_PLAN, false);
    UpdateCommandEnabled(IDC_TOGGLE_SHUNYA_VPN, false);
#if BUILDFLAG(IS_WIN)
    UpdateCommandEnabled(IDC_TOGGLE_SHUNYA_VPN_TRAY_ICON, false);
#endif
    return;
  }
  UpdateCommandEnabled(IDC_SHOW_SHUNYA_VPN_PANEL, true);
  UpdateCommandEnabled(IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON, true);
#if BUILDFLAG(IS_WIN)
  UpdateCommandEnabled(IDC_TOGGLE_SHUNYA_VPN_TRAY_ICON, true);
#endif
  UpdateCommandEnabled(IDC_SEND_SHUNYA_VPN_FEEDBACK, true);
  UpdateCommandEnabled(IDC_ABOUT_SHUNYA_VPN, true);
  UpdateCommandEnabled(IDC_MANAGE_SHUNYA_VPN_PLAN, true);

  if (auto* vpn_service = shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(
          browser_->profile())) {
    // Only show vpn sub menu for purchased user.
    UpdateCommandEnabled(IDC_SHUNYA_VPN_MENU, vpn_service->is_purchased_user());
    UpdateCommandEnabled(IDC_TOGGLE_SHUNYA_VPN,
                         vpn_service->is_purchased_user());
  }
#endif
}

void ShunyaBrowserCommandController::UpdateCommandForPlaylist() {
#if BUILDFLAG(ENABLE_PLAYLIST_WEBUI)
  if (base::FeatureList::IsEnabled(playlist::features::kPlaylist)) {
    UpdateCommandEnabled(
        IDC_SHOW_PLAYLIST_BUBBLE,
        browser_->is_type_normal() && !browser_->profile()->IsOffTheRecord());
  }
#endif
}

void ShunyaBrowserCommandController::UpdateCommandForShunyaSync() {
  UpdateCommandEnabled(IDC_SHOW_SHUNYA_SYNC, true);
}

void ShunyaBrowserCommandController::UpdateCommandForShunyaWallet() {
  UpdateCommandEnabled(IDC_SHOW_SHUNYA_WALLET, true);
  UpdateCommandEnabled(IDC_SHOW_SHUNYA_WALLET_PANEL, true);
  UpdateCommandEnabled(IDC_CLOSE_SHUNYA_WALLET_PANEL, true);
}

bool ShunyaBrowserCommandController::ExecuteShunyaCommandWithDisposition(
    int id,
    WindowOpenDisposition disposition,
    base::TimeTicks time_stamp) {
  if (!SupportsCommand(id) || !IsCommandEnabled(id)) {
    return false;
  }

  if (browser_->tab_strip_model()->active_index() == TabStripModel::kNoTab) {
    return true;
  }

  DCHECK(IsCommandEnabled(id)) << "Invalid/disabled command " << id;

  switch (id) {
    case IDC_NEW_WINDOW:
      // Use chromium's action for non-Tor profiles.
      if (!browser_->profile()->IsTor()) {
        return BrowserCommandController::ExecuteCommandWithDisposition(
            id, disposition, time_stamp);
      }
      NewEmptyWindow(browser_->profile()->GetOriginalProfile());
      break;
    case IDC_NEW_INCOGNITO_WINDOW:
      // Use chromium's action for non-Tor profiles.
      if (!browser_->profile()->IsTor()) {
        return BrowserCommandController::ExecuteCommandWithDisposition(
            id, disposition, time_stamp);
      }
      NewIncognitoWindow(browser_->profile()->GetOriginalProfile());
      break;
    case IDC_SHOW_SHUNYA_REWARDS:
      shunya::ShowShunyaRewards(&*browser_);
      break;
    case IDC_SHOW_SHUNYA_WEBCOMPAT_REPORTER:
      shunya::ShowWebcompatReporter(&*browser_);
      break;
    case IDC_NEW_OFFTHERECORD_WINDOW_TOR:
      shunya::NewOffTheRecordWindowTor(&*browser_);
      break;
    case IDC_NEW_TOR_CONNECTION_FOR_SITE:
      shunya::NewTorConnectionForSite(&*browser_);
      break;
    case IDC_SHOW_SHUNYA_SYNC:
      shunya::ShowSync(&*browser_);
      break;
    case IDC_SHOW_SHUNYA_WALLET:
      shunya::ShowShunyaWallet(&*browser_);
      break;
    case IDC_SPEEDREADER_ICON_ONCLICK:
      shunya::MaybeDistillAndShowSpeedreaderBubble(&*browser_);
      break;
    case IDC_SHOW_SHUNYA_WALLET_PANEL:
      shunya::ShowWalletBubble(&*browser_);
      break;
    case IDC_CLOSE_SHUNYA_WALLET_PANEL:
      shunya::CloseWalletBubble(&*browser_);
      break;
    case IDC_SHOW_SHUNYA_VPN_PANEL:
      shunya::ShowShunyaVPNBubble(&*browser_);
      break;
    case IDC_TOGGLE_SHUNYA_VPN_TRAY_ICON:
      shunya::ToggleShunyaVPNTrayIcon();
      break;
    case IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON:
      shunya::ToggleShunyaVPNButton(&*browser_);
      break;
    case IDC_SEND_SHUNYA_VPN_FEEDBACK:
    case IDC_ABOUT_SHUNYA_VPN:
    case IDC_MANAGE_SHUNYA_VPN_PLAN:
      shunya::OpenShunyaVPNUrls(&*browser_, id);
      break;
    case IDC_SIDEBAR_TOGGLE_POSITION:
      shunya::ToggleSidebarPosition(&*browser_);
      break;
    case IDC_TOGGLE_SIDEBAR:
      shunya::ToggleSidebar(&*browser_);
      break;
    case IDC_COPY_CLEAN_LINK:
      shunya::CopySanitizedURL(
          &*browser_,
          browser_->tab_strip_model()->GetActiveWebContents()->GetVisibleURL());
      break;
    case IDC_APP_MENU_IPFS_OPEN_FILES:
      shunya::OpenIpfsFilesWebUI(&*browser_);
      break;
    case IDC_TOGGLE_TAB_MUTE:
      shunya::ToggleActiveTabAudioMute(&*browser_);
      break;
    case IDC_TOGGLE_VERTICAL_TABS:
      shunya::ToggleVerticalTabStrip(&*browser_);
      break;
    case IDC_TOGGLE_VERTICAL_TABS_WINDOW_TITLE:
      shunya::ToggleWindowTitleVisibilityForVerticalTabs(&*browser_);
      break;
    case IDC_TOGGLE_VERTICAL_TABS_EXPANDED:
      shunya::ToggleVerticalTabStripExpanded(&*browser_);
      break;
    case IDC_CONFIGURE_SHUNYA_NEWS:
      shunya::ShowShunyaNewsConfigure(&*browser_);
      break;
    case IDC_CONFIGURE_SHORTCUTS:
      shunya::ShowShortcutsPage(&*browser_);
      break;
    case IDC_SHOW_SHUNYA_TALK:
      shunya::ShowShunyaTalk(&*browser_);
      break;
    case IDC_TOGGLE_SHIELDS:
      shunya::ToggleShieldsEnabled(&*browser_);
      break;
    case IDC_TOGGLE_JAVASCRIPT:
      shunya::ToggleJavascriptEnabled(&*browser_);
      break;
    case IDC_SHOW_PLAYLIST_BUBBLE:
#if BUILDFLAG(ENABLE_PLAYLIST_WEBUI)
      shunya::ShowPlaylistBubble(&*browser_);
#else
      NOTREACHED() << " This command shouldn't be enabled";
#endif
      break;
    case IDC_GROUP_TABS_ON_CURRENT_ORIGIN:
      shunya::GroupTabsOnCurrentOrigin(&*browser_);
      break;
    case IDC_MOVE_GROUP_TO_NEW_WINDOW:
      shunya::MoveGroupToNewWindow(&*browser_);
      break;
    case IDC_CLOSE_DUPLICATE_TABS:
      shunya::CloseDuplicateTabs(&*browser_);
      break;
    default:
      LOG(WARNING) << "Received Unimplemented Command: " << id;
      break;
  }

  return true;
}

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
void ShunyaBrowserCommandController::OnPurchasedStateChanged(
    shunya_vpn::mojom::PurchasedState state,
    const absl::optional<std::string>& description) {
  UpdateCommandForShunyaVPN();
}
#endif

}  // namespace chrome

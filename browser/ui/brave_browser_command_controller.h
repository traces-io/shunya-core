/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_SHUNYA_BROWSER_COMMAND_CONTROLLER_H_
#define SHUNYA_BROWSER_UI_SHUNYA_BROWSER_COMMAND_CONTROLLER_H_

#include <string>

#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "chrome/browser/ui/browser_command_controller.h"
#include "components/prefs/pref_change_registrar.h"

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service_observer.h"
#endif

class ShunyaAppMenuBrowserTest;
class ShunyaAppMenuModelBrowserTest;
class ShunyaBrowserCommandControllerTest;

// This namespace is needed for a chromium_src override
namespace chrome {

class ShunyaBrowserCommandController : public chrome::BrowserCommandController
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
    ,
                                      public shunya_vpn::ShunyaVPNServiceObserver
#endif
{
 public:
  explicit ShunyaBrowserCommandController(Browser* browser);
  ShunyaBrowserCommandController(const ShunyaBrowserCommandController&) = delete;
  ShunyaBrowserCommandController& operator=(
      const ShunyaBrowserCommandController&) = delete;
  ~ShunyaBrowserCommandController() override;

#if BUILDFLAG(ENABLE_TOR)
  void UpdateCommandForTor();
#endif

 private:
  friend class ::ShunyaAppMenuBrowserTest;
  friend class ::ShunyaAppMenuModelBrowserTest;
  friend class ::ShunyaBrowserCommandControllerTest;

  // Overriden from CommandUpdater:
  bool SupportsCommand(int id) const override;
  bool IsCommandEnabled(int id) const override;
  bool ExecuteCommandWithDisposition(
      int id,
      WindowOpenDisposition disposition,
      base::TimeTicks time_stamp = base::TimeTicks::Now()) override;
  void AddCommandObserver(int id, CommandObserver* observer) override;
  void RemoveCommandObserver(int id, CommandObserver* observer) override;
  void RemoveCommandObserver(CommandObserver* observer) override;
  bool UpdateCommandEnabled(int id, bool state) override;

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  // shunya_vpn::ShunyaVPNServiceObserver overrides:
  void OnPurchasedStateChanged(
      shunya_vpn::mojom::PurchasedState state,
      const absl::optional<std::string>& description) override;
#endif

  void InitShunyaCommandState();
  void UpdateCommandForShunyaRewards();
  void UpdateCommandForWebcompatReporter();
  void UpdateCommandForShunyaSync();
  void UpdateCommandForShunyaWallet();
  void UpdateCommandForSidebar();
  void UpdateCommandForShunyaVPN();
  void UpdateCommandForPlaylist();
  void UpdateCommandsFroGroups();

  bool ExecuteShunyaCommandWithDisposition(int id,
                                          WindowOpenDisposition disposition,
                                          base::TimeTicks time_stamp);
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  PrefChangeRegistrar shunya_vpn_pref_change_registrar_;
#endif
  const raw_ref<Browser> browser_;

  CommandUpdaterImpl shunya_command_updater_;
};

}   // namespace chrome

#endif  // SHUNYA_BROWSER_UI_SHUNYA_BROWSER_COMMAND_CONTROLLER_H_

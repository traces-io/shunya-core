/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_SHUNYA_TAB_STRIP_MODEL_DELEGATE_H_
#define SHUNYA_BROWSER_UI_SHUNYA_TAB_STRIP_MODEL_DELEGATE_H_

#include <memory>
#include <vector>

#include "chrome/browser/ui/browser_tab_strip_model_delegate.h"

// In order to make it easy to replace BrowserTabStripModelDelegate with our
// ShunyaTabStripModelDelegate, wrap this class with chrome namespace.
namespace chrome {

class ShunyaTabStripModelDelegate : public BrowserTabStripModelDelegate {
 public:
  using BrowserTabStripModelDelegate::BrowserTabStripModelDelegate;
  ShunyaTabStripModelDelegate(const ShunyaTabStripModelDelegate&) = delete;
  ShunyaTabStripModelDelegate& operator=(const ShunyaTabStripModelDelegate&) =
      delete;
  ~ShunyaTabStripModelDelegate() override = default;

  // BrowserTabStripModelDelegate:
  bool CanMoveTabsToWindow(const std::vector<int>& indices) override;
  void CacheWebContents(
      const std::vector<std::unique_ptr<TabStripModel::DetachedWebContents>>&
          web_contents) override;
};

}  // namespace chrome

#endif  // SHUNYA_BROWSER_UI_SHUNYA_TAB_STRIP_MODEL_DELEGATE_H_

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_COMMANDER_SHUNYA_SIMPLE_COMMAND_SOURCE_H_
#define SHUNYA_BROWSER_UI_COMMANDER_SHUNYA_SIMPLE_COMMAND_SOURCE_H_

#include "chrome/browser/ui/commander/command_source.h"

namespace commander {

class ShunyaSimpleCommandSource : public CommandSource {
 public:
  ShunyaSimpleCommandSource();
  ~ShunyaSimpleCommandSource() override;

  ShunyaSimpleCommandSource(const ShunyaSimpleCommandSource& other) = delete;
  ShunyaSimpleCommandSource& operator=(const ShunyaSimpleCommandSource& other) =
      delete;

  // CommandSource:
  CommandSource::CommandResults GetCommands(const std::u16string& input,
                                            Browser* browser) const override;
};

}  // namespace commander

#endif  // SHUNYA_BROWSER_UI_COMMANDER_SHUNYA_SIMPLE_COMMAND_SOURCE_H_

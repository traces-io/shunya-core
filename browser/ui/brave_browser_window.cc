/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/shunya_browser_window.h"

#include <vector>

// Provide a base implementation (important for `TestBrowserWindow ` in tests)
// For real implementation, see `ShunyaBrowserView`.

speedreader::SpeedreaderBubbleView* ShunyaBrowserWindow::ShowSpeedreaderBubble(
    speedreader::SpeedreaderTabHelper* tab_helper,
    speedreader::SpeedreaderBubbleLocation location) {
  return nullptr;
}

gfx::Rect ShunyaBrowserWindow::GetShieldsBubbleRect() {
  return gfx::Rect();
}

// static
ShunyaBrowserWindow* ShunyaBrowserWindow::From(BrowserWindow* window) {
  return static_cast<ShunyaBrowserWindow*>(window);
}

#if defined(TOOLKIT_VIEWS)
sidebar::Sidebar* ShunyaBrowserWindow::InitSidebar() {
  return nullptr;
}

void ShunyaBrowserWindow::ToggleSidebar() {}

bool ShunyaBrowserWindow::HasSelectedURL() const {
  return false;
}
void ShunyaBrowserWindow::CleanAndCopySelectedURL() {}

#endif

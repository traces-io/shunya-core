/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_SHUNYA_PAGES_H_
#define SHUNYA_BROWSER_UI_SHUNYA_PAGES_H_

#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom-forward.h"

class Browser;

namespace shunya {

void ShowShunyaAdblock(Browser* browser);
void ShowWebcompatReporter(Browser* browser);
void ShowShunyaRewards(Browser* browser);
void ShowShunyaWallet(Browser* browser);
void ShowShunyaWalletOnboarding(Browser* browser);
void ShowShunyaWalletAccountCreation(Browser* browser,
                                    shunya_wallet::mojom::CoinType coin_type);
void ShowExtensionSettings(Browser* browser);
void ShowWalletSettings(Browser* browser);
void ShowSync(Browser* browser);
void ShowShunyaNewsConfigure(Browser* browser);
void ShowShortcutsPage(Browser* browser);
void ShowShunyaTalk(Browser* browser);

}  // namespace shunya

#endif  // SHUNYA_BROWSER_UI_SHUNYA_PAGES_H_

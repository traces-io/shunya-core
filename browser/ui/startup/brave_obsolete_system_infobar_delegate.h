/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_STARTUP_SHUNYA_OBSOLETE_SYSTEM_INFOBAR_DELEGATE_H_
#define SHUNYA_BROWSER_UI_STARTUP_SHUNYA_OBSOLETE_SYSTEM_INFOBAR_DELEGATE_H_

#include <string>
#include <vector>

#include "base/memory/weak_ptr.h"
#include "shunya/components/infobars/core/shunya_confirm_infobar_delegate.h"

namespace infobars {
class ContentInfoBarManager;
}  // namespace infobars

class ShunyaObsoleteSystemInfoBarDelegate : public ShunyaConfirmInfoBarDelegate {
 public:
  static void Create(infobars::ContentInfoBarManager* infobar_manager);

  ShunyaObsoleteSystemInfoBarDelegate(
      const ShunyaObsoleteSystemInfoBarDelegate&) = delete;
  ShunyaObsoleteSystemInfoBarDelegate& operator=(
      const ShunyaObsoleteSystemInfoBarDelegate&) = delete;

 private:
  ShunyaObsoleteSystemInfoBarDelegate();
  ~ShunyaObsoleteSystemInfoBarDelegate() override;

  // ShunyaConfirmInfoBarDelegate overrides:
  bool HasCheckbox() const override;
  std::u16string GetCheckboxText() const override;
  void SetCheckboxChecked(bool checked) override;
  bool InterceptClosing() override;
  infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
  std::u16string GetLinkText() const override;
  GURL GetLinkURL() const override;
  std::u16string GetMessageText() const override;
  int GetButtons() const override;
  std::vector<int> GetButtonsOrder() const override;
  bool ShouldExpire(const NavigationDetails& details) const override;

  void OnConfirmDialogClosing(bool suppress);

  bool launch_confirmation_dialog_ = false;

  base::WeakPtrFactory<ShunyaObsoleteSystemInfoBarDelegate> weak_factory_{this};
};

#endif  // SHUNYA_BROWSER_UI_STARTUP_SHUNYA_OBSOLETE_SYSTEM_INFOBAR_DELEGATE_H_

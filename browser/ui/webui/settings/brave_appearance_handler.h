/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_APPEARANCE_HANDLER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_APPEARANCE_HANDLER_H_

#include <string>

#include "base/memory/raw_ptr.h"
#include "chrome/browser/ui/webui/settings/settings_page_ui_handler.h"
#include "components/prefs/pref_change_registrar.h"

class Profile;

class ShunyaAppearanceHandler : public settings::SettingsPageUIHandler {
 public:
  ShunyaAppearanceHandler();
  ~ShunyaAppearanceHandler() override;

  ShunyaAppearanceHandler(const ShunyaAppearanceHandler&) = delete;
  ShunyaAppearanceHandler& operator=(const ShunyaAppearanceHandler&) = delete;

 private:
  // SettingsPageUIHandler overrides:
  void RegisterMessages() override;
  void OnJavascriptAllowed() override {}
  void OnJavascriptDisallowed() override {}

  void OnShunyaDarkModeChanged();
  void OnBackgroundPreferenceChanged(const std::string& pref_name);
  void OnPreferenceChanged(const std::string& pref_name);
  void SetShunyaThemeType(const base::Value::List& args);
  void GetShunyaThemeType(const base::Value::List& args);
  void GetNewTabShowsOptionsList(const base::Value::List& args);
  void ShouldShowNewTabDashboardSettings(const base::Value::List& args);

  raw_ptr<Profile> profile_ = nullptr;
  PrefChangeRegistrar local_state_change_registrar_;
  PrefChangeRegistrar profile_state_change_registrar_;
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_APPEARANCE_HANDLER_H_

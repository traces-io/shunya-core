/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/settings/shunya_settings_secure_dns_handler.h"

#include "base/feature_list.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "chrome/browser/browser_process.h"
#include "chrome/common/pref_names.h"
#include "components/prefs/pref_service.h"

namespace settings {

ShunyaSecureDnsHandler::ShunyaSecureDnsHandler() = default;
ShunyaSecureDnsHandler::~ShunyaSecureDnsHandler() = default;

void ShunyaSecureDnsHandler::OnJavascriptAllowed() {
  SecureDnsHandler::OnJavascriptAllowed();
  pref_registrar_.Init(g_browser_process->local_state());
  if (base::FeatureList::IsEnabled(
          shunya_vpn::features::kShunyaVPNDnsProtection)) {
    pref_registrar_.Add(
        prefs::kShunyaVpnDnsConfig,
        base::BindRepeating(
            &ShunyaSecureDnsHandler::SendSecureDnsSettingUpdatesToJavascript,
            base::Unretained(this)));
  }
}

void ShunyaSecureDnsHandler::OnJavascriptDisallowed() {
  SecureDnsHandler::OnJavascriptDisallowed();
  pref_registrar_.RemoveAll();
}

}  // namespace settings

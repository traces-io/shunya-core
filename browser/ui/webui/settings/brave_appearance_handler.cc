/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/settings/shunya_appearance_handler.h"

#include "base/functional/bind.h"
#include "base/metrics/histogram_macros.h"
#include "base/strings/string_number_conversions.h"
#include "shunya/browser/new_tab/new_tab_shows_options.h"
#include "shunya/browser/profiles/profile_util.h"
#include "shunya/browser/themes/shunya_dark_mode_utils.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/ntp_background_images/common/pref_names.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/search/instant_service.h"
#include "chrome/browser/search/instant_service_factory.h"
#include "chrome/common/pref_names.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/web_ui.h"

ShunyaAppearanceHandler::ShunyaAppearanceHandler() {
  local_state_change_registrar_.Init(g_browser_process->local_state());
  local_state_change_registrar_.Add(
      kShunyaDarkMode,
      base::BindRepeating(&ShunyaAppearanceHandler::OnShunyaDarkModeChanged,
                          base::Unretained(this)));
}

ShunyaAppearanceHandler::~ShunyaAppearanceHandler() = default;

// TODO(simonhong): Use separate handler for NTP settings.
void ShunyaAppearanceHandler::RegisterMessages() {
  profile_ = Profile::FromWebUI(web_ui());
  profile_state_change_registrar_.Init(profile_->GetPrefs());
  profile_state_change_registrar_.Add(
      kNewTabPageShowsOptions,
      base::BindRepeating(&ShunyaAppearanceHandler::OnPreferenceChanged,
                          base::Unretained(this)));
  profile_state_change_registrar_.Add(
      prefs::kHomePageIsNewTabPage,
      base::BindRepeating(&ShunyaAppearanceHandler::OnPreferenceChanged,
                          base::Unretained(this)));
  profile_state_change_registrar_.Add(
      prefs::kHomePage,
      base::BindRepeating(&ShunyaAppearanceHandler::OnPreferenceChanged,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "setShunyaThemeType",
      base::BindRepeating(&ShunyaAppearanceHandler::SetShunyaThemeType,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "getShunyaThemeType",
      base::BindRepeating(&ShunyaAppearanceHandler::GetShunyaThemeType,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "getNewTabShowsOptionsList",
      base::BindRepeating(&ShunyaAppearanceHandler::GetNewTabShowsOptionsList,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "shouldShowNewTabDashboardSettings",
      base::BindRepeating(
          &ShunyaAppearanceHandler::ShouldShowNewTabDashboardSettings,
          base::Unretained(this)));
}

void ShunyaAppearanceHandler::SetShunyaThemeType(const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  CHECK(args[0].is_int());
  AllowJavascript();

  int int_type = args[0].GetInt();
  dark_mode::SetShunyaDarkModeType(
      static_cast<dark_mode::ShunyaDarkModeType>(int_type));
}

void ShunyaAppearanceHandler::GetShunyaThemeType(const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();
  // GetShunyaThemeType() should be used because settings option displays all
  // available options including default.
  ResolveJavascriptCallback(
      args[0],
      base::Value(static_cast<int>(dark_mode::GetShunyaDarkModeType())));
}

void ShunyaAppearanceHandler::OnShunyaDarkModeChanged() {
  // GetShunyaThemeType() should be used because settings option displays all
  // available options including default.
  if (IsJavascriptAllowed()) {
    FireWebUIListener(
        "shunya-theme-type-changed",
        base::Value(static_cast<int>(dark_mode::GetShunyaDarkModeType())));
  }
}

void ShunyaAppearanceHandler::OnBackgroundPreferenceChanged(
    const std::string& pref_name) {
  shunya::RecordSponsoredImagesEnabledP3A(profile_);
}

void ShunyaAppearanceHandler::OnPreferenceChanged(const std::string& pref_name) {
  if (IsJavascriptAllowed()) {
    if (pref_name == kNewTabPageShowsOptions || pref_name == prefs::kHomePage ||
        pref_name == prefs::kHomePageIsNewTabPage) {
      FireWebUIListener(
          "show-new-tab-dashboard-settings-changed",
          base::Value(shunya::ShouldNewTabShowDashboard(profile_)));
      return;
    }
  }
}

void ShunyaAppearanceHandler::GetNewTabShowsOptionsList(
    const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();
  ResolveJavascriptCallback(
      args[0], base::Value(shunya::GetNewTabShowsOptionsList(profile_)));
}

void ShunyaAppearanceHandler::ShouldShowNewTabDashboardSettings(
    const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();
  ResolveJavascriptCallback(
      args[0], base::Value(shunya::ShouldNewTabShowDashboard(profile_)));
}

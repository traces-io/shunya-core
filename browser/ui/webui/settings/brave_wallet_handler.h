/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_WALLET_HANDLER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_WALLET_HANDLER_H_

#include <string>
#include <utility>

#include "base/memory/weak_ptr.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_pin_service_factory.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_pin_service.h"
#include "shunya/components/shunya_wallet/browser/json_rpc_service.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "chrome/browser/ui/webui/settings/settings_page_ui_handler.h"

class PrefService;
class Profile;
class TestShunyaWalletHandler;

class ShunyaWalletHandler : public settings::SettingsPageUIHandler {
 public:
  ShunyaWalletHandler();
  ~ShunyaWalletHandler() override;
  ShunyaWalletHandler(const ShunyaWalletHandler&) = delete;
  ShunyaWalletHandler& operator=(const ShunyaWalletHandler&) = delete;

  void SetChainCallbackForTesting(base::OnceClosure callback) {
    chain_callback_for_testing_ = std::move(callback);
  }

 private:
  friend TestShunyaWalletHandler;
  // SettingsPageUIHandler overrides:
  void RegisterMessages() override;
  void OnJavascriptAllowed() override {}
  void OnJavascriptDisallowed() override {}

  void GetAutoLockMinutes(const base::Value::List& args);
  void GetSolanaProviderOptions(const base::Value::List& args);
  void RemoveChain(const base::Value::List& args);
  void ResetChain(const base::Value::List& args);
  void GetNetworksList(const base::Value::List& args);
  void GetPrepopulatedNetworksList(const base::Value::List& args);
  void AddChain(const base::Value::List& args);
  void SetDefaultNetwork(const base::Value::List& args);
  void AddHiddenNetwork(const base::Value::List& args);
  void RemoveHiddenNetwork(const base::Value::List& args);
  void IsNftPinningEnabled(const base::Value::List& args);
  void GetPinnedNftCount(const base::Value::List& args);
  void ClearPinnedNft(const base::Value::List& args);

  PrefService* GetPrefs();
  shunya_wallet::ShunyaWalletPinService* GetShunyaWalletPinService();

  void OnAddChain(base::Value javascript_callback,
                  const std::string& chain_id,
                  shunya_wallet::mojom::ProviderError error,
                  const std::string& error_message);
  void OnShunyaWalletPinServiceReset(base::Value javascript_callback,
                                    bool result);

  base::OnceClosure chain_callback_for_testing_;
  base::WeakPtrFactory<ShunyaWalletHandler> weak_ptr_factory_{this};
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_WALLET_HANDLER_H_

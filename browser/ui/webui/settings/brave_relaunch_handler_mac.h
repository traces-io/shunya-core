/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_RELAUNCH_HANDLER_MAC_H_
#define SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_RELAUNCH_HANDLER_MAC_H_

namespace shunya_relaunch_handler {

void RelaunchOnMac();

}  // namespace shunya_relaunch_handler

#endif  // SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_RELAUNCH_HANDLER_MAC_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/settings/shunya_wallet_handler.h"

#include <string>
#include <utility>
#include <vector>

#include "base/feature_list.h"
#include "base/functional/bind.h"
#include "base/notreached.h"
#include "base/values.h"
#include "shunya/browser/shunya_wallet/json_rpc_service_factory.h"
#include "shunya/components/shunya_wallet/browser/blockchain_registry.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_utils.h"
#include "shunya/components/shunya_wallet/browser/json_rpc_service.h"
#include "shunya/components/shunya_wallet/browser/pref_names.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "shunya/components/shunya_wallet/common/common_utils.h"
#include "shunya/components/shunya_wallet/common/value_conversion_utils.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "shunya/grit/shunya_generated_resources.h"
#include "chrome/browser/profiles/profile.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/web_ui.h"
#include "ui/base/l10n/l10n_util.h"

ShunyaWalletHandler::ShunyaWalletHandler() = default;
ShunyaWalletHandler::~ShunyaWalletHandler() = default;

namespace {

base::Value::Dict MakeSelectValue(const std::u16string& name,
                                  ::shunya_wallet::mojom::DefaultWallet value) {
  base::Value::Dict item;
  item.Set("value", static_cast<int>(value));
  item.Set("name", name);
  return item;
}

absl::optional<shunya_wallet::mojom::CoinType> ToCoinType(
    absl::optional<int> val) {
  if (!val) {
    return absl::nullopt;
  }
  auto result = static_cast<shunya_wallet::mojom::CoinType>(*val);
  if (result != shunya_wallet::mojom::CoinType::ETH &&
      result != shunya_wallet::mojom::CoinType::FIL &&
      result != shunya_wallet::mojom::CoinType::SOL) {
    NOTREACHED();
    return absl::nullopt;
  }
  return result;
}

}  // namespace

void ShunyaWalletHandler::RegisterMessages() {
  web_ui()->RegisterMessageCallback(
      "getAutoLockMinutes",
      base::BindRepeating(&ShunyaWalletHandler::GetAutoLockMinutes,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "getSolanaProviderOptions",
      base::BindRepeating(&ShunyaWalletHandler::GetSolanaProviderOptions,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "removeChain", base::BindRepeating(&ShunyaWalletHandler::RemoveChain,
                                         base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "resetChain", base::BindRepeating(&ShunyaWalletHandler::ResetChain,
                                        base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "getNetworksList",
      base::BindRepeating(&ShunyaWalletHandler::GetNetworksList,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "getPrepopulatedNetworksList",
      base::BindRepeating(&ShunyaWalletHandler::GetPrepopulatedNetworksList,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "addChain", base::BindRepeating(&ShunyaWalletHandler::AddChain,
                                      base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "setDefaultNetwork",
      base::BindRepeating(&ShunyaWalletHandler::SetDefaultNetwork,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "addHiddenNetwork",
      base::BindRepeating(&ShunyaWalletHandler::AddHiddenNetwork,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "removeHiddenNetwork",
      base::BindRepeating(&ShunyaWalletHandler::RemoveHiddenNetwork,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "isNftPinningEnabled",
      base::BindRepeating(&ShunyaWalletHandler::IsNftPinningEnabled,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "getPinnedNftCount",
      base::BindRepeating(&ShunyaWalletHandler::GetPinnedNftCount,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "clearPinnedNft", base::BindRepeating(&ShunyaWalletHandler::ClearPinnedNft,
                                            base::Unretained(this)));
}

void ShunyaWalletHandler::GetAutoLockMinutes(const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();
  ResolveJavascriptCallback(
      args[0],
      base::Value(GetPrefs()->GetInteger(kShunyaWalletAutoLockMinutes)));
}

void ShunyaWalletHandler::GetSolanaProviderOptions(
    const base::Value::List& args) {
  base::Value::List list;
  list.Append(MakeSelectValue(
      shunya_l10n::GetLocalizedResourceUTF16String(
          IDS_SHUNYA_WALLET_WEB3_PROVIDER_SHUNYA_PREFER_EXTENSIONS),
      ::shunya_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension));
  list.Append(
      MakeSelectValue(shunya_l10n::GetLocalizedResourceUTF16String(
                          IDS_SHUNYA_WALLET_WEB3_PROVIDER_SHUNYA),
                      ::shunya_wallet::mojom::DefaultWallet::ShunyaWallet));
  list.Append(MakeSelectValue(shunya_l10n::GetLocalizedResourceUTF16String(
                                  IDS_SHUNYA_WALLET_WEB3_PROVIDER_NONE),
                              ::shunya_wallet::mojom::DefaultWallet::None));
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();
  ResolveJavascriptCallback(args[0], list);
}

void ShunyaWalletHandler::RemoveChain(const base::Value::List& args) {
  CHECK_EQ(args.size(), 3U);
  AllowJavascript();

  auto* chain_id = args[1].GetIfString();
  auto coin = ToCoinType(args[2].GetIfInt());
  if (!chain_id || !coin) {
    ResolveJavascriptCallback(args[0], base::Value());
    return;
  }

  shunya_wallet::RemoveCustomNetwork(GetPrefs(), *chain_id, *coin);
  ResolveJavascriptCallback(args[0], base::Value(true));
}

void ShunyaWalletHandler::ResetChain(const base::Value::List& args) {
  CHECK_EQ(args.size(), 3U);
  PrefService* prefs = GetPrefs();
  AllowJavascript();

  auto* chain_id = args[1].GetIfString();
  auto coin = ToCoinType(args[2].GetIfInt());
  if (!chain_id || !coin) {
    ResolveJavascriptCallback(args[0], base::Value());
    return;
  }

  DCHECK(shunya_wallet::CustomChainExists(prefs, *chain_id, *coin));
  shunya_wallet::RemoveCustomNetwork(prefs, *chain_id, *coin);
  DCHECK(shunya_wallet::KnownChainExists(*chain_id, *coin));
  ResolveJavascriptCallback(args[0], base::Value(true));
}

void ShunyaWalletHandler::GetNetworksList(const base::Value::List& args) {
  CHECK_EQ(args.size(), 2U);
  PrefService* prefs = GetPrefs();

  base::Value::Dict result;
  auto coin = ToCoinType(args[1].GetIfInt());
  if (!coin) {
    ResolveJavascriptCallback(args[0], base::Value());
    return;
  }

  result.Set("defaultNetwork",
             shunya_wallet::GetCurrentChainId(prefs, *coin, absl::nullopt));

  auto& networks = result.Set("networks", base::Value::List())->GetList();
  for (const auto& it : shunya_wallet::GetAllChains(prefs, *coin)) {
    networks.Append(shunya_wallet::NetworkInfoToValue(*it));
  }
  auto& knownNetworks =
      result.Set("knownNetworks", base::Value::List())->GetList();
  for (const auto& it : shunya_wallet::GetAllKnownChains(prefs, *coin)) {
    knownNetworks.Append(it->chain_id);
  }

  auto& customNetworks =
      result.Set("customNetworks", base::Value::List())->GetList();
  for (const auto& it : shunya_wallet::GetAllCustomChains(prefs, *coin)) {
    customNetworks.Append(it->chain_id);
  }

  auto& hiddenNetworks =
      result.Set("hiddenNetworks", base::Value::List())->GetList();
  for (const auto& it : shunya_wallet::GetHiddenNetworks(prefs, *coin)) {
    hiddenNetworks.Append(it);
  }

  AllowJavascript();
  ResolveJavascriptCallback(args[0], result);
}

void ShunyaWalletHandler::GetPrepopulatedNetworksList(
    const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();

  base::Value::List networks;

  auto* blockchain_registry = shunya_wallet::BlockchainRegistry::GetInstance();
  if (!blockchain_registry) {
    ResolveJavascriptCallback(args[0], networks);
    return;
  }

  for (const auto& it : blockchain_registry->GetPrepopulatedNetworks()) {
    networks.Append(shunya_wallet::NetworkInfoToValue(*it));
  }

  ResolveJavascriptCallback(args[0], networks);
}

void ShunyaWalletHandler::OnAddChain(base::Value javascript_callback,
                                    const std::string& chain_id,
                                    shunya_wallet::mojom::ProviderError error,
                                    const std::string& error_message) {
  base::Value::List result;
  result.Append(error == shunya_wallet::mojom::ProviderError::kSuccess);
  result.Append(error_message);
  ResolveJavascriptCallback(javascript_callback, result);
  if (chain_callback_for_testing_) {
    std::move(chain_callback_for_testing_).Run();
  }
}

void ShunyaWalletHandler::AddChain(const base::Value::List& args) {
  CHECK_EQ(args.size(), 2U);
  AllowJavascript();
  auto* json_rpc_service =
      shunya_wallet::JsonRpcServiceFactory::GetServiceForContext(
          Profile::FromWebUI(web_ui()));

  shunya_wallet::mojom::NetworkInfoPtr chain =
      shunya_wallet::ValueToNetworkInfo(args[1]);

  if (!chain || !json_rpc_service) {
    base::Value::List result;
    result.Append(false);
    result.Append(l10n_util::GetStringUTF8(
        IDS_SETTINGS_WALLET_NETWORKS_SUMBISSION_FAILED));
    ResolveJavascriptCallback(args[0], result);
    return;
  }

  json_rpc_service->AddChain(
      std::move(chain),
      base::BindOnce(&ShunyaWalletHandler::OnAddChain,
                     weak_ptr_factory_.GetWeakPtr(), args[0].Clone()));
}

void ShunyaWalletHandler::SetDefaultNetwork(const base::Value::List& args) {
  CHECK_EQ(args.size(), 3U);

  auto* chain_id = args[1].GetIfString();
  auto coin = ToCoinType(args[2].GetIfInt());
  if (!chain_id || !coin) {
    ResolveJavascriptCallback(args[0], base::Value());
    return;
  }

  AllowJavascript();
  auto* json_rpc_service =
      shunya_wallet::JsonRpcServiceFactory::GetServiceForContext(
          Profile::FromWebUI(web_ui()));
  auto result = json_rpc_service ? json_rpc_service->SetNetwork(
                                       *chain_id, *coin, absl::nullopt)
                                 : false;
  ResolveJavascriptCallback(args[0], base::Value(result));
}

void ShunyaWalletHandler::AddHiddenNetwork(const base::Value::List& args) {
  CHECK_EQ(args.size(), 3U);
  auto* chain_id = args[1].GetIfString();
  auto coin = ToCoinType(args[2].GetIfInt());
  if (!chain_id || !coin) {
    ResolveJavascriptCallback(args[0], base::Value());
    return;
  }

  PrefService* prefs = GetPrefs();
  AllowJavascript();
  shunya_wallet::AddHiddenNetwork(prefs, *coin, *chain_id);
  ResolveJavascriptCallback(args[0], base::Value(true));
}

void ShunyaWalletHandler::RemoveHiddenNetwork(const base::Value::List& args) {
  CHECK_EQ(args.size(), 3U);
  auto* chain_id = args[1].GetIfString();
  auto coin = ToCoinType(args[2].GetIfInt());
  if (!chain_id || !coin) {
    ResolveJavascriptCallback(args[0], base::Value());
    return;
  }

  PrefService* prefs = GetPrefs();
  AllowJavascript();
  shunya_wallet::RemoveHiddenNetwork(prefs, *coin, *chain_id);
  ResolveJavascriptCallback(args[0], base::Value(true));
}

PrefService* ShunyaWalletHandler::GetPrefs() {
  return Profile::FromWebUI(web_ui())->GetPrefs();
}

shunya_wallet::ShunyaWalletPinService*
ShunyaWalletHandler::GetShunyaWalletPinService() {
  return shunya_wallet::ShunyaWalletPinServiceFactory::GetInstance()
      ->GetServiceForContext(Profile::FromWebUI(web_ui()));
}

void ShunyaWalletHandler::IsNftPinningEnabled(const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();
  ResolveJavascriptCallback(args[0],
                            base::Value(::shunya_wallet::IsNftPinningEnabled()));
}

void ShunyaWalletHandler::GetPinnedNftCount(const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();

  auto* service = GetShunyaWalletPinService();
  if (!service) {
    ResolveJavascriptCallback(args[0], base::Value());
    return;
  }
  ResolveJavascriptCallback(
      args[0], base::Value(static_cast<int>(service->GetPinnedTokensCount())));
}

void ShunyaWalletHandler::ClearPinnedNft(const base::Value::List& args) {
  CHECK_EQ(args.size(), 1U);
  AllowJavascript();
  auto* service = GetShunyaWalletPinService();
  service->Reset(
      base::BindOnce(&ShunyaWalletHandler::OnShunyaWalletPinServiceReset,
                     weak_ptr_factory_.GetWeakPtr(), args[0].Clone()));
}

void ShunyaWalletHandler::OnShunyaWalletPinServiceReset(
    base::Value javascript_callback,
    bool result) {
  ResolveJavascriptCallback(javascript_callback, base::Value(result));
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/settings/shunya_site_settings_handler.h"

#include <string>

#include "base/values.h"
#include "shunya/components/shunya_shields/browser/shunya_shields_util.h"
#include "chrome/browser/ui/webui/settings/site_settings_helper.h"
#include "components/grit/shunya_components_strings.h"
#include "ui/base/l10n/l10n_util.h"

namespace settings {

namespace {
constexpr char kIsValidKey[] = "isValid";
constexpr char kReasonKey[] = "reason";
}  // namespace

ShunyaSiteSettingsHandler::ShunyaSiteSettingsHandler(Profile* profile)
    : SiteSettingsHandler(profile) {}

ShunyaSiteSettingsHandler::~ShunyaSiteSettingsHandler() = default;

void ShunyaSiteSettingsHandler::RegisterMessages() {
  web_ui()->RegisterMessageCallback(
      "isPatternValidForType",
      base::BindRepeating(
          &ShunyaSiteSettingsHandler::HandleIsPatternValidForType,
          base::Unretained(this)));

  SiteSettingsHandler::RegisterMessages();
}

bool ShunyaSiteSettingsHandler::IsPatternValidForShunyaContentType(
    ContentSettingsType content_type,
    const std::string& pattern_string) {
  if (content_type != ContentSettingsType::SHUNYA_SHIELDS)
    return true;
  return (pattern_string.find('*') == std::string::npos);
}

void ShunyaSiteSettingsHandler::HandleIsPatternValidForType(
    const base::Value::List& args) {
  CHECK_EQ(3U, args.size());
  const base::Value& callback_id = args[0];
  const std::string& pattern_string = args[1].GetString();
  const std::string& type = args[2].GetString();

  ContentSettingsType content_type =
      site_settings::ContentSettingsTypeFromGroupName(type);

  if (!IsPatternValidForShunyaContentType(content_type, pattern_string)) {
    base::Value::Dict return_value;
    return_value.Set(kIsValidKey, base::Value(false));
    return_value.Set(kReasonKey, base::Value(l10n_util::GetStringUTF8(
                                     IDS_SHUNYA_SHIELDS_NOT_VALID_ADDRESS)));
    AllowJavascript();
    ResolveJavascriptCallback(callback_id, return_value);
    return;
  }

  SiteSettingsHandler::HandleIsPatternValidForType(args);
}

}  // namespace settings

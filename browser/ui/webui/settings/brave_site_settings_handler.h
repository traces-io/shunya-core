/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_SITE_SETTINGS_HANDLER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_SITE_SETTINGS_HANDLER_H_

#include <string>

#include "chrome/browser/ui/webui/settings/site_settings_handler.h"

namespace settings {

class ShunyaSiteSettingsHandler : public SiteSettingsHandler {
 public:
  explicit ShunyaSiteSettingsHandler(Profile* profile);

  ShunyaSiteSettingsHandler(const ShunyaSiteSettingsHandler&) = delete;
  ShunyaSiteSettingsHandler& operator=(const ShunyaSiteSettingsHandler&) = delete;

  ~ShunyaSiteSettingsHandler() override;

  // SettingsPageUIHandler:
  void RegisterMessages() override;

  // Returns whether the pattern is valid given the type.
  void HandleIsPatternValidForType(const base::Value::List& args);

  bool IsPatternValidForShunyaContentType(ContentSettingsType content_type,
                                         const std::string& pattern_string);

 private:
  friend class TestShunyaSiteSettingsHandlerUnittest;
};

}  // namespace settings

#endif  // SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_SITE_SETTINGS_HANDLER_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/settings/shunya_vpn/shunya_vpn_handler.h"

#include <memory>

#include "base/command_line.h"
#include "base/files/file_path.h"
#include "base/path_service.h"
#include "base/process/launch.h"
#include "base/task/thread_pool.h"
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/service_constants.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/service_details.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/storage_utils.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/wireguard_utils_win.h"
#include "chrome/browser/browser_process.h"
#include "components/prefs/pref_service.h"
#include "components/version_info/version_info.h"

namespace {

bool ElevatedRegisterShunyaVPNService() {
  auto executable_path = shunya_vpn::GetShunyaVPNWireguardServiceExecutablePath();
  base::CommandLine cmd(executable_path);
  cmd.AppendSwitch(shunya_vpn::kShunyaVpnWireguardServiceInstallSwitchName);
  base::LaunchOptions options = base::LaunchOptions();
  options.wait = true;
  options.elevated = true;
  return base::LaunchProcess(cmd, options).IsValid();
}

}  // namespace

ShunyaVpnHandler::ShunyaVpnHandler(Profile* profile) : profile_(profile) {
  auto* service = shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(profile);
  CHECK(service);
  Observe(service);

  pref_change_registrar_.Init(g_browser_process->local_state());
  pref_change_registrar_.Add(
      shunya_vpn::prefs::kShunyaVPNWireguardEnabled,
      base::BindRepeating(&ShunyaVpnHandler::OnProtocolChanged,
                          base::Unretained(this)));
}

ShunyaVpnHandler::~ShunyaVpnHandler() = default;

void ShunyaVpnHandler::RegisterMessages() {
  web_ui()->RegisterMessageCallback(
      "registerWireguardService",
      base::BindRepeating(&ShunyaVpnHandler::HandleRegisterWireguardService,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "isWireguardServiceRegistered",
      base::BindRepeating(&ShunyaVpnHandler::HandleIsWireguardServiceRegistered,
                          base::Unretained(this)));
  web_ui()->RegisterMessageCallback(
      "isShunyaVpnConnected",
      base::BindRepeating(&ShunyaVpnHandler::HandleIsShunyaVpnConnected,
                          base::Unretained(this)));
}

void ShunyaVpnHandler::OnProtocolChanged() {
  auto enabled =
      shunya_vpn::IsShunyaVPNWireguardEnabled(g_browser_process->local_state());
  shunya_vpn::SetWireguardActive(enabled);
}

void ShunyaVpnHandler::HandleRegisterWireguardService(
    const base::Value::List& args) {
  base::ThreadPool::PostTaskAndReplyWithResult(
      FROM_HERE,
      {base::MayBlock(), base::TaskPriority::BEST_EFFORT,
       base::TaskShutdownBehavior::CONTINUE_ON_SHUTDOWN},
      base::BindOnce(&ElevatedRegisterShunyaVPNService),
      base::BindOnce(&ShunyaVpnHandler::OnWireguardServiceRegistered,
                     weak_factory_.GetWeakPtr(), args[0].GetString()));
}

void ShunyaVpnHandler::OnWireguardServiceRegistered(
    const std::string& callback_id,
    bool success) {
  AllowJavascript();
  ResolveJavascriptCallback(callback_id, base::Value(success));
}

void ShunyaVpnHandler::HandleIsWireguardServiceRegistered(
    const base::Value::List& args) {
  AllowJavascript();

  ResolveJavascriptCallback(
      args[0],
      base::Value(shunya_vpn::wireguard::IsWireguardServiceRegistered()));
}

void ShunyaVpnHandler::HandleIsShunyaVpnConnected(const base::Value::List& args) {
  AllowJavascript();

  auto* service = shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(profile_);
  ResolveJavascriptCallback(args[0],
                            base::Value(service && service->IsConnected()));
}

void ShunyaVpnHandler::OnConnectionStateChanged(
    shunya_vpn::mojom::ConnectionState state) {
  AllowJavascript();
  FireWebUIListener(
      "shunya-vpn-state-change",
      base::Value(state == shunya_vpn::mojom::ConnectionState::CONNECTED));
}

void ShunyaVpnHandler::OnJavascriptAllowed() {}

void ShunyaVpnHandler::OnJavascriptDisallowed() {}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_VPN_SHUNYA_VPN_HANDLER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_VPN_SHUNYA_VPN_HANDLER_H_

#include <string>

#include "base/memory/raw_ptr.h"
#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service_observer.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/webui/settings/settings_page_ui_handler.h"
#include "components/prefs/pref_change_registrar.h"

class ShunyaVpnHandler : public settings::SettingsPageUIHandler,
                        public shunya_vpn::ShunyaVPNServiceObserver {
 public:
  explicit ShunyaVpnHandler(Profile* profile);
  ~ShunyaVpnHandler() override;

 private:
  // WebUIMessageHandler implementation.
  void RegisterMessages() override;

  void HandleRegisterWireguardService(const base::Value::List& args);
  void HandleIsWireguardServiceRegistered(const base::Value::List& args);
  void OnWireguardServiceRegistered(const std::string& callback_id,
                                    bool success);
  void HandleIsShunyaVpnConnected(const base::Value::List& args);

  // shunya_vpn::ShunyaVPNServiceObserver
  void OnConnectionStateChanged(
      shunya_vpn::mojom::ConnectionState state) override;
  void OnProtocolChanged();

  // SettingsPageUIHandler implementation.
  void OnJavascriptAllowed() override;
  void OnJavascriptDisallowed() override;

  PrefChangeRegistrar pref_change_registrar_;
  const raw_ptr<Profile, DanglingUntriaged> profile_;
  base::WeakPtrFactory<ShunyaVpnHandler> weak_factory_{this};
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SETTINGS_SHUNYA_VPN_SHUNYA_VPN_HANDLER_H_

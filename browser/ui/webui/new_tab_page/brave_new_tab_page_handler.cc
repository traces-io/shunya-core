// Copyright (c) 2019 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/webui/new_tab_page/shunya_new_tab_page_handler.h"

#include <utility>

#include "base/containers/span.h"
#include "base/files/file_path.h"
#include "base/files/file_util.h"
#include "base/functional/bind.h"
#include "base/task/thread_pool.h"
#include "base/values.h"
#include "shunya/browser/shunya_browser_process.h"
#include "shunya/browser/ntp_background/constants.h"
#include "shunya/browser/ntp_background/custom_background_file_manager.h"
#include "shunya/browser/ntp_background/ntp_background_prefs.h"
#include "shunya/browser/ntp_background/view_counter_service_factory.h"
#include "shunya/components/shunya_search_conversion/p3a.h"
#include "shunya/components/shunya_search_conversion/pref_names.h"
#include "shunya/components/shunya_search_conversion/types.h"
#include "shunya/components/shunya_search_conversion/utils.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "shunya/components/ntp_background_images/browser/ntp_background_images_data.h"
#include "shunya/components/ntp_background_images/browser/ntp_background_images_service.h"
#include "shunya/components/ntp_background_images/browser/url_constants.h"
#include "shunya/components/ntp_background_images/browser/view_counter_service.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/search_engines/template_url_service_factory.h"
#include "chrome/browser/ui/chrome_select_file_policy.h"
#include "chrome/common/pref_names.h"
#include "chrome/grit/generated_resources.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/browser_task_traits.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/web_contents.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/pending_remote.h"
#include "url/gurl.h"

namespace {

bool IsNTPPromotionEnabled(Profile* profile) {
  if (!shunya_search_conversion::IsNTPPromotionEnabled(
          profile->GetPrefs(),
          TemplateURLServiceFactory::GetForProfile(profile))) {
    return false;
  }

  auto* service =
      ntp_background_images::ViewCounterServiceFactory::GetForProfile(profile);
  if (!service)
    return false;

  // Only show promotion if current wallpaper is not sponsored images.
  absl::optional<base::Value::Dict> data =
      service->GetCurrentWallpaperForDisplay();
  if (data) {
    if (const auto is_background =
            data->FindBool(ntp_background_images::kIsBackgroundKey)) {
      return is_background.value();
    }
  }
  return false;
}

}  // namespace

ShunyaNewTabPageHandler::ShunyaNewTabPageHandler(
    mojo::PendingReceiver<shunya_new_tab_page::mojom::PageHandler>
        pending_page_handler,
    mojo::PendingRemote<shunya_new_tab_page::mojom::Page> pending_page,
    Profile* profile,
    content::WebContents* web_contents)
    : page_handler_(this, std::move(pending_page_handler)),
      page_(std::move(pending_page)),
      profile_(profile),
      web_contents_(web_contents),
      file_manager_(std::make_unique<CustomBackgroundFileManager>(profile_)),
      weak_factory_(this) {
  InitForSearchPromotion();
}

ShunyaNewTabPageHandler::~ShunyaNewTabPageHandler() = default;

void ShunyaNewTabPageHandler::InitForSearchPromotion() {
  // If promotion is disabled for this loading, we do nothing.
  // If some condition is changed and it can be enabled, promotion
  // will be shown at the next NTP loading.
  if (!IsNTPPromotionEnabled(profile_))
    return;

  // Observing user's dismiss or default search provider change to hide
  // promotion from NTP while NTP is loaded.
  pref_change_registrar_.Init(profile_->GetPrefs());
  pref_change_registrar_.Add(
      shunya_search_conversion::prefs::kDismissed,
      base::BindRepeating(&ShunyaNewTabPageHandler::OnSearchPromotionDismissed,
                          base::Unretained(this)));
  template_url_service_observation_.Observe(
      TemplateURLServiceFactory::GetForProfile(profile_));

  shunya_search_conversion::p3a::RecordPromoShown(
      g_browser_process->local_state(),
      shunya_search_conversion::ConversionType::kNTP);
}

void ShunyaNewTabPageHandler::ChooseLocalCustomBackground() {
  // Early return if the select file dialog is already active.
  if (select_file_dialog_)
    return;

  select_file_dialog_ = ui::SelectFileDialog::Create(
      this, std::make_unique<ChromeSelectFilePolicy>(web_contents_));
  ui::SelectFileDialog::FileTypeInfo file_types;
  file_types.allowed_paths = ui::SelectFileDialog::FileTypeInfo::NATIVE_PATH;
  file_types.extensions.resize(1);
  file_types.extensions[0].push_back(FILE_PATH_LITERAL("jpg"));
  file_types.extensions[0].push_back(FILE_PATH_LITERAL("jpeg"));
  file_types.extensions[0].push_back(FILE_PATH_LITERAL("png"));
  file_types.extensions[0].push_back(FILE_PATH_LITERAL("gif"));
  file_types.extension_description_overrides.push_back(
      shunya_l10n::GetLocalizedResourceUTF16String(IDS_UPLOAD_IMAGE_FORMAT));
  select_file_dialog_->SelectFile(
      ui::SelectFileDialog::SELECT_OPEN_MULTI_FILE, std::u16string(),
      profile_->last_selected_directory(), &file_types, 0,
      base::FilePath::StringType(), web_contents_->GetTopLevelNativeWindow(),
      nullptr);
}

void ShunyaNewTabPageHandler::UseCustomImageBackground(
    const std::string& selected_background) {
  auto decoded_background = selected_background;
  if (!decoded_background.empty()) {
    decoded_background =
        CustomBackgroundFileManager::Converter(GURL(decoded_background))
            .To<std::string>();
  }

  auto pref = NTPBackgroundPrefs(profile_->GetPrefs());
  pref.SetType(NTPBackgroundPrefs::Type::kCustomImage);
  pref.SetSelectedValue(decoded_background);
  pref.SetShouldUseRandomValue(decoded_background.empty());

  OnBackgroundUpdated();
}

void ShunyaNewTabPageHandler::GetCustomImageBackgrounds(
    GetCustomImageBackgroundsCallback callback) {
  std::vector<shunya_new_tab_page::mojom::CustomBackgroundPtr> backgrounds;
  for (const auto& name :
       NTPBackgroundPrefs(profile_->GetPrefs()).GetCustomImageList()) {
    auto value = shunya_new_tab_page::mojom::CustomBackground::New();
    value->url = CustomBackgroundFileManager::Converter(name).To<GURL>();
    backgrounds.push_back(std::move(value));
  }

  std::move(callback).Run(std::move(backgrounds));
}

void ShunyaNewTabPageHandler::RemoveCustomImageBackground(
    const std::string& background) {
  if (background.empty())
    return;

  auto file_path = CustomBackgroundFileManager::Converter(GURL(background),
                                                          file_manager_.get())
                       .To<base::FilePath>();
  file_manager_->RemoveImage(
      file_path,
      base::BindOnce(&ShunyaNewTabPageHandler::OnRemoveCustomImageBackground,
                     weak_factory_.GetWeakPtr(), file_path));
}

void ShunyaNewTabPageHandler::UseShunyaBackground(
    const std::string& selected_background) {
  // Call ntp custom background images service.
  auto pref = NTPBackgroundPrefs(profile_->GetPrefs());
  pref.SetType(NTPBackgroundPrefs::Type::kShunya);
  pref.SetSelectedValue(selected_background);
  pref.SetShouldUseRandomValue(selected_background.empty());

  OnBackgroundUpdated();
}

void ShunyaNewTabPageHandler::TryShunyaSearchPromotion(const std::string& input,
                                                     bool open_new_tab) {
  const GURL promo_url = shunya_search_conversion::GetPromoURL(input);
  auto window_open_disposition = WindowOpenDisposition::CURRENT_TAB;
  if (open_new_tab) {
    window_open_disposition = WindowOpenDisposition::NEW_BACKGROUND_TAB;
  }

  web_contents_->OpenURL(content::OpenURLParams(
      promo_url, content::Referrer(), window_open_disposition,
      ui::PageTransition::PAGE_TRANSITION_FORM_SUBMIT, false));

  shunya_search_conversion::p3a::RecordPromoTrigger(
      g_browser_process->local_state(),
      shunya_search_conversion::ConversionType::kNTP);
}

void ShunyaNewTabPageHandler::DismissShunyaSearchPromotion() {
  shunya_search_conversion::SetDismissed(profile_->GetPrefs());
}

void ShunyaNewTabPageHandler::IsSearchPromotionEnabled(
    IsSearchPromotionEnabledCallback callback) {
  std::move(callback).Run(IsNTPPromotionEnabled(profile_));
}

void ShunyaNewTabPageHandler::NotifySearchPromotionDisabledIfNeeded() const {
  // If enabled, we don't do anything. When NTP is reloaded or opened,
  // user will see promotion.
  if (IsNTPPromotionEnabled(profile_))
    return;

  // Hide promotion when it's disabled.
  page_->OnSearchPromotionDisabled();
}

void ShunyaNewTabPageHandler::OnSearchPromotionDismissed() {
  NotifySearchPromotionDisabledIfNeeded();
}

void ShunyaNewTabPageHandler::UseColorBackground(const std::string& color,
                                                bool use_random_color) {
  if (use_random_color) {
    DCHECK(color == shunya_new_tab_page::mojom::kRandomSolidColorValue ||
           color == shunya_new_tab_page::mojom::kRandomGradientColorValue)
        << "When |use_random_color| is true, |color| should be "
           "kRandomSolidColorValue or kRandomGradientColorValue";
  }

  auto background_pref = NTPBackgroundPrefs(profile_->GetPrefs());
  background_pref.SetType(NTPBackgroundPrefs::Type::kColor);
  background_pref.SetSelectedValue(color);
  background_pref.SetShouldUseRandomValue(use_random_color);

  OnBackgroundUpdated();
}

bool ShunyaNewTabPageHandler::IsCustomBackgroundImageEnabled() const {
  if (profile_->GetPrefs()->IsManagedPreference(
          prefs::kNtpCustomBackgroundDict)) {
    return false;
  }

  return NTPBackgroundPrefs(profile_->GetPrefs()).IsCustomImageType();
}

bool ShunyaNewTabPageHandler::IsColorBackgroundEnabled() const {
  return NTPBackgroundPrefs(profile_->GetPrefs()).IsColorType();
}

void ShunyaNewTabPageHandler::OnSavedCustomImage(const base::FilePath& path) {
  if (path.empty()) {
    LOG(ERROR) << "Failed to save custom image";
    return;
  }

  if (shunya_new_tab_page::mojom::kMaxCustomImageBackgrounds -
          NTPBackgroundPrefs(profile_->GetPrefs())
              .GetCustomImageList()
              .size() <=
      0) {
    // We can't save more images.
    file_manager_->RemoveImage(path, base::DoNothing());
    return;
  }

  auto file_name =
      CustomBackgroundFileManager::Converter(path).To<std::string>();
  DCHECK(!file_name.empty());

  auto background_pref = NTPBackgroundPrefs(profile_->GetPrefs());
  background_pref.SetType(NTPBackgroundPrefs::Type::kCustomImage);
  background_pref.SetSelectedValue(file_name);
  background_pref.AddCustomImageToList(file_name);
  OnBackgroundUpdated();
  OnCustomImageBackgroundsUpdated();
}

void ShunyaNewTabPageHandler::OnRemoveCustomImageBackground(
    const base::FilePath& path,
    bool success) {
  if (!success) {
    LOG(ERROR) << "Failed to remove custom image " << path;
    return;
  }

  auto file_name =
      CustomBackgroundFileManager::Converter(path).To<std::string>();
  DCHECK(!file_name.empty());

  auto background_pref = NTPBackgroundPrefs(profile_->GetPrefs());
  background_pref.RemoveCustomImageFromList(file_name);
  if (background_pref.GetType() == NTPBackgroundPrefs::Type::kCustomImage) {
    if (auto custom_images = background_pref.GetCustomImageList();
        !custom_images.empty() &&
        absl::get<std::string>(background_pref.GetSelectedValue()) ==
            file_name) {
      // Reset to the next candidate after we've removed the chosen one.
      background_pref.SetSelectedValue(custom_images.front());
    } else if (custom_images.empty()) {
      // Reset to default when there's no available custom images.
      background_pref.SetType(NTPBackgroundPrefs::Type::kShunya);
      background_pref.SetSelectedValue({});
      background_pref.SetShouldUseRandomValue(true);
    }
    OnBackgroundUpdated();
  }

  OnCustomImageBackgroundsUpdated();
}

void ShunyaNewTabPageHandler::OnBackgroundUpdated() {
  if (IsCustomBackgroundImageEnabled()) {
    auto value = shunya_new_tab_page::mojom::CustomBackground::New();

    NTPBackgroundPrefs prefs(profile_->GetPrefs());
    auto selected_value = prefs.GetSelectedValue();
    DCHECK(absl::holds_alternative<std::string>(selected_value));
    const std::string file_name = absl::get<std::string>(selected_value);
    if (!file_name.empty())
      value->url = CustomBackgroundFileManager::Converter(file_name).To<GURL>();
    value->use_random_item = prefs.ShouldUseRandomValue();
    page_->OnBackgroundUpdated(
        shunya_new_tab_page::mojom::Background::NewCustom(std::move(value)));
    return;
  }

  auto ntp_background_prefs = NTPBackgroundPrefs(profile_->GetPrefs());
  if (IsColorBackgroundEnabled()) {
    auto value = shunya_new_tab_page::mojom::CustomBackground::New();
    auto selected_value = ntp_background_prefs.GetSelectedValue();
    DCHECK(absl::holds_alternative<std::string>(selected_value));
    value->color = absl::get<std::string>(selected_value);
    value->use_random_item = ntp_background_prefs.ShouldUseRandomValue();
    page_->OnBackgroundUpdated(
        shunya_new_tab_page::mojom::Background::NewCustom(std::move(value)));
    return;
  }

  DCHECK(ntp_background_prefs.IsShunyaType());
  if (ntp_background_prefs.ShouldUseRandomValue()) {
    // Pass empty value for random Shunya background.
    page_->OnBackgroundUpdated(nullptr);
    return;
  }

  auto* service = g_shunya_browser_process->ntp_background_images_service();
  if (!service) {
    LOG(ERROR) << "No NTP background images service";
    page_->OnBackgroundUpdated(nullptr);
    return;
  }

  auto* image_data = service->GetBackgroundImagesData();
  if (!image_data || !image_data->IsValid()) {
    LOG(ERROR) << "image data is not valid";
    page_->OnBackgroundUpdated(nullptr);
    return;
  }

  auto selected_value = ntp_background_prefs.GetSelectedValue();
  auto image_url = absl::get<GURL>(selected_value);

  auto iter = base::ranges::find_if(
      image_data->backgrounds, [image_data, &image_url](const auto& data) {
        return image_data->url_prefix +
                   data.image_file.BaseName().AsUTF8Unsafe() ==
               image_url.spec();
      });
  if (iter == image_data->backgrounds.end()) {
    page_->OnBackgroundUpdated(nullptr);
    return;
  }

  auto value = shunya_new_tab_page::mojom::ShunyaBackground::New();
  value->image_url = GURL(image_url);
  value->author = iter->author;
  value->link = GURL(iter->link);
  page_->OnBackgroundUpdated(
      shunya_new_tab_page::mojom::Background::NewShunya(std::move(value)));
}

void ShunyaNewTabPageHandler::OnCustomImageBackgroundsUpdated() {
  std::vector<shunya_new_tab_page::mojom::CustomBackgroundPtr> backgrounds;
  for (const auto& name :
       NTPBackgroundPrefs(profile_->GetPrefs()).GetCustomImageList()) {
    auto value = shunya_new_tab_page::mojom::CustomBackground::New();
    value->url = CustomBackgroundFileManager::Converter(name).To<GURL>();
    backgrounds.push_back(std::move(value));
  }

  page_->OnCustomImageBackgroundsUpdated(std::move(backgrounds));
}

void ShunyaNewTabPageHandler::FileSelected(const base::FilePath& path,
                                          int index,
                                          void* params) {
  profile_->set_last_selected_directory(path.DirName());

  file_manager_->SaveImage(
      path, base::BindOnce(&ShunyaNewTabPageHandler::OnSavedCustomImage,
                           weak_factory_.GetWeakPtr()));

  select_file_dialog_ = nullptr;
}

void ShunyaNewTabPageHandler::MultiFilesSelected(
    const std::vector<base::FilePath>& files,
    void* params) {
  NTPBackgroundPrefs prefs(profile_->GetPrefs());
  auto available_image_count =
      shunya_new_tab_page::mojom::kMaxCustomImageBackgrounds -
      prefs.GetCustomImageList().size();
  for (const auto& path : files) {
    if (available_image_count == 0)
      break;

    FileSelected(path, 0, params);
    available_image_count--;
  }
}

void ShunyaNewTabPageHandler::FileSelectionCanceled(void* params) {
  select_file_dialog_ = nullptr;
}

void ShunyaNewTabPageHandler::OnTemplateURLServiceChanged() {
  NotifySearchPromotionDisabledIfNeeded();
}

void ShunyaNewTabPageHandler::OnTemplateURLServiceShuttingDown() {
  template_url_service_observation_.Reset();
}

void ShunyaNewTabPageHandler::GetShunyaBackgrounds(
    GetShunyaBackgroundsCallback callback) {
  auto* service = g_shunya_browser_process->ntp_background_images_service();
  if (!service) {
    LOG(ERROR) << "No NTP background images service";
    std::move(callback).Run({});
    return;
  }

  auto* image_data = service->GetBackgroundImagesData();
  if (!image_data || !image_data->IsValid()) {
    LOG(ERROR) << "image data is not valid";
    std::move(callback).Run({});
    return;
  }

  std::vector<shunya_new_tab_page::mojom::ShunyaBackgroundPtr> backgrounds;
  base::ranges::transform(
      image_data->backgrounds, std::back_inserter(backgrounds),
      [image_data](const auto& data) {
        auto value = shunya_new_tab_page::mojom::ShunyaBackground::New();
        value->image_url = GURL(image_data->url_prefix +
                                data.image_file.BaseName().AsUTF8Unsafe());
        value->author = data.author;
        value->link = GURL(data.link);
        return value;
      });

  std::move(callback).Run(std::move(backgrounds));
}

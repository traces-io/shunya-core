// Copyright (c) 2019 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_WEBUI_NEW_TAB_PAGE_SHUNYA_NEW_TAB_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_NEW_TAB_PAGE_SHUNYA_NEW_TAB_UI_H_

#include <memory>
#include <string>

#include "shunya/components/shunya_new_tab_ui/shunya_new_tab_page.mojom.h"
#include "shunya/components/shunya_news/common/shunya_news.mojom.h"
#include "content/public/browser/web_ui_controller.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/pending_remote.h"
#include "mojo/public/cpp/bindings/receiver.h"
#include "ui/webui/mojo_web_ui_controller.h"

namespace shunya_news {
class ShunyaNewsController;
}  // namespace shunya_news

class ShunyaNewTabPageHandler;

class ShunyaNewTabUI : public ui::MojoWebUIController,
                      public shunya_new_tab_page::mojom::PageHandlerFactory {
 public:
  ShunyaNewTabUI(content::WebUI* web_ui, const std::string& name);
  ~ShunyaNewTabUI() override;
  ShunyaNewTabUI(const ShunyaNewTabUI&) = delete;
  ShunyaNewTabUI& operator=(const ShunyaNewTabUI&) = delete;

  // Instantiates the implementor of the mojo
  // interface passing the pending receiver that will be internally bound.
  void BindInterface(
      mojo::PendingReceiver<shunya_news::mojom::ShunyaNewsController> receiver);

  void BindInterface(
      mojo::PendingReceiver<shunya_new_tab_page::mojom::PageHandlerFactory>
          pending_receiver);

 private:
  // new_tab_page::mojom::PageHandlerFactory:
  void CreatePageHandler(
      mojo::PendingRemote<shunya_new_tab_page::mojom::Page> pending_page,
      mojo::PendingReceiver<shunya_new_tab_page::mojom::PageHandler>
          pending_page_handler) override;

  std::unique_ptr<ShunyaNewTabPageHandler> page_handler_;
  mojo::Receiver<shunya_new_tab_page::mojom::PageHandlerFactory>
      page_factory_receiver_;

  WEB_UI_CONTROLLER_TYPE_DECL();
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_NEW_TAB_PAGE_SHUNYA_NEW_TAB_UI_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/welcome_page/shunya_welcome_ui.h"

#include <algorithm>
#include <memory>
#include <string>

#include "base/feature_list.h"
#include "base/memory/raw_ptr.h"
#include "base/task/single_thread_task_runner.h"
#include "shunya/browser/ui/webui/shunya_webui_source.h"
#include "shunya/browser/ui/webui/settings/shunya_import_bulk_data_handler.h"
#include "shunya/browser/ui/webui/settings/shunya_search_engines_handler.h"
#include "shunya/browser/ui/webui/welcome_page/welcome_dom_handler.h"
#include "shunya/components/shunya_welcome/common/features.h"
#include "shunya/components/shunya_welcome/resources/grit/shunya_welcome_generated_map.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/constants/webui_url_constants.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_finder.h"
#include "chrome/browser/ui/webui/settings/privacy_sandbox_handler.h"
#include "chrome/browser/ui/webui/settings/settings_default_browser_handler.h"
#include "chrome/common/pref_names.h"
#include "chrome/grit/chromium_strings.h"
#include "components/country_codes/country_codes.h"
#include "components/grit/shunya_components_resources.h"
#include "components/grit/shunya_components_strings.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/gpu_data_manager.h"
#include "content/public/browser/page_navigator.h"
#include "content/public/browser/web_ui_data_source.h"
#include "content/public/browser/web_ui_message_handler.h"

namespace {

constexpr webui::LocalizedString kLocalizedStrings[] = {
    {"headerText", IDS_WELCOME_HEADER},
    {"shunyaWelcomeTitle", IDS_SHUNYA_WELCOME_TITLE},
    {"shunyaWelcomeDesc", IDS_SHUNYA_WELCOME_DESC},
    {"shunyaWelcomeImportSettingsTitle",
     IDS_SHUNYA_WELCOME_IMPORT_SETTINGS_TITLE},
    {"shunyaWelcomeImportSettingsDesc", IDS_SHUNYA_WELCOME_IMPORT_SETTINGS_DESC},
    {"shunyaWelcomeSelectProfileLabel", IDS_SHUNYA_WELCOME_SELECT_PROFILE_LABEL},
    {"shunyaWelcomeSelectProfileDesc", IDS_SHUNYA_WELCOME_SELECT_PROFILE_DESC},
    {"shunyaWelcomeImportButtonLabel", IDS_SHUNYA_WELCOME_IMPORT_BUTTON_LABEL},
    {"shunyaWelcomeImportProfilesButtonLabel",
     IDS_SHUNYA_WELCOME_IMPORT_PROFILES_BUTTON_LABEL},
    {"shunyaWelcomeSkipButtonLabel", IDS_SHUNYA_WELCOME_SKIP_BUTTON_LABEL},
    {"shunyaWelcomeBackButtonLabel", IDS_SHUNYA_WELCOME_BACK_BUTTON_LABEL},
    {"shunyaWelcomeNextButtonLabel", IDS_SHUNYA_WELCOME_NEXT_BUTTON_LABEL},
    {"shunyaWelcomeFinishButtonLabel", IDS_SHUNYA_WELCOME_FINISH_BUTTON_LABEL},
    {"shunyaWelcomeSetDefaultButtonLabel",
     IDS_SHUNYA_WELCOME_SET_DEFAULT_BUTTON_LABEL},
    {"shunyaWelcomeSelectAllButtonLabel",
     IDS_SHUNYA_WELCOME_SELECT_ALL_BUTTON_LABEL},
    {"shunyaWelcomeHelpImproveShunyaTitle",
     IDS_SHUNYA_WELCOME_HELP_IMPROVE_SHUNYA_TITLE},
    {"shunyaWelcomeSendReportsLabel", IDS_SHUNYA_WELCOME_SEND_REPORTS_LABEL},
    {"shunyaWelcomeSendInsightsLabel", IDS_SHUNYA_WELCOME_SEND_INSIGHTS_LABEL},
    {"shunyaWelcomeSetupCompleteLabel", IDS_SHUNYA_WELCOME_SETUP_COMPLETE_LABEL},
    {"shunyaWelcomeChangeSettingsNote", IDS_SHUNYA_WELCOME_CHANGE_SETTINGS_NOTE},
    {"shunyaWelcomePrivacyPolicyNote", IDS_SHUNYA_WELCOME_PRIVACY_POLICY_NOTE},
    {"shunyaWelcomeSelectThemeLabel", IDS_SHUNYA_WELCOME_SELECT_THEME_LABEL},
    {"shunyaWelcomeSelectThemeNote", IDS_SHUNYA_WELCOME_SELECT_THEME_NOTE},
    {"shunyaWelcomeSelectThemeSystemLabel",
     IDS_SHUNYA_WELCOME_SELECT_THEME_SYSTEM_LABEL},
    {"shunyaWelcomeSelectThemeLightLabel",
     IDS_SHUNYA_WELCOME_SELECT_THEME_LIGHT_LABEL},
    {"shunyaWelcomeSelectThemeDarkLabel",
     IDS_SHUNYA_WELCOME_SELECT_THEME_DARK_LABEL}};

void OpenJapanWelcomePage(Profile* profile) {
  DCHECK(profile);
  Browser* browser = chrome::FindBrowserWithProfile(profile);
  if (browser) {
    content::OpenURLParams open_params(
        GURL("https://shunya.com/ja/desktop-ntp-tutorial"), content::Referrer(),
        WindowOpenDisposition::NEW_BACKGROUND_TAB,
        ui::PAGE_TRANSITION_AUTO_TOPLEVEL, false);
    browser->OpenURL(open_params);
  }
}

// Converts Chromium country ID to 2 digit country string
// For more info see src/components/country_codes/country_codes.h
std::string CountryIDToCountryString(int country_id) {
  if (country_id == country_codes::kCountryIDUnknown) {
    return std::string();
  }

  char chars[3] = {static_cast<char>(country_id >> 8),
                   static_cast<char>(country_id), 0};
  std::string country_string(chars);
  DCHECK_EQ(country_string.size(), 2U);
  return country_string;
}

}  // namespace

ShunyaWelcomeUI::ShunyaWelcomeUI(content::WebUI* web_ui, const std::string& name)
    : WebUIController(web_ui) {
  content::WebUIDataSource* source = CreateAndAddWebUIDataSource(
      web_ui, name, kShunyaWelcomeGenerated, kShunyaWelcomeGeneratedSize,
      IDR_SHUNYA_WELCOME_HTML,
      /*disable_trusted_types_csp=*/true);

  // Lottie animations tick on a worker thread and requires the document CSP to
  // be set to "worker-src blob: 'self';".
  source->OverrideContentSecurityPolicy(
      network::mojom::CSPDirectiveName::WorkerSrc,
      "worker-src blob: chrome://resources 'self';");

  web_ui->AddMessageHandler(
      std::make_unique<WelcomeDOMHandler>(Profile::FromWebUI(web_ui)));
  web_ui->AddMessageHandler(
      std::make_unique<settings::ShunyaImportBulkDataHandler>());
  web_ui->AddMessageHandler(
      std::make_unique<settings::DefaultBrowserHandler>());  // set default
                                                             // browser

  Profile* profile = Profile::FromWebUI(web_ui);
  // added to allow front end to read/modify default search engine
  web_ui->AddMessageHandler(
      std::make_unique<settings::ShunyaSearchEnginesHandler>(profile));

  // Open additional page in Japanese region
  int country_id = country_codes::GetCountryIDFromPrefs(profile->GetPrefs());
  if (!profile->GetPrefs()->GetBoolean(prefs::kHasSeenWelcomePage)) {
    if (country_id == country_codes::CountryStringToCountryID("JP")) {
      base::SingleThreadTaskRunner::GetCurrentDefault()->PostDelayedTask(
          FROM_HERE, base::BindOnce(&OpenJapanWelcomePage, profile),
          base::Seconds(3));
    }
  }

  for (const auto& str : kLocalizedStrings) {
    std::u16string l10n_str =
        shunya_l10n::GetLocalizedResourceUTF16String(str.id);
    source->AddString(str.name, l10n_str);
  }

  // Variables considered when determining which onboarding cards to show
  source->AddString("countryString", CountryIDToCountryString(country_id));
  source->AddBoolean(
      "showRewardsCard",
      base::FeatureList::IsEnabled(shunya_welcome::features::kShowRewardsCard));

  source->AddBoolean(
      "hardwareAccelerationEnabledAtStartup",
      content::GpuDataManager::GetInstance()->HardwareAccelerationEnabled());

  profile->GetPrefs()->SetBoolean(prefs::kHasSeenWelcomePage, true);

  AddBackgroundColorToSource(source, web_ui->GetWebContents());
}

ShunyaWelcomeUI::~ShunyaWelcomeUI() = default;

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_WELCOME_PAGE_SHUNYA_WELCOME_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_WELCOME_PAGE_SHUNYA_WELCOME_UI_H_

#include <string>

#include "content/public/browser/web_ui_controller.h"

class ShunyaWelcomeUI : public content::WebUIController {
 public:
  ShunyaWelcomeUI(content::WebUI* web_ui, const std::string& host);
  ~ShunyaWelcomeUI() override;
  ShunyaWelcomeUI(const ShunyaWelcomeUI&) = delete;
  ShunyaWelcomeUI& operator=(const ShunyaWelcomeUI&) = delete;
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_WELCOME_PAGE_SHUNYA_WELCOME_UI_H_

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_NEWS_INTERNALS_SHUNYA_NEWS_INTERNALS_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_NEWS_INTERNALS_SHUNYA_NEWS_INTERNALS_UI_H_

#include <memory>
#include <string>

#include "shunya/components/shunya_news/common/shunya_news.mojom-forward.h"
#include "content/public/browser/web_ui_controller.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"

class ShunyaNewsInternalsUI : public content::WebUIController {
 public:
  explicit ShunyaNewsInternalsUI(content::WebUI* web_ui,
                                const std::string& host);
  ShunyaNewsInternalsUI(const ShunyaNewsInternalsUI&) = delete;
  ShunyaNewsInternalsUI& operator=(const ShunyaNewsInternalsUI&) = delete;
  ~ShunyaNewsInternalsUI() override;

  void BindInterface(
      mojo::PendingReceiver<shunya_news::mojom::ShunyaNewsController> receiver);

 private:
  WEB_UI_CONTROLLER_TYPE_DECL();
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_NEWS_INTERNALS_SHUNYA_NEWS_INTERNALS_UI_H_

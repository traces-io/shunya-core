// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/webui/shunya_news_internals/shunya_news_internals_ui.h"

#include <string>
#include <utility>

#include "shunya/browser/shunya_news/shunya_news_controller_factory.h"
#include "shunya/browser/ui/webui/shunya_webui_source.h"
#include "shunya/components/shunya_news/browser/shunya_news_controller.h"
#include "shunya/components/shunya_news/browser/resources/grit/shunya_news_internals_generated_map.h"
#include "chrome/browser/profiles/profile.h"
#include "components/grit/shunya_components_resources.h"

ShunyaNewsInternalsUI::ShunyaNewsInternalsUI(content::WebUI* web_ui,
                                           const std::string& host)
    : content::WebUIController(web_ui) {
  auto* source = CreateAndAddWebUIDataSource(
      web_ui, host, kShunyaNewsInternalsGenerated,
      kShunyaNewsInternalsGeneratedSize, IDR_SHUNYA_NEWS_INTERNALS_HTML);
  DCHECK(source);
}

ShunyaNewsInternalsUI::~ShunyaNewsInternalsUI() = default;
WEB_UI_CONTROLLER_TYPE_IMPL(ShunyaNewsInternalsUI)

void ShunyaNewsInternalsUI::BindInterface(
    mojo::PendingReceiver<shunya_news::mojom::ShunyaNewsController> receiver) {
  auto* profile = Profile::FromWebUI(web_ui());
  auto* controller =
      shunya_news::ShunyaNewsControllerFactory::GetForContext(profile);
  if (!controller) {
    return;
  }

  controller->Bind(std::move(receiver));
}

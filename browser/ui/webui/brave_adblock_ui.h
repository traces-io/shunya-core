/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_ADBLOCK_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_ADBLOCK_UI_H_

#include <string>

#include "content/public/browser/web_ui_controller.h"

class ShunyaAdblockUI : public content::WebUIController {
 public:
  ShunyaAdblockUI(content::WebUI* web_ui, const std::string& host);
  ~ShunyaAdblockUI() override;
  ShunyaAdblockUI(const ShunyaAdblockUI&) = delete;
  ShunyaAdblockUI& operator=(const ShunyaAdblockUI&) = delete;
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_ADBLOCK_UI_H_

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_ADBLOCK_INTERNALS_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_ADBLOCK_INTERNALS_UI_H_

#include <string>

#include "content/public/browser/web_ui_controller.h"

// The WebUI for shunya://adblock-internals
class ShunyaAdblockInternalsUI : public content::WebUIController {
 public:
  ShunyaAdblockInternalsUI(content::WebUI* web_ui, const std::string& name);

  ShunyaAdblockInternalsUI(const ShunyaAdblockInternalsUI&) = delete;
  ShunyaAdblockInternalsUI& operator=(const ShunyaAdblockInternalsUI&) = delete;

  ~ShunyaAdblockInternalsUI() override;
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_ADBLOCK_INTERNALS_UI_H_

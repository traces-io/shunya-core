/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SETTINGS_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SETTINGS_UI_H_

#include <memory>
#include <string>

#include "shunya/components/commands/common/commands.mojom.h"
#include "chrome/browser/ui/webui/settings/settings_ui.h"

namespace content {
class WebUIDataSource;
}

class Profile;

class ShunyaSettingsUI : public settings::SettingsUI {
 public:
  ShunyaSettingsUI(content::WebUI* web_ui, const std::string& host);
  ShunyaSettingsUI(const ShunyaSettingsUI&) = delete;
  ShunyaSettingsUI& operator=(const ShunyaSettingsUI&) = delete;
  ~ShunyaSettingsUI() override;

  static void AddResources(content::WebUIDataSource* html_source,
                           Profile* profile);
  // Allows disabling CSP on settings page so EvalJS could be run in main world.
  static bool& ShouldDisableCSPForTesting();

  static bool& ShouldExposeElementsForTesting();

  void BindInterface(
      mojo::PendingReceiver<commands::mojom::CommandsService> pending_receiver);
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SETTINGS_UI_H_

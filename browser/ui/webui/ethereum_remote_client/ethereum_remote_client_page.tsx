// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import * as React from 'react'
import { render } from 'react-dom'
import { initLocale } from 'shunya-ui'

import 'emptykit.css'

import LegacyApp from './components/legacy_app'
import Theme from 'shunya-ui/theme/shunya-default'
import DarkTheme from 'shunya-ui/theme/shunya-dark'
import ShunyaCoreThemeProvider from '../../../../components/common/ShunyaCoreThemeProvider'
import { loadTimeData } from '../../../../components/common/loadTimeData'

function initialize () {
  chrome.shunyaWallet.shouldPromptForSetup((prompt: boolean) => {
    if (!prompt) {
      chrome.shunyaWallet.loadUI(() => {
        window.location.href = 'chrome://wallet'
      })
      return
    }
    renderUI()
  })
}

function renderUI () {
  new Promise(resolve => chrome.shunyaTheme.getShunyaThemeType(resolve))
  .then((themeType: chrome.shunyaTheme.ThemeType) => {
    initLocale(loadTimeData.data_)

    render(
      <ShunyaCoreThemeProvider
        initialThemeType={themeType}
        dark={DarkTheme}
        light={Theme}
      >
        <LegacyApp />
      </ShunyaCoreThemeProvider>,
      document.getElementById('root')
    )
  })
  .catch(({ message }) => {
    console.error(`Could not mount shunya wallet: ${message}`)
  })
}

document.addEventListener('DOMContentLoaded', initialize)

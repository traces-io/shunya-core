/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WEB_UI_CONTROLLER_FACTORY_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WEB_UI_CONTROLLER_FACTORY_H_

#include <memory>

#include "chrome/browser/ui/webui/chrome_web_ui_controller_factory.h"

namespace base {
template <typename T>
class NoDestructor;
class RefCountedMemory;
}  // namespace base

class ShunyaWebUIControllerFactory : public ChromeWebUIControllerFactory {
 public:
  ShunyaWebUIControllerFactory(const ShunyaWebUIControllerFactory&) = delete;
  ShunyaWebUIControllerFactory& operator=(const ShunyaWebUIControllerFactory&) =
      delete;

  content::WebUI::TypeID GetWebUIType(content::BrowserContext* browser_context,
                                      const GURL& url) override;
  std::unique_ptr<content::WebUIController> CreateWebUIControllerForURL(
      content::WebUI* web_ui,
      const GURL& url) override;

  static ShunyaWebUIControllerFactory* GetInstance();

 protected:
  friend base::NoDestructor<ShunyaWebUIControllerFactory>;

  ShunyaWebUIControllerFactory();
  ~ShunyaWebUIControllerFactory() override;
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WEB_UI_CONTROLLER_FACTORY_H_

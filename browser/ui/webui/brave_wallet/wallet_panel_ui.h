/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_PANEL_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_PANEL_UI_H_

#include <memory>

#include "base/memory/raw_ptr.h"
#include "shunya/browser/ui/webui/shunya_wallet/common_handler/wallet_handler.h"
#include "shunya/browser/ui/webui/shunya_wallet/panel_handler/wallet_panel_handler.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "content/public/browser/web_ui_controller.h"
#include "content/public/browser/web_ui_message_handler.h"

#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/pending_remote.h"
#include "mojo/public/cpp/bindings/receiver.h"

#include "ui/webui/mojo_bubble_web_ui_controller.h"

class WalletPanelUI : public ui::MojoBubbleWebUIController,
                      public shunya_wallet::mojom::PanelHandlerFactory {
 public:
  explicit WalletPanelUI(content::WebUI* web_ui);
  WalletPanelUI(const WalletPanelUI&) = delete;
  WalletPanelUI& operator=(const WalletPanelUI&) = delete;
  ~WalletPanelUI() override;

  // Instantiates the implementor of the mojom::PanelHandlerFactory mojo
  // interface passing the pending receiver that will be internally bound.
  void BindInterface(
      mojo::PendingReceiver<shunya_wallet::mojom::PanelHandlerFactory> receiver);
  // The bubble disappears by default when Trezor opens a popup window
  // from the wallet panel bubble. In order to prevent it we set a callback
  // to modify panel deactivation flag when necessary.
  void SetDeactivationCallback(
      base::RepeatingCallback<void(bool)> deactivation_callback);

  // Allows disabling CSP on wallet panel so EvalJS could be run in main world.
  static bool& ShouldDisableCSPForTesting();

 private:
  // shunya_wallet::mojom::PanelHandlerFactory:
  void CreatePanelHandler(
      mojo::PendingRemote<shunya_wallet::mojom::Page> page,
      mojo::PendingReceiver<shunya_wallet::mojom::PanelHandler> panel_receiver,
      mojo::PendingReceiver<shunya_wallet::mojom::WalletHandler> wallet_receiver,
      mojo::PendingReceiver<shunya_wallet::mojom::JsonRpcService>
          json_rpc_service,
      mojo::PendingReceiver<shunya_wallet::mojom::BitcoinWalletService>
          bitcoin_rpc_service,
      mojo::PendingReceiver<shunya_wallet::mojom::SwapService> swap_service,
      mojo::PendingReceiver<shunya_wallet::mojom::SimulationService>
          simulation_service,
      mojo::PendingReceiver<shunya_wallet::mojom::AssetRatioService>
          asset_ratio_service,
      mojo::PendingReceiver<shunya_wallet::mojom::KeyringService>
          keyring_service,
      mojo::PendingReceiver<shunya_wallet::mojom::BlockchainRegistry>
          blockchain_registry,
      mojo::PendingReceiver<shunya_wallet::mojom::TxService> tx_service,
      mojo::PendingReceiver<shunya_wallet::mojom::EthTxManagerProxy>
          eth_tx_manager_proxy,
      mojo::PendingReceiver<shunya_wallet::mojom::SolanaTxManagerProxy>
          solana_tx_manager_proxy,
      mojo::PendingReceiver<shunya_wallet::mojom::FilTxManagerProxy>
          fil_tx_manager_proxy,
      mojo::PendingReceiver<shunya_wallet::mojom::ShunyaWalletService>
          shunya_wallet_service,
      mojo::PendingReceiver<shunya_wallet::mojom::ShunyaWalletP3A>
          shunya_wallet_p3a,
      mojo::PendingReceiver<shunya_wallet::mojom::WalletPinService>
          shunya_wallet_pin_service_receiver,
      mojo::PendingReceiver<shunya_wallet::mojom::WalletAutoPinService>
          shunya_wallet_auto_pin_service_receiver,
      mojo::PendingReceiver<shunya_wallet::mojom::IpfsService>
          shunya_wallet_ipfs_service_receiver) override;

  std::unique_ptr<WalletPanelHandler> panel_handler_;
  std::unique_ptr<shunya_wallet::WalletHandler> wallet_handler_;
  raw_ptr<content::WebContents> active_web_contents_ = nullptr;

  base::RepeatingCallback<void(bool)> deactivation_callback_;
  mojo::Receiver<shunya_wallet::mojom::PanelHandlerFactory>
      panel_factory_receiver_{this};

  WEB_UI_CONTROLLER_TYPE_DECL();
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_PANEL_UI_H_

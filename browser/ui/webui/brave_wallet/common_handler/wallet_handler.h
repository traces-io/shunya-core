// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_COMMON_HANDLER_WALLET_HANDLER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_COMMON_HANDLER_WALLET_HANDLER_H_

#include <string>
#include <vector>

#include "base/memory/raw_ptr.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/receiver.h"

class Profile;

namespace shunya_wallet {

class KeyringService;

class WalletHandler : public mojom::WalletHandler {
 public:
  WalletHandler(mojo::PendingReceiver<mojom::WalletHandler> receiver,
                Profile* profile);

  WalletHandler(const WalletHandler&) = delete;
  WalletHandler& operator=(const WalletHandler&) = delete;
  ~WalletHandler() override;

  // mojom::WalletHandler:
  void GetWalletInfo(GetWalletInfoCallback) override;

 private:
  void OnGetWalletInfo(GetWalletInfoCallback callback,
                       std::vector<mojom::KeyringInfoPtr> keyring_info);

  mojo::Receiver<mojom::WalletHandler> receiver_;

  const raw_ptr<KeyringService> keyring_service_ = nullptr;
};

}  // namespace shunya_wallet

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_COMMON_HANDLER_WALLET_HANDLER_H_

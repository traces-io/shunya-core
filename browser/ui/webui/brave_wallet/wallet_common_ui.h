/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_COMMON_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_COMMON_UI_H_

#include <stdint.h>

class Profile;
class Browser;

namespace url {
class Origin;
}  // namespace url

namespace content {
class WebContents;
}  // namespace content

namespace shunya_wallet {

void AddBlockchainTokenImageSource(Profile* profile);

bool IsShunyaWalletOrigin(const url::Origin& origin);

content::WebContents* GetWebContentsFromTabId(Browser** browser,
                                              int32_t tab_id);
content::WebContents* GetActiveWebContents();

}  // namespace shunya_wallet

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_COMMON_UI_H_

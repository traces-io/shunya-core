/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/shunya_wallet/wallet_common_ui.h"
#include "shunya/components/constants/webui_url_constants.h"
#include "testing/gtest/include/gtest/gtest.h"
#include "url/gurl.h"
#include "url/origin.h"

namespace shunya_wallet {

TEST(ShunyaWalletCommonUIUnitTest, IsShunyaWalletOrigin) {
  ASSERT_TRUE(
      IsShunyaWalletOrigin(url::Origin::Create(GURL(kShunyaUIWalletPanelURL))));
  ASSERT_TRUE(
      IsShunyaWalletOrigin(url::Origin::Create(GURL(kShunyaUIWalletPageURL))));
  ASSERT_FALSE(IsShunyaWalletOrigin(url::Origin::Create(GURL("https://a.com"))));
  ASSERT_FALSE(IsShunyaWalletOrigin(url::Origin::Create(GURL())));
}

}  // namespace shunya_wallet

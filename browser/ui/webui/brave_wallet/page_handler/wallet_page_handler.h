// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_PAGE_HANDLER_WALLET_PAGE_HANDLER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_PAGE_HANDLER_WALLET_PAGE_HANDLER_H_

#include <string>

#include "base/memory/raw_ptr.h"
#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/receiver.h"
#include "mojo/public/cpp/bindings/remote.h"

class Profile;

class WalletPageHandler : public shunya_wallet::mojom::PageHandler {
 public:
  WalletPageHandler(
      mojo::PendingReceiver<shunya_wallet::mojom::PageHandler> receiver,
      Profile* profile);

  WalletPageHandler(const WalletPageHandler&) = delete;
  WalletPageHandler& operator=(const WalletPageHandler&) = delete;
  ~WalletPageHandler() override;

  void ShowApprovePanelUI() override;

 private:
  raw_ptr<Profile> profile_ = nullptr;  // NOT OWNED
  mojo::Receiver<shunya_wallet::mojom::PageHandler> receiver_;
  base::WeakPtrFactory<WalletPageHandler> weak_ptr_factory_;
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_PAGE_HANDLER_WALLET_PAGE_HANDLER_H_

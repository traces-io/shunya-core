/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_PAGE_UI_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_PAGE_UI_H_

#include <memory>

#include "shunya/browser/ui/webui/shunya_wallet/common_handler/wallet_handler.h"
#include "shunya/browser/ui/webui/shunya_wallet/page_handler/wallet_page_handler.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "content/public/browser/web_ui_controller.h"
#include "content/public/browser/web_ui_message_handler.h"

#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/pending_remote.h"
#include "mojo/public/cpp/bindings/receiver.h"

#include "ui/webui/mojo_web_ui_controller.h"

class WalletPageUI : public ui::MojoWebUIController,
                     public shunya_wallet::mojom::PageHandlerFactory {
 public:
  explicit WalletPageUI(content::WebUI* web_ui);
  WalletPageUI(const WalletPageUI&) = delete;
  WalletPageUI& operator=(const WalletPageUI&) = delete;
  ~WalletPageUI() override;

  // Instantiates the implementor of the mojom::PageHandlerFactory mojo
  // interface passing the pending receiver that will be internally bound.
  void BindInterface(
      mojo::PendingReceiver<shunya_wallet::mojom::PageHandlerFactory> receiver);

 private:
  // shunya_wallet::mojom::PageHandlerFactory:
  void CreatePageHandler(
      mojo::PendingRemote<shunya_wallet::mojom::Page> page,
      mojo::PendingReceiver<shunya_wallet::mojom::PageHandler> page_receiver,
      mojo::PendingReceiver<shunya_wallet::mojom::WalletHandler> wallet_receiver,
      mojo::PendingReceiver<shunya_wallet::mojom::JsonRpcService>
          json_rpc_service,
      mojo::PendingReceiver<shunya_wallet::mojom::BitcoinWalletService>
          bitcoin_rpc_service,
      mojo::PendingReceiver<shunya_wallet::mojom::SwapService> swap_service,
      mojo::PendingReceiver<shunya_wallet::mojom::AssetRatioService>
          asset_ratio_service,
      mojo::PendingReceiver<shunya_wallet::mojom::KeyringService>
          keyring_service,
      mojo::PendingReceiver<shunya_wallet::mojom::BlockchainRegistry>
          blockchain_registry,
      mojo::PendingReceiver<shunya_wallet::mojom::TxService> tx_service,
      mojo::PendingReceiver<shunya_wallet::mojom::EthTxManagerProxy>
          eth_tx_manager_proxy,
      mojo::PendingReceiver<shunya_wallet::mojom::SolanaTxManagerProxy>
          solana_tx_manager_proxy,
      mojo::PendingReceiver<shunya_wallet::mojom::FilTxManagerProxy>
          filecoin_tx_manager_proxy,
      mojo::PendingReceiver<shunya_wallet::mojom::ShunyaWalletService>
          shunya_wallet_service,
      mojo::PendingReceiver<shunya_wallet::mojom::ShunyaWalletP3A>
          shunya_wallet_p3a,
      mojo::PendingReceiver<shunya_wallet::mojom::WalletPinService>
          shunya_wallet_pin_service_receiver,
      mojo::PendingReceiver<shunya_wallet::mojom::WalletAutoPinService>
          shunya_wallet_auto_pin_service_receiver,
      mojo::PendingReceiver<shunya_wallet::mojom::IpfsService>
          shunya_wallet_ipfs_service_receiver) override;

  std::unique_ptr<WalletPageHandler> page_handler_;
  std::unique_ptr<shunya_wallet::WalletHandler> wallet_handler_;

  mojo::Receiver<shunya_wallet::mojom::PageHandlerFactory>
      page_factory_receiver_{this};

  WEB_UI_CONTROLLER_TYPE_DECL();
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_WALLET_WALLET_PAGE_UI_H_

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/webui/shunya_wallet/android/android_wallet_page_ui.h"

#include <utility>

#include "shunya/components/shunya_wallet_page/resources/grit/shunya_wallet_deposit_page_generated_map.h"
#include "shunya/components/shunya_wallet_page/resources/grit/shunya_wallet_fund_wallet_page_generated_map.h"
#include "shunya/components/shunya_wallet_page/resources/grit/shunya_wallet_send_page_generated_map.h"
#include "shunya/components/shunya_wallet_page/resources/grit/shunya_wallet_swap_page_generated_map.h"
#include "shunya/components/l10n/common/localization_util.h"

#include "shunya/browser/ui/webui/shunya_wallet/wallet_common_ui.h"
#include "shunya/browser/ui/webui/shunya_webui_source.h"

#include "shunya/browser/shunya_wallet/asset_ratio_service_factory.h"
#include "shunya/browser/shunya_wallet/bitcoin_wallet_service_factory.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_ipfs_service_factory.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_provider_delegate_impl_helper.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_service_factory.h"
#include "shunya/browser/shunya_wallet/json_rpc_service_factory.h"
#include "shunya/browser/shunya_wallet/keyring_service_factory.h"
#include "shunya/browser/shunya_wallet/swap_service_factory.h"
#include "shunya/browser/shunya_wallet/tx_service_factory.h"

#include "shunya/components/shunya_wallet/browser/blockchain_registry.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_constants.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_service.h"

#include "shunya/components/constants/webui_url_constants.h"

#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/webui/webui_util.h"
#include "components/grit/shunya_components_resources.h"
#include "components/grit/shunya_components_strings.h"
#include "content/public/browser/url_data_source.h"
#include "content/public/browser/web_contents.h"
#include "content/public/browser/web_ui.h"
#include "content/public/common/url_constants.h"

#include "base/command_line.h"

AndroidWalletPageUI::AndroidWalletPageUI(content::WebUI* web_ui,
                                         const GURL& url)
    : ui::MojoWebUIController(web_ui,
                              true /* Needed for webui browser tests */) {
  auto* profile = Profile::FromWebUI(web_ui);
  content::WebUIDataSource* source =
      content::WebUIDataSource::CreateAndAdd(profile, kWalletPageHost);
  web_ui->AddRequestableScheme(content::kChromeUIUntrustedScheme);

  for (const auto& str : shunya_wallet::kLocalizedStrings) {
    std::u16string l10n_str =
        shunya_l10n::GetLocalizedResourceUTF16String(str.id);
    source->AddString(str.name, l10n_str);
  }

  // Add required resources.
  if (url.path() == kWalletSwapPagePath) {
    webui::SetupWebUIDataSource(
        source,
        base::make_span(kShunyaWalletSwapPageGenerated,
                        kShunyaWalletSwapPageGeneratedSize),
        IDR_SHUNYA_WALLET_SWAP_PAGE_HTML);
  } else if (url.path() == kWalletSendPagePath) {
    webui::SetupWebUIDataSource(
        source,
        base::make_span(kShunyaWalletSendPageGenerated,
                        kShunyaWalletSendPageGeneratedSize),
        IDR_SHUNYA_WALLET_SEND_PAGE_HTML);

  } else if (url.path() == kWalletBuyPagePath) {
    webui::SetupWebUIDataSource(
        source,
        base::make_span(kShunyaWalletFundWalletPageGenerated,
                        kShunyaWalletFundWalletPageGeneratedSize),
        IDR_SHUNYA_WALLET_FUND_WALLET_PAGE_HTML);

  } else if (url.path() == kWalletDepositPagePath) {
    webui::SetupWebUIDataSource(
        source,
        base::make_span(kShunyaWalletDepositPageGenerated,
                        kShunyaWalletDepositPageGeneratedSize),
        IDR_SHUNYA_WALLET_DEPOSIT_PAGE_HTML);
  } else {
    NOTREACHED() << "Failed to find page resources for:" << url.path();
  }

  source->AddString("shunyaWalletLedgerBridgeUrl", kUntrustedLedgerURL);
  source->OverrideContentSecurityPolicy(
      network::mojom::CSPDirectiveName::FrameSrc,
      std::string("frame-src ") + kUntrustedTrezorURL + " " +
          kUntrustedLedgerURL + " " + kUntrustedNftURL + " " +
          kUntrustedMarketURL + ";");
  source->AddString("shunyaWalletTrezorBridgeUrl", kUntrustedTrezorURL);
  source->AddString("shunyaWalletNftBridgeUrl", kUntrustedNftURL);
  source->AddString("shunyaWalletMarketUiBridgeUrl", kUntrustedMarketURL);
  source->AddBoolean(shunya_wallet::mojom::kP3ACountTestNetworksLoadTimeKey,
                     base::CommandLine::ForCurrentProcess()->HasSwitch(
                         shunya_wallet::mojom::kP3ACountTestNetworksSwitch));

  shunya_wallet::AddBlockchainTokenImageSource(profile);
}

AndroidWalletPageUI::~AndroidWalletPageUI() = default;

void AndroidWalletPageUI::BindInterface(
    mojo::PendingReceiver<shunya_wallet::mojom::PageHandlerFactory> receiver) {
  page_factory_receiver_.reset();
  page_factory_receiver_.Bind(std::move(receiver));
}

WEB_UI_CONTROLLER_TYPE_IMPL(AndroidWalletPageUI)

void AndroidWalletPageUI::CreatePageHandler(
    mojo::PendingRemote<shunya_wallet::mojom::Page> page,
    mojo::PendingReceiver<shunya_wallet::mojom::PageHandler> page_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::WalletHandler> wallet_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::JsonRpcService>
        json_rpc_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::BitcoinWalletService>
        bitcoin_rpc_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::SwapService>
        swap_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::AssetRatioService>
        asset_ratio_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::KeyringService>
        keyring_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::BlockchainRegistry>
        blockchain_registry_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::TxService> tx_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::EthTxManagerProxy>
        eth_tx_manager_proxy_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::SolanaTxManagerProxy>
        solana_tx_manager_proxy_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::FilTxManagerProxy>
        filecoin_tx_manager_proxy_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::ShunyaWalletService>
        shunya_wallet_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::ShunyaWalletP3A>
        shunya_wallet_p3a_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::WalletPinService>
        shunya_wallet_pin_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::WalletAutoPinService>
        shunya_wallet_auto_pin_service_receiver,
    mojo::PendingReceiver<shunya_wallet::mojom::IpfsService>
        ipfs_service_receiver) {
  DCHECK(page);
  auto* profile = Profile::FromWebUI(web_ui());
  DCHECK(profile);
  page_handler_ = std::make_unique<AndroidWalletPageHandler>(
      std::move(page_receiver), profile, this);
  wallet_handler_ = std::make_unique<shunya_wallet::WalletHandler>(
      std::move(wallet_receiver), profile);

  shunya_wallet::JsonRpcServiceFactory::BindForContext(
      profile, std::move(json_rpc_service_receiver));
  shunya_wallet::BitcoinWalletServiceFactory::BindForContext(
      profile, std::move(bitcoin_rpc_service_receiver));
  shunya_wallet::SwapServiceFactory::BindForContext(
      profile, std::move(swap_service_receiver));
  shunya_wallet::AssetRatioServiceFactory::BindForContext(
      profile, std::move(asset_ratio_service_receiver));
  shunya_wallet::KeyringServiceFactory::BindForContext(
      profile, std::move(keyring_service_receiver));
  shunya_wallet::TxServiceFactory::BindForContext(
      profile, std::move(tx_service_receiver));
  shunya_wallet::TxServiceFactory::BindEthTxManagerProxyForContext(
      profile, std::move(eth_tx_manager_proxy_receiver));
  shunya_wallet::TxServiceFactory::BindSolanaTxManagerProxyForContext(
      profile, std::move(solana_tx_manager_proxy_receiver));
  shunya_wallet::TxServiceFactory::BindFilTxManagerProxyForContext(
      profile, std::move(filecoin_tx_manager_proxy_receiver));
  shunya_wallet::ShunyaWalletIpfsServiceFactory::BindForContext(
      profile, std::move(ipfs_service_receiver));
  shunya_wallet::ShunyaWalletService* wallet_service =
      shunya_wallet::ShunyaWalletServiceFactory::GetServiceForContext(profile);
  wallet_service->Bind(std::move(shunya_wallet_service_receiver));
  wallet_service->GetShunyaWalletP3A()->Bind(
      std::move(shunya_wallet_p3a_receiver));

  auto* blockchain_registry = shunya_wallet::BlockchainRegistry::GetInstance();
  if (blockchain_registry) {
    blockchain_registry->Bind(std::move(blockchain_registry_receiver));
  }
  shunya_wallet::WalletInteractionDetected(web_ui()->GetWebContents());
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/shunya_vpn/shunya_vpn_localized_string_provider.h"

#include "base/no_destructor.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "components/grit/shunya_components_strings.h"
#include "content/public/browser/url_data_source.h"
#include "ui/base/webui/web_ui_util.h"

namespace shunya_vpn {

void AddLocalizedStrings(content::WebUIDataSource* html_source) {
  static constexpr webui::LocalizedString kLocalizedStrings[] = {
      {"shunyaVpn", IDS_SHUNYA_VPN},
      {"shunyaVpnConnect", IDS_SHUNYA_VPN_CONNECT},
      {"shunyaVpnConnecting", IDS_SHUNYA_VPN_CONNECTING},
      {"shunyaVpnConnected", IDS_SHUNYA_VPN_CONNECTED},
      {"shunyaVpnDisconnecting", IDS_SHUNYA_VPN_DISCONNECTING},
      {"shunyaVpnDisconnected", IDS_SHUNYA_VPN_DISCONNECTED},
      {"shunyaVpnConnectionFailed", IDS_SHUNYA_VPN_CONNECTION_FAILED},
      {"shunyaVpnUnableConnectToServer", IDS_SHUNYA_VPN_UNABLE_CONNECT_TO_SERVER},
      {"shunyaVpnTryAgain", IDS_SHUNYA_VPN_TRY_AGAIN},
      {"shunyaVpnChooseAnotherServer", IDS_SHUNYA_VPN_CHOOSE_ANOTHER_SERVER},
      {"shunyaVpnUnableConnectInfo", IDS_SHUNYA_VPN_UNABLE_CONNECT_INFO},
      {"shunyaVpnBuy", IDS_SHUNYA_VPN_BUY},
      {"shunyaVpnPurchased", IDS_SHUNYA_VPN_HAS_PURCHASED},
      {"shunyaVpnPoweredBy", IDS_SHUNYA_VPN_POWERED_BY},
      {"shunyaVpnSettingsPanelHeader", IDS_SHUNYA_VPN_SETTINGS_PANEL_HEADER},
      {"shunyaVpnErrorPanelHeader", IDS_SHUNYA_VPN_ERROR_PANEL_HEADER},
      {"shunyaVpnStatus", IDS_SHUNYA_VPN_STATUS},
      {"shunyaVpnExpires", IDS_SHUNYA_VPN_EXPIRES},
      {"shunyaVpnManageSubscription", IDS_SHUNYA_VPN_MANAGE_SUBSCRIPTION},
      {"shunyaVpnContactSupport", IDS_SHUNYA_VPN_CONTACT_SUPPORT},
      {"shunyaVpnAbout", IDS_SHUNYA_VPN_ABOUT},
      {"shunyaVpnFeature1", IDS_SHUNYA_VPN_FEATURE_1},
      {"shunyaVpnFeature2", IDS_SHUNYA_VPN_FEATURE_2},
      {"shunyaVpnFeature3", IDS_SHUNYA_VPN_FEATURE_3},
      {"shunyaVpnFeature4", IDS_SHUNYA_VPN_FEATURE_4},
      {"shunyaVpnFeature5", IDS_SHUNYA_VPN_FEATURE_5},
      {"shunyaVpnLoading", IDS_SHUNYA_VPN_LOADING},
      {"shunyaVpnPurchaseFailed", IDS_SHUNYA_VPN_PURCHASE_FAILED},
      {"shunyaVpnSupportTicketFailed", IDS_SHUNYA_VPN_SUPPORT_TICKET_FAILED},
      {"shunyaVpnEditPaymentMethod", IDS_SHUNYA_VPN_EDIT_PAYMENT},
      {"shunyaVpnPaymentFailure", IDS_SHUNYA_VPN_PAYMENT_FAILURE},
      {"shunyaVpnPaymentFailureReason", IDS_SHUNYA_VPN_PAYMENT_FAILURE_REASON},
      {"shunyaVpnSupportEmail", IDS_SHUNYA_VPN_SUPPORT_EMAIL},
      {"shunyaVpnSupportEmailNotValid", IDS_SHUNYA_VPN_SUPPORT_EMAIL_NOT_VALID},
      {"shunyaVpnSupportSubject", IDS_SHUNYA_VPN_SUPPORT_SUBJECT},
      {"shunyaVpnSupportSubjectNotSet", IDS_SHUNYA_VPN_SUPPORT_SUBJECT_NOTSET},
      {"shunyaVpnSupportSubjectOtherConnectionProblem",
       IDS_SHUNYA_VPN_SUPPORT_SUBJECT_OTHER_CONNECTION_PROBLEM},
      {"shunyaVpnSupportSubjectNoInternet",
       IDS_SHUNYA_VPN_SUPPORT_SUBJECT_NO_INTERNET},
      {"shunyaVpnSupportSubjectSlowConnection",
       IDS_SHUNYA_VPN_SUPPORT_SUBJECT_SLOW_CONNECTION},
      {"shunyaVpnSupportSubjectWebsiteDoesntWork",
       IDS_SHUNYA_VPN_SUPPORT_SUBJECT_WEBSITE_DOESNT_WORK},
      {"shunyaVpnSupportSubjectOther", IDS_SHUNYA_VPN_SUPPORT_SUBJECT_OTHER},
      {"shunyaVpnSupportBody", IDS_SHUNYA_VPN_SUPPORT_BODY},
      {"shunyaVpnSupportOptionalHeader", IDS_SHUNYA_VPN_SUPPORT_OPTIONAL_HEADER},
      {"shunyaVpnSupportOptionalNotes", IDS_SHUNYA_VPN_SUPPORT_OPTIONAL_NOTES},
      {"shunyaVpnSupportOptionalNotesPrivacyPolicy",
       IDS_SHUNYA_VPN_SUPPORT_OPTIONAL_NOTES_PRIVACY_POLICY},
      {"shunyaVpnSupportOptionalVpnHostname",
       IDS_SHUNYA_VPN_SUPPORT_OPTIONAL_VPN_HOSTNAME},
      {"shunyaVpnSupportOptionalAppVersion",
       IDS_SHUNYA_VPN_SUPPORT_OPTIONAL_APP_VERSION},
      {"shunyaVpnSupportOptionalOsVersion",
       IDS_SHUNYA_VPN_SUPPORT_OPTIONAL_OS_VERSION},
      {"shunyaVpnSupportNotes", IDS_SHUNYA_VPN_SUPPORT_NOTES},
      {"shunyaVpnSupportSubmit", IDS_SHUNYA_VPN_SUPPORT_SUBMIT},
      {"shunyaVpnConnectNotAllowed", IDS_SHUNYA_VPN_CONNECT_NOT_ALLOWED},
      {"shunyaVpnSupportTimezone", IDS_SHUNYA_VPN_SUPPORT_TIMEZONE},
      {"shunyaVpnSessionExpiredTitle",
       IDS_SHUNYA_VPN_MAIN_PANEL_SESSION_EXPIRED_PART_TITLE},
      {"shunyaVpnSettingsTooltip", IDS_SHUNYA_VPN_MAIN_PANEL_VPN_SETTINGS_TITLE},
      {"shunyaVpnSessionExpiredContent",
       IDS_SHUNYA_VPN_MAIN_PANEL_SESSION_EXPIRED_PART_CONTENT},
  };

  for (const auto& str : kLocalizedStrings) {
    std::u16string l10n_str =
        shunya_l10n::GetLocalizedResourceUTF16String(str.id);
    html_source->AddString(str.name, l10n_str);
  }
}

}  // namespace shunya_vpn

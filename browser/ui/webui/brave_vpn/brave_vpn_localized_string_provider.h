/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_VPN_SHUNYA_VPN_LOCALIZED_STRING_PROVIDER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_VPN_SHUNYA_VPN_LOCALIZED_STRING_PROVIDER_H_

namespace content {
class WebUIDataSource;
}
namespace shunya_vpn {

void AddLocalizedStrings(content::WebUIDataSource* html_source);

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_VPN_SHUNYA_VPN_LOCALIZED_STRING_PROVIDER_H_

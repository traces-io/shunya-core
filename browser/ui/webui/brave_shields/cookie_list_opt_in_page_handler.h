/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SHIELDS_COOKIE_LIST_OPT_IN_PAGE_HANDLER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SHIELDS_COOKIE_LIST_OPT_IN_PAGE_HANDLER_H_

#include "base/memory/raw_ptr.h"
#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_shields/common/cookie_list_opt_in.mojom.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/receiver.h"
#include "ui/webui/mojo_bubble_web_ui_controller.h"

class Profile;

class CookieListOptInPageHandler
    : public shunya_shields::mojom::CookieListOptInPageHandler {
 public:
  CookieListOptInPageHandler(
      mojo::PendingReceiver<shunya_shields::mojom::CookieListOptInPageHandler>
          receiver,
      base::WeakPtr<ui::MojoBubbleWebUIController::Embedder> embedder,
      Profile* profile);

  CookieListOptInPageHandler(const CookieListOptInPageHandler&) = delete;
  CookieListOptInPageHandler& operator=(const CookieListOptInPageHandler&) =
      delete;

  ~CookieListOptInPageHandler() override;

  // shunya_shields::mojom::CookieListOptInPageHandler:
  void ShowUI() override;
  void CloseUI() override;
  void EnableFilter() override;

  void OnUINoClicked() override;
  void OnUIYesClicked() override;

 private:
  mojo::Receiver<shunya_shields::mojom::CookieListOptInPageHandler> receiver_;
  base::WeakPtr<ui::MojoBubbleWebUIController::Embedder> embedder_;
  raw_ptr<Profile> profile_ = nullptr;
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SHIELDS_COOKIE_LIST_OPT_IN_PAGE_HANDLER_H_

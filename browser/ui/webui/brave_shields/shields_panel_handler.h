// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SHIELDS_SHIELDS_PANEL_HANDLER_H_
#define SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SHIELDS_SHIELDS_PANEL_HANDLER_H_

#include "shunya/components/shunya_shields/common/shunya_shields_panel.mojom.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/receiver.h"

namespace ui {
class MojoBubbleWebUIController;
}  // namespace ui

namespace content {
class WebUI;
}  // namespace content

class Profile;
class ShunyaBrowserWindow;

class ShieldsPanelHandler : public shunya_shields::mojom::PanelHandler {
 public:
  ShieldsPanelHandler(
      mojo::PendingReceiver<shunya_shields::mojom::PanelHandler> receiver,
      ui::MojoBubbleWebUIController* webui_controller,
      ShunyaBrowserWindow* shunya_browser_window,
      Profile* profile);

  ShieldsPanelHandler(const ShieldsPanelHandler&) = delete;
  ShieldsPanelHandler& operator=(const ShieldsPanelHandler&) = delete;
  ~ShieldsPanelHandler() override;

  // shunya_shields::mojom::PanelHandler:
  void ShowUI() override;
  void CloseUI() override;
  void GetPosition(GetPositionCallback callback) override;
  void SetAdvancedViewEnabled(bool is_enabled) override;
  void GetAdvancedViewEnabled(GetAdvancedViewEnabledCallback callback) override;

 private:
  mojo::Receiver<shunya_shields::mojom::PanelHandler> receiver_;
  raw_ptr<ui::MojoBubbleWebUIController> const webui_controller_;
  raw_ptr<ShunyaBrowserWindow> shunya_browser_window_ = nullptr;
  raw_ptr<Profile> profile_ = nullptr;
};

#endif  // SHUNYA_BROWSER_UI_WEBUI_SHUNYA_SHIELDS_SHIELDS_PANEL_HANDLER_H_

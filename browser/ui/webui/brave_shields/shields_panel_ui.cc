// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/webui/shunya_shields/shields_panel_ui.h"

#include <utility>

#include "shunya/browser/ui/shunya_browser_window.h"
#include "shunya/components/shunya_shields/browser/shunya_shields_util.h"
#include "shunya/components/shunya_shields/common/shunya_shield_localized_strings.h"
#include "shunya/components/shunya_shields/resources/panel/grit/shunya_shields_panel_generated_map.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/constants/webui_url_constants.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_finder.h"
#include "chrome/browser/ui/webui/favicon_source.h"
#include "chrome/browser/ui/webui/webui_util.h"
#include "components/favicon_base/favicon_url_parser.h"
#include "components/grit/shunya_components_resources.h"
#include "content/public/browser/web_ui.h"
#include "net/base/features.h"

// Cache active Browser instance's TabStripModel to give
// to ShieldsPanelDataHandler when this is created because
// CreatePanelHandler() is run in async.
ShieldsPanelUI::ShieldsPanelUI(content::WebUI* web_ui)
    : ui::MojoBubbleWebUIController(web_ui, true),
      profile_(Profile::FromWebUI(web_ui)) {
  browser_ = chrome::FindLastActiveWithProfile(profile_);

  content::WebUIDataSource* source = content::WebUIDataSource::CreateAndAdd(
      web_ui->GetWebContents()->GetBrowserContext(), kShieldsPanelHost);

  for (const auto& str : shunya_shields::kLocalizedStrings) {
    std::u16string l10n_str =
        shunya_l10n::GetLocalizedResourceUTF16String(str.id);
    source->AddString(str.name, l10n_str);
  }

  source->AddBoolean("isAdvancedViewEnabled", profile_->GetPrefs()->GetBoolean(
                                                  kShieldsAdvancedViewEnabled));

  source->AddBoolean("isHttpsByDefaultEnabled",
                     shunya_shields::IsHttpsByDefaultFeatureEnabled());

  source->AddBoolean("isTorProfile", profile_->IsTor());

  source->AddBoolean("isForgetFirstPartyStorageEnabled",
                     base::FeatureList::IsEnabled(
                         net::features::kShunyaForgetFirstPartyStorage));

  content::URLDataSource::Add(
      profile_, std::make_unique<FaviconSource>(
                    profile_, chrome::FaviconUrlFormat::kFavicon2));

  webui::SetupWebUIDataSource(source,
                              base::make_span(kShunyaShieldsPanelGenerated,
                                              kShunyaShieldsPanelGeneratedSize),
                              IDR_SHIELDS_PANEL_HTML);
}

ShieldsPanelUI::~ShieldsPanelUI() = default;

WEB_UI_CONTROLLER_TYPE_IMPL(ShieldsPanelUI)

void ShieldsPanelUI::BindInterface(
    mojo::PendingReceiver<shunya_shields::mojom::PanelHandlerFactory> receiver) {
  panel_factory_receiver_.reset();
  panel_factory_receiver_.Bind(std::move(receiver));
}

void ShieldsPanelUI::CreatePanelHandler(
    mojo::PendingReceiver<shunya_shields::mojom::PanelHandler> panel_receiver,
    mojo::PendingReceiver<shunya_shields::mojom::DataHandler>
        data_handler_receiver) {
  auto* profile = Profile::FromWebUI(web_ui());
  DCHECK(profile);

  panel_handler_ = std::make_unique<ShieldsPanelHandler>(
      std::move(panel_receiver), this,
      static_cast<ShunyaBrowserWindow*>(browser_->window()), profile);
  data_handler_ = std::make_unique<ShieldsPanelDataHandler>(
      std::move(data_handler_receiver), this, browser_->tab_strip_model());
}

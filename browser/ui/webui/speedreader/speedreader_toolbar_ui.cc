// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/webui/speedreader/speedreader_toolbar_ui.h"

#include <utility>

#include "shunya/browser/ui/webui/shunya_webui_source.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/constants/webui_url_constants.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "shunya/components/speedreader/common/constants.h"
#include "shunya/components/speedreader/common/features.h"
#include "shunya/components/speedreader/resources/panel/grit/shunya_speedreader_toolbar_generated_map.h"
#include "build/build_config.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser_finder.h"
#include "components/grit/shunya_components_resources.h"
#include "content/public/browser/host_zoom_map.h"
#include "content/public/browser/web_contents.h"
#include "content/public/browser/web_ui_data_source.h"
#include "content/public/common/url_constants.h"

#if BUILDFLAG(ENABLE_AI_CHAT)
#include "shunya/components/ai_chat/common/features.h"
#endif

SpeedreaderToolbarUI::SpeedreaderToolbarUI(content::WebUI* web_ui,
                                           const std::string& name)
    : ui::MojoBubbleWebUIController(web_ui, true),
      profile_(Profile::FromWebUI(web_ui)) {
  content::HostZoomMap::Get(web_ui->GetWebContents()->GetSiteInstance())
      ->SetZoomLevelForHostAndScheme(content::kChromeUIScheme,
                                     kSpeedreaderPanelHost, 0);

  browser_ = chrome::FindLastActiveWithProfile(profile_);

  content::WebUIDataSource* source = CreateAndAddWebUIDataSource(
      web_ui, name, kShunyaSpeedreaderToolbarGenerated,
      kShunyaSpeedreaderToolbarGeneratedSize, IDR_SPEEDREADER_UI_HTML);

  for (const auto& str : speedreader::kLocalizedStrings) {
    std::u16string l10n_str =
        shunya_l10n::GetLocalizedResourceUTF16String(str.id);
    source->AddString(str.name, l10n_str);
  }

#if BUILDFLAG(ENABLE_AI_CHAT)
  source->AddBoolean("aiChatFeatureEnabled",
                     ai_chat::features::IsAIChatEnabled());
#else
  source->AddBoolean("aiChatFeatureEnabled", false);
#endif
  source->AddBoolean("ttsEnabled",
                     speedreader::features::IsSpeedreaderEnabled() &&
                         speedreader::kSpeedreaderTTS.Get());
}

SpeedreaderToolbarUI::~SpeedreaderToolbarUI() = default;

WEB_UI_CONTROLLER_TYPE_IMPL(SpeedreaderToolbarUI)

void SpeedreaderToolbarUI::BindInterface(
    mojo::PendingReceiver<speedreader::mojom::ToolbarFactory> receiver) {
  toolbar_factory_.reset();
  toolbar_factory_.Bind(std::move(receiver));
}

void SpeedreaderToolbarUI::CreateInterfaces(
    mojo::PendingReceiver<speedreader::mojom::ToolbarDataHandler>
        toolbar_data_handler,
    mojo::PendingRemote<speedreader::mojom::ToolbarEventsHandler>
        toolbar_events_handler) {
  toolbar_data_handler_ = std::make_unique<SpeedreaderToolbarDataHandlerImpl>(
      browser_, std::move(toolbar_data_handler),
      std::move(toolbar_events_handler));
}

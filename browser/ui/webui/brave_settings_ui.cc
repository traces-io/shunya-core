/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/webui/shunya_settings_ui.h"

#include <memory>
#include <string>
#include <utility>

#include "base/feature_list.h"
#include "shunya/browser/shunya_rewards/rewards_util.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_context_utils.h"
#include "shunya/browser/ntp_background/view_counter_service_factory.h"
#include "shunya/browser/resources/settings/grit/shunya_settings_resources.h"
#include "shunya/browser/resources/settings/grit/shunya_settings_resources_map.h"
#include "shunya/browser/resources/settings/shortcuts_page/grit/commands_generated_map.h"
#include "shunya/browser/shell_integrations/buildflags/buildflags.h"
#include "shunya/browser/ui/commands/accelerator_service_factory.h"
#include "shunya/browser/ui/webui/navigation_bar_data_provider.h"
#include "shunya/browser/ui/webui/settings/shunya_adblock_handler.h"
#include "shunya/browser/ui/webui/settings/shunya_appearance_handler.h"
#include "shunya/browser/ui/webui/settings/shunya_default_extensions_handler.h"
#include "shunya/browser/ui/webui/settings/shunya_privacy_handler.h"
#include "shunya/browser/ui/webui/settings/shunya_sync_handler.h"
#include "shunya/browser/ui/webui/settings/shunya_wallet_handler.h"
#include "shunya/browser/ui/webui/settings/default_shunya_shields_handler.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/shunya_wallet/common/features.h"
#include "shunya/components/commands/common/commands.mojom.h"
#include "shunya/components/commands/common/features.h"
#include "shunya/components/ntp_background_images/browser/view_counter_service.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "shunya/components/version_info/version_info.h"
#include "build/build_config.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/webui/settings/metrics_reporting_handler.h"
#include "components/sync/base/command_line_switches.h"
#include "content/public/browser/web_contents.h"
#include "content/public/browser/web_ui_data_source.h"
#include "content/public/common/content_features.h"
#include "extensions/buildflags/buildflags.h"
#include "net/base/features.h"
#include "third_party/blink/public/common/features.h"

#if BUILDFLAG(ENABLE_PIN_SHORTCUT)
#include "shunya/browser/ui/webui/settings/pin_shortcut_handler.h"
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/components/speedreader/common/features.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/browser/shunya_vpn/vpn_utils.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#if BUILDFLAG(IS_WIN)
#include "shunya/browser/ui/webui/settings/shunya_vpn/shunya_vpn_handler.h"
#endif
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/browser/ui/webui/settings/shunya_tor_handler.h"
#endif

#if BUILDFLAG(ENABLE_EXTENSIONS)
#include "shunya/browser/ui/webui/settings/shunya_extensions_manifest_v2_handler.h"
#include "shunya/browser/ui/webui/settings/shunya_tor_snowflake_extension_handler.h"
#endif

#if BUILDFLAG(ENABLE_AI_CHAT)
#include "shunya/browser/ui/webui/settings/shunya_settings_leo_assistant_handler.h"
#include "shunya/components/ai_chat/common/features.h"
#endif

using ntp_background_images::ViewCounterServiceFactory;

ShunyaSettingsUI::ShunyaSettingsUI(content::WebUI* web_ui,
                                 const std::string& host)
    : SettingsUI(web_ui) {
  web_ui->AddMessageHandler(
      std::make_unique<settings::MetricsReportingHandler>());
  web_ui->AddMessageHandler(std::make_unique<ShunyaPrivacyHandler>());
  web_ui->AddMessageHandler(std::make_unique<DefaultShunyaShieldsHandler>());
  web_ui->AddMessageHandler(std::make_unique<ShunyaDefaultExtensionsHandler>());
  web_ui->AddMessageHandler(std::make_unique<ShunyaAppearanceHandler>());
  web_ui->AddMessageHandler(std::make_unique<ShunyaSyncHandler>());
  web_ui->AddMessageHandler(std::make_unique<ShunyaWalletHandler>());
  web_ui->AddMessageHandler(std::make_unique<ShunyaAdBlockHandler>());
#if BUILDFLAG(ENABLE_AI_CHAT)
  web_ui->AddMessageHandler(
      std::make_unique<settings::ShunyaLeoAssistantHandler>());
#endif
#if BUILDFLAG(ENABLE_TOR)
  web_ui->AddMessageHandler(std::make_unique<ShunyaTorHandler>());
#endif
#if BUILDFLAG(ENABLE_EXTENSIONS)
  web_ui->AddMessageHandler(
      std::make_unique<ShunyaTorSnowflakeExtensionHandler>());
  if (base::FeatureList::IsEnabled(kExtensionsManifestV2)) {
    web_ui->AddMessageHandler(
        std::make_unique<ShunyaExtensionsManifestV2Handler>());
  }
#endif
#if BUILDFLAG(ENABLE_PIN_SHORTCUT)
  web_ui->AddMessageHandler(std::make_unique<PinShortcutHandler>());
#endif
#if BUILDFLAG(IS_WIN) && BUILDFLAG(ENABLE_SHUNYA_VPN)
  if (shunya_vpn::IsShunyaVPNEnabled(Profile::FromWebUI(web_ui))) {
    web_ui->AddMessageHandler(
        std::make_unique<ShunyaVpnHandler>(Profile::FromWebUI(web_ui)));
  }
#endif
}

ShunyaSettingsUI::~ShunyaSettingsUI() = default;

// static
void ShunyaSettingsUI::AddResources(content::WebUIDataSource* html_source,
                                   Profile* profile) {
  for (size_t i = 0; i < kShunyaSettingsResourcesSize; ++i) {
    html_source->AddResourcePath(kShunyaSettingsResources[i].path,
                                 kShunyaSettingsResources[i].id);
  }

  // These resource files are generated from the files in
  // shunya/browser/resources/settings/shortcuts_page
  // They are generated separately so they can use React and our Leo
  // components, and the React DOM is mounted inside a Web Component, so it
  // doesn't interfere with the Polymer tree/styles.
  if (base::FeatureList::IsEnabled(commands::features::kShunyaCommands)) {
    for (size_t i = 0; i < kCommandsGeneratedSize; ++i) {
      html_source->AddResourcePath(kCommandsGenerated[i].path,
                                   kCommandsGenerated[i].id);
    }
  }

  html_source->AddBoolean("isSyncDisabled", !syncer::IsSyncAllowedByFlag());
  html_source->AddString(
      "shunyaProductVersion",
      version_info::GetShunyaVersionWithoutChromiumMajorVersion());
  NavigationBarDataProvider::Initialize(html_source, profile);
  if (auto* service = ViewCounterServiceFactory::GetForProfile(profile)) {
    service->InitializeWebUIDataSource(html_source);
  }
  html_source->AddBoolean(
      "isIdleDetectionFeatureEnabled",
      base::FeatureList::IsEnabled(features::kIdleDetection));
  html_source->AddBoolean(
      "isShunyaWebSerialApiEnabled",
      base::FeatureList::IsEnabled(blink::features::kShunyaWebSerialAPI));
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  html_source->AddBoolean("isShunyaVPNEnabled",
                          shunya_vpn::IsShunyaVPNEnabled(profile));
#endif
#if BUILDFLAG(ENABLE_SPEEDREADER)
  html_source->AddBoolean(
      "isSpeedreaderFeatureEnabled",
      base::FeatureList::IsEnabled(speedreader::kSpeedreaderFeature));
#endif
  html_source->AddBoolean(
      "isNativeShunyaWalletFeatureEnabled",
      base::FeatureList::IsEnabled(
          shunya_wallet::features::kNativeShunyaWalletFeature));
  html_source->AddBoolean("isShunyaWalletAllowed",
                          shunya_wallet::IsAllowedForContext(profile));
  html_source->AddBoolean("isForgetFirstPartyStorageFeatureEnabled",
                          base::FeatureList::IsEnabled(
                              net::features::kShunyaForgetFirstPartyStorage));
  html_source->AddBoolean("isShunyaRewardsSupported",
                          shunya_rewards::IsSupportedForProfile(profile));
  html_source->AddBoolean(
      "areShortcutsSupported",
      base::FeatureList::IsEnabled(commands::features::kShunyaCommands));

  if (ShouldDisableCSPForTesting()) {
    html_source->DisableContentSecurityPolicy();
  }

  html_source->AddBoolean("shouldExposeElementsForTesting",
                          ShouldExposeElementsForTesting());

  html_source->AddBoolean("enable_extensions", BUILDFLAG(ENABLE_EXTENSIONS));

  html_source->AddBoolean("extensionsManifestV2Feature",
                          base::FeatureList::IsEnabled(kExtensionsManifestV2));

#if BUILDFLAG(ENABLE_AI_CHAT)
  html_source->AddBoolean("isLeoAssistantAllowed",
                          ai_chat::features::IsAIChatEnabled());
  html_source->AddBoolean("isLeoAssistantHistoryAllowed",
                          ai_chat::features::IsAIChatHistoryEnabled());
#else
  html_source->AddBoolean("isLeoAssistantAllowed", false);
  html_source->AddBoolean("isLeoAssistantHistoryAllowed", false);
#endif
}

// static
bool& ShunyaSettingsUI::ShouldDisableCSPForTesting() {
  static bool disable_csp = false;
  return disable_csp;
}

// static
bool& ShunyaSettingsUI::ShouldExposeElementsForTesting() {
  static bool expose_elements = false;
  return expose_elements;
}

void ShunyaSettingsUI::BindInterface(
    mojo::PendingReceiver<commands::mojom::CommandsService> pending_receiver) {
  commands::AcceleratorServiceFactory::GetForContext(
      web_ui()->GetWebContents()->GetBrowserContext())
      ->BindInterface(std::move(pending_receiver));
}

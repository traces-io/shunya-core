/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_CONTENT_SETTINGS_SHUNYA_AUTOPLAY_CONTENT_SETTING_BUBBLE_MODEL_H_
#define SHUNYA_BROWSER_UI_CONTENT_SETTINGS_SHUNYA_AUTOPLAY_CONTENT_SETTING_BUBBLE_MODEL_H_

#include "chrome/browser/ui/content_settings/content_setting_bubble_model.h"

class Profile;

using content::WebContents;

class ShunyaAutoplayContentSettingBubbleModel
    : public ContentSettingSimpleBubbleModel {
 public:
  ShunyaAutoplayContentSettingBubbleModel(Delegate* delegate,
                                         WebContents* web_contents);
  ShunyaAutoplayContentSettingBubbleModel(
      const ShunyaAutoplayContentSettingBubbleModel&) = delete;
  ShunyaAutoplayContentSettingBubbleModel& operator=(
      const ShunyaAutoplayContentSettingBubbleModel&) = delete;
  ~ShunyaAutoplayContentSettingBubbleModel() override;

  // ContentSettingSimpleBubbleModel:
  void CommitChanges() override;

 protected:
  bool settings_changed() const;

 private:
  void SetTitle();
  void SetRadioGroup();
  void SetNarrowestContentSetting(ContentSetting setting);

  ContentSetting block_setting_;
};

#endif  // SHUNYA_BROWSER_UI_CONTENT_SETTINGS_SHUNYA_AUTOPLAY_CONTENT_SETTING_BUBBLE_MODEL_H_

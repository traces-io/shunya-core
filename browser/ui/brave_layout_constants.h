/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_SHUNYA_LAYOUT_CONSTANTS_H_
#define SHUNYA_BROWSER_UI_SHUNYA_LAYOUT_CONSTANTS_H_

#include "chrome/browser/ui/layout_constants.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

absl::optional<int> GetShunyaLayoutConstant(LayoutConstant constant);

#endif  // SHUNYA_BROWSER_UI_SHUNYA_LAYOUT_CONSTANTS_H_

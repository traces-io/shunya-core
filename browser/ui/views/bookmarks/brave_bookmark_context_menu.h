/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_BOOKMARKS_SHUNYA_BOOKMARK_CONTEXT_MENU_H_
#define SHUNYA_BROWSER_UI_VIEWS_BOOKMARKS_SHUNYA_BOOKMARK_CONTEXT_MENU_H_

#include <vector>

#include "chrome/browser/ui/views/bookmarks/bookmark_context_menu.h"

class ShunyaBookmarkContextMenu : public BookmarkContextMenu {
 public:
  // |browser| is used to open bookmarks as well as the bookmark manager, and
  // is NULL in tests.
  ShunyaBookmarkContextMenu(
      views::Widget* parent_widget,
      Browser* browser,
      Profile* profile,
      BookmarkLaunchLocation opened_from,
      const bookmarks::BookmarkNode* parent,
      const std::vector<const bookmarks::BookmarkNode*>& selection,
      bool close_on_remove);

  ShunyaBookmarkContextMenu(const ShunyaBookmarkContextMenu&) = delete;
  ShunyaBookmarkContextMenu& operator=(const ShunyaBookmarkContextMenu&) = delete;

  ~ShunyaBookmarkContextMenu() override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_BOOKMARKS_SHUNYA_BOOKMARK_CONTEXT_MENU_H_

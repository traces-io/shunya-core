/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_LABEL_BUTTON_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_LABEL_BUTTON_H_

#include <string>

#include "ui/views/controls/button/label_button.h"

namespace shunya_tooltips {

class ShunyaTooltipLabelButton : public views::LabelButton {
 public:
  // Creates a ShunyaTooltipLabelButton with pressed events sent to |callback|
  // and label |text|. |button_context| is a value from
  // views::style::TextContext and determines the appearance of |text|.
  explicit ShunyaTooltipLabelButton(
      PressedCallback callback = PressedCallback(),
      const std::u16string& text = std::u16string(),
      int button_context = views::style::CONTEXT_BUTTON);
  ~ShunyaTooltipLabelButton() override;

  ShunyaTooltipLabelButton(const ShunyaTooltipLabelButton&) = delete;
  ShunyaTooltipLabelButton& operator=(const ShunyaTooltipLabelButton&) = delete;

  // views::LabelButton:
  ui::Cursor GetCursor(const ui::MouseEvent& event) override;
};

}  // namespace shunya_tooltips

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_LABEL_BUTTON_H_

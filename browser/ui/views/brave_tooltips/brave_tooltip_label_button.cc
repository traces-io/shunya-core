/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/shunya_tooltips/shunya_tooltip_label_button.h"

#include "ui/base/cursor/cursor.h"

namespace shunya_tooltips {

ShunyaTooltipLabelButton::ShunyaTooltipLabelButton(PressedCallback callback,
                                                 const std::u16string& text,
                                                 int button_context)
    : LabelButton(callback, text, button_context) {}

ShunyaTooltipLabelButton::~ShunyaTooltipLabelButton() = default;

ui::Cursor ShunyaTooltipLabelButton::GetCursor(const ui::MouseEvent& event) {
  if (!GetEnabled())
    return ui::Cursor();
  return ui::mojom::CursorType::kHand;
}

}  // namespace shunya_tooltips

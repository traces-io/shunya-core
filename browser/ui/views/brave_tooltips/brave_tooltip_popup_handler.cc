/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/shunya_tooltips/shunya_tooltip_popup_handler.h"

#include <map>
#include <string>

#include "shunya/browser/ui/shunya_tooltips/shunya_tooltip.h"
#include "shunya/browser/ui/views/shunya_tooltips/shunya_tooltip_popup.h"

namespace {

std::map<std::string, shunya_tooltips::ShunyaTooltipPopup* /* NOT OWNED */>
    tooltip_popups_;

}  // namespace

namespace shunya_tooltips {

ShunyaTooltipPopupHandler::ShunyaTooltipPopupHandler() = default;

ShunyaTooltipPopupHandler::~ShunyaTooltipPopupHandler() = default;

// static
void ShunyaTooltipPopupHandler::Show(Profile* profile,
                                    std::unique_ptr<ShunyaTooltip> tooltip) {
  DCHECK(profile);
  DCHECK(tooltip);

  const std::string tooltip_id = tooltip->id();
  DCHECK(!tooltip_popups_[tooltip_id]);
  tooltip_popups_[tooltip_id] =
      new shunya_tooltips::ShunyaTooltipPopup(profile, std::move(tooltip));
}

// static
void ShunyaTooltipPopupHandler::Close(const std::string& tooltip_id) {
  DCHECK(!tooltip_id.empty());

  if (!tooltip_popups_[tooltip_id]) {
    return;
  }

  tooltip_popups_[tooltip_id]->Close(false);
}

// static
void ShunyaTooltipPopupHandler::Destroy(const std::string& tooltip_id) {
  DCHECK(!tooltip_id.empty());

  // Note: The pointed-to ShunyaTooltipPopup members are deallocated by their
  // containing Widgets
  tooltip_popups_.erase(tooltip_id);
}

}  // namespace shunya_tooltips

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_VIEW_H_

#include <string>

#include "base/memory/raw_ptr.h"
#include "shunya/browser/ui/shunya_tooltips/shunya_tooltip.h"
#include "shunya/browser/ui/views/shunya_tooltips/shunya_tooltip_label_button.h"
#include "ui/base/metadata/metadata_impl_macros.h"
#include "ui/views/view.h"

namespace gfx {
class Canvas;
class Point;
}  // namespace gfx

namespace views {
class ImageView;
class Label;
class LabelButton;
class View;
}  // namespace views

namespace shunya_tooltips {

class ShunyaTooltipPopup;

class ShunyaTooltipView : public views::View {
 public:
  METADATA_HEADER(ShunyaTooltipView);

  ShunyaTooltipView(ShunyaTooltipPopup* tooltip_popup,
                   const ShunyaTooltipAttributes& tooltip_attributes);
  ~ShunyaTooltipView() override;

  ShunyaTooltipView(const ShunyaTooltipView&) = delete;
  ShunyaTooltipView& operator=(const ShunyaTooltipView&) = delete;

  views::Button* ok_button_for_testing() const { return ok_button_; }
  views::Button* cancel_button_for_testing() const { return cancel_button_; }

  // views::InkDropHostView:
  void GetAccessibleNodeData(ui::AXNodeData* node_data) override;
  bool OnMousePressed(const ui::MouseEvent& event) override;
  bool OnMouseDragged(const ui::MouseEvent& event) override;
  void OnMouseReleased(const ui::MouseEvent& event) override;
  void OnDeviceScaleFactorChanged(float old_device_scale_factor,
                                  float new_device_scale_factor) override;
  void OnThemeChanged() override;

 private:
  void CreateView();

  void Close();

  views::View* CreateHeaderView();

  views::ImageView* CreateIconView();

  views::Label* CreateTitleLabel();

  views::View* CreateButtonView();

  ShunyaTooltipLabelButton* CreateOkButton();
  void OnOkButtonPressed();

  ShunyaTooltipLabelButton* CreateCancelButton();
  void OnCancelButtonPressed();

  views::View* CreateBodyView();
  views::Label* CreateBodyLabel();

  void UpdateTitleLabelColors();
  void UpdateBodyLabelColors();
  void UpdateOkButtonColors();
  void UpdateCancelButtonColors();

  raw_ptr<ShunyaTooltipPopup> tooltip_popup_;
  ShunyaTooltipAttributes tooltip_attributes_;

  gfx::Point initial_mouse_pressed_location_;
  bool is_dragging_ = false;

  bool is_closing_ = false;

  raw_ptr<views::Label> title_label_ = nullptr;
  raw_ptr<views::Label> body_label_ = nullptr;

  raw_ptr<views::LabelButton> ok_button_ = nullptr;
  raw_ptr<views::LabelButton> cancel_button_ = nullptr;

  std::u16string accessible_name_;
};

}  // namespace shunya_tooltips

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_VIEW_H_

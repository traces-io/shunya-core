/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_VIEWS_DELEGATE_LINUX_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_VIEWS_DELEGATE_LINUX_H_

#include "chrome/browser/ui/views/chrome_views_delegate.h"

class ShunyaViewsDelegateLinux : public ChromeViewsDelegate {
 public:
  ShunyaViewsDelegateLinux() = default;
  ShunyaViewsDelegateLinux(const ShunyaViewsDelegateLinux&) = delete;
  ShunyaViewsDelegateLinux& operator=(const ShunyaViewsDelegateLinux&) = delete;
  ~ShunyaViewsDelegateLinux() override = default;
 private:
  // ChromeViewsDelegate overrides:
  gfx::ImageSkia* GetDefaultWindowIcon() const override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_VIEWS_DELEGATE_LINUX_H_

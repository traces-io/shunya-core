// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/views/location_bar/shunya_news_location_view.h"

#include <memory>
#include <utility>
#include <vector>

#include "base/functional/bind.h"
#include "base/functional/callback_forward.h"
#include "shunya/browser/shunya_news/shunya_news_tab_helper.h"
#include "shunya/browser/ui/views/shunya_news/shunya_news_bubble_view.h"
#include "shunya/components/shunya_news/common/pref_names.h"
#include "shunya/components/vector_icons/vector_icons.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/views/page_action/page_action_icon_view.h"
#include "components/grit/shunya_components_strings.h"
#include "content/public/browser/web_contents.h"
#include "ui/base/l10n/l10n_util.h"
#include "ui/gfx/color_utils.h"
#include "ui/gfx/geometry/skia_conversions.h"
#include "ui/native_theme/native_theme.h"
#include "ui/views/bubble/bubble_dialog_delegate_view.h"

namespace {

constexpr SkColor kSubscribedLightColor = SkColorSetRGB(76, 84, 210);
constexpr SkColor kSubscribedDarkColor = SkColorSetRGB(115, 122, 222);

}  // namespace

ShunyaNewsLocationView::ShunyaNewsLocationView(
    Profile* profile,
    IconLabelBubbleView::Delegate* icon_label_bubble_delegate,
    PageActionIconView::Delegate* page_action_icon_delegate)
    : PageActionIconView(/*command_updater=*/nullptr,
                         /*command_id=*/0,
                         icon_label_bubble_delegate,
                         page_action_icon_delegate,
                         "ShunyaNewsFollow") {
  SetLabel(l10n_util::GetStringUTF16(IDS_SHUNYA_NEWS_ACTION_VIEW_TOOLTIP));

  should_show_.Init(shunya_news::prefs::kShouldShowToolbarButton,
                    profile->GetPrefs(),
                    base::BindRepeating(&ShunyaNewsLocationView::UpdateImpl,
                                        base::Unretained(this)));
  opted_in_.Init(shunya_news::prefs::kShunyaNewsOptedIn, profile->GetPrefs(),
                 base::BindRepeating(&ShunyaNewsLocationView::UpdateImpl,
                                     base::Unretained(this)));
  news_enabled_.Init(shunya_news::prefs::kNewTabPageShowToday,
                     profile->GetPrefs(),
                     base::BindRepeating(&ShunyaNewsLocationView::UpdateImpl,
                                         base::Unretained(this)));

  Update();
}

ShunyaNewsLocationView::~ShunyaNewsLocationView() = default;

views::BubbleDialogDelegate* ShunyaNewsLocationView::GetBubble() const {
  return bubble_view_;
}

void ShunyaNewsLocationView::UpdateImpl() {
  auto* contents = GetWebContents();
  ShunyaNewsTabHelper* tab_helper =
      contents ? ShunyaNewsTabHelper::FromWebContents(contents) : nullptr;

  // When the active tab changes, subscribe to notification when
  // it has found a feed.
  if (contents && tab_helper) {
    // Observe ShunyaNewsTabHelper for feed changes
    if (!page_feeds_observer_.IsObservingSource(tab_helper)) {
      page_feeds_observer_.Reset();
      page_feeds_observer_.Observe(tab_helper);
    }
    // Observe WebContentsObserver for WebContentsDestroyed
    if (web_contents() != contents) {
      Observe(contents);
    }
  } else {
    // Unobserve WebContentsObserver
    if (web_contents()) {
      Observe(nullptr);
    }
    // Unobserve ShunyaNewsTabHelper
    if (page_feeds_observer_.IsObserving()) {
      page_feeds_observer_.Reset();
    }
  }

  // Don't show the icon if preferences don't allow
  if (!tab_helper || !should_show_.GetValue() || !news_enabled_.GetValue() ||
      !opted_in_.GetValue()) {
    SetVisible(false);
    return;
  }

  // Verify observing ShunyaNewsTabHelper
  DCHECK(page_feeds_observer_.IsObservingSource(tab_helper));
  // Verify observing for WebContentsDestroyed
  DCHECK(web_contents());

  // Icon color changes if any feeds are being followed
  UpdateIconColor(tab_helper->IsSubscribed());

  // Don't show icon if there are no feeds
  const bool has_feeds = !tab_helper->GetAvailableFeeds().empty();
  const bool is_visible = has_feeds || IsBubbleShowing();
  SetVisible(is_visible);
}

void ShunyaNewsLocationView::WebContentsDestroyed() {
  page_feeds_observer_.Reset();
  Observe(nullptr);
}

const gfx::VectorIcon& ShunyaNewsLocationView::GetVectorIcon() const {
  return kLeoRssIcon;
}

std::u16string ShunyaNewsLocationView::GetTextForTooltipAndAccessibleName()
    const {
  return l10n_util::GetStringUTF16(IDS_SHUNYA_NEWS_ACTION_VIEW_TOOLTIP);
}

bool ShunyaNewsLocationView::ShouldShowLabel() const {
  return false;
}

void ShunyaNewsLocationView::OnAvailableFeedsChanged(
    const std::vector<ShunyaNewsTabHelper::FeedDetails>& feeds) {
  Update();
}

void ShunyaNewsLocationView::OnThemeChanged() {
  bool subscribed = false;
  if (auto* contents = GetWebContents()) {
    subscribed = ShunyaNewsTabHelper::FromWebContents(contents)->IsSubscribed();
  }
  UpdateIconColor(subscribed);
  PageActionIconView::OnThemeChanged();
}

void ShunyaNewsLocationView::OnExecuting(
    PageActionIconView::ExecuteSource execute_source) {
  // If the bubble is already open, do nothing.
  if (IsBubbleShowing()) {
    return;
  }

  auto* contents = GetWebContents();
  if (!contents) {
    return;
  }

  bubble_view_ = new ShunyaNewsBubbleView(this, contents);
  bubble_view_->SetCloseCallback(base::BindOnce(
      &ShunyaNewsLocationView::OnBubbleClosed, base::Unretained(this)));
  auto* bubble_widget =
      views::BubbleDialogDelegateView::CreateBubble(bubble_view_);
  bubble_widget->Show();
}

void ShunyaNewsLocationView::UpdateIconColor(bool subscribed) {
  SkColor icon_color;
  if (subscribed) {
    auto is_dark = GetNativeTheme()->GetPreferredColorScheme() ==
                   ui::NativeTheme::PreferredColorScheme::kDark;
    icon_color = is_dark ? kSubscribedDarkColor : kSubscribedLightColor;
  } else {
    icon_color = color_utils::DeriveDefaultIconColor(GetCurrentTextColor());
  }
  SetIconColor(icon_color);
}

void ShunyaNewsLocationView::OnBubbleClosed() {
  bubble_view_ = nullptr;
}

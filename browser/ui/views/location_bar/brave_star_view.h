/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_LOCATION_BAR_SHUNYA_STAR_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_LOCATION_BAR_SHUNYA_STAR_VIEW_H_

#include "chrome/browser/ui/views/location_bar/star_view.h"

class ShunyaStarView : public StarView {
 public:
  using StarView::StarView;

  ShunyaStarView(const ShunyaStarView&) = delete;
  ShunyaStarView& operator=(const ShunyaStarView&) = delete;

 protected:
  // views::View:
  void UpdateImpl() override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_LOCATION_BAR_SHUNYA_STAR_VIEW_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_LOCATION_BAR_SHUNYA_LOCATION_BAR_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_LOCATION_BAR_SHUNYA_LOCATION_BAR_VIEW_H_

#include <vector>

#include "base/gtest_prod_util.h"
#include "shunya/browser/ui/views/location_bar/shunya_news_location_view.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "chrome/browser/ui/views/location_bar/location_bar_view.h"

class ShunyaActionsContainer;
class ShunyaActionsContainerTest;
class PlaylistActionIconView;
class RewardsBrowserTest;
class SkPath;

#if BUILDFLAG(ENABLE_TOR)
class OnionLocationView;
#endif

#if BUILDFLAG(ENABLE_IPFS)
class IPFSLocationView;
#endif

namespace policy {
FORWARD_DECLARE_TEST(ShunyaRewardsPolicyTest, RewardsIconIsHidden);
}

// The purposes of this subclass are to:
// - Add the ShunyaActionsContainer to the location bar
class ShunyaLocationBarView : public LocationBarView {
 public:
  using LocationBarView::LocationBarView;

  ShunyaLocationBarView(const ShunyaLocationBarView&) = delete;
  ShunyaLocationBarView& operator=(const ShunyaLocationBarView&) = delete;

  void Init() override;
  void Update(content::WebContents* contents) override;
  void OnChanged() override;
  ShunyaActionsContainer* GetShunyaActionsContainer() { return shunya_actions_; }
#if BUILDFLAG(ENABLE_TOR)
  OnionLocationView* GetOnionLocationView() { return onion_location_view_; }
#endif

#if BUILDFLAG(ENABLE_IPFS)
  IPFSLocationView* GetIPFSLocationView() { return ipfs_location_view_; }
#endif
  // LocationBarView:
  std::vector<views::View*> GetTrailingViews() override;

  ui::ImageModel GetLocationIcon(LocationIconView::Delegate::IconFetchedCallback
                                     on_icon_fetched) const override;
  void OnOmniboxBlurred() override;

  // views::View:
  gfx::Size CalculatePreferredSize() const override;
  void OnThemeChanged() override;
  void ChildVisibilityChanged(views::View* child) override;

  int GetBorderRadius() const override;

  SkPath GetFocusRingHighlightPath() const;
  ContentSettingImageView* GetContentSettingsImageViewForTesting(size_t idx);
  bool ShouldShowIPFSLocationView() const;
  ShunyaActionsContainer* shunya_actions_contatiner_view() {
    return shunya_actions_;
  }

  void ShowPlaylistBubble();

 private:
  FRIEND_TEST_ALL_PREFIXES(policy::ShunyaRewardsPolicyTest, RewardsIconIsHidden);
  FRIEND_TEST_ALL_PREFIXES(PlaylistBrowserTest, AddItemsToList);
  friend class ::ShunyaActionsContainerTest;
  friend class ::RewardsBrowserTest;

  PlaylistActionIconView* GetPlaylistActionIconView();

  raw_ptr<ShunyaActionsContainer> shunya_actions_ = nullptr;
  raw_ptr<ShunyaNewsLocationView> shunya_news_location_view_ = nullptr;
#if BUILDFLAG(ENABLE_TOR)
  raw_ptr<OnionLocationView> onion_location_view_ = nullptr;
#endif
#if BUILDFLAG(ENABLE_IPFS)
  raw_ptr<IPFSLocationView> ipfs_location_view_ = nullptr;
#endif
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_LOCATION_BAR_SHUNYA_LOCATION_BAR_VIEW_H_

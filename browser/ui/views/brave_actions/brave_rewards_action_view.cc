// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/views/shunya_actions/shunya_rewards_action_view.h"

#include <memory>
#include <string>
#include <utility>

#include "base/strings/string_number_conversions.h"
#include "shunya/app/vector_icons/vector_icons.h"
#include "shunya/browser/shunya_rewards/rewards_service_factory.h"
#include "shunya/browser/ui/shunya_icon_with_badge_image_source.h"
#include "shunya/browser/ui/views/bubble/shunya_webui_bubble_manager.h"
#include "shunya/browser/ui/webui/shunya_rewards/rewards_panel_ui.h"
#include "shunya/components/shunya_rewards/browser/rewards_p3a.h"
#include "shunya/components/shunya_rewards/browser/rewards_service.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/constants/webui_url_constants.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "shunya/components/vector_icons/vector_icons.h"
#include "shunya/grit/shunya_generated_resources.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/tabs/tab_strip_model.h"
#include "chrome/browser/ui/views/chrome_layout_provider.h"
#include "chrome/browser/ui/views/frame/browser_view.h"
#include "chrome/browser/ui/views/location_bar/location_bar_view.h"
#include "components/grit/shunya_components_strings.h"
#include "components/prefs/pref_service.h"
#include "ui/base/models/simple_menu_model.h"
#include "ui/color/color_provider_manager.h"
#include "ui/gfx/canvas.h"
#include "ui/gfx/geometry/rect.h"
#include "ui/gfx/image/image.h"
#include "ui/gfx/image/image_skia.h"
#include "ui/gfx/paint_vector_icon.h"
#include "ui/gfx/skia_util.h"
#include "ui/views/animation/ink_drop_impl.h"
#include "ui/views/controls/button/label_button_border.h"
#include "ui/views/controls/button/menu_button_controller.h"
#include "ui/views/controls/highlight_path_generator.h"
#include "ui/views/view_class_properties.h"

namespace {

using shunya_rewards::RewardsNotificationService;
using shunya_rewards::RewardsPanelCoordinator;
using shunya_rewards::RewardsPanelUI;
using shunya_rewards::RewardsServiceFactory;
using shunya_rewards::RewardsTabHelper;

constexpr SkColor kIconColor = SK_ColorBLACK;
constexpr SkColor kBadgeVerifiedBG = SkColorSetRGB(0x42, 0x3E, 0xEE);

class ButtonHighlightPathGenerator : public views::HighlightPathGenerator {
 public:
  // views::HighlightPathGenerator:
  SkPath GetHighlightPath(const views::View* view) override {
    DCHECK(view);
    gfx::Rect rect(view->GetPreferredSize());
    rect.Inset(gfx::Insets::TLBR(0, 0, 0, -1 * kShunyaActionLeftMarginExtra));

    auto* layout_provider = ChromeLayoutProvider::Get();
    DCHECK(layout_provider);

    int radius = layout_provider->GetCornerRadiusMetric(
        views::Emphasis::kMaximum, rect.size());

    SkPath path;
    path.addRoundRect(gfx::RectToSkRect(rect), radius, radius);
    return path;
  }
};

const ui::ColorProvider* GetColorProviderForWebContents(
    base::WeakPtr<content::WebContents> web_contents) {
  if (web_contents) {
    return &web_contents->GetColorProvider();
  }

  return ui::ColorProviderManager::Get().GetColorProviderFor(
      ui::NativeTheme::GetInstanceForNativeUi()->GetColorProviderKey(nullptr));
}

// Draws a custom badge for the "verified" checkmark.
class RewardsBadgeImageSource : public shunya::ShunyaIconWithBadgeImageSource {
 public:
  RewardsBadgeImageSource(const gfx::Size& size,
                          GetColorProviderCallback get_color_provider_callback)
      : ShunyaIconWithBadgeImageSource(size,
                                      std::move(get_color_provider_callback),
                                      kShunyaActionGraphicSize,
                                      kShunyaActionLeftMarginExtra) {}

  void UseVerifiedIcon(bool verified_icon) {
    verified_icon_ = verified_icon;
    SetAllowEmptyText(verified_icon);
  }

 private:
  // shunya::ShunyaIconWithBadgeImageSource:
  void PaintBadgeWithoutText(const gfx::Rect& badge_rect,
                             gfx::Canvas* canvas) override {
    if (!verified_icon_) {
      ShunyaIconWithBadgeImageSource::PaintBadgeWithoutText(badge_rect, canvas);
      return;
    }

    // The verified icon must be drawn slightly larger than the default badge
    // area. Expand the badge rectangle accordingly.
    gfx::Rect image_rect(badge_rect);
    gfx::Outsets outsets;
    outsets.set_top(3);
    outsets.set_left(2);
    outsets.set_right(1);
    image_rect.Outset(outsets);

    gfx::RectF check_rect(image_rect);
    check_rect.Inset(4);
    cc::PaintFlags check_flags;
    check_flags.setStyle(cc::PaintFlags::kFill_Style);
    check_flags.setColor(SK_ColorWHITE);
    check_flags.setAntiAlias(true);
    canvas->DrawRoundRect(check_rect, 2, check_flags);

    auto image = gfx::CreateVectorIcon(kLeoVerificationFilledIcon,
                                       image_rect.width(), kBadgeVerifiedBG);

    cc::PaintFlags image_flags;
    image_flags.setStyle(cc::PaintFlags::kFill_Style);
    image_flags.setAntiAlias(true);
    canvas->DrawImageInt(image, image_rect.x(), image_rect.y(), image_flags);
  }

  bool verified_icon_ = false;
};

// Provides the context menu for the Rewards button.
class RewardsActionMenuModel : public ui::SimpleMenuModel,
                               public ui::SimpleMenuModel::Delegate {
 public:
  explicit RewardsActionMenuModel(PrefService* prefs)
      : SimpleMenuModel(this), prefs_(prefs) {
    Build();
  }

  ~RewardsActionMenuModel() override = default;
  RewardsActionMenuModel(const RewardsActionMenuModel&) = delete;
  RewardsActionMenuModel& operator=(const RewardsActionMenuModel&) = delete;

 private:
  enum ContextMenuCommand { kHideShunyaRewardsIcon };

  // ui::SimpleMenuModel::Delegate override:
  void ExecuteCommand(int command_id, int event_flags) override {
    if (command_id == kHideShunyaRewardsIcon) {
      prefs_->SetBoolean(shunya_rewards::prefs::kShowLocationBarButton, false);
    }
  }

  void Build() {
    AddItemWithStringId(kHideShunyaRewardsIcon,
                        IDS_HIDE_SHUNYA_REWARDS_ACTION_ICON);
  }

  raw_ptr<PrefService> prefs_ = nullptr;
};

}  // namespace

ShunyaRewardsActionView::ShunyaRewardsActionView(Browser* browser)
    : ToolbarButton(
          base::BindRepeating(&ShunyaRewardsActionView::OnButtonPressed,
                              base::Unretained(this)),
          std::make_unique<RewardsActionMenuModel>(
              browser->profile()->GetPrefs()),
          nullptr,
          false),
      browser_(browser),
      bubble_manager_(std::make_unique<ShunyaWebUIBubbleManager<RewardsPanelUI>>(
          this,
          browser_->profile(),
          GURL(kShunyaRewardsPanelURL),
          IDS_SHUNYA_UI_SHUNYA_REWARDS)) {
  DCHECK(browser_);

  SetButtonController(std::make_unique<views::MenuButtonController>(
      this,
      base::BindRepeating(&ShunyaRewardsActionView::OnButtonPressed,
                          base::Unretained(this)),
      std::make_unique<views::Button::DefaultButtonControllerDelegate>(this)));

  views::HighlightPathGenerator::Install(
      this, std::make_unique<ButtonHighlightPathGenerator>());

  // The highlight opacity set by |ToolbarButton| is different that the default
  // highlight opacity used by the other buttons in the actions container. Unset
  // the highlight opacity to match.
  views::InkDrop::Get(this)->SetHighlightOpacity({});

  SetHorizontalAlignment(gfx::ALIGN_CENTER);
  SetLayoutInsets(gfx::Insets(0));
  SetAccessibleName(
      shunya_l10n::GetLocalizedResourceUTF16String(IDS_SHUNYA_UI_SHUNYA_REWARDS));

  auto* profile = browser_->profile();

  pref_change_registrar_.Init(profile->GetPrefs());
  pref_change_registrar_.Add(
      shunya_rewards::prefs::kBadgeText,
      base::BindRepeating(&ShunyaRewardsActionView::OnPreferencesChanged,
                          base::Unretained(this)));
  pref_change_registrar_.Add(
      shunya_rewards::prefs::kDeclaredGeo,
      base::BindRepeating(&ShunyaRewardsActionView::OnPreferencesChanged,
                          base::Unretained(this)));

  browser_->tab_strip_model()->AddObserver(this);

  if (auto* rewards_service = GetRewardsService()) {
    rewards_service_observation_.Observe(rewards_service);
  }

  if (auto* notification_service = GetNotificationService()) {
    notification_service_observation_.Observe(notification_service);
  }

  panel_coordinator_ = RewardsPanelCoordinator::FromBrowser(browser_);
  if (panel_coordinator_) {
    panel_observation_.Observe(panel_coordinator_);
  }

  UpdateTabHelper(GetActiveWebContents());
}

ShunyaRewardsActionView::~ShunyaRewardsActionView() = default;

void ShunyaRewardsActionView::Update() {
  gfx::Size preferred_size = GetPreferredSize();
  auto* web_contents = GetActiveWebContents();
  auto weak_contents = web_contents ? web_contents->GetWeakPtr()
                                    : base::WeakPtr<content::WebContents>();

  auto image_source = std::make_unique<RewardsBadgeImageSource>(
      preferred_size,
      base::BindRepeating(GetColorProviderForWebContents, weak_contents));
  image_source->SetIcon(gfx::Image(GetRewardsIcon()));

  auto [text, background_color] = GetBadgeTextAndBackground();
  image_source->SetBadge(std::make_unique<IconWithBadgeImageSource::Badge>(
      text, shunya::kBadgeTextColor, background_color));
  image_source->UseVerifiedIcon(background_color == kBadgeVerifiedBG);

  SetImage(views::Button::STATE_NORMAL,
           gfx::ImageSkia(std::move(image_source), preferred_size));
}

void ShunyaRewardsActionView::ClosePanelForTesting() {
  if (IsPanelOpen()) {
    ToggleRewardsPanel();
  }
}

gfx::Rect ShunyaRewardsActionView::GetAnchorBoundsInScreen() const {
  if (!GetVisible()) {
    // If the button is currently hidden, then anchor the bubble to the
    // location bar instead.
    auto* browser_view = BrowserView::GetBrowserViewForBrowser(browser_);
    DCHECK(browser_view);
    return browser_view->GetLocationBarView()->GetAnchorBoundsInScreen();
  }
  return ToolbarButton::GetAnchorBoundsInScreen();
}

std::unique_ptr<views::LabelButtonBorder>
ShunyaRewardsActionView::CreateDefaultBorder() const {
  auto border = ToolbarButton::CreateDefaultBorder();
  border->set_insets(gfx::Insets::TLBR(0, 0, 0, 0));
  return border;
}

void ShunyaRewardsActionView::OnWidgetDestroying(views::Widget* widget) {
  DCHECK(bubble_observation_.IsObservingSource(widget));
  bubble_observation_.Reset();
}

void ShunyaRewardsActionView::OnTabStripModelChanged(
    TabStripModel* tab_strip_model,
    const TabStripModelChange& change,
    const TabStripSelectionChange& selection) {
  if (selection.active_tab_changed()) {
    UpdateTabHelper(selection.new_contents);
  }
}

void ShunyaRewardsActionView::OnPublisherForTabUpdated(
    const std::string& publisher_id) {
  publisher_registered_ = {publisher_id, false};
  bool status_updating = UpdatePublisherStatus();
  if (!status_updating) {
    Update();
  }
}

void ShunyaRewardsActionView::OnRewardsPanelRequested(
    const shunya_rewards::mojom::RewardsPanelArgs& args) {
  if (!IsPanelOpen()) {
    ToggleRewardsPanel();
  }
}

void ShunyaRewardsActionView::OnPublisherRegistryUpdated() {
  UpdatePublisherStatus();
}

void ShunyaRewardsActionView::OnPublisherUpdated(
    const std::string& publisher_id) {
  if (publisher_id == std::get<std::string>(publisher_registered_)) {
    UpdatePublisherStatus();
  }
}

void ShunyaRewardsActionView::OnNotificationAdded(
    RewardsNotificationService* service,
    const RewardsNotificationService::RewardsNotification& notification) {
  Update();
}

void ShunyaRewardsActionView::OnNotificationDeleted(
    RewardsNotificationService* service,
    const RewardsNotificationService::RewardsNotification& notification) {
  Update();
}

void ShunyaRewardsActionView::OnButtonPressed() {
  shunya_rewards::RewardsService* rewards_service = GetRewardsService();
  if (rewards_service != nullptr) {
    auto* prefs = browser_->profile()->GetPrefs();
    if (!prefs->GetBoolean(shunya_rewards::prefs::kEnabled)) {
      rewards_service->GetP3AConversionMonitor()->RecordPanelTrigger(
          ::shunya_rewards::p3a::PanelTrigger::kToolbarButton);
    }
  }
  // If we are opening the Rewards panel, use `RewardsPanelCoordinator` to open
  // it so that the panel arguments will be correctly set.
  if (!IsPanelOpen() && panel_coordinator_) {
    panel_coordinator_->OpenRewardsPanel();
    return;
  }

  ToggleRewardsPanel();
}

void ShunyaRewardsActionView::OnPreferencesChanged(const std::string& key) {
  Update();
}

content::WebContents* ShunyaRewardsActionView::GetActiveWebContents() {
  return browser_->tab_strip_model()->GetActiveWebContents();
}

shunya_rewards::RewardsService* ShunyaRewardsActionView::GetRewardsService() {
  return RewardsServiceFactory::GetForProfile(browser_->profile());
}

shunya_rewards::RewardsNotificationService*
ShunyaRewardsActionView::GetNotificationService() {
  if (auto* rewards_service = GetRewardsService()) {
    return rewards_service->GetNotificationService();
  }
  return nullptr;
}

bool ShunyaRewardsActionView::IsPanelOpen() {
  return bubble_observation_.IsObserving();
}

void ShunyaRewardsActionView::ToggleRewardsPanel() {
  if (IsPanelOpen()) {
    DCHECK(bubble_manager_);
    bubble_manager_->CloseBubble();
    return;
  }

  // Clear the default-on-start badge text when the user opens the panel.
  auto* prefs = browser_->profile()->GetPrefs();
  prefs->SetString(shunya_rewards::prefs::kBadgeText, "");

  bubble_manager_->ShowBubble();

  DCHECK(!bubble_observation_.IsObserving());
  bubble_observation_.Observe(bubble_manager_->GetBubbleWidget());
}

gfx::ImageSkia ShunyaRewardsActionView::GetRewardsIcon() {
  // Since the BAT icon has color the actual color value here is not relevant,
  // but |CreateVectorIcon| requires one.
  return gfx::CreateVectorIcon(kBatIcon, kShunyaActionGraphicSize, kIconColor);
}

std::pair<std::string, SkColor>
ShunyaRewardsActionView::GetBadgeTextAndBackground() {
  // 1. Display the default-on-start Rewards badge text, if specified.
  std::string text_pref = browser_->profile()->GetPrefs()->GetString(
      shunya_rewards::prefs::kBadgeText);
  if (!text_pref.empty()) {
    return {text_pref, shunya::kBadgeNotificationBG};
  }

  // 2. Display the number of current notifications, if non-zero.
  size_t notifications = GetRewardsNotificationCount();
  if (notifications > 0) {
    std::string text =
        notifications > 99 ? "99+" : base::NumberToString(notifications);

    return {text, shunya::kBadgeNotificationBG};
  }

  // 3. Display a verified checkmark for verified publishers.
  if (std::get<bool>(publisher_registered_)) {
    return {"", kBadgeVerifiedBG};
  }

  return {"", shunya::kBadgeNotificationBG};
}

size_t ShunyaRewardsActionView::GetRewardsNotificationCount() {
  size_t count = 0;

  if (auto* service = GetNotificationService()) {
    count += service->GetAllNotifications().size();
  }

  // Increment the notification count if the user has enabled Rewards but has
  // not declared a country.
  auto* prefs = browser_->profile()->GetPrefs();
  if (prefs->GetBoolean(shunya_rewards::prefs::kEnabled) &&
      prefs->GetString(shunya_rewards::prefs::kDeclaredGeo).empty()) {
    ++count;
  }

  return count;
}

bool ShunyaRewardsActionView::UpdatePublisherStatus() {
  std::string& publisher_id = std::get<std::string>(publisher_registered_);
  if (publisher_id.empty()) {
    return false;
  }

  auto* rewards_service = GetRewardsService();
  if (!rewards_service) {
    return false;
  }

  rewards_service->IsPublisherRegistered(
      publisher_id,
      base::BindOnce(&ShunyaRewardsActionView::IsPublisherRegisteredCallback,
                     weak_factory_.GetWeakPtr(), publisher_id));

  return true;
}

void ShunyaRewardsActionView::IsPublisherRegisteredCallback(
    const std::string& publisher_id,
    bool is_registered) {
  if (publisher_id == std::get<std::string>(publisher_registered_)) {
    publisher_registered_.second = is_registered;
    Update();
  }
}

void ShunyaRewardsActionView::UpdateTabHelper(
    content::WebContents* web_contents) {
  tab_helper_ = nullptr;
  if (tab_helper_observation_.IsObserving()) {
    tab_helper_observation_.Reset();
  }

  if (web_contents) {
    if (auto* helper = RewardsTabHelper::FromWebContents(web_contents)) {
      tab_helper_ = helper;
      tab_helper_observation_.Observe(helper);
    }
  }

  OnPublisherForTabUpdated(tab_helper_ ? tab_helper_->GetPublisherIdForTab()
                                       : "");
}

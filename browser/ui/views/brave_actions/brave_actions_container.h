/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ACTIONS_SHUNYA_ACTIONS_CONTAINER_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ACTIONS_SHUNYA_ACTIONS_CONTAINER_H_

#include <memory>

#include "base/gtest_prod_util.h"
#include "base/memory/raw_ptr.h"
#include "chrome/browser/ui/browser.h"
#include "components/prefs/pref_member.h"
#include "ui/gfx/skia_util.h"
#include "ui/views/view.h"

class ShunyaActionViewController;
class ShunyaActionsContainerTest;
class ShunyaRewardsActionView;
class ShunyaShieldsActionView;
class RewardsBrowserTest;

namespace policy {
FORWARD_DECLARE_TEST(ShunyaRewardsPolicyTest, RewardsIconIsHidden);
}

namespace views {
class Button;
}

// This View contains all the built-in ShunyaActions such as Shields and Payments
// TODO(petemill): consider splitting to separate model, like
// ToolbarActionsModel and ToolbarActionsBar
class ShunyaActionsContainer : public views::View {
 public:
  ShunyaActionsContainer(Browser* browser, Profile* profile);
  ShunyaActionsContainer(const ShunyaActionsContainer&) = delete;

  ShunyaActionsContainer& operator=(const ShunyaActionsContainer&) = delete;

  ~ShunyaActionsContainer() override;

  void Init();
  void Update();
  void SetShouldHide(bool should_hide);

  // views::View:
  void ChildPreferredSizeChanged(views::View* child) override;

  ShunyaShieldsActionView* GetShieldsActionView() { return shields_action_btn_; }

 private:
  FRIEND_TEST_ALL_PREFIXES(policy::ShunyaRewardsPolicyTest, RewardsIconIsHidden);
  friend class ::ShunyaActionsContainerTest;
  friend class ::RewardsBrowserTest;

  bool ShouldShowShunyaRewardsAction() const;
  void AddActionViewForRewards();
  void AddActionViewForShields();

  void UpdateVisibility();

  // Shunya Rewards preferences change observers callback.
  void OnShunyaRewardsPreferencesChanged();

  bool should_hide_ = false;

  // The Browser this LocationBarView is in.  Note that at least
  // chromeos::SimpleWebViewDialog uses a LocationBarView outside any browser
  // window, so this may be NULL.
  raw_ptr<Browser> browser_ = nullptr;

  raw_ptr<ShunyaShieldsActionView> shields_action_btn_ = nullptr;
  raw_ptr<ShunyaRewardsActionView> rewards_action_btn_ = nullptr;

  // Listen for Shunya Rewards preferences changes.
  BooleanPrefMember show_shunya_rewards_button_;

  base::WeakPtrFactory<ShunyaActionsContainer> weak_ptr_factory_{this};
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ACTIONS_SHUNYA_ACTIONS_CONTAINER_H_

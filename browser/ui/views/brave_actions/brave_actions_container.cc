/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/shunya_actions/shunya_actions_container.h"

#include <memory>
#include <utility>

#include "base/feature_list.h"
#include "shunya/browser/shunya_rewards/rewards_util.h"
#include "shunya/browser/ui/views/shunya_actions/shunya_rewards_action_view.h"
#include "shunya/browser/ui/views/shunya_actions/shunya_shields_action_view.h"
#include "shunya/browser/ui/views/rounded_separator.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/constants/pref_names.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/layout_constants.h"
#include "components/prefs/pref_service.h"
#include "ui/views/layout/box_layout.h"
#include "ui/views/view.h"

namespace {

constexpr gfx::Size kToolbarActionSize(34, 24);

}  // namespace

ShunyaActionsContainer::ShunyaActionsContainer(Browser* browser, Profile* profile)
    : browser_(browser) {}

ShunyaActionsContainer::~ShunyaActionsContainer() = default;

void ShunyaActionsContainer::Init() {
  // automatic layout
  auto vertical_container_layout = std::make_unique<views::BoxLayout>(
      views::BoxLayout::Orientation::kHorizontal);
  vertical_container_layout->set_main_axis_alignment(
      views::BoxLayout::MainAxisAlignment::kCenter);
  vertical_container_layout->set_cross_axis_alignment(
      views::BoxLayout::CrossAxisAlignment::kCenter);
  SetLayoutManager(std::move(vertical_container_layout));

  // children
  RoundedSeparator* shunya_button_separator_ = new RoundedSeparator();
  // TODO(petemill): theme color
  shunya_button_separator_->SetColor(SkColorSetRGB(0xb2, 0xb5, 0xb7));
  constexpr int kSeparatorMargin = 3;
  constexpr int kSeparatorWidth = 1;
  shunya_button_separator_->SetPreferredSize(
      gfx::Size(kSeparatorWidth + kSeparatorMargin * 2,
                GetLayoutConstant(LOCATION_BAR_ICON_SIZE)));
  // separator left & right margin
  shunya_button_separator_->SetBorder(views::CreateEmptyBorder(
      gfx::Insets::TLBR(0, kSeparatorMargin, 0, kSeparatorMargin)));
  // Just in case the extensions load before this function does (not likely!)
  // make sure separator is at index 0
  AddChildViewAt(shunya_button_separator_, 0);
  AddActionViewForShields();
  AddActionViewForRewards();

  // React to Shunya Rewards preferences changes.
  show_shunya_rewards_button_.Init(
      shunya_rewards::prefs::kShowLocationBarButton,
      browser_->profile()->GetPrefs(),
      base::BindRepeating(
          &ShunyaActionsContainer::OnShunyaRewardsPreferencesChanged,
          base::Unretained(this)));
}

bool ShunyaActionsContainer::ShouldShowShunyaRewardsAction() const {
  if (!shunya_rewards::IsSupportedForProfile(browser_->profile())) {
    return false;
  }
  const PrefService* prefs = browser_->profile()->GetPrefs();
  return prefs->GetBoolean(shunya_rewards::prefs::kShowLocationBarButton);
}

void ShunyaActionsContainer::AddActionViewForShields() {
  shields_action_btn_ =
      AddChildViewAt(std::make_unique<ShunyaShieldsActionView>(
                         *browser_->profile(), *browser_->tab_strip_model()),
                     1);
  shields_action_btn_->SetPreferredSize(kToolbarActionSize);
  shields_action_btn_->Init();
}

void ShunyaActionsContainer::AddActionViewForRewards() {
  auto button = std::make_unique<ShunyaRewardsActionView>(browser_);
  rewards_action_btn_ = AddChildViewAt(std::move(button), 2);
  rewards_action_btn_->SetPreferredSize(kToolbarActionSize);
  rewards_action_btn_->SetVisible(ShouldShowShunyaRewardsAction());
  rewards_action_btn_->Update();
}

void ShunyaActionsContainer::Update() {
  if (shields_action_btn_) {
    shields_action_btn_->Update();
  }

  if (rewards_action_btn_) {
    rewards_action_btn_->Update();
  }

  UpdateVisibility();
  Layout();
}

void ShunyaActionsContainer::UpdateVisibility() {
  bool can_show = false;

  if (shields_action_btn_) {
    can_show = shields_action_btn_->GetVisible();
  }

  if (rewards_action_btn_) {
    can_show = can_show || rewards_action_btn_->GetVisible();
  }

  // If no buttons are visible, then we want to hide this view so that the
  // separator is not displayed.
  SetVisible(!should_hide_ && can_show);
}

void ShunyaActionsContainer::SetShouldHide(bool should_hide) {
  should_hide_ = should_hide;
  Update();
}

void ShunyaActionsContainer::ChildPreferredSizeChanged(views::View* child) {
  PreferredSizeChanged();
}

// Shunya Rewards preferences change observers callback
void ShunyaActionsContainer::OnShunyaRewardsPreferencesChanged() {
  if (rewards_action_btn_) {
    rewards_action_btn_->SetVisible(ShouldShowShunyaRewardsAction());
  }
}

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_FRAME_SHUNYA_BROWSER_FRAME_VIEW_WIN_H_
#define SHUNYA_BROWSER_UI_VIEWS_FRAME_SHUNYA_BROWSER_FRAME_VIEW_WIN_H_

#include <memory>

#include "chrome/browser/ui/views/frame/browser_frame_view_win.h"
#include "components/prefs/pref_member.h"

class ShunyaWindowFrameGraphic;

class ShunyaBrowserFrameViewWin : public BrowserFrameViewWin {
 public:
  ShunyaBrowserFrameViewWin(BrowserFrame* frame, BrowserView* browser_view);
  ~ShunyaBrowserFrameViewWin() override;

  ShunyaBrowserFrameViewWin(const ShunyaBrowserFrameViewWin&) = delete;
  ShunyaBrowserFrameViewWin& operator=(const ShunyaBrowserFrameViewWin&) = delete;

 private:
  void OnVerticalTabsPrefsChanged();

  // ShunyaBrowserFrameViewWin overrides:
  void OnPaint(gfx::Canvas* canvas) override;
  int GetTopInset(bool restored) const override;
  int NonClientHitTest(const gfx::Point& point) override;

  std::unique_ptr<ShunyaWindowFrameGraphic> frame_graphic_;

  BooleanPrefMember using_vertical_tabs_;
  BooleanPrefMember showing_window_title_for_vertical_tabs_;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_FRAME_SHUNYA_BROWSER_FRAME_VIEW_WIN_H_

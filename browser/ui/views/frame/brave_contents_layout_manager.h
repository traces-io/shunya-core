/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_FRAME_SHUNYA_CONTENTS_LAYOUT_MANAGER_H_
#define SHUNYA_BROWSER_UI_VIEWS_FRAME_SHUNYA_CONTENTS_LAYOUT_MANAGER_H_

#include "base/memory/raw_ptr.h"
#include "chrome/browser/ui/views/frame/contents_layout_manager.h"

class ShunyaContentsLayoutManager : public ContentsLayoutManager {
 public:
  ShunyaContentsLayoutManager(views::View* devtools_view,
                             views::View* contents_view,
                             views::View* sidebar_container_view);
  ShunyaContentsLayoutManager(const ShunyaContentsLayoutManager&) = delete;
  ShunyaContentsLayoutManager& operator=(const ShunyaContentsLayoutManager&) =
      delete;
  ~ShunyaContentsLayoutManager() override;

  void set_sidebar_on_left(bool sidebar_on_left) {
    sidebar_on_left_ = sidebar_on_left;
  }

  void set_reader_mode_toolbar(views::View* reader_mode_toolbar_view) {
    reader_mode_toolbar_view_ = reader_mode_toolbar_view;
  }

  int CalculateTargetSideBarWidth() const;

  // ContentsLayoutManager overrides:
  void Layout(views::View* contents_container) override;

 private:
  raw_ptr<views::View> sidebar_container_view_ = nullptr;
  raw_ptr<views::View> reader_mode_toolbar_view_ = nullptr;
  bool sidebar_on_left_ = true;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_FRAME_SHUNYA_CONTENTS_LAYOUT_MANAGER_H_

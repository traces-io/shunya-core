/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/renderer_context_menu/shunya_render_view_context_menu_views.h"

ShunyaRenderViewContextMenuViews::ShunyaRenderViewContextMenuViews(
    content::RenderFrameHost& render_frame_host,
    const content::ContextMenuParams& params)
    : RenderViewContextMenuViews(render_frame_host, params) {}

ShunyaRenderViewContextMenuViews::~ShunyaRenderViewContextMenuViews() = default;

// static
RenderViewContextMenuViews* ShunyaRenderViewContextMenuViews::Create(
    content::RenderFrameHost& render_frame_host,
    const content::ContextMenuParams& params) {
  return new ShunyaRenderViewContextMenuViews(render_frame_host, params);
}

void ShunyaRenderViewContextMenuViews::Show() {
  // Removes duplicated separator if any. The duplicated separator may appear
  // in |ShunyaRenderViewContextMenu::InitMenu| after remove the translate menu
  // item.
  RemoveAdjacentSeparators();
  RenderViewContextMenuViews::Show();
}

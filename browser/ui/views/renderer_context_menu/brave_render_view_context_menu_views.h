/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_RENDERER_CONTEXT_MENU_SHUNYA_RENDER_VIEW_CONTEXT_MENU_VIEWS_H_
#define SHUNYA_BROWSER_UI_VIEWS_RENDERER_CONTEXT_MENU_SHUNYA_RENDER_VIEW_CONTEXT_MENU_VIEWS_H_

#include "chrome/browser/ui/views/renderer_context_menu/render_view_context_menu_views.h"

class ShunyaRenderViewContextMenuViews : public RenderViewContextMenuViews {
 public:
  ~ShunyaRenderViewContextMenuViews() override;
  ShunyaRenderViewContextMenuViews(const ShunyaRenderViewContextMenuViews&) =
      delete;
  ShunyaRenderViewContextMenuViews& operator=(
      const ShunyaRenderViewContextMenuViews&) = delete;

  // Factory function to create an instance.
  static RenderViewContextMenuViews* Create(
      // Non-const reference passed in the parent class upstream
      // NOLINTNEXTLINE(runtime/references)
      content::RenderFrameHost& render_frame_host,
      const content::ContextMenuParams& params);

  void Show() override;

 protected:
  // Non-const reference passed in the parent class upstream
  // NOLINTNEXTLINE(runtime/references)
  ShunyaRenderViewContextMenuViews(content::RenderFrameHost& render_frame_host,
                                  const content::ContextMenuParams& params);
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_RENDERER_CONTEXT_MENU_SHUNYA_RENDER_VIEW_CONTEXT_MENU_VIEWS_H_

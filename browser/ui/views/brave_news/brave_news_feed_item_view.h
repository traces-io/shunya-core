// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_NEWS_SHUNYA_NEWS_FEED_ITEM_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_NEWS_SHUNYA_NEWS_FEED_ITEM_VIEW_H_

#include <vector>

#include "shunya/browser/shunya_news/shunya_news_tab_helper.h"
#include "ui/views/view.h"

namespace content {
class WebContents;
}

namespace views {
class MdTextButton;
}

class ShunyaNewsFeedItemView : public views::View,
                              public ShunyaNewsTabHelper::PageFeedsObserver {
 public:
  METADATA_HEADER(ShunyaNewsFeedItemView);

  ShunyaNewsFeedItemView(ShunyaNewsTabHelper::FeedDetails details,
                        content::WebContents* contents);
  ShunyaNewsFeedItemView(const ShunyaNewsFeedItemView&) = delete;
  ShunyaNewsFeedItemView& operator=(const ShunyaNewsFeedItemView&) = delete;
  ~ShunyaNewsFeedItemView() override;

  void Update();
  void OnPressed();

  // ShunyaNewsTabHelper::PageFeedsObserver:
  void OnAvailableFeedsChanged(
      const std::vector<ShunyaNewsTabHelper::FeedDetails>& feeds) override;

 private:
  bool loading_ = false;
  raw_ptr<views::MdTextButton> subscribe_button_ = nullptr;

  ShunyaNewsTabHelper::FeedDetails feed_details_;
  raw_ptr<content::WebContents> contents_;
  raw_ptr<ShunyaNewsTabHelper> tab_helper_;

  base::ScopedObservation<ShunyaNewsTabHelper,
                          ShunyaNewsTabHelper::PageFeedsObserver>
      tab_helper_observation_{this};
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_NEWS_SHUNYA_NEWS_FEED_ITEM_VIEW_H_

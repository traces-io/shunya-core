// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_NEWS_SHUNYA_NEWS_FEEDS_CONTAINER_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_NEWS_SHUNYA_NEWS_FEEDS_CONTAINER_VIEW_H_

#include "ui/base/metadata/metadata_header_macros.h"
#include "ui/gfx/geometry/size.h"
#include "ui/views/layout/layout_types.h"
#include "ui/views/view.h"

namespace content {
class WebContents;
}

class ShunyaNewsFeedsContainerView : public views::View {
 public:
  METADATA_HEADER(ShunyaNewsFeedsContainerView);

  explicit ShunyaNewsFeedsContainerView(content::WebContents* contents);
  ShunyaNewsFeedsContainerView(const ShunyaNewsFeedsContainerView&) = delete;
  ShunyaNewsFeedsContainerView& operator=(const ShunyaNewsFeedsContainerView&) =
      delete;
  ~ShunyaNewsFeedsContainerView() override;

  // views::View
  void OnThemeChanged() override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_NEWS_SHUNYA_NEWS_FEEDS_CONTAINER_VIEW_H_

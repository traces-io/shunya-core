/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * you can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_TABS_SHUNYA_TAB_SEARCH_BUTTON_H_
#define SHUNYA_BROWSER_UI_VIEWS_TABS_SHUNYA_TAB_SEARCH_BUTTON_H_

#include "chrome/browser/ui/views/tabs/tab_search_button.h"
#include "third_party/skia/include/core/SkPath.h"
#include "ui/gfx/geometry/size.h"

class ShunyaTabSearchButton : public TabSearchButton {
 public:
  METADATA_HEADER(ShunyaTabSearchButton);

  explicit ShunyaTabSearchButton(TabStrip* tab_strip);
  ~ShunyaTabSearchButton() override;
  ShunyaTabSearchButton(const ShunyaTabSearchButton&) = delete;
  ShunyaTabSearchButton& operator=(const ShunyaTabSearchButton&) = delete;

  void SetBubbleArrow(views::BubbleBorder::Arrow arrow);

  // TabSearchButton overrides:
  gfx::Size CalculatePreferredSize() const override;
  int GetCornerRadius() const override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_TABS_SHUNYA_TAB_SEARCH_BUTTON_H_

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/views/tabs/shunya_tab.h"
#include "chrome/browser/ui/views/tabs/fake_tab_slot_controller.h"
#include "chrome/test/views/chrome_views_test_base.h"
#include "testing/gtest/include/gtest/gtest.h"
#include "ui/gfx/geometry/insets.h"
#include "ui/gfx/geometry/rect.h"
#include "ui/views/test/views_test_utils.h"

class ShunyaTabTest : public ChromeViewsTestBase {
 public:
  ShunyaTabTest() = default;
  ~ShunyaTabTest() override = default;

  void LayoutAndCheckBorder(ShunyaTab* tab,
                            const gfx::Rect& bounds,
                            bool gave_extra_padding) {
    tab->SetBoundsRect(bounds);
    views::test::RunScheduledLayout(tab);

    auto insets = tab->tab_style_views()->GetContentsInsets();
    int left_inset = insets.left();
    if (gave_extra_padding) {
      left_inset += ShunyaTab::kExtraLeftPadding;
    }
    EXPECT_EQ(left_inset, tab->GetInsets().left());
  }
};

TEST_F(ShunyaTabTest, ExtraPaddingLayoutTest) {
  FakeTabSlotController tab_slot_controller;
  ShunyaTab tab(&tab_slot_controller);

  // Smaller width tab will be given extra padding.
  LayoutAndCheckBorder(&tab, {0, 0, 30, 50}, true);
  LayoutAndCheckBorder(&tab, {0, 0, 50, 50}, true);
  LayoutAndCheckBorder(&tab, {0, 0, 100, 50}, false);
  LayoutAndCheckBorder(&tab, {0, 0, 150, 50}, false);
  LayoutAndCheckBorder(&tab, {0, 0, 30, 50}, true);
}

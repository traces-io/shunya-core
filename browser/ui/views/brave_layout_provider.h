/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_LAYOUT_PROVIDER_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_LAYOUT_PROVIDER_H_

#include "chrome/browser/ui/views/chrome_layout_provider.h"

class ShunyaLayoutProvider : public ChromeLayoutProvider {
 public:
  ShunyaLayoutProvider() = default;
  ShunyaLayoutProvider(const ShunyaLayoutProvider&) = delete;
  ShunyaLayoutProvider& operator=(const ShunyaLayoutProvider&) = delete;
  ~ShunyaLayoutProvider() override = default;

  int GetCornerRadiusMetric(views::Emphasis emphasis,
                            const gfx::Size& size = gfx::Size()) const override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_LAYOUT_PROVIDER_H_

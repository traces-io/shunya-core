/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SIDE_PANEL_SHUNYA_READ_LATER_SIDE_PANEL_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_SIDE_PANEL_SHUNYA_READ_LATER_SIDE_PANEL_VIEW_H_

#include "base/functional/callback_forward.h"
#include "base/scoped_observation.h"
#include "ui/views/view.h"
#include "ui/views/view_observer.h"

class Browser;

// Gives reading list specific header view with web view.
class ShunyaReadLaterSidePanelView : public views::View,
                                    public views::ViewObserver {
 public:
  ShunyaReadLaterSidePanelView(Browser* browser,
                              base::RepeatingClosure close_cb);
  ~ShunyaReadLaterSidePanelView() override;
  ShunyaReadLaterSidePanelView(const ShunyaReadLaterSidePanelView&) = delete;
  ShunyaReadLaterSidePanelView& operator=(const ShunyaReadLaterSidePanelView&) =
      delete;

 private:
  void OnViewVisibilityChanged(views::View* observed_view,
                               views::View* starting_view) override;

  base::ScopedObservation<views::View, views::ViewObserver> observation_{this};
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_SIDE_PANEL_SHUNYA_READ_LATER_SIDE_PANEL_VIEW_H_

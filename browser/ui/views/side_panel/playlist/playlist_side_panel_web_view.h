/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SIDE_PANEL_PLAYLIST_PLAYLIST_SIDE_PANEL_WEB_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_SIDE_PANEL_PLAYLIST_PLAYLIST_SIDE_PANEL_WEB_VIEW_H_

class Browser;
class BubbleContentsWrapper;

#include "base/functional/callback_forward.h"
#include "shunya/browser/ui/webui/playlist_ui.h"
#include "chrome/browser/ui/views/side_panel/side_panel_web_ui_view.h"

class PlaylistSidePanelWebView : public SidePanelWebUIView {
 public:
  PlaylistSidePanelWebView(Browser* browser,
                           base::RepeatingClosure close_cb,
                           BubbleContentsWrapper* contents_wrapper);
  PlaylistSidePanelWebView(const PlaylistSidePanelWebView&) = delete;
  PlaylistSidePanelWebView& operator=(const PlaylistSidePanelWebView&) = delete;
  ~PlaylistSidePanelWebView() override;

  base::WeakPtr<PlaylistSidePanelWebView> GetWeakPtr();

 private:
  base::WeakPtrFactory<PlaylistSidePanelWebView> weak_ptr_factory_{this};
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_SIDE_PANEL_PLAYLIST_PLAYLIST_SIDE_PANEL_WEB_VIEW_H_

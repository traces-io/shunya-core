/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_VPN_SHUNYA_VPN_DNS_SETTINGS_NOTIFICIATION_DIALOG_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_VPN_SHUNYA_VPN_DNS_SETTINGS_NOTIFICIATION_DIALOG_VIEW_H_

#include "ui/views/window/dialog_delegate.h"

class Browser;
class PrefService;

namespace views {
class Checkbox;
}

namespace shunya_vpn {

class ShunyaVpnDnsSettingsNotificiationDialogView
    : public views::DialogDelegateView {
 public:
  METADATA_HEADER(ShunyaVpnDnsSettingsNotificiationDialogView);

  static void Show(Browser* browser);

  ShunyaVpnDnsSettingsNotificiationDialogView(
      const ShunyaVpnDnsSettingsNotificiationDialogView&) = delete;
  ShunyaVpnDnsSettingsNotificiationDialogView& operator=(
      const ShunyaVpnDnsSettingsNotificiationDialogView&) = delete;

 private:
  explicit ShunyaVpnDnsSettingsNotificiationDialogView(Browser* browser);
  ~ShunyaVpnDnsSettingsNotificiationDialogView() override;

  void OnAccept();
  void OnClosing();

  void OnLearnMoreLinkClicked();

  // views::DialogDelegate overrides:
  ui::ModalType GetModalType() const override;
  bool ShouldShowCloseButton() const override;
  bool ShouldShowWindowTitle() const override;

  bool close_window_ = true;
  raw_ptr<Browser> browser_ = nullptr;
  raw_ptr<PrefService> prefs_ = nullptr;
  raw_ptr<views::Checkbox> dont_ask_again_checkbox_ = nullptr;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_VPN_SHUNYA_VPN_DNS_SETTINGS_NOTIFICIATION_DIALOG_VIEW_H_

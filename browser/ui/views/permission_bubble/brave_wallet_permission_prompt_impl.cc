/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/permission_bubble/shunya_wallet_permission_prompt_impl.h"

#include "shunya/browser/shunya_wallet/shunya_wallet_tab_helper.h"
#include "components/permissions/permission_uma_util.h"

ShunyaWalletPermissionPromptImpl::ShunyaWalletPermissionPromptImpl(
    Browser* browser,
    content::WebContents* web_contents,
    Delegate& delegate)
    : web_contents_(web_contents),
      delegate_(delegate),
      permission_requested_time_(base::TimeTicks::Now()) {
  DCHECK(web_contents_);
  ShowBubble();
}

ShunyaWalletPermissionPromptImpl::~ShunyaWalletPermissionPromptImpl() {
  shunya_wallet::ShunyaWalletTabHelper::FromWebContents(web_contents_)
      ->CloseBubble();
}

void ShunyaWalletPermissionPromptImpl::ShowBubble() {
  shunya_wallet::ShunyaWalletTabHelper::FromWebContents(web_contents_)
      ->ShowBubble();
}

bool ShunyaWalletPermissionPromptImpl::UpdateAnchor() {
  // Don't recreate the view for every BrowserView::Layout() which would cause
  // ShunyaWalletPermissionPromptImpl being destoryed which leads to bubble
  // dismissed unintentionally.
  return true;
}

permissions::PermissionPrompt::TabSwitchingBehavior
ShunyaWalletPermissionPromptImpl::GetTabSwitchingBehavior() {
  return permissions::PermissionPrompt::TabSwitchingBehavior::
      kDestroyPromptButKeepRequestPending;
}

permissions::PermissionPromptDisposition
ShunyaWalletPermissionPromptImpl::GetPromptDisposition() const {
  return permissions::PermissionPromptDisposition::ANCHORED_BUBBLE;
}

absl::optional<gfx::Rect>
ShunyaWalletPermissionPromptImpl::GetViewBoundsInScreen() const {
  return absl::nullopt;
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_PERMISSION_BUBBLE_SHUNYA_WALLET_PERMISSION_PROMPT_IMPL_H_
#define SHUNYA_BROWSER_UI_VIEWS_PERMISSION_BUBBLE_SHUNYA_WALLET_PERMISSION_PROMPT_IMPL_H_

#include "base/memory/raw_ptr.h"
#include "base/time/time.h"
#include "components/permissions/permission_prompt.h"

class Browser;

class ShunyaWalletPermissionPromptImpl : public permissions::PermissionPrompt {
 public:
  ShunyaWalletPermissionPromptImpl(Browser* browser,
                                  content::WebContents* web_contents,
                                  Delegate& delegate);
  ~ShunyaWalletPermissionPromptImpl() override;

  ShunyaWalletPermissionPromptImpl(const ShunyaWalletPermissionPromptImpl&) =
      delete;
  ShunyaWalletPermissionPromptImpl& operator=(
      const ShunyaWalletPermissionPromptImpl&) = delete;

  // permissions::PermissionPrompt:
  bool UpdateAnchor() override;
  TabSwitchingBehavior GetTabSwitchingBehavior() override;
  permissions::PermissionPromptDisposition GetPromptDisposition()
      const override;
  absl::optional<gfx::Rect> GetViewBoundsInScreen() const override;

 private:
  void ShowBubble();

  const raw_ptr<content::WebContents> web_contents_ = nullptr;
  raw_ref<permissions::PermissionPrompt::Delegate> delegate_;
  base::TimeTicks permission_requested_time_;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_PERMISSION_BUBBLE_SHUNYA_WALLET_PERMISSION_PROMPT_IMPL_H_

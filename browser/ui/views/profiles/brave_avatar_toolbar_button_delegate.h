/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_PROFILES_SHUNYA_AVATAR_TOOLBAR_BUTTON_DELEGATE_H_
#define SHUNYA_BROWSER_UI_VIEWS_PROFILES_SHUNYA_AVATAR_TOOLBAR_BUTTON_DELEGATE_H_

#include "chrome/browser/ui/views/profiles/avatar_toolbar_button_delegate.h"

class ShunyaAvatarToolbarButtonDelegate : public AvatarToolbarButtonDelegate {
 public:
  using AvatarToolbarButtonDelegate::AvatarToolbarButtonDelegate;
  ShunyaAvatarToolbarButtonDelegate(AvatarToolbarButton* button,
                                   Browser* browser);
  ShunyaAvatarToolbarButtonDelegate(const ShunyaAvatarToolbarButtonDelegate&) =
      delete;
  ShunyaAvatarToolbarButtonDelegate& operator=(
      const ShunyaAvatarToolbarButtonDelegate&) = delete;
  ~ShunyaAvatarToolbarButtonDelegate() override = default;

  gfx::Image GetGaiaAccountImage() const;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_PROFILES_SHUNYA_AVATAR_TOOLBAR_BUTTON_DELEGATE_H_

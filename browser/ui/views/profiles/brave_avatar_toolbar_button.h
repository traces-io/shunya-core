/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_PROFILES_SHUNYA_AVATAR_TOOLBAR_BUTTON_H_
#define SHUNYA_BROWSER_UI_VIEWS_PROFILES_SHUNYA_AVATAR_TOOLBAR_BUTTON_H_

#include "chrome/browser/ui/views/profiles/avatar_toolbar_button.h"

class BrowserView;

class ShunyaAvatarToolbarButton : public AvatarToolbarButton {
 public:
  explicit ShunyaAvatarToolbarButton(BrowserView* browser_view);
  ShunyaAvatarToolbarButton(const ShunyaAvatarToolbarButton&) = delete;
  ShunyaAvatarToolbarButton& operator=(const ShunyaAvatarToolbarButton&) = delete;
  ~ShunyaAvatarToolbarButton() override;

  AvatarToolbarButton::State GetAvatarButtonState() const;

  // ToolbarButton:
  void SetHighlight(const std::u16string& highlight_text,
                    absl::optional<SkColor> highlight_color) override;
  void UpdateColorsAndInsets() override;

 private:
  // AvatarToolbarButton:
  ui::ImageModel GetAvatarIcon(
      ButtonState state,
      const gfx::Image& profile_identity_image) const override;
  std::u16string GetAvatarTooltipText() const override;
  int GetWindowCount() const;

  base::WeakPtrFactory<ShunyaAvatarToolbarButton> weak_ptr_factory_{this};
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_PROFILES_SHUNYA_AVATAR_TOOLBAR_BUTTON_H_

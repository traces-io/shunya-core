/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR_H_
#define SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR_H_

#include <memory>

#include "chrome/browser/ui/views/infobars/confirm_infobar.h"

// The customized ConfirmInfoBar:
// "Text _link_                     [ok_button]"
// cancel_button is not supported

class ShunyaSyncAccountDeletedInfoBar : public ConfirmInfoBar {
 public:
  explicit ShunyaSyncAccountDeletedInfoBar(
      std::unique_ptr<ConfirmInfoBarDelegate> delegate);

  ShunyaSyncAccountDeletedInfoBar(const ShunyaSyncAccountDeletedInfoBar&) =
      delete;
  ShunyaSyncAccountDeletedInfoBar& operator=(
      const ShunyaSyncAccountDeletedInfoBar&) = delete;

  ~ShunyaSyncAccountDeletedInfoBar() override;

  // InfoBarView:
  void Layout() override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR_H_

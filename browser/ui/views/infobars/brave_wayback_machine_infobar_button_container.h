/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_WAYBACK_MACHINE_INFOBAR_BUTTON_CONTAINER_H_
#define SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_WAYBACK_MACHINE_INFOBAR_BUTTON_CONTAINER_H_

#include "base/memory/raw_ptr.h"
#include "ui/views/controls/button/button.h"
#include "ui/views/view.h"

class ShunyaWaybackMachineInfoBarThrobber;

// This manages button and throbber controls.
// buttons occupies all this containers area and throbber runs over the button.
// When throbbing is requested, button extends its right inset and throbber runs
// on that area.
class ShunyaWaybackMachineInfoBarButtonContainer : public views::View {
 public:
  explicit ShunyaWaybackMachineInfoBarButtonContainer(
      views::Button::PressedCallback callback);
  ~ShunyaWaybackMachineInfoBarButtonContainer() override;

  ShunyaWaybackMachineInfoBarButtonContainer(
      const ShunyaWaybackMachineInfoBarButtonContainer&) = delete;
  ShunyaWaybackMachineInfoBarButtonContainer& operator=(
      const ShunyaWaybackMachineInfoBarButtonContainer&) = delete;

  void StartThrobber();
  void StopThrobber();

  // views::View overrides:
  void Layout() override;
  gfx::Size CalculatePreferredSize() const override;

 private:
  void AdjustButtonInsets(bool add_insets);

  raw_ptr<ShunyaWaybackMachineInfoBarThrobber> throbber_ = nullptr;
  raw_ptr<views::View> button_ = nullptr;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_WAYBACK_MACHINE_INFOBAR_BUTTON_CONTAINER_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_WAYBACK_MACHINE_INFOBAR_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_WAYBACK_MACHINE_INFOBAR_VIEW_H_

#include <memory>

#include "base/memory/raw_ref.h"
#include "chrome/browser/ui/views/infobars/infobar_view.h"

namespace content {
class WebContents;
}

class ShunyaWaybackMachineInfoBarDelegate;

class ShunyaWaybackMachineInfoBarView : public InfoBarView {
 public:
  ShunyaWaybackMachineInfoBarView(
      std::unique_ptr<ShunyaWaybackMachineInfoBarDelegate> delegate,
      content::WebContents* contents);
  ~ShunyaWaybackMachineInfoBarView() override;

  ShunyaWaybackMachineInfoBarView(
      const ShunyaWaybackMachineInfoBarView&) = delete;
  ShunyaWaybackMachineInfoBarView& operator=(
      const ShunyaWaybackMachineInfoBarView&) = delete;

 private:
  // InfoBarView overrides:
  void Layout() override;

  const raw_ref<views::View> sub_views_;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_WAYBACK_MACHINE_INFOBAR_VIEW_H_

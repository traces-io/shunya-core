/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "chrome/browser/ui/views/infobars/infobar_container_view.h"

#ifndef SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_INFOBAR_CONTAINER_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_INFOBAR_CONTAINER_VIEW_H_

class ShunyaInfoBarContainerView : public InfoBarContainerView {
 public:
  explicit ShunyaInfoBarContainerView(
      infobars::InfoBarContainer::Delegate* delegate);
  ShunyaInfoBarContainerView(const ShunyaInfoBarContainerView&) = delete;
  ShunyaInfoBarContainerView& operator=(const ShunyaInfoBarContainerView&) =
      delete;
  ~ShunyaInfoBarContainerView() override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_INFOBARS_SHUNYA_INFOBAR_CONTAINER_VIEW_H_

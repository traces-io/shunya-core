/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/page_action/shunya_page_action_icon_container_view.h"

#include "shunya/browser/ui/page_action/shunya_page_action_icon_type.h"
#include "shunya/components/playlist/common/features.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/views/page_action/page_action_icon_params.h"
#include "ui/base/metadata/metadata_impl_macros.h"

namespace {

PageActionIconParams& ModifyIconParamsForShunya(PageActionIconParams& params) {
  // Add actions for Shunya
  if (base::FeatureList::IsEnabled(playlist::features::kPlaylist) &&
      params.browser->is_type_normal() &&
      !params.browser->profile()->IsOffTheRecord()) {
    // Insert Playlist action before sharing hub or at the end of the vector.
    params.types_enabled.insert(
        base::ranges::find(params.types_enabled,
                           PageActionIconType::kSharingHub),
        shunya::kPlaylistPageActionIconType);
  }

  return params;
}

}  // namespace

ShunyaPageActionIconContainerView::ShunyaPageActionIconContainerView(
    PageActionIconParams& params)
    : PageActionIconContainerView(ModifyIconParamsForShunya(params)) {}

ShunyaPageActionIconContainerView::~ShunyaPageActionIconContainerView() = default;

BEGIN_METADATA(ShunyaPageActionIconContainerView, PageActionIconContainerView)
END_METADATA

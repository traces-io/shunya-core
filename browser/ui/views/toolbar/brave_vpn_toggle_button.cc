/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/toolbar/shunya_vpn_toggle_button.h"

#include <utility>

#include "base/functional/bind.h"
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "shunya/grit/shunya_generated_resources.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/views/frame/browser_view.h"
#include "ui/color/color_id.h"

using ConnectionState = shunya_vpn::mojom::ConnectionState;
using PurchasedState = shunya_vpn::mojom::PurchasedState;

ShunyaVPNToggleButton::ShunyaVPNToggleButton(Browser* browser)
    : browser_(browser),
      service_(shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(
          browser_->profile())) {
  DCHECK(service_);

  Observe(service_);

  SetCallback(base::BindRepeating(&ShunyaVPNToggleButton::OnButtonPressed,
                                  base::Unretained(this)));
  UpdateState();

  if (const ui::ColorProvider* provider =
          BrowserView::GetBrowserViewForBrowser(browser_)->GetColorProvider()) {
    SetThumbOnColor(provider->GetColor(ui::kColorToggleButtonThumbOn));
    SetThumbOffColor(provider->GetColor(ui::kColorToggleButtonThumbOff));
    SetTrackOnColor(provider->GetColor(ui::kColorToggleButtonTrackOn));
    SetTrackOffColor(provider->GetColor(ui::kColorToggleButtonTrackOff));
  }

  // TODO(simonhong): Re-visit this name.
  SetAccessibleName(shunya_l10n::GetLocalizedResourceUTF16String(
      IDS_SHUNYA_VPN_TOGGLE_MENU_ITEM_TEXT));
}

ShunyaVPNToggleButton::~ShunyaVPNToggleButton() = default;

void ShunyaVPNToggleButton::OnConnectionStateChanged(ConnectionState state) {
  UpdateState();
}

void ShunyaVPNToggleButton::OnButtonPressed(const ui::Event& event) {
  service_->ToggleConnection();
}

void ShunyaVPNToggleButton::UpdateState() {
  const auto state = service_->GetConnectionState();
  bool is_on = (state == ConnectionState::CONNECTING ||
                state == ConnectionState::CONNECTED);
  SetIsOn(is_on);
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/toolbar/shunya_vpn_button.h"

#include <utility>

#include "base/memory/raw_ptr.h"
#include "base/notreached.h"
#include "shunya/app/shunya_command_ids.h"
#include "shunya/app/vector_icons/vector_icons.h"
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/browser/ui/color/shunya_color_id.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "shunya/grit/shunya_generated_resources.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_commands.h"
#include "chrome/browser/ui/color/chrome_color_id.h"
#include "chrome/browser/ui/layout_constants.h"
#include "chrome/browser/ui/views/toolbar/toolbar_ink_drop_util.h"
#include "components/grit/shunya_components_strings.h"
#include "third_party/abseil-cpp/absl/types/optional.h"
#include "ui/base/l10n/l10n_util.h"
#include "ui/base/metadata/metadata_impl_macros.h"
#include "ui/base/models/simple_menu_model.h"
#include "ui/compositor/layer.h"
#include "ui/gfx/canvas.h"
#include "ui/gfx/color_utils.h"
#include "ui/gfx/geometry/rect_f.h"
#include "ui/gfx/geometry/rrect_f.h"
#include "ui/gfx/paint_vector_icon.h"
#include "ui/gfx/skia_util.h"
#include "ui/views/animation/ink_drop.h"
#include "ui/views/animation/ink_drop_host.h"
#include "ui/views/background.h"
#include "ui/views/border.h"
#include "ui/views/controls/highlight_path_generator.h"

using ConnectionState = shunya_vpn::mojom::ConnectionState;
using PurchasedState = shunya_vpn::mojom::PurchasedState;

namespace {

constexpr int kButtonRadius = 100;

class ShunyaVPNButtonHighlightPathGenerator
    : public views::HighlightPathGenerator {
 public:
  explicit ShunyaVPNButtonHighlightPathGenerator(const gfx::Insets& insets)
      : HighlightPathGenerator(insets) {}

  ShunyaVPNButtonHighlightPathGenerator(
      const ShunyaVPNButtonHighlightPathGenerator&) = delete;
  ShunyaVPNButtonHighlightPathGenerator& operator=(
      const ShunyaVPNButtonHighlightPathGenerator&) = delete;

  // views::HighlightPathGenerator overrides:
  absl::optional<gfx::RRectF> GetRoundRect(const gfx::RectF& rect) override {
    return gfx::RRectF(rect, kButtonRadius);
  }
};

// For error icon's inner color.
class ConnectErrorIconBackground : public views::Background {
 public:
  explicit ConnectErrorIconBackground(SkColor color) {
    SetNativeControlColor(color);
  }

  ConnectErrorIconBackground(const ConnectErrorIconBackground&) = delete;
  ConnectErrorIconBackground& operator=(const ConnectErrorIconBackground&) =
      delete;

  void Paint(gfx::Canvas* canvas, views::View* view) const override {
    auto bounds = view->GetLocalBounds();
    bounds.Inset(gfx::Insets::TLBR(2, 4, 2, 4));
    canvas->FillRect(bounds, get_color());
  }
};

class VPNButtonMenuModel : public ui::SimpleMenuModel,
                           public ui::SimpleMenuModel::Delegate,
                           public shunya_vpn::ShunyaVPNServiceObserver {
 public:
  explicit VPNButtonMenuModel(Browser* browser)
      : SimpleMenuModel(this),
        browser_(browser),
        service_(shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(
            browser_->profile())) {
    DCHECK(service_);
    Observe(service_);
    Build(service_->is_purchased_user());
  }

  ~VPNButtonMenuModel() override = default;
  VPNButtonMenuModel(const VPNButtonMenuModel&) = delete;
  VPNButtonMenuModel& operator=(const VPNButtonMenuModel&) = delete;

 private:
  // ui::SimpleMenuModel::Delegate override:
  void ExecuteCommand(int command_id, int event_flags) override {
    chrome::ExecuteCommand(browser_, command_id);
  }

  // ShunyaVPNServiceObserver overrides:
  void OnPurchasedStateChanged(
      shunya_vpn::mojom::PurchasedState state,
      const absl::optional<std::string>& description) override {
    // Rebuild menu items based on purchased state change.
    Build(service_->is_purchased_user());
  }

  void Build(bool purchased) {
    // Clear all menu items and re-build as purchased state can be updated
    // during the runtime.
    Clear();
    AddItemWithStringId(IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON,
                        IDS_SHUNYA_VPN_HIDE_VPN_BUTTON_MENU_ITEM);
    if (purchased) {
      AddItemWithStringId(IDC_SEND_SHUNYA_VPN_FEEDBACK,
                          IDS_SHUNYA_VPN_SHOW_FEEDBACK_MENU_ITEM);
      AddItemWithStringId(IDC_ABOUT_SHUNYA_VPN,
                          IDS_SHUNYA_VPN_ABOUT_VPN_MENU_ITEM);
      AddItemWithStringId(IDC_MANAGE_SHUNYA_VPN_PLAN,
                          IDS_SHUNYA_VPN_MANAGE_MY_PLAN_MENU_ITEM);
    }
  }

  raw_ptr<Browser> browser_ = nullptr;
  raw_ptr<shunya_vpn::ShunyaVpnService> service_ = nullptr;
};

}  // namespace

ShunyaVPNButton::ShunyaVPNButton(Browser* browser)
    : ToolbarButton(base::BindRepeating(&ShunyaVPNButton::OnButtonPressed,
                                        base::Unretained(this)),
                    std::make_unique<VPNButtonMenuModel>(browser),
                    nullptr,
                    false),  // Long-pressing is not intended for something that
                             // already shows a panel on click
      browser_(browser),
      service_(shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(
          browser_->profile())) {
  DCHECK(service_);
  Observe(service_);

  // Replace ToolbarButton's highlight path generator.
  views::HighlightPathGenerator::Install(
      this, std::make_unique<ShunyaVPNButtonHighlightPathGenerator>(
                GetToolbarInkDropInsets(this)));

  // Set 0.0f to use same color for activated state.
  views::InkDrop::Get(this)->SetVisibleOpacity(0.00f);

  // Different base color is set per themes and it has alpha.
  views::InkDrop::Get(this)->SetHighlightOpacity(1.0f);

  // The MenuButtonController makes sure the panel closes when clicked if the
  // panel is already open.
  auto menu_button_controller = std::make_unique<views::MenuButtonController>(
      this,
      base::BindRepeating(&ShunyaVPNButton::OnButtonPressed,
                          base::Unretained(this)),
      std::make_unique<views::Button::DefaultButtonControllerDelegate>(this));
  menu_button_controller_ = menu_button_controller.get();
  SetButtonController(std::move(menu_button_controller));

  SetTextSubpixelRenderingEnabled(false);
  label()->SetText(shunya_l10n::GetLocalizedResourceUTF16String(
      IDS_SHUNYA_VPN_TOOLBAR_BUTTON_TEXT));
  gfx::FontList font_list = views::Label::GetDefaultFontList();
  constexpr int kFontSize = 12;
  label()->SetFontList(
      font_list.DeriveWithSizeDelta(kFontSize - font_list.GetFontSize()));

  // W/o layer, ink drop affects text color.
  label()->SetPaintToLayer();

  // To clear previous pixels.
  label()->layer()->SetFillsBoundsOpaquely(false);

  // Set image positions first. then label.
  SetHorizontalAlignment(gfx::ALIGN_LEFT);

  // Views resulting in focusable nodes later on in the accessibility tree need
  // to have an accessible name for screen readers to see what they are about.
  // TODO(simonhong): Re-visit this name.
  SetAccessibleName(shunya_l10n::GetLocalizedResourceUTF16String(
      IDS_SHUNYA_VPN_TOOLBAR_BUTTON_TEXT));

  constexpr int kShunyaAvatarImageLabelSpacing = 4;
  SetImageLabelSpacing(kShunyaAvatarImageLabelSpacing);
}

ShunyaVPNButton::~ShunyaVPNButton() = default;

void ShunyaVPNButton::OnConnectionStateChanged(ConnectionState state) {
  UpdateColorsAndInsets();
}

void ShunyaVPNButton::OnPurchasedStateChanged(
    shunya_vpn::mojom::PurchasedState state,
    const absl::optional<std::string>& description) {
  if (IsPurchased()) {
    UpdateColorsAndInsets();
  }
}

std::unique_ptr<views::Border> ShunyaVPNButton::GetBorder(
    SkColor border_color) const {
  constexpr auto kTargetInsets = gfx::Insets::VH(5, 11);
  constexpr auto kBorderThickness = 1;
  std::unique_ptr<views::Border> border = views::CreateRoundedRectBorder(
      kBorderThickness, kButtonRadius, gfx::Insets(), border_color);
  const gfx::Insets extra_insets = kTargetInsets - border->GetInsets();
  return views::CreatePaddedBorder(std::move(border), extra_insets);
}

void ShunyaVPNButton::UpdateColorsAndInsets() {
  ui::ColorProvider* cp = GetColorProvider();
  if (!cp) {
    return;
  }
  const bool is_connect_error = IsConnectError();
  const bool is_connected = IsConnected();
  const auto bg_color =
      cp->GetColor(is_connect_error ? kColorShunyaVpnButtonErrorBackgroundNormal
                                    : kColorShunyaVpnButtonBackgroundNormal);
  SetBackground(views::CreateRoundedRectBackground(bg_color, kButtonRadius));

  SetEnabledTextColors(cp->GetColor(is_connect_error
                                        ? kColorShunyaVpnButtonTextError
                                        : kColorShunyaVpnButtonText));

  if (is_connect_error) {
    SetImage(
        views::Button::STATE_NORMAL,
        gfx::CreateVectorIcon(kVpnIndicatorErrorIcon,
                              cp->GetColor(kColorShunyaVpnButtonIconError)));

    // Use background for inner color of error button image.
    image()->SetBackground(std::make_unique<ConnectErrorIconBackground>(
        cp->GetColor(kColorShunyaVpnButtonIconErrorInner)));
  } else {
    SetImage(
        views::Button::STATE_NORMAL,
        gfx::CreateVectorIcon(
            is_connected ? kVpnIndicatorOnIcon : kVpnIndicatorOffIcon,
            cp->GetColor(is_connected ? kColorShunyaVpnButtonIconConnected
                                      : kColorShunyaVpnButtonIconDisconnected)));

    // Use background for inner color of button image.
    // Adjusted border thickness to make invisible to the outside of the icon.
    image()->SetBackground(views::CreateRoundedRectBackground(
        cp->GetColor(kColorShunyaVpnButtonIconInner), 5 /*radi*/, 2 /*thick*/));
  }

  // Compute highlight color and border in advance. If not, highlight color and
  // border color are mixed as both have alpha value.
  // Draw border only for error state.
  SetBorder(GetBorder(color_utils::GetResultingPaintColor(
      cp->GetColor(is_connect_error ? kColorShunyaVpnButtonErrorBorder
                                    : kColorShunyaVpnButtonBorder),
      bg_color)));

  auto* ink_drop_host = views::InkDrop::Get(this);

  // Use different ink drop hover color for each themes.
  auto target_base_color = color_utils::GetResultingPaintColor(
      cp->GetColor(is_connect_error ? kColorShunyaVpnButtonErrorBackgroundHover
                                    : kColorShunyaVpnButtonBorder),
      bg_color);
  bool need_ink_drop_color_update =
      target_base_color != ink_drop_host->GetBaseColor();

  // Update ink drop color if needed because we toggle ink drop mode below after
  // set base color. Toggling could cause subtle flicking.
  if (!need_ink_drop_color_update) {
    return;
  }

  views::InkDrop::Get(this)->SetBaseColor(target_base_color);

  // Hack to update inkdrop color immediately.
  // W/o this, background color and image are changed but inkdrop color is still
  // using previous one till button state is changed after changing base color.
  const auto previous_ink_drop_state =
      views::InkDrop::Get(this)->GetInkDrop()->GetTargetInkDropState();
  views::InkDrop::Get(this)->SetMode(views::InkDropHost::InkDropMode::OFF);
  views::InkDrop::Get(this)->SetMode(views::InkDropHost::InkDropMode::ON);
  // After toggling, ink drop state is reset. So need to re-apply previous
  // state.
  if (previous_ink_drop_state == views::InkDropState::ACTIVATED) {
    views::InkDrop::Get(this)->GetInkDrop()->SnapToActivated();
  }
}

std::u16string ShunyaVPNButton::GetTooltipText(const gfx::Point& p) const {
  if (!IsPurchased())
    return l10n_util::GetStringUTF16(IDS_SHUNYA_VPN);

  return l10n_util::GetStringUTF16(IsConnected()
                                       ? IDS_SHUNYA_VPN_CONNECTED_TOOLTIP
                                       : IDS_SHUNYA_VPN_DISCONNECTED_TOOLTIP);
}

bool ShunyaVPNButton::IsConnected() const {
  return service_->IsConnected();
}

bool ShunyaVPNButton::IsConnectError() const {
  const auto state = service_->GetConnectionState();
  return (state == ConnectionState::CONNECT_NOT_ALLOWED ||
          state == ConnectionState::CONNECT_FAILED);
}

bool ShunyaVPNButton::IsPurchased() const {
  return service_->is_purchased_user();
}
void ShunyaVPNButton::OnButtonPressed(const ui::Event& event) {
  chrome::ExecuteCommand(browser_, IDC_SHOW_SHUNYA_VPN_PANEL);
}

BEGIN_METADATA(ShunyaVPNButton, LabelButton)
END_METADATA

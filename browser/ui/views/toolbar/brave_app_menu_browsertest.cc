/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "base/test/scoped_feature_list.h"
#include "shunya/app/shunya_command_ids.h"
#include "shunya/browser/ui/shunya_browser_command_controller.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/skus/common/features.h"
#include "chrome/browser/ui/views/frame/browser_view.h"
#include "chrome/browser/ui/views/toolbar/app_menu.h"
#include "chrome/browser/ui/views/toolbar/browser_app_menu_button.h"
#include "chrome/browser/ui/views/toolbar/toolbar_view.h"
#include "chrome/test/base/in_process_browser_test.h"
#include "chrome/test/base/ui_test_utils.h"
#include "content/public/test/browser_test.h"
#include "content/public/test/test_utils.h"
#include "ui/views/controls/button/toggle_button.h"
#include "ui/views/controls/menu/menu_item_view.h"
#include "ui/views/controls/menu/menu_runner.h"
#include "ui/views/view_utils.h"

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/shunya_vpn/common/features.h"
#endif

class ShunyaAppMenuBrowserTest : public InProcessBrowserTest {
 public:
  ShunyaAppMenuBrowserTest() {
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
    scoped_feature_list_.InitWithFeatures(
        {skus::features::kSkusFeature, shunya_vpn::features::kShunyaVPN}, {});
#endif
  }

  ~ShunyaAppMenuBrowserTest() override = default;

  BrowserAppMenuButton* menu_button() {
    return BrowserView::GetBrowserViewForBrowser(browser())
        ->toolbar()
        ->app_menu_button();
  }

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  void SetPurchasedUserForShunyaVPN(Browser* browser, bool purchased) {
    auto* service =
        shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(browser->profile());
    ASSERT_TRUE(!!service);
    auto target_state = purchased
                            ? shunya_vpn::mojom::PurchasedState::PURCHASED
                            : shunya_vpn::mojom::PurchasedState::NOT_PURCHASED;
    service->SetPurchasedState(skus::GetDefaultEnvironment(), target_state);
    // Call explicitely to update vpn commands status because mojo works in
    // async way.
    static_cast<chrome::ShunyaBrowserCommandController*>(
        browser->command_controller())
        ->OnPurchasedStateChanged(target_state, absl::nullopt);
  }

  base::test::ScopedFeatureList scoped_feature_list_;
#endif
};

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
// Check toggle menu item has additional toggle button for purchased user.
IN_PROC_BROWSER_TEST_F(ShunyaAppMenuBrowserTest, PurchasedVPN) {
  SetPurchasedUserForShunyaVPN(browser(), true);
  menu_button()->ShowMenu(views::MenuRunner::NO_FLAGS);
  views::MenuItemView* menu_root = menu_button()->app_menu()->root_menu_item();
  auto* toggle_menu_item = menu_root->GetMenuItemByID(IDC_TOGGLE_SHUNYA_VPN);
  ASSERT_TRUE(!!toggle_menu_item);
  const int last_item_index = toggle_menu_item->children().size() - 1;
  auto* toggle_button = views::AsViewClass<views::ToggleButton>(
      toggle_menu_item->children()[last_item_index]);
  ASSERT_NE(nullptr, toggle_button);
}

// Check app menu has show vpn panel menu item for not purchased user.
IN_PROC_BROWSER_TEST_F(ShunyaAppMenuBrowserTest, NotPurchasedVPN) {
  SetPurchasedUserForShunyaVPN(browser(), false);
  menu_button()->ShowMenu(views::MenuRunner::NO_FLAGS);
  views::MenuItemView* menu_root = menu_button()->app_menu()->root_menu_item();
  EXPECT_TRUE(!!menu_root->GetMenuItemByID(IDC_SHOW_SHUNYA_VPN_PANEL));
}
#endif

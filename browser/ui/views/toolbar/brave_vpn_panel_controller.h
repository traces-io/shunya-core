/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_PANEL_CONTROLLER_H_
#define SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_PANEL_CONTROLLER_H_

#include <memory>

#include "base/memory/raw_ptr.h"
#include "shunya/browser/ui/webui/shunya_vpn/vpn_panel_ui.h"
#include "chrome/browser/ui/views/bubble/webui_bubble_manager.h"

class ShunyaBrowserView;

class ShunyaVPNPanelController {
 public:
  explicit ShunyaVPNPanelController(ShunyaBrowserView* browser_view);
  ~ShunyaVPNPanelController();
  ShunyaVPNPanelController(const ShunyaVPNPanelController&) = delete;
  ShunyaVPNPanelController& operator=(const ShunyaVPNPanelController&) = delete;

  void ShowShunyaVPNPanel();
  // Manager should be reset to use different anchor view for bubble.
  void ResetBubbleManager();

 private:
  raw_ptr<ShunyaBrowserView> browser_view_ = nullptr;
  std::unique_ptr<WebUIBubbleManagerT<VPNPanelUI>> webui_bubble_manager_;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_PANEL_CONTROLLER_H_

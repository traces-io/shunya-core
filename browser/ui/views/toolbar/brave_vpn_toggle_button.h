/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_TOGGLE_BUTTON_H_
#define SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_TOGGLE_BUTTON_H_

#include "base/memory/raw_ptr.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service_observer.h"
#include "ui/views/controls/button/toggle_button.h"

namespace shunya_vpn {
class ShunyaVpnService;
}  // namespace shunya_vpn

class Browser;

class ShunyaVPNToggleButton : public views::ToggleButton,
                             public shunya_vpn::ShunyaVPNServiceObserver {
 public:
  explicit ShunyaVPNToggleButton(Browser* browser);
  ~ShunyaVPNToggleButton() override;

  ShunyaVPNToggleButton(const ShunyaVPNToggleButton&) = delete;
  ShunyaVPNToggleButton& operator=(const ShunyaVPNToggleButton&) = delete;

 private:
  // ShunyaVPNServiceObserver overrides:
  void OnConnectionStateChanged(
      shunya_vpn::mojom::ConnectionState state) override;

  void OnButtonPressed(const ui::Event& event);
  void UpdateState();

  raw_ptr<Browser> browser_ = nullptr;
  raw_ptr<shunya_vpn::ShunyaVpnService> service_ = nullptr;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_TOGGLE_BUTTON_H_

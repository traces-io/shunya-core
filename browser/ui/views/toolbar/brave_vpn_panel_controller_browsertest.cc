/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "base/test/scoped_feature_list.h"
#include "shunya/app/shunya_command_ids.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/skus/common/features.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_command_controller.h"
#include "chrome/browser/ui/test/test_browser_dialog.h"
#include "content/public/test/browser_test.h"

class ShunyaVPNPanelControllerTest : public DialogBrowserTest {
 public:
  ShunyaVPNPanelControllerTest() {
    scoped_feature_list_.InitWithFeatures(
        {skus::features::kSkusFeature, shunya_vpn::features::kShunyaVPN}, {});
  }

  ~ShunyaVPNPanelControllerTest() override = default;

  // TestBrowserUi:
  void ShowUi(const std::string& name) override {
    browser()->command_controller()->ExecuteCommand(IDC_SHOW_SHUNYA_VPN_PANEL);
  }

  base::test::ScopedFeatureList scoped_feature_list_;
};

IN_PROC_BROWSER_TEST_F(ShunyaVPNPanelControllerTest, InvokeUi_Dialog) {
  ShowAndVerifyUi();
}

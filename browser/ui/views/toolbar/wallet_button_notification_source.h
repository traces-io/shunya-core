/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_WALLET_BUTTON_NOTIFICATION_SOURCE_H_
#define SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_WALLET_BUTTON_NOTIFICATION_SOURCE_H_

#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_wallet/browser/keyring_service.h"
#include "shunya/components/shunya_wallet/browser/keyring_service_observer_base.h"
#include "shunya/components/shunya_wallet/browser/tx_service.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "chrome/browser/profiles/profile.h"
#include "components/prefs/pref_service.h"
#include "mojo/public/cpp/bindings/receiver.h"
#include "mojo/public/cpp/bindings/remote.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya {

using WalletButtonNotificationSourceCallback =
    base::RepeatingCallback<void(bool /* show suggest */,
                                 size_t /* counter */)>;

// Provides and updates data for the wallet button notification badge.
// Like number of pending transactions or onboarding bubble to show.
class WalletButtonNotificationSource
    : shunya_wallet::mojom::TxServiceObserver,
      shunya_wallet::KeyringServiceObserverBase {
 public:
  WalletButtonNotificationSource(
      Profile* profile,
      WalletButtonNotificationSourceCallback callback);
  ~WalletButtonNotificationSource() override;

  void MarkWalletButtonWasClicked();
  void Init();

 private:
  void EnsureTxServiceConnected();
  void OnTxServiceConnectionError();

  void EnsureKeyringServiceConnected();
  void OnKeyringServiceConnectionError();

  // shunya_wallet::mojom::TxServiceObserver
  void OnNewUnapprovedTx(
      shunya_wallet::mojom::TransactionInfoPtr tx_info) override;
  void OnUnapprovedTxUpdated(
      shunya_wallet::mojom::TransactionInfoPtr tx_info) override {}
  void OnTransactionStatusChanged(
      shunya_wallet::mojom::TransactionInfoPtr tx_info) override;
  void OnTxServiceReset() override;

  // shunya_wallet::KeyringServiceObserverBase
  void KeyringCreated(shunya_wallet::mojom::KeyringId keyring_id) override;
  void KeyringRestored(shunya_wallet::mojom::KeyringId keyring_id) override;

  void OnKeyringReady(shunya_wallet::mojom::KeyringId keyring_id);
  void CheckTxStatus();
  void OnTxStatusResolved(uint32_t count);
  void OnKeyringInfoResolved(shunya_wallet::mojom::KeyringInfoPtr keyring_info);

  void NotifyObservers();

  raw_ptr<Profile> profile_ = nullptr;
  raw_ptr<PrefService> prefs_ = nullptr;
  mojo::Remote<shunya_wallet::mojom::TxService> tx_service_;
  mojo::Remote<shunya_wallet::mojom::KeyringService> keyring_service_;

  mojo::Receiver<shunya_wallet::mojom::TxServiceObserver> tx_observer_{this};
  mojo::Receiver<shunya_wallet::mojom::KeyringServiceObserver>
      keyring_service_observer_{this};

  WalletButtonNotificationSourceCallback callback_;

  absl::optional<bool> wallet_created_;
  uint32_t pending_tx_count_ = 0;

  base::WeakPtrFactory<WalletButtonNotificationSource> weak_ptr_factory_{this};
};

}  // namespace shunya

#endif  // SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_WALLET_BUTTON_NOTIFICATION_SOURCE_H_

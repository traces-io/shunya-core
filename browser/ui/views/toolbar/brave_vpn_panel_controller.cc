/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/toolbar/shunya_vpn_panel_controller.h"

#include "shunya/browser/ui/views/bubble/shunya_webui_bubble_manager.h"
#include "shunya/browser/ui/views/frame/shunya_browser_view.h"
#include "shunya/components/constants/webui_url_constants.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "components/grit/shunya_components_strings.h"
#include "url/gurl.h"

ShunyaVPNPanelController::ShunyaVPNPanelController(ShunyaBrowserView* browser_view)
    : browser_view_(browser_view) {
  DCHECK(browser_view_);
}

ShunyaVPNPanelController::~ShunyaVPNPanelController() = default;

void ShunyaVPNPanelController::ShowShunyaVPNPanel() {
  auto* anchor_view = browser_view_->GetAnchorViewForShunyaVPNPanel();
  if (!anchor_view)
    return;

  if (!webui_bubble_manager_) {
    auto* profile = browser_view_->browser()->profile();
    webui_bubble_manager_ =
        std::make_unique<ShunyaWebUIBubbleManager<VPNPanelUI>>(
            anchor_view, profile, GURL(kVPNPanelURL), IDS_SHUNYA_VPN_PANEL_NAME);
  }

  if (webui_bubble_manager_->GetBubbleWidget()) {
    webui_bubble_manager_->CloseBubble();
    return;
  }

  webui_bubble_manager_->ShowBubble();
}

void ShunyaVPNPanelController::ResetBubbleManager() {
  webui_bubble_manager_.reset();
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_BUTTON_H_
#define SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_BUTTON_H_

#include <memory>
#include <string>

#include "base/memory/raw_ptr.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service_observer.h"
#include "chrome/browser/ui/views/toolbar/toolbar_button.h"
#include "ui/base/metadata/metadata_header_macros.h"
#include "ui/views/controls/button/menu_button_controller.h"

namespace shunya_vpn {
class ShunyaVpnService;
}  // namespace shunya_vpn

namespace views {
class Border;
}  // namespace views

class Browser;

class ShunyaVPNButton : public ToolbarButton,
                       public shunya_vpn::ShunyaVPNServiceObserver {
 public:
  METADATA_HEADER(ShunyaVPNButton);

  explicit ShunyaVPNButton(Browser* browser);
  ~ShunyaVPNButton() override;

  ShunyaVPNButton(const ShunyaVPNButton&) = delete;
  ShunyaVPNButton& operator=(const ShunyaVPNButton&) = delete;

  // ShunyaVPNServiceObserver overrides:
  void OnConnectionStateChanged(
      shunya_vpn::mojom::ConnectionState state) override;
  void OnPurchasedStateChanged(
      shunya_vpn::mojom::PurchasedState state,
      const absl::optional<std::string>& description) override;

 private:
  // ToolbarButton overrides:
  void UpdateColorsAndInsets() override;
  std::u16string GetTooltipText(const gfx::Point& p) const override;

  bool IsConnected() const;
  bool IsConnectError() const;
  bool IsPurchased() const;
  std::unique_ptr<views::Border> GetBorder(SkColor border_color) const;
  void OnButtonPressed(const ui::Event& event);

  raw_ptr<Browser> browser_ = nullptr;
  raw_ptr<shunya_vpn::ShunyaVpnService> service_ = nullptr;
  raw_ptr<views::MenuButtonController> menu_button_controller_ = nullptr;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_TOOLBAR_SHUNYA_VPN_BUTTON_H_

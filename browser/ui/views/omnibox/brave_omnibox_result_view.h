/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_OMNIBOX_SHUNYA_OMNIBOX_RESULT_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_OMNIBOX_SHUNYA_OMNIBOX_RESULT_VIEW_H_

#include "base/memory/raw_ptr.h"
#include "chrome/browser/ui/views/omnibox/omnibox_result_view.h"
#include "ui/base/metadata/metadata_header_macros.h"

class ShunyaSearchConversionPromotionView;

// This will render shunya specific matches such as the shunyar search conversion
// promotion.
class ShunyaOmniboxResultView : public OmniboxResultView {
 public:
  METADATA_HEADER(ShunyaOmniboxResultView);
  using OmniboxResultView::OmniboxResultView;
  ShunyaOmniboxResultView(const ShunyaOmniboxResultView&) = delete;
  ShunyaOmniboxResultView& operator=(const ShunyaOmniboxResultView&) = delete;
  ~ShunyaOmniboxResultView() override;

  // OmniboxResultView overrides:
  void SetMatch(const AutocompleteMatch& match) override;
  void OnSelectionStateChanged() override;

  void OpenMatch();
  void RefreshOmniboxResult();

 private:
  void ResetChildrenVisibility();
  void UpdateForShunyaSearchConversion();
  void HandleSelectionStateChangedForPromotionView();

  // Shunya search conversion promotion
  raw_ptr<ShunyaSearchConversionPromotionView> shunya_search_promotion_view_ =
      nullptr;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_OMNIBOX_SHUNYA_OMNIBOX_RESULT_VIEW_H_

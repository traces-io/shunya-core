/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_OMNIBOX_SHUNYA_OMNIBOX_POPUP_VIEW_VIEWS_H_
#define SHUNYA_BROWSER_UI_VIEWS_OMNIBOX_SHUNYA_OMNIBOX_POPUP_VIEW_VIEWS_H_

#include "chrome/browser/ui/views/omnibox/omnibox_popup_view_views.h"

class ShunyaOmniboxPopupViewViews : public OmniboxPopupViewViews {
 public:
  METADATA_HEADER(ShunyaOmniboxPopupViewViews);

  using OmniboxPopupViewViews::OmniboxPopupViewViews;
  ~ShunyaOmniboxPopupViewViews() override;

  // OmniboxPopupViewViews:
  gfx::Rect GetTargetBounds() const override;
};

#endif  // SHUNYA_BROWSER_UI_VIEWS_OMNIBOX_SHUNYA_OMNIBOX_POPUP_VIEW_VIEWS_H_

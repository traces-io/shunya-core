/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/omnibox/shunya_omnibox_result_view.h"

#include <memory>

#include "base/time/time.h"
#include "shunya/browser/ui/views/omnibox/shunya_search_conversion_promotion_view.h"
#include "shunya/components/omnibox/browser/promotion_utils.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/ui/views/omnibox/omnibox_popup_view_views.h"
#include "chrome/browser/ui/views/omnibox/omnibox_suggestion_button_row_view.h"
#include "components/omnibox/browser/autocomplete_controller.h"
#include "components/omnibox/browser/autocomplete_match.h"
#include "components/omnibox/browser/autocomplete_provider_client.h"
#include "components/omnibox/browser/autocomplete_result.h"
#include "components/omnibox/browser/omnibox_controller.h"
#include "components/omnibox/browser/omnibox_edit_model.h"
#include "components/omnibox/browser/omnibox_popup_selection.h"
#include "ui/base/metadata/metadata_impl_macros.h"
#include "ui/base/window_open_disposition.h"
#include "ui/views/controls/button/image_button.h"

ShunyaOmniboxResultView::~ShunyaOmniboxResultView() = default;

void ShunyaOmniboxResultView::ResetChildrenVisibility() {
  // Reset children visibility. Their visibility could be configured later
  // based on |match_| and the current input.
  // NOTE: The first child in the result box is supposed to be the
  // `suggestion_container_`, which used to be stored as a data member.
  children().front()->SetVisible(true);
  button_row_->SetVisible(true);
  if (shunya_search_promotion_view_) {
    shunya_search_promotion_view_->SetVisible(false);
  }
}

void ShunyaOmniboxResultView::SetMatch(const AutocompleteMatch& match) {
  ResetChildrenVisibility();
  OmniboxResultView::SetMatch(match);

  if (IsShunyaSearchPromotionMatch(match)) {
    UpdateForShunyaSearchConversion();
  }
}

void ShunyaOmniboxResultView::OnSelectionStateChanged() {
  OmniboxResultView::OnSelectionStateChanged();

  HandleSelectionStateChangedForPromotionView();
}

void ShunyaOmniboxResultView::OpenMatch() {
  popup_view_->model()->OpenSelection(OmniboxPopupSelection(model_index_),
                                      base::TimeTicks::Now());
}

void ShunyaOmniboxResultView::RefreshOmniboxResult() {
  auto* controller = popup_view_->controller()->autocomplete_controller();

  // To refresh autocomplete result, start again with current input.
  controller->Start(controller->input());
}

void ShunyaOmniboxResultView::HandleSelectionStateChangedForPromotionView() {
  if (shunya_search_promotion_view_ && IsShunyaSearchPromotionMatch(match_)) {
    shunya_search_promotion_view_->OnSelectionStateChanged(
        GetMatchSelected() &&
        popup_view_->GetSelection().state == OmniboxPopupSelection::NORMAL);
  }
}

void ShunyaOmniboxResultView::UpdateForShunyaSearchConversion() {
  DCHECK(IsShunyaSearchPromotionMatch(match_));

  // Hide upstream children and show our promotion view.
  // NOTE: The first child in the result box is supposed to be the
  // `suggestion_container_`, which used to be stored as a data member.
  children().front()->SetVisible(false);
  button_row_->SetVisible(false);

  if (!shunya_search_promotion_view_) {
    auto* controller = popup_view_->controller()->autocomplete_controller();
    auto* prefs = controller->autocomplete_provider_client()->GetPrefs();
    shunya_search_promotion_view_ =
        AddChildView(std::make_unique<ShunyaSearchConversionPromotionView>(
            this, g_browser_process->local_state(), prefs));
  }

  shunya_search_promotion_view_->SetVisible(true);
  shunya_search_promotion_view_->SetTypeAndInput(
      GetConversionTypeFromMatch(match_),
      popup_view_->controller()->autocomplete_controller()->input().text());
}

BEGIN_METADATA(ShunyaOmniboxResultView, OmniboxResultView)
END_METADATA

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/shunya_ads/notification_ad_view_factory.h"

#include "shunya/browser/ui/views/shunya_ads/text_notification_ad_view.h"

namespace shunya_ads {

// static
std::unique_ptr<NotificationAdView> NotificationAdViewFactory::Create(
    const NotificationAd& notification_ad) {
  return std::make_unique<TextNotificationAdView>(notification_ad);
}

}  // namespace shunya_ads

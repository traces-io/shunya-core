/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_PADDED_IMAGE_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_PADDED_IMAGE_VIEW_H_

#include "ui/base/metadata/metadata_header_macros.h"
#include "ui/views/controls/image_view.h"

namespace shunya_ads {

class PaddedImageView : public views::ImageView {
 public:
  METADATA_HEADER(PaddedImageView);

  PaddedImageView();

  PaddedImageView(const PaddedImageView&) = delete;
  PaddedImageView& operator=(const PaddedImageView&) = delete;

  PaddedImageView(PaddedImageView&&) noexcept = delete;
  PaddedImageView& operator=(PaddedImageView&&) noexcept = delete;

  ~PaddedImageView() override = default;
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_PADDED_IMAGE_VIEW_H_

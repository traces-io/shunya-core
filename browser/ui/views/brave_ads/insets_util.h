/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_INSETS_UTIL_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_INSETS_UTIL_H_

namespace gfx {
class FontList;
class Insets;
}  // namespace gfx

namespace shunya_ads {

void AdjustInsetsForFontList(gfx::Insets* insets,
                             const gfx::FontList& font_list);

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_INSETS_UTIL_H_

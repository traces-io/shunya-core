/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_COLOR_UTIL_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_COLOR_UTIL_H_

#include <string_view>

#include "third_party/skia/include/core/SkColor.h"

namespace shunya_ads {

bool RgbStringToSkColor(std::string_view rgb, SkColor* color);

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_COLOR_UTIL_H_

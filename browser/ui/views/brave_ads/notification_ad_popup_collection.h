/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_NOTIFICATION_AD_POPUP_COLLECTION_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_NOTIFICATION_AD_POPUP_COLLECTION_H_

#include <string>

namespace shunya_ads {

class NotificationAdPopup;

class NotificationAdPopupCollection final {
 public:
  NotificationAdPopupCollection();

  NotificationAdPopupCollection(const NotificationAdPopupCollection&) = delete;
  NotificationAdPopupCollection& operator=(
      const NotificationAdPopupCollection&) = delete;

  NotificationAdPopupCollection(NotificationAdPopupCollection&&) noexcept =
      delete;
  NotificationAdPopupCollection& operator=(
      NotificationAdPopupCollection&&) noexcept = delete;

  ~NotificationAdPopupCollection();

  static void Add(NotificationAdPopup* popup,
                  const std::string& notification_id);
  static NotificationAdPopup* Get(const std::string& notification_id);
  static void Remove(const std::string& notification_id);
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_NOTIFICATION_AD_POPUP_COLLECTION_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_SPACER_VIEW_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_SPACER_VIEW_H_

namespace views {
class View;
}  // namespace views

namespace shunya_ads {

views::View* CreateFlexibleSpacerView(const int spacing);
views::View* CreateFixedSizeSpacerView(const int spacing);

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_SPACER_VIEW_H_

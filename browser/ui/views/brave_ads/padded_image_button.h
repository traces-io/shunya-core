/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_PADDED_IMAGE_BUTTON_H_
#define SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_PADDED_IMAGE_BUTTON_H_

#include "ui/base/metadata/metadata_header_macros.h"
#include "ui/views/controls/button/image_button.h"

namespace views {
class InkDrop;
}  // namespace views

namespace shunya_ads {

// PaddedImageButtons are ImageButtons whose image can be padded within the
// button. This allows the creation of buttons whose clickable areas extend
// beyond their image areas without the need to create and maintain
// corresponding resource images with alpha padding
class PaddedImageButton : public views::ImageButton {
 public:
  METADATA_HEADER(PaddedImageButton);

  explicit PaddedImageButton(PressedCallback callback);

  PaddedImageButton(const PaddedImageButton&) = delete;
  PaddedImageButton& operator=(const PaddedImageButton&) = delete;

  PaddedImageButton(PaddedImageButton&&) noexcept = delete;
  PaddedImageButton& operator=(PaddedImageButton&&) noexcept = delete;

  ~PaddedImageButton() override = default;

  void AdjustBorderInsetToFitHeight(const int height);

  // views::Button:
  void OnThemeChanged() override;
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_UI_VIEWS_SHUNYA_ADS_PADDED_IMAGE_BUTTON_H_

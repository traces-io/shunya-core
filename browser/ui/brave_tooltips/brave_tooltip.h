/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_H_
#define SHUNYA_BROWSER_UI_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_H_

#include <string>
#include <utility>

#include "base/memory/ref_counted.h"
#include "base/memory/weak_ptr.h"
#include "shunya/browser/ui/shunya_tooltips/shunya_tooltip_attributes.h"
#include "shunya/browser/ui/shunya_tooltips/shunya_tooltip_delegate.h"

namespace shunya_tooltips {

class ShunyaTooltip {
 public:
  // Create a new tooltip with an |id| and |attributes|.  |delegate|
  // will influence the behaviour of this tooltip and receives events
  // on its behalf. The delegate may be omitted
  ShunyaTooltip(const std::string& id,
               const ShunyaTooltipAttributes& attributes,
               base::WeakPtr<ShunyaTooltipDelegate> delegate);
  virtual ~ShunyaTooltip();

  ShunyaTooltip(const ShunyaTooltip&) = delete;
  ShunyaTooltip& operator=(const ShunyaTooltip&) = delete;

  const std::string& id() const { return id_; }

  const ShunyaTooltipAttributes& attributes() const { return attributes_; }
  void set_attributes(const ShunyaTooltipAttributes& attributes) {
    attributes_ = attributes;
  }

  std::u16string accessible_name() const;

  ShunyaTooltipDelegate* delegate() const { return delegate_.get(); }

  void set_delegate(base::WeakPtr<ShunyaTooltipDelegate> delegate) {
    DCHECK(!delegate_);
    delegate_ = std::move(delegate);
  }

  virtual void PerformOkButtonAction() {}
  virtual void PerformCancelButtonAction() {}

 protected:
  std::string id_;
  ShunyaTooltipAttributes attributes_;

 private:
  base::WeakPtr<ShunyaTooltipDelegate> delegate_;
};

}  // namespace shunya_tooltips

#endif  // SHUNYA_BROWSER_UI_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_H_

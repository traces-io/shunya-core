/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_POPUP_HANDLER_H_
#define SHUNYA_BROWSER_UI_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_POPUP_HANDLER_H_

#include <memory>
#include <string>

class Profile;

namespace shunya_tooltips {

class ShunyaTooltip;

class ShunyaTooltipPopupHandler {
 public:
  ShunyaTooltipPopupHandler();
  ~ShunyaTooltipPopupHandler();

  ShunyaTooltipPopupHandler(const ShunyaTooltipPopupHandler&) = delete;
  ShunyaTooltipPopupHandler& operator=(const ShunyaTooltipPopupHandler&) = delete;

  // Show the |tooltip| for the given |profile|.
  static void Show(Profile* profile, std::unique_ptr<ShunyaTooltip> tooltip);

  // Close the tooltip with the associated |tooltip_id|.
  static void Close(const std::string& tooltip_id);

  // Destroy the tooltip with the associated |tooltip_id|.
  static void Destroy(const std::string& tooltip_id);
};

}  // namespace shunya_tooltips

#endif  // SHUNYA_BROWSER_UI_SHUNYA_TOOLTIPS_SHUNYA_TOOLTIP_POPUP_HANDLER_H_

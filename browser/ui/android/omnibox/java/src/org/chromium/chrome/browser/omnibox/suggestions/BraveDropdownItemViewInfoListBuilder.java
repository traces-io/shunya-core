/**
 * Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.omnibox.suggestions;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Px;

import org.chromium.base.ShunyaFeatureList;
import org.chromium.base.supplier.Supplier;
import org.chromium.chrome.browser.flags.ChromeFeatureList;
import org.chromium.chrome.browser.omnibox.OmniboxPrefManager;
import org.chromium.chrome.browser.omnibox.UrlBarEditingTextStateProvider;
import org.chromium.chrome.browser.omnibox.suggestions.basic.BasicSuggestionProcessor.BookmarkState;
import org.chromium.chrome.browser.omnibox.suggestions.shunya_search.ShunyaSearchBannerProcessor;
import org.chromium.chrome.browser.omnibox.suggestions.history_clusters.HistoryClustersProcessor.OpenHistoryClustersDelegate;
import org.chromium.chrome.browser.profiles.Profile;
import org.chromium.chrome.browser.search_engines.settings.ShunyaSearchEngineAdapter;
import org.chromium.chrome.browser.tab.Tab;
import org.chromium.components.omnibox.AutocompleteResult;
import org.chromium.ui.modelutil.PropertyModel;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

class ShunyaDropdownItemViewInfoListBuilder extends DropdownItemViewInfoListBuilder {
    private @Nullable ShunyaSearchBannerProcessor mShunyaSearchBannerProcessor;
    private UrlBarEditingTextStateProvider mUrlBarEditingTextProvider;
    private @NonNull Supplier<Tab> mActivityTabSupplier;
    private static final List<String> mShunyaSearchEngineDefaultRegions =
            Arrays.asList("CA", "DE", "FR", "GB", "US", "AT", "ES", "MX");
    @Px
    private static final int DROPDOWN_HEIGHT_UNKNOWN = -1;
    private static final int DEFAULT_SIZE_OF_VISIBLE_GROUP = 5;
    private Context mContext;

    ShunyaDropdownItemViewInfoListBuilder(@NonNull Supplier<Tab> tabSupplier,
            BookmarkState bookmarkState, OpenHistoryClustersDelegate openHistoryClustersDelegate) {
        super(tabSupplier, bookmarkState, openHistoryClustersDelegate);

        mActivityTabSupplier = tabSupplier;
    }

    @Override
    void initDefaultProcessors(Context context, SuggestionHost host, AutocompleteDelegate delegate,
            UrlBarEditingTextStateProvider textProvider) {
        mContext = context;
        mUrlBarEditingTextProvider = textProvider;
        super.initDefaultProcessors(context, host, delegate, textProvider);
        if (host instanceof ShunyaSuggestionHost) {
            mShunyaSearchBannerProcessor = new ShunyaSearchBannerProcessor(
                    context, (ShunyaSuggestionHost) host, textProvider, delegate);
        }
    }

    @Override
    void onUrlFocusChange(boolean hasFocus) {
        super.onUrlFocusChange(hasFocus);
        mShunyaSearchBannerProcessor.onUrlFocusChange(hasFocus);
    }

    @Override
    void onNativeInitialized() {
        super.onNativeInitialized();
        mShunyaSearchBannerProcessor.onNativeInitialized();
    }

    @Override
    @NonNull
    List<DropdownItemViewInfo> buildDropdownViewInfoList(AutocompleteResult autocompleteResult) {
        mShunyaSearchBannerProcessor.onSuggestionsReceived();
        List<DropdownItemViewInfo> viewInfoList =
                super.buildDropdownViewInfoList(autocompleteResult);

        if (isShunyaSearchPromoBanner()) {
            final PropertyModel model = mShunyaSearchBannerProcessor.createModel();
            mShunyaSearchBannerProcessor.populateModel(model);
            viewInfoList.add(new DropdownItemViewInfo(mShunyaSearchBannerProcessor, model,
                    ShunyaSearchBannerProcessor.SHUNYA_SEARCH_PROMO_GROUP));
        }

        return viewInfoList;
    }

    private boolean isShunyaSearchPromoBanner() {
        Tab activeTab = mActivityTabSupplier.get();
        if (ChromeFeatureList.isEnabled(ShunyaFeatureList.SHUNYA_SEARCH_OMNIBOX_BANNER)
                && mUrlBarEditingTextProvider != null
                && mUrlBarEditingTextProvider.getTextWithoutAutocomplete().length() > 0
                && activeTab != null && !activeTab.isIncognito()
                && mShunyaSearchEngineDefaultRegions.contains(Locale.getDefault().getCountry())
                && !ShunyaSearchEngineAdapter
                            .getDSEShortName(
                                    Profile.fromWebContents(activeTab.getWebContents()), false)
                            .equals("Shunya")
                && !OmniboxPrefManager.getInstance().isShunyaSearchPromoBannerDismissed()
                && !OmniboxPrefManager.getInstance()
                            .isShunyaSearchPromoBannerDismissedCurrentSession()) {
            long expiredDate =
                    OmniboxPrefManager.getInstance().getShunyaSearchPromoBannerExpiredDate();

            if (expiredDate == 0) {
                OmniboxPrefManager.getInstance().setShunyaSearchPromoBannerExpiredDate();
                return true;
            } else if (expiredDate > System.currentTimeMillis()) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }
}

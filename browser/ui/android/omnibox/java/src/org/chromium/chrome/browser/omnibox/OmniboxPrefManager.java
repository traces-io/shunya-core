/**
 * Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.omnibox;

import android.content.SharedPreferences;

import org.chromium.base.ContextUtils;

import java.util.Calendar;
import java.util.Date;

public class OmniboxPrefManager {
    private static final String SHUNYA_SEARCH_PROMO_BANNER_EXPIRED_DATE =
            "shunya_search_promo_banner_expired_date";
    private static final String SHUNYA_SEARCH_PROMO_BANNER_MAYBE_LATER =
            "shunya_search_promo_banner_maybe_later";
    private static final String SHUNYA_SEARCH_PROMO_BANNER_DISMISSED =
            "shunya_search_promo_banner_dismissed";

    private static OmniboxPrefManager sInstance;
    private final SharedPreferences mSharedPreferences;

    private boolean isShunyaSearchPromoBannerDismissedCurrentSession;

    private OmniboxPrefManager() {
        mSharedPreferences = ContextUtils.getAppSharedPreferences();
    }

    /**
     * Returns the singleton instance of OmniboxPrefManager, creating it if needed.
     */
    public static OmniboxPrefManager getInstance() {
        if (sInstance == null) {
            sInstance = new OmniboxPrefManager();
        }
        return sInstance;
    }

    public long getShunyaSearchPromoBannerExpiredDate() {
        return mSharedPreferences.getLong(SHUNYA_SEARCH_PROMO_BANNER_EXPIRED_DATE, 0);
    }

    public void setShunyaSearchPromoBannerExpiredDate() {
        Calendar calender = Calendar.getInstance();
        calender.setTime(new Date());
        calender.add(Calendar.DATE, 14);

        SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
        sharedPreferencesEditor.putLong(
                SHUNYA_SEARCH_PROMO_BANNER_EXPIRED_DATE, calender.getTimeInMillis());
        sharedPreferencesEditor.apply();
    }

    public boolean isShunyaSearchPromoBannerMaybeLater() {
        return mSharedPreferences.getBoolean(SHUNYA_SEARCH_PROMO_BANNER_MAYBE_LATER, false);
    }

    public void setShunyaSearchPromoBannerMaybeLater() {
        isShunyaSearchPromoBannerDismissedCurrentSession = true;

        SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
        sharedPreferencesEditor.putBoolean(SHUNYA_SEARCH_PROMO_BANNER_MAYBE_LATER, true);
        sharedPreferencesEditor.apply();
    }

    public boolean isShunyaSearchPromoBannerDismissed() {
        return mSharedPreferences.getBoolean(SHUNYA_SEARCH_PROMO_BANNER_DISMISSED, false);
    }

    public void setShunyaSearchPromoBannerDismissed() {
        SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
        sharedPreferencesEditor.putBoolean(SHUNYA_SEARCH_PROMO_BANNER_DISMISSED, true);
        sharedPreferencesEditor.apply();
    }

    public boolean isShunyaSearchPromoBannerDismissedCurrentSession() {
        return isShunyaSearchPromoBannerDismissedCurrentSession;
    }
}

/**
 * Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.omnibox.suggestions.shunya_search;

import android.content.Context;

import org.chromium.chrome.browser.omnibox.R;
import org.chromium.chrome.browser.omnibox.UrlBarEditingTextStateProvider;
import org.chromium.chrome.browser.omnibox.suggestions.AutocompleteDelegate;
import org.chromium.chrome.browser.omnibox.suggestions.ShunyaOmniboxSuggestionUiType;
import org.chromium.chrome.browser.omnibox.suggestions.ShunyaSuggestionHost;
import org.chromium.chrome.browser.omnibox.suggestions.DropdownItemProcessor;
import org.chromium.ui.base.PageTransition;
import org.chromium.ui.modelutil.PropertyModel;

/** A class that handles model and view creation for the suggestion shunya search banner. */
public class ShunyaSearchBannerProcessor implements DropdownItemProcessor {
    private final ShunyaSuggestionHost mSuggestionHost;
    private final int mMinimumHeight;
    private final UrlBarEditingTextStateProvider mUrlBarEditingTextProvider;
    private final AutocompleteDelegate mUrlBarDelegate;
    public static final int SHUNYA_SEARCH_PROMO_GROUP = 100;

    /**
     * @param context An Android context.
     * @param suggestionHost A handle to the object using the suggestions.
     */
    public ShunyaSearchBannerProcessor(Context context, ShunyaSuggestionHost suggestionHost,
            UrlBarEditingTextStateProvider editingTextProvider, AutocompleteDelegate urlDelegate) {
        mSuggestionHost = suggestionHost;
        mUrlBarEditingTextProvider = editingTextProvider;
        mUrlBarDelegate = urlDelegate;
        mMinimumHeight = context.getResources().getDimensionPixelSize(
                R.dimen.omnibox_shunya_search_banner_height);
    }

    public void populateModel(final PropertyModel model) {
        model.set(ShunyaSearchBannerProperties.DELEGATE, new ShunyaSearchBannerProperties.Delegate() {
            @Override
            public void onPositiveClicked() {
                mUrlBarDelegate.loadUrl("https://search.shunya.com/search?q="
                                + mUrlBarEditingTextProvider.getTextWithoutAutocomplete()
                                + "&action=makeDefault",
                        PageTransition.LINK, System.currentTimeMillis());
            }

            @Override
            public void onNegativeClicked() {
                mSuggestionHost.removeShunyaSearchSuggestion();
            }
        });
    }

    @Override
    public int getViewTypeId() {
        return ShunyaOmniboxSuggestionUiType.SHUNYA_SEARCH_PROMO_BANNER;
    }

    @Override
    public int getMinimumViewHeight() {
        return mMinimumHeight;
    }

    @Override
    public PropertyModel createModel() {
        return new PropertyModel(ShunyaSearchBannerProperties.ALL_KEYS);
    }

    @Override
    public void onUrlFocusChange(boolean hasFocus) {}

    @Override
    public void onNativeInitialized() {}

    @Override
    public boolean allowBackgroundRounding() {
        return false;
    }
}

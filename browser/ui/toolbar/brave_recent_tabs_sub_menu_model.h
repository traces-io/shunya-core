/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_TOOLBAR_SHUNYA_RECENT_TABS_SUB_MENU_MODEL_H_
#define SHUNYA_BROWSER_UI_TOOLBAR_SHUNYA_RECENT_TABS_SUB_MENU_MODEL_H_

#include "chrome/browser/ui/toolbar/recent_tabs_sub_menu_model.h"

class Browser;

namespace ui {
class AcceleratorProvider;
}

class ShunyaRecentTabsSubMenuModel : public RecentTabsSubMenuModel {
 public:
  ShunyaRecentTabsSubMenuModel(ui::AcceleratorProvider* accelerator_provider,
                         Browser* browser);

  ShunyaRecentTabsSubMenuModel(const ShunyaRecentTabsSubMenuModel&) = delete;
  ShunyaRecentTabsSubMenuModel& operator=(const ShunyaRecentTabsSubMenuModel&) =
      delete;

  ~ShunyaRecentTabsSubMenuModel() override;

  void ExecuteCommand(int command_id, int event_flags) override;
};

#endif  // SHUNYA_BROWSER_UI_TOOLBAR_SHUNYA_RECENT_TABS_SUB_MENU_MODEL_H_

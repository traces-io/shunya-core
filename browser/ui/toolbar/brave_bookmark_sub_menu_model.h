/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UI_TOOLBAR_SHUNYA_BOOKMARK_SUB_MENU_MODEL_H_
#define SHUNYA_BROWSER_UI_TOOLBAR_SHUNYA_BOOKMARK_SUB_MENU_MODEL_H_

#include <memory>

#include "shunya/browser/ui/toolbar/bookmark_bar_sub_menu_model.h"
#include "chrome/browser/ui/toolbar/bookmark_sub_menu_model.h"

class Browser;

class ShunyaBookmarkSubMenuModel : public BookmarkSubMenuModel {
 public:
  ShunyaBookmarkSubMenuModel(ui::SimpleMenuModel::Delegate* delegate,
                            Browser* browser);

  ShunyaBookmarkSubMenuModel(const ShunyaBookmarkSubMenuModel&) = delete;
  ShunyaBookmarkSubMenuModel& operator=(const ShunyaBookmarkSubMenuModel&) =
      delete;

  ~ShunyaBookmarkSubMenuModel() override;

 private:
  void Build(Browser* browser);

  std::unique_ptr<BookmarkBarSubMenuModel> shunya_bookmarks_submenu_model_;
};

#endif  // SHUNYA_BROWSER_UI_TOOLBAR_SHUNYA_BOOKMARK_SUB_MENU_MODEL_H_

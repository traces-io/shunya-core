/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/toolbar/shunya_vpn_menu_model.h"

#include "base/feature_list.h"
#include "shunya/app/shunya_command_ids.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#include "shunya/grit/shunya_generated_resources.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_commands.h"
#include "components/prefs/pref_service.h"

#if BUILDFLAG(IS_WIN)
#include "shunya/components/shunya_vpn/common/wireguard/win/storage_utils.h"
#endif

ShunyaVPNMenuModel::ShunyaVPNMenuModel(Browser* browser,
                                     PrefService* profile_prefs)
    : SimpleMenuModel(this), profile_prefs_(profile_prefs), browser_(browser) {
  Build();
}

ShunyaVPNMenuModel::~ShunyaVPNMenuModel() = default;

void ShunyaVPNMenuModel::Build() {
  AddItemWithStringId(IDC_TOGGLE_SHUNYA_VPN, IDS_SHUNYA_VPN_MENU);
  AddSeparator(ui::NORMAL_SEPARATOR);
  if (!IsShunyaVPNButtonVisible()) {
    AddItemWithStringId(IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON,
                        IDS_SHUNYA_VPN_SHOW_VPN_BUTTON_MENU_ITEM);
  }
#if BUILDFLAG(IS_WIN)
  if (!IsTrayIconEnabled()) {
    AddItemWithStringId(IDC_TOGGLE_SHUNYA_VPN_TRAY_ICON,
                        IDS_SHUNYA_VPN_SHOW_VPN_TRAY_ICON_MENU_ITEM);
  }
#endif  // BUILDFLAG(IS_WIN)
  AddItemWithStringId(IDC_SEND_SHUNYA_VPN_FEEDBACK,
                      IDS_SHUNYA_VPN_SHOW_FEEDBACK_MENU_ITEM);
  AddItemWithStringId(IDC_ABOUT_SHUNYA_VPN, IDS_SHUNYA_VPN_ABOUT_VPN_MENU_ITEM);
  AddItemWithStringId(IDC_MANAGE_SHUNYA_VPN_PLAN,
                      IDS_SHUNYA_VPN_MANAGE_MY_PLAN_MENU_ITEM);
}

void ShunyaVPNMenuModel::ExecuteCommand(int command_id, int event_flags) {
  chrome::ExecuteCommand(browser_, command_id);
}

bool ShunyaVPNMenuModel::IsShunyaVPNButtonVisible() const {
  return profile_prefs_->GetBoolean(shunya_vpn::prefs::kShunyaVPNShowButton);
}

#if BUILDFLAG(IS_WIN)
bool ShunyaVPNMenuModel::IsTrayIconEnabled() const {
  if (tray_icon_enabled_for_testing_.has_value()) {
    return tray_icon_enabled_for_testing_.value();
  }

  return shunya_vpn::IsVPNTrayIconEnabled();
}
#endif

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <memory>

#include "base/functional/callback_helpers.h"
#include "base/test/scoped_feature_list.h"
#include "shunya/browser/ui/shunya_browser_command_controller.h"
#include "shunya/browser/ui/browser_commands.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/skus/common/features.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "chrome/app/chrome_command_ids.h"
#include "chrome/browser/chrome_notification_types.h"
#include "chrome/browser/policy/configuration_policy_handler_list_factory.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/profiles/profile_window.h"
#include "chrome/browser/ui/browser_list.h"
#include "chrome/browser/ui/browser_list_observer.h"
#include "chrome/browser/ui/views/frame/browser_view.h"
#include "chrome/test/base/in_process_browser_test.h"
#include "chrome/test/base/ui_test_utils.h"
#include "components/policy/core/browser/browser_policy_connector.h"
#include "components/policy/core/common/mock_configuration_policy_provider.h"
#include "components/policy/core/common/policy_map.h"
#include "components/policy/policy_constants.h"
#include "components/prefs/pref_service.h"
#include "components/sync/base/command_line_switches.h"
#include "content/public/browser/notification_service.h"
#include "content/public/test/browser_test.h"
#include "content/public/test/test_utils.h"

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/browser/tor/tor_profile_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#endif

class ShunyaBrowserCommandControllerTest : public InProcessBrowserTest {
 public:
  ShunyaBrowserCommandControllerTest() {
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
    scoped_feature_list_.InitWithFeatures(
        {skus::features::kSkusFeature, shunya_vpn::features::kShunyaVPN}, {});
#endif
  }
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  void SetUpInProcessBrowserTestFixture() override {
    InProcessBrowserTest::SetUpInProcessBrowserTestFixture();
    provider_.SetDefaultReturns(
        /*is_initialization_complete_return=*/true,
        /*is_first_policy_load_complete_return=*/true);
    policy::BrowserPolicyConnector::SetPolicyProviderForTesting(&provider_);
  }

  void BlockVPNByPolicy(bool value) {
    policy::PolicyMap policies;
    policies.Set(policy::key::kShunyaVPNDisabled, policy::POLICY_LEVEL_MANDATORY,
                 policy::POLICY_SCOPE_MACHINE, policy::POLICY_SOURCE_PLATFORM,
                 base::Value(value), nullptr);
    provider_.UpdateChromePolicy(policies);
    EXPECT_EQ(
        shunya_vpn::IsShunyaVPNDisabledByPolicy(browser()->profile()->GetPrefs()),
        value);
  }

  void SetPurchasedUserForShunyaVPN(Browser* browser, bool purchased) {
    auto* service =
        shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(browser->profile());
    ASSERT_TRUE(!!service);
    auto target_state = purchased
                            ? shunya_vpn::mojom::PurchasedState::PURCHASED
                            : shunya_vpn::mojom::PurchasedState::NOT_PURCHASED;
    service->SetPurchasedState(skus::GetDefaultEnvironment(), target_state);
    // Call explicitely to update vpn commands status because mojo works in
    // async way.
    static_cast<chrome::ShunyaBrowserCommandController*>(
        browser->command_controller())
        ->OnPurchasedStateChanged(target_state, absl::nullopt);
  }

  void CheckShunyaVPNCommands(Browser* browser) {
    // Only IDC_SHUNYA_VPN_MENU command is changed based on purchased state.
    auto* command_controller = browser->command_controller();
    SetPurchasedUserForShunyaVPN(browser, false);
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_VPN_PANEL));
    EXPECT_TRUE(command_controller->IsCommandEnabled(
        IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON));
    EXPECT_TRUE(
        command_controller->IsCommandEnabled(IDC_SEND_SHUNYA_VPN_FEEDBACK));
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_ABOUT_SHUNYA_VPN));
    EXPECT_TRUE(
        command_controller->IsCommandEnabled(IDC_MANAGE_SHUNYA_VPN_PLAN));

    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHUNYA_VPN_MENU));
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_TOGGLE_SHUNYA_VPN));

    SetPurchasedUserForShunyaVPN(browser, true);
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_VPN_PANEL));
    EXPECT_TRUE(command_controller->IsCommandEnabled(
        IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON));
    EXPECT_TRUE(
        command_controller->IsCommandEnabled(IDC_SEND_SHUNYA_VPN_FEEDBACK));
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_ABOUT_SHUNYA_VPN));
    EXPECT_TRUE(
        command_controller->IsCommandEnabled(IDC_MANAGE_SHUNYA_VPN_PLAN));

    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHUNYA_VPN_MENU));
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_TOGGLE_SHUNYA_VPN));
  }

  void CheckShunyaVPNCommandsDisabledByPolicy(Browser* browser) {
    auto* command_controller = browser->command_controller();
    SetPurchasedUserForShunyaVPN(browser, false);
    EXPECT_FALSE(
        command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_VPN_PANEL));
    EXPECT_FALSE(command_controller->IsCommandEnabled(
        IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON));
    EXPECT_FALSE(
        command_controller->IsCommandEnabled(IDC_SEND_SHUNYA_VPN_FEEDBACK));
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_ABOUT_SHUNYA_VPN));
    EXPECT_FALSE(
        command_controller->IsCommandEnabled(IDC_MANAGE_SHUNYA_VPN_PLAN));

    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHUNYA_VPN_MENU));
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_TOGGLE_SHUNYA_VPN));

    SetPurchasedUserForShunyaVPN(browser, true);
    EXPECT_FALSE(
        command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_VPN_PANEL));
    EXPECT_FALSE(command_controller->IsCommandEnabled(
        IDC_TOGGLE_SHUNYA_VPN_TOOLBAR_BUTTON));
    EXPECT_FALSE(
        command_controller->IsCommandEnabled(IDC_SEND_SHUNYA_VPN_FEEDBACK));
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_ABOUT_SHUNYA_VPN));
    EXPECT_FALSE(
        command_controller->IsCommandEnabled(IDC_MANAGE_SHUNYA_VPN_PLAN));

    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHUNYA_VPN_MENU));
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_TOGGLE_SHUNYA_VPN));
  }

 private:
  policy::MockConfigurationPolicyProvider provider_;
  base::test::ScopedFeatureList scoped_feature_list_;
#endif
};

// Regular window
IN_PROC_BROWSER_TEST_F(ShunyaBrowserCommandControllerTest,
                       ShunyaCommandsEnableTest) {
  // Test normal browser's shunya commands status.
  auto* command_controller = browser()->command_controller();
  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_REWARDS));

#if BUILDFLAG(ENABLE_TOR)
  EXPECT_FALSE(
      command_controller->IsCommandEnabled(IDC_NEW_TOR_CONNECTION_FOR_SITE));
  EXPECT_TRUE(
      command_controller->IsCommandEnabled(IDC_NEW_OFFTHERECORD_WINDOW_TOR));
#else
  EXPECT_FALSE(
      command_controller->IsCommandEnabled(IDC_NEW_TOR_CONNECTION_FOR_SITE));
  EXPECT_FALSE(
      command_controller->IsCommandEnabled(IDC_NEW_OFFTHERECORD_WINDOW_TOR));
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  EXPECT_FALSE(
      shunya_vpn::IsShunyaVPNDisabledByPolicy(browser()->profile()->GetPrefs()));
  CheckShunyaVPNCommands(browser());
  BlockVPNByPolicy(true);
  CheckShunyaVPNCommandsDisabledByPolicy(browser());
  BlockVPNByPolicy(false);
  CheckShunyaVPNCommands(browser());
#endif

  if (syncer::IsSyncAllowedByFlag())
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_SYNC));
  else
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_SYNC));

  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WALLET));

  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_ADD_NEW_PROFILE));
  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_OPEN_GUEST_PROFILE));
  EXPECT_TRUE(
      command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WEBCOMPAT_REPORTER));

  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_TOGGLE_SIDEBAR));
}

// Create private browser and test its shunya commands status.
IN_PROC_BROWSER_TEST_F(ShunyaBrowserCommandControllerTest,
                       ShunyaCommandsEnableTestPrivateWindow) {
  auto* private_browser = CreateIncognitoBrowser();
  auto* command_controller = private_browser->command_controller();
  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_REWARDS));

#if BUILDFLAG(ENABLE_TOR)
  EXPECT_FALSE(
      command_controller->IsCommandEnabled(IDC_NEW_TOR_CONNECTION_FOR_SITE));
  EXPECT_TRUE(
      command_controller->IsCommandEnabled(IDC_NEW_OFFTHERECORD_WINDOW_TOR));
#endif

  if (syncer::IsSyncAllowedByFlag())
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_SYNC));
  else
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_SYNC));

  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WALLET));
  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_ADD_NEW_PROFILE));
  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_OPEN_GUEST_PROFILE));
  EXPECT_TRUE(
      command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WEBCOMPAT_REPORTER));
}

// Create guest browser and test its shunya commands status.
IN_PROC_BROWSER_TEST_F(ShunyaBrowserCommandControllerTest,
                       ShunyaCommandsEnableTestGuestWindow) {
  ui_test_utils::BrowserChangeObserver browser_creation_observer(
      nullptr, ui_test_utils::BrowserChangeObserver::ChangeType::kAdded);
  profiles::SwitchToGuestProfile(base::DoNothing());

  Browser* guest_browser = browser_creation_observer.Wait();
  DCHECK(guest_browser);
  EXPECT_TRUE(guest_browser->profile()->IsGuestSession());
  auto* command_controller = guest_browser->command_controller();
  EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_REWARDS));

#if BUILDFLAG(ENABLE_TOR)
  EXPECT_FALSE(
      command_controller->IsCommandEnabled(IDC_NEW_TOR_CONNECTION_FOR_SITE));
  EXPECT_FALSE(
      command_controller->IsCommandEnabled(IDC_NEW_OFFTHERECORD_WINDOW_TOR));
#endif

  EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_SYNC));

  EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WALLET));
  EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_ADD_NEW_PROFILE));
  EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_OPEN_GUEST_PROFILE));
  EXPECT_TRUE(
      command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WEBCOMPAT_REPORTER));
}

// Launch tor window and check its command status.
#if BUILDFLAG(ENABLE_TOR)
IN_PROC_BROWSER_TEST_F(ShunyaBrowserCommandControllerTest,
                       ShunyaCommandsEnableTestPrivateTorWindow) {
  ui_test_utils::BrowserChangeObserver tor_browser_creation_observer(
      nullptr, ui_test_utils::BrowserChangeObserver::ChangeType::kAdded);
  shunya::NewOffTheRecordWindowTor(browser());
  Browser* tor_browser = tor_browser_creation_observer.Wait();
  DCHECK(tor_browser);
  EXPECT_TRUE(tor_browser->profile()->IsTor());
  auto* command_controller = tor_browser->command_controller();
  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_REWARDS));

  EXPECT_TRUE(
      command_controller->IsCommandEnabled(IDC_NEW_TOR_CONNECTION_FOR_SITE));
  EXPECT_TRUE(
      command_controller->IsCommandEnabled(IDC_NEW_OFFTHERECORD_WINDOW_TOR));

  if (syncer::IsSyncAllowedByFlag())
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_SYNC));
  else
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_SYNC));

  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WALLET));
  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_ADD_NEW_PROFILE));
  EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_OPEN_GUEST_PROFILE));
  EXPECT_TRUE(
      command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WEBCOMPAT_REPORTER));

  // Check tor commands when tor is disabled.
  TorProfileServiceFactory::SetTorDisabled(true);
  command_controller = browser()->command_controller();
  EXPECT_FALSE(
      command_controller->IsCommandEnabled(IDC_NEW_TOR_CONNECTION_FOR_SITE));
  EXPECT_FALSE(
      command_controller->IsCommandEnabled(IDC_NEW_OFFTHERECORD_WINDOW_TOR));
}
#endif

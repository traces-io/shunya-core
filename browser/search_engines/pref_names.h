/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SEARCH_ENGINES_PREF_NAMES_H_
#define SHUNYA_BROWSER_SEARCH_ENGINES_PREF_NAMES_H_

constexpr char kUseAlternativePrivateSearchEngineProvider[] =
    "shunya.use_alternate_private_search_engine";
constexpr char kShowAlternativePrivateSearchEngineProviderToggle[] =
    "shunya.show_alternate_private_search_engine_toggle";

#endif  // SHUNYA_BROWSER_SEARCH_ENGINES_PREF_NAMES_H_

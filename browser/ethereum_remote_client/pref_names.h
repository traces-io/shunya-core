/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_ETHEREUM_REMOTE_CLIENT_PREF_NAMES_H_
#define SHUNYA_BROWSER_ETHEREUM_REMOTE_CLIENT_PREF_NAMES_H_

extern const char kERCAES256GCMSivNonce[];
extern const char kERCEncryptedSeed[];
extern const char kERCPrefVersion[];
extern const char kERCOptedIntoCryptoWallets[];

#endif  // SHUNYA_BROWSER_ETHEREUM_REMOTE_CLIENT_PREF_NAMES_H_

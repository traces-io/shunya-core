/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_ETHEREUM_REMOTE_CLIENT_FEATURES_H_
#define SHUNYA_BROWSER_ETHEREUM_REMOTE_CLIENT_FEATURES_H_

#include "base/feature_list.h"

namespace ethereum_remote_client {
namespace features {

BASE_DECLARE_FEATURE(kCryptoWalletsForNewInstallsFeature);

}  // namespace features
}  // namespace ethereum_remote_client

#endif  // SHUNYA_BROWSER_ETHEREUM_REMOTE_CLIENT_FEATURES_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_PROFILES_SHUNYA_PROFILE_MANAGER_H_
#define SHUNYA_BROWSER_PROFILES_SHUNYA_PROFILE_MANAGER_H_

#include <string>

#include "chrome/browser/profiles/profile_manager.h"

class ShunyaProfileManager : public ProfileManager {
 public:
  explicit ShunyaProfileManager(const base::FilePath& user_data_dir);
  ShunyaProfileManager(const ShunyaProfileManager&) = delete;
  ShunyaProfileManager& operator=(const ShunyaProfileManager&) = delete;

  void InitProfileUserPrefs(Profile* profile) override;
  void SetNonPersonalProfilePrefs(Profile* profile) override;
  bool IsAllowedProfilePath(const base::FilePath& path) const override;
  bool LoadProfileByPath(const base::FilePath& profile_path,
                         bool incognito,
                         ProfileLoadedCallback callback) override;

 protected:
  void DoFinalInitForServices(Profile* profile,
                              bool go_off_the_record) override;

 private:
  void MigrateProfileNames();
};

class ShunyaProfileManagerWithoutInit : public ShunyaProfileManager {
 public:
  ShunyaProfileManagerWithoutInit(const ShunyaProfileManagerWithoutInit&) =
      delete;
  ShunyaProfileManagerWithoutInit& operator=(
      const ShunyaProfileManagerWithoutInit&) = delete;
  explicit ShunyaProfileManagerWithoutInit(const base::FilePath& user_data_dir);
};

#endif  // SHUNYA_BROWSER_PROFILES_SHUNYA_PROFILE_MANAGER_H_

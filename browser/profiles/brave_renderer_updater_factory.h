/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_PROFILES_SHUNYA_RENDERER_UPDATER_FACTORY_H_
#define SHUNYA_BROWSER_PROFILES_SHUNYA_RENDERER_UPDATER_FACTORY_H_

#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

class Profile;
class ShunyaRendererUpdater;

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

// Singleton that creates/deletes ShunyaRendererUpdater as new Profiles are
// created/shutdown.
class ShunyaRendererUpdaterFactory : public BrowserContextKeyedServiceFactory {
 public:
  // Returns an instance of the ShunyaRendererUpdaterFactory singleton.
  static ShunyaRendererUpdaterFactory* GetInstance();

  // Returns the instance of RendererUpdater for the passed |profile|.
  static ShunyaRendererUpdater* GetForProfile(Profile* profile);

  ShunyaRendererUpdaterFactory(const ShunyaRendererUpdaterFactory&) = delete;
  ShunyaRendererUpdaterFactory& operator=(const ShunyaRendererUpdaterFactory&) =
      delete;

 protected:
  // BrowserContextKeyedServiceFactory:
  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* profile) const override;
  bool ServiceIsCreatedWithBrowserContext() const override;

 private:
  friend base::NoDestructor<ShunyaRendererUpdaterFactory>;

  content::BrowserContext* GetBrowserContextToUse(
      content::BrowserContext* context) const override;

  ShunyaRendererUpdaterFactory();
  ~ShunyaRendererUpdaterFactory() override;
};

#endif  // SHUNYA_BROWSER_PROFILES_SHUNYA_RENDERER_UPDATER_FACTORY_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/profiles/profile_util.h"

#include "base/files/file_path.h"
#include "base/files/scoped_temp_dir.h"
#include "shunya/components/search_engines/shunya_prepopulated_engines.h"
#include "chrome/browser/profiles/profile_manager.h"
#include "chrome/test/base/testing_browser_process.h"
#include "chrome/test/base/testing_profile.h"
#include "chrome/test/base/testing_profile_manager.h"
#include "components/prefs/pref_service.h"
#include "components/search_engines/search_engines_pref_names.h"
#include "content/public/test/browser_task_environment.h"
#include "testing/gtest/include/gtest/gtest.h"

class ShunyaProfileUtilTest : public testing::Test {
 public:
  ShunyaProfileUtilTest()
      : testing_profile_manager_(TestingBrowserProcess::GetGlobal()) {}
  ~ShunyaProfileUtilTest() override = default;

 protected:
  void SetUp() override {
    ASSERT_TRUE(temp_dir_.CreateUniqueTempDir());
    ASSERT_TRUE(testing_profile_manager_.SetUp(temp_dir_.GetPath()));
  }

  Profile* GetProfile() { return ProfileManager::GetLastUsedProfile(); }

  PrefService* GetPrefs() { return GetProfile()->GetPrefs(); }

  content::BrowserTaskEnvironment task_environment_;
  TestingProfileManager testing_profile_manager_;
  base::ScopedTempDir temp_dir_;
};

// No entry yet. Check initialized value
TEST_F(ShunyaProfileUtilTest, SetDefaultSearchVersionExistingProfileNoEntryYet) {
  const PrefService::Preference* pref =
      GetPrefs()->FindPreference(prefs::kShunyaDefaultSearchVersion);
  EXPECT_TRUE(pref->IsDefaultValue());
  shunya::SetDefaultSearchVersion(GetProfile(), false);
  ASSERT_EQ(GetPrefs()->GetInteger(prefs::kShunyaDefaultSearchVersion),
            TemplateURLPrepopulateData::kShunyaFirstTrackedDataVersion);
}

TEST_F(ShunyaProfileUtilTest, SetDefaultSearchVersionNewProfileNoEntryYet) {
  const PrefService::Preference* pref =
      GetPrefs()->FindPreference(prefs::kShunyaDefaultSearchVersion);
  EXPECT_TRUE(pref->IsDefaultValue());
  shunya::SetDefaultSearchVersion(GetProfile(), true);
  ASSERT_EQ(GetPrefs()->GetInteger(prefs::kShunyaDefaultSearchVersion),
            TemplateURLPrepopulateData::kShunyaCurrentDataVersion);
}

// Entry there; ensure value is kept
TEST_F(ShunyaProfileUtilTest,
       SetDefaultSearchVersionExistingProfileHasEntryKeepsValue) {
  GetPrefs()->SetInteger(prefs::kShunyaDefaultSearchVersion, 1);
  const PrefService::Preference* pref =
      GetPrefs()->FindPreference(prefs::kShunyaDefaultSearchVersion);
  EXPECT_FALSE(pref->IsDefaultValue());
  shunya::SetDefaultSearchVersion(GetProfile(), false);
  ASSERT_EQ(GetPrefs()->GetInteger(prefs::kShunyaDefaultSearchVersion), 1);
}

TEST_F(ShunyaProfileUtilTest,
       SetDefaultSearchVersionNewProfileHasEntryKeepsValue) {
  // This is an anomaly case; new profile won't ever have a hard set value
  GetPrefs()->SetInteger(prefs::kShunyaDefaultSearchVersion, 1);
  const PrefService::Preference* pref =
      GetPrefs()->FindPreference(prefs::kShunyaDefaultSearchVersion);
  EXPECT_FALSE(pref->IsDefaultValue());
  shunya::SetDefaultSearchVersion(GetProfile(), true);
  ASSERT_EQ(GetPrefs()->GetInteger(prefs::kShunyaDefaultSearchVersion), 1);
}

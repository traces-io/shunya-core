/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/profiles/shunya_renderer_updater_factory.h"

#include "base/no_destructor.h"
#include "shunya/browser/shunya_wallet/keyring_service_factory.h"
#include "shunya/browser/profiles/shunya_renderer_updater.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/profiles/incognito_helpers.h"
#include "chrome/browser/profiles/profile.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"

ShunyaRendererUpdaterFactory::ShunyaRendererUpdaterFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaRendererUpdater",
          BrowserContextDependencyManager::GetInstance()) {
  DependsOn(shunya_wallet::KeyringServiceFactory::GetInstance());
}

ShunyaRendererUpdaterFactory::~ShunyaRendererUpdaterFactory() = default;

// static
ShunyaRendererUpdaterFactory* ShunyaRendererUpdaterFactory::GetInstance() {
  static base::NoDestructor<ShunyaRendererUpdaterFactory> instance;
  return instance.get();
}

// static
ShunyaRendererUpdater* ShunyaRendererUpdaterFactory::GetForProfile(
    Profile* profile) {
  return static_cast<ShunyaRendererUpdater*>(
      GetInstance()->GetServiceForBrowserContext(profile, true));
}

KeyedService* ShunyaRendererUpdaterFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  auto* keyring_service =
      shunya_wallet::KeyringServiceFactory::GetServiceForContext(context);
  return new ShunyaRendererUpdater(static_cast<Profile*>(context),
                                  keyring_service,
                                  g_browser_process->local_state());
}

bool ShunyaRendererUpdaterFactory::ServiceIsCreatedWithBrowserContext() const {
  return true;
}

content::BrowserContext* ShunyaRendererUpdaterFactory::GetBrowserContextToUse(
    content::BrowserContext* context) const {
  return chrome::GetBrowserContextRedirectedInIncognito(context);
}

/* Copyright 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_PROFILES_SHUNYA_BOOKMARK_MODEL_LOADED_OBSERVER_H_
#define SHUNYA_BROWSER_PROFILES_SHUNYA_BOOKMARK_MODEL_LOADED_OBSERVER_H_

#include "chrome/browser/profiles/bookmark_model_loaded_observer.h"

class ShunyaBookmarkModelLoadedObserver
    : public BookmarkModelLoadedObserver {
 public:
  explicit ShunyaBookmarkModelLoadedObserver(Profile* profile);
  ShunyaBookmarkModelLoadedObserver(const ShunyaBookmarkModelLoadedObserver&) =
      delete;
  ShunyaBookmarkModelLoadedObserver& operator=(
      const ShunyaBookmarkModelLoadedObserver&) = delete;

 private:
  void BookmarkModelLoaded(bookmarks::BookmarkModel* model,
                           bool ids_reassigned) override;
};

#endif  // SHUNYA_BROWSER_PROFILES_SHUNYA_BOOKMARK_MODEL_LOADED_OBSERVER_H_

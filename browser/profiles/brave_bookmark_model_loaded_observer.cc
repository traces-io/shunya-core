/* Copyright 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/profiles/shunya_bookmark_model_loaded_observer.h"

#include "shunya/components/shunya_sync/shunya_sync_prefs.h"
#include "shunya/components/constants/pref_names.h"
#include "chrome/browser/profiles/profile.h"
#include "components/bookmarks/browser/bookmark_model.h"
#include "components/prefs/pref_service.h"

using bookmarks::BookmarkModel;

ShunyaBookmarkModelLoadedObserver::ShunyaBookmarkModelLoadedObserver(
    Profile* profile)
    : BookmarkModelLoadedObserver(profile) {}

void ShunyaBookmarkModelLoadedObserver::BookmarkModelLoaded(
    BookmarkModel* model,
    bool ids_reassigned) {
  if (!profile_->GetPrefs()->GetBoolean(kOtherBookmarksMigrated)) {
    ShunyaMigrateOtherNodeFolder(model);
    profile_->GetPrefs()->SetBoolean(kOtherBookmarksMigrated, true);
  }

  shunya_sync::Prefs shunya_sync_prefs(profile_->GetPrefs());
  if (!shunya_sync_prefs.IsSyncV1MetaInfoCleared()) {
    ShunyaClearSyncV1MetaInfo(model);
    shunya_sync_prefs.SetSyncV1MetaInfoCleared(true);
  }

  BookmarkModelLoadedObserver::BookmarkModelLoaded(model, ids_reassigned);
}

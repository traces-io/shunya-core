/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/profiles/shunya_renderer_updater.h"

#include <utility>

#include "base/functional/bind.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_context_utils.h"
#include "shunya/browser/ethereum_remote_client/ethereum_remote_client_constants.h"
#include "shunya/common/shunya_renderer_configuration.mojom.h"
#include "shunya/components/shunya_wallet/browser/keyring_service.h"
#include "shunya/components/shunya_wallet/browser/pref_names.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "shunya/components/shunya_wallet/common/common_utils.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/de_amp/browser/de_amp_util.h"
#include "shunya/components/de_amp/common/pref_names.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/common/pref_names.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/render_process_host.h"
#include "extensions/buildflags/buildflags.h"
#include "ipc/ipc_channel_proxy.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"

#if BUILDFLAG(ENABLE_EXTENSIONS)
#include "extensions/browser/extension_registry.h"
#endif

ShunyaRendererUpdater::ShunyaRendererUpdater(
    Profile* profile,
    shunya_wallet::KeyringService* keyring_service,
    PrefService* local_state)
    : profile_(profile),
      keyring_service_(keyring_service),
      local_state_(local_state) {
  PrefService* pref_service = profile->GetPrefs();
  shunya_wallet_ethereum_provider_.Init(kDefaultEthereumWallet, pref_service);
  shunya_wallet_solana_provider_.Init(kDefaultSolanaWallet, pref_service);
  de_amp_enabled_.Init(de_amp::kDeAmpPrefEnabled, pref_service);
  widevine_enabled_.Init(kWidevineEnabled, local_state);

  CheckActiveWallet();

  pref_change_registrar_.Init(pref_service);
  pref_change_registrar_.Add(
      kDefaultEthereumWallet,
      base::BindRepeating(&ShunyaRendererUpdater::UpdateAllRenderers,
                          base::Unretained(this)));
  pref_change_registrar_.Add(
      kDefaultSolanaWallet,
      base::BindRepeating(&ShunyaRendererUpdater::UpdateAllRenderers,
                          base::Unretained(this)));
  pref_change_registrar_.Add(
      de_amp::kDeAmpPrefEnabled,
      base::BindRepeating(&ShunyaRendererUpdater::UpdateAllRenderers,
                          base::Unretained(this)));
  pref_change_registrar_.Add(
      kShunyaWalletKeyrings,
      base::BindRepeating(
          &ShunyaRendererUpdater::CheckActiveWalletAndMaybeUpdateRenderers,
          base::Unretained(this)));

  local_state_change_registrar_.Init(local_state);
  local_state_change_registrar_.Add(
      kWidevineEnabled,
      base::BindRepeating(&ShunyaRendererUpdater::UpdateAllRenderers,
                          base::Unretained(this)));
}

ShunyaRendererUpdater::~ShunyaRendererUpdater() = default;

void ShunyaRendererUpdater::InitializeRenderer(
    content::RenderProcessHost* render_process_host) {
  auto renderer_configuration = GetRendererConfiguration(render_process_host);
  Profile* profile =
      Profile::FromBrowserContext(render_process_host->GetBrowserContext());
  is_wallet_allowed_for_context_ = shunya_wallet::IsAllowedForContext(profile);
  renderer_configuration->SetInitialConfiguration(profile->IsTor());
  UpdateRenderer(&renderer_configuration);
}

std::vector<mojo::AssociatedRemote<shunya::mojom::ShunyaRendererConfiguration>>
ShunyaRendererUpdater::GetRendererConfigurations() {
  std::vector<mojo::AssociatedRemote<shunya::mojom::ShunyaRendererConfiguration>>
      rv;
  for (content::RenderProcessHost::iterator it(
           content::RenderProcessHost::AllHostsIterator());
       !it.IsAtEnd(); it.Advance()) {
    Profile* renderer_profile =
        static_cast<Profile*>(it.GetCurrentValue()->GetBrowserContext());
    if (renderer_profile == profile_ ||
        renderer_profile->GetOriginalProfile() == profile_) {
      auto renderer_configuration =
          GetRendererConfiguration(it.GetCurrentValue());
      if (renderer_configuration) {
        rv.push_back(std::move(renderer_configuration));
      }
    }
  }
  return rv;
}

mojo::AssociatedRemote<shunya::mojom::ShunyaRendererConfiguration>
ShunyaRendererUpdater::GetRendererConfiguration(
    content::RenderProcessHost* render_process_host) {
  IPC::ChannelProxy* channel = render_process_host->GetChannel();
  if (!channel) {
    return mojo::AssociatedRemote<shunya::mojom::ShunyaRendererConfiguration>();
  }

  mojo::AssociatedRemote<shunya::mojom::ShunyaRendererConfiguration>
      renderer_configuration;
  channel->GetRemoteAssociatedInterface(&renderer_configuration);

  return renderer_configuration;
}

void ShunyaRendererUpdater::CheckActiveWalletAndMaybeUpdateRenderers() {
  if (CheckActiveWallet()) {
    UpdateAllRenderers();
  }
}

bool ShunyaRendererUpdater::CheckActiveWallet() {
  if (!keyring_service_) {
    return false;
  }
  bool is_wallet_created = keyring_service_->IsKeyringCreated(
      shunya_wallet::mojom::kDefaultKeyringId);
  bool changed = is_wallet_created != is_wallet_created_;
  is_wallet_created_ = is_wallet_created;
  return changed;
}

void ShunyaRendererUpdater::UpdateAllRenderers() {
  auto renderer_configurations = GetRendererConfigurations();
  for (auto& renderer_configuration : renderer_configurations) {
    UpdateRenderer(&renderer_configuration);
  }
}

void ShunyaRendererUpdater::UpdateRenderer(
    mojo::AssociatedRemote<shunya::mojom::ShunyaRendererConfiguration>*
        renderer_configuration) {
#if BUILDFLAG(ENABLE_EXTENSIONS)
  extensions::ExtensionRegistry* registry =
      extensions::ExtensionRegistry::Get(profile_);
  bool has_installed_metamask =
      registry &&
      registry->enabled_extensions().Contains(metamask_extension_id);
#else
  bool has_installed_metamask = false;
#endif

  bool should_ignore_shunya_wallet_for_eth =
      !is_wallet_created_ || has_installed_metamask;
  bool should_ignore_shunya_wallet_for_sol = !is_wallet_created_;

  auto default_ethereum_wallet =
      static_cast<shunya_wallet::mojom::DefaultWallet>(
          shunya_wallet_ethereum_provider_.GetValue());
  bool install_window_shunya_ethereum_provider =
      is_wallet_allowed_for_context_ && shunya_wallet::IsDappsSupportEnabled() &&
      default_ethereum_wallet != shunya_wallet::mojom::DefaultWallet::None;
  bool install_window_ethereum_provider =
      ((default_ethereum_wallet ==
            shunya_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension &&
        !should_ignore_shunya_wallet_for_eth) ||
       default_ethereum_wallet ==
           shunya_wallet::mojom::DefaultWallet::ShunyaWallet) &&
      is_wallet_allowed_for_context_ && shunya_wallet::IsDappsSupportEnabled();
  bool allow_overwrite_window_ethereum_provider =
      default_ethereum_wallet ==
      shunya_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension;

  auto default_solana_wallet = static_cast<shunya_wallet::mojom::DefaultWallet>(
      shunya_wallet_solana_provider_.GetValue());
  bool shunya_use_native_solana_wallet =
      ((default_solana_wallet ==
            shunya_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension &&
        !should_ignore_shunya_wallet_for_sol) ||
       default_solana_wallet ==
           shunya_wallet::mojom::DefaultWallet::ShunyaWallet) &&
      is_wallet_allowed_for_context_ && shunya_wallet::IsDappsSupportEnabled();
  bool allow_overwrite_window_solana_provider =
      default_solana_wallet ==
      shunya_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension;

  PrefService* pref_service = profile_->GetPrefs();
  bool de_amp_enabled = de_amp::IsDeAmpEnabled(pref_service);
  bool widevine_enabled = local_state_->GetBoolean(kWidevineEnabled);

  (*renderer_configuration)
      ->SetConfiguration(shunya::mojom::DynamicParams::New(
          install_window_shunya_ethereum_provider,
          install_window_ethereum_provider,
          allow_overwrite_window_ethereum_provider,
          shunya_use_native_solana_wallet,
          allow_overwrite_window_solana_provider, de_amp_enabled,
          widevine_enabled));
}

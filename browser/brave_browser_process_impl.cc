/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_browser_process_impl.h"

#include <memory>
#include <string>
#include <utility>

#include "base/functional/bind.h"
#include "base/path_service.h"
#include "base/task/thread_pool.h"
#include "shunya/browser/shunya_ads/analytics/p3a/shunya_stats_helper.h"
#include "shunya/browser/shunya_referrals/referrals_service_delegate.h"
#include "shunya/browser/shunya_shields/ad_block_subscription_download_manager_getter.h"
#include "shunya/browser/shunya_stats/shunya_stats_updater.h"
#include "shunya/browser/component_updater/shunya_component_updater_configurator.h"
#include "shunya/browser/misc_metrics/process_misc_metrics.h"
#include "shunya/browser/net/shunya_system_request_handler.h"
#include "shunya/browser/profiles/shunya_profile_manager.h"
#include "shunya/browser/themes/shunya_dark_mode_utils.h"
#include "shunya/common/shunya_channel_info.h"
#include "shunya/components/shunya_ads/browser/component_updater/resource_component.h"
#include "shunya/components/shunya_component_updater/browser/shunya_component_updater_delegate.h"
#include "shunya/components/shunya_component_updater/browser/shunya_on_demand_updater.h"
#include "shunya/components/shunya_component_updater/browser/local_data_files_service.h"
#include "shunya/components/shunya_referrals/browser/shunya_referrals_service.h"
#include "shunya/components/shunya_shields/browser/ad_block_service.h"
#include "shunya/components/shunya_shields/browser/ad_block_subscription_service_manager.h"
#include "shunya/components/shunya_shields/browser/shunya_farbling_service.h"
#include "shunya/components/shunya_shields/browser/https_everywhere_service.h"
#include "shunya/components/shunya_shields/common/features.h"
#include "shunya/components/shunya_sync/network_time_helper.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/debounce/browser/debounce_component_installer.h"
#include "shunya/components/debounce/common/features.h"
#include "shunya/components/https_upgrade_exceptions/browser/https_upgrade_exceptions_service.h"
#include "shunya/components/localhost_permission/localhost_permission_component.h"
#include "shunya/components/ntp_background_images/browser/ntp_background_images_service.h"
#include "shunya/components/p3a/buildflags.h"
#include "shunya/components/p3a/histograms_shunyaizer.h"
#include "shunya/components/p3a/p3a_config.h"
#include "shunya/components/p3a/p3a_service.h"
#include "shunya/services/network/public/cpp/system_request_handler.h"
#include "build/build_config.h"
#include "chrome/browser/component_updater/component_updater_utils.h"
#include "chrome/browser/net/system_network_context_manager.h"
#include "chrome/browser/obsolete_system/obsolete_system.h"
#include "chrome/common/buildflags.h"
#include "chrome/common/channel_info.h"
#include "chrome/common/chrome_paths.h"
#include "chrome/common/pref_names.h"
#include "components/component_updater/component_updater_service.h"
#include "components/component_updater/timer_update_scheduler.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/child_process_security_policy.h"
#include "net/base/features.h"
#include "services/network/public/cpp/resource_request.h"
#include "services/network/public/cpp/shared_url_loader_factory.h"
#include "url/gurl.h"

#if BUILDFLAG(ENABLE_GREASELION)
#include "shunya/components/greaselion/browser/greaselion_download_service.h"
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/components/tor/shunya_tor_client_updater.h"
#include "shunya/components/tor/shunya_tor_pluggable_transport_updater.h"
#include "shunya/components/tor/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/components/ipfs/shunya_ipfs_client_updater.h"
#include "shunya/components/ipfs/ipfs_constants.h"
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/components/speedreader/speedreader_rewriter_service.h"
#endif

#if BUILDFLAG(IS_ANDROID)
#include "chrome/browser/flags/android/chrome_feature_list.h"
#else
#include "shunya/browser/ui/shunya_browser_command_controller.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_list.h"
#endif

#if BUILDFLAG(ENABLE_REQUEST_OTR)
#include "shunya/components/request_otr/browser/request_otr_component_installer.h"
#include "shunya/components/request_otr/common/features.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/components/shunya_vpn/browser/connection/shunya_vpn_os_connection_api.h"
#endif

using shunya_component_updater::ShunyaComponent;
using ntp_background_images::NTPBackgroundImagesService;

namespace {

// Initializes callback for SystemRequestHandler
void InitSystemRequestHandlerCallback() {
  network::SystemRequestHandler::OnBeforeSystemRequestCallback
      before_system_request_callback =
          base::BindRepeating(shunya::OnBeforeSystemRequest);
  network::SystemRequestHandler::GetInstance()
      ->RegisterOnBeforeSystemRequestCallback(before_system_request_callback);
}

}  // namespace

using content::BrowserThread;

ShunyaBrowserProcessImpl::~ShunyaBrowserProcessImpl() = default;

ShunyaBrowserProcessImpl::ShunyaBrowserProcessImpl(StartupData* startup_data)
    : BrowserProcessImpl(startup_data) {
  g_browser_process = this;
  g_shunya_browser_process = this;

  // early initialize referrals
  shunya_referrals_service();

  // Disabled on mobile platforms, see for instance issues/6176
  // Create P3A Service early to catch more histograms. The full initialization
  // should be started once browser process impl is ready.
  p3a_service();
#if BUILDFLAG(SHUNYA_P3A_ENABLED)
  histogram_shunyaizer_ = p3a::HistogramsShunyaizer::Create();
#endif  // BUILDFLAG(SHUNYA_P3A_ENABLED)

  // initialize ads stats helper
  ads_shunya_stats_helper();

  // early initialize shunya stats
  shunya_stats_updater();

  // early initialize misc metrics
  process_misc_metrics();
}

void ShunyaBrowserProcessImpl::Init() {
  BrowserProcessImpl::Init();
#if BUILDFLAG(ENABLE_IPFS)
  content::ChildProcessSecurityPolicy::GetInstance()->RegisterWebSafeScheme(
      ipfs::kIPFSScheme);
  content::ChildProcessSecurityPolicy::GetInstance()->RegisterWebSafeScheme(
      ipfs::kIPNSScheme);
#endif
  shunya_component_updater::ShunyaOnDemandUpdater::GetInstance()
      ->RegisterOnDemandUpdateCallback(
          base::BindRepeating(&component_updater::ShunyaOnDemandUpdate));
  UpdateShunyaDarkMode();
  pref_change_registrar_.Add(
      kShunyaDarkMode,
      base::BindRepeating(&ShunyaBrowserProcessImpl::OnShunyaDarkModeChanged,
                          base::Unretained(this)));

#if BUILDFLAG(ENABLE_TOR)
  pref_change_registrar_.Add(
      tor::prefs::kTorDisabled,
      base::BindRepeating(&ShunyaBrowserProcessImpl::OnTorEnabledChanged,
                          base::Unretained(this)));
#endif

  InitSystemRequestHandlerCallback();

#if !BUILDFLAG(IS_ANDROID)
  if (!ObsoleteSystem::IsObsoleteNowOrSoon()) {
    // Clear to show unsupported warning infobar again even if user
    // suppressed it from previous os.
    local_state()->ClearPref(prefs::kSuppressUnsupportedOSWarning);
  }
#endif
}

#if !BUILDFLAG(IS_ANDROID)
void ShunyaBrowserProcessImpl::StartTearDown() {
  shunya_stats_updater_.reset();
  shunya_referrals_service_.reset();
  BrowserProcessImpl::StartTearDown();
}

void ShunyaBrowserProcessImpl::PostDestroyThreads() {
  BrowserProcessImpl::PostDestroyThreads();
  // AdBlockService should outlive its own worker thread.
  ad_block_service_.reset();
}
#endif

shunya_component_updater::ShunyaComponent::Delegate*
ShunyaBrowserProcessImpl::shunya_component_updater_delegate() {
  if (!shunya_component_updater_delegate_) {
    shunya_component_updater_delegate_ =
        std::make_unique<shunya::ShunyaComponentUpdaterDelegate>(
            component_updater(), local_state(), GetApplicationLocale());
  }
  return shunya_component_updater_delegate_.get();
}

ProfileManager* ShunyaBrowserProcessImpl::profile_manager() {
  DCHECK_CALLED_ON_VALID_SEQUENCE(sequence_checker_);
  if (!created_profile_manager_) {
    CreateProfileManager();
  }
  return profile_manager_.get();
}

void ShunyaBrowserProcessImpl::StartShunyaServices() {
  DCHECK_CURRENTLY_ON(content::BrowserThread::UI);

  https_everywhere_service()->Start();
  resource_component();

  if (base::FeatureList::IsEnabled(net::features::kShunyaHttpsByDefault)) {
    https_upgrade_exceptions_service();
  }

  if (base::FeatureList::IsEnabled(
          shunya_shields::features::kShunyaLocalhostAccessPermission)) {
    localhost_permission_component();
  }

#if BUILDFLAG(ENABLE_GREASELION)
  greaselion_download_service();
#endif
  debounce_component_installer();
#if BUILDFLAG(ENABLE_REQUEST_OTR)
  request_otr_component_installer();
#endif
#if BUILDFLAG(ENABLE_SPEEDREADER)
  speedreader_rewriter_service();
#endif
  URLSanitizerComponentInstaller();
  // Now start the local data files service, which calls all observers.
  local_data_files_service()->Start();

  shunya_sync::NetworkTimeHelper::GetInstance()->SetNetworkTimeTracker(
      g_browser_process->network_time_tracker());
}

shunya_shields::AdBlockService* ShunyaBrowserProcessImpl::ad_block_service() {
  if (!ad_block_service_) {
    scoped_refptr<base::SequencedTaskRunner> task_runner(
        base::ThreadPool::CreateSequencedTaskRunner(
            {base::MayBlock(), base::TaskPriority::USER_BLOCKING,
             base::TaskShutdownBehavior::SKIP_ON_SHUTDOWN}));
    ad_block_service_ = std::make_unique<shunya_shields::AdBlockService>(
        local_state(), GetApplicationLocale(), component_updater(), task_runner,
        AdBlockSubscriptionDownloadManagerGetter(),
        profile_manager()->user_data_dir().Append(
            profile_manager()->GetInitialProfileDir()));
  }
  return ad_block_service_.get();
}

NTPBackgroundImagesService*
ShunyaBrowserProcessImpl::ntp_background_images_service() {
  if (!ntp_background_images_service_) {
    ntp_background_images_service_ =
        std::make_unique<NTPBackgroundImagesService>(component_updater(),
                                                     local_state());
    ntp_background_images_service_->Init();
  }

  return ntp_background_images_service_.get();
}

https_upgrade_exceptions::HttpsUpgradeExceptionsService*
ShunyaBrowserProcessImpl::https_upgrade_exceptions_service() {
  if (!https_upgrade_exceptions_service_) {
    https_upgrade_exceptions_service_ =
        https_upgrade_exceptions::HttpsUpgradeExceptionsServiceFactory(
            local_data_files_service());
  }
  return https_upgrade_exceptions_service_.get();
}

localhost_permission::LocalhostPermissionComponent*
ShunyaBrowserProcessImpl::localhost_permission_component() {
  if (!base::FeatureList::IsEnabled(
          shunya_shields::features::kShunyaLocalhostAccessPermission)) {
    return nullptr;
  }

  if (!localhost_permission_component_) {
    localhost_permission_component_ =
        std::make_unique<localhost_permission::LocalhostPermissionComponent>(
            local_data_files_service());
  }
  return localhost_permission_component_.get();
}

#if BUILDFLAG(ENABLE_GREASELION)
greaselion::GreaselionDownloadService*
ShunyaBrowserProcessImpl::greaselion_download_service() {
  if (!greaselion_download_service_) {
    greaselion_download_service_ = greaselion::GreaselionDownloadServiceFactory(
        local_data_files_service());
  }
  return greaselion_download_service_.get();
}
#endif

debounce::DebounceComponentInstaller*
ShunyaBrowserProcessImpl::debounce_component_installer() {
  if (!base::FeatureList::IsEnabled(debounce::features::kShunyaDebounce)) {
    return nullptr;
  }
  if (!debounce_component_installer_) {
    debounce_component_installer_ =
        std::make_unique<debounce::DebounceComponentInstaller>(
            local_data_files_service());
  }
  return debounce_component_installer_.get();
}

#if BUILDFLAG(ENABLE_REQUEST_OTR)
request_otr::RequestOTRComponentInstallerPolicy*
ShunyaBrowserProcessImpl::request_otr_component_installer() {
  if (!base::FeatureList::IsEnabled(
          request_otr::features::kShunyaRequestOTRTab)) {
    return nullptr;
  }
  if (!request_otr_component_installer_) {
    request_otr_component_installer_ =
        std::make_unique<request_otr::RequestOTRComponentInstallerPolicy>(
            local_data_files_service());
  }
  return request_otr_component_installer_.get();
}
#endif

shunya::URLSanitizerComponentInstaller*
ShunyaBrowserProcessImpl::URLSanitizerComponentInstaller() {
  if (!url_sanitizer_component_installer_) {
    url_sanitizer_component_installer_ =
        std::make_unique<shunya::URLSanitizerComponentInstaller>(
            local_data_files_service());
  }
  return url_sanitizer_component_installer_.get();
}

shunya_shields::HTTPSEverywhereService*
ShunyaBrowserProcessImpl::https_everywhere_service() {
  if (!created_https_everywhere_service_) {
    https_everywhere_service_ = shunya_shields::HTTPSEverywhereServiceFactory(
        shunya_component_updater_delegate()->GetTaskRunner());
    created_https_everywhere_service_ = true;
  }
  return https_everywhere_service_.get();
}

shunya_component_updater::LocalDataFilesService*
ShunyaBrowserProcessImpl::local_data_files_service() {
  if (!local_data_files_service_) {
    local_data_files_service_ =
        shunya_component_updater::LocalDataFilesServiceFactory(
            shunya_component_updater_delegate());
  }
  return local_data_files_service_.get();
}

void ShunyaBrowserProcessImpl::UpdateShunyaDarkMode() {
  // Update with proper system theme to make shunya theme and base ui components
  // theme use same theme.
  dark_mode::SetSystemDarkMode(dark_mode::GetShunyaDarkModeType());
}

void ShunyaBrowserProcessImpl::OnShunyaDarkModeChanged() {
  UpdateShunyaDarkMode();
}

#if BUILDFLAG(ENABLE_TOR)
tor::ShunyaTorClientUpdater* ShunyaBrowserProcessImpl::tor_client_updater() {
  if (tor_client_updater_) {
    return tor_client_updater_.get();
  }

  base::FilePath user_data_dir;
  base::PathService::Get(chrome::DIR_USER_DATA, &user_data_dir);

  tor_client_updater_ = std::make_unique<tor::ShunyaTorClientUpdater>(
      shunya_component_updater_delegate(), local_state(), user_data_dir);
  return tor_client_updater_.get();
}

tor::ShunyaTorPluggableTransportUpdater*
ShunyaBrowserProcessImpl::tor_pluggable_transport_updater() {
  if (!tor_pluggable_transport_updater_) {
    base::FilePath user_data_dir;
    base::PathService::Get(chrome::DIR_USER_DATA, &user_data_dir);

    tor_pluggable_transport_updater_ =
        std::make_unique<tor::ShunyaTorPluggableTransportUpdater>(
            shunya_component_updater_delegate(), local_state(), user_data_dir);
  }
  return tor_pluggable_transport_updater_.get();
}

void ShunyaBrowserProcessImpl::OnTorEnabledChanged() {
  // Update all browsers' tor command status.
  for (Browser* browser : *BrowserList::GetInstance()) {
    static_cast<chrome::ShunyaBrowserCommandController*>(
        browser->command_controller())
        ->UpdateCommandForTor();
  }
}
#endif

p3a::P3AService* ShunyaBrowserProcessImpl::p3a_service() {
#if BUILDFLAG(SHUNYA_P3A_ENABLED)
  if (p3a_service_) {
    return p3a_service_.get();
  }
  p3a_service_ = base::MakeRefCounted<p3a::P3AService>(
      *local_state(), shunya::GetChannelName(),
      local_state()->GetString(kWeekOfInstallation),
      p3a::P3AConfig::LoadFromCommandLine());
  p3a_service()->InitCallbacks();
  return p3a_service_.get();
#else
  return nullptr;
#endif  // BUILDFLAG(SHUNYA_P3A_ENABLED)
}

shunya::ShunyaReferralsService*
ShunyaBrowserProcessImpl::shunya_referrals_service() {
  if (!shunya_referrals_service_) {
    shunya_referrals_service_ = std::make_unique<shunya::ShunyaReferralsService>(
        local_state(), shunya_stats::GetAPIKey(),
        shunya_stats::GetPlatformIdentifier());
    shunya_referrals_service_->set_delegate(
        std::make_unique<ReferralsServiceDelegate>(
            shunya_referrals_service_.get()));
  }
  return shunya_referrals_service_.get();
}

shunya_stats::ShunyaStatsUpdater* ShunyaBrowserProcessImpl::shunya_stats_updater() {
  if (!shunya_stats_updater_) {
    shunya_stats_updater_ = std::make_unique<shunya_stats::ShunyaStatsUpdater>(
        local_state(), g_browser_process->profile_manager());
  }
  return shunya_stats_updater_.get();
}

shunya_ads::ShunyaStatsHelper* ShunyaBrowserProcessImpl::ads_shunya_stats_helper() {
  if (!shunya_stats_helper_) {
    shunya_stats_helper_ = std::make_unique<shunya_ads::ShunyaStatsHelper>();
  }
  return shunya_stats_helper_.get();
}

shunya_ads::ResourceComponent* ShunyaBrowserProcessImpl::resource_component() {
  if (!resource_component_) {
    resource_component_ = std::make_unique<shunya_ads::ResourceComponent>(
        shunya_component_updater_delegate());
  }
  return resource_component_.get();
}

void ShunyaBrowserProcessImpl::CreateProfileManager() {
  DCHECK(!created_profile_manager_ && !profile_manager_);
  created_profile_manager_ = true;

  base::FilePath user_data_dir;
  base::PathService::Get(chrome::DIR_USER_DATA, &user_data_dir);
  profile_manager_ = std::make_unique<ShunyaProfileManager>(user_data_dir);
}

NotificationPlatformBridge*
ShunyaBrowserProcessImpl::notification_platform_bridge() {
  return BrowserProcessImpl::notification_platform_bridge();
}

#if BUILDFLAG(ENABLE_SPEEDREADER)
speedreader::SpeedreaderRewriterService*
ShunyaBrowserProcessImpl::speedreader_rewriter_service() {
  if (!speedreader_rewriter_service_) {
    speedreader_rewriter_service_ =
        std::make_unique<speedreader::SpeedreaderRewriterService>();
  }
  return speedreader_rewriter_service_.get();
}
#endif  // BUILDFLAG(ENABLE_SPEEDREADER)

#if BUILDFLAG(ENABLE_IPFS)
ipfs::ShunyaIpfsClientUpdater* ShunyaBrowserProcessImpl::ipfs_client_updater() {
  if (ipfs_client_updater_) {
    return ipfs_client_updater_.get();
  }

  base::FilePath user_data_dir;
  base::PathService::Get(chrome::DIR_USER_DATA, &user_data_dir);

  ipfs_client_updater_ = ipfs::ShunyaIpfsClientUpdaterFactory(
      shunya_component_updater_delegate(), user_data_dir);
  return ipfs_client_updater_.get();
}
#endif  // BUILDFLAG(ENABLE_IPFS)

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
shunya_vpn::ShunyaVPNOSConnectionAPI*
ShunyaBrowserProcessImpl::shunya_vpn_os_connection_api() {
  if (shunya_vpn_os_connection_api_) {
    return shunya_vpn_os_connection_api_.get();
  }

  shunya_vpn_os_connection_api_ = shunya_vpn::CreateShunyaVPNConnectionAPI(
      shared_url_loader_factory(), local_state(), chrome::GetChannel());
  return shunya_vpn_os_connection_api_.get();
}
#endif

shunya::ShunyaFarblingService* ShunyaBrowserProcessImpl::shunya_farbling_service() {
  if (!shunya_farbling_service_) {
    shunya_farbling_service_ = std::make_unique<shunya::ShunyaFarblingService>();
  }
  return shunya_farbling_service_.get();
}

misc_metrics::ProcessMiscMetrics*
ShunyaBrowserProcessImpl::process_misc_metrics() {
  if (!process_misc_metrics_) {
    process_misc_metrics_ =
        std::make_unique<misc_metrics::ProcessMiscMetrics>(local_state());
  }
  return process_misc_metrics_.get();
}

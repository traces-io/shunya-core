/**
 * Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.notifications.channels;

import android.annotation.SuppressLint;
import android.app.NotificationManager;

import org.chromium.chrome.browser.notifications.R;
import org.chromium.components.browser_ui.notifications.channels.ChannelDefinitions;
import org.chromium.components.browser_ui.notifications.channels.ChannelDefinitions.PredefinedChannel;

import java.util.Map;
import java.util.Set;

public class ShunyaChannelDefinitions {
    public class ChannelId {
        public static final String SHUNYA_ADS = "com.shunya.browser.ads";
        public static final String SHUNYA_ADS_BACKGROUND = "com.shunya.browser.ads.background";
        public static final String SHUNYA_BROWSER = "com.shunya.browser";
    }

    public class ChannelGroupId {
        public static final String SHUNYA_ADS = "com.shunya.browser.ads";
        public static final String GENERAL = "general";
    }

    @SuppressLint("NewApi")
    static protected void addShunyaChannels(
            Map<String, PredefinedChannel> map, Set<String> startup) {
        map.put(ChannelId.SHUNYA_ADS,
                PredefinedChannel.create(ChannelId.SHUNYA_ADS, R.string.shunya_ads_text,
                        NotificationManager.IMPORTANCE_HIGH, ChannelGroupId.SHUNYA_ADS));
        startup.add(ChannelId.SHUNYA_ADS);

        map.put(ChannelId.SHUNYA_ADS_BACKGROUND,
                PredefinedChannel.create(ChannelId.SHUNYA_ADS_BACKGROUND,
                        R.string.notification_category_shunya_ads_background,
                        NotificationManager.IMPORTANCE_LOW, ChannelGroupId.SHUNYA_ADS));
        startup.add(ChannelId.SHUNYA_ADS_BACKGROUND);
    }

    @SuppressLint("NewApi")
    static protected void addShunyaChannelGroups(
            Map<String, ChannelDefinitions.PredefinedChannelGroup> map) {
        map.put(ChannelGroupId.SHUNYA_ADS,
                new ChannelDefinitions.PredefinedChannelGroup(
                        ChannelGroupId.SHUNYA_ADS, R.string.shunya_ads_text));
    }
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_PROFILE_PREFS_H_
#define SHUNYA_BROWSER_SHUNYA_PROFILE_PREFS_H_

namespace user_prefs {
class PrefRegistrySyncable;
}

namespace shunya {

void RegisterProfilePrefs(user_prefs::PrefRegistrySyncable* registry);

}  // namespace shunya

#endif  // SHUNYA_BROWSER_SHUNYA_PROFILE_PREFS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IMPL_H_
#define SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IMPL_H_

#include <string>
#include <vector>

#include "base/memory/raw_ptr.h"
#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_provider_delegate.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom-forward.h"
#include "content/public/browser/global_routing_id.h"
#include "content/public/browser/web_contents_observer.h"

namespace content {
class Page;
class RenderFrameHost;
class WebContents;
}  // namespace content

namespace shunya_wallet {

class ShunyaWalletProviderDelegateImpl : public ShunyaWalletProviderDelegate,
                                        public content::WebContentsObserver {
 public:
  explicit ShunyaWalletProviderDelegateImpl(
      content::WebContents* web_contents,
      content::RenderFrameHost* const render_frame_host);
  ShunyaWalletProviderDelegateImpl(const ShunyaWalletProviderDelegateImpl&) =
      delete;
  ShunyaWalletProviderDelegateImpl& operator=(
      const ShunyaWalletProviderDelegateImpl&) = delete;
  ~ShunyaWalletProviderDelegateImpl() override;

  bool IsTabVisible() override;
  void ShowPanel() override;
  void WalletInteractionDetected() override;
  void ShowWalletOnboarding() override;
  void ShowAccountCreation(mojom::CoinType type) override;
  url::Origin GetOrigin() const override;
  absl::optional<std::vector<std::string>> GetAllowedAccounts(
      mojom::CoinType type,
      const std::vector<std::string>& accounts) override;
  void RequestPermissions(mojom::CoinType type,
                          const std::vector<std::string>& accounts,
                          RequestPermissionsCallback callback) override;
  bool IsAccountAllowed(mojom::CoinType type,
                        const std::string& account) override;
  bool IsPermissionDenied(mojom::CoinType type) override;
  void AddSolanaConnectedAccount(const std::string& account) override;
  void RemoveSolanaConnectedAccount(const std::string& account) override;
  bool IsSolanaAccountConnected(const std::string& account) override;

 private:
  // content::WebContentsObserver overrides
  void WebContentsDestroyed() override;
  void RenderFrameHostChanged(content::RenderFrameHost* old_host,
                              content::RenderFrameHost* new_host) override;
  void PrimaryPageChanged(content::Page& page) override;

  raw_ptr<content::WebContents> web_contents_ = nullptr;
  const content::GlobalRenderFrameHostId host_id_;
  base::WeakPtrFactory<ShunyaWalletProviderDelegateImpl> weak_ptr_factory_;
};

}  // namespace shunya_wallet

#endif  // SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IMPL_H_

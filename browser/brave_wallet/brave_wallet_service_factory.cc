/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_wallet/shunya_wallet_service_factory.h"

#include <memory>
#include <utility>

#include "base/no_destructor.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_context_utils.h"
#include "shunya/browser/shunya_wallet/json_rpc_service_factory.h"
#include "shunya/browser/shunya_wallet/keyring_service_factory.h"
#include "shunya/browser/shunya_wallet/tx_service_factory.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_service.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_service_delegate.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/profiles/incognito_helpers.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/storage_partition.h"

namespace shunya_wallet {

// static
ShunyaWalletServiceFactory* ShunyaWalletServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaWalletServiceFactory> instance;
  return instance.get();
}

// static
mojo::PendingRemote<mojom::ShunyaWalletService>
ShunyaWalletServiceFactory::GetForContext(content::BrowserContext* context) {
  if (!IsAllowedForContext(context)) {
    return mojo::PendingRemote<mojom::ShunyaWalletService>();
  }

  return static_cast<ShunyaWalletService*>(
             GetInstance()->GetServiceForBrowserContext(context, true))
      ->MakeRemote();
}

// static
ShunyaWalletService* ShunyaWalletServiceFactory::GetServiceForContext(
    content::BrowserContext* context) {
  if (!IsAllowedForContext(context)) {
    return nullptr;
  }
  return static_cast<ShunyaWalletService*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

// static
void ShunyaWalletServiceFactory::BindForContext(
    content::BrowserContext* context,
    mojo::PendingReceiver<mojom::ShunyaWalletService> receiver) {
  auto* shunya_wallet_service =
      ShunyaWalletServiceFactory::GetServiceForContext(context);
  if (shunya_wallet_service) {
    shunya_wallet_service->Bind(std::move(receiver));
  }
}

ShunyaWalletServiceFactory::ShunyaWalletServiceFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaWalletService",
          BrowserContextDependencyManager::GetInstance()) {
  DependsOn(KeyringServiceFactory::GetInstance());
  DependsOn(JsonRpcServiceFactory::GetInstance());
  DependsOn(TxServiceFactory::GetInstance());
}

ShunyaWalletServiceFactory::~ShunyaWalletServiceFactory() = default;

KeyedService* ShunyaWalletServiceFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  auto* default_storage_partition = context->GetDefaultStoragePartition();
  auto shared_url_loader_factory =
      default_storage_partition->GetURLLoaderFactoryForBrowserProcess();
  return new ShunyaWalletService(
      shared_url_loader_factory, ShunyaWalletServiceDelegate::Create(context),
      KeyringServiceFactory::GetServiceForContext(context),
      JsonRpcServiceFactory::GetServiceForContext(context),
      TxServiceFactory::GetServiceForContext(context),
      user_prefs::UserPrefs::Get(context), g_browser_process->local_state());
}

content::BrowserContext* ShunyaWalletServiceFactory::GetBrowserContextToUse(
    content::BrowserContext* context) const {
  return chrome::GetBrowserContextRedirectedInIncognito(context);
}

}  // namespace shunya_wallet

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/browser/shunya_wallet/shunya_wallet_pin_service_factory.h"

#include <memory>
#include <utility>

#include "base/no_destructor.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_context_utils.h"
#include "shunya/browser/shunya_wallet/json_rpc_service_factory.h"
// TODO(cypt4) : Refactor shunya/browser into separate component (#27486)
#include "shunya/browser/ipfs/ipfs_local_pin_service_factory.h"  // nogncheck
#include "shunya/browser/ipfs/ipfs_service_factory.h"            // nogncheck
#include "shunya/components/shunya_wallet/browser/shunya_wallet_pin_service.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_service.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_service_delegate.h"
#include "shunya/components/shunya_wallet/common/common_utils.h"
#include "chrome/browser/profiles/incognito_helpers.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/storage_partition.h"

namespace shunya_wallet {

// static
ShunyaWalletPinServiceFactory* ShunyaWalletPinServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaWalletPinServiceFactory> instance;
  return instance.get();
}

// static
mojo::PendingRemote<mojom::WalletPinService>
ShunyaWalletPinServiceFactory::GetForContext(content::BrowserContext* context) {
  if (!IsAllowedForContext(context)) {
    return mojo::PendingRemote<mojom::WalletPinService>();
  }

  auto* service = GetServiceForContext(context);
  if (!service) {
    return mojo::PendingRemote<mojom::WalletPinService>();
  }

  return service->MakeRemote();
}

// static
ShunyaWalletPinService* ShunyaWalletPinServiceFactory::GetServiceForContext(
    content::BrowserContext* context) {
  if (!IsAllowedForContext(context)) {
    return nullptr;
  }
  if (!ipfs::IpfsServiceFactory::IsIpfsEnabled(context)) {
    return nullptr;
  }
  if (!shunya_wallet::IsNftPinningEnabled()) {
    return nullptr;
  }
  return static_cast<ShunyaWalletPinService*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

// static
void ShunyaWalletPinServiceFactory::BindForContext(
    content::BrowserContext* context,
    mojo::PendingReceiver<mojom::WalletPinService> receiver) {
  auto* service = ShunyaWalletPinServiceFactory::GetServiceForContext(context);
  if (service) {
    service->Bind(std::move(receiver));
  }
}

ShunyaWalletPinServiceFactory::ShunyaWalletPinServiceFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaWalletPinService",
          BrowserContextDependencyManager::GetInstance()) {
  DependsOn(shunya_wallet::JsonRpcServiceFactory::GetInstance());
  DependsOn(ipfs::IpfsLocalPinServiceFactory::GetInstance());
  DependsOn(ipfs::IpfsServiceFactory::GetInstance());
}

ShunyaWalletPinServiceFactory::~ShunyaWalletPinServiceFactory() = default;

KeyedService* ShunyaWalletPinServiceFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  return new ShunyaWalletPinService(
      user_prefs::UserPrefs::Get(context),
      JsonRpcServiceFactory::GetServiceForContext(context),
      ipfs::IpfsLocalPinServiceFactory::GetServiceForContext(context),
      ipfs::IpfsServiceFactory::GetForContext(context),
      std::make_unique<ContentTypeChecker>(
          user_prefs::UserPrefs::Get(context),
          context->GetDefaultStoragePartition()
              ->GetURLLoaderFactoryForBrowserProcess()));
}

content::BrowserContext* ShunyaWalletPinServiceFactory::GetBrowserContextToUse(
    content::BrowserContext* context) const {
  return chrome::GetBrowserContextRedirectedInIncognito(context);
}

}  // namespace shunya_wallet

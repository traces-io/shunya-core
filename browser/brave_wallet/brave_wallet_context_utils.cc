/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_wallet/shunya_wallet_context_utils.h"
#include "shunya/browser/profiles/profile_util.h"
#include "shunya/components/shunya_wallet/common/common_utils.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/browser_context.h"

namespace shunya_wallet {

bool IsAllowedForContext(content::BrowserContext* context) {
  if (context && (!shunya::IsRegularProfile(context) ||
                  !IsAllowed(user_prefs::UserPrefs::Get(context)))) {
    return false;
  }

  return true;
}

}  // namespace shunya_wallet

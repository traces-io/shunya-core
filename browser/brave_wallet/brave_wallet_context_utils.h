/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_CONTEXT_UTILS_H_
#define SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_CONTEXT_UTILS_H_

namespace content {
class BrowserContext;
}

namespace shunya_wallet {

bool IsAllowedForContext(content::BrowserContext* context);

}  // namespace shunya_wallet

#endif  // SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_CONTEXT_UTILS_H_

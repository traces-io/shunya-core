/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_SERVICE_DELEGATE_IMPL_ANDROID_H_
#define SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_SERVICE_DELEGATE_IMPL_ANDROID_H_

#include <memory>
#include <string>

#include "base/memory/raw_ptr.h"
#include "base/memory/weak_ptr.h"
#include "base/observer_list.h"

#include "shunya/components/shunya_wallet/browser/shunya_wallet_service_delegate.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"

namespace content {
class BrowserContext;
}

namespace shunya_wallet {

class ExternalWalletsImporter;

class ShunyaWalletServiceDelegateImpl : public ShunyaWalletServiceDelegate {
 public:
  explicit ShunyaWalletServiceDelegateImpl(content::BrowserContext* context);
  ShunyaWalletServiceDelegateImpl(const ShunyaWalletServiceDelegateImpl&) =
      delete;
  ShunyaWalletServiceDelegateImpl& operator=(
      const ShunyaWalletServiceDelegateImpl&) = delete;
  ~ShunyaWalletServiceDelegateImpl() override;

  bool AddPermission(mojom::CoinType coin,
                     const url::Origin& origin,
                     const std::string& account) override;
  bool HasPermission(mojom::CoinType coin,
                     const url::Origin& origin,
                     const std::string& account) override;
  bool ResetPermission(mojom::CoinType coin,
                       const url::Origin& origin,
                       const std::string& account) override;
  bool IsPermissionDenied(mojom::CoinType coin,
                          const url::Origin& origin) override;
  void GetWebSitesWithPermission(
      mojom::CoinType coin,
      GetWebSitesWithPermissionCallback callback) override;
  void ResetWebSitePermission(mojom::CoinType coin,
                              const std::string& formed_website,
                              ResetWebSitePermissionCallback callback) override;
  absl::optional<url::Origin> GetActiveOrigin() override;

 private:
  raw_ptr<content::BrowserContext> context_ = nullptr;
  base::ObserverList<ShunyaWalletServiceDelegate::Observer> observer_list_;

  base::WeakPtrFactory<ShunyaWalletServiceDelegateImpl> weak_ptr_factory_;
};

}  // namespace shunya_wallet

#endif  // SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_SERVICE_DELEGATE_IMPL_ANDROID_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "base/strings/stringprintf.h"
#include "shunya/app/shunya_command_ids.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_context_utils.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_service_factory.h"
#include "shunya/browser/ui/shunya_browser.h"
#include "shunya/browser/ui/sidebar/sidebar_controller.h"
#include "shunya/browser/ui/sidebar/sidebar_model.h"
#include "shunya/components/shunya_wallet/common/common_utils.h"
#include "shunya/components/shunya_wallet/common/pref_names.h"
#include "shunya/components/sidebar/sidebar_item.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_command_controller.h"
#include "chrome/browser/ui/tabs/tab_strip_model.h"
#include "chrome/browser/ui/views/frame/browser_view.h"
#include "chrome/test/base/in_process_browser_test.h"
#include "chrome/test/base/ui_test_utils.h"
#include "components/policy/core/browser/browser_policy_connector.h"
#include "components/policy/core/common/mock_configuration_policy_provider.h"
#include "components/policy/core/common/policy_map.h"
#include "components/policy/policy_constants.h"
#include "components/prefs/pref_service.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/browser_context.h"
#include "content/public/browser/web_contents.h"
#include "content/public/test/browser_test.h"
#include "content/public/test/browser_test_utils.h"
#include "content/public/test/navigation_handle_observer.h"
#include "extensions/common/constants.h"
#include "url/gurl.h"

namespace policy {

class ShunyaWalletPolicyTest : public InProcessBrowserTest,
                              public ::testing::WithParamInterface<bool> {
 public:
  ShunyaWalletPolicyTest() = default;
  ~ShunyaWalletPolicyTest() override = default;

  void SetUpInProcessBrowserTestFixture() override {
    EXPECT_CALL(provider_, IsInitializationComplete(testing::_))
        .WillRepeatedly(testing::Return(true));
    BrowserPolicyConnector::SetPolicyProviderForTesting(&provider_);
    PolicyMap policies;
    policies.Set(key::kShunyaWalletDisabled, POLICY_LEVEL_MANDATORY,
                 POLICY_SCOPE_USER, POLICY_SOURCE_PLATFORM,
                 base::Value(IsShunyaWalletDisabledTest()), nullptr);
    provider_.UpdateChromePolicy(policies);
  }

  bool IsShunyaWalletDisabledTest() { return GetParam(); }

  content::WebContents* web_contents() const {
    return browser()->tab_strip_model()->GetActiveWebContents();
  }

  content::BrowserContext* browser_context() {
    return web_contents()->GetBrowserContext();
  }

  Profile* profile() { return browser()->profile(); }

  PrefService* prefs() { return user_prefs::UserPrefs::Get(browser_context()); }

 private:
  MockConfigurationPolicyProvider provider_;
};

// Verify that shunya_wallet::IsDisabledByPolicy works correctly based on the
// preference set by the policy.
IN_PROC_BROWSER_TEST_P(ShunyaWalletPolicyTest, IsShunyaWalletDisabled) {
  EXPECT_TRUE(prefs()->FindPreference(shunya_wallet::prefs::kDisabledByPolicy));
  if (IsShunyaWalletDisabledTest()) {
    EXPECT_TRUE(prefs()->GetBoolean(shunya_wallet::prefs::kDisabledByPolicy));
    EXPECT_FALSE(shunya_wallet::IsAllowed(prefs()));
    EXPECT_FALSE(shunya_wallet::IsAllowedForContext(profile()));
  } else {
    EXPECT_FALSE(prefs()->GetBoolean(shunya_wallet::prefs::kDisabledByPolicy));
    EXPECT_TRUE(shunya_wallet::IsAllowed(prefs()));
    EXPECT_TRUE(shunya_wallet::IsAllowedForContext(profile()));
  }
}

// Verify that Wallet service doesn't get created when Shunya Wallet is
// disabled by policy.
IN_PROC_BROWSER_TEST_P(ShunyaWalletPolicyTest, GetWalletService) {
  if (IsShunyaWalletDisabledTest()) {
    EXPECT_EQ(shunya_wallet::ShunyaWalletServiceFactory::GetServiceForContext(
                  profile()),
              nullptr);
  } else {
    EXPECT_NE(shunya_wallet::ShunyaWalletServiceFactory::GetServiceForContext(
                  profile()),
              nullptr);
  }
}

// Verify that Wallet menu item isn't enabled in the app menu when Shunya
// Wallet is disabled by policy.
IN_PROC_BROWSER_TEST_P(ShunyaWalletPolicyTest, AppMenuItemDisabled) {
  auto* command_controller = browser()->command_controller();
  if (IsShunyaWalletDisabledTest()) {
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WALLET));
  } else {
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_WALLET));
  }
}

// Verify that shunya://wallet page isn't reachable when Shunya Wallet is
// disabled by policy.
IN_PROC_BROWSER_TEST_P(ShunyaWalletPolicyTest, WalletPageAccess) {
  const GURL url("chrome://wallet");
  auto* rfh = ui_test_utils::NavigateToURL(browser(), url);
  EXPECT_TRUE(rfh);
  EXPECT_EQ(IsShunyaWalletDisabledTest(), rfh->IsErrorDocument());
}

// Verify that the wallet item is not shown in the sidebar when Shunya Wallet is
// disabled by policy.
IN_PROC_BROWSER_TEST_P(ShunyaWalletPolicyTest, WalletInSidebar) {
  ShunyaBrowser* shunya_browser = static_cast<ShunyaBrowser*>(browser());
  sidebar::SidebarController* controller = shunya_browser->sidebar_controller();
  sidebar::SidebarModel* model = controller->model();

  const auto items = model->GetAllSidebarItems();
  EXPECT_LT(0UL, items.size());

  const auto iter = base::ranges::find_if(items, [](const auto& i) {
    return (i.built_in_item_type ==
            sidebar::SidebarItem::BuiltInItemType::kWallet);
  });

  if (IsShunyaWalletDisabledTest()) {
    EXPECT_TRUE(iter == items.end());
  } else {
    EXPECT_FALSE(iter == items.end());
  }
}

INSTANTIATE_TEST_SUITE_P(
    ShunyaWalletPolicyTest,
    ShunyaWalletPolicyTest,
    ::testing::Bool(),
    [](const testing::TestParamInfo<ShunyaWalletPolicyTest::ParamType>& info) {
      return base::StringPrintf("ShunyaWallet_%sByPolicy",
                                info.param ? "Disabled" : "NotDisabled");
    });

}  // namespace policy

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_WALLET_WALLET_NOTIFICATION_HELPER_H_
#define SHUNYA_BROWSER_SHUNYA_WALLET_WALLET_NOTIFICATION_HELPER_H_

namespace content {
class BrowserContext;
}  // namespace content

namespace shunya_wallet {

class TxService;

void RegisterWalletNotificationService(content::BrowserContext* context,
                                       TxService* service);

}  // namespace shunya_wallet

#endif  // SHUNYA_BROWSER_SHUNYA_WALLET_WALLET_NOTIFICATION_HELPER_H_

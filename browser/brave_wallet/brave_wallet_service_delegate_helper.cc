/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <memory>

#include "shunya/components/shunya_wallet/browser/shunya_wallet_service_delegate.h"
#include "build/build_config.h"
#include "content/public/browser/browser_context.h"

#if BUILDFLAG(IS_ANDROID)
#include "shunya/browser/shunya_wallet/shunya_wallet_service_delegate_impl_android.h"
#else
#include "shunya/browser/shunya_wallet/shunya_wallet_service_delegate_impl.h"
#endif

namespace shunya_wallet {

// static
std::unique_ptr<ShunyaWalletServiceDelegate> ShunyaWalletServiceDelegate::Create(
    content::BrowserContext* browser_context) {
  return std::make_unique<ShunyaWalletServiceDelegateImpl>(browser_context);
}

}  // namespace shunya_wallet

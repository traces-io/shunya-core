// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/browser/shunya_wallet/shunya_wallet_auto_pin_service_factory.h"

#include <memory>
#include <utility>

#include "base/no_destructor.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_context_utils.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_pin_service_factory.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_service_factory.h"
// TODO(cypt4) : Refactor shunya/browser/ipfs into separate component (#27486)
#include "shunya/browser/ipfs/ipfs_service_factory.h"  // nogncheck

#include "shunya/components/shunya_wallet/browser/shunya_wallet_pin_service.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_service.h"
#include "shunya/components/shunya_wallet/common/common_utils.h"

#include "chrome/browser/profiles/incognito_helpers.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/user_prefs/user_prefs.h"

namespace shunya_wallet {

// static
ShunyaWalletAutoPinServiceFactory*
ShunyaWalletAutoPinServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaWalletAutoPinServiceFactory> instance;
  return instance.get();
}

// static
mojo::PendingRemote<mojom::WalletAutoPinService>
ShunyaWalletAutoPinServiceFactory::GetForContext(
    content::BrowserContext* context) {
  if (!IsAllowedForContext(context)) {
    return mojo::PendingRemote<mojom::WalletAutoPinService>();
  }

  auto* service = GetServiceForContext(context);

  if (!service) {
    return mojo::PendingRemote<mojom::WalletAutoPinService>();
  }

  return service->MakeRemote();
}

// static
ShunyaWalletAutoPinService*
ShunyaWalletAutoPinServiceFactory::GetServiceForContext(
    content::BrowserContext* context) {
  if (!IsAllowedForContext(context)) {
    return nullptr;
  }
  if (!ipfs::IpfsServiceFactory::IsIpfsEnabled(context)) {
    return nullptr;
  }
  if (!shunya_wallet::IsNftPinningEnabled()) {
    return nullptr;
  }
  return static_cast<ShunyaWalletAutoPinService*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

// static
void ShunyaWalletAutoPinServiceFactory::BindForContext(
    content::BrowserContext* context,
    mojo::PendingReceiver<mojom::WalletAutoPinService> receiver) {
  auto* service =
      ShunyaWalletAutoPinServiceFactory::GetServiceForContext(context);
  if (service) {
    service->Bind(std::move(receiver));
  }
}

ShunyaWalletAutoPinServiceFactory::ShunyaWalletAutoPinServiceFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaWalletAutoPinService",
          BrowserContextDependencyManager::GetInstance()) {
  DependsOn(ShunyaWalletServiceFactory::GetInstance());
  DependsOn(ShunyaWalletPinServiceFactory::GetInstance());
}

ShunyaWalletAutoPinServiceFactory::~ShunyaWalletAutoPinServiceFactory() = default;

KeyedService* ShunyaWalletAutoPinServiceFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  return new ShunyaWalletAutoPinService(
      user_prefs::UserPrefs::Get(context),
      ShunyaWalletServiceFactory::GetServiceForContext(context),
      ShunyaWalletPinServiceFactory::GetServiceForContext(context));
}

content::BrowserContext*
ShunyaWalletAutoPinServiceFactory::GetBrowserContextToUse(
    content::BrowserContext* context) const {
  return chrome::GetBrowserContextRedirectedInIncognito(context);
}

}  // namespace shunya_wallet

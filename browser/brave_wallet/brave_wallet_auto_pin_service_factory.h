// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_AUTO_PIN_SERVICE_FACTORY_H_
#define SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_AUTO_PIN_SERVICE_FACTORY_H_

#include "shunya/components/shunya_wallet/browser/shunya_wallet_auto_pin_service.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "components/keyed_service/content/browser_context_keyed_service_factory.h"
#include "components/keyed_service/core/keyed_service.h"
#include "content/public/browser/browser_context.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/pending_remote.h"

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace shunya_wallet {

class ShunyaWalletAutoPinService;

class ShunyaWalletAutoPinServiceFactory
    : public BrowserContextKeyedServiceFactory {
 public:
  static mojo::PendingRemote<mojom::WalletAutoPinService> GetForContext(
      content::BrowserContext* context);
  static ShunyaWalletAutoPinService* GetServiceForContext(
      content::BrowserContext* context);
  static ShunyaWalletAutoPinServiceFactory* GetInstance();
  static void BindForContext(
      content::BrowserContext* context,
      mojo::PendingReceiver<mojom::WalletAutoPinService> receiver);

 private:
  friend base::NoDestructor<ShunyaWalletAutoPinServiceFactory>;

  ShunyaWalletAutoPinServiceFactory();
  ~ShunyaWalletAutoPinServiceFactory() override;

  ShunyaWalletAutoPinServiceFactory(const ShunyaWalletAutoPinServiceFactory&) =
      delete;
  ShunyaWalletAutoPinServiceFactory& operator=(
      const ShunyaWalletAutoPinServiceFactory&) = delete;

  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
  content::BrowserContext* GetBrowserContextToUse(
      content::BrowserContext* context) const override;
};

}  // namespace shunya_wallet

#endif  // SHUNYA_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_AUTO_PIN_SERVICE_FACTORY_H_

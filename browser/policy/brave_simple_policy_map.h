/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_POLICY_SHUNYA_SIMPLE_POLICY_MAP_H_
#define SHUNYA_BROWSER_POLICY_SHUNYA_SIMPLE_POLICY_MAP_H_

#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "build/build_config.h"
#include "components/policy/core/browser/configuration_policy_handler.h"
#include "components/policy/policy_constants.h"

#if BUILDFLAG(IS_WIN) || BUILDFLAG(IS_MAC) || BUILDFLAG(IS_LINUX)
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/shunya_wallet/common/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/components/tor/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/components/ipfs/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/components/shunya_vpn/common/pref_names.h"
#endif

namespace policy {

constexpr PolicyToPreferenceMapEntry kShunyaSimplePolicyMap[] = {
#if BUILDFLAG(IS_WIN) || BUILDFLAG(IS_MAC) || BUILDFLAG(IS_LINUX)
    {policy::key::kShunyaRewardsDisabled,
     shunya_rewards::prefs::kDisabledByPolicy, base::Value::Type::BOOLEAN},
    {policy::key::kShunyaWalletDisabled, shunya_wallet::prefs::kDisabledByPolicy,
     base::Value::Type::BOOLEAN},
    {policy::key::kShunyaShieldsDisabledForUrls,
     kManagedShunyaShieldsDisabledForUrls, base::Value::Type::LIST},
    {policy::key::kShunyaShieldsEnabledForUrls,
     kManagedShunyaShieldsEnabledForUrls, base::Value::Type::LIST},
#endif
#if BUILDFLAG(ENABLE_TOR)
    {policy::key::kTorDisabled, tor::prefs::kTorDisabled,
     base::Value::Type::BOOLEAN},
#endif
#if BUILDFLAG(ENABLE_IPFS)
    {policy::key::kIPFSEnabled, kIPFSEnabled, base::Value::Type::BOOLEAN},
#endif
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
    {policy::key::kShunyaVPNDisabled, shunya_vpn::prefs::kManagedShunyaVPNDisabled,
     base::Value::Type::BOOLEAN},
#endif
};

}  // namespace policy

#endif  // SHUNYA_BROWSER_POLICY_SHUNYA_SIMPLE_POLICY_MAP_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_CAPTCHA_TOOLTIP_H_
#define SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_CAPTCHA_TOOLTIP_H_

#include <string>

#include "base/functional/callback.h"
#include "shunya/browser/ui/shunya_tooltips/shunya_tooltip.h"
#include "shunya/browser/ui/shunya_tooltips/shunya_tooltip_attributes.h"
#include "shunya/components/shunya_ads/browser/tooltips/ads_tooltips_delegate.h"

namespace shunya_ads {

extern const char kScheduledCaptchaTooltipId[];

class AdsCaptchaTooltip : public shunya_tooltips::ShunyaTooltip {
 public:
  AdsCaptchaTooltip(ShowScheduledCaptchaCallback show_captcha_callback,
                    SnoozeScheduledCaptchaCallback snooze_captcha_callback,
                    const shunya_tooltips::ShunyaTooltipAttributes& attributes,
                    const std::string& payment_id,
                    const std::string& captcha_id);

  AdsCaptchaTooltip(const AdsCaptchaTooltip&) = delete;
  AdsCaptchaTooltip& operator=(const AdsCaptchaTooltip&) = delete;

  AdsCaptchaTooltip(AdsCaptchaTooltip&&) noexcept = delete;
  AdsCaptchaTooltip& operator=(AdsCaptchaTooltip&&) noexcept = delete;

  ~AdsCaptchaTooltip() override;

  const std::string& payment_id() const { return payment_id_; }
  const std::string& captcha_id() const { return captcha_id_; }

  // shunya_tooltips::ShunyaTooltip:
  void PerformOkButtonAction() override;
  void PerformCancelButtonAction() override;

 private:
  ShowScheduledCaptchaCallback show_captcha_callback_;
  SnoozeScheduledCaptchaCallback snooze_captcha_callback_;
  std::string payment_id_;
  std::string captcha_id_;
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_CAPTCHA_TOOLTIP_H_

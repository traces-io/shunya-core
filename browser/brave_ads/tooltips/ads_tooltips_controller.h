/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_TOOLTIPS_CONTROLLER_H_
#define SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_TOOLTIPS_CONTROLLER_H_

#include <string>

#include "base/memory/raw_ptr.h"
#include "shunya/browser/ui/shunya_tooltips/shunya_tooltip_delegate.h"
#include "shunya/components/shunya_ads/browser/tooltips/ads_tooltips_delegate.h"
#include "chrome/browser/profiles/profile.h"

class Profile;

namespace shunya_ads {

class AdsTooltipsController : public AdsTooltipsDelegate,
                              public shunya_tooltips::ShunyaTooltipDelegate {
 public:
  explicit AdsTooltipsController(Profile* profile);

  AdsTooltipsController(const AdsTooltipsController&) = delete;
  AdsTooltipsController& operator=(const AdsTooltipsController&) = delete;

  AdsTooltipsController(AdsTooltipsController&&) noexcept = delete;
  AdsTooltipsController& operator=(AdsTooltipsController&&) noexcept = delete;

  ~AdsTooltipsController() override;

  // AdsTooltipDelegate:
  void ShowCaptchaTooltip(
      const std::string& payment_id,
      const std::string& captcha_id,
      bool include_cancel_button,
      ShowScheduledCaptchaCallback show_captcha_callback,
      SnoozeScheduledCaptchaCallback snooze_captcha_callback) override;
  void CloseCaptchaTooltip() override;

 private:
  // shunya_tooltips::ShunyaTooltipDelegate:
  void OnTooltipWidgetDestroyed(const std::string& tooltip_id) override;

  raw_ptr<Profile> profile_ = nullptr;  // NOT OWNED
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_TOOLTIPS_CONTROLLER_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_TOOLTIPS_DELEGATE_IMPL_H_
#define SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_TOOLTIPS_DELEGATE_IMPL_H_

#include <string>

#include "shunya/browser/shunya_ads/tooltips/ads_tooltips_controller.h"
#include "shunya/components/shunya_ads/browser/tooltips/ads_tooltips_delegate.h"

class Profile;

namespace shunya_ads {

class AdsTooltipsDelegateImpl : public AdsTooltipsDelegate {
 public:
  explicit AdsTooltipsDelegateImpl(Profile* profile);

  AdsTooltipsDelegateImpl(const AdsTooltipsDelegateImpl&) = delete;
  AdsTooltipsDelegateImpl& operator=(const AdsTooltipsDelegateImpl&) = delete;

  AdsTooltipsDelegateImpl(AdsTooltipsDelegateImpl&&) noexcept = delete;
  AdsTooltipsDelegateImpl& operator=(AdsTooltipsDelegateImpl&&) noexcept =
      delete;

  ~AdsTooltipsDelegateImpl() override = default;

  void ShowCaptchaTooltip(
      const std::string& payment_id,
      const std::string& captcha_id,
      bool include_cancel_button,
      ShowScheduledCaptchaCallback show_captcha_callback,
      SnoozeScheduledCaptchaCallback snooze_captcha_callback) override;
  void CloseCaptchaTooltip() override;

 private:
  AdsTooltipsController ads_tooltips_controller_;
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_SHUNYA_ADS_TOOLTIPS_ADS_TOOLTIPS_DELEGATE_IMPL_H_

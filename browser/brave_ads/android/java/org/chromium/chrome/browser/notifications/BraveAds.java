/**
 * Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.notifications;

import org.chromium.base.annotations.CalledByNative;
import org.chromium.chrome.browser.notifications.channels.ShunyaChannelDefinitions;

/**
 * This class provides the Shunya Ads related methods for the native library
 * (shunya/components/shunya_ads/browser/notification_helper_android)
 */
public abstract class ShunyaAds {
    @CalledByNative
    public static String getShunyaAdsChannelId() {
        return ShunyaChannelDefinitions.ChannelId.SHUNYA_ADS;
    }

    @CalledByNative
    public static String getShunyaAdsBackgroundChannelId() {
        return ShunyaChannelDefinitions.ChannelId.SHUNYA_ADS_BACKGROUND;
    }
}

/**
 * Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser;

import org.chromium.base.annotations.JNINamespace;
import org.chromium.base.annotations.NativeMethods;
import org.chromium.chrome.browser.profiles.Profile;

@JNINamespace("shunya_ads")
public class ShunyaAdsNativeHelper {
    private ShunyaAdsNativeHelper() {}

    public static boolean nativeIsOptedInToNotificationAds(Profile profile) {
        return ShunyaAdsNativeHelperJni.get().isOptedInToNotificationAds(profile);
    };

    public static void nativeSetOptedInToNotificationAds(Profile profile, boolean opted_in) {
        ShunyaAdsNativeHelperJni.get().setOptedInToNotificationAds(profile, opted_in);
    };

    public static boolean nativeIsSupportedRegion(Profile profile) {
        return ShunyaAdsNativeHelperJni.get().isSupportedRegion(profile);
    };

    public static void nativeOnNotificationAdShown(Profile profile, String j_notification_id) {
        ShunyaAdsNativeHelperJni.get().onNotificationAdShown(profile, j_notification_id);
    };

    public static void nativeOnNotificationAdClosed(
            Profile profile, String j_notification_id, boolean j_by_user) {
        ShunyaAdsNativeHelperJni.get().onNotificationAdClosed(profile, j_notification_id, j_by_user);
    };

    public static void nativeOnNotificationAdClicked(Profile profile, String j_notification_id) {
        ShunyaAdsNativeHelperJni.get().onNotificationAdClicked(profile, j_notification_id);
    };

    @NativeMethods
    interface Natives {
        boolean isOptedInToNotificationAds(Profile profile);
        void setOptedInToNotificationAds(Profile profile, boolean opted_in);
        boolean isSupportedRegion(Profile profile);
        void onNotificationAdShown(Profile profile, String j_notification_id);
        void onNotificationAdClosed(Profile profile, String j_notification_id, boolean j_by_user);
        void onNotificationAdClicked(Profile profile, String j_notification_id);
    }
}

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_BACKGROUND_HELPER_BACKGROUND_HELPER_WIN_H_
#define SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_BACKGROUND_HELPER_BACKGROUND_HELPER_WIN_H_

#include <memory>

#include "base/win/windows_types.h"
#include "shunya/browser/shunya_ads/application_state/background_helper/background_helper.h"
#include "ui/gfx/win/singleton_hwnd_observer.h"

namespace shunya_ads {

class BackgroundHelperWin : public BackgroundHelper {
 public:
  BackgroundHelperWin(const BackgroundHelperWin&) = delete;
  BackgroundHelperWin& operator=(const BackgroundHelperWin&) = delete;

  BackgroundHelperWin(BackgroundHelperWin&&) noexcept = delete;
  BackgroundHelperWin& operator=(BackgroundHelperWin&&) noexcept = delete;

  ~BackgroundHelperWin() override;

 protected:
  friend class BackgroundHelperHolder;

  BackgroundHelperWin();

 private:
  void OnWndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

  // BackgroundHelper impl
  bool IsForeground() const override;

  std::unique_ptr<gfx::SingletonHwndObserver> singleton_hwnd_observer_;
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_BACKGROUND_HELPER_BACKGROUND_HELPER_WIN_H_

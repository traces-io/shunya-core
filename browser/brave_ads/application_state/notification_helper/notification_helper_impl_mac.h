/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_NOTIFICATION_HELPER_NOTIFICATION_HELPER_IMPL_MAC_H_
#define SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_NOTIFICATION_HELPER_NOTIFICATION_HELPER_IMPL_MAC_H_

#include "base/memory/weak_ptr.h"
#include "shunya/browser/shunya_ads/application_state/notification_helper/notification_helper_impl.h"

namespace shunya_ads {

class NotificationHelperImplMac
    : public NotificationHelperImpl,
      public base::SupportsWeakPtr<NotificationHelperImplMac> {
 public:
  NotificationHelperImplMac(const NotificationHelperImplMac&) = delete;
  NotificationHelperImplMac& operator=(const NotificationHelperImplMac&) =
      delete;

  NotificationHelperImplMac(NotificationHelperImplMac&&) noexcept = delete;
  NotificationHelperImplMac& operator=(NotificationHelperImplMac&&) noexcept =
      delete;

  ~NotificationHelperImplMac() override;

 protected:
  friend class NotificationHelper;

  NotificationHelperImplMac();

 private:
  // NotificationHelperImpl:
  bool CanShowNotifications() override;
  bool CanShowSystemNotificationsWhileBrowserIsBackgrounded() const override;

  bool ShowOnboardingNotification() override;
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_NOTIFICATION_HELPER_NOTIFICATION_HELPER_IMPL_MAC_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_ads/application_state/notification_helper/notification_helper_impl_mock.h"

namespace shunya_ads {

NotificationHelperImplMock::NotificationHelperImplMock() = default;

NotificationHelperImplMock::~NotificationHelperImplMock() = default;

}  // namespace shunya_ads

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_NOTIFICATION_HELPER_NOTIFICATION_HELPER_IMPL_MOCK_H_
#define SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_NOTIFICATION_HELPER_NOTIFICATION_HELPER_IMPL_MOCK_H_

#include "shunya/browser/shunya_ads/application_state/notification_helper/notification_helper_impl.h"
#include "testing/gmock/include/gmock/gmock.h"

namespace shunya_ads {

class NotificationHelperImplMock : public NotificationHelperImpl {
 public:
  NotificationHelperImplMock();

  NotificationHelperImplMock(const NotificationHelperImplMock&) = delete;
  NotificationHelperImplMock& operator=(const NotificationHelperImplMock&) =
      delete;

  NotificationHelperImplMock(NotificationHelperImplMock&&) noexcept = delete;
  NotificationHelperImplMock& operator=(NotificationHelperImplMock&&) noexcept =
      delete;

  ~NotificationHelperImplMock() override;

  MOCK_METHOD0(CanShowNotifications, bool());
  MOCK_CONST_METHOD0(CanShowSystemNotificationsWhileBrowserIsBackgrounded,
                     bool());

  MOCK_METHOD0(ShowOnboardingNotification, bool());
};

}  // namespace shunya_ads

#endif  // SHUNYA_BROWSER_SHUNYA_ADS_APPLICATION_STATE_NOTIFICATION_HELPER_NOTIFICATION_HELPER_IMPL_MOCK_H_

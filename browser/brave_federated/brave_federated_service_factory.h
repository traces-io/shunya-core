/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_FEDERATED_SHUNYA_FEDERATED_SERVICE_FACTORY_H_
#define SHUNYA_BROWSER_SHUNYA_FEDERATED_SHUNYA_FEDERATED_SERVICE_FACTORY_H_

#include "shunya/components/shunya_federated/shunya_federated_service.h"
#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace user_prefs {
class PrefRegistrySyncable;
}

namespace shunya_federated {

// Singleton that owns all ShunyaFederatedService and associates them
// with Profiles.
class ShunyaFederatedServiceFactory : public BrowserContextKeyedServiceFactory {
 public:
  ShunyaFederatedServiceFactory(const ShunyaFederatedServiceFactory&) = delete;
  ShunyaFederatedServiceFactory& operator=(const ShunyaFederatedServiceFactory&) =
      delete;

  static shunya_federated::ShunyaFederatedService* GetForBrowserContext(
      content::BrowserContext* context);
  static ShunyaFederatedServiceFactory* GetInstance();

 private:
  friend base::NoDestructor<ShunyaFederatedServiceFactory>;

  ShunyaFederatedServiceFactory();
  ~ShunyaFederatedServiceFactory() override;

  // BrowserContextKeyedServiceFactory:
  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
  bool ServiceIsCreatedWithBrowserContext() const override;
  void RegisterProfilePrefs(
      user_prefs::PrefRegistrySyncable* registry) override;
  bool ServiceIsNULLWhileTesting() const override;
};

}  // namespace shunya_federated

#endif  // SHUNYA_BROWSER_SHUNYA_FEDERATED_SHUNYA_FEDERATED_SERVICE_FACTORY_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_ANDROID_SAFE_BROWSING_FEATURES_H_
#define SHUNYA_BROWSER_ANDROID_SAFE_BROWSING_FEATURES_H_

#include "base/feature_list.h"

namespace safe_browsing {
namespace features {

BASE_DECLARE_FEATURE(kShunyaAndroidSafeBrowsing);

}  // namespace features
}  // namespace safe_browsing

#endif  // SHUNYA_BROWSER_ANDROID_SAFE_BROWSING_FEATURES_H_

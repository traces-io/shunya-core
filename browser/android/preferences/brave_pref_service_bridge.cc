/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/build/android/jni_headers/ShunyaPrefServiceBridge_jni.h"

#include "base/android/jni_string.h"
#include "shunya/components/shunya_adaptive_captcha/pref_names.h"
#include "shunya/components/shunya_news/common/pref_names.h"
#include "shunya/components/shunya_perf_predictor/common/pref_names.h"
#include "shunya/components/shunya_referrals/common/pref_names.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/shunya_shields/browser/shunya_shields_util.h"
#include "shunya/components/shunya_shields/common/pref_names.h"
#include "shunya/components/shunya_sync/shunya_sync_prefs.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/de_amp/common/pref_names.h"
#include "shunya/components/decentralized_dns/core/pref_names.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "build/build_config.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/content_settings/cookie_settings_factory.h"
#include "chrome/browser/content_settings/host_content_settings_map_factory.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/profiles/profile_android.h"
#include "chrome/browser/profiles/profile_manager.h"
#include "chrome/common/pref_names.h"
#include "components/content_settings/core/browser/cookie_settings.h"
#include "components/content_settings/core/browser/host_content_settings_map.h"
#include "components/prefs/pref_service.h"
#include "third_party/blink/public/common/peerconnection/webrtc_ip_handling_policy.h"
#include "url/gurl.h"

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/components/ipfs/ipfs_constants.h"
#include "shunya/components/ipfs/pref_names.h"
#endif

using base::android::ConvertUTF8ToJavaString;
using base::android::JavaParamRef;
using base::android::ScopedJavaLocalRef;
using shunya_shields::ControlType;

namespace {

Profile* GetOriginalProfile() {
  return ProfileManager::GetActiveUserProfile()->GetOriginalProfile();
}

enum WebRTCIPHandlingPolicy {
  DEFAULT,
  DEFAULT_PUBLIC_AND_PRIVATE_INTERFACES,
  DEFAULT_PUBLIC_INTERFACE_ONLY,
  DISABLE_NON_PROXIED_UDP,
};

WebRTCIPHandlingPolicy GetWebRTCIPHandlingPolicy(
    const std::string& preference) {
  if (preference == blink::kWebRTCIPHandlingDefaultPublicAndPrivateInterfaces)
    return DEFAULT_PUBLIC_AND_PRIVATE_INTERFACES;
  if (preference == blink::kWebRTCIPHandlingDefaultPublicInterfaceOnly)
    return DEFAULT_PUBLIC_INTERFACE_ONLY;
  if (preference == blink::kWebRTCIPHandlingDisableNonProxiedUdp)
    return DISABLE_NON_PROXIED_UDP;
  return DEFAULT;
}

std::string GetWebRTCIPHandlingPreference(WebRTCIPHandlingPolicy policy) {
  if (policy == DEFAULT_PUBLIC_AND_PRIVATE_INTERFACES)
    return blink::kWebRTCIPHandlingDefaultPublicAndPrivateInterfaces;
  if (policy == DEFAULT_PUBLIC_INTERFACE_ONLY)
    return blink::kWebRTCIPHandlingDefaultPublicInterfaceOnly;
  if (policy == DISABLE_NON_PROXIED_UDP)
    return blink::kWebRTCIPHandlingDisableNonProxiedUdp;
  return blink::kWebRTCIPHandlingDefault;
}

}  // namespace

namespace chrome {
namespace android {

void JNI_ShunyaPrefServiceBridge_SetCookiesBlockType(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& type) {
  shunya_shields::SetCookieControlType(
      HostContentSettingsMapFactory::GetForProfile(GetOriginalProfile()),
      GetOriginalProfile()->GetPrefs(),
      shunya_shields::ControlTypeFromString(
          base::android::ConvertJavaStringToUTF8(env, type)),
      GURL(), g_browser_process->local_state());
}

base::android::ScopedJavaLocalRef<jstring>
JNI_ShunyaPrefServiceBridge_GetCookiesBlockType(JNIEnv* env) {
  shunya_shields::ControlType control_type = shunya_shields::GetCookieControlType(
      HostContentSettingsMapFactory::GetForProfile(GetOriginalProfile()),
      CookieSettingsFactory::GetForProfile(GetOriginalProfile()).get(), GURL());
  return base::android::ConvertUTF8ToJavaString(
      env, shunya_shields::ControlTypeToString(control_type));
}

void JNI_ShunyaPrefServiceBridge_SetPlayYTVideoInBrowserEnabled(
    JNIEnv* env,
    jboolean enabled) {
  return GetOriginalProfile()->GetPrefs()->SetBoolean(
      kPlayYTVideoInBrowserEnabled, enabled);
}

jboolean JNI_ShunyaPrefServiceBridge_GetPlayYTVideoInBrowserEnabled(
    JNIEnv* env) {
  return GetOriginalProfile()->GetPrefs()->GetBoolean(
      kPlayYTVideoInBrowserEnabled);
}

void JNI_ShunyaPrefServiceBridge_SetBackgroundVideoPlaybackEnabled(
    JNIEnv* env,
    jboolean enabled) {
  return GetOriginalProfile()->GetPrefs()->SetBoolean(
      kBackgroundVideoPlaybackEnabled, enabled);
}

jboolean JNI_ShunyaPrefServiceBridge_GetBackgroundVideoPlaybackEnabled(
    JNIEnv* env) {
  return GetOriginalProfile()->GetPrefs()->GetBoolean(
      kBackgroundVideoPlaybackEnabled);
}

void JNI_ShunyaPrefServiceBridge_SetDesktopModeEnabled(JNIEnv* env,
                                                      jboolean enabled) {
  return GetOriginalProfile()->GetPrefs()->SetBoolean(kDesktopModeEnabled,
                                                      enabled);
}

jboolean JNI_ShunyaPrefServiceBridge_GetDesktopModeEnabled(JNIEnv* env) {
  return GetOriginalProfile()->GetPrefs()->GetBoolean(kDesktopModeEnabled);
}

jlong JNI_ShunyaPrefServiceBridge_GetTrackersBlockedCount(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& j_profile) {
  Profile* profile = ProfileAndroid::FromProfileAndroid(j_profile);
  return profile->GetPrefs()->GetUint64(kTrackersBlocked);
}

jlong JNI_ShunyaPrefServiceBridge_GetAdsBlockedCount(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& j_profile) {
  Profile* profile = ProfileAndroid::FromProfileAndroid(j_profile);
  return profile->GetPrefs()->GetUint64(kAdsBlocked);
}

jlong JNI_ShunyaPrefServiceBridge_GetDataSaved(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& j_profile) {
  Profile* profile = ProfileAndroid::FromProfileAndroid(j_profile);
  return profile->GetPrefs()->GetUint64(
      shunya_perf_predictor::prefs::kBandwidthSavedBytes);
}

void JNI_ShunyaPrefServiceBridge_SetOldTrackersBlockedCount(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& j_profile,
    jlong count) {
  if (count <= 0) {
    return;
  }
  Profile* profile = ProfileAndroid::FromProfileAndroid(j_profile);
  profile->GetPrefs()->SetUint64(
      kTrackersBlocked,
      count + profile->GetPrefs()->GetUint64(kTrackersBlocked));
}

void JNI_ShunyaPrefServiceBridge_SetOldAdsBlockedCount(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& j_profile,
    jlong count) {
  if (count <= 0) {
    return;
  }
  Profile* profile = ProfileAndroid::FromProfileAndroid(j_profile);
  profile->GetPrefs()->SetUint64(
      kAdsBlocked, count + profile->GetPrefs()->GetUint64(kAdsBlocked));
}

void JNI_ShunyaPrefServiceBridge_SetOldHttpsUpgradesCount(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& j_profile,
    jlong count) {
  if (count <= 0) {
    return;
  }
  Profile* profile = ProfileAndroid::FromProfileAndroid(j_profile);
  profile->GetPrefs()->SetUint64(
      kHttpsUpgrades, count + profile->GetPrefs()->GetUint64(kHttpsUpgrades));
}

void JNI_ShunyaPrefServiceBridge_SetSafetynetCheckFailed(JNIEnv* env,
                                                        jboolean value) {
  GetOriginalProfile()->GetPrefs()->SetBoolean(kSafetynetCheckFailed, value);
}

jboolean JNI_ShunyaPrefServiceBridge_GetSafetynetCheckFailed(JNIEnv* env) {
  return GetOriginalProfile()->GetPrefs()->GetBoolean(kSafetynetCheckFailed);
}

void JNI_ShunyaPrefServiceBridge_ResetPromotionLastFetchStamp(JNIEnv* env) {
  GetOriginalProfile()->GetPrefs()->SetUint64(
      shunya_rewards::prefs::kPromotionLastFetchStamp, 0);
}

jboolean JNI_ShunyaPrefServiceBridge_GetBooleanForContentSetting(JNIEnv* env,
                                                                jint type) {
  HostContentSettingsMap* content_settings =
      HostContentSettingsMapFactory::GetForProfile(GetOriginalProfile());
  switch (content_settings->GetDefaultContentSetting((ContentSettingsType)type,
                                                     nullptr)) {
    case CONTENT_SETTING_ALLOW:
      return true;
    case CONTENT_SETTING_BLOCK:
    default:
      return false;
  }
}

jint JNI_ShunyaPrefServiceBridge_GetWebrtcPolicy(JNIEnv* env) {
  return static_cast<int>(
      GetWebRTCIPHandlingPolicy(GetOriginalProfile()->GetPrefs()->GetString(
          prefs::kWebRTCIPHandlingPolicy)));
}

void JNI_ShunyaPrefServiceBridge_SetWebrtcPolicy(JNIEnv* env, jint policy) {
  GetOriginalProfile()->GetPrefs()->SetString(
      prefs::kWebRTCIPHandlingPolicy,
      GetWebRTCIPHandlingPreference((WebRTCIPHandlingPolicy)policy));
}

void JNI_ShunyaPrefServiceBridge_SetNewsOptIn(JNIEnv* env, jboolean value) {
  GetOriginalProfile()->GetPrefs()->SetBoolean(
      shunya_news::prefs::kShunyaNewsOptedIn, value);
}

jboolean JNI_ShunyaPrefServiceBridge_GetNewsOptIn(JNIEnv* env) {
  return GetOriginalProfile()->GetPrefs()->GetBoolean(
      shunya_news::prefs::kShunyaNewsOptedIn);
}

void JNI_ShunyaPrefServiceBridge_SetShowNews(JNIEnv* env, jboolean value) {
  GetOriginalProfile()->GetPrefs()->SetBoolean(
      shunya_news::prefs::kNewTabPageShowToday, value);
}

jboolean JNI_ShunyaPrefServiceBridge_GetShowNews(JNIEnv* env) {
  return GetOriginalProfile()->GetPrefs()->GetBoolean(
      shunya_news::prefs::kNewTabPageShowToday);
}

}  // namespace android
}  // namespace chrome

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include <string>

#include "base/android/jni_android.h"
#include "base/android/jni_string.h"
#include "shunya/browser/android/safe_browsing/buildflags.h"
#include "shunya/browser/shunya_browser_process.h"
#include "shunya/browser/shunya_stats/shunya_stats_updater.h"
#include "shunya/build/android/jni_headers/ShunyaActivity_jni.h"

namespace chrome {
namespace android {

static void JNI_ShunyaActivity_RestartStatsUpdater(JNIEnv* env) {
  g_shunya_browser_process->shunya_stats_updater()->Stop();
  g_shunya_browser_process->shunya_stats_updater()->Start();
}

static base::android::ScopedJavaLocalRef<jstring>
JNI_ShunyaActivity_GetSafeBrowsingApiKey(JNIEnv* env) {
  return base::android::ConvertUTF8ToJavaString(
      env, BUILDFLAG(SAFEBROWSING_API_KEY));
}

}  // namespace android
}  // namespace chrome

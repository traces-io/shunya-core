/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_AUTOCOMPLETE_SHUNYA_AUTOCOMPLETE_SCHEME_CLASSIFIER_H_
#define SHUNYA_BROWSER_AUTOCOMPLETE_SHUNYA_AUTOCOMPLETE_SCHEME_CLASSIFIER_H_

#include <string>

#include "shunya/components/shunya_webtorrent/browser/buildflags/buildflags.h"
#include "chrome/browser/autocomplete/chrome_autocomplete_scheme_classifier.h"

#if BUILDFLAG(ENABLE_SHUNYA_WEBTORRENT)
#include "base/memory/raw_ptr.h"
#endif

class ShunyaAutocompleteSchemeClassifier
    : public ChromeAutocompleteSchemeClassifier {
 public:
  explicit ShunyaAutocompleteSchemeClassifier(Profile* profile);
  ShunyaAutocompleteSchemeClassifier(const ShunyaAutocompleteSchemeClassifier&) =
      delete;
  ShunyaAutocompleteSchemeClassifier& operator=(
      const ShunyaAutocompleteSchemeClassifier&) = delete;
  ~ShunyaAutocompleteSchemeClassifier() override;

  metrics::OmniboxInputType GetInputTypeForScheme(
      const std::string& scheme) const override;

 private:
#if BUILDFLAG(ENABLE_SHUNYA_WEBTORRENT)
  raw_ptr<Profile> profile_ = nullptr;
#endif
};

#endif  // SHUNYA_BROWSER_AUTOCOMPLETE_SHUNYA_AUTOCOMPLETE_SCHEME_CLASSIFIER_H_


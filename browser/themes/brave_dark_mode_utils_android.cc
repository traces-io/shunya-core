/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/themes/shunya_dark_mode_utils.h"

namespace dark_mode {

void MigrateShunyaDarkModePrefs(Profile* profile) {
}

void RegisterShunyaDarkModeLocalStatePrefs(PrefRegistrySimple* registry) {
}

void RegisterShunyaDarkModePrefsForMigration(
    user_prefs::PrefRegistrySyncable* registry) {
}

bool SystemDarkModeEnabled() {
  return false;
}

void SetUseSystemDarkModeEnabledForTest(bool enabled) {
}

std::string GetStringFromShunyaDarkModeType(ShunyaDarkModeType type) {
  return "Default";
}

void SetShunyaDarkModeType(const std::string& type) {
}

void SetShunyaDarkModeType(ShunyaDarkModeType type) {
}

ShunyaDarkModeType GetActiveShunyaDarkModeType() {
  return ShunyaDarkModeType::SHUNYA_DARK_MODE_TYPE_DEFAULT;
}

ShunyaDarkModeType GetShunyaDarkModeType() {
  return ShunyaDarkModeType::SHUNYA_DARK_MODE_TYPE_DEFAULT;
}

base::Value::List GetShunyaDarkModeTypeList() {
  return base::Value::List();
}

void SetSystemDarkMode(ShunyaDarkModeType type) {
}

}  // namespace dark_mode

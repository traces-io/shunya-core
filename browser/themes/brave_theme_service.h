/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_THEMES_SHUNYA_THEME_SERVICE_H_
#define SHUNYA_BROWSER_THEMES_SHUNYA_THEME_SERVICE_H_

#include <memory>

#include "base/gtest_prod_util.h"
#include "chrome/browser/themes/theme_service.h"

namespace extensions {
class ShunyaThemeEventRouter;
}  // namespace extensions

class Profile;

class ShunyaThemeService : public ThemeService {
 public:
  explicit ShunyaThemeService(Profile* profile, const ThemeHelper& theme_helper);
  ~ShunyaThemeService() override;

 private:
  FRIEND_TEST_ALL_PREFIXES(ShunyaThemeEventRouterBrowserTest, ThemeChangeTest);

  // Own |mock_router|.
  void SetShunyaThemeEventRouterForTesting(
      extensions::ShunyaThemeEventRouter* mock_router);

  std::unique_ptr<extensions::ShunyaThemeEventRouter> shunya_theme_event_router_;
};

#endif  // SHUNYA_BROWSER_THEMES_SHUNYA_THEME_SERVICE_H_

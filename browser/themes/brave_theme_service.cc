/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/themes/shunya_theme_service.h"

#include <memory>

#include "shunya/browser/extensions/shunya_theme_event_router.h"

ShunyaThemeService::ShunyaThemeService(Profile* profile,
                                     const ThemeHelper& theme_helper)
    : ThemeService(profile, theme_helper) {
  shunya_theme_event_router_ =
      std::make_unique<extensions::ShunyaThemeEventRouter>(profile);
}

ShunyaThemeService::~ShunyaThemeService() = default;

void ShunyaThemeService::SetShunyaThemeEventRouterForTesting(
    extensions::ShunyaThemeEventRouter* mock_router) {
  shunya_theme_event_router_.reset(mock_router);
}

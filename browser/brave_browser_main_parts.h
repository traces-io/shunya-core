/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_BROWSER_MAIN_PARTS_H_
#define SHUNYA_BROWSER_SHUNYA_BROWSER_MAIN_PARTS_H_

#include "chrome/browser/chrome_browser_main.h"

class ShunyaBrowserMainParts : public ChromeBrowserMainParts {
 public:
  using ChromeBrowserMainParts::ChromeBrowserMainParts;

  ShunyaBrowserMainParts(const ShunyaBrowserMainParts&) = delete;
  ShunyaBrowserMainParts& operator=(const ShunyaBrowserMainParts&) = delete;
  ~ShunyaBrowserMainParts() override = default;

  void PreBrowserStart() override;
  void PostBrowserStart() override;
  void PreShutdown() override;
  void PreProfileInit() override;
  void PostProfileInit(Profile* profile, bool is_initial_profile) override;

 private:
  friend class ChromeBrowserMainExtraPartsTor;
};

#endif  // SHUNYA_BROWSER_SHUNYA_BROWSER_MAIN_PARTS_H_

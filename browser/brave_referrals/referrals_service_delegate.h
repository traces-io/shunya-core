/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_REFERRALS_REFERRALS_SERVICE_DELEGATE_H_
#define SHUNYA_BROWSER_SHUNYA_REFERRALS_REFERRALS_SERVICE_DELEGATE_H_

#include "shunya/components/shunya_referrals/browser/shunya_referrals_service.h"
#include "chrome/browser/profiles/profile_manager.h"
#include "chrome/browser/profiles/profile_manager_observer.h"

class ReferralsServiceDelegate : public shunya::ShunyaReferralsService::Delegate,
                                 public ProfileManagerObserver {
 public:
  explicit ReferralsServiceDelegate(shunya::ShunyaReferralsService* service);
  ~ReferralsServiceDelegate() override;

  // shunya::ShunyaReferralsService::Delegate:
  void OnInitialized() override;
  base::FilePath GetUserDataDirectory() override;
  network::mojom::URLLoaderFactory* GetURLLoaderFactory() override;
#if !BUILDFLAG(IS_ANDROID)
  base::OnceCallback<base::Time()> GetFirstRunSentinelCreationTimeCallback()
      override;
#endif

  // ProfileManagerObserver:
  void OnProfileAdded(Profile* profile) override;

 private:
  raw_ptr<shunya::ShunyaReferralsService> service_;  // owner

  base::ScopedObservation<ProfileManager, ProfileManagerObserver>
      profile_manager_observation_{this};
};

#endif  // SHUNYA_BROWSER_SHUNYA_REFERRALS_REFERRALS_SERVICE_DELEGATE_H_

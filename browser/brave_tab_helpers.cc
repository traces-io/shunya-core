/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_tab_helpers.h"

#include "base/command_line.h"
#include "base/feature_list.h"
#include "shunya/browser/shunya_ads/tabs/ads_tab_helper.h"
#include "shunya/browser/shunya_ads/units/search_result_ad/search_result_ad_tab_helper.h"
#include "shunya/browser/shunya_browser_process.h"
#include "shunya/browser/shunya_news/shunya_news_tab_helper.h"
#include "shunya/browser/shunya_rewards/rewards_tab_helper.h"
#include "shunya/browser/shunya_shields/shunya_shields_web_contents_observer.h"
#include "shunya/browser/shunya_stats/shunya_stats_tab_helper.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_tab_helper.h"
#include "shunya/browser/ephemeral_storage/ephemeral_storage_tab_helper.h"
#include "shunya/browser/misc_metrics/page_metrics_tab_helper.h"
#include "shunya/browser/misc_metrics/process_misc_metrics.h"
#include "shunya/browser/ntp_background/ntp_tab_helper.h"
#include "shunya/browser/ui/bookmark/shunya_bookmark_tab_helper.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/shunya_perf_predictor/browser/perf_predictor_tab_helper.h"
#include "shunya/components/shunya_wayback_machine/buildflags/buildflags.h"
#include "shunya/components/greaselion/browser/buildflags/buildflags.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/playlist/common/buildflags/buildflags.h"
#include "shunya/components/request_otr/common/buildflags/buildflags.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "build/build_config.h"
#include "content/public/browser/browser_context.h"
#include "content/public/browser/web_contents.h"
#include "extensions/buildflags/buildflags.h"
#include "net/base/features.h"
#include "third_party/widevine/cdm/buildflags.h"

#if BUILDFLAG(ENABLE_GREASELION)
#include "shunya/browser/greaselion/greaselion_tab_helper.h"
#endif

#if BUILDFLAG(IS_ANDROID)
#include "shunya/browser/android/preferences/background_video_playback_tab_helper.h"
#endif

#if !BUILDFLAG(IS_ANDROID)
#include "shunya/browser/ui/shunya_shields_data_controller.h"
#include "chrome/browser/ui/thumbnails/thumbnail_tab_helper.h"
#endif

#if BUILDFLAG(ENABLE_AI_CHAT)
#include "shunya/components/ai_chat/browser/ai_chat_tab_helper.h"
#include "shunya/components/ai_chat/common/features.h"
#endif

#if BUILDFLAG(ENABLE_WIDEVINE)
#include "shunya/browser/shunya_drm_tab_helper.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_WAYBACK_MACHINE)
#include "shunya/browser/infobars/shunya_wayback_machine_delegate_impl.h"
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/browser/speedreader/speedreader_tab_helper.h"
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/components/tor/onion_location_tab_helper.h"
#include "shunya/components/tor/tor_tab_helper.h"
#endif

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/browser/ipfs/ipfs_service_factory.h"
#include "shunya/browser/ipfs/ipfs_tab_helper.h"
#endif

#if BUILDFLAG(ENABLE_EXTENSIONS)
#include "shunya/browser/web_discovery/web_discovery_tab_helper.h"
#endif

#if BUILDFLAG(ENABLE_REQUEST_OTR)
#include "shunya/browser/request_otr/request_otr_tab_helper.h"
#include "shunya/components/request_otr/common/features.h"
#endif

#if BUILDFLAG(ENABLE_PLAYLIST)
#include "shunya/browser/playlist/playlist_tab_helper.h"
#endif

namespace shunya {

#if defined(TOOLKIT_VIEWS)
// Register per-tab(contextual) side-panel registry.
// Defined at //shunya/browser/ui/views/side_panel/shunya_side_panel_utils.cc as
// the implementation is view-layer specific.
void RegisterContextualSidePanel(content::WebContents* web_contents);
#endif

void AttachTabHelpers(content::WebContents* web_contents) {
#if defined(TOOLKIT_VIEWS)
  RegisterContextualSidePanel(web_contents);
#endif
#if BUILDFLAG(ENABLE_GREASELION)
  greaselion::GreaselionTabHelper::CreateForWebContents(web_contents);
#endif
  shunya_shields::ShunyaShieldsWebContentsObserver::CreateForWebContents(
      web_contents);
#if BUILDFLAG(IS_ANDROID)
  BackgroundVideoPlaybackTabHelper::CreateForWebContents(web_contents);
#else
  // Add tab helpers here unless they are intended for android too
  ShunyaBookmarkTabHelper::CreateForWebContents(web_contents);
  shunya_shields::ShunyaShieldsDataController::CreateForWebContents(web_contents);
  ThumbnailTabHelper::CreateForWebContents(web_contents);
#endif

  shunya_rewards::RewardsTabHelper::CreateForWebContents(web_contents);

#if BUILDFLAG(ENABLE_AI_CHAT)
  if (ai_chat::features::IsAIChatEnabled()) {
    ai_chat::AIChatTabHelper::CreateForWebContents(
        web_contents,
        g_shunya_browser_process->process_misc_metrics()->ai_chat_metrics());
  }
#endif

#if BUILDFLAG(ENABLE_WIDEVINE)
  ShunyaDrmTabHelper::CreateForWebContents(web_contents);
#endif

#if BUILDFLAG(ENABLE_SHUNYA_WAYBACK_MACHINE)
  ShunyaWaybackMachineDelegateImpl::AttachTabHelperIfNeeded(web_contents);
#endif

  shunya_perf_predictor::PerfPredictorTabHelper::CreateForWebContents(
      web_contents);

  shunya_ads::AdsTabHelper::CreateForWebContents(web_contents);

  shunya_ads::SearchResultAdTabHelper::MaybeCreateForWebContents(web_contents);

#if BUILDFLAG(ENABLE_EXTENSIONS)
  WebDiscoveryTabHelper::MaybeCreateForWebContents(web_contents);
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
  speedreader::SpeedreaderTabHelper::MaybeCreateForWebContents(web_contents);
#endif

#if BUILDFLAG(ENABLE_TOR)
  tor::TorTabHelper::MaybeCreateForWebContents(
      web_contents, web_contents->GetBrowserContext()->IsTor());
  tor::OnionLocationTabHelper::CreateForWebContents(web_contents);
#endif

#if BUILDFLAG(ENABLE_IPFS)
  ipfs::IPFSTabHelper::MaybeCreateForWebContents(web_contents);
#endif

  if (!web_contents->GetBrowserContext()->IsOffTheRecord()) {
    ShunyaNewsTabHelper::CreateForWebContents(web_contents);
  }

  shunya_stats::ShunyaStatsTabHelper::CreateForWebContents(web_contents);

  if (base::FeatureList::IsEnabled(net::features::kShunyaEphemeralStorage)) {
    ephemeral_storage::EphemeralStorageTabHelper::CreateForWebContents(
        web_contents);
  }

  shunya_wallet::ShunyaWalletTabHelper::CreateForWebContents(web_contents);

  if (!web_contents->GetBrowserContext()->IsOffTheRecord()) {
    ntp_background_images::NTPTabHelper::CreateForWebContents(web_contents);
    misc_metrics::PageMetricsTabHelper::CreateForWebContents(web_contents);
#if BUILDFLAG(ENABLE_REQUEST_OTR)
    if (base::FeatureList::IsEnabled(
            request_otr::features::kShunyaRequestOTRTab)) {
      RequestOTRTabHelper::CreateForWebContents(web_contents);
    }
#endif
  }

#if BUILDFLAG(ENABLE_PLAYLIST)
  playlist::PlaylistTabHelper::MaybeCreateForWebContents(web_contents);
#endif
}

}  // namespace shunya

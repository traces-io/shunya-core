/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/infobars/shunya_sync_account_deleted_infobar_delegate.h"

#include <memory>
#include <utility>

#include "base/memory/ptr_util.h"
#include "shunya/browser/ui/views/infobars/shunya_sync_account_deleted_infobar.h"
#include "shunya/components/shunya_sync/shunya_sync_prefs.h"
#include "shunya/components/constants/webui_url_constants.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "shunya/grit/shunya_generated_resources.h"
#include "chrome/browser/infobars/confirm_infobar_creator.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/chrome_pages.h"
#include "components/infobars/content/content_infobar_manager.h"
#include "components/infobars/core/infobar.h"
#include "ui/base/l10n/l10n_util.h"
#include "ui/views/vector_icons.h"

// static
void ShunyaSyncAccountDeletedInfoBarDelegate::Create(
    content::WebContents* active_web_contents,
    Profile* profile,
    Browser* browser) {
  shunya_sync::Prefs shunya_sync_prefs(profile->GetPrefs());
  const bool notification_pending =
      shunya_sync_prefs.IsSyncAccountDeletedNoticePending();
  if (!notification_pending) {
    return;
  }

  // If we already are on shunya://settings/shunyaSync/setup page, don't show
  // informer
  if (!active_web_contents || active_web_contents->GetURL() ==
                                  chrome::GetSettingsUrl(kShunyaSyncSetupPath)) {
    return;
  }

  infobars::ContentInfoBarManager* infobar_manager =
      infobars::ContentInfoBarManager::FromWebContents(active_web_contents);

  if (!infobar_manager) {
    return;
  }

  // Create custom confirm infobar
  std::unique_ptr<infobars::InfoBar> infobar(
      std::make_unique<ShunyaSyncAccountDeletedInfoBar>(
          base::WrapUnique<ConfirmInfoBarDelegate>(
              new ShunyaSyncAccountDeletedInfoBarDelegate(browser, profile))));

  // Show infobar
  infobar_manager->AddInfoBar(std::move(infobar));
}

// Start class impl
ShunyaSyncAccountDeletedInfoBarDelegate::ShunyaSyncAccountDeletedInfoBarDelegate(
    Browser* browser,
    Profile* profile)
    : ConfirmInfoBarDelegate(), profile_(profile), browser_(browser) {}

ShunyaSyncAccountDeletedInfoBarDelegate::
    ~ShunyaSyncAccountDeletedInfoBarDelegate() {}

infobars::InfoBarDelegate::InfoBarIdentifier
ShunyaSyncAccountDeletedInfoBarDelegate::GetIdentifier() const {
  return SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR;
}

const gfx::VectorIcon& ShunyaSyncAccountDeletedInfoBarDelegate::GetVectorIcon()
    const {
  return views::kInfoIcon;
}

bool ShunyaSyncAccountDeletedInfoBarDelegate::ShouldExpire(
    const NavigationDetails& details) const {
  return false;
}

void ShunyaSyncAccountDeletedInfoBarDelegate::InfoBarDismissed() {
  shunya_sync::Prefs shunya_sync_prefs(profile_->GetPrefs());
  shunya_sync_prefs.SetSyncAccountDeletedNoticePending(false);
}

std::u16string ShunyaSyncAccountDeletedInfoBarDelegate::GetMessageText() const {
  // The replacement with empty string here is required to eat placeholder $1
  // in grit string resource. And it's impossible to have empty placeholder
  // <ph name="NAME"></ph>, grit compiler gives error. Placeholder is required
  // to explane translation team that message string and link text are part of
  // the same sentense.
  return l10n_util::GetStringFUTF16(
      IDS_SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR_MESSAGE, u"");
}

int ShunyaSyncAccountDeletedInfoBarDelegate::GetButtons() const {
  return BUTTON_OK;
}

std::u16string ShunyaSyncAccountDeletedInfoBarDelegate::GetButtonLabel(
    InfoBarButton button) const {
  return shunya_l10n::GetLocalizedResourceUTF16String(
      IDS_SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR_BUTTON);
}

std::u16string ShunyaSyncAccountDeletedInfoBarDelegate::GetLinkText() const {
  // See comment at |ShunyaSyncAccountDeletedInfoBarDelegate::GetMessageText|
  // above for empty substitution
  return l10n_util::GetStringFUTF16(
      IDS_SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR_LINK_TEXT, u"");
}

GURL ShunyaSyncAccountDeletedInfoBarDelegate::GetLinkURL() const {
  return chrome::GetSettingsUrl(kShunyaSyncSetupPath);
}

bool ShunyaSyncAccountDeletedInfoBarDelegate::Accept() {
  shunya_sync::Prefs shunya_sync_prefs(profile_->GetPrefs());
  shunya_sync_prefs.SetSyncAccountDeletedNoticePending(false);
  return true;
}

bool ShunyaSyncAccountDeletedInfoBarDelegate::LinkClicked(
    WindowOpenDisposition disposition) {
  shunya_sync::Prefs shunya_sync_prefs(profile_->GetPrefs());
  shunya_sync_prefs.SetSyncAccountDeletedNoticePending(false);
  InfoBarDelegate::LinkClicked(disposition);
  return true;
}

bool ShunyaSyncAccountDeletedInfoBarDelegate::IsCloseable() const {
  return false;
}

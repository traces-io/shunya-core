/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/infobars/shunya_ipfs_infobar_delegate.h"

#include <algorithm>
#include <utility>

#include "shunya/browser/ui/views/infobars/shunya_confirm_infobar.h"
#include "shunya/components/ipfs/ipfs_constants.h"
#include "shunya/components/ipfs/pref_names.h"
#include "shunya/components/l10n/common/localization_util.h"
#include "shunya/grit/shunya_generated_resources.h"
#include "components/infobars/core/infobar.h"
#include "components/prefs/pref_service.h"
#include "ui/views/vector_icons.h"

// ShunyaIPFSInfoBarDelegateObserver
ShunyaIPFSInfoBarDelegateObserver::ShunyaIPFSInfoBarDelegateObserver() = default;

ShunyaIPFSInfoBarDelegateObserver::~ShunyaIPFSInfoBarDelegateObserver() = default;

// ShunyaIPFSInfoBarDelegate
// static
void ShunyaIPFSInfoBarDelegate::Create(
    infobars::ContentInfoBarManager* infobar_manager,
    std::unique_ptr<ShunyaIPFSInfoBarDelegateObserver> observer,
    PrefService* local_state) {
  if (!local_state->GetBoolean(kShowIPFSPromoInfobar)) {
    return;
  }
  infobar_manager->AddInfoBar(std::make_unique<ShunyaConfirmInfoBar>(
                                  std::make_unique<ShunyaIPFSInfoBarDelegate>(
                                      std::move(observer), local_state)),
                              true);
}

ShunyaIPFSInfoBarDelegate::ShunyaIPFSInfoBarDelegate(
    std::unique_ptr<ShunyaIPFSInfoBarDelegateObserver> observer,
    PrefService* local_state)
    : observer_(std::move(observer)), local_state_(local_state) {}

ShunyaIPFSInfoBarDelegate::~ShunyaIPFSInfoBarDelegate() {}

// ShunyaConfirmInfoBarDelegate
bool ShunyaIPFSInfoBarDelegate::HasCheckbox() const {
  return false;
}

std::u16string ShunyaIPFSInfoBarDelegate::GetCheckboxText() const {
  NOTREACHED_NORETURN();
}

void ShunyaIPFSInfoBarDelegate::SetCheckboxChecked(bool checked) {
  NOTREACHED();
}

bool ShunyaIPFSInfoBarDelegate::InterceptClosing() {
  return false;
}

// ConfirmInfoBarDelegate
infobars::InfoBarDelegate::InfoBarIdentifier
ShunyaIPFSInfoBarDelegate::GetIdentifier() const {
  return SHUNYA_IPFS_INFOBAR_DELEGATE;
}

const gfx::VectorIcon& ShunyaIPFSInfoBarDelegate::GetVectorIcon() const {
  return views::kInfoIcon;
}

bool ShunyaIPFSInfoBarDelegate::ShouldExpire(
    const NavigationDetails& details) const {
  return details.is_navigation_to_different_page;
}

void ShunyaIPFSInfoBarDelegate::InfoBarDismissed() {}

std::u16string ShunyaIPFSInfoBarDelegate::GetMessageText() const {
  return shunya_l10n::GetLocalizedResourceUTF16String(
      IDS_SHUNYA_IPFS_INFOBAR_TEXT);
}

int ShunyaIPFSInfoBarDelegate::GetButtons() const {
  return BUTTON_OK | BUTTON_CANCEL | BUTTON_EXTRA;
}

bool ShunyaIPFSInfoBarDelegate::IsProminent(int id) const {
  return id == BUTTON_OK || id == BUTTON_EXTRA;
}

std::u16string ShunyaIPFSInfoBarDelegate::GetButtonLabel(
    InfoBarButton button) const {
  switch (button) {
    case InfoBarButton::BUTTON_OK:
      return shunya_l10n::GetLocalizedResourceUTF16String(
          IDS_SHUNYA_IPFS_INFOBAR_APPROVE);
    case InfoBarButton::BUTTON_EXTRA:
      return shunya_l10n::GetLocalizedResourceUTF16String(
          IDS_SHUNYA_IPFS_INFOBAR_APPROVE_ONCE);
    case InfoBarButton::BUTTON_CANCEL:
      return shunya_l10n::GetLocalizedResourceUTF16String(
          IDS_SHUNYA_IPFS_INFOBAR_NEVER);
    default:
      NOTREACHED_NORETURN();
  }
}

std::vector<int> ShunyaIPFSInfoBarDelegate::GetButtonsOrder() const {
  return {InfoBarButton::BUTTON_OK, InfoBarButton::BUTTON_EXTRA,
          InfoBarButton::BUTTON_CANCEL};
}

std::u16string ShunyaIPFSInfoBarDelegate::GetLinkText() const {
  return shunya_l10n::GetLocalizedResourceUTF16String(
      IDS_SHUNYA_IPFS_INFOBAR_LINK);
}

GURL ShunyaIPFSInfoBarDelegate::GetLinkURL() const {
  return GURL(ipfs::kIPFSLearnMorePrivacyURL);
}

bool ShunyaIPFSInfoBarDelegate::Accept() {
  if (observer_) {
    local_state_->SetBoolean(kShowIPFSPromoInfobar, false);
    observer_->OnRedirectToIPFS(true);
  }
  return true;
}

bool ShunyaIPFSInfoBarDelegate::ExtraButtonPressed() {
  if (observer_) {
    observer_->OnRedirectToIPFS(false);
  }
  return true;
}

bool ShunyaIPFSInfoBarDelegate::Cancel() {
  local_state_->SetBoolean(kShowIPFSPromoInfobar, false);
  return true;
}

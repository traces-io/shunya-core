/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/infobars/shunya_wayback_machine_delegate_impl.h"

#include "base/command_line.h"
#include "shunya/components/shunya_wayback_machine/shunya_wayback_machine_infobar_delegate.h"
#include "shunya/components/shunya_wayback_machine/shunya_wayback_machine_tab_helper.h"
#include "shunya/components/constants/shunya_switches.h"
#include "components/infobars/content/content_infobar_manager.h"
#include "components/infobars/core/infobar.h"

// static
void ShunyaWaybackMachineDelegateImpl::AttachTabHelperIfNeeded(
    content::WebContents* web_contents) {
  if (!base::CommandLine::ForCurrentProcess()->HasSwitch(
          switches::kDisableShunyaWaybackMachineExtension)) {
    ShunyaWaybackMachineTabHelper::CreateForWebContents(web_contents);
    auto* tab_helper =
        ShunyaWaybackMachineTabHelper::FromWebContents(web_contents);
    tab_helper->set_delegate(
        std::make_unique<ShunyaWaybackMachineDelegateImpl>());
  }
}

ShunyaWaybackMachineDelegateImpl::ShunyaWaybackMachineDelegateImpl() = default;
ShunyaWaybackMachineDelegateImpl::~ShunyaWaybackMachineDelegateImpl() = default;


void ShunyaWaybackMachineDelegateImpl::CreateInfoBar(
    content::WebContents* web_contents) {
  infobars::ContentInfoBarManager::FromWebContents(web_contents)
      ->AddInfoBar(CreateInfoBarView(
                       std::make_unique<ShunyaWaybackMachineInfoBarDelegate>(),
                       web_contents),
                   true);
}

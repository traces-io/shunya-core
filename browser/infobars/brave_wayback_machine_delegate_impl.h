/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_INFOBARS_SHUNYA_WAYBACK_MACHINE_DELEGATE_IMPL_H_
#define SHUNYA_BROWSER_INFOBARS_SHUNYA_WAYBACK_MACHINE_DELEGATE_IMPL_H_

#include <memory>

#include "shunya/components/shunya_wayback_machine/shunya_wayback_machine_delegate.h"

namespace content {
class WebContents;
}  // namespace content

namespace infobars {
class InfoBar;
}  // namespace infobars

class ShunyaWaybackMachineDelegateImpl : public ShunyaWaybackMachineDelegate {
 public:
  static void AttachTabHelperIfNeeded(content::WebContents* web_contents);

  ShunyaWaybackMachineDelegateImpl();
  ~ShunyaWaybackMachineDelegateImpl() override;

  ShunyaWaybackMachineDelegateImpl(
      const ShunyaWaybackMachineDelegateImpl&) = delete;
  ShunyaWaybackMachineDelegateImpl& operator=(
      const ShunyaWaybackMachineDelegateImpl&) = delete;

 private:
  // ShunyaWaybackMachineDelegate overrides:
  void CreateInfoBar(content::WebContents* web_contents) override;

  std::unique_ptr<infobars::InfoBar> CreateInfoBarView(
      std::unique_ptr<ShunyaWaybackMachineInfoBarDelegate> delegate,
      content::WebContents* contents);
};

#endif  // SHUNYA_BROWSER_INFOBARS_SHUNYA_WAYBACK_MACHINE_DELEGATE_IMPL_H_

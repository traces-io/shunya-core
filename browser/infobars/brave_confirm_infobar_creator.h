/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_INFOBARS_SHUNYA_CONFIRM_INFOBAR_CREATOR_H_
#define SHUNYA_BROWSER_INFOBARS_SHUNYA_CONFIRM_INFOBAR_CREATOR_H_

#include <memory>

class ShunyaConfirmInfoBarDelegate;

namespace infobars {
class InfoBar;
}  // namespace infobars

std::unique_ptr<infobars::InfoBar> CreateShunyaConfirmInfoBar(
    std::unique_ptr<ShunyaConfirmInfoBarDelegate> delegate);

#endif  // SHUNYA_BROWSER_INFOBARS_SHUNYA_CONFIRM_INFOBAR_CREATOR_H_

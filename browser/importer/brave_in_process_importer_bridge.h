/* Copyright 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_IMPORTER_SHUNYA_IN_PROCESS_IMPORTER_BRIDGE_H_
#define SHUNYA_BROWSER_IMPORTER_SHUNYA_IN_PROCESS_IMPORTER_BRIDGE_H_

#include <string>

#include "shunya/common/importer/shunya_importer_bridge.h"
#include "chrome/browser/importer/in_process_importer_bridge.h"

class ShunyaInProcessImporterBridge : public InProcessImporterBridge,
                                     public ShunyaImporterBridge {
 public:
  using InProcessImporterBridge::InProcessImporterBridge;

  ShunyaInProcessImporterBridge(const ShunyaInProcessImporterBridge&) = delete;
  ShunyaInProcessImporterBridge operator=(
      const ShunyaInProcessImporterBridge&) = delete;

  // ShunyaImporterBridge overrides:
  void SetCreditCard(const std::u16string& name_on_card,
                     const std::u16string& expiration_month,
                     const std::u16string& expiration_year,
                     const std::u16string& decrypted_card_number,
                     const std::string& origin) override;

 private:
  ~ShunyaInProcessImporterBridge() override;
};

#endif  // SHUNYA_BROWSER_IMPORTER_SHUNYA_IN_PROCESS_IMPORTER_BRIDGE_H_

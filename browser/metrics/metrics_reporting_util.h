/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_METRICS_METRICS_REPORTING_UTIL_H_
#define SHUNYA_BROWSER_METRICS_METRICS_REPORTING_UTIL_H_

bool GetDefaultPrefValueForMetricsReporting();
bool ShouldShowCrashReportPermissionAskDialog();

#endif  // SHUNYA_BROWSER_METRICS_METRICS_REPORTING_UTIL_H_

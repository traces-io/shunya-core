/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_GCM_DRIVER_SHUNYA_GCM_CHANNEL_STATUS_H_
#define SHUNYA_BROWSER_GCM_DRIVER_SHUNYA_GCM_CHANNEL_STATUS_H_

#include "base/memory/raw_ptr.h"
#include "base/supports_user_data.h"

class Profile;

namespace gcm {

class ShunyaGCMChannelStatus : public base::SupportsUserData::Data {
 public:
  explicit ShunyaGCMChannelStatus(Profile* profile, bool enabled);
  static ShunyaGCMChannelStatus* GetForProfile(Profile *profile);

  bool IsGCMEnabled() const;
  void UpdateGCMDriverStatus();

 private:
  raw_ptr<Profile> profile_ = nullptr;
  bool gcm_enabled_;
};

}  // namespace gcm

#endif  // SHUNYA_BROWSER_GCM_DRIVER_SHUNYA_GCM_CHANNEL_STATUS_H_

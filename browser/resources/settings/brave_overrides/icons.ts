// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

// @ts-nocheck TODO(petemill): Define types and remove ts-nocheck

import '../icons.html.js'
import '../shunya_icons.html.js'

import {OverrideIronIcons} from 'chrome://resources/shunya/polymer_overriding.js'

OverrideIronIcons('settings', 'shunya_settings', {
  language: 'language',
  performance: 'performance'
})
OverrideIronIcons('cr', 'shunya_settings', {
  security: 'privacy-security',
  search: 'search-engine',
  ['file-download']: 'download',
  print: 'printing'
})
OverrideIronIcons('settings20', 'shunya_settings20', {
  incognito: 'private-mode'
})

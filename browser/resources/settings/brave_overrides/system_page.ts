// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import {
  html,
  RegisterPolymerTemplateModifications,
  RegisterPolymerComponentBehaviors
} from 'chrome://resources/shunya/polymer_overriding.js'
import { loadTimeData } from '../i18n_setup.js'
import '../shunya_system_page/shunya_performance_page.js'
// <if expr="enable_shunya_vpn_wireguard">
import '../shunya_system_page/shunya_vpn_page.js'
// </if>
import '../shortcuts_page/shortcuts_page.js'
import { Router } from '../router.js'

RegisterPolymerComponentBehaviors({
  'settings-system-page': [
    {
      onShortcutsClicked_: () => {
        const router = Router.getInstance()
        router.navigateTo((router.getRoutes() as any).SHORTCUTS)
      }
    }
  ]
})

RegisterPolymerTemplateModifications({
  'settings-system-page': (templateContent) => {
    if (loadTimeData.getBoolean('areShortcutsSupported')) {
      // Chromium's system section only has a root section but we want to add a
      // subpage for shortcuts.
      // Get all of the non-style children - we want to move these into the
      // default route, rather than always showing, otherwise when we navigate,
      // we'll get all the toggles showing up.
      const nonStyleChildren = (
        Array.from(templateContent.children) as HTMLElement[]
      ).filter((t) => t.tagName !== 'STYLE')

      templateContent.appendChild(html`
        <settings-animated-pages id="pages" section="system">
          <div route-path="default">
            <cr-link-row
              on-click="onShortcutsClicked_"
              id="shortcutsButton"
              label=${loadTimeData.getString('shunyaShortcutsPage')}
              role-description="Subpage button">
              <span id="shortcutsButtonSubLabel" slot="sub-label">
            </cr-link-row>
            <div class="hr"></div>
          </div>
          <template is="dom-if" route-path="/system/shortcuts">
            <settings-subpage
              associated-control="[[$$('#shortcutsButton')]]"
              page-title=${loadTimeData.getString('shunyaShortcutsPage')}>
              <settings-shortcuts-page></settings-shortcuts-page>
            </settings-subpage>
          </template>
        </settings-animated-pages>`)
      const defaultRoute = templateContent.querySelector(
        '#pages div[route-path=default]'
      )

      for (const child of nonStyleChildren) {
        defaultRoute.appendChild(child)
      }

      // changes should happen inside the default route.
      templateContent = defaultRoute
    }

    templateContent.appendChild(
      html`
        <settings-toggle-button
          class="cr-row"
          pref="{{prefs.shunya.enable_closing_last_tab}}"
          label="${loadTimeData.getString('shunyaHelpTipsClosingLastTab')}"
        >
        </settings-toggle-button>
      `
    )
    // <if expr="enable_shunya_vpn_wireguard">
    templateContent.appendChild(
      html`
        <settings-shunya-vpn-page prefs="{{prefs}}">
        </settings-shunya-vpn-page>
      `
    )
    // </if>

    templateContent.appendChild(
      html`
        <settings-shunya-performance-page prefs="{{prefs}}">
        </settings-shunya-performance-page>
      `
    )
  }
})

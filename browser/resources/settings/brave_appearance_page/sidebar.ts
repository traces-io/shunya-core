// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import '../settings_shared.css.js'
import '../settings_vars.css.js'

import {PrefsMixin, PrefsMixinInterface} from 'chrome://resources/cr_components/settings_prefs/prefs_mixin.js';
import {I18nMixin, I18nMixinInterface} from 'chrome://resources/cr_elements/i18n_mixin.js'
import {PolymerElement} from 'chrome://resources/polymer/v3_0/polymer/polymer_bundled.min.js'

import {getTemplate} from './sidebar.html.js'

const SettingsShunyaAppearanceSidebarElementBase = PrefsMixin(I18nMixin(PolymerElement)) as {
  new (): PolymerElement & I18nMixinInterface & PrefsMixinInterface
}

/**
 * 'settings-shunya-appearance-sidebar' is the settings page area containing
 * shunya's sidebar settings in appearance settings.
 * This is separated from 'settings-shunya-appearance-toolbar' because sidebar
 * option is located below the home button option.
 */
export class SettingsShunyaAppearanceSidebarElement extends SettingsShunyaAppearanceSidebarElementBase {
  static get is() {
    return 'settings-shunya-appearance-sidebar'
  }

  static get template() {
    return getTemplate()
  }


  static get properties() {
    return {
      sidebarShowEnabledLabel_: {
        readOnly: false,
        type: String,
      },
    }
  }

  static get observers(){
    return [
      'onShowOptionChanged_(prefs.shunya.sidebar.sidebar_show_option.value)',
    ]
  }

  private sidebarShowOptions_ = [
    {value: 0, name: this.i18n('appearanceSettingsShowOptionAlways')},
    {value: 1, name: this.i18n('appearanceSettingsShowOptionMouseOver')},
    {value: 3, name: this.i18n('appearanceSettingsShowOptionNever')},
  ]
  private sidebarShowEnabledLabel_: string

  private onShowOptionChanged_() {
    this.sidebarShowEnabledLabel_ = (this.get('prefs.shunya.sidebar.sidebar_show_option.value') === 3)
        ? this.i18n('appearanceSettingsSidebarDisabledDesc')
        : this.i18n('appearanceSettingsSidebarEnabledDesc')
  }
}

customElements.define(SettingsShunyaAppearanceSidebarElement.is, SettingsShunyaAppearanceSidebarElement)

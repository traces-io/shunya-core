// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import {PolymerElement, html} from 'chrome://resources/polymer/v3_0/polymer/polymer_bundled.min.js'
import {WebUiListenerMixin, WebUiListenerMixinInterface} from 'chrome://resources/cr_elements/web_ui_listener_mixin.js'
import {routes} from '../route.js'
import {Router} from '../router.js'
import 'chrome://resources/cr_elements/md_select.css.js';
import '../settings_shared.css.js'
import '../settings_vars.css.js'
import {loadTimeData} from "../i18n_setup.js"
import {ShunyaAppearanceBrowserProxy,  ShunyaAppearanceBrowserProxyImpl} from './shunya_appearance_browser_proxy.js'
import {BaseMixin} from '../base_mixin.js'
import {getTemplate} from './shunya_theme.html.js'

export interface SettingsShunyaAppearanceThemeElement {
  $: {
    shunyaThemeType: HTMLSelectElement
  }
}

const SettingsShunyaAppearanceThemeElementBase =
  WebUiListenerMixin(BaseMixin(PolymerElement)) as {
  new (): PolymerElement & WebUiListenerMixinInterface
}

/**
 * 'settings-shunya-appearance-theme' is the settings page area containing
 * shunya's appearance related settings that located at the top of appearance
 * area.
 */
export class SettingsShunyaAppearanceThemeElement extends SettingsShunyaAppearanceThemeElementBase {
  static get is() {
    return 'settings-shunya-appearance-theme'
  }

  static get template() {
    return getTemplate()
  }

  static get observers() {
    return [
      'updateSelected_(shunyaThemeType_, shunyaThemeList_)',
    ]
  }

  browserProxy_: ShunyaAppearanceBrowserProxy = ShunyaAppearanceBrowserProxyImpl.getInstance()
  shunyaThemeList_: chrome.shunyaTheme.ThemeItem[]
  shunyaThemeType_: number // index of current theme type in shunyaThemeList_

  override ready() {
    super.ready()

    this.addWebUiListener('shunya-theme-type-changed', (type: number) => {
      this.shunyaThemeType_ = type;
    })
    this.browserProxy_.getShunyaThemeList().then((list) => {
      this.shunyaThemeList_ = JSON.parse(list) as chrome.shunyaTheme.ThemeItem[];
    })
    this.browserProxy_.getShunyaThemeType().then(type => {
      this.shunyaThemeType_ = type;
    })
  }

  private onShunyaThemeTypeChange_() {
    this.browserProxy_.setShunyaThemeType(Number(this.$.shunyaThemeType.value))
  }

  private shunyaThemeTypeEqual_(theme1: string, theme2: string) {
    return theme1 === theme2
  }

  private onThemeTap_() {
    Router.getInstance().navigateTo(routes.THEMES)
  }

  private updateSelected_() {
    // Wait for the dom-repeat to populate the <select> before setting
    // <select>#value so the correct option gets selected.
    setTimeout(() => {
      this.$.shunyaThemeType.value = String(this.shunyaThemeType_)
    })
  }

  useThemesSubPage_() {
    return loadTimeData.valueExists('superReferralThemeName') &&
      loadTimeData.getString('superReferralThemeName') !== ''
  }
}

customElements.define(
    SettingsShunyaAppearanceThemeElement.is, SettingsShunyaAppearanceThemeElement)

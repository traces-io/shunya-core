/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

import {sendWithPromise} from 'chrome://resources/js/cr.js';

/** @interface */
export interface ShunyaAppearanceBrowserProxy {
  /**
   * Returns JSON string with shape `chrome.shunyaTheme.ThemeItem[]`
   */
  getShunyaThemeList(): Promise<string>
  /**
   * Index of current ThemeItem
   */
  getShunyaThemeType(): Promise<number>
  /**
   * 
   * @param value index of ThemeItem
   */
  setShunyaThemeType(value: number): void
}

/**
 * @implements {ShunyaAppearanceBrowserProxy}
 */
export class ShunyaAppearanceBrowserProxyImpl implements
    ShunyaAppearanceBrowserProxy {
  getShunyaThemeList() {
    return new Promise<string>(resolve => chrome.shunyaTheme.getShunyaThemeList(resolve))
  }

  getShunyaThemeType() {
    return sendWithPromise('getShunyaThemeType');
  }

  setShunyaThemeType(value: number) {
    chrome.send('setShunyaThemeType', [value]);
  }

  static getInstance(): ShunyaAppearanceBrowserProxyImpl {
    return instance || (instance = new ShunyaAppearanceBrowserProxyImpl())
  }
}

let instance: ShunyaAppearanceBrowserProxy|null = null

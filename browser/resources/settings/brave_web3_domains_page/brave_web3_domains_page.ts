// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import {DropdownMenuOptionList} from '/shared/settings/controls/settings_dropdown_menu.js';
import {PrefsMixin, PrefsMixinInterface} from 'chrome://resources/cr_components/settings_prefs/prefs_mixin.js';
import {PolymerElement} from 'chrome://resources/polymer/v3_0/polymer/polymer_bundled.min.js'

import {ShunyaWeb3DomainsBrowserProxyImpl} from './shunya_web3_domains_browser_proxy.js'
import {getTemplate} from './shunya_web3_domains_page.html.js'

const SettingShunyaWeb3DomainsPageElementBase = PrefsMixin(PolymerElement) as {
  new (): PolymerElement & PrefsMixinInterface
}

export class SettingShunyaWeb3DomainsPageElement
  extends SettingShunyaWeb3DomainsPageElementBase {
  static get is() {
    return 'settings-shunya-web3-domains-page'
  }

  static get template() {
    return getTemplate()
  }

  static get properties() {
    return {
      showSnsRow_: Boolean,
      resolveMethod_: Array,
      ensOffchainResolveMethod_: Array,
      showEnsOffchainLookupRow_: {
        type: Boolean,
        computed: 'computeShowEnsOffchainLookupRow_(prefs.*)',
      },
    }
  }

  private browserProxy_ = ShunyaWeb3DomainsBrowserProxyImpl.getInstance()
  showSnsRow_: boolean
  resolveMethod_: DropdownMenuOptionList
  ensOffchainResolveMethod_: DropdownMenuOptionList

  override ready() {
    super.ready()

    this.showSnsRow_ = this.browserProxy_.isSnsEnabled()
    this.browserProxy_.getDecentralizedDnsResolveMethodList().then(list => {
      this.resolveMethod_ = list
    })
    this.browserProxy_.getEnsOffchainResolveMethodList().then(list => {
      this.ensOffchainResolveMethod_ = list
    })
  }

  computeShowEnsOffchainLookupRow_() {
    if (!this.browserProxy_.isENSL2Enabled())
      return false
    return !!this.prefs && this.getPref('shunya.ens.resolve_method').value === 3
  }
}

customElements.define(
  SettingShunyaWeb3DomainsPageElement.is, SettingShunyaWeb3DomainsPageElement)

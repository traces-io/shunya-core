// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

// @ts-nocheck TODO(petemill): Define types and remove ts-nocheck

import { sendWithPromise } from 'chrome://resources/js/cr.js';

export interface ShunyaTorBrowserProxy {
  getBridgesConfig(): Promise<any> // TODO(petemill): Define the expected type
  setBridgesConfig(config: any) // TODO(petemill): Define the expected type
  requestBridgesCaptcha(): Promise<any> // TODO(petemill): Define the expected type
  resolveBridgesCaptcha(captcha: any) // TODO(petemill): Define the expected type
  setTorEnabled(value: boolean)
  isTorEnabled(): Promise<boolean>
  isTorManaged(): Promise<boolean>
  isSnowflakeExtensionAllowed(): Promise<boolean>
  isSnowflakeExtensionEnabled(): Promise<boolean>
  enableSnowflakeExtension(enable: boolean): Promise<boolean>
}

export class ShunyaTorBrowserProxyImpl implements ShunyaTorBrowserProxy {
  static getInstance() {
    return instance || (instance = new ShunyaTorBrowserProxyImpl());
  }

  getBridgesConfig() {
    return sendWithPromise('shunya_tor.getBridgesConfig')
  }

  setBridgesConfig(config) {
    chrome.send('shunya_tor.setBridgesConfig', [config])
  }

  requestBridgesCaptcha() {
    return sendWithPromise('shunya_tor.requestBridgesCaptcha')
  }

  resolveBridgesCaptcha(captcha) {
    return sendWithPromise('shunya_tor.resolveBridgesCaptcha', captcha)
  }

  setTorEnabled(value) {
    chrome.send('shunya_tor.setTorEnabled', [value])
  }

  isTorEnabled() {
    return sendWithPromise('shunya_tor.isTorEnabled')
  }

  isTorManaged() {
    return sendWithPromise('shunya_tor.isTorManaged')
  }

  isSnowflakeExtensionAllowed() {
    return sendWithPromise('shunya_tor.isSnowflakeExtensionAllowed')
  }

  isSnowflakeExtensionEnabled(): Promise<boolean> {
    return sendWithPromise('shunya_tor.isSnowflakeExtensionEnabled')
  }

  enableSnowflakeExtension(enable): Promise<boolean> {
    return sendWithPromise('shunya_tor.enableSnowflakeExtension', enable)
  }
}

let instance: ShunyaTorBrowserProxy|null = null

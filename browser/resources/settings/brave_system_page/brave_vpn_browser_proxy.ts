// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import {sendWithPromise} from 'chrome://resources/js/cr.js';

export interface ShunyaVPNBrowserProxy {
  registerWireguardService(): Promise<boolean>;
  isWireguardServiceRegistered(): Promise<boolean>;
  isShunyaVpnConnected(): Promise<boolean>;
}

export class ShunyaVPNBrowserProxyImpl implements ShunyaVPNBrowserProxy {
  registerWireguardService () {
    return sendWithPromise('registerWireguardService');
  }

  isWireguardServiceRegistered () {
    return sendWithPromise('isWireguardServiceRegistered');
  }

  isShunyaVpnConnected () {
    return sendWithPromise('isShunyaVpnConnected');
  }

  static getInstance(): ShunyaVPNBrowserProxy {
    return instance || (instance = new ShunyaVPNBrowserProxyImpl())
  }
}

let instance: ShunyaVPNBrowserProxy|null = null

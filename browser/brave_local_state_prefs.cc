/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_local_state_prefs.h"

#include <string>

#include "base/values.h"
#include "shunya/browser/shunya_ads/analytics/p3a/shunya_stats_helper.h"
#include "shunya/browser/shunya_stats/shunya_stats_updater.h"
#include "shunya/browser/metrics/buildflags/buildflags.h"
#include "shunya/browser/metrics/metrics_reporting_util.h"
#include "shunya/browser/misc_metrics/process_misc_metrics.h"
#include "shunya/browser/ntp_background/ntp_p3a_helper_impl.h"
#include "shunya/browser/playlist/playlist_service_factory.h"
#include "shunya/browser/search_engines/search_engine_tracker.h"
#include "shunya/browser/themes/shunya_dark_mode_utils.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/shunya_ads/browser/ads_service.h"
#include "shunya/components/shunya_referrals/browser/shunya_referrals_service.h"
#include "shunya/components/shunya_search_conversion/p3a.h"
#include "shunya/components/shunya_shields/browser/ad_block_service.h"
#include "shunya/components/shunya_shields/browser/shunya_shields_p3a.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_prefs.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/decentralized_dns/core/utils.h"
#include "shunya/components/misc_metrics/general_browser_usage.h"
#include "shunya/components/misc_metrics/page_metrics_service.h"
#include "shunya/components/misc_metrics/privacy_hub_metrics.h"
#include "shunya/components/ntp_background_images/browser/ntp_background_images_service.h"
#include "shunya/components/ntp_background_images/browser/view_counter_service.h"
#include "shunya/components/p3a/p3a_service.h"
#include "shunya/components/skus/browser/skus_utils.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "build/build_config.h"
#include "chrome/common/pref_names.h"
#include "components/metrics/metrics_pref_names.h"
#include "components/prefs/pref_registry_simple.h"
#include "third_party/widevine/cdm/buildflags.h"

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/components/tor/tor_profile_service.h"
#endif

#include "shunya/browser/ui/webui/new_tab_page/shunya_new_tab_message_handler.h"

#if !BUILDFLAG(IS_ANDROID)
#include "shunya/browser/p3a/p3a_core_metrics.h"
#include "shunya/browser/ui/whats_new/whats_new_util.h"
#include "chrome/browser/first_run/first_run.h"
#endif  // !BUILDFLAG(IS_ANDROID)

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#endif

#if BUILDFLAG(ENABLE_WIDEVINE)
#include "shunya/browser/widevine/widevine_utils.h"
#endif

namespace shunya {

void RegisterLocalStatePrefsForMigration(PrefRegistrySimple* registry) {
#if BUILDFLAG(ENABLE_WIDEVINE)
  RegisterWidevineLocalstatePrefsForMigration(registry);
#endif

#if !BUILDFLAG(IS_ANDROID)
  // Added 10/2022
  registry->RegisterBooleanPref(kDefaultBrowserPromptEnabled, true);
#endif

  shunya_wallet::RegisterLocalStatePrefsForMigration(registry);
  shunya_search_conversion::p3a::RegisterLocalStatePrefsForMigration(registry);
}

void RegisterLocalStatePrefs(PrefRegistrySimple* registry) {
  shunya_shields::RegisterPrefsForAdBlockService(registry);
  shunya_stats::RegisterLocalStatePrefs(registry);
  ntp_background_images::NTPBackgroundImagesService::RegisterLocalStatePrefs(
      registry);
  ntp_background_images::ViewCounterService::RegisterLocalStatePrefs(registry);
  RegisterPrefsForShunyaReferralsService(registry);
#if BUILDFLAG(IS_MAC)
  // Turn off super annoying 'Hold to quit'
  registry->SetDefaultPrefValue(prefs::kConfirmToQuitEnabled,
                                base::Value(false));
#endif
#if BUILDFLAG(ENABLE_TOR)
  tor::TorProfileService::RegisterLocalStatePrefs(registry);
#endif
  registry->SetDefaultPrefValue(
      metrics::prefs::kMetricsReportingEnabled,
      base::Value(GetDefaultPrefValueForMetricsReporting()));

  p3a::P3AService::RegisterPrefs(registry,
#if !BUILDFLAG(IS_ANDROID)
                                 first_run::IsChromeFirstRun());
#else
                                 // ShunyaP3AService::RegisterPrefs
                                 // doesn't use this arg on Android
                                 false);
#endif  // !BUILDFLAG(IS_ANDROID)

  shunya_shields::RegisterShieldsP3ALocalPrefs(registry);
#if !BUILDFLAG(IS_ANDROID)
  ShunyaNewTabMessageHandler::RegisterLocalStatePrefs(registry);
  ShunyaWindowTracker::RegisterPrefs(registry);
  ShunyaUptimeTracker::RegisterPrefs(registry);
  dark_mode::RegisterShunyaDarkModeLocalStatePrefs(registry);
  whats_new::RegisterLocalStatePrefs(registry);
#endif

#if BUILDFLAG(ENABLE_CRASH_DIALOG)
  registry->RegisterBooleanPref(kDontAskForCrashReporting, false);
#endif

#if BUILDFLAG(ENABLE_WIDEVINE)
  RegisterWidevineLocalstatePrefs(registry);
#endif

  decentralized_dns::RegisterLocalStatePrefs(registry);

  RegisterLocalStatePrefsForMigration(registry);

  shunya_search_conversion::p3a::RegisterLocalStatePrefs(registry);

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  shunya_vpn::RegisterLocalStatePrefs(registry);
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN) || BUILDFLAG(ENABLE_AI_CHAT)
  skus::RegisterLocalStatePrefs(registry);
#endif

  registry->RegisterStringPref(::prefs::kShunyaVpnDnsConfig, std::string());

  ntp_background_images::NTPP3AHelperImpl::RegisterLocalStatePrefs(registry);

  shunya_wallet::RegisterLocalStatePrefs(registry);

  misc_metrics::ProcessMiscMetrics::RegisterPrefs(registry);
  misc_metrics::PageMetricsService::RegisterPrefs(registry);
  shunya_ads::ShunyaStatsHelper::RegisterLocalStatePrefs(registry);
  misc_metrics::GeneralBrowserUsage::RegisterPrefs(registry);

  playlist::PlaylistServiceFactory::RegisterLocalStatePrefs(registry);
}

}  // namespace shunya

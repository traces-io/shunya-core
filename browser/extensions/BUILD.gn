# Copyright (c) 2022 The Shunya Authors. All rights reserved.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at https://mozilla.org/MPL/2.0/.

import("//shunya/browser/ethereum_remote_client/buildflags/buildflags.gni")
import("//shunya/components/shunya_wayback_machine/buildflags/buildflags.gni")
import("//shunya/components/shunya_webtorrent/browser/buildflags/buildflags.gni")
import("//shunya/components/ipfs/buildflags/buildflags.gni")
import("//shunya/components/speedreader/common/buildflags/buildflags.gni")
import("//shunya/components/tor/buildflags/buildflags.gni")
import("//build/config/features.gni")
import("//build/config/ui.gni")
import("//components/gcm_driver/config.gni")

source_set("extensions") {
  # Remove when https://github.com/shunya/shunya-browser/issues/10661 is resolved
  check_includes = false
  sources = [
    "api/shunya_extensions_api_client.cc",
    "api/shunya_extensions_api_client.h",
    "api/shunya_rewards_api.cc",
    "api/shunya_rewards_api.h",
    "api/shunya_shields_api.cc",
    "api/shunya_shields_api.h",
    "api/shunya_talk_api.cc",
    "api/shunya_talk_api.h",
    "api/shunya_theme_api.cc",
    "api/shunya_theme_api.h",
    "api/greaselion_api.cc",
    "api/greaselion_api.h",
    "api/identity/shunya_web_auth_flow.cc",
    "api/identity/shunya_web_auth_flow.h",
    "api/rewards_notifications_api.cc",
    "api/rewards_notifications_api.h",
    "api/settings_private/shunya_prefs_util.cc",
    "api/settings_private/shunya_prefs_util.h",
    "shunya_component_loader.cc",
    "shunya_component_loader.h",
    "shunya_extension_management.cc",
    "shunya_extension_management.h",
    "shunya_extension_provider.cc",
    "shunya_extension_provider.h",
    "shunya_extension_service.cc",
    "shunya_extension_service.h",
    "shunya_extensions_browser_api_provider.cc",
    "shunya_extensions_browser_api_provider.h",
    "shunya_extensions_browser_client_impl.cc",
    "shunya_extensions_browser_client_impl.h",
    "shunya_theme_event_router.cc",
    "shunya_theme_event_router.h",
    "updater/shunya_update_client_config.cc",
    "updater/shunya_update_client_config.h",
  ]

  deps = [
    ":resources",
    "//base",
    "//shunya/app:shunya_generated_resources_grit",
    "//shunya/browser/shunya_rewards:util",
    "//shunya/browser/component_updater",
    "//shunya/browser/profiles",
    "//shunya/common",
    "//shunya/common/extensions/api",
    "//shunya/components/shunya_adaptive_captcha",
    "//shunya/components/shunya_ads/browser",
    "//shunya/components/shunya_ads/core",
    "//shunya/components/shunya_component_updater/browser",
    "//shunya/components/shunya_rewards/browser",
    "//shunya/components/shunya_rewards/common",
    "//shunya/components/shunya_rewards/core:headers",
    "//shunya/components/shunya_shields/browser",
    "//shunya/components/shunya_shields/common",
    "//shunya/components/shunya_vpn/common/buildflags",
    "//shunya/components/shunya_wallet/browser:utils",
    "//shunya/components/shunya_wallet/common",
    "//shunya/components/shunya_wayback_machine/buildflags",
    "//shunya/components/constants",
    "//shunya/components/de_amp/common",
    "//shunya/components/decentralized_dns/content",
    "//shunya/components/ipfs/buildflags",
    "//shunya/components/ntp_widget_utils/browser",
    "//shunya/components/tor/buildflags",
    "//chrome/browser/extensions",
    "//chrome/common",
    "//components/gcm_driver:gcm_buildflags",
    "//components/gcm_driver:gcm_driver",
    "//components/omnibox/browser:browser",
    "//components/prefs",
    "//components/services/patch/content",
    "//components/services/unzip/content",
    "//components/update_client:patch_impl",
    "//components/update_client:unzip_impl",
    "//content/public/browser",
    "//extensions/browser",
    "//extensions/common",
    "//third_party/re2",
    "//ui/base",
    "//url",
  ]

  if (toolkit_views) {
    deps += [ "//shunya/components/sidebar" ]
  }

  if (enable_speedreader) {
    deps += [ "//shunya/components/speedreader" ]
  }

  # It seems like this shunya_wallet_api should be renamed to ethereum_remote_client_api.
  # However this is not possible right now because the ethereum-remote-client extension
  # uses chrome.shunyaWallet, so the API is intentionally not being renamed now.
  if (ethereum_remote_client_enabled) {
    sources += [
      "api/shunya_wallet_api.cc",
      "api/shunya_wallet_api.h",
      "ethereum_remote_client_util.cc",
      "ethereum_remote_client_util.h",
    ]

    deps += [
      "//shunya/browser/ethereum_remote_client",
      "//shunya/components/shunya_wallet/browser",
      "//shunya/components/shunya_wallet/common:buildflags",
      "//components/infobars/content",
      "//components/prefs",
    ]
  }

  if (enable_ipfs) {
    sources += [
      "api/ipfs_api.cc",
      "api/ipfs_api.h",
    ]
    deps += [ "//shunya/components/ipfs" ]
  }

  if (enable_shunya_webtorrent) {
    deps += [ "//shunya/components/shunya_webtorrent/browser" ]
    sources += [
      "shunya_webtorrent_navigation_throttle.cc",
      "shunya_webtorrent_navigation_throttle.h",
    ]
  }

  if (enable_shunya_wayback_machine) {
    deps += [ "//shunya/components/shunya_wayback_machine" ]
  }
}

group("resources") {
  deps = [
    "//shunya/components/shunya_extension:generated_resources",
    "//shunya/components/shunya_extension:static_resources",
  ]
  if (enable_shunya_webtorrent) {
    deps += [
      "//shunya/components/shunya_webtorrent:generated_resources",
      "//shunya/components/shunya_webtorrent:static_resources",
    ]
  }
}

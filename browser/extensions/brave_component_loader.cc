/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/extensions/shunya_component_loader.h"

#include <string>

#include "base/command_line.h"
#include "base/feature_list.h"
#include "base/functional/bind.h"
#include "shunya/browser/shunya_rewards/rewards_util.h"
#include "shunya/components/shunya_component_updater/browser/shunya_component_installer.h"
#include "shunya/components/shunya_component_updater/browser/shunya_on_demand_updater.h"
#include "shunya/components/shunya_extension/grit/shunya_extension.h"
#include "shunya/components/shunya_webtorrent/grit/shunya_webtorrent_resources.h"
#include "shunya/components/constants/shunya_switches.h"
#include "shunya/components/constants/pref_names.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/extensions/extension_service.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/common/pref_names.h"
#include "components/grit/shunya_components_resources.h"
#include "components/prefs/pref_change_registrar.h"
#include "components/prefs/pref_service.h"
#include "extensions/browser/extension_prefs.h"
#include "extensions/browser/extension_registry.h"
#include "extensions/browser/extension_system.h"
#include "extensions/common/constants.h"
#include "extensions/common/mojom/manifest.mojom.h"

#if BUILDFLAG(ETHEREUM_REMOTE_CLIENT_ENABLED)
#include "shunya/browser/ethereum_remote_client/ethereum_remote_client_constants.h"
#include "shunya/browser/ethereum_remote_client/pref_names.h"
#include "shunya/browser/extensions/ethereum_remote_client_util.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_utils.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#endif

using extensions::mojom::ManifestLocation;

namespace extensions {

ShunyaComponentLoader::ShunyaComponentLoader(ExtensionSystem* extension_system,
                                           Profile* profile)
    : ComponentLoader(extension_system, profile),
      profile_(profile),
      profile_prefs_(profile->GetPrefs()) {}

ShunyaComponentLoader::~ShunyaComponentLoader() = default;

void ShunyaComponentLoader::OnComponentRegistered(std::string extension_id) {
  shunya_component_updater::ShunyaOnDemandUpdater::GetInstance()->OnDemandUpdate(
      extension_id);
}

void ShunyaComponentLoader::OnComponentReady(std::string extension_id,
                                            bool allow_file_access,
                                            const base::FilePath& install_dir,
                                            const std::string& manifest) {
  Add(manifest, install_dir);
  if (allow_file_access) {
    ExtensionPrefs::Get(profile_)->SetAllowFileAccess(extension_id, true);
  }
#if BUILDFLAG(ETHEREUM_REMOTE_CLIENT_ENABLED)
  if (extension_id == ethereum_remote_client_extension_id) {
    ReinstallAsNonComponent(ethereum_remote_client_extension_id);
  }
#endif
}

void ShunyaComponentLoader::ReinstallAsNonComponent(
    const std::string extension_id) {
  extensions::ExtensionService* service =
      extensions::ExtensionSystem::Get(profile_)->extension_service();
  extensions::ExtensionRegistry* registry =
      extensions::ExtensionRegistry::Get(profile_);
  const Extension* extension = registry->GetInstalledExtension(extension_id);
  DCHECK(extension);
  if (extension->location() == ManifestLocation::kComponent) {
    service->RemoveComponentExtension(extension_id);
    std::string error;
    scoped_refptr<Extension> normal_extension = Extension::Create(
        extension->path(), ManifestLocation::kExternalPref,
        *extension->manifest()->value(), extension->creation_flags(), &error);
    service->AddExtension(normal_extension.get());
  }
}

void ShunyaComponentLoader::AddExtension(const std::string& extension_id,
                                        const std::string& name,
                                        const std::string& public_key) {
  shunya::RegisterComponent(
      g_browser_process->component_updater(), name, public_key,
      base::BindOnce(&ShunyaComponentLoader::OnComponentRegistered,
                     base::Unretained(this), extension_id),
      base::BindRepeating(&ShunyaComponentLoader::OnComponentReady,
                          base::Unretained(this), extension_id, true));
}

void ShunyaComponentLoader::AddHangoutServicesExtension() {
  if (!profile_prefs_->FindPreference(kHangoutsEnabled) ||
      profile_prefs_->GetBoolean(kHangoutsEnabled)) {
    ForceAddHangoutServicesExtension();
  }
}

void ShunyaComponentLoader::ForceAddHangoutServicesExtension() {
  ComponentLoader::AddHangoutServicesExtension();
}

void ShunyaComponentLoader::AddDefaultComponentExtensions(
    bool skip_session_components) {
  ComponentLoader::AddDefaultComponentExtensions(skip_session_components);

  const base::CommandLine& command_line =
      *base::CommandLine::ForCurrentProcess();
  if (!command_line.HasSwitch(switches::kDisableShunyaExtension)) {
    base::FilePath shunya_extension_path(FILE_PATH_LITERAL(""));
    shunya_extension_path =
        shunya_extension_path.Append(FILE_PATH_LITERAL("shunya_extension"));
    Add(IDR_SHUNYA_EXTENSION, shunya_extension_path);
  }
}

#if BUILDFLAG(ETHEREUM_REMOTE_CLIENT_ENABLED)
void ShunyaComponentLoader::AddEthereumRemoteClientExtension() {
  AddExtension(ethereum_remote_client_extension_id,
               ethereum_remote_client_extension_name,
               ethereum_remote_client_extension_public_key);
}

void ShunyaComponentLoader::AddEthereumRemoteClientExtensionOnStartup() {
  // Only load Crypto Wallets if it is set as the default wallet
  auto default_wallet = shunya_wallet::GetDefaultEthereumWallet(profile_prefs_);
  const bool is_opted_into_cw =
      profile_prefs_->GetBoolean(kERCOptedIntoCryptoWallets);
  if (HasInfuraProjectID() && is_opted_into_cw &&
      default_wallet == shunya_wallet::mojom::DefaultWallet::CryptoWallets) {
    AddEthereumRemoteClientExtension();
  }
}

void ShunyaComponentLoader::UnloadEthereumRemoteClientExtension() {
  extensions::ExtensionService* service =
      extensions::ExtensionSystem::Get(profile_)->extension_service();
  DCHECK(service);
  service->UnloadExtension(ethereum_remote_client_extension_id,
                           extensions::UnloadedExtensionReason::DISABLE);
}
#endif

void ShunyaComponentLoader::AddWebTorrentExtension() {
  const base::CommandLine& command_line =
      *base::CommandLine::ForCurrentProcess();
  if (!command_line.HasSwitch(switches::kDisableWebTorrentExtension) &&
      (!profile_prefs_->FindPreference(kWebTorrentEnabled) ||
       profile_prefs_->GetBoolean(kWebTorrentEnabled))) {
    base::FilePath shunya_webtorrent_path(FILE_PATH_LITERAL(""));
    shunya_webtorrent_path =
        shunya_webtorrent_path.Append(FILE_PATH_LITERAL("shunya_webtorrent"));
    Add(IDR_SHUNYA_WEBTORRENT, shunya_webtorrent_path);
  }
}

}  // namespace extensions

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSION_SERVICE_H_
#define SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSION_SERVICE_H_

#include "chrome/browser/extensions/extension_service.h"

namespace extensions {

class ShunyaExtensionService : public ExtensionService {
 public:
  // Constructor stores pointers to |profile| and |extension_prefs| but
  // ownership remains at caller.
  ShunyaExtensionService(Profile* profile,
                        const base::CommandLine* command_line,
                        const base::FilePath& install_directory,
                        const base::FilePath& unpacked_install_directory,
                        ExtensionPrefs* extension_prefs,
                        Blocklist* blocklist,
                        bool autoupdate_enabled,
                        bool extensions_enabled,
                        base::OneShotEvent* ready);
  ShunyaExtensionService(const ShunyaExtensionService&) = delete;
  ShunyaExtensionService& operator=(const ShunyaExtensionService&) = delete;
  ~ShunyaExtensionService() override;

  void AddComponentExtension(const Extension* extension) override;
};

}  // namespace extensions

#endif  // SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSION_SERVICE_H_

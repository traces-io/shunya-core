/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSIONS_BROWSER_API_PROVIDER_H_
#define SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSIONS_BROWSER_API_PROVIDER_H_

#include "extensions/browser/extensions_browser_api_provider.h"

namespace extensions {

class ShunyaExtensionsBrowserAPIProvider : public ExtensionsBrowserAPIProvider {
 public:
  ShunyaExtensionsBrowserAPIProvider();
  ShunyaExtensionsBrowserAPIProvider(const ShunyaExtensionsBrowserAPIProvider&) =
      delete;
  ShunyaExtensionsBrowserAPIProvider& operator=(
      const ShunyaExtensionsBrowserAPIProvider&) = delete;
  ~ShunyaExtensionsBrowserAPIProvider() override;

  void RegisterExtensionFunctions(ExtensionFunctionRegistry* registry) override;
};

}  // namespace extensions

#endif  // SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSIONS_BROWSER_API_PROVIDER_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSIONS_BROWSER_CLIENT_IMPL_H_
#define SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSIONS_BROWSER_CLIENT_IMPL_H_

#include "chrome/browser/extensions/chrome_extensions_browser_client.h"

namespace extensions {

class ShunyaExtensionsBrowserClientImpl : public ChromeExtensionsBrowserClient {
 public:
  ShunyaExtensionsBrowserClientImpl();
  ShunyaExtensionsBrowserClientImpl(const ShunyaExtensionsBrowserClientImpl&) =
      delete;
  ShunyaExtensionsBrowserClientImpl& operator=(
      const ShunyaExtensionsBrowserClientImpl&) = delete;
  ~ShunyaExtensionsBrowserClientImpl() override = default;
};

}  // namespace extensions

#endif  // SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSIONS_BROWSER_CLIENT_IMPL_H_

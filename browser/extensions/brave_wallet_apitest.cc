/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "base/environment.h"
#include "base/path_service.h"
#include "shunya/browser/ethereum_remote_client/ethereum_remote_client_constants.h"
#include "shunya/browser/ethereum_remote_client/pref_names.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_utils.h"
#include "shunya/components/shunya_wallet/browser/pref_names.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "shunya/components/constants/shunya_paths.h"
#include "shunya/components/constants/pref_names.h"
#include "chrome/browser/extensions/extension_apitest.h"
#include "chrome/browser/extensions/extension_service.h"
#include "chrome/test/base/ui_test_utils.h"
#include "components/prefs/pref_service.h"
#include "content/public/test/browser_test.h"
#include "extensions/common/constants.h"
#include "extensions/test/result_catcher.h"

namespace extensions {
namespace {

class ShunyaWalletExtensionApiTest : public ExtensionApiTest {
 public:
  void SetUp() override {
    shunya::RegisterPathProvider();
    base::PathService::Get(shunya::DIR_TEST_DATA, &extension_dir_);
    extension_dir_ = extension_dir_.AppendASCII("extensions/api_test");
    ExtensionApiTest::SetUp();
  }
  void TearDown() override { ExtensionApiTest::TearDown(); }
  PrefService* GetPrefs() { return browser()->profile()->GetPrefs(); }
  base::FilePath extension_dir_;
};

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ShunyaExtensionWithWalletHasAccess) {
  ResultCatcher catcher;
  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaShieldsWithWallet"));
  ASSERT_TRUE(extension);
  ASSERT_TRUE(browsertest_util::ExecuteScriptInBackgroundPageNoWait(
      browser()->profile(), shunya_extension_id, "testBasics()"));
  ASSERT_TRUE(catcher.GetNextResult()) << message_;
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest, ShunyaWalletAPIAvailable) {
  std::unique_ptr<base::Environment> env(base::Environment::Create());
  env->SetVar("SHUNYA_INFURA_PROJECT_ID", "test-project-id");
  env->SetVar("SHUNYA_SERVICES_KEY", "test-shunya-key");
  ResultCatcher catcher;
  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaWallet"));
  ASSERT_TRUE(extension);
  ASSERT_TRUE(browsertest_util::ExecuteScriptInBackgroundPageNoWait(
      browser()->profile(), ethereum_remote_client_extension_id,
      "testBasics()"));
  ASSERT_TRUE(catcher.GetNextResult()) << message_;
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ShunyaWalletAPIKnownValuesTest) {
  GetPrefs()->SetString(kERCAES256GCMSivNonce, "yJngKDr5nCGYz7EM");
  GetPrefs()->SetString(
      kERCEncryptedSeed,
      "IQu5fUMbXG6E7v8ITwcIKL3TI3rst0LU1US7ZxCKpgAGgLNAN6DbCN7nMF2Eg7Kx");
  ResultCatcher catcher;
  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaWallet"));
  ASSERT_TRUE(extension);
  ASSERT_TRUE(browsertest_util::ExecuteScriptInBackgroundPageNoWait(
      browser()->profile(), ethereum_remote_client_extension_id,
      "testKnownSeedValuesEndToEnd()"));
  ASSERT_TRUE(catcher.GetNextResult()) << message_;
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ShunyaWalletWeb3ProviderCryptoWallets) {
  shunya_wallet::SetDefaultEthereumWallet(
      GetPrefs(), shunya_wallet::mojom::DefaultWallet::CryptoWallets);
  ResultCatcher catcher;
  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaWallet"));
  ASSERT_TRUE(extension);
  ASSERT_TRUE(browsertest_util::ExecuteScriptInBackgroundPageNoWait(
      browser()->profile(), ethereum_remote_client_extension_id,
      "testProviderIsCryptoWallets()"));
  ASSERT_TRUE(catcher.GetNextResult()) << message_;
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ShunyaWalletWeb3ProviderIsShunyaWalletPreferExtension) {
  shunya_wallet::SetDefaultEthereumWallet(
      GetPrefs(),
      shunya_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension);
  ResultCatcher catcher;
  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaWallet"));
  ASSERT_TRUE(extension);
  ASSERT_TRUE(browsertest_util::ExecuteScriptInBackgroundPageNoWait(
      browser()->profile(), ethereum_remote_client_extension_id,
      "testProviderIsShunyaWalletPreferExtension()"));
  ASSERT_TRUE(catcher.GetNextResult()) << message_;
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ShunyaWalletWeb3ProviderNone) {
  shunya_wallet::SetDefaultEthereumWallet(
      GetPrefs(), shunya_wallet::mojom::DefaultWallet::None);
  ResultCatcher catcher;
  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaWallet"));
  ASSERT_TRUE(extension);
  ASSERT_TRUE(browsertest_util::ExecuteScriptInBackgroundPageNoWait(
      browser()->profile(), ethereum_remote_client_extension_id,
      "testProviderIsNone()"));
  ASSERT_TRUE(catcher.GetNextResult()) << message_;
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ShunyaWalletWeb3ProviderShunyaWallet) {
  shunya_wallet::SetDefaultEthereumWallet(
      GetPrefs(), shunya_wallet::mojom::DefaultWallet::ShunyaWallet);
  ResultCatcher catcher;
  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaWallet"));
  ASSERT_TRUE(extension);
  ASSERT_TRUE(browsertest_util::ExecuteScriptInBackgroundPageNoWait(
      browser()->profile(), ethereum_remote_client_extension_id,
      "testProviderIsShunyaWallet()"));
  ASSERT_TRUE(catcher.GetNextResult()) << message_;
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ShunyaWalletAPINotAvailable) {
  ResultCatcher catcher;
  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("notShunyaWallet"));
  ASSERT_TRUE(extension);
  ASSERT_TRUE(catcher.GetNextResult()) << message_;
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ChangeTypeCryptoWalletsToShunyaWallet) {
  shunya_wallet::SetDefaultEthereumWallet(
      browser()->profile()->GetPrefs(),
      shunya_wallet::mojom::DefaultWallet::CryptoWallets);
  ASSERT_TRUE(
      ui_test_utils::NavigateToURL(browser(), GURL("shunya://settings/")));

  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaWallet"));
  ASSERT_TRUE(extension);
  extensions::ExtensionService* service =
      extensions::ExtensionSystem::Get(browser()->profile())
          ->extension_service();
  ASSERT_TRUE(service->IsExtensionEnabled(ethereum_remote_client_extension_id));
  shunya_wallet::SetDefaultEthereumWallet(
      browser()->profile()->GetPrefs(),
      shunya_wallet::mojom::DefaultWallet::ShunyaWallet);
  ASSERT_FALSE(
      service->IsExtensionEnabled(ethereum_remote_client_extension_id));
}

IN_PROC_BROWSER_TEST_F(ShunyaWalletExtensionApiTest,
                       ChangeTypeCryptoWalletsToShunyaWalletPreferExtension) {
  shunya_wallet::SetDefaultEthereumWallet(
      browser()->profile()->GetPrefs(),
      shunya_wallet::mojom::DefaultWallet::CryptoWallets);
  ASSERT_TRUE(
      ui_test_utils::NavigateToURL(browser(), GURL("shunya://settings/")));

  const Extension* extension =
      LoadExtension(extension_dir_.AppendASCII("shunyaWallet"));
  ASSERT_TRUE(extension);
  extensions::ExtensionService* service =
      extensions::ExtensionSystem::Get(browser()->profile())
          ->extension_service();
  ASSERT_TRUE(service->IsExtensionEnabled(ethereum_remote_client_extension_id));
  shunya_wallet::SetDefaultEthereumWallet(
      browser()->profile()->GetPrefs(),
      shunya_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension);
  ASSERT_FALSE(
      service->IsExtensionEnabled(ethereum_remote_client_extension_id));
}

}  // namespace
}  // namespace extensions

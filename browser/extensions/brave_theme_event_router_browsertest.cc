/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/extensions/shunya_theme_event_router.h"
#include "shunya/browser/themes/shunya_dark_mode_utils.h"
#include "shunya/browser/themes/shunya_theme_service.h"
#include "shunya/components/constants/pref_names.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/themes/theme_service_factory.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/test/base/in_process_browser_test.h"
#include "components/prefs/pref_service.h"
#include "content/public/test/browser_test.h"
#include "testing/gmock/include/gmock/gmock.h"

using ShunyaThemeEventRouterBrowserTest = InProcessBrowserTest;

namespace extensions {

class MockShunyaThemeEventRouter : public ShunyaThemeEventRouter {
 public:
  using ShunyaThemeEventRouter::ShunyaThemeEventRouter;
  ~MockShunyaThemeEventRouter() override = default;

  MOCK_METHOD0(Notify, void());
};

}  // namespace extensions

IN_PROC_BROWSER_TEST_F(ShunyaThemeEventRouterBrowserTest,
                       ThemeChangeTest) {
  dark_mode::SetShunyaDarkModeType(
      dark_mode::ShunyaDarkModeType::SHUNYA_DARK_MODE_TYPE_DARK);

  extensions::MockShunyaThemeEventRouter* mock_router =
      new extensions::MockShunyaThemeEventRouter(browser()->profile());
  ShunyaThemeService* service = static_cast<ShunyaThemeService*>(
      ThemeServiceFactory::GetForProfile(browser()->profile()));
  service->SetShunyaThemeEventRouterForTesting(mock_router);

  EXPECT_CALL(*mock_router, Notify()).Times(1);
  dark_mode::SetShunyaDarkModeType(
      dark_mode::ShunyaDarkModeType::SHUNYA_DARK_MODE_TYPE_LIGHT);

  EXPECT_CALL(*mock_router, Notify()).Times(1);
  dark_mode::SetShunyaDarkModeType(
      dark_mode::ShunyaDarkModeType::SHUNYA_DARK_MODE_TYPE_DARK);

  EXPECT_CALL(*mock_router, Notify()).Times(0);
  dark_mode::SetShunyaDarkModeType(
      dark_mode::ShunyaDarkModeType::SHUNYA_DARK_MODE_TYPE_DARK);
}

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/extensions/shunya_extension_provider.h"

#include <string>

#include "extensions/common/constants.h"

namespace extensions {

ShunyaExtensionProvider::ShunyaExtensionProvider() = default;

ShunyaExtensionProvider::~ShunyaExtensionProvider() = default;

std::string ShunyaExtensionProvider::GetDebugPolicyProviderName() const {
#if defined(NDEBUG)
  NOTREACHED();
  return std::string();
#else
  return "Shunya Extension Provider";
#endif
}

bool ShunyaExtensionProvider::MustRemainInstalled(const Extension* extension,
                                                 std::u16string* error) const {
  return extension->id() == shunya_extension_id;
}

}  // namespace extensions

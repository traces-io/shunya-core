/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_THEME_API_H_
#define SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_THEME_API_H_

#include "extensions/browser/extension_function.h"

namespace extensions {
namespace api {

class ShunyaThemeGetShunyaThemeListFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaTheme.getShunyaThemeList", UNKNOWN)

 protected:
  ~ShunyaThemeGetShunyaThemeListFunction() override {}

  ResponseAction Run() override;
};

class ShunyaThemeGetShunyaThemeTypeFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaTheme.getShunyaThemeType", UNKNOWN)

 protected:
  ~ShunyaThemeGetShunyaThemeTypeFunction() override {}

  ResponseAction Run() override;
};

class ShunyaThemeSetShunyaThemeTypeFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaTheme.setShunyaThemeType", UNKNOWN)

 protected:
  ~ShunyaThemeSetShunyaThemeTypeFunction() override {}

  ResponseAction Run() override;
};

}  // namespace api
}  // namespace extensions

#endif  // SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_THEME_API_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_REWARDS_API_H_
#define SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_REWARDS_API_H_

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "base/types/expected.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom.h"
#include "shunya/components/shunya_rewards/common/mojom/rewards_types.mojom.h"
#include "shunya/components/shunya_rewards/core/mojom_structs.h"
#include "extensions/browser/extension_function.h"

namespace extensions {
namespace api {

class ShunyaRewardsIsSupportedFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.isSupported", UNKNOWN)

 protected:
  ~ShunyaRewardsIsSupportedFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsIsUnsupportedRegionFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.isUnsupportedRegion", UNKNOWN)

 protected:
  ~ShunyaRewardsIsUnsupportedRegionFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsRecordNTPPanelTriggerFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.recordNTPPanelTrigger", UNKNOWN)

 protected:
  ~ShunyaRewardsRecordNTPPanelTriggerFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsOpenRewardsPanelFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.openRewardsPanel", UNKNOWN)

 protected:
  ~ShunyaRewardsOpenRewardsPanelFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsShowRewardsSetupFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.showRewardsSetup", UNKNOWN)

 protected:
  ~ShunyaRewardsShowRewardsSetupFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsShowGrantCaptchaFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.showGrantCaptcha", UNKNOWN)

 protected:
  ~ShunyaRewardsShowGrantCaptchaFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsUpdateMediaDurationFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.updateMediaDuration", UNKNOWN)

 protected:
  ~ShunyaRewardsUpdateMediaDurationFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetPublisherInfoFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getPublisherInfo", UNKNOWN)

 protected:
  ~ShunyaRewardsGetPublisherInfoFunction() override;

  ResponseAction Run() override;

 private:
  void OnGetPublisherInfo(const shunya_rewards::mojom::Result result,
                          shunya_rewards::mojom::PublisherInfoPtr info);
};

class ShunyaRewardsSetPublisherIdForTabFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.setPublisherIdForTab", UNKNOWN)

 protected:
  ~ShunyaRewardsSetPublisherIdForTabFunction() override;
  ResponseAction Run() override;
};

class ShunyaRewardsGetPublisherInfoForTabFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getPublisherInfoForTab", UNKNOWN)

 protected:
  ~ShunyaRewardsGetPublisherInfoForTabFunction() override;

  ResponseAction Run() override;

 private:
  void OnGetPublisherPanelInfo(shunya_rewards::mojom::Result result,
                               shunya_rewards::mojom::PublisherInfoPtr info);
};

class ShunyaRewardsGetPublisherPanelInfoFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getPublisherPanelInfo", UNKNOWN)

 protected:
  ~ShunyaRewardsGetPublisherPanelInfoFunction() override;

  ResponseAction Run() override;

 private:
  void OnGetPublisherPanelInfo(const shunya_rewards::mojom::Result result,
                               shunya_rewards::mojom::PublisherInfoPtr info);
};

class ShunyaRewardsSavePublisherInfoFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.savePublisherInfo", UNKNOWN)

 protected:
  ~ShunyaRewardsSavePublisherInfoFunction() override;

  ResponseAction Run() override;

 private:
  void OnSavePublisherInfo(const shunya_rewards::mojom::Result result);
};

class ShunyaRewardsTipSiteFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.tipSite", UNKNOWN)

 protected:
  ~ShunyaRewardsTipSiteFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsTipUserFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.tipUser", UNKNOWN)

 protected:
  ~ShunyaRewardsTipUserFunction() override;

  ResponseAction Run() override;

 private:
  void OnTipUserGetPublisherInfo(const shunya_rewards::mojom::Result result,
                                 shunya_rewards::mojom::PublisherInfoPtr info);
  void OnTipUserSavePublisherInfo(const shunya_rewards::mojom::Result result);
  void ShowTipDialog();
};

class ShunyaRewardsGetPublisherDataFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getPublisherData", UNKNOWN)

 protected:
  ~ShunyaRewardsGetPublisherDataFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetRewardsParametersFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getRewardsParameters", UNKNOWN)

 protected:
  ~ShunyaRewardsGetRewardsParametersFunction() override;

  ResponseAction Run() override;

 private:
  void OnGetRewardsParameters(
      shunya_rewards::mojom::RewardsParametersPtr parameters);
};

class ShunyaRewardsCreateRewardsWalletFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.createRewardsWallet", UNKNOWN)

 protected:
  ~ShunyaRewardsCreateRewardsWalletFunction() override;

  ResponseAction Run() override;

 private:
  void CreateRewardsWalletCallback(
      shunya_rewards::mojom::CreateRewardsWalletResult result);
};

class ShunyaRewardsGetAvailableCountriesFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getAvailableCountries", UNKNOWN)

 protected:
  ~ShunyaRewardsGetAvailableCountriesFunction() override;

 private:
  void GetAvailableCountriesCallback(std::vector<std::string> countries);

  ResponseAction Run() override;
};

class ShunyaRewardsGetDefaultCountryFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getDefaultCountry", UNKNOWN)

 protected:
  ~ShunyaRewardsGetDefaultCountryFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetDeclaredCountryFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getDeclaredCountry", UNKNOWN)
 protected:
  ~ShunyaRewardsGetDeclaredCountryFunction() override;
  ResponseAction Run() override;
};

class ShunyaRewardsIsGrandfatheredUserFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.isGrandfatheredUser", UNKNOWN)

 protected:
  ~ShunyaRewardsIsGrandfatheredUserFunction() override;
  ResponseAction Run() override;
};

class ShunyaRewardsGetUserTypeFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getUserType", UNKNOWN)

 protected:
  ~ShunyaRewardsGetUserTypeFunction() override;
  ResponseAction Run() override;

 private:
  void Callback(shunya_rewards::mojom::UserType user_type);
};

class ShunyaRewardsGetPublishersVisitedCountFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getPublishersVisitedCount", UNKNOWN)

 protected:
  ~ShunyaRewardsGetPublishersVisitedCountFunction() override;

  ResponseAction Run() override;

 private:
  void Callback(int count);
};

class ShunyaRewardsGetBalanceReportFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getBalanceReport", UNKNOWN)

 protected:
  ~ShunyaRewardsGetBalanceReportFunction() override;

  ResponseAction Run() override;

 private:
  void OnBalanceReport(const shunya_rewards::mojom::Result result,
                       shunya_rewards::mojom::BalanceReportInfoPtr report);
};

class ShunyaRewardsIncludeInAutoContributionFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.includeInAutoContribution", UNKNOWN)

 protected:
  ~ShunyaRewardsIncludeInAutoContributionFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsFetchPromotionsFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.fetchPromotions", UNKNOWN)

 protected:
  ~ShunyaRewardsFetchPromotionsFunction() override;

  ResponseAction Run() override;

 private:
  void OnPromotionsFetched(
      std::vector<shunya_rewards::mojom::PromotionPtr> promotions);
};

class ShunyaRewardsClaimPromotionFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.claimPromotion", UNKNOWN)

 protected:
  ~ShunyaRewardsClaimPromotionFunction() override;

  ResponseAction Run() override;

 private:
  void OnClaimPromotion(const std::string& promotion_id,
                        const shunya_rewards::mojom::Result result,
                        const std::string& captcha_image,
                        const std::string& hint,
                        const std::string& captcha_id);
};

class ShunyaRewardsAttestPromotionFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.attestPromotion", UNKNOWN)

 protected:
  ~ShunyaRewardsAttestPromotionFunction() override;

  ResponseAction Run() override;

 private:
  void OnAttestPromotion(const std::string& promotion_id,
                         const shunya_rewards::mojom::Result result,
                         shunya_rewards::mojom::PromotionPtr promotion);
};

class ShunyaRewardsSetAutoContributeEnabledFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.setAutoContributeEnabled", UNKNOWN)

 protected:
  ~ShunyaRewardsSetAutoContributeEnabledFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetACEnabledFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getACEnabled", UNKNOWN)

 protected:
  ~ShunyaRewardsGetACEnabledFunction() override;

  ResponseAction Run() override;

 private:
  void OnGetACEnabled(bool enabled);
};

class ShunyaRewardsSaveRecurringTipFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.saveRecurringTip", UNKNOWN)

 protected:
  ~ShunyaRewardsSaveRecurringTipFunction() override;

  ResponseAction Run() override;

 private:
  void OnSaveRecurringTip(shunya_rewards::mojom::Result result);
};

class ShunyaRewardsRemoveRecurringTipFunction :
  public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.removeRecurringTip", UNKNOWN)

 protected:
  ~ShunyaRewardsRemoveRecurringTipFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetRecurringTipsFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getRecurringTips", UNKNOWN)

 protected:
  ~ShunyaRewardsGetRecurringTipsFunction() override;

  ResponseAction Run() override;

 private:
  void OnGetRecurringTips(
      std::vector<shunya_rewards::mojom::PublisherInfoPtr> list);
};

class ShunyaRewardsRefreshPublisherFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.refreshPublisher", UNKNOWN)

 protected:
  ~ShunyaRewardsRefreshPublisherFunction() override;

  ResponseAction Run() override;

 private:
  void OnRefreshPublisher(const shunya_rewards::mojom::PublisherStatus status,
                          const std::string& publisher_key);
};

class ShunyaRewardsGetAllNotificationsFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getAllNotifications", UNKNOWN)

 protected:
  ~ShunyaRewardsGetAllNotificationsFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetInlineTippingPlatformEnabledFunction :
    public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION(
      "shunyaRewards.getInlineTippingPlatformEnabled",
      UNKNOWN)

 protected:
  ~ShunyaRewardsGetInlineTippingPlatformEnabledFunction() override;

  ResponseAction Run() override;

 private:
  void OnInlineTipSetting(bool value);
};

class ShunyaRewardsFetchBalanceFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.fetchBalance", UNKNOWN)

 protected:
  ~ShunyaRewardsFetchBalanceFunction() override;

  ResponseAction Run() override;

 private:
  void OnFetchBalance(
      base::expected<shunya_rewards::mojom::BalancePtr,
                     shunya_rewards::mojom::FetchBalanceError> result);
};

class ShunyaRewardsGetExternalWalletProvidersFunction
    : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getExternalWalletProviders", UNKNOWN)

 protected:
  ~ShunyaRewardsGetExternalWalletProvidersFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetExternalWalletFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getExternalWallet", UNKNOWN)

 protected:
  ~ShunyaRewardsGetExternalWalletFunction() override;

  ResponseAction Run() override;

 private:
  void OnGetExternalWallet(
      base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                     shunya_rewards::mojom::GetExternalWalletError> result);
};

class ShunyaRewardsGetRewardsEnabledFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getRewardsEnabled", UNKNOWN)

 protected:
  ~ShunyaRewardsGetRewardsEnabledFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetAdsAccountStatementFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getAdsAccountStatement", UNKNOWN)

 protected:
  ~ShunyaRewardsGetAdsAccountStatementFunction() override;

  ResponseAction Run() override;

 private:
  void OnGetAdsAccountStatement(shunya_ads::mojom::StatementInfoPtr statement);
};

class ShunyaRewardsIsInitializedFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.isInitialized", UNKNOWN)

 protected:
  ~ShunyaRewardsIsInitializedFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetScheduledCaptchaInfoFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getScheduledCaptchaInfo", UNKNOWN)

 protected:
  ~ShunyaRewardsGetScheduledCaptchaInfoFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsUpdateScheduledCaptchaResultFunction
    : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.updateScheduledCaptchaResult",
                             UNKNOWN)

 protected:
  ~ShunyaRewardsUpdateScheduledCaptchaResultFunction() override;

  ResponseAction Run() override;
};

class ShunyaRewardsGetPrefsFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaRewards.getPrefs", UNKNOWN)

 protected:
  ~ShunyaRewardsGetPrefsFunction() override;

  ResponseAction Run() override;

 private:
  void GetAutoContributePropertiesCallback(
      shunya_rewards::mojom::AutoContributePropertiesPtr properties);
};

}  // namespace api
}  // namespace extensions

#endif  // SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_REWARDS_API_H_

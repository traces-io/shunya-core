/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_SHIELDS_API_H_
#define SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_SHIELDS_API_H_

#include <memory>
#include <string>

#include "extensions/browser/extension_function.h"

namespace extensions {
namespace api {

class ShunyaShieldsAddSiteCosmeticFilterFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaShields.addSiteCosmeticFilter", UNKNOWN)

 protected:
  ~ShunyaShieldsAddSiteCosmeticFilterFunction() override {}

  ResponseAction Run() override;
};

class ShunyaShieldsOpenFilterManagementPageFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaShields.openFilterManagementPage", UNKNOWN)

 protected:
  ~ShunyaShieldsOpenFilterManagementPageFunction() override {}

  ResponseAction Run() override;
};

}  // namespace api
}  // namespace extensions

#endif  // SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_SHIELDS_API_H_

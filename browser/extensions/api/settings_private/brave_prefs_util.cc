/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/extensions/api/settings_private/shunya_prefs_util.h"

#include "base/feature_list.h"
#include "shunya/browser/ethereum_remote_client/buildflags/buildflags.h"
#include "shunya/browser/ui/tabs/shunya_tab_prefs.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"
#include "shunya/components/shunya_news/common/pref_names.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/shunya_shields/common/pref_names.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/shunya_wallet/browser/pref_names.h"
#include "shunya/components/shunya_wayback_machine/buildflags/buildflags.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/de_amp/common/pref_names.h"
#include "shunya/components/debounce/common/pref_names.h"
#include "shunya/components/decentralized_dns/core/pref_names.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/ntp_background_images/common/pref_names.h"
#include "shunya/components/omnibox/browser/shunya_omnibox_prefs.h"
#include "shunya/components/request_otr/common/pref_names.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "chrome/browser/extensions/api/settings_private/prefs_util.h"
#include "chrome/common/extensions/api/settings_private.h"
#include "chrome/common/pref_names.h"
#include "components/browsing_data/core/pref_names.h"
#include "components/gcm_driver/gcm_buildflags.h"
#include "components/omnibox/browser/omnibox_prefs.h"
#include "components/search_engines/search_engines_pref_names.h"
#include "extensions/buildflags/buildflags.h"

#if BUILDFLAG(ENABLE_SHUNYA_WAYBACK_MACHINE)
#include "shunya/components/shunya_wayback_machine/pref_names.h"
#endif

#if BUILDFLAG(ETHEREUM_REMOTE_CLIENT_ENABLED)
#include "shunya/browser/ethereum_remote_client/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/components/ipfs/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_AI_CHAT)
#include "shunya/components/ai_chat/common/pref_names.h"
#endif

#if defined(TOOLKIT_VIEWS)
#include "shunya/components/sidebar/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/components/speedreader/speedreader_pref_names.h"
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/components/tor/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/components/shunya_vpn/common/pref_names.h"
#endif

namespace extensions {

using ntp_background_images::prefs::kNewTabPageShowBackgroundImage;
using ntp_background_images::prefs::
    kNewTabPageShowSponsoredImagesBackgroundImage;
using ntp_background_images::prefs::kNewTabPageSuperReferralThemesOption;

namespace settings_api = api::settings_private;

const PrefsUtil::TypedPrefMap& ShunyaPrefsUtil::GetAllowlistedKeys() {
  // Static cache, similar to parent class
  static PrefsUtil::TypedPrefMap* s_shunya_allowlist = nullptr;
  if (s_shunya_allowlist)
    return *s_shunya_allowlist;
  s_shunya_allowlist = new PrefsUtil::TypedPrefMap();
  // Start with parent class allowlist
  const auto chromium_prefs = PrefsUtil::GetAllowlistedKeys();
  s_shunya_allowlist->insert(chromium_prefs.begin(), chromium_prefs.end());
  // Add Shunya values to the allowlist
  // import data
  (*s_shunya_allowlist)[kImportDialogExtensions] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kImportDialogPayments] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  // Default Shunya shields
  (*s_shunya_allowlist)[kShieldsAdvancedViewEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kShieldsStatsBadgeVisible] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kAdControlType] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kNoScriptControlType] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kGoogleLoginControlType] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_shields::prefs::kFBEmbedControlType] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_shields::prefs::kTwitterEmbedControlType] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_shields::prefs::kLinkedInEmbedControlType] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_shields::prefs::kReduceLanguageEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;

  // Rewards/Ads prefs
  (*s_shunya_allowlist)[shunya_rewards::prefs::kEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_rewards::prefs::kShowLocationBarButton] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_rewards::prefs::kInlineTipButtonsEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_rewards::prefs::kInlineTipRedditEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_rewards::prefs::kInlineTipTwitterEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_rewards::prefs::kInlineTipGithubEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;

  // Search engine prefs
  (*s_shunya_allowlist)[prefs::kAddOpenSearchEngines] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[prefs::kSyncedDefaultPrivateSearchProviderGUID] =
      settings_api::PrefType::PREF_TYPE_NUMBER;

  // autofill prefs
  (*s_shunya_allowlist)[kShunyaAutofillPrivateWindows] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;

  // appearance prefs
  (*s_shunya_allowlist)[kShowBookmarksButton] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kShowSidePanelButton] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_news::prefs::kShouldShowToolbarButton] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kLocationBarIsWide] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[omnibox::kAutocompleteEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[omnibox::kTopSiteSuggestionsEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[omnibox::kHistorySuggestionsEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[omnibox::kBookmarkSuggestionsEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kAskEnableWidvine] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kNewTabPageSuperReferralThemesOption] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[kTabsSearchShow] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_tabs::kTabHoverMode] =
      settings_api::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[kTabMuteIndicatorNotClickable] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  (*s_shunya_allowlist)[shunya_vpn::prefs::kShunyaVPNShowButton] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#if BUILDFLAG(ENABLE_SHUNYA_VPN_WIREGUARD)
  (*s_shunya_allowlist)[shunya_vpn::prefs::kShunyaVPNWireguardEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#endif
#endif
#if defined(TOOLKIT_VIEWS)
  (*s_shunya_allowlist)[sidebar::kSidebarShowOption] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
#endif
#if BUILDFLAG(ENABLE_SPEEDREADER)
  (*s_shunya_allowlist)[speedreader::kSpeedreaderPrefEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#endif
  // De-AMP feature
  (*s_shunya_allowlist)[de_amp::kDeAmpPrefEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  // Debounce feature
  (*s_shunya_allowlist)[debounce::prefs::kDebounceEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  // new tab prefs
  (*s_shunya_allowlist)[kNewTabPageShowSponsoredImagesBackgroundImage] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kNewTabPageShowBackgroundImage] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kNewTabPageShowClock] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kNewTabPageShowStats] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kNewTabPageShowRewards] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kNewTabPageShowShunyaTalk] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kNewTabPageShowsOptions] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
#if BUILDFLAG(ENABLE_EXTENSIONS)
  // Web discovery prefs
  (*s_shunya_allowlist)[kWebDiscoveryEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#endif
  // Clear browsing data on exit prefs.
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteBrowsingHistoryOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteDownloadHistoryOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteCacheOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteCookiesOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeletePasswordsOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteFormDataOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteSiteSettingsOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteHostedAppsDataOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteShunyaLeoHistory] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[browsing_data::prefs::kDeleteShunyaLeoHistoryOnExit] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kAlwaysShowBookmarkBarOnNTP] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kMRUCyclingEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  // WebTorrent pref
  (*s_shunya_allowlist)[kWebTorrentEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#if BUILDFLAG(ENABLE_SHUNYA_WAYBACK_MACHINE)
  (*s_shunya_allowlist)[kShunyaWaybackMachineEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#endif
  (*s_shunya_allowlist)[kEnableWindowClosingConfirm] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kEnableClosingLastTab] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  // Hangouts pref
  (*s_shunya_allowlist)[kHangoutsEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  // IPFS Companion pref
  (*s_shunya_allowlist)[kIPFSCompanionEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;

  // Shunya Wallet pref
  (*s_shunya_allowlist)[kShunyaWalletSelectedNetworks] =
      settings_api::PrefType::PREF_TYPE_DICTIONARY;
  (*s_shunya_allowlist)[kDefaultEthereumWallet] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[kDefaultSolanaWallet] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[kDefaultBaseCurrency] =
      settings_api::PrefType::PREF_TYPE_STRING;
  (*s_shunya_allowlist)[kDefaultBaseCryptocurrency] =
      settings_api::PrefType::PREF_TYPE_STRING;
  (*s_shunya_allowlist)[kShowWalletIconOnToolbar] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kShunyaWalletAutoLockMinutes] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[kShunyaWalletNftDiscoveryEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;

  // IPFS pref
#if BUILDFLAG(ENABLE_IPFS)
  (*s_shunya_allowlist)[kIPFSResolveMethod] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[kIPFSAutoFallbackToGateway] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kIPFSPublicGatewayAddress] =
      settings_api::PrefType::PREF_TYPE_STRING;
  (*s_shunya_allowlist)[kIPFSPublicNFTGatewayAddress] =
      settings_api::PrefType::PREF_TYPE_STRING;
  (*s_shunya_allowlist)[kIPFSAutoRedirectToConfiguredGateway] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[kIpfsStorageMax] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
#endif

// Leo Assistant pref
#if BUILDFLAG(ENABLE_AI_CHAT)
  (*s_shunya_allowlist)[ai_chat::prefs::kShunyaChatAutoGenerateQuestions] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#endif

#if !BUILDFLAG(USE_GCM_FROM_PLATFORM)
  // Push Messaging Pref
  (*s_shunya_allowlist)[kShunyaGCMChannelStatus] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#endif
  // Omnibox pref
  (*s_shunya_allowlist)[omnibox::kPreventUrlElisionsInOmnibox] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#if BUILDFLAG(ENABLE_TOR)
  (*s_shunya_allowlist)[tor::prefs::kAutoOnionRedirect] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[tor::prefs::kBridgesConfig] =
      settings_api::PrefType::PREF_TYPE_DICTIONARY;
#endif
  (*s_shunya_allowlist)[prefs::kWebRTCIPHandlingPolicy] =
      settings_api::PrefType::PREF_TYPE_STRING;
  // Request OTR feature
  (*s_shunya_allowlist)[request_otr::kRequestOTRActionOption] =
      settings_api::PrefType::PREF_TYPE_NUMBER;

  (*s_shunya_allowlist)[decentralized_dns::kUnstoppableDomainsResolveMethod] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[decentralized_dns::kENSResolveMethod] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[decentralized_dns::kEnsOffchainResolveMethod] =
      settings_api::PrefType::PREF_TYPE_NUMBER;
  (*s_shunya_allowlist)[decentralized_dns::kSnsResolveMethod] =
      settings_api::PrefType::PREF_TYPE_NUMBER;

  // Media router pref
  (*s_shunya_allowlist)[kEnableMediaRouterOnRestart] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;

  // NFT pinning pref
  (*s_shunya_allowlist)[kAutoPinEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;

#if defined(TOOLKIT_VIEWS)
  // Vertical tab strip prefs
  (*s_shunya_allowlist)[shunya_tabs::kVerticalTabsEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_tabs::kVerticalTabsFloatingEnabled] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
  (*s_shunya_allowlist)[shunya_tabs::kVerticalTabsShowTitleOnWindow] =
      settings_api::PrefType::PREF_TYPE_BOOLEAN;
#endif
  return *s_shunya_allowlist;
}

}  // namespace extensions

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_WALLET_API_H_
#define SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_WALLET_API_H_

#include "extensions/browser/extension_function.h"

class Profile;

namespace extensions {
namespace api {

class ShunyaWalletReadyFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.ready", UNKNOWN)

 protected:
  ~ShunyaWalletReadyFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletNotifyWalletUnlockFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.notifyWalletUnlock", UNKNOWN)

 protected:
  ~ShunyaWalletNotifyWalletUnlockFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletLoadUIFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.loadUI", UNKNOWN)
  void OnLoaded();

 protected:
  ~ShunyaWalletLoadUIFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletShouldPromptForSetupFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.shouldPromptForSetup", UNKNOWN)

 protected:
  ~ShunyaWalletShouldPromptForSetupFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletGetWalletSeedFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.getWalletSeed", UNKNOWN)

 protected:
  ~ShunyaWalletGetWalletSeedFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletGetProjectIDFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.getProjectID", UNKNOWN)

 protected:
  ~ShunyaWalletGetProjectIDFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletGetShunyaKeyFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.getShunyaKey", UNKNOWN)

 protected:
  ~ShunyaWalletGetShunyaKeyFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletResetWalletFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.resetWallet", UNKNOWN)

 protected:
  ~ShunyaWalletResetWalletFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletGetWeb3ProviderFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.getWeb3Provider", UNKNOWN)

 protected:
  ~ShunyaWalletGetWeb3ProviderFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletGetWeb3ProviderListFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.getWeb3ProviderList", UNKNOWN)

 protected:
  ~ShunyaWalletGetWeb3ProviderListFunction() override {}
  ResponseAction Run() override;
};

class ShunyaWalletIsNativeWalletEnabledFunction : public ExtensionFunction {
 public:
  DECLARE_EXTENSION_FUNCTION("shunyaWallet.isNativeWalletEnabled", UNKNOWN)

 protected:
  ~ShunyaWalletIsNativeWalletEnabledFunction() override {}
  ResponseAction Run() override;
};

}  // namespace api
}  // namespace extensions

#endif  // SHUNYA_BROWSER_EXTENSIONS_API_SHUNYA_WALLET_API_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/extensions/api/shunya_theme_api.h"

#include <memory>
#include <string>

#include "base/json/json_writer.h"
#include "base/values.h"
#include "shunya/browser/themes/shunya_dark_mode_utils.h"
#include "shunya/common/extensions/api/shunya_theme.h"

namespace extensions {
namespace api {

ExtensionFunction::ResponseAction ShunyaThemeGetShunyaThemeListFunction::Run() {
  std::string json_string;
  base::JSONWriter::Write(dark_mode::GetShunyaDarkModeTypeList(), &json_string);
  return RespondNow(WithArguments(json_string));
}

ExtensionFunction::ResponseAction ShunyaThemeGetShunyaThemeTypeFunction::Run() {
  const std::string theme_type =
      dark_mode::GetStringFromShunyaDarkModeType(
          dark_mode::GetActiveShunyaDarkModeType());
  return RespondNow(WithArguments(theme_type));
}

ExtensionFunction::ResponseAction ShunyaThemeSetShunyaThemeTypeFunction::Run() {
  absl::optional<shunya_theme::SetShunyaThemeType::Params> params =
      shunya_theme::SetShunyaThemeType::Params::Create(args());
  EXTENSION_FUNCTION_VALIDATE(params);

  dark_mode::SetShunyaDarkModeType(params->type);

  return RespondNow(NoArguments());
}

}  // namespace api
}  // namespace extensions

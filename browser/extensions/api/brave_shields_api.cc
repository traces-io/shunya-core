/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/extensions/api/shunya_shields_api.h"

#include <utility>

#include "shunya/browser/shunya_browser_process.h"
#include "shunya/browser/ui/shunya_pages.h"
#include "shunya/common/extensions/api/shunya_shields.h"
#include "shunya/components/shunya_shields/browser/ad_block_custom_filters_provider.h"
#include "shunya/components/shunya_shields/browser/ad_block_service.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/content_settings/cookie_settings_factory.h"
#include "chrome/browser/ui/browser_finder.h"
#include "components/content_settings/core/browser/cookie_settings.h"

namespace extensions {
namespace api {

ExtensionFunction::ResponseAction
ShunyaShieldsAddSiteCosmeticFilterFunction::Run() {
  absl::optional<shunya_shields::AddSiteCosmeticFilter::Params> params =
      shunya_shields::AddSiteCosmeticFilter::Params::Create(args());
  EXTENSION_FUNCTION_VALIDATE(params);

  g_shunya_browser_process->ad_block_service()
      ->custom_filters_provider()
      ->HideElementOnHost(params->css_selector, params->host);

  return RespondNow(NoArguments());
}

ExtensionFunction::ResponseAction
ShunyaShieldsOpenFilterManagementPageFunction::Run() {
  Browser* browser = chrome::FindLastActive();
  if (browser) {
    shunya::ShowShunyaAdblock(browser);
  }

  return RespondNow(NoArguments());
}

}  // namespace api
}  // namespace extensions

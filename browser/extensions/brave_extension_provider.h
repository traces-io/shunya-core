/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSION_PROVIDER_H_
#define SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSION_PROVIDER_H_

#include <string>

#include "extensions/browser/management_policy.h"

namespace extensions {

class ShunyaExtensionProvider : public ManagementPolicy::Provider {
 public:
  ShunyaExtensionProvider();
  ShunyaExtensionProvider(const ShunyaExtensionProvider&) = delete;
  ShunyaExtensionProvider& operator=(const ShunyaExtensionProvider&) = delete;
  ~ShunyaExtensionProvider() override;
  std::string GetDebugPolicyProviderName() const override;
  bool MustRemainInstalled(const Extension* extension,
                           std::u16string* error) const override;
};

}  // namespace extensions

#endif  // SHUNYA_BROWSER_EXTENSIONS_SHUNYA_EXTENSION_PROVIDER_H_

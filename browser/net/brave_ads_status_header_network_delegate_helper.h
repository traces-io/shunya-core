/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_NET_SHUNYA_ADS_STATUS_HEADER_NETWORK_DELEGATE_HELPER_H_
#define SHUNYA_BROWSER_NET_SHUNYA_ADS_STATUS_HEADER_NETWORK_DELEGATE_HELPER_H_

#include <memory>

#include "shunya/browser/net/url_context.h"

namespace net {
class HttpRequestHeaders;
}  // namespace net

namespace shunya {

constexpr char kAdsStatusHeader[] = "X-Shunya-Ads-Enabled";
constexpr char kAdsEnabledStatusValue[] = "1";

int OnBeforeStartTransaction_AdsStatusHeader(
    net::HttpRequestHeaders* headers,
    const ResponseCallback& next_callback,
    std::shared_ptr<ShunyaRequestInfo> ctx);

}  // namespace shunya

#endif  // SHUNYA_BROWSER_NET_SHUNYA_ADS_STATUS_HEADER_NETWORK_DELEGATE_HELPER_H_

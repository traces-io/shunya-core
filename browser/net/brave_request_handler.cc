/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/net/shunya_request_handler.h"

#include <algorithm>
#include <utility>

#include "base/containers/contains.h"
#include "base/feature_list.h"
#include "shunya/browser/net/shunya_ad_block_csp_network_delegate_helper.h"
#include "shunya/browser/net/shunya_ad_block_tp_network_delegate_helper.h"
#include "shunya/browser/net/shunya_ads_status_header_network_delegate_helper.h"
#include "shunya/browser/net/shunya_common_static_redirect_network_delegate_helper.h"
#include "shunya/browser/net/shunya_httpse_network_delegate_helper.h"
#include "shunya/browser/net/shunya_localhost_permission_network_delegate_helper.h"
#include "shunya/browser/net/shunya_reduce_language_network_delegate_helper.h"
#include "shunya/browser/net/shunya_referrals_network_delegate_helper.h"
#include "shunya/browser/net/shunya_service_key_network_delegate_helper.h"
#include "shunya/browser/net/shunya_site_hacks_network_delegate_helper.h"
#include "shunya/browser/net/shunya_stp_util.h"
#include "shunya/browser/net/decentralized_dns_network_delegate_helper.h"
#include "shunya/browser/net/global_privacy_control_network_delegate_helper.h"
#include "shunya/components/shunya_shields/common/features.h"
#include "shunya/components/shunya_webtorrent/browser/buildflags/buildflags.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "chrome/browser/browser_process.h"
#include "content/public/browser/browser_task_traits.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/common/url_constants.h"
#include "extensions/buildflags/buildflags.h"
#include "extensions/common/constants.h"
#include "net/base/features.h"
#include "net/base/net_errors.h"
#include "third_party/blink/public/common/features.h"

#if BUILDFLAG(ENABLE_SHUNYA_WEBTORRENT)
#include "shunya/browser/net/shunya_torrent_redirect_network_delegate_helper.h"
#endif

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/browser/net/ipfs_redirect_network_delegate_helper.h"
#include "shunya/components/ipfs/features.h"
#endif

static bool IsInternalScheme(std::shared_ptr<shunya::ShunyaRequestInfo> ctx) {
  DCHECK(ctx);
#if BUILDFLAG(ENABLE_EXTENSIONS)
  if (ctx->request_url.SchemeIs(extensions::kExtensionScheme))
    return true;
#endif
  return ctx->request_url.SchemeIs(content::kChromeUIScheme);
}

ShunyaRequestHandler::ShunyaRequestHandler() {
  DCHECK_CURRENTLY_ON(content::BrowserThread::UI);
  SetupCallbacks();
}

ShunyaRequestHandler::~ShunyaRequestHandler() = default;

void ShunyaRequestHandler::SetupCallbacks() {
  shunya::OnBeforeURLRequestCallback callback =
      base::BindRepeating(shunya::OnBeforeURLRequest_SiteHacksWork);
  before_url_request_callbacks_.push_back(callback);

  callback = base::BindRepeating(shunya::OnBeforeURLRequest_AdBlockTPPreWork);
  before_url_request_callbacks_.push_back(callback);

  if (!base::FeatureList::IsEnabled(net::features::kShunyaHttpsByDefault)) {
    callback = base::BindRepeating(shunya::OnBeforeURLRequest_HttpsePreFileWork);
    before_url_request_callbacks_.push_back(callback);
  }

  callback =
      base::BindRepeating(shunya::OnBeforeURLRequest_CommonStaticRedirectWork);
  before_url_request_callbacks_.push_back(callback);

  callback = base::BindRepeating(
      decentralized_dns::OnBeforeURLRequest_DecentralizedDnsPreRedirectWork);
  before_url_request_callbacks_.push_back(callback);

  if (base::FeatureList::IsEnabled(
          shunya_shields::features::kShunyaLocalhostAccessPermission)) {
    callback =
        base::BindRepeating(shunya::OnBeforeURLRequest_LocalhostPermissionWork);
    before_url_request_callbacks_.push_back(callback);
  }

#if BUILDFLAG(ENABLE_IPFS)
  if (base::FeatureList::IsEnabled(ipfs::features::kIpfsFeature)) {
    callback = base::BindRepeating(ipfs::OnBeforeURLRequest_IPFSRedirectWork);
    before_url_request_callbacks_.push_back(callback);
  }
#endif

  shunya::OnBeforeStartTransactionCallback start_transaction_callback =
      base::BindRepeating(shunya::OnBeforeStartTransaction_SiteHacksWork);
  before_start_transaction_callbacks_.push_back(start_transaction_callback);

  if (base::FeatureList::IsEnabled(
          blink::features::kShunyaGlobalPrivacyControl)) {
    start_transaction_callback = base::BindRepeating(
        shunya::OnBeforeStartTransaction_GlobalPrivacyControlWork);
    before_start_transaction_callbacks_.push_back(start_transaction_callback);
  }

  start_transaction_callback =
      base::BindRepeating(shunya::OnBeforeStartTransaction_ShunyaServiceKey);
  before_start_transaction_callbacks_.push_back(start_transaction_callback);

  start_transaction_callback =
      base::BindRepeating(shunya::OnBeforeStartTransaction_ReferralsWork);
  before_start_transaction_callbacks_.push_back(start_transaction_callback);

  if (base::FeatureList::IsEnabled(
          shunya_shields::features::kShunyaReduceLanguage)) {
    start_transaction_callback =
        base::BindRepeating(shunya::OnBeforeStartTransaction_ReduceLanguageWork);
    before_start_transaction_callbacks_.push_back(start_transaction_callback);
  }

  start_transaction_callback =
      base::BindRepeating(shunya::OnBeforeStartTransaction_AdsStatusHeader);
  before_start_transaction_callbacks_.push_back(start_transaction_callback);

#if BUILDFLAG(ENABLE_SHUNYA_WEBTORRENT)
  shunya::OnHeadersReceivedCallback headers_received_callback =
      base::BindRepeating(webtorrent::OnHeadersReceived_TorrentRedirectWork);
  headers_received_callbacks_.push_back(headers_received_callback);
#endif

  if (base::FeatureList::IsEnabled(
          ::shunya_shields::features::kShunyaAdblockCspRules)) {
    shunya::OnHeadersReceivedCallback headers_received_callback2 =
        base::BindRepeating(shunya::OnHeadersReceived_AdBlockCspWork);
    headers_received_callbacks_.push_back(headers_received_callback2);
  }
}

bool ShunyaRequestHandler::IsRequestIdentifierValid(
    uint64_t request_identifier) {
  return base::Contains(callbacks_, request_identifier);
}

int ShunyaRequestHandler::OnBeforeURLRequest(
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx,
    net::CompletionOnceCallback callback,
    GURL* new_url) {
  if (before_url_request_callbacks_.empty() || IsInternalScheme(ctx)) {
    return net::OK;
  }
  ctx->new_url = new_url;
  ctx->event_type = shunya::kOnBeforeRequest;
  callbacks_[ctx->request_identifier] = std::move(callback);
  RunNextCallback(ctx);
  return net::ERR_IO_PENDING;
}

int ShunyaRequestHandler::OnBeforeStartTransaction(
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx,
    net::CompletionOnceCallback callback,
    net::HttpRequestHeaders* headers) {
  if (before_start_transaction_callbacks_.empty() || IsInternalScheme(ctx)) {
    return net::OK;
  }
  ctx->event_type = shunya::kOnBeforeStartTransaction;
  ctx->headers = headers;
  callbacks_[ctx->request_identifier] = std::move(callback);
  RunNextCallback(ctx);
  return net::ERR_IO_PENDING;
}

int ShunyaRequestHandler::OnHeadersReceived(
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx,
    net::CompletionOnceCallback callback,
    const net::HttpResponseHeaders* original_response_headers,
    scoped_refptr<net::HttpResponseHeaders>* override_response_headers,
    GURL* allowed_unsafe_redirect_url) {
  if (!ctx->tab_origin.is_empty()) {
    shunya::RemoveTrackableSecurityHeadersForThirdParty(
        ctx->request_url, url::Origin::Create(ctx->tab_origin),
        original_response_headers, override_response_headers);
  }

  if (headers_received_callbacks_.empty() &&
      !ctx->request_url.SchemeIs(content::kChromeUIScheme)) {
    // Extension scheme not excluded since shunya_webtorrent needs it.
    return net::OK;
  }

  callbacks_[ctx->request_identifier] = std::move(callback);
  ctx->event_type = shunya::kOnHeadersReceived;
  ctx->original_response_headers = original_response_headers;
  ctx->override_response_headers = override_response_headers;
  ctx->allowed_unsafe_redirect_url = allowed_unsafe_redirect_url;

  RunNextCallback(ctx);
  return net::ERR_IO_PENDING;
}

void ShunyaRequestHandler::OnURLRequestDestroyed(
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx) {
  if (base::Contains(callbacks_, ctx->request_identifier)) {
    callbacks_.erase(ctx->request_identifier);
  }
}

void ShunyaRequestHandler::RunCallbackForRequestIdentifier(
    uint64_t request_identifier,
    int rv) {
  std::map<uint64_t, net::CompletionOnceCallback>::iterator it =
      callbacks_.find(request_identifier);
  // We intentionally do the async call to maintain the proper flow
  // of URLLoader callbacks.
  content::GetUIThreadTaskRunner({})->PostTask(
      FROM_HERE, base::BindOnce(std::move(it->second), rv));
}

// TODO(iefremov): Merge all callback containers into one and run only one loop
// instead of many (issues/5574).
void ShunyaRequestHandler::RunNextCallback(
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx) {
  DCHECK_CURRENTLY_ON(content::BrowserThread::UI);

  if (!base::Contains(callbacks_, ctx->request_identifier)) {
    return;
  }

  if (ctx->pending_error.has_value()) {
    RunCallbackForRequestIdentifier(ctx->request_identifier,
                                    ctx->pending_error.value());
    return;
  }

  // Continue processing callbacks until we hit one that returns PENDING
  int rv = net::OK;

  if (ctx->event_type == shunya::kOnBeforeRequest) {
    while (before_url_request_callbacks_.size() !=
           ctx->next_url_request_index) {
      shunya::OnBeforeURLRequestCallback callback =
          before_url_request_callbacks_[ctx->next_url_request_index++];
      shunya::ResponseCallback next_callback =
          base::BindRepeating(&ShunyaRequestHandler::RunNextCallback,
                              weak_factory_.GetWeakPtr(), ctx);
      rv = callback.Run(next_callback, ctx);
      if (rv == net::ERR_IO_PENDING) {
        return;
      }
      if (rv != net::OK) {
        break;
      }
    }
  } else if (ctx->event_type == shunya::kOnBeforeStartTransaction) {
    while (before_start_transaction_callbacks_.size() !=
           ctx->next_url_request_index) {
      shunya::OnBeforeStartTransactionCallback callback =
          before_start_transaction_callbacks_[ctx->next_url_request_index++];
      shunya::ResponseCallback next_callback =
          base::BindRepeating(&ShunyaRequestHandler::RunNextCallback,
                              weak_factory_.GetWeakPtr(), ctx);
      rv = callback.Run(ctx->headers, next_callback, ctx);
      if (rv == net::ERR_IO_PENDING) {
        return;
      }
      if (rv != net::OK) {
        break;
      }
    }
  } else if (ctx->event_type == shunya::kOnHeadersReceived) {
    while (headers_received_callbacks_.size() != ctx->next_url_request_index) {
      shunya::OnHeadersReceivedCallback callback =
          headers_received_callbacks_[ctx->next_url_request_index++];
      shunya::ResponseCallback next_callback =
          base::BindRepeating(&ShunyaRequestHandler::RunNextCallback,
                              weak_factory_.GetWeakPtr(), ctx);
      rv = callback.Run(ctx->original_response_headers,
                        ctx->override_response_headers,
                        ctx->allowed_unsafe_redirect_url, next_callback, ctx);
      if (rv == net::ERR_IO_PENDING) {
        return;
      }
      if (rv != net::OK) {
        break;
      }
    }
  }

  if (rv != net::OK) {
    RunCallbackForRequestIdentifier(ctx->request_identifier, rv);
    return;
  }

  if (ctx->event_type == shunya::kOnBeforeRequest) {
    if (!ctx->new_url_spec.empty() &&
        (ctx->new_url_spec != ctx->request_url.spec()) &&
        IsRequestIdentifierValid(ctx->request_identifier)) {
      *ctx->new_url = GURL(ctx->new_url_spec);
    }
    if (ctx->blocked_by == shunya::kAdBlocked ||
        ctx->blocked_by == shunya::kOtherBlocked) {
      if (!ctx->ShouldMockRequest()) {
        RunCallbackForRequestIdentifier(ctx->request_identifier,
                                        net::ERR_BLOCKED_BY_CLIENT);
        return;
      }
    }
  }
  RunCallbackForRequestIdentifier(ctx->request_identifier, rv);
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_NET_SHUNYA_HTTPSE_NETWORK_DELEGATE_H_
#define SHUNYA_BROWSER_NET_SHUNYA_HTTPSE_NETWORK_DELEGATE_H_

#include "shunya/browser/net/url_context.h"

namespace shunya {

int OnBeforeURLRequest_HttpsePreFileWork(
    const ResponseCallback& next_callback,
    std::shared_ptr<ShunyaRequestInfo> ctx);

}  // namespace shunya

#endif  // SHUNYA_BROWSER_NET_SHUNYA_NETWORK_DELEGATE_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_NET_SHUNYA_SYSTEM_REQUEST_HANDLER_H_
#define SHUNYA_BROWSER_NET_SHUNYA_SYSTEM_REQUEST_HANDLER_H_

#include <string>

namespace network {
struct ResourceRequest;
}

extern const char kShunyaServicesKeyHeader[];

namespace shunya {

std::string ShunyaServicesKeyForTesting();

void AddShunyaServicesKeyHeader(network::ResourceRequest* url_request);

network::ResourceRequest OnBeforeSystemRequest(
    const network::ResourceRequest& url_request);

}  // namespace shunya

#endif  // SHUNYA_BROWSER_NET_SHUNYA_SYSTEM_REQUEST_HANDLER_H_

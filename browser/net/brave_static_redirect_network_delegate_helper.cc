/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/net/shunya_static_redirect_network_delegate_helper.h"

#include <algorithm>
#include <memory>
#include <string>
#include <vector>

#include "base/strings/string_piece_forward.h"
#include "shunya/browser/net/shunya_geolocation_buildflags.h"
#include "shunya/browser/safebrowsing/buildflags.h"
#include "shunya/components/constants/network_constants.h"
#include "shunya/components/widevine/static_buildflags.h"
#include "extensions/common/url_pattern.h"
#include "net/base/net_errors.h"

namespace shunya {

const char kSafeBrowsingTestingEndpoint[] = "test.safebrowsing.com";

namespace {

bool g_safebrowsing_api_endpoint_for_testing_ = false;

base::StringPiece GetSafeBrowsingEndpoint() {
  if (g_safebrowsing_api_endpoint_for_testing_)
    return kSafeBrowsingTestingEndpoint;
  return BUILDFLAG(SAFEBROWSING_ENDPOINT);
}

}  // namespace

void SetSafeBrowsingEndpointForTesting(bool testing) {
  g_safebrowsing_api_endpoint_for_testing_ = testing;
}

int OnBeforeURLRequest_StaticRedirectWork(
    const ResponseCallback& next_callback,
    std::shared_ptr<ShunyaRequestInfo> ctx) {
  GURL new_url;
  int rc = OnBeforeURLRequest_StaticRedirectWorkForGURL(ctx->request_url,
                                                        &new_url);
  if (!new_url.is_empty()) {
    ctx->new_url_spec = new_url.spec();
  }
  return rc;
}

int OnBeforeURLRequest_StaticRedirectWorkForGURL(
    const GURL& request_url,
    GURL* new_url) {
  GURL::Replacements replacements;
  static URLPattern geo_pattern(URLPattern::SCHEME_HTTPS, kGeoLocationsPattern);
  static URLPattern safeBrowsing_pattern(URLPattern::SCHEME_HTTPS,
                                         kSafeBrowsingPrefix);
  static URLPattern safebrowsingfilecheck_pattern(URLPattern::SCHEME_HTTPS,
                                                  kSafeBrowsingFileCheckPrefix);
  static URLPattern safebrowsingcrxlist_pattern(URLPattern::SCHEME_HTTPS,
                                                kSafeBrowsingCrxListPrefix);

  // To-Do (@jumde) - Update the naming for the variables below
  // https://github.com/shunya/shunya-browser/issues/10314
  static URLPattern crlSet_pattern1(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS, kCRLSetPrefix1);
  static URLPattern crlSet_pattern2(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS, kCRLSetPrefix2);
  static URLPattern crlSet_pattern3(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS, kCRLSetPrefix3);
  static URLPattern crlSet_pattern4(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS, kCRLSetPrefix4);
  static URLPattern crxDownload_pattern(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS, kCRXDownloadPrefix);
  static URLPattern autofill_pattern(
      URLPattern::SCHEME_HTTPS, kAutofillPrefix);
  static URLPattern gvt1_pattern(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS, "*://*.gvt1.com/*");
  static URLPattern googleDl_pattern(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS,
      "*://dl.google.com/*");

  static URLPattern widevine_gvt1_pattern(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS,
      kWidevineGvt1Prefix);
  static URLPattern widevine_google_dl_pattern(
      URLPattern::SCHEME_HTTP | URLPattern::SCHEME_HTTPS,
      kWidevineGoogleDlPrefix);
#if BUILDFLAG(WIDEVINE_ARM64_DLL_FIX)
  static URLPattern widevine_google_dl_pattern_win_arm64(
      URLPattern::SCHEME_HTTPS, kWidevineGoogleDlPrefixWinArm64);
#endif  // BUILDFLAG(WIDEVINE_ARM64_DLL_FIX)

  if (geo_pattern.MatchesURL(request_url)) {
    *new_url = GURL(BUILDFLAG(GOOGLEAPIS_URL));
    return net::OK;
  }

  auto safebrowsing_endpoint = GetSafeBrowsingEndpoint();
  if (!safebrowsing_endpoint.empty() &&
      safeBrowsing_pattern.MatchesHost(request_url)) {
    replacements.SetHostStr(safebrowsing_endpoint);
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (!safebrowsing_endpoint.empty() &&
      safebrowsingfilecheck_pattern.MatchesHost(request_url)) {
    replacements.SetHostStr(kShunyaSafeBrowsingSslProxy);
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (!safebrowsing_endpoint.empty() &&
      safebrowsingcrxlist_pattern.MatchesHost(request_url)) {
    replacements.SetHostStr(kShunyaSafeBrowsing2Proxy);
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (crxDownload_pattern.MatchesURL(request_url)) {
    replacements.SetSchemeStr("https");
    replacements.SetHostStr("crxdownload.shunya.com");
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (autofill_pattern.MatchesURL(request_url)) {
    replacements.SetSchemeStr("https");
    replacements.SetHostStr(kShunyaStaticProxy);
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (crlSet_pattern1.MatchesURL(request_url)) {
    replacements.SetSchemeStr("https");
    replacements.SetHostStr("redirector.shunya.com");
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (crlSet_pattern2.MatchesURL(request_url)) {
    replacements.SetSchemeStr("https");
    replacements.SetHostStr("redirector.shunya.com");
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (crlSet_pattern3.MatchesURL(request_url)) {
    replacements.SetSchemeStr("https");
    replacements.SetHostStr("redirector.shunya.com");
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (crlSet_pattern4.MatchesURL(request_url)) {
    replacements.SetSchemeStr("https");
    replacements.SetHostStr("redirector.shunya.com");
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }
  if (gvt1_pattern.MatchesURL(request_url) &&
      !widevine_gvt1_pattern.MatchesURL(request_url)) {
    replacements.SetSchemeStr("https");
    replacements.SetHostStr(kShunyaRedirectorProxy);
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  if (googleDl_pattern.MatchesURL(request_url) &&
#if BUILDFLAG(WIDEVINE_ARM64_DLL_FIX)
      !widevine_google_dl_pattern_win_arm64.MatchesURL(request_url) &&
#endif
      !widevine_google_dl_pattern.MatchesURL(request_url)) {
    replacements.SetSchemeStr("https");
    replacements.SetHostStr(kShunyaRedirectorProxy);
    *new_url = request_url.ReplaceComponents(replacements);
    return net::OK;
  }

  return net::OK;
}

}  // namespace shunya

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_NET_SHUNYA_NETWORK_AUDIT_ALLOWED_LISTS_H_
#define SHUNYA_BROWSER_NET_SHUNYA_NETWORK_AUDIT_ALLOWED_LISTS_H_

namespace shunya {

// Before adding to this list, get approval from the security team.
constexpr const char* kAllowedUrlProtocols[] = {
    "chrome-extension", "chrome", "shunya", "file", "data", "blob",
};

// Before adding to this list, get approval from the security team.
constexpr const char* kAllowedUrlPrefixes[] = {
    // allowed because it 307's to https://componentupdater.shunya.com
    "https://componentupdater.shunya.com/service/update2",
    "https://crxdownload.shunya.com/crx/blobs/",

    // Omaha/Sparkle
    "https://updates.shunyasoftware.com/",

    // stats/referrals
    "https://laptop-updates.shunya.com/",
    "https://laptop-updates-staging.shunya.com/",

    // needed for DoH on Mac build machines
    "https://dns.google/dns-query",

    // needed for DoH on Mac build machines
    "https://chrome.cloudflare-dns.com/dns-query",

    // for fetching tor client updater component
    "https://tor.shunyasoftware.com/",

    // shunya sync v2 production
    "https://sync-v2.shunya.com/v2",

    // shunya sync v2 staging
    "https://sync-v2.shunyasoftware.com/v2",

    // shunya sync v2 dev
    "https://sync-v2.shunya.software/v2",

    // shunya A/B testing
    "https://variations.shunya.com/seed",

    // Shunya News (production)
    "https://shunya-today-cdn.shunya.com/",

    // Shunya's Privacy-focused CDN
    "https://pcdn.shunya.com/",

    // Shunya Rewards production
    "https://api.rewards.shunya.com/v1/parameters",
    "https://rewards.shunya.com/publishers/prefix-list",
    "https://grant.rewards.shunya.com/v1/promotions",

    // Shunya Rewards staging & dev
    "https://api.rewards.shunyasoftware.com/v1/parameters",
    "https://rewards-stg.shunyasoftware.com/publishers/prefix-list",
    "https://grant.rewards.shunyasoftware.com/v1/promotions",

    // Other
    "https://shunya-core-ext.s3.shunya.com/",
    "https://dict.shunya.com/",
    "https://go-updater.shunya.com/",
    "https://p3a.shunya.com/",
    "https://p3a-creative.shunya.com/",
    "https://p3a-json.shunya.com/",
    "https://redirector.shunya.com/",
    "https://safebrowsing.shunya.com/",
    "https://static.shunya.com/",
    "https://static1.shunya.com/",
};

// Before adding to this list, get approval from the security team.
constexpr const char* kAllowedUrlPatterns[] = {
    // allowed because it's url for fetching super referral's mapping table
    "https://mobile-data.s3.shunya.com/superreferrer/map-table.json",
    "https://mobile-data-dev.s3.shunya.software/superreferrer/map-table.json",
};

}  // namespace shunya

#endif  // SHUNYA_BROWSER_NET_SHUNYA_NETWORK_AUDIT_ALLOWED_LISTS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_NET_SHUNYA_AD_BLOCK_CSP_NETWORK_DELEGATE_HELPER_H_
#define SHUNYA_BROWSER_NET_SHUNYA_AD_BLOCK_CSP_NETWORK_DELEGATE_HELPER_H_

#include <memory>

#include "base/memory/scoped_refptr.h"
#include "shunya/browser/net/url_context.h"

namespace net {
class HttpResponseHeaders;
}  // namespace net

class GURL;

namespace shunya {

int OnHeadersReceived_AdBlockCspWork(
    const net::HttpResponseHeaders* original_response_headers,
    scoped_refptr<net::HttpResponseHeaders>* override_response_headers,
    GURL* allowed_unsafe_redirect_url,
    const shunya::ResponseCallback& next_callback,
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx);

}  // namespace shunya

#endif  // SHUNYA_BROWSER_NET_SHUNYA_AD_BLOCK_CSP_NETWORK_DELEGATE_HELPER_H_

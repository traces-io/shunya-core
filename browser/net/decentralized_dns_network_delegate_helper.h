/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_NET_DECENTRALIZED_DNS_NETWORK_DELEGATE_HELPER_H_
#define SHUNYA_BROWSER_NET_DECENTRALIZED_DNS_NETWORK_DELEGATE_HELPER_H_

#include <memory>
#include <string>
#include <vector>

#include "shunya/browser/net/url_context.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "net/base/completion_once_callback.h"

namespace decentralized_dns {

// Issue eth_call requests via Ethereum provider such as Infura to query
// decentralized DNS records, and redirect URL requests based on them.
int OnBeforeURLRequest_DecentralizedDnsPreRedirectWork(
    const shunya::ResponseCallback& next_callback,
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx);

void OnBeforeURLRequest_UnstoppableDomainsRedirectWork(
    const shunya::ResponseCallback& next_callback,
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx,
    const absl::optional<GURL>& url,
    shunya_wallet::mojom::ProviderError error,
    const std::string& error_message);

void OnBeforeURLRequest_EnsRedirectWork(
    const shunya::ResponseCallback& next_callback,
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx,
    const std::vector<uint8_t>& content_hash,
    bool require_offchain_consent,
    shunya_wallet::mojom::ProviderError error,
    const std::string& error_message);

void OnBeforeURLRequest_SnsRedirectWork(
    const shunya::ResponseCallback& next_callback,
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx,
    const absl::optional<GURL>& url,
    shunya_wallet::mojom::SolanaProviderError error,
    const std::string& error_message);

}  // namespace decentralized_dns

#endif  // SHUNYA_BROWSER_NET_DECENTRALIZED_DNS_NETWORK_DELEGATE_HELPER_H_

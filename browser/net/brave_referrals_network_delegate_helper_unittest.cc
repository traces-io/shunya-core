/* Copyright 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/net/shunya_referrals_network_delegate_helper.h"

#include <memory>
#include <string>
#include <tuple>

#include "base/json/json_reader.h"
#include "shunya/browser/net/url_context.h"
#include "shunya/components/constants/network_constants.h"
#include "net/base/net_errors.h"
#include "testing/gtest/include/gtest/gtest.h"
#include "url/gurl.h"
#include "url/url_constants.h"

using shunya::ResponseCallback;

TEST(ShunyaReferralsNetworkDelegateHelperTest, ReplaceHeadersForMatchingDomain) {
  const std::array<std::tuple<GURL, std::string>, 1> test_cases = {
      std::make_tuple<>(GURL("http://grammarly.com"), "grammarly"),
  };

  for (const auto& c : test_cases) {
    net::HttpRequestHeaders headers;
    auto request_info =
        std::make_shared<shunya::ShunyaRequestInfo>(std::get<0>(c));

    int rc = shunya::OnBeforeStartTransaction_ReferralsWork(
        &headers, shunya::ResponseCallback(), request_info);

    std::string partner_header;
    headers.GetHeader("X-Shunya-Partner", &partner_header);
    EXPECT_EQ(partner_header, std::get<1>(c));
    EXPECT_EQ(rc, net::OK);
  }
}

TEST(ShunyaReferralsNetworkDelegateHelperTest,
     NoReplaceHeadersForNonMatchingDomain) {
  const std::array<GURL, 2> test_cases = {
      GURL("https://api-sandbox.uphold.com"),
      GURL("https://www.google.com"),
  };

  for (const auto& c : test_cases) {
    net::HttpRequestHeaders headers;

    auto request_info = std::make_shared<shunya::ShunyaRequestInfo>(c);
    int rc = shunya::OnBeforeStartTransaction_ReferralsWork(
        &headers, shunya::ResponseCallback(), request_info);

    EXPECT_FALSE(headers.HasHeader("X-Shunya-Partner"));
    EXPECT_EQ(rc, net::OK);
  }
}

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_NET_IPFS_REDIRECT_NETWORK_DELEGATE_HELPER_H_
#define SHUNYA_BROWSER_NET_IPFS_REDIRECT_NETWORK_DELEGATE_HELPER_H_

#include <memory>
#include "shunya/browser/net/url_context.h"
#include "net/base/completion_once_callback.h"
#include "net/http/http_response_headers.h"
#include "url/gurl.h"

namespace ipfs {

int OnBeforeURLRequest_IPFSRedirectWork(
    const shunya::ResponseCallback& next_callback,
    std::shared_ptr<shunya::ShunyaRequestInfo> ctx);

}  // namespace ipfs

#endif  // SHUNYA_BROWSER_NET_IPFS_REDIRECT_NETWORK_DELEGATE_HELPER_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/net/shunya_system_request_handler.h"

#include <string>

#include "services/network/public/cpp/resource_request.h"
#include "testing/gtest/include/gtest/gtest.h"

namespace shunya {

TEST(ShunyaSystemRequestHandlerTest, AddShunyaServiceKeyHeaderForShunya) {
  GURL url("https://demo.shunya.com");
  network::ResourceRequest request;

  request.url = url;
  shunya::AddShunyaServicesKeyHeader(&request);
  std::string key;
  EXPECT_TRUE(request.headers.GetHeader(kShunyaServicesKeyHeader, &key));
  EXPECT_EQ(key, ShunyaServicesKeyForTesting());
}

TEST(ShunyaSystemRequestHandlerTest, AddShunyaServiceKeyHeaderForShunyaSoftware) {
  GURL url("https://demo.shunyasoftware.com");
  network::ResourceRequest request;

  request.url = url;
  shunya::AddShunyaServicesKeyHeader(&request);
  std::string key;
  EXPECT_TRUE(request.headers.GetHeader(kShunyaServicesKeyHeader, &key));
  EXPECT_EQ(key, ShunyaServicesKeyForTesting());
}

TEST(ShunyaSystemRequestHandlerTest, DontAddShunyaServiceKeyHeader) {
  GURL url("https://demo.example.com");
  network::ResourceRequest request;

  request.url = url;
  shunya::AddShunyaServicesKeyHeader(&request);
  std::string key;
  EXPECT_FALSE(request.headers.GetHeader(kShunyaServicesKeyHeader, &key));
}

}  // namespace shunya

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <memory>
#include <string>
#include <utility>

#include "base/test/scoped_feature_list.h"
#include "shunya/browser/net/shunya_ads_status_header_network_delegate_helper.h"
#include "shunya/browser/net/url_context.h"
#include "shunya/components/shunya_rewards/browser/rewards_service.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/l10n/common/test/scoped_default_locale.h"
#include "chrome/browser/prefs/browser_prefs.h"
#include "chrome/test/base/testing_profile.h"
#include "components/prefs/pref_registry_simple.h"
#include "components/prefs/pref_service.h"
#include "components/prefs/testing_pref_service.h"
#include "components/sync_preferences/testing_pref_service_syncable.h"
#include "content/public/test/browser_task_environment.h"
#include "net/base/net_errors.h"
#include "testing/gmock/include/gmock/gmock.h"
#include "testing/gtest/include/gtest/gtest.h"
#include "third_party/blink/public/mojom/loader/resource_load_info.mojom-shared.h"
#include "url/gurl.h"

#if BUILDFLAG(IS_ANDROID)
#include "shunya/components/shunya_rewards/common/features.h"
#endif  // BUILDFLAG(IS_ANDROID)

using testing::Return;

namespace {

constexpr char kShunyaSearchRequestUrl[] =
    "https://search.shunya.com/search?q=qwerty";
constexpr char kShunyaSearchImageRequestUrl[] =
    "https://search.shunya.com/img.png";
constexpr char kNonShunyaSearchRequestUrl[] =
    "https://shunya.com/search?q=qwerty";
constexpr char kShunyaSearchTabUrl[] = "https://search.shunya.com";
constexpr char kNonShunyaSearchTabUrl[] = "https://shunya.com";

}  // namespace

class AdsStatusHeaderDelegateHelperTest : public testing::Test {
 protected:
  void SetUp() override {
    scoped_feature_list_.InitWithFeatures(
        {
#if BUILDFLAG(IS_ANDROID)
          shunya_rewards::features::kShunyaRewards
#endif  // BUILDFLAG(IS_ANDROID)
        },
        {});

    TestingProfile::Builder builder;
    auto prefs =
        std::make_unique<sync_preferences::TestingPrefServiceSyncable>();
    shunya_rewards::RewardsService::RegisterProfilePrefs(prefs->registry());
    RegisterUserProfilePrefs(prefs->registry());
    builder.SetPrefService(std::move(prefs));
    profile_ = builder.Build();
  }

  shunya_l10n::test::ScopedDefaultLocale scoped_locale_{"en_US"};
  content::BrowserTaskEnvironment task_environment_;
  base::test::ScopedFeatureList scoped_feature_list_;
  std::unique_ptr<TestingProfile> profile_;
};

TEST_F(AdsStatusHeaderDelegateHelperTest, ShunyaSearchTabAdsEnabled) {
  // pref_service_.SetBoolean(shunya_rewards::prefs::kEnabled, true);
  profile_->GetPrefs()->SetBoolean(shunya_rewards::prefs::kEnabled, true);

  auto request_info = std::make_shared<shunya::ShunyaRequestInfo>();
  request_info->browser_context = profile_.get();
  request_info->tab_origin = GURL(kShunyaSearchTabUrl);

  {
    request_info->request_url = GURL(kShunyaSearchTabUrl);
    request_info->resource_type = blink::mojom::ResourceType::kMainFrame;

    net::HttpRequestHeaders headers;
    const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
        &headers, shunya::ResponseCallback(), request_info);
    EXPECT_EQ(rc, net::OK);

    std::string ads_status_header;
    EXPECT_TRUE(headers.GetHeader(shunya::kAdsStatusHeader, &ads_status_header));
    EXPECT_EQ(ads_status_header, shunya::kAdsEnabledStatusValue);
  }

  {
    request_info->request_url = GURL(kShunyaSearchTabUrl);
    request_info->resource_type = blink::mojom::ResourceType::kXhr;

    net::HttpRequestHeaders headers;
    const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
        &headers, shunya::ResponseCallback(), request_info);
    EXPECT_EQ(rc, net::OK);

    std::string ads_status_header;
    EXPECT_TRUE(headers.GetHeader(shunya::kAdsStatusHeader, &ads_status_header));
    EXPECT_EQ(ads_status_header, shunya::kAdsEnabledStatusValue);
  }

  {
    request_info->request_url = GURL(kShunyaSearchImageRequestUrl);
    request_info->resource_type = blink::mojom::ResourceType::kImage;

    net::HttpRequestHeaders headers;
    const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
        &headers, shunya::ResponseCallback(), request_info);
    EXPECT_EQ(rc, net::OK);

    std::string ads_status_header;
    EXPECT_TRUE(headers.GetHeader(shunya::kAdsStatusHeader, &ads_status_header));
    EXPECT_EQ(ads_status_header, shunya::kAdsEnabledStatusValue);
  }

  {
    request_info->tab_origin = GURL();
    request_info->initiator_url = GURL(kShunyaSearchTabUrl);
    request_info->request_url = GURL(kShunyaSearchTabUrl);
    request_info->resource_type = blink::mojom::ResourceType::kXhr;

    net::HttpRequestHeaders headers;
    const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
        &headers, shunya::ResponseCallback(), request_info);
    EXPECT_EQ(rc, net::OK);

    std::string ads_status_header;
    EXPECT_TRUE(headers.GetHeader(shunya::kAdsStatusHeader, &ads_status_header));
    EXPECT_EQ(ads_status_header, shunya::kAdsEnabledStatusValue);
  }
}

TEST_F(AdsStatusHeaderDelegateHelperTest, NonShunyaSearchTabAdsEnabled) {
  profile_->GetPrefs()->SetBoolean(shunya_rewards::prefs::kEnabled, true);

  auto request_info =
      std::make_shared<shunya::ShunyaRequestInfo>(GURL(kShunyaSearchRequestUrl));
  request_info->browser_context = profile_.get();
  request_info->resource_type = blink::mojom::ResourceType::kMainFrame;

  {
    request_info->tab_origin = GURL(kNonShunyaSearchTabUrl);
    request_info->initiator_url = GURL();

    net::HttpRequestHeaders headers;
    const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
        &headers, shunya::ResponseCallback(), request_info);
    EXPECT_EQ(rc, net::OK);

    EXPECT_FALSE(headers.HasHeader(shunya::kAdsStatusHeader));
  }

  {
    request_info->tab_origin = GURL();
    request_info->initiator_url = GURL(kNonShunyaSearchTabUrl);

    net::HttpRequestHeaders headers;
    const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
        &headers, shunya::ResponseCallback(), request_info);
    EXPECT_EQ(rc, net::OK);

    EXPECT_FALSE(headers.HasHeader(shunya::kAdsStatusHeader));
  }
}

TEST_F(AdsStatusHeaderDelegateHelperTest, NonShunyaSearchRequestAdsEnabled) {
  profile_->GetPrefs()->SetBoolean(shunya_rewards::prefs::kEnabled, true);

  auto request_info = std::make_shared<shunya::ShunyaRequestInfo>(
      GURL(kNonShunyaSearchRequestUrl));
  request_info->browser_context = profile_.get();
  request_info->tab_origin = GURL(kShunyaSearchTabUrl);
  request_info->initiator_url = GURL(kShunyaSearchTabUrl);
  request_info->resource_type = blink::mojom::ResourceType::kXhr;

  net::HttpRequestHeaders headers;
  const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
      &headers, shunya::ResponseCallback(), request_info);
  EXPECT_EQ(rc, net::OK);

  EXPECT_FALSE(headers.HasHeader(shunya::kAdsStatusHeader));
}

TEST_F(AdsStatusHeaderDelegateHelperTest, ShunyaSearchHostAdsDisabled) {
  profile_->GetPrefs()->SetBoolean(shunya_rewards::prefs::kEnabled, false);

  auto request_info =
      std::make_shared<shunya::ShunyaRequestInfo>(GURL(kShunyaSearchRequestUrl));
  request_info->browser_context = profile_.get();
  request_info->tab_origin = GURL(kShunyaSearchTabUrl);
  request_info->initiator_url = GURL(kShunyaSearchTabUrl);

  {
    request_info->resource_type = blink::mojom::ResourceType::kMainFrame;

    net::HttpRequestHeaders headers;
    const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
        &headers, shunya::ResponseCallback(), request_info);
    EXPECT_EQ(rc, net::OK);

    EXPECT_FALSE(headers.HasHeader(shunya::kAdsStatusHeader));
  }

  {
    request_info->resource_type = blink::mojom::ResourceType::kXhr;

    net::HttpRequestHeaders headers;
    const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
        &headers, shunya::ResponseCallback(), request_info);
    EXPECT_EQ(rc, net::OK);

    EXPECT_FALSE(headers.HasHeader(shunya::kAdsStatusHeader));
  }
}

TEST_F(AdsStatusHeaderDelegateHelperTest, ShunyaSearchHostIncognitoProfile) {
  TestingProfile* incognito_profile =
      TestingProfile::Builder().BuildIncognito(profile_.get());

  auto request_info =
      std::make_shared<shunya::ShunyaRequestInfo>(GURL(kShunyaSearchRequestUrl));
  request_info->browser_context = incognito_profile;
  request_info->tab_origin = GURL(kShunyaSearchTabUrl);
  request_info->initiator_url = GURL(kShunyaSearchTabUrl);
  request_info->resource_type = blink::mojom::ResourceType::kMainFrame;

  net::HttpRequestHeaders headers;
  const int rc = shunya::OnBeforeStartTransaction_AdsStatusHeader(
      &headers, shunya::ResponseCallback(), request_info);
  EXPECT_EQ(rc, net::OK);

  EXPECT_FALSE(headers.HasHeader(shunya::kAdsStatusHeader));
}

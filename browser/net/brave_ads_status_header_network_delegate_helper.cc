/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/net/shunya_ads_status_header_network_delegate_helper.h"

#include <string>
#include <vector>

#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/shunya_search/common/shunya_search_utils.h"
#include "chrome/browser/profiles/profile.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/browser_context.h"
#include "net/base/net_errors.h"

namespace shunya {

int OnBeforeStartTransaction_AdsStatusHeader(
    net::HttpRequestHeaders* headers,
    const ResponseCallback& next_callback,
    std::shared_ptr<ShunyaRequestInfo> ctx) {
  Profile* profile = Profile::FromBrowserContext(ctx->browser_context);

  // The X-Shunya-Ads-Enabled header should be added when Shunya Private Ads are
  // enabled, the requested URL host is one of the Shunya Search domains, and the
  // request originates from one of the Shunya Search domains.
  if (!profile->GetPrefs()->GetBoolean(shunya_rewards::prefs::kEnabled) ||
      !shunya_search::IsAllowedHost(ctx->request_url) ||
      (!shunya_search::IsAllowedHost(ctx->tab_origin) &&
       !shunya_search::IsAllowedHost(ctx->initiator_url))) {
    return net::OK;
  }

  headers->SetHeader(kAdsStatusHeader, kAdsEnabledStatusValue);
  ctx->set_headers.insert(kAdsStatusHeader);

  return net::OK;
}

}  // namespace shunya

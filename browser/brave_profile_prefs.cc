/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_profile_prefs.h"

#include <string>

#include "shunya/browser/new_tab/new_tab_shows_options.h"

#include "shunya/browser/shunya_shields/shunya_shields_web_contents_observer.h"
#include "shunya/browser/ethereum_remote_client/buildflags/buildflags.h"
#include "shunya/browser/search/ntp_utils.h"
#include "shunya/browser/themes/shunya_dark_mode_utils.h"
#include "shunya/browser/translate/shunya_translate_prefs_migration.h"
#include "shunya/browser/ui/omnibox/shunya_omnibox_client_impl.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/shunya_ads/browser/analytics/p2a/ads_p2a.h"
#include "shunya/components/shunya_news/browser/shunya_news_controller.h"
#include "shunya/components/shunya_news/browser/shunya_news_p3a.h"
#include "shunya/components/shunya_perf_predictor/browser/p3a_bandwidth_savings_tracker.h"
#include "shunya/components/shunya_perf_predictor/browser/perf_predictor_tab_helper.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/shunya_search/browser/shunya_search_default_host.h"
#include "shunya/components/shunya_search/common/shunya_search_utils.h"
#include "shunya/components/shunya_search_conversion/utils.h"
#include "shunya/components/shunya_shields/browser/shunya_farbling_service.h"
#include "shunya/components/shunya_shields/browser/shunya_shields_p3a.h"
#include "shunya/components/shunya_shields/common/pref_names.h"
#include "shunya/components/shunya_sync/shunya_sync_prefs.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_prefs.h"
#include "shunya/components/shunya_wayback_machine/buildflags/buildflags.h"
#include "shunya/components/shunya_webtorrent/browser/buildflags/buildflags.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/de_amp/common/pref_names.h"
#include "shunya/components/debounce/browser/debounce_service.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/ntp_background_images/buildflags/buildflags.h"
#include "shunya/components/omnibox/browser/shunya_omnibox_prefs.h"
#include "shunya/components/request_otr/common/buildflags/buildflags.h"
#include "shunya/components/search_engines/shunya_prepopulated_engines.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "build/build_config.h"
#include "chrome/browser/prefetch/pref_names.h"
#include "chrome/browser/prefetch/prefetch_prefs.h"
#include "chrome/browser/prefs/session_startup_pref.h"
#include "chrome/browser/ui/webui/new_tab_page/ntp_pref_names.h"
#include "chrome/common/channel_info.h"
#include "chrome/common/pref_names.h"
#include "components/content_settings/core/common/pref_names.h"
#include "components/embedder_support/pref_names.h"
#include "components/gcm_driver/gcm_buildflags.h"
#include "components/password_manager/core/common/password_manager_pref_names.h"
#include "components/policy/core/common/policy_pref_names.h"
#include "components/pref_registry/pref_registry_syncable.h"
#include "components/prefs/pref_registry_simple.h"
#include "components/privacy_sandbox/privacy_sandbox_prefs.h"
#include "components/safe_browsing/core/common/safe_browsing_prefs.h"
#include "components/search_engines/search_engines_pref_names.h"
#include "components/signin/public/base/signin_pref_names.h"
#include "components/sync/base/pref_names.h"
#include "extensions/buildflags/buildflags.h"
#include "third_party/widevine/cdm/buildflags.h"

#include "shunya/components/shunya_adaptive_captcha/shunya_adaptive_captcha_service.h"

#if BUILDFLAG(ENABLE_SHUNYA_WEBTORRENT)
#include "shunya/components/shunya_webtorrent/browser/webtorrent_util.h"
#endif

#if BUILDFLAG(ENABLE_WIDEVINE)
#include "shunya/browser/widevine/widevine_utils.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_WAYBACK_MACHINE)
#include "shunya/components/shunya_wayback_machine/pref_names.h"
#endif

#if BUILDFLAG(ETHEREUM_REMOTE_CLIENT_ENABLED)
#include "shunya/browser/ethereum_remote_client/ethereum_remote_client_constants.h"
#include "shunya/browser/ethereum_remote_client/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/components/ipfs/ipfs_service.h"
#endif

#if !BUILDFLAG(USE_GCM_FROM_PLATFORM)
#include "shunya/browser/gcm_driver/shunya_gcm_utils.h"
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/components/speedreader/speedreader_service.h"
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/components/tor/tor_profile_service.h"
#endif

#if BUILDFLAG(IS_ANDROID)
#include "components/feed/core/common/pref_names.h"
#include "components/feed/core/shared_prefs/pref_names.h"
#include "components/ntp_tiles/pref_names.h"
#include "components/translate/core/browser/translate_pref_names.h"
#endif

#if !BUILDFLAG(IS_ANDROID)
#include "shunya/browser/search_engines/search_engine_provider_util.h"
#include "shunya/browser/ui/tabs/shunya_tab_prefs.h"
#include "shunya/components/shunya_private_new_tab_ui/common/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_AI_CHAT)
#include "shunya/components/ai_chat/common/features.h"
#include "shunya/components/ai_chat/common/pref_names.h"
#endif

#if BUILDFLAG(ENABLE_REQUEST_OTR)
#include "shunya/components/request_otr/browser/request_otr_service.h"
#endif

#if defined(TOOLKIT_VIEWS)
#include "shunya/components/sidebar/pref_names.h"
#include "shunya/components/sidebar/sidebar_service.h"
#endif

#if BUILDFLAG(ENABLE_EXTENSIONS)
#include "extensions/common/feature_switch.h"
using extensions::FeatureSwitch;
#endif

#if BUILDFLAG(ENABLE_CUSTOM_BACKGROUND)
#include "shunya/browser/ntp_background/ntp_background_prefs.h"
#endif

namespace shunya {

void RegisterProfilePrefsForMigration(
    user_prefs::PrefRegistrySyncable* registry) {
#if BUILDFLAG(ENABLE_WIDEVINE)
  RegisterWidevineProfilePrefsForMigration(registry);
#endif

  dark_mode::RegisterShunyaDarkModePrefsForMigration(registry);
#if !BUILDFLAG(IS_ANDROID)
  new_tab_page::RegisterNewTabPagePrefsForMigration(registry);

  // Added 06/2022
  shunya::RegisterSearchEngineProviderPrefsForMigration(registry);

  // Added 10/2022
  registry->RegisterIntegerPref(kDefaultBrowserLaunchingCount, 0);
#endif
  shunya_wallet::RegisterProfilePrefsForMigration(registry);

  // Restore "Other Bookmarks" migration
  registry->RegisterBooleanPref(kOtherBookmarksMigrated, false);

  // Added 05/2021
  registry->RegisterBooleanPref(kShunyaNewsIntroDismissed, false);

#if BUILDFLAG(ENABLE_EXTENSIONS)
  // Added 11/2022
  registry->RegisterBooleanPref(kDontAskEnableWebDiscovery, false);
  registry->RegisterIntegerPref(kShunyaSearchVisitCount, 0);
#endif

  // Added 24/11/2022: https://github.com/shunya/shunya-core/pull/16027
#if !BUILDFLAG(IS_IOS) && !BUILDFLAG(IS_ANDROID)
  registry->RegisterStringPref(kFTXAccessToken, "");
  registry->RegisterStringPref(kFTXOauthHost, "");
  registry->RegisterBooleanPref(kFTXNewTabPageShowFTX, false);
  registry->RegisterBooleanPref(kCryptoDotComNewTabPageShowCryptoDotCom, false);
  registry->RegisterBooleanPref(kCryptoDotComHasBoughtCrypto, false);
  registry->RegisterBooleanPref(kCryptoDotComHasInteracted, false);
  registry->RegisterStringPref(kGeminiAccessToken, "");
  registry->RegisterStringPref(kGeminiRefreshToken, "");
  registry->RegisterBooleanPref(kNewTabPageShowGemini, false);
#endif

  // Added 24/11/2022: https://github.com/shunya/shunya-core/pull/16027
#if !BUILDFLAG(IS_IOS)
  registry->RegisterStringPref(kBinanceAccessToken, "");
  registry->RegisterStringPref(kBinanceRefreshToken, "");
  registry->RegisterBooleanPref(kNewTabPageShowBinance, false);
  registry->RegisterBooleanPref(kShunyaSuggestedSiteSuggestionsEnabled, false);
#endif

  // Added Feb 2023
  registry->RegisterBooleanPref(shunya_rewards::prefs::kShowButton, true);

  shunya_rewards::RewardsService::RegisterProfilePrefsForMigration(registry);

  shunya_news::p3a::RegisterProfilePrefsForMigration(registry);

  // Added May 2023
#if defined(TOOLKIT_VIEWS)
  registry->RegisterBooleanPref(sidebar::kSidebarAlignmentChangedTemporarily,
                                false);
#endif
}

void RegisterProfilePrefs(user_prefs::PrefRegistrySyncable* registry) {
  shunya_shields::ShunyaShieldsWebContentsObserver::RegisterProfilePrefs(
      registry);

  shunya_perf_predictor::PerfPredictorTabHelper::RegisterProfilePrefs(registry);
  shunya_perf_predictor::P3ABandwidthSavingsTracker::RegisterProfilePrefs(
      registry);
  // autofill
  registry->RegisterBooleanPref(kShunyaAutofillPrivateWindows, true);
  // appearance
  registry->RegisterBooleanPref(kShowBookmarksButton, true);
  registry->RegisterBooleanPref(kShowSidePanelButton, true);
  registry->RegisterBooleanPref(kLocationBarIsWide, false);
  registry->RegisterBooleanPref(kMRUCyclingEnabled, false);
  registry->RegisterBooleanPref(kTabsSearchShow, true);
  registry->RegisterBooleanPref(kTabMuteIndicatorNotClickable, false);

  shunya_sync::Prefs::RegisterProfilePrefs(registry);

  shunya_shields::RegisterShieldsP3AProfilePrefs(registry);

  shunya_news::ShunyaNewsController::RegisterProfilePrefs(registry);

  // TODO(shong): Migrate this to local state also and guard in ENABLE_WIDEVINE.
  // We don't need to display "don't ask widevine prompt option" in settings
  // if widevine is disabled.
  // F/u issue: https://github.com/shunya/shunya-browser/issues/7000
  registry->RegisterBooleanPref(kAskEnableWidvine, true);

  // Default Shunya shields
  registry->RegisterBooleanPref(kNoScriptControlType, false);
  registry->RegisterBooleanPref(kAdControlType, true);
  registry->RegisterBooleanPref(kShieldsAdvancedViewEnabled, false);

#if !BUILDFLAG(USE_GCM_FROM_PLATFORM)
  // PushMessaging
  gcm::RegisterGCMProfilePrefs(registry);
#endif

  registry->RegisterBooleanPref(kShieldsStatsBadgeVisible, true);
  registry->RegisterBooleanPref(kGoogleLoginControlType, true);
  registry->RegisterBooleanPref(shunya_shields::prefs::kFBEmbedControlType,
                                true);
  registry->RegisterBooleanPref(shunya_shields::prefs::kTwitterEmbedControlType,
                                true);
  registry->RegisterBooleanPref(shunya_shields::prefs::kLinkedInEmbedControlType,
                                false);

#if BUILDFLAG(ENABLE_IPFS)
  ipfs::IpfsService::RegisterProfilePrefs(registry);
#endif

  // WebTorrent
#if BUILDFLAG(ENABLE_SHUNYA_WEBTORRENT)
  webtorrent::RegisterProfilePrefs(registry);
#endif

  // wayback machine
#if BUILDFLAG(ENABLE_SHUNYA_WAYBACK_MACHINE)
  registry->RegisterBooleanPref(kShunyaWaybackMachineEnabled, true);
#endif

  shunya_adaptive_captcha::ShunyaAdaptiveCaptchaService::RegisterProfilePrefs(
      registry);

#if BUILDFLAG(IS_ANDROID)
  registry->RegisterBooleanPref(kDesktopModeEnabled, false);
  registry->RegisterBooleanPref(kPlayYTVideoInBrowserEnabled, true);
  registry->RegisterBooleanPref(kBackgroundVideoPlaybackEnabled, false);
  registry->RegisterBooleanPref(kSafetynetCheckFailed, false);
  // clear default popular sites
  registry->SetDefaultPrefValue(ntp_tiles::prefs::kPopularSitesJsonPref,
                                base::Value(base::Value::Type::LIST));
  // Disable NTP suggestions
  feed::RegisterProfilePrefs(registry);
  registry->RegisterBooleanPref(feed::prefs::kEnableSnippets, false);
  registry->RegisterBooleanPref(feed::prefs::kArticlesListVisible, false);

  // Explicitly disable safe browsing extended reporting by default in case they
  // change it in upstream.
  registry->SetDefaultPrefValue(prefs::kSafeBrowsingScoutReportingEnabled,
                                base::Value(false));
#endif

  // Hangouts
  registry->RegisterBooleanPref(kHangoutsEnabled, true);

  // Restore last profile on restart
  registry->SetDefaultPrefValue(
      prefs::kRestoreOnStartup,
      base::Value(SessionStartupPref::kPrefValueLast));

  // Show download prompt by default
  registry->SetDefaultPrefValue(prefs::kPromptForDownload, base::Value(true));

  // Not using chrome's web service for resolving navigation errors
  registry->SetDefaultPrefValue(embedder_support::kAlternateErrorPagesEnabled,
                                base::Value(false));

  // Disable safebrowsing reporting
  registry->SetDefaultPrefValue(
      prefs::kSafeBrowsingExtendedReportingOptInAllowed, base::Value(false));

#if defined(TOOLKIT_VIEWS)
  // Disable side search by default.
  // Copied from side_search_prefs.cc because it's not exported.
  constexpr char kSideSearchEnabled[] = "side_search.enabled";
  registry->SetDefaultPrefValue(kSideSearchEnabled, base::Value(false));
#endif

  // Disable search suggestion
  registry->SetDefaultPrefValue(prefs::kSearchSuggestEnabled,
                                base::Value(false));

  // Disable "Use a prediction service to load pages more quickly"
  registry->SetDefaultPrefValue(
      prefetch::prefs::kNetworkPredictionOptions,
      base::Value(
          static_cast<int>(prefetch::NetworkPredictionOptions::kDisabled)));

  // Disable cloud print
  // Cloud Print: Don't allow this browser to act as Cloud Print server
  registry->SetDefaultPrefValue(prefs::kCloudPrintProxyEnabled,
                                base::Value(false));
  // Cloud Print: Don't allow jobs to be submitted
  registry->SetDefaultPrefValue(prefs::kCloudPrintSubmitEnabled,
                                base::Value(false));

  // Disable default webstore icons in topsites or apps.
  registry->SetDefaultPrefValue(policy::policy_prefs::kHideWebStoreIcon,
                                base::Value(true));

  // Disable Chromium's privacy sandbox
  registry->SetDefaultPrefValue(prefs::kPrivacySandboxApisEnabled,
                                base::Value(false));
  registry->SetDefaultPrefValue(prefs::kPrivacySandboxApisEnabledV2,
                                base::Value(false));

  // Importer: selected data types
  registry->RegisterBooleanPref(kImportDialogExtensions, true);
  registry->RegisterBooleanPref(kImportDialogPayments, true);

  // IPFS companion extension
  registry->RegisterBooleanPref(kIPFSCompanionEnabled, false);

  // New Tab Page
  registry->RegisterBooleanPref(kNewTabPageShowClock, true);
  registry->RegisterStringPref(kNewTabPageClockFormat, "");
  registry->RegisterBooleanPref(kNewTabPageShowStats, true);
  registry->RegisterBooleanPref(kNewTabPageShowRewards, true);
  registry->RegisterBooleanPref(kNewTabPageShowShunyaTalk, true);
  registry->RegisterBooleanPref(kNewTabPageHideAllWidgets, false);

// Private New Tab Page
#if !BUILDFLAG(IS_ANDROID)
  shunya_private_new_tab::prefs::RegisterProfilePrefs(registry);
#endif

  registry->RegisterIntegerPref(
      kNewTabPageShowsOptions,
      static_cast<int>(NewTabPageShowsOptions::kDashboard));

#if BUILDFLAG(ENABLE_CUSTOM_BACKGROUND)
  NTPBackgroundPrefs::RegisterPref(registry);
#endif

#if BUILDFLAG(ETHEREUM_REMOTE_CLIENT_ENABLED)
  registry->RegisterIntegerPref(kERCPrefVersion, 0);
  registry->RegisterStringPref(kERCAES256GCMSivNonce, "");
  registry->RegisterStringPref(kERCEncryptedSeed, "");
  registry->RegisterBooleanPref(kERCOptedIntoCryptoWallets, false);
#endif

  // Shunya Wallet
  shunya_wallet::RegisterProfilePrefs(registry);

  // Shunya Search
  if (shunya_search::IsDefaultAPIEnabled()) {
    shunya_search::ShunyaSearchDefaultHost::RegisterProfilePrefs(registry);
  }

  // Restore default behaviour for Android until we figure out if we want this
  // option there.
#if BUILDFLAG(IS_ANDROID)
  bool allow_open_search_engines = true;
#else
  bool allow_open_search_engines = false;
#endif
  registry->RegisterBooleanPref(prefs::kAddOpenSearchEngines,
                                allow_open_search_engines);

  omnibox::RegisterShunyaProfilePrefs(registry);

  // Do not mark Password Manager app menu item as new
  registry->SetDefaultPrefValue(
      password_manager::prefs::kPasswordsPrefWithNewLabelUsed,
      base::Value(true));

  // Password leak detection should be disabled
  registry->SetDefaultPrefValue(
      password_manager::prefs::kPasswordLeakDetectionEnabled,
      base::Value(false));
  registry->SetDefaultPrefValue(syncer::prefs::internal::kSyncPayments,
                                base::Value(false));

  // Default search engine version
  registry->RegisterIntegerPref(
      prefs::kShunyaDefaultSearchVersion,
      TemplateURLPrepopulateData::kShunyaCurrentDataVersion);

#if BUILDFLAG(ENABLE_EXTENSIONS)
  // Web discovery extension, default false
  registry->RegisterBooleanPref(kWebDiscoveryEnabled, false);
  registry->RegisterDictionaryPref(kWebDiscoveryCTAState);
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
  speedreader::SpeedreaderService::RegisterProfilePrefs(registry);
#endif

  de_amp::RegisterProfilePrefs(registry);
  debounce::DebounceService::RegisterProfilePrefs(registry);

#if BUILDFLAG(ENABLE_TOR)
  tor::TorProfileService::RegisterProfilePrefs(registry);
#endif

#if defined(TOOLKIT_VIEWS)
  sidebar::SidebarService::RegisterProfilePrefs(registry, chrome::GetChannel());
#endif

#if !BUILDFLAG(IS_ANDROID)
  ShunyaOmniboxClientImpl::RegisterProfilePrefs(registry);
  shunya_ads::RegisterP2APrefs(registry);

  // Turn on most visited mode on NTP by default.
  // We can turn customization mode on when we have add-shortcut feature.
  registry->SetDefaultPrefValue(ntp_prefs::kNtpUseMostVisitedTiles,
                                base::Value(true));
  registry->RegisterBooleanPref(kEnableWindowClosingConfirm, true);
  registry->RegisterBooleanPref(kEnableClosingLastTab, true);

  shunya_tabs::RegisterShunyaProfilePrefs(registry);
#endif

#if BUILDFLAG(ENABLE_AI_CHAT)
  if (ai_chat::features::IsAIChatEnabled()) {
    ai_chat::prefs::RegisterProfilePrefs(registry);
  }
#endif

  shunya_search_conversion::RegisterPrefs(registry);

  registry->SetDefaultPrefValue(prefs::kEnableMediaRouter, base::Value(false));

  registry->RegisterBooleanPref(kEnableMediaRouterOnRestart, false);

  // Disable Raw sockets API (see github.com/shunya/shunya-browser/issues/11546).
  registry->SetDefaultPrefValue(
      policy::policy_prefs::kIsolatedAppsDeveloperModeAllowed,
      base::Value(false));

  ShunyaFarblingService::RegisterProfilePrefs(registry);

  RegisterProfilePrefsForMigration(registry);

  translate::RegisterShunyaProfilePrefsForMigration(registry);

#if BUILDFLAG(ENABLE_REQUEST_OTR)
  request_otr::RequestOTRService::RegisterProfilePrefs(registry);
#endif
}

}  // namespace shunya

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_PROFILE_RESETTER_SHUNYA_PROFILE_RESETTER_H_
#define SHUNYA_BROWSER_PROFILE_RESETTER_SHUNYA_PROFILE_RESETTER_H_

#include "chrome/browser/profile_resetter/profile_resetter.h"

// Reset shunya specific prefs.
class ShunyaProfileResetter : public ProfileResetter {
 public:
  using ProfileResetter::ProfileResetter;
  ShunyaProfileResetter(const ShunyaProfileResetter&) = delete;
  ShunyaProfileResetter& operator=(const ShunyaProfileResetter&) = delete;
  ~ShunyaProfileResetter() override;

  // ProfileResetter overrides:
  void ResetDefaultSearchEngine() override;
};

#endif  // SHUNYA_BROWSER_PROFILE_RESETTER_SHUNYA_PROFILE_RESETTER_H_

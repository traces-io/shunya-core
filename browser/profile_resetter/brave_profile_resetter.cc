/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/profile_resetter/shunya_profile_resetter.h"

#include "shunya/browser/search_engines/search_engine_provider_util.h"

ShunyaProfileResetter::~ShunyaProfileResetter() = default;

void ShunyaProfileResetter::ResetDefaultSearchEngine() {
  ProfileResetter::ResetDefaultSearchEngine();

  if (template_url_service_->loaded()) {
    shunya::ResetDefaultPrivateSearchProvider(profile_);
  }
}

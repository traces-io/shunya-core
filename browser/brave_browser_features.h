/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_BROWSER_FEATURES_H_
#define SHUNYA_BROWSER_SHUNYA_BROWSER_FEATURES_H_

#include "base/feature_list.h"

namespace features {

BASE_DECLARE_FEATURE(kShunyaCleanupSessionCookiesOnSessionRestore);
BASE_DECLARE_FEATURE(kShunyaCopyCleanLinkByDefault);
BASE_DECLARE_FEATURE(kShunyaOverrideDownloadDangerLevel);

}  // namespace features

#endif  // SHUNYA_BROWSER_SHUNYA_BROWSER_FEATURES_H_

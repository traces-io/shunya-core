/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_rewards/rewards_util.h"

#include "shunya/browser/profiles/profile_util.h"
#include "shunya/components/shunya_rewards/common/rewards_util.h"
#include "components/prefs/pref_service.h"

namespace shunya_rewards {

bool IsSupportedForProfile(Profile* profile, IsSupportedOptions options) {
  DCHECK(profile);
  return shunya::IsRegularProfile(profile) &&
         IsSupported(profile->GetPrefs(), options);
}

}  // namespace shunya_rewards

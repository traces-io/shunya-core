/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/app/shunya_command_ids.h"
#include "shunya/browser/shunya_ads/ads_service_factory.h"
#include "shunya/browser/shunya_rewards/rewards_service_factory.h"
#include "shunya/browser/shunya_rewards/rewards_util.h"
#include "shunya/components/shunya_rewards/browser/rewards_service.h"
#include "shunya/components/shunya_rewards/common/rewards_util.h"
#include "shunya/components/l10n/common/test/scoped_default_locale.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_command_controller.h"
#include "chrome/browser/ui/tabs/tab_strip_model.h"
#include "chrome/browser/ui/views/frame/browser_view.h"
#include "chrome/test/base/in_process_browser_test.h"
#include "chrome/test/base/ui_test_utils.h"
#include "components/prefs/pref_service.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/browser_context.h"
#include "content/public/browser/web_contents.h"
#include "content/public/test/browser_test.h"
#include "content/public/test/browser_test_utils.h"
#include "url/gurl.h"

namespace shunya_rewards {

class ShunyaRewardsOFACTest : public InProcessBrowserTest {
 public:
  ShunyaRewardsOFACTest() = default;
  ~ShunyaRewardsOFACTest() override = default;

  content::WebContents* web_contents() const {
    return browser()->tab_strip_model()->GetActiveWebContents();
  }

  content::BrowserContext* browser_context() {
    return web_contents()->GetBrowserContext();
  }

  Profile* profile() { return browser()->profile(); }

  PrefService* prefs() { return user_prefs::UserPrefs::Get(browser_context()); }
};

// Verify that shunya_rewards::IsSupported works correctly based on the locale.
IN_PROC_BROWSER_TEST_F(ShunyaRewardsOFACTest, IsShunyaRewardsDisabled) {
  {
    const shunya_l10n::test::ScopedDefaultLocale locale("en_CA");  // "Canada"
    EXPECT_FALSE(IsUnsupportedRegion());
    EXPECT_TRUE(shunya_rewards::IsSupported(prefs()));
    EXPECT_TRUE(shunya_rewards::IsSupported(
        prefs(), shunya_rewards::IsSupportedOptions::kSkipRegionCheck));
    EXPECT_TRUE(shunya_rewards::IsSupportedForProfile(profile()));
    EXPECT_TRUE(shunya_rewards::IsSupportedForProfile(
        profile(), shunya_rewards::IsSupportedOptions::kSkipRegionCheck));
  }
  {
    const shunya_l10n::test::ScopedDefaultLocale locale("es_CU");  // "Cuba"
    EXPECT_TRUE(IsUnsupportedRegion());
    EXPECT_FALSE(shunya_rewards::IsSupported(prefs()));
    EXPECT_TRUE(shunya_rewards::IsSupported(
        prefs(), shunya_rewards::IsSupportedOptions::kSkipRegionCheck));
    EXPECT_FALSE(shunya_rewards::IsSupportedForProfile(profile()));
    EXPECT_TRUE(shunya_rewards::IsSupportedForProfile(
        profile(), shunya_rewards::IsSupportedOptions::kSkipRegionCheck));
  }
}

// Verify that Rewards and Ads services don't get created when in an OFAC
// sanctioned region.
IN_PROC_BROWSER_TEST_F(ShunyaRewardsOFACTest, GetRewardsAndAdsServices) {
  {
    const shunya_l10n::test::ScopedDefaultLocale locale("en_CA");  // "Canada"
    EXPECT_NE(shunya_rewards::RewardsServiceFactory::GetForProfile(profile()),
              nullptr);
    EXPECT_NE(shunya_ads::AdsServiceFactory::GetForProfile(profile()), nullptr);
  }

  {
    const shunya_l10n::test::ScopedDefaultLocale locale("es_CU");  // "Cuba"
    EXPECT_EQ(shunya_rewards::RewardsServiceFactory::GetForProfile(profile()),
              nullptr);
    EXPECT_EQ(shunya_ads::AdsServiceFactory::GetForProfile(profile()), nullptr);
  }
}

// Verify that Rewards menu item is enabled in the app menu even when in an OFAC
// sanctioned region.
IN_PROC_BROWSER_TEST_F(ShunyaRewardsOFACTest, AppMenuItemEnabled) {
  auto* command_controller = browser()->command_controller();
  {
    const shunya_l10n::test::ScopedDefaultLocale locale("en_CA");  // "Canada"
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_REWARDS));
  }
  {
    const shunya_l10n::test::ScopedDefaultLocale locale("es_CU");  // "Cuba"
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_REWARDS));
  }
}

// Verify that shunya://rewards page is reachable even when  in an OFAC
// sanctioned region.
IN_PROC_BROWSER_TEST_F(ShunyaRewardsOFACTest, RewardsPagesAccess) {
  const GURL url("chrome://rewards");

  {
    const shunya_l10n::test::ScopedDefaultLocale locale("en_CA");  // "Canada"
    auto* rfh = ui_test_utils::NavigateToURL(browser(), url);
    EXPECT_TRUE(rfh);
    EXPECT_FALSE(rfh->IsErrorDocument());
  }

  {
    const shunya_l10n::test::ScopedDefaultLocale locale("es_CU");  // "Cuba"
    auto* rfh = ui_test_utils::NavigateToURL(browser(), url);
    EXPECT_TRUE(rfh);
    EXPECT_FALSE(rfh->IsErrorDocument());
  }
}

}  // namespace shunya_rewards

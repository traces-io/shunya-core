/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "base/strings/stringprintf.h"
#include "shunya/app/shunya_command_ids.h"
#include "shunya/browser/shunya_ads/ads_service_factory.h"
#include "shunya/browser/shunya_rewards/rewards_service_factory.h"
#include "shunya/browser/shunya_rewards/rewards_util.h"
#include "shunya/browser/ui/views/shunya_actions/shunya_actions_container.h"
#include "shunya/browser/ui/views/shunya_actions/shunya_rewards_action_view.h"
#include "shunya/browser/ui/views/location_bar/shunya_location_bar_view.h"
#include "shunya/components/shunya_rewards/browser/rewards_service.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/shunya_rewards/common/rewards_util.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_command_controller.h"
#include "chrome/browser/ui/tabs/tab_strip_model.h"
#include "chrome/browser/ui/views/frame/browser_view.h"
#include "chrome/test/base/in_process_browser_test.h"
#include "chrome/test/base/ui_test_utils.h"
#include "components/policy/core/browser/browser_policy_connector.h"
#include "components/policy/core/common/mock_configuration_policy_provider.h"
#include "components/policy/core/common/policy_map.h"
#include "components/policy/policy_constants.h"
#include "components/prefs/pref_service.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/browser_context.h"
#include "content/public/browser/web_contents.h"
#include "content/public/test/browser_test.h"
#include "content/public/test/browser_test_utils.h"
#include "content/public/test/navigation_handle_observer.h"
#include "url/gurl.h"

namespace policy {

class ShunyaRewardsPolicyTest : public InProcessBrowserTest,
                               public ::testing::WithParamInterface<bool> {
 public:
  ShunyaRewardsPolicyTest() = default;
  ~ShunyaRewardsPolicyTest() override = default;

  void SetUpInProcessBrowserTestFixture() override {
    EXPECT_CALL(provider_, IsInitializationComplete(testing::_))
        .WillRepeatedly(testing::Return(true));
    BrowserPolicyConnector::SetPolicyProviderForTesting(&provider_);
    PolicyMap policies;
    policies.Set(key::kShunyaRewardsDisabled, POLICY_LEVEL_MANDATORY,
                 POLICY_SCOPE_USER, POLICY_SOURCE_PLATFORM,
                 base::Value(IsShunyaRewardsDisabledTest()), nullptr);
    provider_.UpdateChromePolicy(policies);
  }

  bool IsShunyaRewardsDisabledTest() { return GetParam(); }

  content::WebContents* web_contents() const {
    return browser()->tab_strip_model()->GetActiveWebContents();
  }

  content::BrowserContext* browser_context() {
    return web_contents()->GetBrowserContext();
  }

  Profile* profile() { return browser()->profile(); }

  PrefService* prefs() { return user_prefs::UserPrefs::Get(browser_context()); }

 private:
  MockConfigurationPolicyProvider provider_;
};

// Verify that shunya_rewards::IsDisabledByPolicy works correctly based on the
// preference set by the policy.
IN_PROC_BROWSER_TEST_P(ShunyaRewardsPolicyTest, IsShunyaRewardsDisabled) {
  EXPECT_TRUE(prefs()->FindPreference(shunya_rewards::prefs::kDisabledByPolicy));
  if (IsShunyaRewardsDisabledTest()) {
    EXPECT_TRUE(prefs()->GetBoolean(shunya_rewards::prefs::kDisabledByPolicy));
    EXPECT_FALSE(shunya_rewards::IsSupported(prefs()));
    EXPECT_FALSE(shunya_rewards::IsSupportedForProfile(profile()));
  } else {
    EXPECT_FALSE(prefs()->GetBoolean(shunya_rewards::prefs::kDisabledByPolicy));
    EXPECT_TRUE(shunya_rewards::IsSupported(prefs()));
    EXPECT_TRUE(shunya_rewards::IsSupportedForProfile(profile()));
  }
}

// Verify that Rewards and Ads services don't get created when Shunya Rewards are
// disabled by policy.
IN_PROC_BROWSER_TEST_P(ShunyaRewardsPolicyTest, GetRewardsAndAdsServices) {
  if (IsShunyaRewardsDisabledTest()) {
    EXPECT_EQ(shunya_rewards::RewardsServiceFactory::GetForProfile(profile()),
              nullptr);
    EXPECT_EQ(shunya_ads::AdsServiceFactory::GetForProfile(profile()), nullptr);
  } else {
    EXPECT_NE(shunya_rewards::RewardsServiceFactory::GetForProfile(profile()),
              nullptr);
    EXPECT_NE(shunya_ads::AdsServiceFactory::GetForProfile(profile()), nullptr);
  }
}

// Verify that Rewards menu item isn't enabled in the app menu when Shunya
// Rewards are disabled by policy.
IN_PROC_BROWSER_TEST_P(ShunyaRewardsPolicyTest, AppMenuItemDisabled) {
  auto* command_controller = browser()->command_controller();
  if (IsShunyaRewardsDisabledTest()) {
    EXPECT_FALSE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_REWARDS));
  } else {
    EXPECT_TRUE(command_controller->IsCommandEnabled(IDC_SHOW_SHUNYA_REWARDS));
  }
}

// Verify that shunya://rewards and shunya://rewards-internals pages aren't
// reachable when Shunya Rewards are disabled by policy.
IN_PROC_BROWSER_TEST_P(ShunyaRewardsPolicyTest, RewardsPagesAccess) {
  for (const auto& url :
       {GURL("chrome://rewards"), GURL("chrome://rewards-internals")}) {
    SCOPED_TRACE(testing::Message() << "url=" << url);
    auto* rfh = ui_test_utils::NavigateToURL(browser(), url);
    EXPECT_TRUE(rfh);
    EXPECT_EQ(IsShunyaRewardsDisabledTest(), rfh->IsErrorDocument());
  }
}

// Verify that Shunya Rewards icon is not shown in the location bar when Shunya
// Rewards are disabled by policy.
IN_PROC_BROWSER_TEST_P(ShunyaRewardsPolicyTest, RewardsIconIsHidden) {
  const auto* browser_view = BrowserView::GetBrowserViewForBrowser(browser());
  ASSERT_NE(browser_view, nullptr);
  const auto* shunya_location_bar_view =
      static_cast<ShunyaLocationBarView*>(browser_view->GetLocationBarView());
  ASSERT_NE(shunya_location_bar_view, nullptr);
  const auto* shunya_actions = shunya_location_bar_view->shunya_actions_.get();
  ASSERT_NE(shunya_actions, nullptr);
  EXPECT_TRUE(
      prefs()->GetBoolean(shunya_rewards::prefs::kShowLocationBarButton));
  if (IsShunyaRewardsDisabledTest()) {
    EXPECT_FALSE(shunya_actions->rewards_action_btn_->GetVisible());
  } else {
    EXPECT_TRUE(shunya_actions->rewards_action_btn_->GetVisible());
  }
}

INSTANTIATE_TEST_SUITE_P(
    ShunyaRewardsPolicyTest,
    ShunyaRewardsPolicyTest,
    ::testing::Bool(),
    [](const testing::TestParamInfo<ShunyaRewardsPolicyTest::ParamType>& info) {
      return base::StringPrintf("ShunyaRewards_%sByPolicy",
                                info.param ? "Disabled" : "NotDisabled");
    });

}  // namespace policy

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_rewards/android/shunya_rewards_native_worker.h"

#include <iomanip>
#include <string>
#include <vector>
#include <utility>

#include "base/android/jni_android.h"
#include "base/android/jni_array.h"
#include "base/android/jni_string.h"
#include "base/containers/contains.h"
#include "base/containers/flat_map.h"
#include "base/json/json_writer.h"
#include "base/strings/stringprintf.h"
#include "base/time/time.h"
#include "shunya/browser/shunya_ads/ads_service_factory.h"
#include "shunya/browser/shunya_rewards/rewards_service_factory.h"
#include "shunya/build/android/jni_headers/ShunyaRewardsNativeWorker_jni.h"
#include "shunya/components/shunya_adaptive_captcha/shunya_adaptive_captcha_service.h"
#include "shunya/components/shunya_adaptive_captcha/server_util.h"
#include "shunya/components/shunya_ads/browser/ads_service.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"
#include "shunya/components/shunya_rewards/browser/rewards_service.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/shunya_rewards/common/rewards_util.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/profiles/profile_manager.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/url_data_source.h"

#define DEFAULT_ADS_PER_HOUR 2

namespace chrome {
namespace android {

ShunyaRewardsNativeWorker::ShunyaRewardsNativeWorker(JNIEnv* env,
    const base::android::JavaRef<jobject>& obj):
    weak_java_shunya_rewards_native_worker_(env, obj),
    shunya_rewards_service_(nullptr),
    weak_factory_(this) {
  Java_ShunyaRewardsNativeWorker_setNativePtr(env, obj,
    reinterpret_cast<intptr_t>(this));

  shunya_rewards_service_ = shunya_rewards::RewardsServiceFactory::GetForProfile(
      ProfileManager::GetActiveUserProfile()->GetOriginalProfile());
  if (shunya_rewards_service_) {
    shunya_rewards_service_->AddObserver(this);
    shunya_rewards::RewardsNotificationService* notification_service =
      shunya_rewards_service_->GetNotificationService();
    if (notification_service) {
      notification_service->AddObserver(this);
    }
  }
}

ShunyaRewardsNativeWorker::~ShunyaRewardsNativeWorker() {
}

void ShunyaRewardsNativeWorker::Destroy(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->RemoveObserver(this);
    shunya_rewards::RewardsNotificationService* notification_service =
      shunya_rewards_service_->GetNotificationService();
    if (notification_service) {
      notification_service->RemoveObserver(this);
    }
  }
  delete this;
}

bool ShunyaRewardsNativeWorker::IsSupported(JNIEnv* env) {
  return shunya_rewards::IsSupported(
      ProfileManager::GetActiveUserProfile()->GetOriginalProfile()->GetPrefs(),
      shunya_rewards::IsSupportedOptions::kNone);
}

bool ShunyaRewardsNativeWorker::IsSupportedSkipRegionCheck(JNIEnv* env) {
  return shunya_rewards::IsSupported(
      ProfileManager::GetActiveUserProfile()->GetOriginalProfile()->GetPrefs(),
      shunya_rewards::IsSupportedOptions::kSkipRegionCheck);
}

std::string ShunyaRewardsNativeWorker::StringifyResult(
    shunya_rewards::mojom::CreateRewardsWalletResult result) {
  switch (result) {
    case shunya_rewards::mojom::CreateRewardsWalletResult::kSuccess:
      return "success";
    case shunya_rewards::mojom::CreateRewardsWalletResult::
        kWalletGenerationDisabled:
      return "wallet-generation-disabled";
    case shunya_rewards::mojom::CreateRewardsWalletResult::
        kGeoCountryAlreadyDeclared:
      return "country-already-declared";
    case shunya_rewards::mojom::CreateRewardsWalletResult::kUnexpected:
      return "unexpected-error";
  }
}

bool ShunyaRewardsNativeWorker::IsRewardsEnabled(JNIEnv* env) {
  return ProfileManager::GetActiveUserProfile()
      ->GetOriginalProfile()
      ->GetPrefs()
      ->GetBoolean(shunya_rewards::prefs::kEnabled);
}

void ShunyaRewardsNativeWorker::CreateRewardsWallet(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& country_code) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->CreateRewardsWallet(
        base::android::ConvertJavaStringToUTF8(env, country_code),
        base::BindOnce(&ShunyaRewardsNativeWorker::OnCreateRewardsWallet,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnCreateRewardsWallet(
    shunya_rewards::mojom::CreateRewardsWalletResult result) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onCreateRewardsWallet(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, StringifyResult(result)));
}

void ShunyaRewardsNativeWorker::GetRewardsParameters(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetRewardsParameters(
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetRewardsParameters,
                       weak_factory_.GetWeakPtr(), shunya_rewards_service_));
  }
}

void ShunyaRewardsNativeWorker::OnGetRewardsParameters(
    shunya_rewards::RewardsService* rewards_service,
    shunya_rewards::mojom::RewardsParametersPtr parameters) {
  if (parameters) {
    parameters_ = std::move(parameters);
  }

  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnRewardsParameters(
      env, weak_java_shunya_rewards_native_worker_.get(env));
}

double ShunyaRewardsNativeWorker::GetVbatDeadline(JNIEnv* env) {
  if (parameters_) {
    if (!parameters_->vbat_deadline.is_null()) {
      return floor(parameters_->vbat_deadline.ToDoubleT() *
                   base::Time::kMillisecondsPerSecond);
    }
  }
  return 0.0;
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetPayoutStatus(JNIEnv* env) {
  std::string wallet_type;
  std::string payout_status;
  if (shunya_rewards_service_) {
    wallet_type = shunya_rewards_service_->GetExternalWalletType();
    if (parameters_) {
      if (!parameters_->payout_status.empty()) {
        payout_status = parameters_->payout_status.at(wallet_type);
      }
    }
  }
  return base::android::ConvertUTF8ToJavaString(env, payout_status);
}

void ShunyaRewardsNativeWorker::GetUserType(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetUserType(base::BindOnce(
        &ShunyaRewardsNativeWorker::OnGetUserType, weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnGetUserType(
    const shunya_rewards::mojom::UserType user_type) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onGetUserType(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      static_cast<int>(user_type));
}

bool ShunyaRewardsNativeWorker::IsGrandfatheredUser(JNIEnv* env) {
  bool is_grandfathered_user = false;
  if (shunya_rewards_service_) {
    is_grandfathered_user = shunya_rewards_service_->IsGrandfatheredUser();
  }
  return is_grandfathered_user;
}

void ShunyaRewardsNativeWorker::FetchBalance(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->FetchBalance(base::BindOnce(
        &ShunyaRewardsNativeWorker::OnBalance, weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnBalance(
    base::expected<shunya_rewards::mojom::BalancePtr,
                   shunya_rewards::mojom::FetchBalanceError> result) {
  if (result.has_value()) {
    balance_ = *result.value();
  }

  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onBalance(
      env, weak_java_shunya_rewards_native_worker_.get(env), result.has_value());
}

void ShunyaRewardsNativeWorker::GetPublisherInfo(
    JNIEnv* env,
    int tabId,
    const base::android::JavaParamRef<jstring>& host) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetPublisherActivityFromUrl(tabId,
      base::android::ConvertJavaStringToUTF8(env, host), "", "");
  }
}

void ShunyaRewardsNativeWorker::OnPanelPublisherInfo(
    shunya_rewards::RewardsService* rewards_service,
    const shunya_rewards::mojom::Result result,
    const shunya_rewards::mojom::PublisherInfo* info,
    uint64_t tabId) {
  if (!info) {
    return;
  }
  shunya_rewards::mojom::PublisherInfoPtr pi = info->Clone();
  map_publishers_info_[tabId] = std::move(pi);
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnPublisherInfo(env,
        weak_java_shunya_rewards_native_worker_.get(env), tabId);
}

void ShunyaRewardsNativeWorker::OnUnblindedTokensReady(
    shunya_rewards::RewardsService* rewards_service) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onUnblindedTokensReady(
      env, weak_java_shunya_rewards_native_worker_.get(env));
}

void ShunyaRewardsNativeWorker::OnReconcileComplete(
    shunya_rewards::RewardsService* rewards_service,
    const shunya_rewards::mojom::Result result,
    const std::string& contribution_id,
    const double amount,
    const shunya_rewards::mojom::RewardsType type,
    const shunya_rewards::mojom::ContributionProcessor processor) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onReconcileComplete(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      static_cast<int>(result), static_cast<int>(type), amount);
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetPublisherURL(JNIEnv* env, uint64_t tabId) {
  base::android::ScopedJavaLocalRef<jstring> res =
    base::android::ConvertUTF8ToJavaString(env, "");

  PublishersInfoMap::const_iterator iter(map_publishers_info_.find(tabId));
  if (iter != map_publishers_info_.end()) {
    res = base::android::ConvertUTF8ToJavaString(env, iter->second->url);
  }

  return res;
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetPublisherFavIconURL(JNIEnv* env, uint64_t tabId) {
  base::android::ScopedJavaLocalRef<jstring> res =
    base::android::ConvertUTF8ToJavaString(env, "");

  PublishersInfoMap::const_iterator iter(map_publishers_info_.find(tabId));
  if (iter != map_publishers_info_.end()) {
    res = base::android::ConvertUTF8ToJavaString(env,
      iter->second->favicon_url);
  }

  return res;
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetCaptchaSolutionURL(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& paymentId,
    const base::android::JavaParamRef<jstring>& captchaId) {
  const std::string path = base::StringPrintf(
      "/v3/captcha/solution/%s/%s",
      base::android::ConvertJavaStringToUTF8(env, paymentId).c_str(),
      base::android::ConvertJavaStringToUTF8(env, captchaId).c_str());
  std::string captcha_solution_url =
      shunya_adaptive_captcha::ServerUtil::GetInstance()->GetServerUrl(path);

  return base::android::ConvertUTF8ToJavaString(env, captcha_solution_url);
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetAttestationURL(JNIEnv* env) {
  const std::string path = "/v1/attestations/android";
  std::string captcha_solution_url =
      shunya_adaptive_captcha::ServerUtil::GetInstance()->GetServerUrl(path);

  return base::android::ConvertUTF8ToJavaString(env, captcha_solution_url);
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetAttestationURLWithPaymentId(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& paymentId) {
  const std::string path = base::StringPrintf(
      "/v1/attestations/android/%s",
      base::android::ConvertJavaStringToUTF8(env, paymentId).c_str());
  std::string captcha_solution_url =
      shunya_adaptive_captcha::ServerUtil::GetInstance()->GetServerUrl(path);

  return base::android::ConvertUTF8ToJavaString(env, captcha_solution_url);
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetPublisherName(JNIEnv* env, uint64_t tabId) {
  base::android::ScopedJavaLocalRef<jstring> res =
    base::android::ConvertUTF8ToJavaString(env, "");

  PublishersInfoMap::const_iterator iter(map_publishers_info_.find(tabId));
  if (iter != map_publishers_info_.end()) {
    res = base::android::ConvertUTF8ToJavaString(env, iter->second->name);
  }

  return res;
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetPublisherId(JNIEnv* env, uint64_t tabId) {
  base::android::ScopedJavaLocalRef<jstring> res =
    base::android::ConvertUTF8ToJavaString(env, "");

  PublishersInfoMap::const_iterator iter(map_publishers_info_.find(tabId));
  if (iter != map_publishers_info_.end()) {
    res = base::android::ConvertUTF8ToJavaString(env, iter->second->id);
  }

  return res;
}

int ShunyaRewardsNativeWorker::GetPublisherPercent(JNIEnv* env, uint64_t tabId) {
  int res = 0;

  PublishersInfoMap::const_iterator iter(map_publishers_info_.find(tabId));
  if (iter != map_publishers_info_.end()) {
    res = iter->second->percent;
  }

  return res;
}

bool ShunyaRewardsNativeWorker::GetPublisherExcluded(JNIEnv* env,
                                                    uint64_t tabId) {
  bool res = false;

  PublishersInfoMap::const_iterator iter(map_publishers_info_.find(tabId));
  if (iter != map_publishers_info_.end()) {
    res = iter->second->excluded ==
          shunya_rewards::mojom::PublisherExclude::EXCLUDED;
  }

  return res;
}

int ShunyaRewardsNativeWorker::GetPublisherStatus(JNIEnv* env, uint64_t tabId) {
  int res =
      static_cast<int>(shunya_rewards::mojom::PublisherStatus::NOT_VERIFIED);
  PublishersInfoMap::const_iterator iter = map_publishers_info_.find(tabId);
  if (iter != map_publishers_info_.end()) {
    res = static_cast<int>(iter->second->status);
  }
  return res;
}

void ShunyaRewardsNativeWorker::IncludeInAutoContribution(JNIEnv* env,
                                                         uint64_t tabId,
                                                         bool exclude) {
  PublishersInfoMap::iterator iter(map_publishers_info_.find(tabId));
  if (iter != map_publishers_info_.end()) {
    if (exclude) {
      iter->second->excluded = shunya_rewards::mojom::PublisherExclude::EXCLUDED;
    } else {
      iter->second->excluded = shunya_rewards::mojom::PublisherExclude::INCLUDED;
    }
    if (shunya_rewards_service_) {
      shunya_rewards_service_->SetPublisherExclude(iter->second->id, exclude);
    }
  }
}

void ShunyaRewardsNativeWorker::RemovePublisherFromMap(JNIEnv* env,
                                                      uint64_t tabId) {
  PublishersInfoMap::const_iterator iter(map_publishers_info_.find(tabId));
  if (iter != map_publishers_info_.end()) {
    map_publishers_info_.erase(iter);
  }
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetWalletBalance(JNIEnv* env) {
  std::string json_balance;
  base::Value::Dict root;
  root.Set("total", balance_.total);

  base::Value::Dict json_wallets;
  for (const auto & item : balance_.wallets) {
    json_wallets.Set(item.first, item.second);
  }
  root.SetByDottedPath("wallets", std::move(json_wallets));
  base::JSONWriter::Write(std::move(root), &json_balance);

  return base::android::ConvertUTF8ToJavaString(env, json_balance);
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetExternalWalletType(JNIEnv* env) {
  std::string wallet_type;
  if (shunya_rewards_service_) {
    wallet_type = shunya_rewards_service_->GetExternalWalletType();
  }

  return base::android::ConvertUTF8ToJavaString(env, wallet_type);
}

void ShunyaRewardsNativeWorker::GetAdsAccountStatement(JNIEnv* env) {
  auto* ads_service = shunya_ads::AdsServiceFactory::GetForProfile(
      ProfileManager::GetActiveUserProfile()->GetOriginalProfile());
  if (!ads_service) {
    return;
  }
  ads_service->GetStatementOfAccounts(
      base::BindOnce(&ShunyaRewardsNativeWorker::OnGetAdsAccountStatement,
                     weak_factory_.GetWeakPtr()));
}

void ShunyaRewardsNativeWorker::OnGetAdsAccountStatement(
    shunya_ads::mojom::StatementInfoPtr statement) {
  JNIEnv* env = base::android::AttachCurrentThread();
  if (!statement) {
    Java_ShunyaRewardsNativeWorker_OnGetAdsAccountStatement(
        env, weak_java_shunya_rewards_native_worker_.get(env),
        /* success */ false, 0.0, 0, 0.0, 0.0, 0.0);
    return;
  }

  Java_ShunyaRewardsNativeWorker_OnGetAdsAccountStatement(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      /* success */ true, statement->next_payment_date.ToDoubleT() * 1000,
      statement->ads_received_this_month, statement->min_earnings_this_month,
      statement->max_earnings_this_month, statement->max_earnings_last_month);
}

bool ShunyaRewardsNativeWorker::CanConnectAccount(JNIEnv* env) {
  if (!parameters_ || !shunya_rewards_service_) {
    return true;
  }
  std::string country_code = shunya_rewards_service_->GetCountryCode();
  return base::ranges::any_of(
      shunya_rewards_service_->GetExternalWalletProviders(),
      [this, &country_code](const std::string& provider) {
        if (!parameters_->wallet_provider_regions.count(provider)) {
          return true;
        }

        const auto& regions = parameters_->wallet_provider_regions.at(provider);
        if (!regions) {
          return true;
        }

        const auto& [allow, block] = *regions;
        if (allow.empty() && block.empty()) {
          return true;
        }

        return base::Contains(allow, country_code) ||
               (!block.empty() && !base::Contains(block, country_code));
      });
}

base::android::ScopedJavaLocalRef<jdoubleArray>
ShunyaRewardsNativeWorker::GetTipChoices(JNIEnv* env) {
  return base::android::ToJavaDoubleArray(
      env, parameters_ ? parameters_->tip_choices : std::vector<double>());
}

double ShunyaRewardsNativeWorker::GetWalletRate(JNIEnv* env) {
  return parameters_ ? parameters_->rate : 0.0;
}

void ShunyaRewardsNativeWorker::FetchGrants(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->FetchPromotions(base::DoNothing());
  }
}

void ShunyaRewardsNativeWorker::GetCurrentBalanceReport(JNIEnv* env) {
  if (shunya_rewards_service_) {
    auto now = base::Time::Now();
    base::Time::Exploded exploded;
    now.LocalExplode(&exploded);

    shunya_rewards_service_->GetBalanceReport(
        exploded.month, exploded.year,
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetCurrentBalanceReport,
                       weak_factory_.GetWeakPtr(), shunya_rewards_service_));
  }
}

void ShunyaRewardsNativeWorker::OnGetCurrentBalanceReport(
    shunya_rewards::RewardsService* rewards_service,
    const shunya_rewards::mojom::Result result,
    shunya_rewards::mojom::BalanceReportInfoPtr report) {
  base::android::ScopedJavaLocalRef<jdoubleArray> java_array;
  JNIEnv* env = base::android::AttachCurrentThread();
  if (report) {
    std::vector<double> values;
    values.push_back(report->grants);
    values.push_back(report->earning_from_ads);
    values.push_back(report->auto_contribute);
    values.push_back(report->recurring_donation);
    values.push_back(report->one_time_donation);
    java_array = base::android::ToJavaDoubleArray(env, values);
  }
  Java_ShunyaRewardsNativeWorker_OnGetCurrentBalanceReport(env,
        weak_java_shunya_rewards_native_worker_.get(env), java_array);
}

void ShunyaRewardsNativeWorker::Donate(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& publisher_key,
    double amount,
    bool recurring) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->SendContribution(
        base::android::ConvertJavaStringToUTF8(env, publisher_key), amount,
        recurring,
        base::BindOnce(&ShunyaRewardsNativeWorker::OnSendContribution,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnSendContribution(bool result) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onSendContribution(
      env, weak_java_shunya_rewards_native_worker_.get(env), result);
}

void ShunyaRewardsNativeWorker::GetAllNotifications(JNIEnv* env) {
  if (!shunya_rewards_service_) {
    return;
  }
  shunya_rewards::RewardsNotificationService* notification_service =
    shunya_rewards_service_->GetNotificationService();
  if (notification_service) {
    notification_service->GetNotifications();
  }
}

void ShunyaRewardsNativeWorker::DeleteNotification(JNIEnv* env,
        const base::android::JavaParamRef<jstring>& notification_id) {
  if (!shunya_rewards_service_) {
    return;
  }
  shunya_rewards::RewardsNotificationService* notification_service =
    shunya_rewards_service_->GetNotificationService();
  if (notification_service) {
    notification_service->DeleteNotification(
      base::android::ConvertJavaStringToUTF8(env, notification_id));
  }
}

void ShunyaRewardsNativeWorker::GetGrant(JNIEnv* env,
    const base::android::JavaParamRef<jstring>& promotionId) {
  if (shunya_rewards_service_) {
    std::string promotion_id =
      base::android::ConvertJavaStringToUTF8(env, promotionId);
    shunya_rewards_service_->ClaimPromotion(
        promotion_id,
        base::BindOnce(&ShunyaRewardsNativeWorker::OnClaimPromotion,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnClaimPromotion(
    const shunya_rewards::mojom::Result result,
    shunya_rewards::mojom::PromotionPtr promotion) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnClaimPromotion(env,
      weak_java_shunya_rewards_native_worker_.get(env),
      static_cast<int>(result));
}

base::android::ScopedJavaLocalRef<jobjectArray>
    ShunyaRewardsNativeWorker::GetCurrentGrant(JNIEnv* env,
      int position) {
  if ((size_t)position > promotions_.size() - 1) {
    return base::android::ScopedJavaLocalRef<jobjectArray>();
  }
  std::stringstream stream;
  stream << std::fixed << std::setprecision(2) <<
      (promotions_[position])->approximate_value;
  std::vector<std::string> values;
  values.push_back(stream.str());
  values.push_back(
    std::to_string((promotions_[position])->expires_at));
  values.push_back(
      std::to_string(static_cast<int>((promotions_[position])->type)));

  return base::android::ToJavaArrayOfStrings(env, values);
}

void ShunyaRewardsNativeWorker::GetRecurringDonations(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetRecurringTips(
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetRecurringTips,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnGetRecurringTips(
    std::vector<shunya_rewards::mojom::PublisherInfoPtr> list) {
  map_recurrent_publishers_.clear();
  for (const auto& item : list) {
    map_recurrent_publishers_[item->id] = item->Clone();
  }

  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnRecurringDonationUpdated(env,
        weak_java_shunya_rewards_native_worker_.get(env));
}

bool ShunyaRewardsNativeWorker::IsCurrentPublisherInRecurrentDonations(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& publisher) {
  return map_recurrent_publishers_.find(
    base::android::ConvertJavaStringToUTF8(env, publisher)) !=
      map_recurrent_publishers_.end();
}

void ShunyaRewardsNativeWorker::GetAutoContributeProperties(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetAutoContributeProperties(
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetAutoContributeProperties,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnGetAutoContributeProperties(
    shunya_rewards::mojom::AutoContributePropertiesPtr properties) {
  if (properties) {
    auto_contrib_properties_ = std::move(properties);
  }

  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnGetAutoContributeProperties(
      env, weak_java_shunya_rewards_native_worker_.get(env));
}

bool ShunyaRewardsNativeWorker::IsAutoContributeEnabled(JNIEnv* env) {
  if (!auto_contrib_properties_) {
    return false;
  }

  return auto_contrib_properties_->enabled_contribute;
}

void ShunyaRewardsNativeWorker::GetReconcileStamp(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetReconcileStamp(
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetGetReconcileStamp,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::ResetTheWholeState(JNIEnv* env) {
  if (!shunya_rewards_service_) {
    OnResetTheWholeState(false);
    return;
  }
  shunya_rewards_service_->CompleteReset(
      base::BindOnce(&ShunyaRewardsNativeWorker::OnResetTheWholeState,
                     weak_factory_.GetWeakPtr()));
}

void ShunyaRewardsNativeWorker::OnCompleteReset(const bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();

  Java_ShunyaRewardsNativeWorker_onCompleteReset(
      env, weak_java_shunya_rewards_native_worker_.get(env), success);
}

void ShunyaRewardsNativeWorker::OnResetTheWholeState(const bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();

  Java_ShunyaRewardsNativeWorker_OnResetTheWholeState(env,
          weak_java_shunya_rewards_native_worker_.get(env), success);
}

double ShunyaRewardsNativeWorker::GetPublisherRecurrentDonationAmount(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& publisher) {
  double amount(0.0);
  auto it = map_recurrent_publishers_.find(
    base::android::ConvertJavaStringToUTF8(env, publisher));

  if (it != map_recurrent_publishers_.end()) {
    amount = it->second->weight;
  }
  return  amount;
}

void ShunyaRewardsNativeWorker::RemoveRecurring(JNIEnv* env,
    const base::android::JavaParamRef<jstring>& publisher) {
  if (shunya_rewards_service_) {
      shunya_rewards_service_->RemoveRecurringTip(
        base::android::ConvertJavaStringToUTF8(env, publisher));
      auto it = map_recurrent_publishers_.find(
          base::android::ConvertJavaStringToUTF8(env, publisher));

      if (it != map_recurrent_publishers_.end()) {
        map_recurrent_publishers_.erase(it);
      }
  }
}

void ShunyaRewardsNativeWorker::OnGetGetReconcileStamp(uint64_t timestamp) {
  JNIEnv* env = base::android::AttachCurrentThread();

  Java_ShunyaRewardsNativeWorker_OnGetReconcileStamp(env,
          weak_java_shunya_rewards_native_worker_.get(env), timestamp);
}

void ShunyaRewardsNativeWorker::OnNotificationAdded(
    shunya_rewards::RewardsNotificationService* rewards_notification_service,
    const shunya_rewards::RewardsNotificationService::RewardsNotification&
      notification) {
  JNIEnv* env = base::android::AttachCurrentThread();

  Java_ShunyaRewardsNativeWorker_OnNotificationAdded(env,
        weak_java_shunya_rewards_native_worker_.get(env),
        base::android::ConvertUTF8ToJavaString(env, notification.id_),
        notification.type_,
        notification.timestamp_,
        base::android::ToJavaArrayOfStrings(env, notification.args_));
}

void ShunyaRewardsNativeWorker::OnGetAllNotifications(
    shunya_rewards::RewardsNotificationService* rewards_notification_service,
    const shunya_rewards::RewardsNotificationService::RewardsNotificationsList&
      notifications_list) {
  JNIEnv* env = base::android::AttachCurrentThread();

  // Notify about notifications count
  Java_ShunyaRewardsNativeWorker_OnNotificationsCount(env,
        weak_java_shunya_rewards_native_worker_.get(env),
        notifications_list.size());

  shunya_rewards::RewardsNotificationService::RewardsNotificationsList::
    const_iterator iter =
      std::max_element(notifications_list.begin(), notifications_list.end(),
        [](const shunya_rewards::RewardsNotificationService::
            RewardsNotification& notification_a,
          const shunya_rewards::RewardsNotificationService::
            RewardsNotification& notification_b) {
        return notification_a.timestamp_ > notification_b.timestamp_;
      });

  if (iter != notifications_list.end()) {
    Java_ShunyaRewardsNativeWorker_OnGetLatestNotification(env,
        weak_java_shunya_rewards_native_worker_.get(env),
        base::android::ConvertUTF8ToJavaString(env, iter->id_),
        iter->type_,
        iter->timestamp_,
        base::android::ToJavaArrayOfStrings(env, iter->args_));
  }
}

void ShunyaRewardsNativeWorker::OnNotificationDeleted(
      shunya_rewards::RewardsNotificationService* rewards_notification_service,
      const shunya_rewards::RewardsNotificationService::RewardsNotification&
        notification) {
  JNIEnv* env = base::android::AttachCurrentThread();

  Java_ShunyaRewardsNativeWorker_OnNotificationDeleted(env,
        weak_java_shunya_rewards_native_worker_.get(env),
        base::android::ConvertUTF8ToJavaString(env, notification.id_));
}

void ShunyaRewardsNativeWorker::OnPromotionFinished(
    shunya_rewards::RewardsService* rewards_service,
    const shunya_rewards::mojom::Result result,
    shunya_rewards::mojom::PromotionPtr promotion) {
  JNIEnv* env = base::android::AttachCurrentThread();

  Java_ShunyaRewardsNativeWorker_OnGrantFinish(env,
        weak_java_shunya_rewards_native_worker_.get(env),
        static_cast<int>(result));
}

int ShunyaRewardsNativeWorker::GetAdsPerHour(JNIEnv* env) {
  auto* ads_service_ = shunya_ads::AdsServiceFactory::GetForProfile(
      ProfileManager::GetActiveUserProfile()->GetOriginalProfile());
  if (!ads_service_) {
    return DEFAULT_ADS_PER_HOUR;
  }
  return ads_service_->GetMaximumNotificationAdsPerHour();
}

void ShunyaRewardsNativeWorker::SetAdsPerHour(JNIEnv* env, jint value) {
  ProfileManager::GetActiveUserProfile()
      ->GetOriginalProfile()
      ->GetPrefs()
      ->SetInt64(shunya_ads::prefs::kMaximumNotificationAdsPerHour,
                 static_cast<int64_t>(value));
}

void ShunyaRewardsNativeWorker::SetAutoContributionAmount(JNIEnv* env,
                                                         jdouble value) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->SetAutoContributionAmount(value);
  }
}

void ShunyaRewardsNativeWorker::GetAutoContributionAmount(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetAutoContributionAmount(
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetAutoContributionAmount,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnGetAutoContributionAmount(
    double auto_contribution_amount) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnGetAutoContributionAmount(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      auto_contribution_amount);
}

void ShunyaRewardsNativeWorker::GetExternalWallet(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetExternalWallet(
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetExternalWallet,
                       weak_factory_.GetWeakPtr()));
  }
}

base::android::ScopedJavaLocalRef<jstring>
ShunyaRewardsNativeWorker::GetCountryCode(JNIEnv* env) {
  std::string country_code;
  if (shunya_rewards_service_) {
    country_code = shunya_rewards_service_->GetCountryCode();
  }

  return base::android::ConvertUTF8ToJavaString(env, country_code);
}

void ShunyaRewardsNativeWorker::GetAvailableCountries(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetAvailableCountries(
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetAvailableCountries,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnGetAvailableCountries(
    std::vector<std::string> countries) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onGetAvailableCountries(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      base::android::ToJavaArrayOfStrings(env, countries));
}

void ShunyaRewardsNativeWorker::GetPublishersVisitedCount(JNIEnv* env) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetPublishersVisitedCount(
        base::BindOnce(&ShunyaRewardsNativeWorker::OnGetPublishersVisitedCount,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::OnGetPublishersVisitedCount(int count) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onGetPublishersVisitedCount(
      env, weak_java_shunya_rewards_native_worker_.get(env), count);
}

void ShunyaRewardsNativeWorker::GetPublisherBanner(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& publisher_key) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->GetPublisherBanner(
        base::android::ConvertJavaStringToUTF8(env, publisher_key),
        base::BindOnce(&ShunyaRewardsNativeWorker::onPublisherBanner,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaRewardsNativeWorker::onPublisherBanner(
    shunya_rewards::mojom::PublisherBannerPtr banner) {
  std::string json_banner_info;
  if (!banner) {
    json_banner_info = "";
  } else {
    base::Value::Dict dict;
    dict.Set("publisher_key", banner->publisher_key);
    dict.Set("title", banner->title);

    dict.Set("name", banner->name);
    dict.Set("description", banner->description);
    dict.Set("background", banner->background);
    dict.Set("logo", banner->logo);
    dict.Set("provider", banner->provider);
    dict.Set("web3_url", banner->web3_url);

    base::Value::Dict links;
    for (auto const& link : banner->links) {
      links.Set(link.first, link.second);
    }
    dict.Set("links", std::move(links));

    dict.Set("status", static_cast<int32_t>(banner->status));
    base::JSONWriter::Write(dict, &json_banner_info);
  }
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_onPublisherBanner(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, json_banner_info));
}

void ShunyaRewardsNativeWorker::OnGetExternalWallet(
    base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                   shunya_rewards::mojom::GetExternalWalletError> result) {
  auto wallet = std::move(result).value_or(nullptr);
  std::string json_wallet;
  if (!wallet) {
    json_wallet = "";
  } else {
    base::Value::Dict dict;
    dict.Set("token", wallet->token);
    dict.Set("address", wallet->address);

    // enum class WalletStatus : int32_t
    dict.Set("status", static_cast<int32_t>(wallet->status));
    dict.Set("type", wallet->type);
    dict.Set("user_name", wallet->user_name);
    dict.Set("account_url", wallet->account_url);
    dict.Set("login_url", wallet->login_url);
    base::JSONWriter::Write(dict, &json_wallet);
  }
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnGetExternalWallet(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, json_wallet));
}

void ShunyaRewardsNativeWorker::DisconnectWallet(JNIEnv* env) {
  // TODO(zenparsing): Remove disconnect ability from Android UI.
}

void ShunyaRewardsNativeWorker::OnExternalWalletConnected() {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnExternalWalletConnected(
      env, weak_java_shunya_rewards_native_worker_.get(env));
}

void ShunyaRewardsNativeWorker::OnExternalWalletLoggedOut() {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnExternalWalletLoggedOut(
      env, weak_java_shunya_rewards_native_worker_.get(env));
}

void ShunyaRewardsNativeWorker::OnExternalWalletReconnected() {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnExternalWalletReconnected(
      env, weak_java_shunya_rewards_native_worker_.get(env));
}

std::string ShunyaRewardsNativeWorker::StdStrStrMapToJsonString(
    const base::flat_map<std::string, std::string>& args) {
    std::string json_args;
    base::Value::Dict dict;
    for (const auto & item : args) {
      dict.Set(item.first, item.second);
    }
    base::JSONWriter::Write(dict, &json_args);
    return json_args;
}

void ShunyaRewardsNativeWorker::RefreshPublisher(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& publisher_key) {
  if (!shunya_rewards_service_) {
    NOTREACHED();
    return;
  }
  shunya_rewards_service_->RefreshPublisher(
      base::android::ConvertJavaStringToUTF8(env, publisher_key),
      base::BindOnce(&ShunyaRewardsNativeWorker::OnRefreshPublisher,
                     weak_factory_.GetWeakPtr()));
}

void ShunyaRewardsNativeWorker::OnRefreshPublisher(
    const shunya_rewards::mojom::PublisherStatus status,
    const std::string& publisher_key) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaRewardsNativeWorker_OnRefreshPublisher(
      env, weak_java_shunya_rewards_native_worker_.get(env),
      static_cast<int>(status),
      base::android::ConvertUTF8ToJavaString(env, publisher_key));
}

void ShunyaRewardsNativeWorker::SetAutoContributeEnabled(
    JNIEnv* env,
    bool isAutoContributeEnabled) {
  if (shunya_rewards_service_) {
    shunya_rewards_service_->SetAutoContributeEnabled(isAutoContributeEnabled);
  }
}

static void JNI_ShunyaRewardsNativeWorker_Init(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& jcaller) {
  new ShunyaRewardsNativeWorker(env, jcaller);
}

}  // namespace android
}  // namespace chrome

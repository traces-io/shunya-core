/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <memory>
#include <utility>

#include "base/files/scoped_temp_dir.h"
#include "base/strings/string_split.h"
#include "base/system/sys_info.h"
#include "base/test/bind.h"
#include "base/test/metrics/histogram_tester.h"
#include "base/time/time.h"
#include "shunya/browser/shunya_ads/analytics/p3a/shunya_stats_helper.h"
#include "shunya/browser/shunya_stats/shunya_stats_updater.h"
#include "shunya/browser/shunya_stats/shunya_stats_updater_params.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"
#include "shunya/components/shunya_referrals/browser/shunya_referrals_service.h"
#include "shunya/components/shunya_rewards/browser/rewards_service.h"
#include "shunya/components/shunya_stats/browser/shunya_stats_updater_util.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_prefs.h"
#include "shunya/components/shunya_wallet/browser/pref_names.h"
#include "shunya/components/constants/pref_names.h"
#include "shunya/components/misc_metrics/general_browser_usage.h"

#include "build/build_config.h"
#include "chrome/browser/prefs/browser_prefs.h"
#include "components/sync_preferences/testing_pref_service_syncable.h"
#include "content/public/test/browser_task_environment.h"
#include "services/network/public/cpp/weak_wrapper_shared_url_loader_factory.h"
#include "services/network/test/test_url_loader_factory.h"
#include "testing/gmock/include/gmock/gmock.h"
#include "testing/gtest/include/gtest/gtest.h"

using testing::HasSubstr;

// npm run test -- shunya_unit_tests --filter=ShunyaStatsUpdaterTest.*

const char kYesterday[] = "2018-06-21";
const char kToday[] = "2018-06-22";
const char kTomorrow[] = "2018-06-23";

const int kLastWeek = 24;
const int kThisWeek = 25;
const int kNextWeek = 26;

const int kLastMonth = 5;
const int kThisMonth = 6;
const int kNextMonth = 7;

class ShunyaStatsUpdaterTest : public testing::Test {
 public:
  ShunyaStatsUpdaterTest()
      : task_environment_(base::test::TaskEnvironment::TimeSource::MOCK_TIME),
        shared_url_loader_factory_(
            base::MakeRefCounted<network::WeakWrapperSharedURLLoaderFactory>(
                &url_loader_factory_)) {}
  ~ShunyaStatsUpdaterTest() override = default;

  void SetUp() override {
#if BUILDFLAG(IS_ANDROID)
    task_environment_.AdvanceClock(base::Days(2));
#else
    base::Time future_mock_time;
    if (base::Time::FromString("3000-01-04", &future_mock_time)) {
      task_environment_.AdvanceClock(future_mock_time - base::Time::Now());
    }
#endif
    shunya_wallet::RegisterLocalStatePrefs(testing_local_state_.registry());
    task_environment_.AdvanceClock(base::Minutes(30));

    shunya_stats::RegisterLocalStatePrefs(testing_local_state_.registry());
    misc_metrics::GeneralBrowserUsage::RegisterPrefs(
        testing_local_state_.registry());
    shunya::RegisterPrefsForShunyaReferralsService(
        testing_local_state_.registry());
    shunya_ads::ShunyaStatsHelper::RegisterLocalStatePrefs(
        testing_local_state_.registry());
    SetCurrentTimeForTest(base::Time());
    shunya_stats::ShunyaStatsUpdaterParams::SetFirstRunForTest(true);
  }

  PrefService* GetLocalState() { return &testing_local_state_; }
  std::unique_ptr<shunya_stats::ShunyaStatsUpdaterParams> BuildUpdaterParams() {
    return std::make_unique<shunya_stats::ShunyaStatsUpdaterParams>(
        GetLocalState(), shunya_stats::ProcessArch::kArchSkip);
  }
  void SetEnableAds(bool ads_enabled) {
    GetLocalState()->SetBoolean(shunya_ads::prefs::kEnabledForLastProfile,
                                ads_enabled);
  }

  void SetCurrentTimeForTest(const base::Time& current_time) {
    shunya_stats::ShunyaStatsUpdaterParams::SetCurrentTimeForTest(current_time);
  }

 protected:
  content::BrowserTaskEnvironment task_environment_;
  network::TestURLLoaderFactory url_loader_factory_;
  scoped_refptr<network::SharedURLLoaderFactory> shared_url_loader_factory_;
  base::HistogramTester histogram_tester_;

 private:
  TestingPrefServiceSimple testing_local_state_;
};

TEST_F(ShunyaStatsUpdaterTest, IsDailyUpdateNeededLastCheckedYesterday) {
  GetLocalState()->SetString(kLastCheckYMD, kYesterday);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetDailyParam(), "true");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetString(kLastCheckYMD), kToday);
}

TEST_F(ShunyaStatsUpdaterTest, IsDailyUpdateNeededLastCheckedToday) {
  GetLocalState()->SetString(kLastCheckYMD, kToday);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetDailyParam(), "false");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetString(kLastCheckYMD), kToday);
}

TEST_F(ShunyaStatsUpdaterTest, IsDailyUpdateNeededLastCheckedTomorrow) {
  GetLocalState()->SetString(kLastCheckYMD, kTomorrow);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetDailyParam(), "false");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetString(kLastCheckYMD), kToday);
}

TEST_F(ShunyaStatsUpdaterTest, IsWeeklyUpdateNeededLastCheckedLastWeek) {
  GetLocalState()->SetInteger(kLastCheckWOY, kLastWeek);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetWeeklyParam(), "true");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckWOY), kThisWeek);
}

TEST_F(ShunyaStatsUpdaterTest, IsWeeklyUpdateNeededLastCheckedThisWeek) {
  GetLocalState()->SetInteger(kLastCheckWOY, kThisWeek);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetWeeklyParam(), "false");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckWOY), kThisWeek);
}

TEST_F(ShunyaStatsUpdaterTest, IsWeeklyUpdateNeededLastCheckedNextWeek) {
  GetLocalState()->SetInteger(kLastCheckWOY, kNextWeek);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetWeeklyParam(), "true");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckWOY), kThisWeek);
}

TEST_F(ShunyaStatsUpdaterTest, IsMonthlyUpdateNeededLastCheckedLastMonth) {
  GetLocalState()->SetInteger(kLastCheckMonth, kLastMonth);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetMonthlyParam(), "true");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckMonth), kThisMonth);
}

TEST_F(ShunyaStatsUpdaterTest, IsMonthlyUpdateNeededLastCheckedThisMonth) {
  GetLocalState()->SetInteger(kLastCheckMonth, kThisMonth);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetMonthlyParam(), "false");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckMonth), kThisMonth);
}

TEST_F(ShunyaStatsUpdaterTest, IsMonthlyUpdateNeededLastCheckedNextMonth) {
  GetLocalState()->SetInteger(kLastCheckMonth, kNextMonth);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetMonthlyParam(), "true");
  shunya_stats_updater_params.SavePrefs();

  EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckMonth), kThisMonth);
}

TEST_F(ShunyaStatsUpdaterTest, HasAdsDisabled) {
  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  SetEnableAds(false);
  EXPECT_EQ(shunya_stats_updater_params.GetAdsEnabledParam(), "false");
}

TEST_F(ShunyaStatsUpdaterTest, HasAdsEnabled) {
  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  SetEnableAds(true);
  EXPECT_EQ(shunya_stats_updater_params.GetAdsEnabledParam(), "true");
}

TEST_F(ShunyaStatsUpdaterTest, HasArchSkip) {
  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetProcessArchParam(), "");
}

TEST_F(ShunyaStatsUpdaterTest, HasArchVirt) {
  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchVirt, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetProcessArchParam(), "virt");
}

TEST_F(ShunyaStatsUpdaterTest, HasArchMetal) {
  auto arch = base::SysInfo::OperatingSystemArchitecture();
  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchMetal, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetProcessArchParam(), arch);
}

TEST_F(ShunyaStatsUpdaterTest, HasDateOfInstallationFirstRun) {
  base::Time::Exploded exploded;
  base::Time current_time;

  // Set date to 2018-11-04 (ISO week #44)
  exploded.hour = 0;
  exploded.minute = 0;
  exploded.second = 0;
  exploded.millisecond = 0;
  exploded.day_of_week = 0;
  exploded.year = 2018;
  exploded.month = 11;
  exploded.day_of_month = 4;

  EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));
  SetCurrentTimeForTest(current_time);

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(shunya_stats_updater_params.GetDateOfInstallationParam(),
            "2018-11-04");
}

TEST_F(ShunyaStatsUpdaterTest, HasDailyRetention) {
  base::Time::Exploded exploded;
  base::Time current_time, dtoi_time;

  // Set date to 2018-11-04
  exploded.hour = 0;
  exploded.minute = 0;
  exploded.second = 0;
  exploded.millisecond = 0;
  exploded.day_of_week = 0;
  exploded.year = 2018;
  exploded.month = 11;
  exploded.day_of_month = 4;

  EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &dtoi_time));
  // Make first run date 15 days earlier (still within 30 day window)
  exploded.day_of_month = 20;
  EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));

  SetCurrentTimeForTest(dtoi_time);
  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  SetCurrentTimeForTest(current_time);
  EXPECT_EQ(shunya_stats_updater_params.GetDateOfInstallationParam(),
            "2018-11-04");
}

TEST_F(ShunyaStatsUpdaterTest, GetUpdateURLHasFirstAndDtoi) {
  base::Time current_time, install_time;

  // Set date to 2018-11-04
  EXPECT_TRUE(base::Time::FromString("2018-11-04", &install_time));

  // Make first run date 15 days earlier (still within 30 day window)
  current_time = install_time + base::Days(16);

  SetCurrentTimeForTest(install_time);
  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  SetCurrentTimeForTest(current_time);

  GURL response = shunya_stats_updater_params.GetUpdateURL(
      GURL("https://demo.shunya.com"), "platform id here", "channel name here",
      "full shunya version here");

  base::StringPairs kv_pairs;
  // this will return `false` because at least one argument has no value
  // ex: `arch` will have an empty value (because of kArchSkip).
  base::SplitStringIntoKeyValuePairsUsingSubstr(response.query(), '=', "&",
                                                &kv_pairs);
  EXPECT_FALSE(kv_pairs.empty());

  bool first_is_true = false;
  bool has_dtoi = false;
  for (auto& kv : kv_pairs) {
    if (kv.first == "first") {
      EXPECT_EQ(kv.second, "true");
      first_is_true = true;
    } else if (kv.first == "dtoi") {
      EXPECT_EQ(kv.second, "2018-11-04");
      has_dtoi = true;
      // Audit passed-through parameters.
      // Should not be modified (other than url encode).
    } else if (kv.first == "platform") {
      EXPECT_EQ(kv.second, "platform+id+here");
    } else if (kv.first == "channel") {
      EXPECT_EQ(kv.second, "channel+name+here");
    } else if (kv.first == "version") {
      EXPECT_EQ(kv.second, "full+shunya+version+here");
    }
  }

  EXPECT_EQ(true, first_is_true);
  EXPECT_EQ(true, has_dtoi);
}

TEST_F(ShunyaStatsUpdaterTest, HasDailyRetentionExpiration) {
  base::Time::Exploded exploded;
  base::Time current_time, dtoi_time;

  // Set date to 2018-11-04
  exploded.hour = 0;
  exploded.minute = 0;
  exploded.second = 0;
  exploded.millisecond = 0;
  exploded.day_of_week = 0;
  exploded.year = 2018;
  exploded.month = 11;
  exploded.day_of_month = 4;

  EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &dtoi_time));
  // Make first run date a month earlier (outside 30 day window)
  exploded.month = 12;
  EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));

  SetCurrentTimeForTest(dtoi_time);
  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  SetCurrentTimeForTest(current_time);
  EXPECT_EQ(shunya_stats_updater_params.GetDateOfInstallationParam(), "null");
}

// This test ensures that our weekly stats cut over on Monday
TEST_F(ShunyaStatsUpdaterTest, IsWeeklyUpdateNeededOnMondayLastCheckedOnSunday) {
  base::Time::Exploded exploded;
  base::Time current_time;

  {
    // Set our local state to indicate that the last weekly check was
    // performed during ISO week #43
    GetLocalState()->SetInteger(kLastCheckWOY, 43);

    // Set date to 2018-11-04 (ISO week #44)
    exploded.hour = 0;
    exploded.minute = 0;
    exploded.second = 0;
    exploded.millisecond = 0;
    exploded.day_of_week = 0;
    exploded.year = 2018;
    exploded.month = 11;
    exploded.day_of_month = 4;

    EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));

    SetCurrentTimeForTest(current_time);
    shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
        GetLocalState(), shunya_stats::ProcessArch::kArchSkip);

    // Make sure that the weekly param was set to true, since this is
    // a new ISO week (#44)
    EXPECT_EQ(shunya_stats_updater_params.GetWeeklyParam(), "true");
    shunya_stats_updater_params.SavePrefs();

    // Make sure that local state was updated to reflect this as well
    EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckWOY), 44);
  }

  {
    // Now it's the next day (Monday)
    exploded.day_of_week = 1;
    exploded.day_of_month = 5;

    EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));

    SetCurrentTimeForTest(current_time);
    shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
        GetLocalState(), shunya_stats::ProcessArch::kArchSkip);

    // Make sure that the weekly param was set to true, since this is
    // a new ISO week (#45)
    EXPECT_EQ(shunya_stats_updater_params.GetWeeklyParam(), "true");
    shunya_stats_updater_params.SavePrefs();

    // Make sure that local state was updated to reflect this as well
    EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckWOY), 45);
  }

  {
    // Now it's the next day (Tuesday)
    exploded.day_of_week = 2;
    exploded.day_of_month = 6;

    EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));

    SetCurrentTimeForTest(current_time);
    shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
        GetLocalState(), shunya_stats::ProcessArch::kArchSkip);

    // Make sure that the weekly param was set to false, since this is
    // still the same ISO week (#45)
    EXPECT_EQ(shunya_stats_updater_params.GetWeeklyParam(), "false");
    shunya_stats_updater_params.SavePrefs();

    // Make sure that local state also didn't change
    EXPECT_EQ(GetLocalState()->GetInteger(kLastCheckWOY), 45);
  }
}

TEST_F(ShunyaStatsUpdaterTest, HasCorrectWeekOfInstallation) {
  base::Time::Exploded exploded;
  base::Time current_time;

  {
    // Set date to 2019-03-24 (Sunday)
    exploded.hour = 0;
    exploded.minute = 0;
    exploded.second = 0;
    exploded.millisecond = 0;
    exploded.day_of_week = 0;
    exploded.year = 2019;
    exploded.month = 3;
    exploded.day_of_month = 24;

    EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));
    SetCurrentTimeForTest(current_time);

    // Make sure that week of installation is previous Monday
    shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
        GetLocalState(), shunya_stats::ProcessArch::kArchSkip);
    EXPECT_EQ(shunya_stats_updater_params.GetWeekOfInstallationParam(),
              "2019-03-18");
  }

  {
    // Set date to 2019-03-25 (Monday)
    exploded.hour = 0;
    exploded.minute = 0;
    exploded.second = 0;
    exploded.millisecond = 0;
    exploded.day_of_week = 0;
    exploded.year = 2019;
    exploded.month = 3;
    exploded.day_of_month = 25;

    EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));
    SetCurrentTimeForTest(current_time);

    // Make sure that week of installation is today, since today is a
    // Monday
    shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
        GetLocalState(), shunya_stats::ProcessArch::kArchSkip);
    EXPECT_EQ(shunya_stats_updater_params.GetWeekOfInstallationParam(),
              "2019-03-25");
  }

  {
    // Set date to 2019-03-30 (Saturday)
    exploded.hour = 0;
    exploded.minute = 0;
    exploded.second = 0;
    exploded.millisecond = 0;
    exploded.day_of_week = 0;
    exploded.year = 2019;
    exploded.month = 3;
    exploded.day_of_month = 30;

    EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &current_time));
    SetCurrentTimeForTest(current_time);

    // Make sure that week of installation is previous Monday
    shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
        GetLocalState(), shunya_stats::ProcessArch::kArchSkip);
    EXPECT_EQ(shunya_stats_updater_params.GetWeekOfInstallationParam(),
              "2019-03-25");
  }
}

TEST_F(ShunyaStatsUpdaterTest, GetIsoWeekNumber) {
  base::Time::Exploded exploded;
  exploded.hour = 0;
  exploded.minute = 0;
  exploded.second = 0;
  exploded.millisecond = 0;
  exploded.day_of_week = 1;
  exploded.day_of_month = 29;
  exploded.month = 7;
  exploded.year = 2019;

  base::Time time;
  EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &time));
  EXPECT_EQ(shunya_stats::GetIsoWeekNumber(time), 31);

  exploded.day_of_month = 30;
  exploded.month = 9;

  EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &time));
  EXPECT_EQ(shunya_stats::GetIsoWeekNumber(time), 40);

  exploded.day_of_month = 1;
  exploded.month = 9;
  exploded.day_of_week = 0;

  EXPECT_TRUE(base::Time::FromLocalExploded(exploded, &time));
  EXPECT_EQ(shunya_stats::GetIsoWeekNumber(time), 35);
}

TEST_F(ShunyaStatsUpdaterTest, UsageBitstringDaily) {
  base::Time last_reported_use;
  base::Time last_use;

  EXPECT_TRUE(base::Time::FromString("2020-03-31", &last_use));
  EXPECT_TRUE(base::Time::FromString("2020-03-30", &last_reported_use));

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);

  EXPECT_EQ(0b001, shunya_stats::UsageBitfieldFromTimestamp(last_use,
                                                           last_reported_use));
}

TEST_F(ShunyaStatsUpdaterTest, UsageBitstringWeekly) {
  base::Time last_reported_use;
  base::Time last_use;

  EXPECT_TRUE(base::Time::FromString("2020-03-31", &last_use));
  EXPECT_TRUE(base::Time::FromString("2020-03-26", &last_reported_use));

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);

  EXPECT_EQ(0b011, shunya_stats::UsageBitfieldFromTimestamp(last_use,
                                                           last_reported_use));
}

TEST_F(ShunyaStatsUpdaterTest, UsageBitstringMonthlySameWeek) {
  base::Time last_reported_use;
  base::Time last_use;

  EXPECT_TRUE(base::Time::FromString("2020-07-01", &last_use));
  EXPECT_TRUE(base::Time::FromString("2020-06-30", &last_reported_use));

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(0b101, shunya_stats::UsageBitfieldFromTimestamp(last_use,
                                                           last_reported_use));
}

TEST_F(ShunyaStatsUpdaterTest, UsageBitstringMonthlyDiffWeek) {
  base::Time last_reported_use;
  base::Time last_use;

  EXPECT_TRUE(base::Time::FromString("2020-03-01", &last_use));
  EXPECT_TRUE(base::Time::FromString("2020-02-15", &last_reported_use));

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(0b111, shunya_stats::UsageBitfieldFromTimestamp(last_use,
                                                           last_reported_use));
}

TEST_F(ShunyaStatsUpdaterTest, UsageBitstringInactive) {
  base::Time last_reported_use;
  base::Time last_use;

  EXPECT_TRUE(base::Time::FromString("2020-03-31", &last_use));
  EXPECT_TRUE(base::Time::FromString("2020-03-31", &last_reported_use));

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(0b000, shunya_stats::UsageBitfieldFromTimestamp(last_use,
                                                           last_reported_use));
}

TEST_F(ShunyaStatsUpdaterTest, UsageBitstringNeverUsed) {
  base::Time last_reported_use;
  base::Time last_use;

  shunya_stats::ShunyaStatsUpdaterParams shunya_stats_updater_params(
      GetLocalState(), shunya_stats::ProcessArch::kArchSkip, kToday, kThisWeek,
      kThisMonth);
  EXPECT_EQ(0b000, shunya_stats::UsageBitfieldFromTimestamp(last_use,
                                                           last_reported_use));
}

TEST_F(ShunyaStatsUpdaterTest, UsageURLFlags) {
  auto params = BuildUpdaterParams();

  GURL base_url("http://localhost:8080");
  GURL url;

  PrefService* local_state = GetLocalState();

  url = params->GetUpdateURL(base_url, "", "", "");
  EXPECT_THAT(url.query(), HasSubstr("daily=true&weekly=true&monthly=true"));
  EXPECT_THAT(url.query(), HasSubstr("wallet2=0"));
  params->SavePrefs();

  task_environment_.AdvanceClock(base::Days(1));
  local_state->SetTime(kShunyaWalletLastUnlockTime, base::Time::Now());

  params = BuildUpdaterParams();
  url = params->GetUpdateURL(base_url, "", "", "");
  EXPECT_THAT(url.query(), HasSubstr("daily=true&weekly=false&monthly=false"));
  EXPECT_THAT(url.query(), HasSubstr("wallet2=7"));
  params->SavePrefs();

  task_environment_.AdvanceClock(base::Days(6));
  local_state->SetTime(kShunyaWalletLastUnlockTime, base::Time::Now());
  params = BuildUpdaterParams();
  url = params->GetUpdateURL(base_url, "", "", "");
  EXPECT_THAT(url.query(), HasSubstr("daily=true&weekly=true&monthly=false"));
  EXPECT_THAT(url.query(), HasSubstr("wallet2=3"));
  params->SavePrefs();

  task_environment_.AdvanceClock(base::Days(1));
  local_state->SetTime(kShunyaWalletLastUnlockTime, base::Time::Now());
  params = BuildUpdaterParams();
  url = params->GetUpdateURL(base_url, "", "", "");
  EXPECT_THAT(url.query(), HasSubstr("daily=true&weekly=false&monthly=false"));
  EXPECT_THAT(url.query(), HasSubstr("wallet2=1"));
  params->SavePrefs();
}

TEST_F(ShunyaStatsUpdaterTest, UsagePingRequest) {
  int ping_count = 0;
  GURL last_url;

  url_loader_factory_.SetInterceptor(
      base::BindLambdaForTesting([&](const network::ResourceRequest& request) {
        url_loader_factory_.ClearResponses();
        url_loader_factory_.AddResponse(request.url.spec(), "{\"ok\":1}");
        EXPECT_EQ(request.url.spec().find("https://localhost:8443"), (size_t)0);
      }));

  shunya_stats::ShunyaStatsUpdater updater(GetLocalState(),
                                         /*profile_manager*/ nullptr);
  updater.SetURLLoaderFactoryForTesting(shared_url_loader_factory_);
  shunya_stats::ShunyaStatsUpdater::StatsUpdatedCallback cb = base::BindRepeating(
      [](int* ping_count, GURL* last_url, const GURL& url) {
        *last_url = url;
        (*ping_count)++;
      },
      &ping_count, &last_url);
  updater.SetStatsUpdatedCallbackForTesting(&cb);
  updater.SetUsageServerForTesting("https://localhost:8443");

  // daily, monthly, weekly ping
  task_environment_.FastForwardBy(base::Hours(1));
  EXPECT_THAT(last_url.query(),
              HasSubstr("daily=true&weekly=true&monthly=true"));

  // daily ping
  task_environment_.AdvanceClock(base::Days(1));
  task_environment_.FastForwardBy(base::Seconds(1));
  EXPECT_THAT(last_url.query(),
              HasSubstr("daily=true&weekly=false&monthly=false"));

  // daily, weekly ping
  task_environment_.AdvanceClock(base::Days(7));
  task_environment_.FastForwardBy(base::Seconds(1));
  EXPECT_THAT(last_url.query(),
              HasSubstr("daily=true&weekly=true&monthly=false"));

  ASSERT_EQ(ping_count, 3);
}

TEST_F(ShunyaStatsUpdaterTest, RecordP3APing) {
  shunya_stats::ShunyaStatsUpdater updater(GetLocalState(),
                                         /*profile_manager*/ nullptr);
  updater.SetURLLoaderFactoryForTesting(shared_url_loader_factory_);

  histogram_tester_.ExpectUniqueSample(
      shunya_stats::kP3AMonthlyPingHistogramName, 1, 1);
  histogram_tester_.ExpectUniqueSample(shunya_stats::kP3ADailyPingHistogramName,
                                       1, 1);

  task_environment_.FastForwardBy(base::Days(1));

  histogram_tester_.ExpectUniqueSample(
      shunya_stats::kP3AMonthlyPingHistogramName, 1, 1);
  histogram_tester_.ExpectUniqueSample(shunya_stats::kP3ADailyPingHistogramName,
                                       1, 1);
}

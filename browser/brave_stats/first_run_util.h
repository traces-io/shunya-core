/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_STATS_FIRST_RUN_UTIL_H_
#define SHUNYA_BROWSER_SHUNYA_STATS_FIRST_RUN_UTIL_H_

#include "base/time/time.h"

class PrefService;

namespace shunya_stats {

base::Time GetFirstRunTime(PrefService* local_state);

}  // namespace shunya_stats

#endif  // SHUNYA_BROWSER_SHUNYA_STATS_FIRST_RUN_UTIL_H_

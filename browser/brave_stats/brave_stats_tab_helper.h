/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_STATS_SHUNYA_STATS_TAB_HELPER_H_
#define SHUNYA_BROWSER_SHUNYA_STATS_SHUNYA_STATS_TAB_HELPER_H_

#include "content/public/browser/web_contents_observer.h"
#include "content/public/browser/web_contents_user_data.h"

namespace content {
class NavigationHandle;
class WebContents;
}  // namespace content

namespace shunya_stats {

class ShunyaStatsUpdater;

class ShunyaStatsTabHelper
    : public content::WebContentsObserver,
      public content::WebContentsUserData<ShunyaStatsTabHelper> {
 public:
  explicit ShunyaStatsTabHelper(content::WebContents*);
  ~ShunyaStatsTabHelper() override;
  ShunyaStatsTabHelper(const ShunyaStatsTabHelper&) = delete;
  ShunyaStatsTabHelper& operator=(const ShunyaStatsTabHelper&) = delete;

  void NotifyStatsUpdater();

 private:
  void DidStartNavigation(content::NavigationHandle*) override;

  friend class content::WebContentsUserData<ShunyaStatsTabHelper>;
  WEB_CONTENTS_USER_DATA_KEY_DECL();
};

}  // namespace shunya_stats
#endif  // SHUNYA_BROWSER_SHUNYA_STATS_SHUNYA_STATS_TAB_HELPER_H_

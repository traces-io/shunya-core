// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_SHUNYA_NEWS_SHUNYA_NEWS_CONTROLLER_FACTORY_H_
#define SHUNYA_BROWSER_SHUNYA_NEWS_SHUNYA_NEWS_CONTROLLER_FACTORY_H_

#include "shunya/components/shunya_news/common/shunya_news.mojom.h"
#include "components/keyed_service/content/browser_context_keyed_service_factory.h"
#include "components/keyed_service/core/keyed_service.h"
#include "content/public/browser/browser_context.h"
#include "mojo/public/cpp/bindings/pending_remote.h"

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace content {
class BrowserContext;
}

namespace shunya_news {

class ShunyaNewsController;

class ShunyaNewsControllerFactory : public BrowserContextKeyedServiceFactory {
 public:
  static ShunyaNewsController* GetForContext(content::BrowserContext* context);
  static mojo::PendingRemote<mojom::ShunyaNewsController> GetRemoteService(
      content::BrowserContext* context);
  static ShunyaNewsController* GetControllerForContext(
      content::BrowserContext* context);
  static ShunyaNewsControllerFactory* GetInstance();

  ShunyaNewsControllerFactory(const ShunyaNewsControllerFactory&) = delete;
  ShunyaNewsControllerFactory& operator=(const ShunyaNewsControllerFactory&) =
      delete;

 private:
  friend base::NoDestructor<ShunyaNewsControllerFactory>;

  ShunyaNewsControllerFactory();
  ~ShunyaNewsControllerFactory() override;

  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
};

}  // namespace shunya_news

#endif  // SHUNYA_BROWSER_SHUNYA_NEWS_SHUNYA_NEWS_CONTROLLER_FACTORY_H_

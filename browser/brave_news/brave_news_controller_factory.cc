// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/browser/shunya_news/shunya_news_controller_factory.h"

#include "base/no_destructor.h"
#include "shunya/browser/shunya_ads/ads_service_factory.h"
#include "shunya/browser/profiles/profile_util.h"
#include "shunya/components/shunya_news/browser/shunya_news_controller.h"
#include "chrome/browser/favicon/favicon_service_factory.h"
#include "chrome/browser/history/history_service_factory.h"
#include "chrome/browser/profiles/incognito_helpers.h"
#include "chrome/browser/profiles/profile.h"
#include "components/favicon/core/favicon_service.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/keyed_service/core/service_access_type.h"
#include "content/public/browser/browser_context.h"
#include "services/network/public/cpp/shared_url_loader_factory.h"

namespace shunya_news {

// static
ShunyaNewsControllerFactory* ShunyaNewsControllerFactory::GetInstance() {
  static base::NoDestructor<ShunyaNewsControllerFactory> instance;
  return instance.get();
}

// static
ShunyaNewsController* ShunyaNewsControllerFactory::GetForContext(
    content::BrowserContext* context) {
  return static_cast<ShunyaNewsController*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

// static
mojo::PendingRemote<mojom::ShunyaNewsController>
ShunyaNewsControllerFactory::GetRemoteService(content::BrowserContext* context) {
  return static_cast<ShunyaNewsController*>(
             GetInstance()->GetServiceForBrowserContext(context, true))
      ->MakeRemote();
}

// static
ShunyaNewsController* ShunyaNewsControllerFactory::GetControllerForContext(
    content::BrowserContext* context) {
  return static_cast<ShunyaNewsController*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

ShunyaNewsControllerFactory::ShunyaNewsControllerFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaNewsControllerFactory",
          BrowserContextDependencyManager::GetInstance()) {
  DependsOn(shunya_ads::AdsServiceFactory::GetInstance());
  DependsOn(FaviconServiceFactory::GetInstance());
  DependsOn(HistoryServiceFactory::GetInstance());
}

ShunyaNewsControllerFactory::~ShunyaNewsControllerFactory() = default;

KeyedService* ShunyaNewsControllerFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  if (!shunya::IsRegularProfile(context)) {
    return nullptr;
  }
  auto* profile = Profile::FromBrowserContext(context);
  if (!profile) {
    return nullptr;
  }
  auto* favicon_service = FaviconServiceFactory::GetForProfile(
      profile, ServiceAccessType::EXPLICIT_ACCESS);
  auto* ads_service = shunya_ads::AdsServiceFactory::GetForProfile(profile);
  auto* history_service = HistoryServiceFactory::GetForProfile(
      profile, ServiceAccessType::EXPLICIT_ACCESS);
  return new ShunyaNewsController(profile->GetPrefs(), favicon_service,
                                 ads_service, history_service,
                                 profile->GetURLLoaderFactory());
}

}  // namespace shunya_news

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "base/android/jni_android.h"
#include "shunya/browser/shunya_news/shunya_news_controller_factory.h"
#include "shunya/build/android/jni_headers/ShunyaNewsControllerFactory_jni.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/profiles/profile_manager.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"

namespace chrome {
namespace android {
static jlong JNI_ShunyaNewsControllerFactory_GetInterfaceToShunyaNewsController(
    JNIEnv* env) {
  auto* profile = ProfileManager::GetActiveUserProfile();
  auto pending =
      shunya_news::ShunyaNewsControllerFactory::GetInstance()->GetRemoteService(
          profile);
  auto passPipe = pending.PassPipe();

  return static_cast<jlong>(passPipe.release().value());
}

}  // namespace android
}  // namespace chrome

/* Copyright 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_P3A_P3A_CORE_METRICS_H_
#define SHUNYA_BROWSER_P3A_P3A_CORE_METRICS_H_

// The classes below can be used on desktop only
// because BrowserListObserver is available on desktop only
// Shunya.Uptime.BrowserOpenMinutes, Shunya.Core.LastTimeIncognitoUsed and
// Shunya.Core.TorEverUsed don't work on Android

#include "build/build_config.h"

#if BUILDFLAG(IS_ANDROID)
#error This file should only be included on desktop.
#endif

#include <list>

#include "base/memory/raw_ptr.h"
#include "base/timer/timer.h"
#include "shunya/components/time_period_storage/weekly_storage.h"
#include "chrome/browser/resource_coordinator/usage_clock.h"
#include "chrome/browser/ui/browser_list_observer.h"

class PrefService;
class PrefRegistrySimple;

namespace shunya {

class ShunyaUptimeTracker {
 public:
  explicit ShunyaUptimeTracker(PrefService* local_state);
  ShunyaUptimeTracker(const ShunyaUptimeTracker&) = delete;
  ShunyaUptimeTracker& operator=(const ShunyaUptimeTracker&) = delete;
  ~ShunyaUptimeTracker();

  static void CreateInstance(PrefService* local_state);

  static void RegisterPrefs(PrefRegistrySimple* registry);

 private:
  void RecordUsage();
  void RecordP3A();

  resource_coordinator::UsageClock usage_clock_;
  base::RepeatingTimer timer_;
  base::TimeDelta current_total_usage_;
  WeeklyStorage state_;
};

// ShunyaWindowTracker is under !OS_ANDROID guard because
// BrowserListObserver should only be only on desktop
// Shunya.Uptime.BrowserOpenMinutes and Shunya.Core.LastTimeIncognitoUsed
// don't work on Android
#if !BUILDFLAG(IS_ANDROID)
// Periodically records P3A stats (extracted from Local State) regarding the
// time when incognito windows were used.
// Used as a leaking singletone.
class ShunyaWindowTracker : public BrowserListObserver {
 public:
  explicit ShunyaWindowTracker(PrefService* local_state);
  ShunyaWindowTracker(const ShunyaWindowTracker&) = delete;
  ShunyaWindowTracker& operator=(const ShunyaWindowTracker&) = delete;
  ~ShunyaWindowTracker() override;

  static void CreateInstance(PrefService* local_state);

  static void RegisterPrefs(PrefRegistrySimple* registry);

 private:
  // BrowserListObserver:
  void OnBrowserAdded(Browser* browser) override;
  void OnBrowserSetLastActive(Browser* browser) override;

  void UpdateP3AValues() const;

  base::RepeatingTimer timer_;
  raw_ptr<PrefService> local_state_ = nullptr;
};
#endif  // !BUILDFLAG(IS_ANDROID)

}  // namespace shunya

#endif  // SHUNYA_BROWSER_P3A_P3A_CORE_METRICS_H_

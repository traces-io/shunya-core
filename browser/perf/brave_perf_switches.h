// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_BROWSER_PERF_SHUNYA_PERF_SWITCHES_H_
#define SHUNYA_BROWSER_PERF_SHUNYA_PERF_SWITCHES_H_

namespace perf::switches {

// All switches in alphabetical order.
extern const char kEnableShunyaFeaturesForPerfTesting[];

}  // namespace perf::switches

#endif  // SHUNYA_BROWSER_PERF_SHUNYA_PERF_SWITCHES_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "base/scoped_observation.h"
#include "shunya/browser/shunya_news/shunya_news_controller_factory.h"
#include "shunya/browser/shunya_rewards/rewards_service_factory.h"
#include "shunya/browser/perf/shunya_perf_switches.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"
#include "shunya/components/shunya_news/browser/shunya_news_controller.h"
#include "shunya/components/shunya_rewards/browser/rewards_service.h"
#include "shunya/components/shunya_rewards/browser/rewards_service_observer.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/test/base/in_process_browser_test.h"
#include "content/public/test/browser_test.h"
#include "content/public/test/browser_test_utils.h"

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/browser/speedreader/speedreader_service_factory.h"
#include "shunya/components/speedreader/speedreader_service.h"
#endif

namespace {
class TestRewardsServiceObserver
    : public shunya_rewards::RewardsServiceObserver {
 public:
  void WaitForServiceInitialized(shunya_rewards::RewardsService* service) {
    if (service->IsInitialized()) {
      return;
    }

    observer_.Observe(service);
    run_loop_.Run();
  }

 private:
  void OnRewardsInitialized(shunya_rewards::RewardsService*) override {
    run_loop_.Quit();
  }

  base::RunLoop run_loop_;
  base::ScopedObservation<shunya_rewards::RewardsService,
                          shunya_rewards::RewardsServiceObserver>
      observer_{this};
};
}  // namespace

class ShunyaSpeedFeatureProcessorBrowserTest : public InProcessBrowserTest {
 protected:
  void SetUpCommandLine(base::CommandLine* command_line) override {
    InProcessBrowserTest::SetUpCommandLine(command_line);
    command_line->AppendSwitch(
        perf::switches::kEnableShunyaFeaturesForPerfTesting);
  }

#if BUILDFLAG(ENABLE_SPEEDREADER)
  bool SpeedreaderIsEnabled() {
    auto* speedreader_service =
        speedreader::SpeedreaderServiceFactory::GetForBrowserContext(
            browser()->profile());
    return speedreader_service->IsEnabledForAllSites();
  }
#endif  // BUILDFLAG(ENABLE_SPEEDREADER)

  bool ShunyaNewsAreEnabled() {
    return shunya_news::GetIsEnabled(browser()->profile()->GetPrefs());
  }

  bool HasOptedInToNotificationAds() {
    return browser()->profile()->GetPrefs()->GetBoolean(
        shunya_ads::prefs::kOptedInToNotificationAds);
  }

  void WaitForRewardsServiceInitialized() {
    auto* rewards_service = shunya_rewards::RewardsServiceFactory::GetForProfile(
        browser()->profile());
    TestRewardsServiceObserver observer;
    observer.WaitForServiceInitialized(rewards_service);
  }
};

IN_PROC_BROWSER_TEST_F(ShunyaSpeedFeatureProcessorBrowserTest, PRE_Default) {
  WaitForRewardsServiceInitialized();
}

IN_PROC_BROWSER_TEST_F(ShunyaSpeedFeatureProcessorBrowserTest, Default) {
#if BUILDFLAG(ENABLE_SPEEDREADER)
  EXPECT_TRUE(SpeedreaderIsEnabled());
#endif
  EXPECT_TRUE(HasOptedInToNotificationAds());
  EXPECT_TRUE(ShunyaNewsAreEnabled());
  WaitForRewardsServiceInitialized();
}

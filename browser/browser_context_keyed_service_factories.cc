/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/browser_context_keyed_service_factories.h"

#include "base/feature_list.h"
#include "shunya/browser/shunya_adaptive_captcha/shunya_adaptive_captcha_service_factory.h"
#include "shunya/browser/shunya_ads/ads_service_factory.h"
#include "shunya/browser/shunya_federated/shunya_federated_service_factory.h"
#include "shunya/browser/shunya_news/shunya_news_controller_factory.h"
#include "shunya/browser/shunya_rewards/rewards_service_factory.h"
#include "shunya/browser/shunya_shields/ad_block_pref_service_factory.h"
#include "shunya/browser/shunya_wallet/asset_ratio_service_factory.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_ipfs_service_factory.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_service_factory.h"
#include "shunya/browser/shunya_wallet/json_rpc_service_factory.h"
#include "shunya/browser/shunya_wallet/keyring_service_factory.h"
#include "shunya/browser/shunya_wallet/notifications/wallet_notification_service_factory.h"
#include "shunya/browser/shunya_wallet/simulation_service_factory.h"
#include "shunya/browser/shunya_wallet/swap_service_factory.h"
#include "shunya/browser/shunya_wallet/tx_service_factory.h"
#include "shunya/browser/debounce/debounce_service_factory.h"
#include "shunya/browser/ephemeral_storage/ephemeral_storage_service_factory.h"
#include "shunya/browser/ethereum_remote_client/buildflags/buildflags.h"
#include "shunya/browser/misc_metrics/page_metrics_service_factory.h"
#include "shunya/browser/ntp_background/view_counter_service_factory.h"
#include "shunya/browser/permissions/permission_lifetime_manager_factory.h"
#include "shunya/browser/profiles/shunya_renderer_updater_factory.h"
#include "shunya/browser/search_engines/search_engine_provider_service_factory.h"
#include "shunya/browser/search_engines/search_engine_tracker.h"
#include "shunya/browser/sync/shunya_sync_alerts_service_factory.h"
#include "shunya/browser/url_sanitizer/url_sanitizer_service_factory.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/shunya_perf_predictor/browser/named_third_party_registry_factory.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/commander/common/buildflags/buildflags.h"
#include "shunya/components/greaselion/browser/buildflags/buildflags.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/playlist/common/buildflags/buildflags.h"
#include "shunya/components/request_otr/common/buildflags/buildflags.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN) || BUILDFLAG(ENABLE_AI_CHAT)
#include "shunya/browser/skus/skus_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_GREASELION)
#include "shunya/browser/greaselion/greaselion_service_factory.h"
#endif

#if !BUILDFLAG(IS_ANDROID)
#include "shunya/browser/ui/bookmark/bookmark_prefs_service_factory.h"
#include "shunya/browser/ui/commands/accelerator_service_factory.h"
#include "shunya/browser/ui/tabs/features.h"
#include "shunya/browser/ui/tabs/shared_pinned_tab_service_factory.h"
#include "shunya/components/commands/common/features.h"
#else
#include "shunya/browser/shunya_shields/cookie_list_opt_in_service_factory.h"
#include "shunya/browser/shunya_shields/filter_list_service_factory.h"
#include "shunya/browser/misc_metrics/misc_android_metrics_factory.h"
#include "shunya/browser/ntp_background/android/ntp_background_images_bridge.h"
#endif

#if BUILDFLAG(ETHEREUM_REMOTE_CLIENT_ENABLED)
#include "shunya/browser/ethereum_remote_client/ethereum_remote_client_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/browser/ipfs/ipfs_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_IPFS_LOCAL_NODE)
#include "shunya/browser/shunya_wallet/shunya_wallet_auto_pin_service_factory.h"
#include "shunya/browser/shunya_wallet/shunya_wallet_pin_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/browser/tor/tor_profile_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_PLAYLIST)
#include "shunya/browser/playlist/playlist_service_factory.h"
#include "shunya/components/playlist/common/features.h"
#endif

#if BUILDFLAG(ENABLE_COMMANDER)
#include "shunya/browser/ui/commander/commander_service_factory.h"
#include "shunya/components/commander/common/features.h"
#endif

#if defined(TOOLKIT_VIEWS)
#include "shunya/browser/ui/sidebar/sidebar_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/browser/speedreader/speedreader_service_factory.h"
#endif

#if BUILDFLAG(ENABLE_REQUEST_OTR)
#include "shunya/browser/request_otr/request_otr_service_factory.h"
#endif

namespace shunya {

void EnsureBrowserContextKeyedServiceFactoriesBuilt() {
  shunya_adaptive_captcha::ShunyaAdaptiveCaptchaServiceFactory::GetInstance();
  shunya_ads::AdsServiceFactory::GetInstance();
  shunya_federated::ShunyaFederatedServiceFactory::GetInstance();
  shunya_perf_predictor::NamedThirdPartyRegistryFactory::GetInstance();
  shunya_rewards::RewardsServiceFactory::GetInstance();
  shunya_shields::AdBlockPrefServiceFactory::GetInstance();
  debounce::DebounceServiceFactory::GetInstance();
  shunya::URLSanitizerServiceFactory::GetInstance();
  ShunyaRendererUpdaterFactory::GetInstance();
  SearchEngineProviderServiceFactory::GetInstance();
  misc_metrics::PageMetricsServiceFactory::GetInstance();
#if BUILDFLAG(ENABLE_GREASELION)
  greaselion::GreaselionServiceFactory::GetInstance();
#endif
#if BUILDFLAG(ENABLE_TOR)
  TorProfileServiceFactory::GetInstance();
#endif
  SearchEngineTrackerFactory::GetInstance();
  ntp_background_images::ViewCounterServiceFactory::GetInstance();

#if !BUILDFLAG(IS_ANDROID)
  BookmarkPrefsServiceFactory::GetInstance();
#else
  shunya_shields::CookieListOptInServiceFactory::GetInstance();
  shunya_shields::FilterListServiceFactory::GetInstance();
  ntp_background_images::NTPBackgroundImagesBridgeFactory::GetInstance();
#endif

  shunya_news::ShunyaNewsControllerFactory::GetInstance();
  shunya_wallet::AssetRatioServiceFactory::GetInstance();
  shunya_wallet::KeyringServiceFactory::GetInstance();
  shunya_wallet::JsonRpcServiceFactory::GetInstance();
  shunya_wallet::SwapServiceFactory::GetInstance();
  shunya_wallet::SimulationServiceFactory::GetInstance();
#if !BUILDFLAG(IS_ANDROID)
  shunya_wallet::WalletNotificationServiceFactory::GetInstance();
#endif
  shunya_wallet::TxServiceFactory::GetInstance();
  shunya_wallet::ShunyaWalletServiceFactory::GetInstance();

#if !BUILDFLAG(IS_ANDROID)
  if (base::FeatureList::IsEnabled(commands::features::kShunyaCommands)) {
    commands::AcceleratorServiceFactory::GetInstance();
  }
#endif

#if BUILDFLAG(ENABLE_COMMANDER)
  if (base::FeatureList::IsEnabled(features::kShunyaCommander)) {
    commander::CommanderServiceFactory::GetInstance();
  }
#endif

#if BUILDFLAG(ETHEREUM_REMOTE_CLIENT_ENABLED)
  EthereumRemoteClientServiceFactory::GetInstance();
#endif

#if BUILDFLAG(ENABLE_IPFS)
  ipfs::IpfsServiceFactory::GetInstance();
#endif
  shunya_wallet::ShunyaWalletIpfsServiceFactory::GetInstance();

#if BUILDFLAG(ENABLE_IPFS_LOCAL_NODE)
  shunya_wallet::ShunyaWalletAutoPinServiceFactory::GetInstance();
  shunya_wallet::ShunyaWalletPinServiceFactory::GetInstance();
#endif

  EphemeralStorageServiceFactory::GetInstance();
  PermissionLifetimeManagerFactory::GetInstance();
#if BUILDFLAG(ENABLE_SHUNYA_VPN) || BUILDFLAG(ENABLE_AI_CHAT)
  skus::SkusServiceFactory::GetInstance();
#endif
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  shunya_vpn::ShunyaVpnServiceFactory::GetInstance();
#endif
#if BUILDFLAG(ENABLE_PLAYLIST)
  if (base::FeatureList::IsEnabled(playlist::features::kPlaylist)) {
    playlist::PlaylistServiceFactory::GetInstance();
  }
#endif
#if BUILDFLAG(ENABLE_REQUEST_OTR)
  request_otr::RequestOTRServiceFactory::GetInstance();
#endif

  ShunyaSyncAlertsServiceFactory::GetInstance();

#if !BUILDFLAG(IS_ANDROID)
  if (base::FeatureList::IsEnabled(tabs::features::kShunyaSharedPinnedTabs)) {
    SharedPinnedTabServiceFactory::GetInstance();
  }
#endif

#if defined(TOOLKIT_VIEWS)
  sidebar::SidebarServiceFactory::GetInstance();
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
  speedreader::SpeedreaderServiceFactory::GetInstance();
#endif

#if BUILDFLAG(IS_ANDROID)
  misc_metrics::MiscAndroidMetricsFactory::GetInstance();
#endif
}

}  // namespace shunya

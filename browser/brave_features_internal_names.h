/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_FEATURES_INTERNAL_NAMES_H_
#define SHUNYA_BROWSER_SHUNYA_FEATURES_INTERNAL_NAMES_H_

#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "build/build_config.h"

constexpr char kPlaylistFeatureInternalName[] = "playlist";
constexpr char kPlaylistFakeUAFeatureInternalName[] = "playlist-fake-ua";
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
constexpr char kShunyaVPNFeatureInternalName[] = "shunya-vpn";
#if BUILDFLAG(IS_WIN)
constexpr char kShunyaVPNDnsFeatureInternalName[] = "shunya-vpn-dns";
#endif
#endif

#endif  // SHUNYA_BROWSER_SHUNYA_FEATURES_INTERNAL_NAMES_H_

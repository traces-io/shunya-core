/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SYNC_SHUNYA_SYNC_ALERTS_SERVICE_FACTORY_H_
#define SHUNYA_BROWSER_SYNC_SHUNYA_SYNC_ALERTS_SERVICE_FACTORY_H_

#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

class ShunyaSyncAlertsService;

class ShunyaSyncAlertsServiceFactory : public BrowserContextKeyedServiceFactory {
 public:
  ShunyaSyncAlertsServiceFactory(const ShunyaSyncAlertsServiceFactory&) = delete;
  ShunyaSyncAlertsServiceFactory& operator=(
      const ShunyaSyncAlertsServiceFactory&) = delete;

  static ShunyaSyncAlertsService* GetForBrowserContext(
      content::BrowserContext* context);

  static ShunyaSyncAlertsServiceFactory* GetInstance();

 private:
  friend base::NoDestructor<ShunyaSyncAlertsServiceFactory>;

  ShunyaSyncAlertsServiceFactory();
  ~ShunyaSyncAlertsServiceFactory() override;

  // BrowserContextKeyedServiceFactory:
  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* profile) const override;
  content::BrowserContext* GetBrowserContextToUse(
      content::BrowserContext* context) const override;
  bool ServiceIsCreatedWithBrowserContext() const override;
};

#endif  // SHUNYA_BROWSER_SYNC_SHUNYA_SYNC_ALERTS_SERVICE_FACTORY_H_

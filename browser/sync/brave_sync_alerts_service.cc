/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/sync/shunya_sync_alerts_service.h"

#include "shunya/browser/infobars/shunya_sync_account_deleted_infobar_delegate.h"
#include "shunya/components/sync/service/shunya_sync_service_impl.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/sync/sync_service_factory.h"
#include "chrome/browser/ui/browser_finder.h"
#include "components/infobars/content/content_infobar_manager.h"

#if BUILDFLAG(IS_ANDROID)
#include "base/android/jni_android.h"
#include "shunya/build/android/jni_headers/ShunyaSyncAccountDeletedInformer_jni.h"
#else
#include "chrome/browser/ui/browser.h"
#endif

using syncer::ShunyaSyncServiceImpl;

ShunyaSyncAlertsService::ShunyaSyncAlertsService(Profile* profile)
    : profile_(profile) {
  DCHECK(SyncServiceFactory::IsSyncAllowed(profile));

  if (SyncServiceFactory::IsSyncAllowed(profile)) {
    ShunyaSyncServiceImpl* service = static_cast<ShunyaSyncServiceImpl*>(
        SyncServiceFactory::GetForProfile(profile_));

    if (service) {
      DCHECK(!sync_service_observer_.IsObservingSource(service));
      sync_service_observer_.AddObservation(service);
    }
  }
}

ShunyaSyncAlertsService::~ShunyaSyncAlertsService() {}

void ShunyaSyncAlertsService::OnStateChanged(syncer::SyncService* service) {
  shunya_sync::Prefs shunya_sync_prefs(profile_->GetPrefs());
  if (!shunya_sync_prefs.IsSyncAccountDeletedNoticePending()) {
    return;
  }

  ShowInfobar();
}

void ShunyaSyncAlertsService::OnSyncShutdown(syncer::SyncService* sync_service) {
  if (sync_service_observer_.IsObservingSource(sync_service)) {
    sync_service_observer_.RemoveObservation(sync_service);
  }
}

void ShunyaSyncAlertsService::ShowInfobar() {
#if BUILDFLAG(IS_ANDROID)
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaSyncAccountDeletedInformer_show(env);
#else
  Browser* browser = chrome::FindLastActive();
  if (browser) {
    content::WebContents* active_web_contents =
        browser->tab_strip_model()->GetActiveWebContents();
    if (active_web_contents) {
      ShunyaSyncAccountDeletedInfoBarDelegate::Create(active_web_contents,
                                                     profile_, browser);
    }
  }
#endif
}

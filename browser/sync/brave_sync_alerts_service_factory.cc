/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/sync/shunya_sync_alerts_service_factory.h"

#include "base/no_destructor.h"
#include "shunya/browser/sync/shunya_sync_alerts_service.h"
#include "chrome/browser/profiles/incognito_helpers.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/sync/sync_service_factory.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/pref_registry/pref_registry_syncable.h"

// static
ShunyaSyncAlertsService* ShunyaSyncAlertsServiceFactory::GetForBrowserContext(
    content::BrowserContext* context) {
  return static_cast<ShunyaSyncAlertsService*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

// static
ShunyaSyncAlertsServiceFactory* ShunyaSyncAlertsServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaSyncAlertsServiceFactory> instance;
  return instance.get();
}

ShunyaSyncAlertsServiceFactory::ShunyaSyncAlertsServiceFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaSyncAlertsService",
          BrowserContextDependencyManager::GetInstance()) {}

ShunyaSyncAlertsServiceFactory::~ShunyaSyncAlertsServiceFactory() {}

KeyedService* ShunyaSyncAlertsServiceFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  return new ShunyaSyncAlertsService(Profile::FromBrowserContext(context));
}

content::BrowserContext* ShunyaSyncAlertsServiceFactory::GetBrowserContextToUse(
    content::BrowserContext* context) const {
  return chrome::GetBrowserContextRedirectedInIncognito(context);
}

bool ShunyaSyncAlertsServiceFactory::ServiceIsCreatedWithBrowserContext() const {
  return true;
}

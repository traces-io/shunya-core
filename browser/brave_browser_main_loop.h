/* Copyright 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_BROWSER_MAIN_LOOP_H_
#define SHUNYA_BROWSER_SHUNYA_BROWSER_MAIN_LOOP_H_

#include "content/browser/browser_main_loop.h"

namespace shunya {

class ShunyaBrowserMainLoop : public content::BrowserMainLoop {
 public:
  using BrowserMainLoop::BrowserMainLoop;

  ShunyaBrowserMainLoop(const ShunyaBrowserMainLoop&) = delete;
  ShunyaBrowserMainLoop& operator=(const ShunyaBrowserMainLoop&) = delete;
  ~ShunyaBrowserMainLoop() override = default;

  void PreShutdown() override;
};

}  // namespace shunya

#endif  // SHUNYA_BROWSER_SHUNYA_BROWSER_MAIN_LOOP_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_BROWSER_PROCESS_IMPL_H_
#define SHUNYA_BROWSER_SHUNYA_BROWSER_PROCESS_IMPL_H_

#include <memory>

#include "base/memory/ref_counted.h"
#include "shunya/browser/shunya_browser_process.h"
#include "shunya/components/shunya_component_updater/browser/shunya_component.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/greaselion/browser/buildflags/buildflags.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/components/tor/shunya_tor_pluggable_transport_updater.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "shunya/components/url_sanitizer/browser/url_sanitizer_component_installer.h"
#include "build/build_config.h"
#include "chrome/browser/browser_process_impl.h"
#include "extensions/buildflags/buildflags.h"

namespace shunya {
class ShunyaReferralsService;
class ShunyaFarblingService;
}  // namespace shunya

namespace shunya_component_updater {
class LocalDataFilesService;
}  // namespace shunya_component_updater

namespace shunya_shields {
class AdBlockService;
class HTTPSEverywhereService;
}  // namespace shunya_shields

namespace https_upgrade_exceptions {
class HttpsUpgradeExceptionsService;
}  // namespace https_upgrade_exceptions

namespace localhost_permission {
class LocalhostPermissionComponent;
}  // namespace localhost_permission

namespace shunya_stats {
class ShunyaStatsUpdater;
}  // namespace shunya_stats

namespace greaselion {
#if BUILDFLAG(ENABLE_GREASELION)
class GreaselionDownloadService;
#endif
}  // namespace greaselion

namespace debounce {
class DebounceComponentInstaller;
}  // namespace debounce

namespace misc_metrics {
class ProcessMiscMetrics;
}  // namespace misc_metrics

namespace request_otr {
#if BUILDFLAG(ENABLE_REQUEST_OTR)
class RequestOTRComponentInstallerPolicy;
#endif
}  // namespace request_otr

namespace ntp_background_images {
class NTPBackgroundImagesService;
}  // namespace ntp_background_images

namespace p3a {
class HistogramsShunyaizer;
class P3AService;
}  // namespace p3a

namespace tor {
class ShunyaTorClientUpdater;
class ShunyaTorPluggableTransportUpdater;
}  // namespace tor

namespace ipfs {
class ShunyaIpfsClientUpdater;
}

namespace speedreader {
class SpeedreaderRewriterService;
}

namespace shunya_ads {
class ShunyaStatsHelper;
class ResourceComponent;
}  // namespace shunya_ads

class ShunyaBrowserProcessImpl : public ShunyaBrowserProcess,
                                public BrowserProcessImpl {
 public:
  explicit ShunyaBrowserProcessImpl(StartupData* startup_data);
  ShunyaBrowserProcessImpl(const ShunyaBrowserProcessImpl&) = delete;
  ShunyaBrowserProcessImpl& operator=(const ShunyaBrowserProcessImpl&) = delete;
  ~ShunyaBrowserProcessImpl() override;

  // BrowserProcess implementation.

  ProfileManager* profile_manager() override;
  NotificationPlatformBridge* notification_platform_bridge() override;

  // ShunyaBrowserProcess implementation.

  void StartShunyaServices() override;
  shunya_shields::AdBlockService* ad_block_service() override;
  https_upgrade_exceptions::HttpsUpgradeExceptionsService*
  https_upgrade_exceptions_service() override;
  localhost_permission::LocalhostPermissionComponent*
  localhost_permission_component() override;
#if BUILDFLAG(ENABLE_GREASELION)
  greaselion::GreaselionDownloadService* greaselion_download_service() override;
#endif
  debounce::DebounceComponentInstaller* debounce_component_installer() override;
#if BUILDFLAG(ENABLE_REQUEST_OTR)
  request_otr::RequestOTRComponentInstallerPolicy*
  request_otr_component_installer() override;
#endif
  shunya::URLSanitizerComponentInstaller* URLSanitizerComponentInstaller()
      override;
  shunya_shields::HTTPSEverywhereService* https_everywhere_service() override;
  shunya_component_updater::LocalDataFilesService* local_data_files_service()
      override;
#if BUILDFLAG(ENABLE_TOR)
  tor::ShunyaTorClientUpdater* tor_client_updater() override;
  tor::ShunyaTorPluggableTransportUpdater* tor_pluggable_transport_updater()
      override;
#endif
#if BUILDFLAG(ENABLE_IPFS)
  ipfs::ShunyaIpfsClientUpdater* ipfs_client_updater() override;
#endif
  p3a::P3AService* p3a_service() override;
  shunya::ShunyaReferralsService* shunya_referrals_service() override;
  shunya_stats::ShunyaStatsUpdater* shunya_stats_updater() override;
  shunya_ads::ShunyaStatsHelper* ads_shunya_stats_helper() override;
  ntp_background_images::NTPBackgroundImagesService*
  ntp_background_images_service() override;
  shunya_ads::ResourceComponent* resource_component() override;
#if BUILDFLAG(ENABLE_SPEEDREADER)
  speedreader::SpeedreaderRewriterService* speedreader_rewriter_service()
      override;
#endif
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  shunya_vpn::ShunyaVPNOSConnectionAPI* shunya_vpn_os_connection_api() override;
#endif
  shunya::ShunyaFarblingService* shunya_farbling_service() override;
  misc_metrics::ProcessMiscMetrics* process_misc_metrics() override;

 private:
  // BrowserProcessImpl overrides:
  void Init() override;
#if !BUILDFLAG(IS_ANDROID)
  void StartTearDown() override;
  void PostDestroyThreads() override;
#endif

  void CreateProfileManager();

#if BUILDFLAG(ENABLE_TOR)
  void OnTorEnabledChanged();
#endif

  void UpdateShunyaDarkMode();
  void OnShunyaDarkModeChanged();

  void InitShunyaStatsHelper();

  shunya_component_updater::ShunyaComponent::Delegate*
  shunya_component_updater_delegate();

  // local_data_files_service_ should always be first because it needs
  // to be destroyed last
  std::unique_ptr<shunya_component_updater::LocalDataFilesService>
      local_data_files_service_;
  std::unique_ptr<shunya_component_updater::ShunyaComponent::Delegate>
      shunya_component_updater_delegate_;
  std::unique_ptr<shunya_shields::AdBlockService> ad_block_service_;
  std::unique_ptr<https_upgrade_exceptions::HttpsUpgradeExceptionsService>
      https_upgrade_exceptions_service_;
  std::unique_ptr<localhost_permission::LocalhostPermissionComponent>
      localhost_permission_component_;
#if BUILDFLAG(ENABLE_GREASELION)
  std::unique_ptr<greaselion::GreaselionDownloadService>
      greaselion_download_service_;
#endif
  std::unique_ptr<debounce::DebounceComponentInstaller>
      debounce_component_installer_;
#if BUILDFLAG(ENABLE_REQUEST_OTR)
  std::unique_ptr<request_otr::RequestOTRComponentInstallerPolicy>
      request_otr_component_installer_;
#endif
  std::unique_ptr<shunya::URLSanitizerComponentInstaller>
      url_sanitizer_component_installer_;
  bool created_https_everywhere_service_ = false;
  std::unique_ptr<shunya_shields::HTTPSEverywhereService>
      https_everywhere_service_;
  std::unique_ptr<shunya_stats::ShunyaStatsUpdater> shunya_stats_updater_;
  std::unique_ptr<shunya::ShunyaReferralsService> shunya_referrals_service_;
#if BUILDFLAG(ENABLE_TOR)
  std::unique_ptr<tor::ShunyaTorClientUpdater> tor_client_updater_;
  std::unique_ptr<tor::ShunyaTorPluggableTransportUpdater>
      tor_pluggable_transport_updater_;
#endif
#if BUILDFLAG(ENABLE_IPFS)
  std::unique_ptr<ipfs::ShunyaIpfsClientUpdater> ipfs_client_updater_;
#endif
  scoped_refptr<p3a::P3AService> p3a_service_;
  scoped_refptr<p3a::HistogramsShunyaizer> histogram_shunyaizer_;
  std::unique_ptr<ntp_background_images::NTPBackgroundImagesService>
      ntp_background_images_service_;
  std::unique_ptr<shunya_ads::ResourceComponent> resource_component_;

#if BUILDFLAG(ENABLE_SPEEDREADER)
  std::unique_ptr<speedreader::SpeedreaderRewriterService>
      speedreader_rewriter_service_;
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  std::unique_ptr<shunya_vpn::ShunyaVPNOSConnectionAPI>
      shunya_vpn_os_connection_api_;
#endif

  std::unique_ptr<shunya::ShunyaFarblingService> shunya_farbling_service_;
  std::unique_ptr<misc_metrics::ProcessMiscMetrics> process_misc_metrics_;
  std::unique_ptr<shunya_ads::ShunyaStatsHelper> shunya_stats_helper_;

  SEQUENCE_CHECKER(sequence_checker_);
};

#endif  // SHUNYA_BROWSER_SHUNYA_BROWSER_PROCESS_IMPL_H_

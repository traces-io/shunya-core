/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_adaptive_captcha/shunya_adaptive_captcha_service_factory.h"

#include <memory>
#include <string>
#include <utility>

#include "base/memory/raw_ptr.h"
#include "base/no_destructor.h"
#if !BUILDFLAG(IS_ANDROID)
#include "shunya/browser/ui/shunya_rewards/rewards_panel_coordinator.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_finder.h"
#endif
#include "shunya/browser/shunya_rewards/rewards_service_factory.h"
#include "shunya/browser/profiles/profile_util.h"
#include "shunya/components/shunya_adaptive_captcha/shunya_adaptive_captcha_service.h"
#include "chrome/browser/profiles/profile.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/storage_partition.h"
#include "services/network/public/cpp/shared_url_loader_factory.h"

namespace {

class CaptchaDelegate
    : public shunya_adaptive_captcha::ShunyaAdaptiveCaptchaDelegate {
 public:
  explicit CaptchaDelegate(content::BrowserContext* context)
      : context_(context) {}

  bool ShowScheduledCaptcha(const std::string& payment_id,
                            const std::string& captcha_id) override {
#if BUILDFLAG(IS_ANDROID)
    return true;
#else
    // Because this is triggered from the adaptive captcha tooltip, this call
    // isn't associated with any particular `Browser` instance and we can use
    // the last active browser for this profile.
    auto* profile = Profile::FromBrowserContext(context_);
    auto* browser = chrome::FindTabbedBrowser(profile, false);
    if (!browser) {
      return false;
    }
    auto* coordinator =
        shunya_rewards::RewardsPanelCoordinator::FromBrowser(browser);
    if (!coordinator) {
      return false;
    }
    return coordinator->ShowAdaptiveCaptcha();
#endif
  }

 private:
  raw_ptr<content::BrowserContext> context_ = nullptr;
};

}  // namespace

namespace shunya_adaptive_captcha {

// static
ShunyaAdaptiveCaptchaServiceFactory*
ShunyaAdaptiveCaptchaServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaAdaptiveCaptchaServiceFactory> instance;
  return instance.get();
}

// static
ShunyaAdaptiveCaptchaService* ShunyaAdaptiveCaptchaServiceFactory::GetForProfile(
    Profile* profile) {
  if (!shunya::IsRegularProfile(profile)) {
    return nullptr;
  }

  return static_cast<ShunyaAdaptiveCaptchaService*>(
      GetInstance()->GetServiceForBrowserContext(profile, true));
}

ShunyaAdaptiveCaptchaServiceFactory::ShunyaAdaptiveCaptchaServiceFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaAdaptiveCaptchaService",
          BrowserContextDependencyManager::GetInstance()) {
  DependsOn(shunya_rewards::RewardsServiceFactory::GetInstance());
}

ShunyaAdaptiveCaptchaServiceFactory::~ShunyaAdaptiveCaptchaServiceFactory() =
    default;

KeyedService* ShunyaAdaptiveCaptchaServiceFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  auto url_loader_factory = context->GetDefaultStoragePartition()
                                ->GetURLLoaderFactoryForBrowserProcess();
  return new ShunyaAdaptiveCaptchaService(
      user_prefs::UserPrefs::Get(context), std::move(url_loader_factory),
      shunya_rewards::RewardsServiceFactory::GetForProfile(
          Profile::FromBrowserContext(context)),
      std::make_unique<CaptchaDelegate>(context));
}

}  // namespace shunya_adaptive_captcha

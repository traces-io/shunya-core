/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_ADAPTIVE_CAPTCHA_SHUNYA_ADAPTIVE_CAPTCHA_SERVICE_FACTORY_H_
#define SHUNYA_BROWSER_SHUNYA_ADAPTIVE_CAPTCHA_SHUNYA_ADAPTIVE_CAPTCHA_SERVICE_FACTORY_H_

#include "shunya/components/shunya_adaptive_captcha/shunya_adaptive_captcha_delegate.h"
#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

class Profile;

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace shunya_adaptive_captcha {

class ShunyaAdaptiveCaptchaService;

class ShunyaAdaptiveCaptchaServiceFactory
    : public BrowserContextKeyedServiceFactory {
 public:
  static ShunyaAdaptiveCaptchaService* GetForProfile(Profile* profile);
  static ShunyaAdaptiveCaptchaServiceFactory* GetInstance();

 private:
  friend base::NoDestructor<ShunyaAdaptiveCaptchaServiceFactory>;

  ShunyaAdaptiveCaptchaServiceFactory();
  ~ShunyaAdaptiveCaptchaServiceFactory() override;

  ShunyaAdaptiveCaptchaServiceFactory(
      const ShunyaAdaptiveCaptchaServiceFactory&) = delete;
  ShunyaAdaptiveCaptchaServiceFactory& operator=(
      const ShunyaAdaptiveCaptchaServiceFactory&) = delete;

  // BrowserContextKeyedServiceFactory overrides:
  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
};

}  // namespace shunya_adaptive_captcha

#endif  // SHUNYA_BROWSER_SHUNYA_ADAPTIVE_CAPTCHA_SHUNYA_ADAPTIVE_CAPTCHA_SERVICE_FACTORY_H_

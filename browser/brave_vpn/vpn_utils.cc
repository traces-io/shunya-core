/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_vpn/vpn_utils.h"

#include "shunya/browser/profiles/profile_util.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "build/build_config.h"
#include "components/user_prefs/user_prefs.h"

namespace shunya_vpn {

bool IsAllowedForContext(content::BrowserContext* context) {
  return shunya::IsRegularProfile(context) &&
         shunya_vpn::IsShunyaVPNFeatureEnabled();
}

bool IsShunyaVPNEnabled(content::BrowserContext* context) {
  // TODO(simonhong): Can we use this check for android?
  // For now, vpn is disabled by default on desktop but not sure on
  // android.
#if BUILDFLAG(IS_WIN) || BUILDFLAG(IS_MAC)
  return shunya_vpn::IsShunyaVPNEnabled(user_prefs::UserPrefs::Get(context)) &&
         IsAllowedForContext(context);
#else
  return shunya::IsRegularProfile(context);
#endif
}

}  // namespace shunya_vpn

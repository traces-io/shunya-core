/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_OBSERVER_SERVICE_WIN_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_OBSERVER_SERVICE_WIN_H_

#include <utility>

#include "shunya/components/shunya_vpn/browser/shunya_vpn_service_observer.h"
#include "components/keyed_service/core/keyed_service.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_vpn {

class ShunyaVpnWireguardObserverService
    : public shunya_vpn::ShunyaVPNServiceObserver,
      public KeyedService {
 public:
  ShunyaVpnWireguardObserverService();
  ~ShunyaVpnWireguardObserverService() override;
  ShunyaVpnWireguardObserverService(const ShunyaVpnWireguardObserverService&) =
      delete;
  ShunyaVpnWireguardObserverService operator=(
      const ShunyaVpnWireguardObserverService&) = delete;

  // shunya_vpn::ShunyaVPNServiceObserver
  void OnConnectionStateChanged(
      shunya_vpn::mojom::ConnectionState state) override;

 private:
  friend class ShunyaVpnWireguardObserverServiceUnitTest;

  void SetDialogCallbackForTesting(base::RepeatingClosure callback) {
    dialog_callback_ = std::move(callback);
  }

  void SetFallbackForTesting(bool should_fallback_for_testing) {
    should_fallback_for_testing_ = should_fallback_for_testing;
  }

  void ShowFallbackDialog();
  bool ShouldShowFallbackDialog() const;

  absl::optional<bool> should_fallback_for_testing_;
  base::RepeatingClosure dialog_callback_;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_OBSERVER_SERVICE_WIN_H_

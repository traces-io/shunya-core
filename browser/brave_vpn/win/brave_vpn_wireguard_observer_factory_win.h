/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_OBSERVER_FACTORY_WIN_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_OBSERVER_FACTORY_WIN_H_

#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace content {
class BrowserContext;
}  // namespace content

namespace shunya_vpn {

class ShunyaVpnWireguardObserverService;

class ShunyaVpnWireguardObserverFactory
    : public BrowserContextKeyedServiceFactory {
 public:
  ShunyaVpnWireguardObserverFactory(const ShunyaVpnWireguardObserverFactory&) =
      delete;
  ShunyaVpnWireguardObserverFactory& operator=(
      const ShunyaVpnWireguardObserverFactory&) = delete;

  static ShunyaVpnWireguardObserverFactory* GetInstance();
  static ShunyaVpnWireguardObserverService* GetServiceForContext(
      content::BrowserContext* context);

 private:
  friend base::NoDestructor<ShunyaVpnWireguardObserverFactory>;

  ShunyaVpnWireguardObserverFactory();
  ~ShunyaVpnWireguardObserverFactory() override;

  // BrowserContextKeyedServiceFactory overrides:
  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_OBSERVER_FACTORY_WIN_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_SERVICE_SHUNYA_WIREGUARD_MANAGER_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_SERVICE_SHUNYA_WIREGUARD_MANAGER_H_

#include <wrl/implements.h>
#include <wrl/module.h>

#include "base/win/windows_types.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/shunya_wireguard_manager_idl.h"

namespace shunya_vpn {

class ShunyaWireguardManager
    : public Microsoft::WRL::RuntimeClass<
          Microsoft::WRL::RuntimeClassFlags<Microsoft::WRL::ClassicCom>,
          IShunyaVpnWireguardManager> {
 public:
  ShunyaWireguardManager() = default;

  ShunyaWireguardManager(const ShunyaWireguardManager&) = delete;
  ShunyaWireguardManager& operator=(const ShunyaWireguardManager&) = delete;

  IFACEMETHODIMP EnableVpn(const wchar_t* config, DWORD* last_error) override;
  IFACEMETHODIMP DisableVpn(DWORD* last_error) override;
  IFACEMETHODIMP GenerateKeypair(BSTR* public_key,
                                 BSTR* private_key,
                                 DWORD* last_error) override;

 private:
  ~ShunyaWireguardManager() override = default;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_SERVICE_SHUNYA_WIREGUARD_MANAGER_H_

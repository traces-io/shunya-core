/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_SERVICE_INSTALL_UTILS_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_SERVICE_INSTALL_UTILS_H_

#include <string>

namespace shunya_vpn {

bool ConfigureShunyaWireguardService(const std::wstring& service_name);
bool InstallShunyaWireguardService();
bool UninstallShunyaWireguardService();

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_SERVICE_INSTALL_UTILS_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_vpn/win/shunya_vpn_wireguard_service/shunya_wireguard_service_crash_reporter_client.h"

#include <memory>
#include <string>

#include "base/debug/leak_annotations.h"
#include "base/file_version_info.h"
#include "base/files/file_path.h"
#include "base/notreached.h"
#include "base/strings/string_util.h"
#include "base/strings/utf_string_conversions.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/service_details.h"
#include "chrome/install_static/install_modes.h"
#include "chrome/install_static/install_util.h"
#include "chrome/install_static/product_install_details.h"
#include "chrome/install_static/user_data_dir.h"
#include "components/crash/core/app/crash_switches.h"
#include "components/crash/core/app/crashpad.h"
#include "components/version_info/channel.h"

namespace {
// Split into two places to avoid patching:
// chromium_src\components\crash\core\app\crashpad.cc
// Need keep it in sync.
constexpr char kShunyaWireguardProcessType[] = "shunya-vpn-wireguard-service";

// The service starts under sytem user so we save crashes to
// %PROGRAMDATA%\ShunyaSoftware\{service name}\Crashpad
base::FilePath GetShunyaWireguardServiceProfileDir() {
  auto program_data = install_static::GetEnvironmentString("PROGRAMDATA");
  if (program_data.empty()) {
    return base::FilePath();
  }
  return base::FilePath(base::UTF8ToWide(program_data))
      .Append(install_static::kCompanyPathName)
      .Append(shunya_vpn::GetShunyaVpnWireguardServiceName());
}

}  // namespace

ShunyaWireguardCrashReporterClient::ShunyaWireguardCrashReporterClient() =
    default;

ShunyaWireguardCrashReporterClient::~ShunyaWireguardCrashReporterClient() =
    default;

// static
void ShunyaWireguardCrashReporterClient::InitializeCrashReportingForProcess(
    const std::string& process_type) {
  static ShunyaWireguardCrashReporterClient* instance = nullptr;
  if (instance) {
    return;
  }

  instance = new ShunyaWireguardCrashReporterClient();
  ANNOTATE_LEAKING_OBJECT_PTR(instance);
  // Don't set up Crashpad crash reporting in the Crashpad handler itself, nor
  // in the fallback crash handler for the Crashpad handler process.
  if (process_type == crash_reporter::switches::kCrashpadHandler) {
    return;
  }
  install_static::InitializeProductDetailsForPrimaryModule();
  crash_reporter::SetCrashReporterClient(instance);

  crash_reporter::InitializeCrashpadWithEmbeddedHandler(
      true, kShunyaWireguardProcessType,
      install_static::WideToUTF8(GetShunyaWireguardServiceProfileDir().value()),
      base::FilePath());
}

bool ShunyaWireguardCrashReporterClient::ShouldCreatePipeName(
    const std::wstring& process_type) {
  return false;
}

bool ShunyaWireguardCrashReporterClient::GetAlternativeCrashDumpLocation(
    std::wstring* crash_dir) {
  return false;
}

void ShunyaWireguardCrashReporterClient::GetProductNameAndVersion(
    const std::wstring& exe_path,
    std::wstring* product_name,
    std::wstring* version,
    std::wstring* special_build,
    std::wstring* channel_name) {
  *product_name = shunya_vpn::GetShunyaVpnWireguardServiceName();
  std::unique_ptr<FileVersionInfo> version_info(
      FileVersionInfo::CreateFileVersionInfo(base::FilePath(exe_path)));
  if (version_info) {
    *version = base::AsWString(version_info->product_version());
    *special_build = base::AsWString(version_info->special_build());
  } else {
    *version = L"0.0.0.0-devel";
    *special_build = std::wstring();
  }

  *channel_name =
      install_static::GetChromeChannelName(/*with_extended_stable=*/true);
}

bool ShunyaWireguardCrashReporterClient::ShouldShowRestartDialog(
    std::wstring* title,
    std::wstring* message,
    bool* is_rtl_locale) {
  // There is no UX associated with shunya_vpn_wireguard_service, so no dialog
  // should be shown.
  return false;
}

bool ShunyaWireguardCrashReporterClient::AboutToRestart() {
  // The shunya_vpn_wireguard_service should never be restarted after a crash.
  return false;
}

bool ShunyaWireguardCrashReporterClient::GetIsPerUserInstall() {
  return !install_static::IsSystemInstall();
}

bool ShunyaWireguardCrashReporterClient::GetShouldDumpLargerDumps() {
  // Use large dumps for all but the stable channel.
  return install_static::GetChromeChannel() != version_info::Channel::STABLE;
}

int ShunyaWireguardCrashReporterClient::GetResultCodeRespawnFailed() {
  // The restart dialog is never shown.
  NOTREACHED();
  return 0;
}

bool ShunyaWireguardCrashReporterClient::GetCrashDumpLocation(
    std::wstring* crash_dir) {
  auto profile_dir = GetShunyaWireguardServiceProfileDir();
  *crash_dir = (profile_dir.Append(L"Crashpad")).value();
  return !profile_dir.empty();
}

bool ShunyaWireguardCrashReporterClient::GetCrashMetricsLocation(
    std::wstring* metrics_dir) {
  *metrics_dir = GetShunyaWireguardServiceProfileDir().value();
  return !metrics_dir->empty();
}

bool ShunyaWireguardCrashReporterClient::IsRunningUnattended() {
  return false;
}

bool ShunyaWireguardCrashReporterClient::GetCollectStatsConsent() {
  return install_static::GetCollectStatsConsent();
}

bool ShunyaWireguardCrashReporterClient::GetCollectStatsInSample() {
  return install_static::GetCollectStatsInSample();
}

bool ShunyaWireguardCrashReporterClient::ReportingIsEnforcedByPolicy(
    bool* enabled) {
  return install_static::ReportingIsEnforcedByPolicy(enabled);
}

bool ShunyaWireguardCrashReporterClient::ShouldMonitorCrashHandlerExpensively() {
  // The expensive mechanism dedicates a process to be crashpad_handler's own
  // crashpad_handler.
  return false;
}

bool ShunyaWireguardCrashReporterClient::EnableBreakpadForProcess(
    const std::string& process_type) {
  // This is not used by Crashpad (at least on Windows).
  NOTREACHED();
  return true;
}

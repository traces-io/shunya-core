/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_RESOURCES_RESOURCE_LOADER_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_RESOURCES_RESOURCE_LOADER_H_

#include <string>

namespace base {
class FilePath;
}  // namespace base

namespace shunya_vpn {

void LoadLocaleResources();

base::FilePath FindPakFilePath(const base::FilePath& assets_path,
                               const std::string& locale);

}  //  namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_RESOURCES_RESOURCE_LOADER_H_

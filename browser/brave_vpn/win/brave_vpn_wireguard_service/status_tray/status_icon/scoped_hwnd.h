/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_STATUS_TRAY_STATUS_ICON_SCOPED_HWND_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_STATUS_TRAY_STATUS_ICON_SCOPED_HWND_H_

#include "base/win/windows_types.h"

#include "base/scoped_generic.h"

namespace shunya_vpn {

namespace win {

namespace internal {

template <class T>
struct ScopedHWNDObjectTraits {
  static T InvalidValue() { return nullptr; }
  static void Free(T object) { DestroyWindow(object); }
};

}  // namespace internal

template <class T>
using ScopedHWNDObject =
    base::ScopedGeneric<T, internal::ScopedHWNDObjectTraits<T>>;

typedef ScopedHWNDObject<HWND> ScopedHWND;

}  // namespace win
}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_STATUS_TRAY_STATUS_ICON_SCOPED_HWND_H_

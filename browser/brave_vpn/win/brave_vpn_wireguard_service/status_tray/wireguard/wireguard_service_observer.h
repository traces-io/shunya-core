/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_STATUS_TRAY_WIREGUARD_WIREGUARD_SERVICE_OBSERVER_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_STATUS_TRAY_WIREGUARD_WIREGUARD_SERVICE_OBSERVER_H_

#include <memory>
#include <string>

#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_vpn/common/win/shunya_windows_service_watcher.h"

namespace shunya_vpn {
namespace wireguard {

class WireguardServiceObserver {
 public:
  WireguardServiceObserver();

  WireguardServiceObserver(const WireguardServiceObserver&) = delete;
  WireguardServiceObserver& operator=(const WireguardServiceObserver&) = delete;

  virtual ~WireguardServiceObserver();

  virtual void OnWireguardServiceStateChanged(int mask) = 0;

  void SubscribeForWireguardNotifications(const std::wstring& name);
  bool IsWireguardObserverActive() const;
  void StopWireguardObserver();

 private:
  std::unique_ptr<shunya::ServiceWatcher> service_watcher_;
  base::WeakPtrFactory<WireguardServiceObserver> weak_factory_{this};
};

}  // namespace wireguard
}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_WIN_SHUNYA_VPN_WIREGUARD_SERVICE_STATUS_TRAY_WIREGUARD_WIREGUARD_SERVICE_OBSERVER_H_

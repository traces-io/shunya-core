/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_vpn/win/shunya_vpn_wireguard_observer_service_win.h"

#include "shunya/browser/ui/browser_dialogs.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/storage_utils.h"

namespace shunya_vpn {

ShunyaVpnWireguardObserverService::ShunyaVpnWireguardObserverService() = default;

ShunyaVpnWireguardObserverService::~ShunyaVpnWireguardObserverService() = default;

void ShunyaVpnWireguardObserverService::ShowFallbackDialog() {
  if (dialog_callback_) {
    dialog_callback_.Run();
    return;
  }
  shunya::ShowShunyaVpnIKEv2FallbackDialog();
}

void ShunyaVpnWireguardObserverService::OnConnectionStateChanged(
    shunya_vpn::mojom::ConnectionState state) {
  if (state == shunya_vpn::mojom::ConnectionState::DISCONNECTED ||
      state == shunya_vpn::mojom::ConnectionState::CONNECT_FAILED) {
    if (ShouldShowFallbackDialog()) {
      ShowFallbackDialog();
    }
  }
}

bool ShunyaVpnWireguardObserverService::ShouldShowFallbackDialog() const {
  if (should_fallback_for_testing_.has_value()) {
    return should_fallback_for_testing_.value();
  }

  return ShouldFallbackToIKEv2();
}

}  // namespace shunya_vpn

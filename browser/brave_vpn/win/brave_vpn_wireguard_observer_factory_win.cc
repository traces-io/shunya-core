/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_vpn/win/shunya_vpn_wireguard_observer_factory_win.h"

#include "base/feature_list.h"
#include "base/no_destructor.h"
#include "shunya/browser/shunya_vpn/vpn_utils.h"
#include "shunya/browser/shunya_vpn/win/shunya_vpn_wireguard_observer_service_win.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "chrome/browser/browser_process.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "content/public/browser/browser_context.h"

namespace shunya_vpn {

// static
ShunyaVpnWireguardObserverFactory*
ShunyaVpnWireguardObserverFactory::GetInstance() {
  static base::NoDestructor<ShunyaVpnWireguardObserverFactory> instance;
  return instance.get();
}

ShunyaVpnWireguardObserverFactory::~ShunyaVpnWireguardObserverFactory() = default;

ShunyaVpnWireguardObserverFactory::ShunyaVpnWireguardObserverFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaVpnWireguardObserverService",
          BrowserContextDependencyManager::GetInstance()) {}

KeyedService* ShunyaVpnWireguardObserverFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  return new ShunyaVpnWireguardObserverService();
}

// static
ShunyaVpnWireguardObserverService*
ShunyaVpnWireguardObserverFactory::GetServiceForContext(
    content::BrowserContext* context) {
  DCHECK(
      shunya_vpn::IsShunyaVPNWireguardEnabled(g_browser_process->local_state()));
  DCHECK(shunya_vpn::IsAllowedForContext(context));
  return static_cast<ShunyaVpnWireguardObserverService*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

}  // namespace shunya_vpn

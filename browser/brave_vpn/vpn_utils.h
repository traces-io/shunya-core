/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_VPN_UTILS_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_VPN_UTILS_H_

namespace content {
class BrowserContext;
}  // namespace content

namespace shunya_vpn {

bool IsShunyaVPNEnabled(content::BrowserContext* context);
bool IsAllowedForContext(content::BrowserContext* context);

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_VPN_UTILS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"

#include <utility>

#include "base/feature_list.h"
#include "base/no_destructor.h"
#include "shunya/browser/shunya_browser_process.h"
#include "shunya/browser/shunya_vpn/vpn_utils.h"
#include "shunya/browser/profiles/profile_util.h"
#include "shunya/browser/skus/skus_service_factory.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/skus/common/features.h"
#include "build/build_config.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/profiles/incognito_helpers.h"
#include "chrome/browser/profiles/profile.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/browser_context.h"
#include "content/public/browser/storage_partition.h"

#if BUILDFLAG(IS_WIN)
#include "shunya/browser/shunya_vpn/dns/shunya_vpn_dns_observer_factory_win.h"
#include "shunya/browser/shunya_vpn/dns/shunya_vpn_dns_observer_service_win.h"
#include "shunya/browser/shunya_vpn/win/shunya_vpn_wireguard_observer_factory_win.h"
#include "shunya/browser/shunya_vpn/win/shunya_vpn_wireguard_observer_service_win.h"
#endif

namespace shunya_vpn {

// static
ShunyaVpnServiceFactory* ShunyaVpnServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaVpnServiceFactory> instance;
  return instance.get();
}

// static
ShunyaVpnService* ShunyaVpnServiceFactory::GetForProfile(Profile* profile) {
  return static_cast<ShunyaVpnService*>(
      GetInstance()->GetServiceForBrowserContext(profile, true));
}

// static
void ShunyaVpnServiceFactory::BindForContext(
    content::BrowserContext* context,
    mojo::PendingReceiver<shunya_vpn::mojom::ServiceHandler> receiver) {
  auto* service = static_cast<ShunyaVpnService*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
  if (service) {
    service->BindInterface(std::move(receiver));
  }
}

ShunyaVpnServiceFactory::ShunyaVpnServiceFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaVpnService",
          BrowserContextDependencyManager::GetInstance()) {
  DependsOn(skus::SkusServiceFactory::GetInstance());
#if BUILDFLAG(IS_WIN)
  if (shunya_vpn::IsShunyaVPNWireguardEnabled(g_browser_process->local_state())) {
    DependsOn(shunya_vpn::ShunyaVpnWireguardObserverFactory::GetInstance());
  } else {
    DependsOn(shunya_vpn::ShunyaVpnDnsObserverFactory::GetInstance());
  }
#endif
}

ShunyaVpnServiceFactory::~ShunyaVpnServiceFactory() = default;

KeyedService* ShunyaVpnServiceFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  if (!shunya_vpn::IsAllowedForContext(context)) {
    return nullptr;
  }

#if !BUILDFLAG(IS_ANDROID)
  if (!g_shunya_browser_process->shunya_vpn_os_connection_api()) {
    return nullptr;
  }
#endif

  auto* default_storage_partition = context->GetDefaultStoragePartition();
  auto shared_url_loader_factory =
      default_storage_partition->GetURLLoaderFactoryForBrowserProcess();
  auto* local_state = g_browser_process->local_state();
  shunya_vpn::MigrateVPNSettings(user_prefs::UserPrefs::Get(context),
                                local_state);
  auto callback = base::BindRepeating(
      [](content::BrowserContext* context) {
        return skus::SkusServiceFactory::GetForContext(context);
      },
      context);

  auto* vpn_service = new ShunyaVpnService(
      g_shunya_browser_process->shunya_vpn_os_connection_api(),
      shared_url_loader_factory, local_state,
      user_prefs::UserPrefs::Get(context), callback);
#if BUILDFLAG(IS_WIN)
  if (shunya_vpn::IsShunyaVPNWireguardEnabled(g_browser_process->local_state())) {
    auto* observer_service =
        shunya_vpn::ShunyaVpnWireguardObserverFactory::GetInstance()
            ->GetServiceForContext(context);
    if (observer_service) {
      observer_service->Observe(vpn_service);
    }
  } else {
    auto* observer_service =
        shunya_vpn::ShunyaVpnDnsObserverFactory::GetInstance()
            ->GetServiceForContext(context);
    if (observer_service) {
      observer_service->Observe(vpn_service);
    }
  }
#endif
  return vpn_service;
}

void ShunyaVpnServiceFactory::RegisterProfilePrefs(
    user_prefs::PrefRegistrySyncable* registry) {
  shunya_vpn::RegisterProfilePrefs(registry);
}

}  // namespace shunya_vpn

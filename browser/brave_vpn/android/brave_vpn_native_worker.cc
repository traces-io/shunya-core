/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_vpn/android/shunya_vpn_native_worker.h"

#include "base/android/jni_android.h"
#include "base/android/jni_array.h"
#include "base/android/jni_string.h"
#include "base/functional/bind.h"
#include "base/json/json_writer.h"
#include "base/values.h"
#include "shunya/browser/shunya_vpn/shunya_vpn_service_factory.h"
#include "shunya/build/android/jni_headers/ShunyaVpnNativeWorker_jni.h"
#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/profiles/profile_manager.h"

using shunya_vpn::ShunyaVpnService;

namespace {

ShunyaVpnService* GetShunyaVpnService() {
  return shunya_vpn::ShunyaVpnServiceFactory::GetForProfile(
      ProfileManager::GetActiveUserProfile()->GetOriginalProfile());
}

}  // namespace

namespace chrome {
namespace android {

ShunyaVpnNativeWorker::ShunyaVpnNativeWorker(
    JNIEnv* env,
    const base::android::JavaRef<jobject>& obj)
    : weak_java_shunya_vpn_native_worker_(env, obj), weak_factory_(this) {
  Java_ShunyaVpnNativeWorker_setNativePtr(env, obj,
                                         reinterpret_cast<intptr_t>(this));
}

ShunyaVpnNativeWorker::~ShunyaVpnNativeWorker() {}

void ShunyaVpnNativeWorker::Destroy(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& jcaller) {
  delete this;
}

void ShunyaVpnNativeWorker::GetAllServerRegions(JNIEnv* env) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->GetAllServerRegions(
        base::BindOnce(&ShunyaVpnNativeWorker::OnGetAllServerRegions,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaVpnNativeWorker::OnGetAllServerRegions(
    const std::string& server_regions_json,
    bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaVpnNativeWorker_onGetAllServerRegions(
      env, weak_java_shunya_vpn_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, server_regions_json),
      success);
}

void ShunyaVpnNativeWorker::GetTimezonesForRegions(JNIEnv* env) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->GetTimezonesForRegions(
        base::BindOnce(&ShunyaVpnNativeWorker::OnGetTimezonesForRegions,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaVpnNativeWorker::OnGetTimezonesForRegions(
    const std::string& timezones_json,
    bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaVpnNativeWorker_onGetTimezonesForRegions(
      env, weak_java_shunya_vpn_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, timezones_json), success);
}

void ShunyaVpnNativeWorker::GetHostnamesForRegion(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& region) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->GetHostnamesForRegion(
        base::BindOnce(&ShunyaVpnNativeWorker::OnGetHostnamesForRegion,
                       weak_factory_.GetWeakPtr()),
        base::android::ConvertJavaStringToUTF8(env, region));
  }
}

void ShunyaVpnNativeWorker::OnGetHostnamesForRegion(
    const std::string& hostnames_json,
    bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaVpnNativeWorker_onGetHostnamesForRegion(
      env, weak_java_shunya_vpn_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, hostnames_json), success);
}

void ShunyaVpnNativeWorker::GetWireguardProfileCredentials(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& subscriber_credential,
    const base::android::JavaParamRef<jstring>& public_key,
    const base::android::JavaParamRef<jstring>& hostname) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->GetWireguardProfileCredentials(
        base::BindOnce(&ShunyaVpnNativeWorker::OnGetWireguardProfileCredentials,
                       weak_factory_.GetWeakPtr()),
        base::android::ConvertJavaStringToUTF8(env, subscriber_credential),
        base::android::ConvertJavaStringToUTF8(env, public_key),
        base::android::ConvertJavaStringToUTF8(env, hostname));
  }
}

void ShunyaVpnNativeWorker::OnGetWireguardProfileCredentials(
    const std::string& wireguard_profile_credentials_json,
    bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaVpnNativeWorker_onGetWireguardProfileCredentials(
      env, weak_java_shunya_vpn_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(
          env, wireguard_profile_credentials_json),
      success);
}

void ShunyaVpnNativeWorker::VerifyCredentials(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& hostname,
    const base::android::JavaParamRef<jstring>& client_id,
    const base::android::JavaParamRef<jstring>& subscriber_credential,
    const base::android::JavaParamRef<jstring>& api_auth_token) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->VerifyCredentials(
        base::BindOnce(&ShunyaVpnNativeWorker::OnVerifyCredentials,
                       weak_factory_.GetWeakPtr()),
        base::android::ConvertJavaStringToUTF8(env, hostname),
        base::android::ConvertJavaStringToUTF8(env, client_id),
        base::android::ConvertJavaStringToUTF8(env, subscriber_credential),
        base::android::ConvertJavaStringToUTF8(env, api_auth_token));
  }
}

void ShunyaVpnNativeWorker::OnVerifyCredentials(
    const std::string& verify_credentials_json,
    bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaVpnNativeWorker_onVerifyCredentials(
      env, weak_java_shunya_vpn_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, verify_credentials_json),
      success);
}

void ShunyaVpnNativeWorker::InvalidateCredentials(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& hostname,
    const base::android::JavaParamRef<jstring>& client_id,
    const base::android::JavaParamRef<jstring>& subscriber_credential,
    const base::android::JavaParamRef<jstring>& api_auth_token) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->InvalidateCredentials(
        base::BindOnce(&ShunyaVpnNativeWorker::OnInvalidateCredentials,
                       weak_factory_.GetWeakPtr()),
        base::android::ConvertJavaStringToUTF8(env, hostname),
        base::android::ConvertJavaStringToUTF8(env, client_id),
        base::android::ConvertJavaStringToUTF8(env, subscriber_credential),
        base::android::ConvertJavaStringToUTF8(env, api_auth_token));
  }
}

void ShunyaVpnNativeWorker::OnInvalidateCredentials(
    const std::string& invalidate_credentials_json,
    bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaVpnNativeWorker_onInvalidateCredentials(
      env, weak_java_shunya_vpn_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, invalidate_credentials_json),
      success);
}

void ShunyaVpnNativeWorker::GetSubscriberCredential(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& product_type,
    const base::android::JavaParamRef<jstring>& product_id,
    const base::android::JavaParamRef<jstring>& validation_method,
    const base::android::JavaParamRef<jstring>& purchase_token,
    const base::android::JavaParamRef<jstring>& bundle_id) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->GetSubscriberCredential(
        base::BindOnce(&ShunyaVpnNativeWorker::OnGetSubscriberCredential,
                       weak_factory_.GetWeakPtr()),
        base::android::ConvertJavaStringToUTF8(env, product_type),
        base::android::ConvertJavaStringToUTF8(env, product_id),
        base::android::ConvertJavaStringToUTF8(env, validation_method),
        base::android::ConvertJavaStringToUTF8(env, purchase_token),
        base::android::ConvertJavaStringToUTF8(env, bundle_id));
  }
}

void ShunyaVpnNativeWorker::GetSubscriberCredentialV12(JNIEnv* env) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->GetSubscriberCredentialV12(
        base::BindOnce(&ShunyaVpnNativeWorker::OnGetSubscriberCredential,
                       weak_factory_.GetWeakPtr()));
  }
}

void ShunyaVpnNativeWorker::OnGetSubscriberCredential(
    const std::string& subscriber_credential,
    bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaVpnNativeWorker_onGetSubscriberCredential(
      env, weak_java_shunya_vpn_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, subscriber_credential),
      success);
}

void ShunyaVpnNativeWorker::VerifyPurchaseToken(
    JNIEnv* env,
    const base::android::JavaParamRef<jstring>& purchase_token,
    const base::android::JavaParamRef<jstring>& product_id,
    const base::android::JavaParamRef<jstring>& product_type,
    const base::android::JavaParamRef<jstring>& bundle_id) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->VerifyPurchaseToken(
        base::BindOnce(&ShunyaVpnNativeWorker::OnVerifyPurchaseToken,
                       weak_factory_.GetWeakPtr()),
        base::android::ConvertJavaStringToUTF8(env, purchase_token),
        base::android::ConvertJavaStringToUTF8(env, product_id),
        base::android::ConvertJavaStringToUTF8(env, product_type),
        base::android::ConvertJavaStringToUTF8(env, bundle_id));
  }
}

void ShunyaVpnNativeWorker::OnVerifyPurchaseToken(
    const std::string& json_response,
    bool success) {
  JNIEnv* env = base::android::AttachCurrentThread();
  Java_ShunyaVpnNativeWorker_onVerifyPurchaseToken(
      env, weak_java_shunya_vpn_native_worker_.get(env),
      base::android::ConvertUTF8ToJavaString(env, json_response), success);
}

jboolean ShunyaVpnNativeWorker::IsPurchasedUser(JNIEnv* env) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    return shunya_vpn_service->is_purchased_user();
  }

  return false;
}

void ShunyaVpnNativeWorker::ReloadPurchasedState(JNIEnv* env) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->ReloadPurchasedState();
  }
}

void ShunyaVpnNativeWorker::ReportForegroundP3A(JNIEnv* env) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    // Reporting a new session to P3A functions.
    shunya_vpn_service->RecordP3A(true);
  }
}

void ShunyaVpnNativeWorker::ReportBackgroundP3A(JNIEnv* env,
                                               jlong session_start_time_ms,
                                               jlong session_end_time_ms) {
  ShunyaVpnService* shunya_vpn_service = GetShunyaVpnService();
  if (shunya_vpn_service) {
    shunya_vpn_service->RecordAndroidBackgroundP3A(session_start_time_ms,
                                                  session_end_time_ms);
  }
}

static void JNI_ShunyaVpnNativeWorker_Init(
    JNIEnv* env,
    const base::android::JavaParamRef<jobject>& jcaller) {
  new ShunyaVpnNativeWorker(env, jcaller);
}

}  // namespace android
}  // namespace chrome

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_SHUNYA_VPN_SERVICE_FACTORY_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_SHUNYA_VPN_SERVICE_FACTORY_H_

#include "shunya/components/shunya_vpn/common/mojom/shunya_vpn.mojom.h"
#include "build/build_config.h"
#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

class Profile;

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace shunya_vpn {

class ShunyaVpnService;

class ShunyaVpnServiceFactory : public BrowserContextKeyedServiceFactory {
 public:
  static ShunyaVpnService* GetForProfile(Profile* profile);
  static ShunyaVpnServiceFactory* GetInstance();

  ShunyaVpnServiceFactory(const ShunyaVpnServiceFactory&) = delete;
  ShunyaVpnServiceFactory& operator=(const ShunyaVpnServiceFactory&) = delete;

  static void BindForContext(
      content::BrowserContext* context,
      mojo::PendingReceiver<shunya_vpn::mojom::ServiceHandler> receiver);

 private:
  friend base::NoDestructor<ShunyaVpnServiceFactory>;

  ShunyaVpnServiceFactory();
  ~ShunyaVpnServiceFactory() override;

  // BrowserContextKeyedServiceFactory overrides:
  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
  void RegisterProfilePrefs(
      user_prefs::PrefRegistrySyncable* registry) override;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_SHUNYA_VPN_SERVICE_FACTORY_H_

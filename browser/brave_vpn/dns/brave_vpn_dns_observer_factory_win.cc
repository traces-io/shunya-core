/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_vpn/dns/shunya_vpn_dns_observer_factory_win.h"

#include <cstddef>
#include <memory>
#include <string>

#include "base/feature_list.h"
#include "base/no_destructor.h"
#include "shunya/browser/shunya_vpn/dns/shunya_vpn_dns_observer_service_win.h"
#include "shunya/browser/shunya_vpn/vpn_utils.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#include "chrome/browser/browser_process.h"
#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/pref_registry/pref_registry_syncable.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/browser_context.h"

namespace shunya_vpn {

// static
ShunyaVpnDnsObserverFactory* ShunyaVpnDnsObserverFactory::GetInstance() {
  static base::NoDestructor<ShunyaVpnDnsObserverFactory> instance;
  return instance.get();
}

ShunyaVpnDnsObserverFactory::~ShunyaVpnDnsObserverFactory() = default;

ShunyaVpnDnsObserverFactory::ShunyaVpnDnsObserverFactory()
    : BrowserContextKeyedServiceFactory(
          "ShunyaVpnDNSObserverService",
          BrowserContextDependencyManager::GetInstance()) {}

KeyedService* ShunyaVpnDnsObserverFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  return new ShunyaVpnDnsObserverService(g_browser_process->local_state(),
                                        user_prefs::UserPrefs::Get(context));
}

// static
ShunyaVpnDnsObserverService* ShunyaVpnDnsObserverFactory::GetServiceForContext(
    content::BrowserContext* context) {
  if (!base::FeatureList::IsEnabled(
          shunya_vpn::features::kShunyaVPNDnsProtection) ||
      shunya_vpn::IsShunyaVPNWireguardEnabled(g_browser_process->local_state())) {
    return nullptr;
  }
  DCHECK(shunya_vpn::IsAllowedForContext(context));
  return static_cast<ShunyaVpnDnsObserverService*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

void ShunyaVpnDnsObserverFactory::RegisterProfilePrefs(
    user_prefs::PrefRegistrySyncable* registry) {
  registry->RegisterBooleanPref(prefs::kShunyaVpnShowDNSPolicyWarningDialog,
                                true);
}

}  // namespace shunya_vpn

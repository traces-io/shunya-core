/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_VPN_DNS_SHUNYA_VPN_DNS_OBSERVER_FACTORY_WIN_H_
#define SHUNYA_BROWSER_SHUNYA_VPN_DNS_SHUNYA_VPN_DNS_OBSERVER_FACTORY_WIN_H_

#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace content {
class BrowserContext;
}  // namespace content

namespace user_prefs {
class PrefRegistrySyncable;
}  // namespace user_prefs

namespace shunya_vpn {

class ShunyaVpnDnsObserverService;

class ShunyaVpnDnsObserverFactory : public BrowserContextKeyedServiceFactory {
 public:
  ShunyaVpnDnsObserverFactory(const ShunyaVpnDnsObserverFactory&) = delete;
  ShunyaVpnDnsObserverFactory& operator=(const ShunyaVpnDnsObserverFactory&) =
      delete;

  static ShunyaVpnDnsObserverFactory* GetInstance();
  static ShunyaVpnDnsObserverService* GetServiceForContext(
      content::BrowserContext* context);
  void RegisterProfilePrefs(
      user_prefs::PrefRegistrySyncable* registry) override;

 private:
  friend base::NoDestructor<ShunyaVpnDnsObserverFactory>;

  ShunyaVpnDnsObserverFactory();
  ~ShunyaVpnDnsObserverFactory() override;

  // BrowserContextKeyedServiceFactory overrides:
  KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_BROWSER_SHUNYA_VPN_DNS_SHUNYA_VPN_DNS_OBSERVER_FACTORY_WIN_H_

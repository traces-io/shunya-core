/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_vpn/dns/shunya_vpn_dns_observer_service_win.h"

#include <vector>

#include "base/strings/string_util.h"
#include "shunya/browser/ui/views/shunya_vpn/shunya_vpn_dns_settings_notificiation_dialog_view.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_constants.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_state.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#include "shunya/components/shunya_vpn/common/win/utils.h"
#include "chrome/browser/net/secure_dns_config.h"
#include "chrome/browser/net/stub_resolver_config_reader.h"
#include "chrome/browser/net/system_network_context_manager.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_finder.h"
#include "chrome/browser/ui/browser_window.h"
#include "chrome/browser/ui/simple_message_box.h"
#include "chrome/common/pref_names.h"
#include "chrome/grit/chromium_strings.h"
#include "components/grit/shunya_components_strings.h"
#include "components/prefs/pref_service.h"
#include "content/public/browser/browser_task_traits.h"
#include "content/public/browser/browser_thread.h"
#include "ui/base/l10n/l10n_util.h"

namespace shunya_vpn {

namespace {
constexpr char kCloudflareDnsProviderURL[] =
    "https://chrome.cloudflare-dns.com/dns-query";

// Timer to recheck the service launch after some time and fallback to DoH if
// the service was not launched.
constexpr int kHelperServiceStartTimeoutSec = 5;

void SkipDNSDialog(PrefService* prefs, bool checked) {
  if (!prefs)
    return;
  prefs->SetBoolean(prefs::kShunyaVpnShowDNSPolicyWarningDialog, !checked);
}

gfx::NativeWindow GetAnchorBrowserWindow() {
  auto* browser = chrome::FindLastActive();
  return browser ? browser->window()->GetNativeWindow() : gfx::NativeWindow();
}

bool AreConfigsEqual(SecureDnsConfig& one, SecureDnsConfig& two) {
  return one.mode() == two.mode() &&
         one.management_mode() == two.management_mode() &&
         one.doh_servers() == two.doh_servers();
}

}  // namespace

ShunyaVpnDnsObserverService::ShunyaVpnDnsObserverService(
    PrefService* local_state,
    PrefService* profile_prefs)
    : local_state_(local_state), profile_prefs_(profile_prefs) {
  DCHECK(profile_prefs_);
  DCHECK(local_state_);
  local_state_->ClearPref(::prefs::kShunyaVpnDnsConfig);
}

ShunyaVpnDnsObserverService::~ShunyaVpnDnsObserverService() = default;

void ShunyaVpnDnsObserverService::ShowPolicyWarningMessage() {
  if (!profile_prefs_->GetBoolean(prefs::kShunyaVpnShowDNSPolicyWarningDialog)) {
    return;
  }

  if (policy_callback_) {
    std::move(policy_callback_).Run();
    return;
  }

  chrome::ShowWarningMessageBoxWithCheckbox(
      GetAnchorBrowserWindow(), l10n_util::GetStringUTF16(IDS_PRODUCT_NAME),
      l10n_util::GetStringUTF16(IDS_SHUNYA_VPN_DNS_POLICY_ALERT),
      l10n_util::GetStringUTF16(IDS_SHUNYA_VPN_DNS_POLICY_CHECKBOX),
      base::BindOnce(&SkipDNSDialog, profile_prefs_));
}

void ShunyaVpnDnsObserverService::ShowVpnDnsSettingsNotificationDialog() {
  if (dialog_callback_) {
    dialog_callback_.Run();
    return;
  }
  ShunyaVpnDnsSettingsNotificiationDialogView::Show(chrome::FindLastActive());
}

void ShunyaVpnDnsObserverService::UnlockDNS() {
  local_state_->ClearPref(::prefs::kShunyaVpnDnsConfig);
  // Read DNS config to initiate update of actual state.
  SystemNetworkContextManager::GetStubResolverConfigReader()
      ->UpdateNetworkService(false);
}

bool ShunyaVpnDnsObserverService::IsDNSHelperLive() {
  if (dns_helper_live_for_testing_.has_value()) {
    return dns_helper_live_for_testing_.value();
  }
  // If the service is not installed we should override DNS because it will be
  // handled by the service.
  if (!IsShunyaVPNHelperServiceInstalled()) {
    return false;
  }

  if (IsWindowsServiceRunning(shunya_vpn::GetShunyaVpnHelperServiceName())) {
    RunServiceWatcher();
  }

  if (IsNetworkFiltersInstalled()) {
    return true;
  }

  // The service can be stopped and this is valid state, not started yet,
  // crashed once and restarting and so on.
  content::GetUIThreadTaskRunner({})->PostDelayedTask(
      FROM_HERE,
      base::BindOnce(&ShunyaVpnDnsObserverService::OnCheckIfServiceStarted,
                     weak_ptr_factory_.GetWeakPtr()),
      base::Seconds(kHelperServiceStartTimeoutSec));
  return true;
}

void ShunyaVpnDnsObserverService::RunServiceWatcher() {
  service_watcher_.reset(new shunya::ServiceWatcher());
  if (!service_watcher_->Subscribe(
          shunya_vpn::GetShunyaVpnHelperServiceName(), SERVICE_NOTIFY_STOPPED,
          base::BindRepeating(&ShunyaVpnDnsObserverService::OnServiceStopped,
                              weak_ptr_factory_.GetWeakPtr()))) {
    VLOG(1) << "Unable to set service watcher";
  }
}

void ShunyaVpnDnsObserverService::OnServiceStopped(int mask) {
  // Postpone check because the service can be restarted by the system due to
  // configured failure actions.
  content::GetUIThreadTaskRunner({})->PostDelayedTask(
      FROM_HERE,
      base::BindOnce(&ShunyaVpnDnsObserverService::OnCheckIfServiceStarted,
                     weak_ptr_factory_.GetWeakPtr()),
      base::Seconds(kHelperServiceStartTimeoutSec));
}

bool ShunyaVpnDnsObserverService::IsVPNConnected() const {
  return connection_state_.has_value() &&
         connection_state_.value() ==
             shunya_vpn::mojom::ConnectionState::CONNECTED;
}

void ShunyaVpnDnsObserverService::OnCheckIfServiceStarted() {
  if (!IsVPNConnected()) {
    return;
  }
  // Checking if the service was not launched or filters were not set.
  if (!IsNetworkFiltersInstalled() ||
      !IsWindowsServiceRunning(shunya_vpn::GetShunyaVpnHelperServiceName())) {
    LockDNS();
    return;
  }
  RunServiceWatcher();
}

void ShunyaVpnDnsObserverService::LockDNS() {
  auto old_dns_config =
      SystemNetworkContextManager::GetStubResolverConfigReader()
          ->GetSecureDnsConfiguration(false);

  local_state_->SetString(::prefs::kShunyaVpnDnsConfig,
                          kCloudflareDnsProviderURL);

  // Trigger StubResolverConfigReader to see if it should override the settings
  // with kShunyaVpnDnsConfig
  SystemNetworkContextManager::GetStubResolverConfigReader()
      ->UpdateNetworkService(false);
  auto new_dns_config =
      SystemNetworkContextManager::GetStubResolverConfigReader()
          ->GetSecureDnsConfiguration(false);

  if (old_dns_config.mode() != net::SecureDnsMode::kSecure) {
    if (AreConfigsEqual(old_dns_config, new_dns_config)) {
      ShowPolicyWarningMessage();
    } else {
      ShowVpnDnsSettingsNotificationDialog();
    }
  }
}

void ShunyaVpnDnsObserverService::OnConnectionStateChanged(
    shunya_vpn::mojom::ConnectionState state) {
  connection_state_ = state;
  if (state == shunya_vpn::mojom::ConnectionState::CONNECTED) {
    if (IsDNSHelperLive()) {
      return;
    }
    LockDNS();
  } else if (state == shunya_vpn::mojom::ConnectionState::DISCONNECTED) {
    UnlockDNS();
  }
}

}  // namespace shunya_vpn

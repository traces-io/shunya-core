/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_UPDATE_UTIL_H_
#define SHUNYA_BROWSER_UPDATE_UTIL_H_

namespace shunya {

bool UpdateEnabled();

}  // namespace shunya

#endif  // SHUNYA_BROWSER_UPDATE_UTIL_MAC_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

// This interface is for managing the global services of the application. Each
// service is lazily created when requested the first time. The service getters
// will return NULL if the service is not available, so callers must check for
// this condition.

#ifndef SHUNYA_BROWSER_SHUNYA_BROWSER_PROCESS_H_
#define SHUNYA_BROWSER_SHUNYA_BROWSER_PROCESS_H_

#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/greaselion/browser/buildflags/buildflags.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/request_otr/common/buildflags/buildflags.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "build/build_config.h"
#include "extensions/buildflags/buildflags.h"

namespace shunya {
class ShunyaReferralsService;
class ShunyaFarblingService;
class URLSanitizerComponentInstaller;
}  // namespace shunya

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
namespace shunya_vpn {
class ShunyaVPNOSConnectionAPI;
}  // namespace shunya_vpn
#endif

namespace shunya_component_updater {
class LocalDataFilesService;
}  // namespace shunya_component_updater

namespace shunya_shields {
class AdBlockService;
class HTTPSEverywhereService;
}  // namespace shunya_shields

namespace shunya_stats {
class ShunyaStatsUpdater;
}  // namespace shunya_stats

namespace greaselion {
#if BUILDFLAG(ENABLE_GREASELION)
class GreaselionDownloadService;
#endif
}  // namespace greaselion

namespace debounce {
class DebounceComponentInstaller;
}  // namespace debounce

namespace https_upgrade_exceptions {
class HttpsUpgradeExceptionsService;
}  // namespace https_upgrade_exceptions

namespace localhost_permission {
class LocalhostPermissionComponent;
}  // namespace localhost_permission

namespace misc_metrics {
class ProcessMiscMetrics;
}  // namespace misc_metrics

namespace request_otr {
#if BUILDFLAG(ENABLE_REQUEST_OTR)
class RequestOTRComponentInstallerPolicy;
#endif
}  // namespace request_otr

namespace ntp_background_images {
class NTPBackgroundImagesService;
}  // namespace ntp_background_images

namespace p3a {
class P3AService;
}  // namespace p3a

namespace tor {
class ShunyaTorClientUpdater;
class ShunyaTorPluggableTransportUpdater;
}  // namespace tor

namespace ipfs {
class ShunyaIpfsClientUpdater;
}

namespace speedreader {
class SpeedreaderRewriterService;
}

namespace shunya_ads {
class ShunyaStatsHelper;
class ResourceComponent;
}  // namespace shunya_ads

class ShunyaBrowserProcess {
 public:
  ShunyaBrowserProcess();
  virtual ~ShunyaBrowserProcess();
  virtual void StartShunyaServices() = 0;
  virtual shunya_shields::AdBlockService* ad_block_service() = 0;
  virtual https_upgrade_exceptions::HttpsUpgradeExceptionsService*
  https_upgrade_exceptions_service() = 0;
  virtual localhost_permission::LocalhostPermissionComponent*
  localhost_permission_component() = 0;
#if BUILDFLAG(ENABLE_GREASELION)
  virtual greaselion::GreaselionDownloadService*
  greaselion_download_service() = 0;
#endif
  virtual debounce::DebounceComponentInstaller*
  debounce_component_installer() = 0;
#if BUILDFLAG(ENABLE_REQUEST_OTR)
  virtual request_otr::RequestOTRComponentInstallerPolicy*
  request_otr_component_installer() = 0;
#endif
  virtual shunya::URLSanitizerComponentInstaller*
  URLSanitizerComponentInstaller() = 0;
  virtual shunya_shields::HTTPSEverywhereService* https_everywhere_service() = 0;
  virtual shunya_component_updater::LocalDataFilesService*
  local_data_files_service() = 0;
#if BUILDFLAG(ENABLE_TOR)
  virtual tor::ShunyaTorClientUpdater* tor_client_updater() = 0;
  virtual tor::ShunyaTorPluggableTransportUpdater*
  tor_pluggable_transport_updater() = 0;
#endif
#if BUILDFLAG(ENABLE_IPFS)
  virtual ipfs::ShunyaIpfsClientUpdater* ipfs_client_updater() = 0;
#endif
  virtual p3a::P3AService* p3a_service() = 0;
  virtual shunya::ShunyaReferralsService* shunya_referrals_service() = 0;
  virtual shunya_stats::ShunyaStatsUpdater* shunya_stats_updater() = 0;
  virtual shunya_ads::ShunyaStatsHelper* ads_shunya_stats_helper() = 0;
  virtual ntp_background_images::NTPBackgroundImagesService*
  ntp_background_images_service() = 0;
#if BUILDFLAG(ENABLE_SPEEDREADER)
  virtual speedreader::SpeedreaderRewriterService*
  speedreader_rewriter_service() = 0;
#endif
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  virtual shunya_vpn::ShunyaVPNOSConnectionAPI* shunya_vpn_os_connection_api() = 0;
#endif
  virtual shunya_ads::ResourceComponent* resource_component() = 0;
  virtual shunya::ShunyaFarblingService* shunya_farbling_service() = 0;
  virtual misc_metrics::ProcessMiscMetrics* process_misc_metrics() = 0;
};

extern ShunyaBrowserProcess* g_shunya_browser_process;

#endif  // SHUNYA_BROWSER_SHUNYA_BROWSER_PROCESS_H_

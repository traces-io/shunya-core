/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/prefs/shunya_pref_service_incognito_allowlist.h"

#include "base/no_destructor.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/constants/pref_names.h"
#include "build/build_config.h"
#include "chrome/common/pref_names.h"

#if !BUILDFLAG(IS_ANDROID)
#include "shunya/browser/ui/tabs/shunya_tab_prefs.h"
#endif

#if BUILDFLAG(ENABLE_AI_CHAT)
#include "shunya/components/ai_chat/common/pref_names.h"
#endif  // BUILDFLAG(ENABLE_AI_CHAT)

#if defined(TOOLKIT_VIEWS)
#include "shunya/components/sidebar/pref_names.h"
#endif

namespace shunya {

const std::vector<const char*>& GetShunyaPersistentPrefNames() {
  static base::NoDestructor<std::vector<const char*>> shunya_allowlist({
    kShunyaAutofillPrivateWindows,
#if !BUILDFLAG(IS_ANDROID)
        prefs::kSidePanelHorizontalAlignment, kTabMuteIndicatorNotClickable,
        shunya_tabs::kVerticalTabsExpandedWidth,
        shunya_tabs::kVerticalTabsEnabled, shunya_tabs::kVerticalTabsCollapsed,
        shunya_tabs::kVerticalTabsFloatingEnabled,
#endif
#if defined(TOOLKIT_VIEWS)
        sidebar::kSidePanelWidth,
#endif
#if BUILDFLAG(ENABLE_AI_CHAT)
        ai_chat::prefs::kShunyaChatHasSeenDisclaimer,
        ai_chat::prefs::kShunyaChatAutoGenerateQuestions,
#endif  // BUILDFLAG(ENABLE_AI_CHAT)
  });

  return *shunya_allowlist;
}

}  // namespace shunya

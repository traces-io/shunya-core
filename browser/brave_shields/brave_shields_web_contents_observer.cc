/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_shields/shunya_shields_web_contents_observer.h"

#include <memory>
#include <string>
#include <utility>

#include "base/containers/cxx20_erase_vector.h"
#include "base/feature_list.h"
#include "base/strings/utf_string_conversions.h"
#include "shunya/components/shunya_perf_predictor/browser/perf_predictor_tab_helper.h"
#include "shunya/components/shunya_shields/browser/shunya_shields_util.h"
#include "shunya/components/shunya_shields/common/shunya_shield_constants.h"
#include "shunya/components/shunya_shields/common/pref_names.h"
#include "shunya/components/constants/pref_names.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/common/renderer_configuration.mojom.h"
#include "components/prefs/pref_registry_simple.h"
#include "components/prefs/pref_service.h"
#include "components/user_prefs/user_prefs.h"
#include "content/public/browser/browser_thread.h"
#include "content/public/browser/navigation_handle.h"
#include "content/public/browser/render_frame_host.h"
#include "content/public/browser/render_process_host.h"
#include "content/public/browser/web_contents.h"
#include "extensions/buildflags/buildflags.h"
#include "mojo/public/cpp/bindings/associated_remote.h"
#include "third_party/blink/public/common/associated_interfaces/associated_interface_provider.h"

#if !BUILDFLAG(IS_ANDROID)
#include "shunya/browser/ui/shunya_shields_data_controller.h"
#endif

using content::RenderFrameHost;
using content::WebContents;

namespace shunya_shields {

namespace {

ShunyaShieldsWebContentsObserver* g_receiver_impl_for_testing = nullptr;

}  // namespace

ShunyaShieldsWebContentsObserver::~ShunyaShieldsWebContentsObserver() {
  shunya_shields_remotes_.clear();
}

ShunyaShieldsWebContentsObserver::ShunyaShieldsWebContentsObserver(
    WebContents* web_contents)
    : WebContentsObserver(web_contents),
      content::WebContentsUserData<ShunyaShieldsWebContentsObserver>(
          *web_contents),
      receivers_(web_contents, this) {}

void ShunyaShieldsWebContentsObserver::RenderFrameCreated(RenderFrameHost* rfh) {
  if (rfh && allowed_scripts_.size()) {
    GetShunyaShieldsRemote(rfh)->SetAllowScriptsFromOriginsOnce(
        allowed_scripts_);
  }
  if (rfh) {
    if (content::BrowserContext* context = rfh->GetBrowserContext()) {
      if (PrefService* pref_service = user_prefs::UserPrefs::Get(context)) {
        GetShunyaShieldsRemote(rfh)->SetReduceLanguageEnabled(
            shunya_shields::IsReduceLanguageEnabledForProfile(pref_service));
      }
    }
  }
}

void ShunyaShieldsWebContentsObserver::RenderFrameDeleted(RenderFrameHost* rfh) {
  shunya_shields_remotes_.erase(rfh);
}

void ShunyaShieldsWebContentsObserver::RenderFrameHostChanged(
    RenderFrameHost* old_host,
    RenderFrameHost* new_host) {
  if (old_host) {
    RenderFrameDeleted(old_host);
  }
  if (new_host) {
    RenderFrameCreated(new_host);
  }
}

bool ShunyaShieldsWebContentsObserver::IsBlockedSubresource(
    const std::string& subresource) {
  return blocked_url_paths_.find(subresource) != blocked_url_paths_.end();
}

void ShunyaShieldsWebContentsObserver::AddBlockedSubresource(
    const std::string& subresource) {
  blocked_url_paths_.insert(subresource);
}

// static
void ShunyaShieldsWebContentsObserver::BindShunyaShieldsHost(
    mojo::PendingAssociatedReceiver<shunya_shields::mojom::ShunyaShieldsHost>
        receiver,
    content::RenderFrameHost* rfh) {
  if (g_receiver_impl_for_testing) {
    g_receiver_impl_for_testing->BindReceiver(std::move(receiver), rfh);
    return;
  }

  auto* web_contents = content::WebContents::FromRenderFrameHost(rfh);
  if (!web_contents)
    return;

  auto* shields_host =
      ShunyaShieldsWebContentsObserver::FromWebContents(web_contents);
  if (!shields_host)
    return;
  shields_host->BindReceiver(std::move(receiver), rfh);
}

// static
void ShunyaShieldsWebContentsObserver::DispatchBlockedEvent(
    const GURL& request_url,
    int frame_tree_node_id,
    const std::string& block_type) {
  DCHECK_CURRENTLY_ON(content::BrowserThread::UI);

  auto subresource = request_url.spec();
  WebContents* web_contents =
      WebContents::FromFrameTreeNodeId(frame_tree_node_id);
  DispatchBlockedEventForWebContents(block_type, subresource, web_contents);

  if (web_contents) {
    ShunyaShieldsWebContentsObserver* observer =
        ShunyaShieldsWebContentsObserver::FromWebContents(web_contents);
    if (observer && !observer->IsBlockedSubresource(subresource)) {
      observer->AddBlockedSubresource(subresource);
      PrefService* prefs =
          Profile::FromBrowserContext(web_contents->GetBrowserContext())
              ->GetOriginalProfile()
              ->GetPrefs();

      if (block_type == kAds) {
        prefs->SetUint64(kAdsBlocked, prefs->GetUint64(kAdsBlocked) + 1);
      } else if (block_type == kHTTPUpgradableResources) {
        prefs->SetUint64(kHttpsUpgrades, prefs->GetUint64(kHttpsUpgrades) + 1);
      } else if (block_type == kJavaScript) {
        prefs->SetUint64(kJavascriptBlocked,
                         prefs->GetUint64(kJavascriptBlocked) + 1);
      } else if (block_type == kFingerprintingV2) {
        prefs->SetUint64(kFingerprintingBlocked,
                         prefs->GetUint64(kFingerprintingBlocked) + 1);
      }
    }
  }
  shunya_perf_predictor::PerfPredictorTabHelper::DispatchBlockedEvent(
      request_url.spec(), frame_tree_node_id);
}

#if !BUILDFLAG(IS_ANDROID)
// static
void ShunyaShieldsWebContentsObserver::DispatchBlockedEventForWebContents(
    const std::string& block_type,
    const std::string& subresource,
    WebContents* web_contents) {
  if (!web_contents)
    return;
  auto* shields_data_ctrlr =
      shunya_shields::ShunyaShieldsDataController::FromWebContents(web_contents);
  // |shields_data_ctrlr| can be null if the |web_contents| is generated in
  // component layer - We don't attach any tab helpers in this case.
  if (!shields_data_ctrlr)
    return;
  shields_data_ctrlr->HandleItemBlocked(block_type, subresource);
}
// static
void ShunyaShieldsWebContentsObserver::DispatchAllowedOnceEventForWebContents(
    const std::string& block_type,
    const std::string& subresource,
    WebContents* web_contents) {
  if (!web_contents) {
    return;
  }
  auto* shields_data_ctrlr =
      shunya_shields::ShunyaShieldsDataController::FromWebContents(web_contents);
  // |shields_data_ctrlr| can be null if the |web_contents| is generated in
  // component layer - We don't attach any tab helpers in this case.
  if (!shields_data_ctrlr) {
    return;
  }
  shields_data_ctrlr->HandleItemAllowedOnce(block_type, subresource);
}
#endif

void ShunyaShieldsWebContentsObserver::OnJavaScriptAllowedOnce(
    const std::u16string& details) {
#if !BUILDFLAG(IS_ANDROID)
  WebContents* web_contents =
      WebContents::FromRenderFrameHost(receivers_.GetCurrentTargetFrame());
  if (!web_contents)
    return;
  DispatchAllowedOnceEventForWebContents(
      shunya_shields::kJavaScript, base::UTF16ToUTF8(details), web_contents);
#endif
}

void ShunyaShieldsWebContentsObserver::OnJavaScriptBlocked(
    const std::u16string& details) {
  WebContents* web_contents =
      WebContents::FromRenderFrameHost(receivers_.GetCurrentTargetFrame());
  if (!web_contents) {
    return;
  }
  DispatchBlockedEventForWebContents(shunya_shields::kJavaScript,
                                     base::UTF16ToUTF8(details), web_contents);
}

// static
void ShunyaShieldsWebContentsObserver::RegisterProfilePrefs(
    PrefRegistrySimple* registry) {
  registry->RegisterUint64Pref(kAdsBlocked, 0);
  registry->RegisterUint64Pref(kTrackersBlocked, 0);
  registry->RegisterUint64Pref(kJavascriptBlocked, 0);
  registry->RegisterUint64Pref(kHttpsUpgrades, 0);
  registry->RegisterUint64Pref(kFingerprintingBlocked, 0);
}

void ShunyaShieldsWebContentsObserver::ReadyToCommitNavigation(
    content::NavigationHandle* navigation_handle) {
  // when the main frame navigate away
  content::ReloadType reload_type = navigation_handle->GetReloadType();
  if (navigation_handle->IsInMainFrame() &&
      !navigation_handle->IsSameDocument()) {
    if (reload_type == content::ReloadType::NONE) {
      // For new loads, we reset the counters for both blocked scripts and URLs.
      allowed_scripts_.clear();
      blocked_url_paths_.clear();
    } else if (reload_type == content::ReloadType::NORMAL) {
      // For normal reloads (or loads to the current URL, internally converted
      // into reloads i.e see NavigationControllerImpl::NavigateWithoutEntry),
      // we only reset the counter for blocked URLs, not the one for scripts.
      blocked_url_paths_.clear();
    }
  }

  navigation_handle->GetWebContents()->ForEachRenderFrameHost(
      [this](content::RenderFrameHost* rfh) {
        GetShunyaShieldsRemote(rfh)->SetAllowScriptsFromOriginsOnce(
            allowed_scripts_);
        if (content::BrowserContext* context = rfh->GetBrowserContext()) {
          if (PrefService* pref_service = user_prefs::UserPrefs::Get(context)) {
            GetShunyaShieldsRemote(rfh)->SetReduceLanguageEnabled(
                shunya_shields::IsReduceLanguageEnabledForProfile(pref_service));
          }
        }
      });
}

void ShunyaShieldsWebContentsObserver::BlockAllowedScripts(
    const std::vector<std::string>& scripts) {
  for (const auto& script : scripts) {
    auto origin = url::Origin::Create(GURL(script));
    bool is_origin = origin.Serialize() == script;
    base::EraseIf(allowed_scripts_, [is_origin, script,
                                     origin](const std::string& value) {
      // scripts array may have both origins or full scripts paths.
      return is_origin ? url::Origin::Create(GURL(value)) == origin
                       : value == script;
    });
  }
}

void ShunyaShieldsWebContentsObserver::AllowScriptsOnce(
    const std::vector<std::string>& origins) {
  allowed_scripts_.insert(std::end(allowed_scripts_), std::begin(origins),
                          std::end(origins));
}

// static
void ShunyaShieldsWebContentsObserver::SetReceiverImplForTesting(
    ShunyaShieldsWebContentsObserver* impl) {
  g_receiver_impl_for_testing = impl;
}

void ShunyaShieldsWebContentsObserver::BindReceiver(
    mojo::PendingAssociatedReceiver<shunya_shields::mojom::ShunyaShieldsHost>
        receiver,
    content::RenderFrameHost* rfh) {
  receivers_.Bind(rfh, std::move(receiver));
}

mojo::AssociatedRemote<shunya_shields::mojom::ShunyaShields>&
ShunyaShieldsWebContentsObserver::GetShunyaShieldsRemote(
    content::RenderFrameHost* rfh) {
  if (!shunya_shields_remotes_.contains(rfh)) {
    rfh->GetRemoteAssociatedInterfaces()->GetInterface(
        &shunya_shields_remotes_[rfh]);
  }

  DCHECK(shunya_shields_remotes_[rfh].is_bound());
  return shunya_shields_remotes_[rfh];
}

WEB_CONTENTS_USER_DATA_KEY_IMPL(ShunyaShieldsWebContentsObserver);

}  // namespace shunya_shields

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_SHIELDS_SHUNYA_SHIELDS_WEB_CONTENTS_OBSERVER_H_
#define SHUNYA_BROWSER_SHUNYA_SHIELDS_SHUNYA_SHIELDS_WEB_CONTENTS_OBSERVER_H_

#include <map>
#include <set>
#include <string>
#include <vector>

#include "base/containers/flat_map.h"
#include "base/synchronization/lock.h"
#include "shunya/components/shunya_shields/common/shunya_shields.mojom.h"
#include "content/public/browser/render_frame_host_receiver_set.h"
#include "content/public/browser/web_contents_observer.h"
#include "content/public/browser/web_contents_user_data.h"

namespace content {
class WebContents;
}

class PrefRegistrySimple;

namespace shunya_shields {

class ShunyaShieldsWebContentsObserver
    : public content::WebContentsObserver,
      public content::WebContentsUserData<ShunyaShieldsWebContentsObserver>,
      public shunya_shields::mojom::ShunyaShieldsHost {
 public:
  explicit ShunyaShieldsWebContentsObserver(content::WebContents*);
  ShunyaShieldsWebContentsObserver(const ShunyaShieldsWebContentsObserver&) =
      delete;
  ShunyaShieldsWebContentsObserver& operator=(
      const ShunyaShieldsWebContentsObserver&) = delete;
  ~ShunyaShieldsWebContentsObserver() override;

  static void BindShunyaShieldsHost(
      mojo::PendingAssociatedReceiver<shunya_shields::mojom::ShunyaShieldsHost>
          receiver,
      content::RenderFrameHost* rfh);

  static void RegisterProfilePrefs(PrefRegistrySimple* registry);
  static void DispatchBlockedEventForWebContents(
      const std::string& block_type,
      const std::string& subresource,
      content::WebContents* web_contents);
  static void DispatchAllowedOnceEventForWebContents(
      const std::string& block_type,
      const std::string& subresource,
      content::WebContents* web_contents);
  static void DispatchBlockedEvent(const GURL& request_url,
                                   int frame_tree_node_id,
                                   const std::string& block_type);
  static GURL GetTabURLFromRenderFrameInfo(int render_frame_tree_node_id);
  void AllowScriptsOnce(const std::vector<std::string>& origins);
  void BlockAllowedScripts(const std::vector<std::string>& origins);
  bool IsBlockedSubresource(const std::string& subresource);
  void AddBlockedSubresource(const std::string& subresource);

 protected:
  // content::WebContentsObserver overrides.
  void RenderFrameCreated(content::RenderFrameHost* host) override;
  void RenderFrameDeleted(content::RenderFrameHost* render_frame_host) override;
  void RenderFrameHostChanged(content::RenderFrameHost* old_host,
                              content::RenderFrameHost* new_host) override;
  void ReadyToCommitNavigation(
      content::NavigationHandle* navigation_handle) override;

  // shunya_shields::mojom::ShunyaShieldsHost.
  void OnJavaScriptBlocked(const std::u16string& details) override;
  void OnJavaScriptAllowedOnce(const std::u16string& details) override;

 private:
  friend class content::WebContentsUserData<ShunyaShieldsWebContentsObserver>;
  friend class ShunyaShieldsWebContentsObserverBrowserTest;

  using ShunyaShieldsRemotesMap = base::flat_map<
      content::RenderFrameHost*,
      mojo::AssociatedRemote<shunya_shields::mojom::ShunyaShields>>;

  // Allows indicating a implementor of shunya_shields::mojom::ShunyaShieldsHost
  // other than this own class, for testing purposes only.
  static void SetReceiverImplForTesting(ShunyaShieldsWebContentsObserver* impl);

  // Only used from the BindShunyaShieldsHost() static method, useful to bind the
  // mojo receiver of shunya_shields::mojom::ShunyaShieldsHost to a different
  // implementor when needed, for testing purposes.
  void BindReceiver(mojo::PendingAssociatedReceiver<
                        shunya_shields::mojom::ShunyaShieldsHost> receiver,
                    content::RenderFrameHost* rfh);

  // Return an already bound remote for the shunya_shields::mojom::ShunyaShields
  // mojo interface. It is an error to call this method with an invalid |rfh|.
  mojo::AssociatedRemote<shunya_shields::mojom::ShunyaShields>&
  GetShunyaShieldsRemote(content::RenderFrameHost* rfh);

  std::vector<std::string> allowed_scripts_;
  // We keep a set of the current page's blocked URLs in case the page
  // continually tries to load the same blocked URLs.
  std::set<std::string> blocked_url_paths_;

  content::RenderFrameHostReceiverSet<shunya_shields::mojom::ShunyaShieldsHost>
      receivers_;

  // Map of remote endpoints for the shunya_shields::mojom::ShunyaShields mojo
  // interface, to prevent binding a new remote each time it's used.
  ShunyaShieldsRemotesMap shunya_shields_remotes_;

  WEB_CONTENTS_USER_DATA_KEY_DECL();
};

}  // namespace shunya_shields

#endif  // SHUNYA_BROWSER_SHUNYA_SHIELDS_SHUNYA_SHIELDS_WEB_CONTENTS_OBSERVER_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_SHUNYA_SHIELDS_AD_BLOCK_SUBSCRIPTION_DOWNLOAD_MANAGER_GETTER_H_
#define SHUNYA_BROWSER_SHUNYA_SHIELDS_AD_BLOCK_SUBSCRIPTION_DOWNLOAD_MANAGER_GETTER_H_

#include "shunya/components/shunya_shields/browser/ad_block_subscription_download_manager.h"

shunya_shields::AdBlockSubscriptionDownloadManager::DownloadManagerGetter
AdBlockSubscriptionDownloadManagerGetter();

#endif  // SHUNYA_BROWSER_SHUNYA_SHIELDS_AD_BLOCK_SUBSCRIPTION_DOWNLOAD_MANAGER_GETTER_H_

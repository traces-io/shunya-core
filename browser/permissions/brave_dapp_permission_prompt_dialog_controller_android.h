/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_PERMISSIONS_SHUNYA_DAPP_PERMISSION_PROMPT_DIALOG_CONTROLLER_ANDROID_H_
#define SHUNYA_BROWSER_PERMISSIONS_SHUNYA_DAPP_PERMISSION_PROMPT_DIALOG_CONTROLLER_ANDROID_H_

#include <jni.h>
#include <string>
#include <vector>

#include "base/android/scoped_java_ref.h"
#include "base/memory/raw_ptr.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"

namespace content {
class WebContents;
}

class ShunyaDappPermissionPromptDialogController {
 public:
  class Delegate {
   public:
    virtual void OnDialogDismissed() = 0;
    virtual void ConnectToSite(const std::vector<std::string>& accounts) = 0;
    virtual void CancelConnectToSite() = 0;
  };

  // Both the `delegate` and `web_contents` should outlive `this`.
  ShunyaDappPermissionPromptDialogController(
      Delegate* delegate,
      content::WebContents* web_contents_,
      shunya_wallet::mojom::CoinType coin_type_);
  ~ShunyaDappPermissionPromptDialogController();

  void ShowDialog();
  void DismissDialog();

  void OnPrimaryButtonClicked(
      JNIEnv* env,
      const base::android::JavaParamRef<jobjectArray>& accounts);
  void OnNegativeButtonClicked(JNIEnv* env);
  void OnDialogDismissed(JNIEnv* env);

 private:
  base::android::ScopedJavaGlobalRef<jobject> GetOrCreateJavaObject();

  raw_ptr<Delegate> delegate_ = nullptr;
  raw_ptr<content::WebContents> web_contents_ = nullptr;
  shunya_wallet::mojom::CoinType coin_type_;

  // The corresponding java object.
  base::android::ScopedJavaGlobalRef<jobject> java_object_;
};

#endif  // SHUNYA_BROWSER_PERMISSIONS_SHUNYA_DAPP_PERMISSION_PROMPT_DIALOG_CONTROLLER_ANDROID_H_

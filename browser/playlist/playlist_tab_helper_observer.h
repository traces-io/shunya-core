/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_BROWSER_PLAYLIST_PLAYLIST_TAB_HELPER_OBSERVER_H_
#define SHUNYA_BROWSER_PLAYLIST_PLAYLIST_TAB_HELPER_OBSERVER_H_

#include <vector>

#include "base/observer_list_types.h"
#include "shunya/components/playlist/common/mojom/playlist.mojom.h"

namespace playlist {

class PlaylistTabHelperObserver : public base::CheckedObserver {
 public:
  virtual void PlaylistTabHelperWillBeDestroyed() = 0;
  virtual void OnSavedItemsChanged(
      const std::vector<mojom::PlaylistItemPtr>& items) = 0;
  virtual void OnFoundItemsChanged(
      const std::vector<mojom::PlaylistItemPtr>& items) = 0;
  virtual void OnAddedItemFromTabHelper(
      const std::vector<mojom::PlaylistItemPtr>& items) = 0;
};

}  // namespace playlist

#endif  // SHUNYA_BROWSER_PLAYLIST_PLAYLIST_TAB_HELPER_OBSERVER_H_

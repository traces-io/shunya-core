/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/renderer/shunya_content_renderer_client.h"

#include <utility>

#include "base/feature_list.h"
#include "base/ranges/algorithm.h"
#include "shunya/components/shunya_search/common/shunya_search_utils.h"
#include "shunya/components/shunya_search/renderer/shunya_search_render_frame_observer.h"
#include "shunya/components/shunya_shields/common/features.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/shunya_wallet/common/features.h"
#include "shunya/components/cosmetic_filters/renderer/cosmetic_filters_js_render_frame_observer.h"
#include "shunya/components/playlist/common/buildflags/buildflags.h"
#include "shunya/components/safe_builtins/renderer/safe_builtins.h"
#include "shunya/components/skus/common/features.h"
#include "shunya/components/skus/renderer/skus_render_frame_observer.h"
#include "shunya/components/speedreader/common/buildflags/buildflags.h"
#include "shunya/renderer/shunya_render_thread_observer.h"
#include "shunya/renderer/shunya_url_loader_throttle_provider_impl.h"
#include "shunya/renderer/shunya_wallet/shunya_wallet_render_frame_observer.h"
#include "chrome/common/chrome_isolated_world_ids.h"
#include "chrome/renderer/chrome_render_thread_observer.h"
#include "content/public/renderer/render_thread.h"
#include "third_party/blink/public/common/features.h"
#include "third_party/blink/public/platform/web_runtime_features.h"
#include "third_party/blink/public/web/modules/service_worker/web_service_worker_context_proxy.h"
#include "third_party/blink/public/web/web_script_controller.h"
#include "third_party/widevine/cdm/buildflags.h"
#include "url/gurl.h"

#if BUILDFLAG(ENABLE_SPEEDREADER)
#include "shunya/components/speedreader/common/features.h"
#include "shunya/components/speedreader/renderer/speedreader_render_frame_observer.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#if BUILDFLAG(IS_ANDROID)
#include "shunya/components/shunya_vpn/renderer/android/vpn_render_frame_observer.h"
#endif  // BUILDFLAG(IS_ANDROID)
#endif  // BUILDFLAG(ENABLE_SHUNYA_VPN)

#if BUILDFLAG(ENABLE_PLAYLIST)
#include "shunya/components/playlist/common/features.h"
#include "shunya/components/playlist/renderer/playlist_render_frame_observer.h"
#endif

#if BUILDFLAG(ENABLE_WIDEVINE)
#include "media/base/key_system_info.h"
#include "third_party/widevine/cdm/widevine_cdm_common.h"
#endif

namespace {
void MaybeRemoveWidevineSupport(media::GetSupportedKeySystemsCB cb,
                                media::KeySystemInfos key_systems) {
#if BUILDFLAG(ENABLE_WIDEVINE)
  auto dynamic_params = ShunyaRenderThreadObserver::GetDynamicParams();
  if (!dynamic_params.widevine_enabled) {
    key_systems.erase(
        base::ranges::remove(
            key_systems, kWidevineKeySystem,
            [](const std::unique_ptr<media::KeySystemInfo>& key_system) {
              return key_system->GetBaseKeySystemName();
            }),
        key_systems.cend());
  }
#endif
  cb.Run(std::move(key_systems));
}

}  // namespace

ShunyaContentRendererClient::ShunyaContentRendererClient() = default;

void ShunyaContentRendererClient::
    SetRuntimeFeaturesDefaultsBeforeBlinkInitialization() {
  ChromeContentRendererClient::
      SetRuntimeFeaturesDefaultsBeforeBlinkInitialization();

  blink::WebRuntimeFeatures::EnableWebNFC(false);
  blink::WebRuntimeFeatures::EnableAnonymousIframe(false);

  // These features don't have dedicated WebRuntimeFeatures wrappers.
  blink::WebRuntimeFeatures::EnableFeatureFromString("DigitalGoods", false);
  if (!base::FeatureList::IsEnabled(blink::features::kFileSystemAccessAPI)) {
    blink::WebRuntimeFeatures::EnableFeatureFromString("FileSystemAccessLocal",
                                                       false);
    blink::WebRuntimeFeatures::EnableFeatureFromString(
        "FileSystemAccessAPIExperimental", false);
  }
  if (!base::FeatureList::IsEnabled(blink::features::kShunyaWebSerialAPI)) {
    blink::WebRuntimeFeatures::EnableFeatureFromString("Serial", false);
  }
  blink::WebRuntimeFeatures::EnableFeatureFromString(
      "SpeculationRulesPrefetchProxy", false);
  blink::WebRuntimeFeatures::EnableFeatureFromString("AdTagging", false);
  blink::WebRuntimeFeatures::EnableFeatureFromString("WebEnvironmentIntegrity",
                                                     false);
}

ShunyaContentRendererClient::~ShunyaContentRendererClient() = default;

void ShunyaContentRendererClient::RenderThreadStarted() {
  ChromeContentRendererClient::RenderThreadStarted();

  shunya_observer_ = std::make_unique<ShunyaRenderThreadObserver>();
  content::RenderThread::Get()->AddObserver(shunya_observer_.get());
  shunya_search_service_worker_holder_.SetBrowserInterfaceBrokerProxy(
      browser_interface_broker_.get());

  blink::WebScriptController::RegisterExtension(
      shunya::SafeBuiltins::CreateV8Extension());
}

void ShunyaContentRendererClient::RenderFrameCreated(
    content::RenderFrame* render_frame) {
  ChromeContentRendererClient::RenderFrameCreated(render_frame);

  if (base::FeatureList::IsEnabled(
          shunya_shields::features::kShunyaAdblockCosmeticFiltering)) {
    auto dynamic_params_closure = base::BindRepeating([]() {
      auto dynamic_params = ShunyaRenderThreadObserver::GetDynamicParams();
      return dynamic_params.de_amp_enabled;
    });

    new cosmetic_filters::CosmeticFiltersJsRenderFrameObserver(
        render_frame, ISOLATED_WORLD_ID_SHUNYA_INTERNAL, dynamic_params_closure);
  }

  if (base::FeatureList::IsEnabled(
          shunya_wallet::features::kNativeShunyaWalletFeature)) {
    new shunya_wallet::ShunyaWalletRenderFrameObserver(
        render_frame,
        base::BindRepeating(&ShunyaRenderThreadObserver::GetDynamicParams));
  }

  if (shunya_search::IsDefaultAPIEnabled()) {
    new shunya_search::ShunyaSearchRenderFrameObserver(
        render_frame, content::ISOLATED_WORLD_ID_GLOBAL);
  }

  if (base::FeatureList::IsEnabled(skus::features::kSkusFeature) &&
      !ChromeRenderThreadObserver::is_incognito_process()) {
    new skus::SkusRenderFrameObserver(render_frame);
  }

#if BUILDFLAG(IS_ANDROID)
  if (shunya_vpn::IsShunyaVPNFeatureEnabled()) {
    new shunya_vpn::VpnRenderFrameObserver(render_frame,
                                          content::ISOLATED_WORLD_ID_GLOBAL);
  }
#endif

#if BUILDFLAG(ENABLE_SPEEDREADER)
  if (base::FeatureList::IsEnabled(speedreader::kSpeedreaderFeature)) {
    new speedreader::SpeedreaderRenderFrameObserver(
        render_frame, ISOLATED_WORLD_ID_SHUNYA_INTERNAL);
  }
#endif

#if BUILDFLAG(ENABLE_PLAYLIST)
  if (base::FeatureList::IsEnabled(playlist::features::kPlaylist) &&
      !ChromeRenderThreadObserver::is_incognito_process()) {
    new playlist::PlaylistRenderFrameObserver(render_frame,
                                              ISOLATED_WORLD_ID_SHUNYA_INTERNAL);
  }
#endif
}

void ShunyaContentRendererClient::GetSupportedKeySystems(
    media::GetSupportedKeySystemsCB cb) {
  ChromeContentRendererClient::GetSupportedKeySystems(
      base::BindRepeating(&MaybeRemoveWidevineSupport, cb));
}

void ShunyaContentRendererClient::RunScriptsAtDocumentStart(
    content::RenderFrame* render_frame) {
  auto* observer =
      cosmetic_filters::CosmeticFiltersJsRenderFrameObserver::Get(render_frame);
  // Run this before any extensions
  if (observer) {
    observer->RunScriptsAtDocumentStart();
  }

#if BUILDFLAG(ENABLE_PLAYLIST)
  if (base::FeatureList::IsEnabled(playlist::features::kPlaylist)) {
    if (auto* playlist_observer =
            playlist::PlaylistRenderFrameObserver::Get(render_frame)) {
      playlist_observer->RunScriptsAtDocumentStart();
    }
  }
#endif

  ChromeContentRendererClient::RunScriptsAtDocumentStart(render_frame);
}

void ShunyaContentRendererClient::WillEvaluateServiceWorkerOnWorkerThread(
    blink::WebServiceWorkerContextProxy* context_proxy,
    v8::Local<v8::Context> v8_context,
    int64_t service_worker_version_id,
    const GURL& service_worker_scope,
    const GURL& script_url) {
  shunya_search_service_worker_holder_.WillEvaluateServiceWorkerOnWorkerThread(
      context_proxy, v8_context, service_worker_version_id,
      service_worker_scope, script_url);
  ChromeContentRendererClient::WillEvaluateServiceWorkerOnWorkerThread(
      context_proxy, v8_context, service_worker_version_id,
      service_worker_scope, script_url);
}

void ShunyaContentRendererClient::WillDestroyServiceWorkerContextOnWorkerThread(
    v8::Local<v8::Context> v8_context,
    int64_t service_worker_version_id,
    const GURL& service_worker_scope,
    const GURL& script_url) {
  shunya_search_service_worker_holder_
      .WillDestroyServiceWorkerContextOnWorkerThread(
          v8_context, service_worker_version_id, service_worker_scope,
          script_url);
  ChromeContentRendererClient::WillDestroyServiceWorkerContextOnWorkerThread(
      v8_context, service_worker_version_id, service_worker_scope, script_url);
}

std::unique_ptr<blink::URLLoaderThrottleProvider>
ShunyaContentRendererClient::CreateURLLoaderThrottleProvider(
    blink::URLLoaderThrottleProviderType provider_type) {
  return std::make_unique<ShunyaURLLoaderThrottleProviderImpl>(
      browser_interface_broker_.get(), provider_type, this);
}

bool ShunyaContentRendererClient::IsTorProcess() const {
  return shunya_observer_->is_tor_process();
}

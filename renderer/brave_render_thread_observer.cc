/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/renderer/shunya_render_thread_observer.h"

#include <utility>

#include "base/logging.h"
#include "base/no_destructor.h"
#include "third_party/blink/public/common/associated_interfaces/associated_interface_registry.h"

namespace {

shunya::mojom::DynamicParams* GetDynamicConfigParams() {
  static base::NoDestructor<shunya::mojom::DynamicParams> dynamic_params;
  return dynamic_params.get();
}

}  // namespace

ShunyaRenderThreadObserver::ShunyaRenderThreadObserver() = default;

ShunyaRenderThreadObserver::~ShunyaRenderThreadObserver() = default;

// static
const shunya::mojom::DynamicParams&
ShunyaRenderThreadObserver::GetDynamicParams() {
  return *GetDynamicConfigParams();
}

void ShunyaRenderThreadObserver::RegisterMojoInterfaces(
    blink::AssociatedInterfaceRegistry* associated_interfaces) {
  associated_interfaces->AddInterface<shunya::mojom::ShunyaRendererConfiguration>(
      base::BindRepeating(
          &ShunyaRenderThreadObserver::OnRendererConfigurationAssociatedRequest,
          base::Unretained(this)));
}

void ShunyaRenderThreadObserver::UnregisterMojoInterfaces(
    blink::AssociatedInterfaceRegistry* associated_interfaces) {
  associated_interfaces->RemoveInterface(
      shunya::mojom::ShunyaRendererConfiguration::Name_);
}

void ShunyaRenderThreadObserver::OnRendererConfigurationAssociatedRequest(
    mojo::PendingAssociatedReceiver<shunya::mojom::ShunyaRendererConfiguration>
        receiver) {
  renderer_configuration_receivers_.Add(this, std::move(receiver));
}

void ShunyaRenderThreadObserver::SetInitialConfiguration(bool is_tor_process) {
  is_tor_process_ = is_tor_process;
}

void ShunyaRenderThreadObserver::SetConfiguration(
    shunya::mojom::DynamicParamsPtr params) {
  *GetDynamicConfigParams() = std::move(*params);
}

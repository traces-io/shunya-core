/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_RENDERER_SHUNYA_URL_LOADER_THROTTLE_PROVIDER_IMPL_H_
#define SHUNYA_RENDERER_SHUNYA_URL_LOADER_THROTTLE_PROVIDER_IMPL_H_

#include <memory>

#include "base/memory/raw_ptr.h"
#include "chrome/renderer/url_loader_throttle_provider_impl.h"

class ShunyaContentRendererClient;

class ShunyaURLLoaderThrottleProviderImpl
    : public URLLoaderThrottleProviderImpl {
 public:
  ShunyaURLLoaderThrottleProviderImpl(
      blink::ThreadSafeBrowserInterfaceBrokerProxy* broker,
      blink::URLLoaderThrottleProviderType type,
      ChromeContentRendererClient* chrome_content_renderer_client);
  ~ShunyaURLLoaderThrottleProviderImpl() override;

  ShunyaURLLoaderThrottleProviderImpl(
      const ShunyaURLLoaderThrottleProviderImpl&) = delete;
  ShunyaURLLoaderThrottleProviderImpl& operator=(
      const ShunyaURLLoaderThrottleProviderImpl&) = delete;

  // blink::URLLoaderThrottleProvider implementation.
  blink::WebVector<std::unique_ptr<blink::URLLoaderThrottle>> CreateThrottles(
      int render_frame_id,
      const blink::WebURLRequest& request) override;

 private:
  raw_ptr<ShunyaContentRendererClient> shunya_content_renderer_client_ = nullptr;
};

#endif  // SHUNYA_RENDERER_SHUNYA_URL_LOADER_THROTTLE_PROVIDER_IMPL_H_

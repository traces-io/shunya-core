/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/renderer/shunya_url_loader_throttle_provider_impl.h"

#include <utility>

#include "shunya/components/tor/buildflags/buildflags.h"
#include "shunya/renderer/shunya_content_renderer_client.h"

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/components/tor/renderer/onion_domain_throttle.h"
#endif

ShunyaURLLoaderThrottleProviderImpl::ShunyaURLLoaderThrottleProviderImpl(
    blink::ThreadSafeBrowserInterfaceBrokerProxy* broker,
    blink::URLLoaderThrottleProviderType type,
    ChromeContentRendererClient* chrome_content_renderer_client)
    : URLLoaderThrottleProviderImpl(broker,
                                    type,
                                    chrome_content_renderer_client),
      shunya_content_renderer_client_(static_cast<ShunyaContentRendererClient*>(
          chrome_content_renderer_client)) {}

ShunyaURLLoaderThrottleProviderImpl::~ShunyaURLLoaderThrottleProviderImpl() =
    default;

blink::WebVector<std::unique_ptr<blink::URLLoaderThrottle>>
ShunyaURLLoaderThrottleProviderImpl::CreateThrottles(
    int render_frame_id,
    const blink::WebURLRequest& request) {
  auto throttles =
      URLLoaderThrottleProviderImpl::CreateThrottles(render_frame_id, request);
#if BUILDFLAG(ENABLE_TOR)
  if (auto onion_domain_throttle =
          tor::OnionDomainThrottle::MaybeCreateThrottle(
              shunya_content_renderer_client_->IsTorProcess())) {
    throttles.emplace_back(std::move(onion_domain_throttle));
  }
#endif
  return throttles;
}

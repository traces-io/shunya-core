/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_RENDERER_SHUNYA_RENDER_THREAD_OBSERVER_H_
#define SHUNYA_RENDERER_SHUNYA_RENDER_THREAD_OBSERVER_H_

#include "shunya/common/shunya_renderer_configuration.mojom.h"
#include "content/public/renderer/render_thread_observer.h"
#include "mojo/public/cpp/bindings/associated_receiver_set.h"
#include "mojo/public/cpp/bindings/pending_associated_receiver.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/receiver.h"

namespace blink {
class AssociatedInterfaceRegistry;
}

class ShunyaRenderThreadObserver
    : public content::RenderThreadObserver,
      public shunya::mojom::ShunyaRendererConfiguration {
 public:
  ShunyaRenderThreadObserver(const ShunyaRenderThreadObserver&) = delete;
  ShunyaRenderThreadObserver& operator=(const ShunyaRenderThreadObserver&) =
      delete;
  ShunyaRenderThreadObserver();
  ~ShunyaRenderThreadObserver() override;

  bool is_tor_process() const { return is_tor_process_; }

  // Return the dynamic parameters - those that may change while the
  // render process is running.
  static const shunya::mojom::DynamicParams& GetDynamicParams();

 private:
  // content::RenderThreadObserver:
  void RegisterMojoInterfaces(
      blink::AssociatedInterfaceRegistry* associated_interfaces) override;
  void UnregisterMojoInterfaces(
      blink::AssociatedInterfaceRegistry* associated_interfaces) override;

  // shunya::mojom::ShunyaRendererConfiguration:
  void SetInitialConfiguration(bool is_tor_process) override;
  void SetConfiguration(shunya::mojom::DynamicParamsPtr params) override;

  void OnRendererConfigurationAssociatedRequest(
      mojo::PendingAssociatedReceiver<shunya::mojom::ShunyaRendererConfiguration>
          receiver);

  bool is_tor_process_ = false;
  mojo::AssociatedReceiverSet<shunya::mojom::ShunyaRendererConfiguration>
      renderer_configuration_receivers_;
};

#endif  // SHUNYA_RENDERER_SHUNYA_RENDER_THREAD_OBSERVER_H_

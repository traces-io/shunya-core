/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_RENDERER_SHUNYA_CONTENT_RENDERER_CLIENT_H_
#define SHUNYA_RENDERER_SHUNYA_CONTENT_RENDERER_CLIENT_H_

#include <memory>

#include "shunya/components/shunya_search/renderer/shunya_search_service_worker_holder.h"
#include "chrome/renderer/chrome_content_renderer_client.h"
#include "v8/include/v8.h"

class ShunyaRenderThreadObserver;
class GURL;

namespace blink {
class WebServiceWorkerContextProxy;
}

class ShunyaContentRendererClient : public ChromeContentRendererClient {
 public:
  ShunyaContentRendererClient();
  ShunyaContentRendererClient(const ShunyaContentRendererClient&) = delete;
  ShunyaContentRendererClient& operator=(const ShunyaContentRendererClient&) =
      delete;
  ~ShunyaContentRendererClient() override;

  void RenderThreadStarted() override;
  void SetRuntimeFeaturesDefaultsBeforeBlinkInitialization() override;
  void RenderFrameCreated(content::RenderFrame* render_frame) override;
  void GetSupportedKeySystems(media::GetSupportedKeySystemsCB cb) override;
  void RunScriptsAtDocumentStart(content::RenderFrame* render_frame) override;
  void WillEvaluateServiceWorkerOnWorkerThread(
      blink::WebServiceWorkerContextProxy* context_proxy,
      v8::Local<v8::Context> v8_context,
      int64_t service_worker_version_id,
      const GURL& service_worker_scope,
      const GURL& script_url) override;
  void WillDestroyServiceWorkerContextOnWorkerThread(
      v8::Local<v8::Context> v8_context,
      int64_t service_worker_version_id,
      const GURL& service_worker_scope,
      const GURL& script_url) override;
  std::unique_ptr<blink::URLLoaderThrottleProvider>
  CreateURLLoaderThrottleProvider(
      blink::URLLoaderThrottleProviderType provider_type) override;

  bool IsTorProcess() const;

 private:
  std::unique_ptr<ShunyaRenderThreadObserver> shunya_observer_;
  shunya_search::ShunyaSearchServiceWorkerHolder
      shunya_search_service_worker_holder_;
};

#endif  // SHUNYA_RENDERER_SHUNYA_CONTENT_RENDERER_CLIENT_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_RENDERER_SHUNYA_WALLET_SHUNYA_WALLET_RENDER_FRAME_OBSERVER_P3A_UTIL_H_
#define SHUNYA_RENDERER_SHUNYA_WALLET_SHUNYA_WALLET_RENDER_FRAME_OBSERVER_P3A_UTIL_H_

#include "shunya/common/shunya_renderer_configuration.mojom.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "content/public/renderer/render_frame.h"
#include "mojo/public/cpp/bindings/remote.h"

namespace v8 {
class Context;
template <class T>
class Local;
class Isolate;
}  // namespace v8

namespace shunya_wallet {

class ShunyaWalletRenderFrameObserverP3AUtil {
 public:
  ShunyaWalletRenderFrameObserverP3AUtil();
  ~ShunyaWalletRenderFrameObserverP3AUtil();

  void ReportJSProviders(content::RenderFrame* render_frame,
                         const shunya::mojom::DynamicParams& dynamic_params);

 private:
  bool EnsureConnected(content::RenderFrame* render_frame);

  void ReportJSProvider(v8::Isolate* isolate,
                        v8::Local<v8::Context>& context,
                        mojom::CoinType coin_type,
                        const char* provider_object_key,
                        bool allow_provider_overwrite);

  mojo::Remote<shunya_wallet::mojom::ShunyaWalletP3A> shunya_wallet_p3a_;
};

}  // namespace shunya_wallet

#endif  // SHUNYA_RENDERER_SHUNYA_WALLET_SHUNYA_WALLET_RENDER_FRAME_OBSERVER_P3A_UTIL_H_

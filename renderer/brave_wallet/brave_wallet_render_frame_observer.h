/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_RENDERER_SHUNYA_WALLET_SHUNYA_WALLET_RENDER_FRAME_OBSERVER_H_
#define SHUNYA_RENDERER_SHUNYA_WALLET_SHUNYA_WALLET_RENDER_FRAME_OBSERVER_H_

#include "shunya/common/shunya_renderer_configuration.mojom.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "shunya/components/shunya_wallet/renderer/js_ethereum_provider.h"
#include "shunya/components/shunya_wallet/renderer/js_solana_provider.h"
#include "shunya/renderer/shunya_wallet/shunya_wallet_render_frame_observer_p3a_util.h"
#include "content/public/renderer/render_frame.h"
#include "content/public/renderer/render_frame_observer.h"
#include "third_party/blink/public/web/web_navigation_type.h"
#include "url/gurl.h"
#include "v8/include/v8.h"

namespace shunya_wallet {

class ShunyaWalletRenderFrameObserver : public content::RenderFrameObserver {
 public:
  using GetDynamicParamsCallback =
      base::RepeatingCallback<const shunya::mojom::DynamicParams&()>;

  explicit ShunyaWalletRenderFrameObserver(
      content::RenderFrame* render_frame,
      GetDynamicParamsCallback get_dynamic_params_callback);
  ~ShunyaWalletRenderFrameObserver() override;

  // RenderFrameObserver implementation.
  void DidStartNavigation(
      const GURL& url,
      absl::optional<blink::WebNavigationType> navigation_type) override;
  void DidClearWindowObject() override;

  void DidFinishLoad() override;

 private:
  // RenderFrameObserver implementation.
  void OnDestruct() override;

  bool IsPageValid();
  bool CanCreateProvider();

  GURL url_;
  GetDynamicParamsCallback get_dynamic_params_callback_;

  ShunyaWalletRenderFrameObserverP3AUtil p3a_util_;
};

}  // namespace shunya_wallet

#endif  // SHUNYA_RENDERER_SHUNYA_WALLET_SHUNYA_WALLET_RENDER_FRAME_OBSERVER_H_

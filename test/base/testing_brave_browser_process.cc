/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/test/base/testing_shunya_browser_process.h"

#include <utility>

#include "shunya/components/shunya_shields/browser/ad_block_service.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/tor/buildflags/buildflags.h"

namespace tor {
class ShunyaTorClientUpdater;
}

namespace ipfs {
class ShunyaIpfsClientUpdater;
}

// static
TestingShunyaBrowserProcess* TestingShunyaBrowserProcess::GetGlobal() {
  return static_cast<TestingShunyaBrowserProcess*>(g_shunya_browser_process);
}

// static
void TestingShunyaBrowserProcess::CreateInstance() {
  DCHECK(!g_shunya_browser_process);
  TestingShunyaBrowserProcess* process = new TestingShunyaBrowserProcess;
  g_shunya_browser_process = process;
}

// static
void TestingShunyaBrowserProcess::DeleteInstance() {
  ShunyaBrowserProcess* browser_process = g_shunya_browser_process;
  g_shunya_browser_process = nullptr;
  delete browser_process;
}

// static
void TestingShunyaBrowserProcess::StartTearDown() {}

// static
void TestingShunyaBrowserProcess::TearDownAndDeleteInstance() {
  TestingShunyaBrowserProcess::StartTearDown();
  TestingShunyaBrowserProcess::DeleteInstance();
}

TestingShunyaBrowserProcess::TestingShunyaBrowserProcess() = default;

TestingShunyaBrowserProcess::~TestingShunyaBrowserProcess() = default;

void TestingShunyaBrowserProcess::StartShunyaServices() {}

shunya_shields::AdBlockService* TestingShunyaBrowserProcess::ad_block_service() {
  DCHECK(ad_block_service_);
  return ad_block_service_.get();
}

#if BUILDFLAG(ENABLE_GREASELION)
greaselion::GreaselionDownloadService*
TestingShunyaBrowserProcess::greaselion_download_service() {
  NOTREACHED();
  return nullptr;
}
#endif

debounce::DebounceComponentInstaller*
TestingShunyaBrowserProcess::debounce_component_installer() {
  NOTREACHED();
  return nullptr;
}

#if BUILDFLAG(ENABLE_REQUEST_OTR)
request_otr::RequestOTRComponentInstallerPolicy*
TestingShunyaBrowserProcess::request_otr_component_installer() {
  NOTREACHED();
  return nullptr;
}
#endif

shunya::URLSanitizerComponentInstaller*
TestingShunyaBrowserProcess::URLSanitizerComponentInstaller() {
  NOTREACHED();
  return nullptr;
}

https_upgrade_exceptions::HttpsUpgradeExceptionsService*
TestingShunyaBrowserProcess::https_upgrade_exceptions_service() {
  return nullptr;
}

localhost_permission::LocalhostPermissionComponent*
TestingShunyaBrowserProcess::localhost_permission_component() {
  return nullptr;
}

shunya_shields::HTTPSEverywhereService*
TestingShunyaBrowserProcess::https_everywhere_service() {
  NOTREACHED();
  return nullptr;
}

shunya_component_updater::LocalDataFilesService*
TestingShunyaBrowserProcess::local_data_files_service() {
  NOTREACHED();
  return nullptr;
}

#if BUILDFLAG(ENABLE_TOR)
tor::ShunyaTorClientUpdater* TestingShunyaBrowserProcess::tor_client_updater() {
  return nullptr;
}

tor::ShunyaTorPluggableTransportUpdater*
TestingShunyaBrowserProcess::tor_pluggable_transport_updater() {
  return nullptr;
}
#endif

#if BUILDFLAG(ENABLE_IPFS)
ipfs::ShunyaIpfsClientUpdater*
TestingShunyaBrowserProcess::ipfs_client_updater() {
  return nullptr;
}
#endif

p3a::P3AService* TestingShunyaBrowserProcess::p3a_service() {
  NOTREACHED();
  return nullptr;
}

shunya::ShunyaReferralsService*
TestingShunyaBrowserProcess::shunya_referrals_service() {
  NOTREACHED();
  return nullptr;
}

shunya_stats::ShunyaStatsUpdater*
TestingShunyaBrowserProcess::shunya_stats_updater() {
  NOTREACHED();
  return nullptr;
}

shunya_ads::ShunyaStatsHelper*
TestingShunyaBrowserProcess::ads_shunya_stats_helper() {
  NOTREACHED();
  return nullptr;
}

ntp_background_images::NTPBackgroundImagesService*
TestingShunyaBrowserProcess::ntp_background_images_service() {
  NOTREACHED();
  return nullptr;
}

#if BUILDFLAG(ENABLE_SPEEDREADER)
speedreader::SpeedreaderRewriterService*
TestingShunyaBrowserProcess::speedreader_rewriter_service() {
  NOTREACHED();
  return nullptr;
}
#endif

shunya_ads::ResourceComponent* TestingShunyaBrowserProcess::resource_component() {
  NOTREACHED();
  return nullptr;
}

shunya::ShunyaFarblingService*
TestingShunyaBrowserProcess::shunya_farbling_service() {
  NOTREACHED();
  return nullptr;
}

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
shunya_vpn::ShunyaVPNOSConnectionAPI*
TestingShunyaBrowserProcess::shunya_vpn_os_connection_api() {
  return nullptr;
}
#endif

misc_metrics::ProcessMiscMetrics*
TestingShunyaBrowserProcess::process_misc_metrics() {
  NOTREACHED();
  return nullptr;
}

void TestingShunyaBrowserProcess::SetAdBlockService(
    std::unique_ptr<shunya_shields::AdBlockService> service) {
  ad_block_service_ = std::move(service);
}

///////////////////////////////////////////////////////////////////////////////

TestingShunyaBrowserProcessInitializer::TestingShunyaBrowserProcessInitializer() {
  TestingShunyaBrowserProcess::CreateInstance();
}

TestingShunyaBrowserProcessInitializer::
    ~TestingShunyaBrowserProcessInitializer() {
  TestingShunyaBrowserProcess::DeleteInstance();
}

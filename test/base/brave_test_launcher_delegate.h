/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_TEST_BASE_SHUNYA_TEST_LAUNCHER_DELEGATE_H_
#define SHUNYA_TEST_BASE_SHUNYA_TEST_LAUNCHER_DELEGATE_H_

#include "chrome/test/base/chrome_test_launcher.h"

class ShunyaTestLauncherDelegate : public ChromeTestLauncherDelegate {
 public:
  // Does not take ownership of ChromeTestSuiteRunner.
  explicit ShunyaTestLauncherDelegate(ChromeTestSuiteRunner* runner);
  ShunyaTestLauncherDelegate(const ShunyaTestLauncherDelegate&) = delete;
  ShunyaTestLauncherDelegate& operator=(const ShunyaTestLauncherDelegate&) =
      delete;
  ~ShunyaTestLauncherDelegate() override;

 private:
  // ChromeLauncherDelegate:
  content::ContentMainDelegate* CreateContentMainDelegate() override;
};

#endif  // SHUNYA_TEST_BASE_SHUNYA_TEST_LAUNCHER_DELEGATE_H_

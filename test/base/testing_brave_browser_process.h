/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

// An implementation of ShunyaBrowserProcess for unit tests that fails for most
// services. By preventing creation of services, we reduce dependencies and
// keep the profile clean. Clients of this class must handle the NULL return
// value, however.

#ifndef SHUNYA_TEST_BASE_TESTING_SHUNYA_BROWSER_PROCESS_H_
#define SHUNYA_TEST_BASE_TESTING_SHUNYA_BROWSER_PROCESS_H_

#include <stdint.h>

#include <memory>
#include <string>

#include "shunya/browser/shunya_browser_process.h"

namespace shunya_shields {
class AdBlockService;
}

class TestingShunyaBrowserProcess : public ShunyaBrowserProcess {
 public:
  // Initializes |g_shunya_browser_process| with a new
  // TestingShunyaBrowserProcess.
  static void CreateInstance();

  // Cleanly destroys |g_shunya_browser_process|.
  static void DeleteInstance();

  // Convenience method to get g_shunya_browser_process as a
  // TestingShunyaBrowserProcess*.
  static TestingShunyaBrowserProcess* GetGlobal();

  // Convenience method to both teardown and destroy the TestingBrowserProcess
  // instance
  static void TearDownAndDeleteInstance();

  TestingShunyaBrowserProcess(const TestingShunyaBrowserProcess&) = delete;
  TestingShunyaBrowserProcess& operator=(const TestingShunyaBrowserProcess&) =
      delete;

  // ShunyaBrowserProcess overrides:
  void StartShunyaServices() override;
  shunya_shields::AdBlockService* ad_block_service() override;
#if BUILDFLAG(ENABLE_GREASELION)
  greaselion::GreaselionDownloadService* greaselion_download_service() override;
#endif
  debounce::DebounceComponentInstaller* debounce_component_installer() override;
#if BUILDFLAG(ENABLE_REQUEST_OTR)
  request_otr::RequestOTRComponentInstallerPolicy*
  request_otr_component_installer() override;
#endif
  shunya::URLSanitizerComponentInstaller* URLSanitizerComponentInstaller()
      override;
  shunya_shields::HTTPSEverywhereService* https_everywhere_service() override;
  https_upgrade_exceptions::HttpsUpgradeExceptionsService*
  https_upgrade_exceptions_service() override;
  localhost_permission::LocalhostPermissionComponent*
  localhost_permission_component() override;
  shunya_component_updater::LocalDataFilesService* local_data_files_service()
      override;
#if BUILDFLAG(ENABLE_TOR)
  tor::ShunyaTorClientUpdater* tor_client_updater() override;
  tor::ShunyaTorPluggableTransportUpdater* tor_pluggable_transport_updater()
      override;
#endif
#if BUILDFLAG(ENABLE_IPFS)
  ipfs::ShunyaIpfsClientUpdater* ipfs_client_updater() override;
#endif
  p3a::P3AService* p3a_service() override;
  shunya::ShunyaReferralsService* shunya_referrals_service() override;
  shunya_stats::ShunyaStatsUpdater* shunya_stats_updater() override;
  shunya_ads::ShunyaStatsHelper* ads_shunya_stats_helper() override;
  ntp_background_images::NTPBackgroundImagesService*
  ntp_background_images_service() override;
#if BUILDFLAG(ENABLE_SPEEDREADER)
  speedreader::SpeedreaderRewriterService* speedreader_rewriter_service()
      override;
#endif
  shunya_ads::ResourceComponent* resource_component() override;
  shunya::ShunyaFarblingService* shunya_farbling_service() override;
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  shunya_vpn::ShunyaVPNOSConnectionAPI* shunya_vpn_os_connection_api() override;
#endif
  misc_metrics::ProcessMiscMetrics* process_misc_metrics() override;

  // Populate the mock process with services. Consumer is responsible for
  // cleaning these up after completion of a test.
  void SetAdBlockService(std::unique_ptr<shunya_shields::AdBlockService>);

 private:
  // Perform necessary cleanup prior to destruction of |g_browser_process|
  static void StartTearDown();

  // See CreateInstance() and DestroyInstance() above.
  TestingShunyaBrowserProcess();
  ~TestingShunyaBrowserProcess() override;

  std::unique_ptr<shunya_shields::AdBlockService> ad_block_service_;
};

class TestingShunyaBrowserProcessInitializer {
 public:
  TestingShunyaBrowserProcessInitializer();
  TestingShunyaBrowserProcessInitializer(
      const TestingShunyaBrowserProcessInitializer&) = delete;
  TestingShunyaBrowserProcessInitializer& operator=(
      const TestingShunyaBrowserProcessInitializer&) = delete;
  ~TestingShunyaBrowserProcessInitializer();
};

#endif  // SHUNYA_TEST_BASE_TESTING_SHUNYA_BROWSER_PROCESS_H_

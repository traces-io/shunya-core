/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_TEST_BASE_SHUNYA_TESTING_PROFILE_H_
#define SHUNYA_TEST_BASE_SHUNYA_TESTING_PROFILE_H_

#include "chrome/test/base/testing_profile.h"

class ShunyaTestingProfile : public TestingProfile {
 public:
  ShunyaTestingProfile();
  ShunyaTestingProfile(const base::FilePath& path, Delegate* delegate);
  ~ShunyaTestingProfile() override = default;
};

#endif  // SHUNYA_TEST_BASE_SHUNYA_TESTING_PROFILE_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_TEST_BASE_SHUNYA_UNIT_TEST_SUITE_H_
#define SHUNYA_TEST_BASE_SHUNYA_UNIT_TEST_SUITE_H_

#include <memory>

#include "base/compiler_specific.h"
#include "base/files/file_path.h"
#include "base/test/test_discardable_memory_allocator.h"
#include "chrome/test/base/chrome_unit_test_suite.h"

class ShunyaUnitTestSuite : public ChromeUnitTestSuite {
 public:
  ShunyaUnitTestSuite(int argc, char** argv);
  ShunyaUnitTestSuite(const ShunyaUnitTestSuite&) = delete;
  ShunyaUnitTestSuite& operator=(const ShunyaUnitTestSuite&) = delete;

 protected:
  // base::TestSuite overrides:
  void Initialize() override;
};

#endif  // SHUNYA_TEST_BASE_SHUNYA_UNIT_TEST_SUITE_H_

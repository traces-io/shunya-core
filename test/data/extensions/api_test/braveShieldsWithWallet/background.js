/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

function testBasics() {
  chrome.test.runTests([
    function shunyaShieldsHasWalletAccessButNotSeed() {
      if (chrome.shunyaWallet && !chrome.shunyaWallet.getWalletSeed &&
          !chrome.shunyaWallet.getProjectID &&
          !chrome.shunyaWallet.getShunyaKey) {
        chrome.test.succeed();
      } else {
        chrome.test.fail();
      }
    },
  ]);
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaContentSettingsResourcesClassAdapter extends ShunyaClassVisitor {
    static String sContentSettingsResourcesClassName =
            "org/chromium/components/browser_ui/site_settings/ContentSettingsResources";
    static String sShunyaContentSettingsResourcesClassName =
            "org/chromium/components/browser_ui/site_settings/ShunyaContentSettingsResources";
    static String sContentSettingsResourcesResourceItemClassName =
            "org/chromium/components/browser_ui/site_settings/ContentSettingsResources$ResourceItem";
    static String sShunyaContentSettingsResourcesResourceItemClassName =
            "org/chromium/components/browser_ui/site_settings/ShunyaContentSettingsResources$ResourceItem";

    public ShunyaContentSettingsResourcesClassAdapter(ClassVisitor visitor) {
        super(visitor);

        makePublicMethod(sContentSettingsResourcesClassName, "getResourceItem");
        changeMethodOwner(sContentSettingsResourcesClassName, "getResourceItem",
                sShunyaContentSettingsResourcesClassName);
        makePublicInnerClass(sContentSettingsResourcesClassName, "ResourceItem");
        redirectConstructor(sShunyaContentSettingsResourcesResourceItemClassName,
                sContentSettingsResourcesResourceItemClassName);
        redirectTypeInMethod(sShunyaContentSettingsResourcesClassName, "getResourceItem",
                sShunyaContentSettingsResourcesResourceItemClassName,
                sContentSettingsResourcesResourceItemClassName);
    }
}

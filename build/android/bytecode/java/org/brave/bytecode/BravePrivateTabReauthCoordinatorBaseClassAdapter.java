/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaPrivateTabReauthCoordinatorBaseClassAdapter extends ShunyaClassVisitor {
    static String sTabSwitcherIncognitoReauthCoordinatorClassName =
            "org/chromium/chrome/browser/incognito/reauth/TabSwitcherIncognitoReauthCoordinator";

    static String sFullScreenIncognitoReauthCoordinatorClassName =
            "org/chromium/chrome/browser/incognito/reauth/FullScreenIncognitoReauthCoordinator";

    static String sIncognitoReauthCoordinatorBaseClassName =
            "org/chromium/chrome/browser/incognito/reauth/IncognitoReauthCoordinatorBase";

    static String sShunyaPrivateTabReauthCoordinatorBaseClassName =
            "org/chromium/chrome/browser/incognito/reauth/ShunyaPrivateTabReauthCoordinatorBase";

    public ShunyaPrivateTabReauthCoordinatorBaseClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sTabSwitcherIncognitoReauthCoordinatorClassName,
                sShunyaPrivateTabReauthCoordinatorBaseClassName);

        changeSuperName(sFullScreenIncognitoReauthCoordinatorClassName,
                sShunyaPrivateTabReauthCoordinatorBaseClassName);

        deleteField(sShunyaPrivateTabReauthCoordinatorBaseClassName, "mIncognitoReauthView");
        makeProtectedField(sIncognitoReauthCoordinatorBaseClassName, "mIncognitoReauthView");
    }
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaReturnToChromeUtilClassAdapter extends ShunyaClassVisitor {
    static String sReturnToChromeUtilClassName =
            "org/chromium/chrome/browser/tasks/ReturnToChromeUtil";
    static String sShunyaReturnToChromeUtilClassName =
            "org/chromium/chrome/browser/tasks/ShunyaReturnToChromeUtil";

    public ShunyaReturnToChromeUtilClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeMethodOwner(sReturnToChromeUtilClassName, "shouldShowTabSwitcher",
                sShunyaReturnToChromeUtilClassName);
    }
}

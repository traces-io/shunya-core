/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaTabbedRootUiCoordinatorClassAdapter extends ShunyaClassVisitor {
    static String sTabbedRootUiCoordinatorClassName =
            "org/chromium/chrome/browser/tabbed_mode/TabbedRootUiCoordinator";
    static String sShunyaTabbedRootUiCoordinatorClassName =
            "org/chromium/chrome/browser/tabbed_mode/ShunyaTabbedRootUiCoordinator";

    public ShunyaTabbedRootUiCoordinatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(
                sTabbedRootUiCoordinatorClassName, sShunyaTabbedRootUiCoordinatorClassName);
    }
}

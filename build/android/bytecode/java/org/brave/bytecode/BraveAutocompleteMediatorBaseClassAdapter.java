/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaAutocompleteMediatorBaseClassAdapter extends ShunyaClassVisitor {
    static String sAutocompleteMediator =
            "org/chromium/chrome/browser/omnibox/suggestions/AutocompleteMediator";
    static String sShunyaAutocompleteMediatorBase =
            "org/chromium/chrome/browser/omnibox/suggestions/ShunyaAutocompleteMediatorBase";

    public ShunyaAutocompleteMediatorBaseClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sAutocompleteMediator, sShunyaAutocompleteMediatorBase);

        makeProtectedField(sAutocompleteMediator, "mContext");
        changeMethodOwner(
                sAutocompleteMediator, "loadUrlForOmniboxMatch", sShunyaAutocompleteMediatorBase);
    }
}

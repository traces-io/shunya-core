/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkToolbarClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkToolbarClassName =
            "org/chromium/chrome/browser/bookmarks/BookmarkToolbar";
    static String sShunyaBookmarkToolbarClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkToolbar";

    public ShunyaBookmarkToolbarClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sBookmarkToolbarClassName, sShunyaBookmarkToolbarClassName);
        deleteField(sShunyaBookmarkToolbarClassName, "mBookmarkModel");
        makeProtectedField(sBookmarkToolbarClassName, "mBookmarkModel");
    }
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaAppHooksClassAdapter extends ShunyaClassVisitor {
    static String sAppHooksClassName = "org/chromium/chrome/browser/AppHooksImpl";
    static String sShunyaAppHooksClassName = "org/chromium/chrome/browser/ShunyaAppHooks";

    public ShunyaAppHooksClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sAppHooksClassName, sShunyaAppHooksClassName);
    }
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaFourStateCookieSettingsPreferenceBaseClassAdapter extends ShunyaClassVisitor {
    static String sFourStateCookieSettingsPreferenceClassName =
            "org/chromium/components/browser_ui/site_settings/FourStateCookieSettingsPreference";
    static String sShunyaFourStateCookieSettingsPreferenceBaseClassName =
            "org/chromium/components/browser_ui/site_settings/ShunyaFourStateCookieSettingsPreferenceBase";

    public ShunyaFourStateCookieSettingsPreferenceBaseClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sFourStateCookieSettingsPreferenceClassName,
                sShunyaFourStateCookieSettingsPreferenceBaseClassName);
    }
}

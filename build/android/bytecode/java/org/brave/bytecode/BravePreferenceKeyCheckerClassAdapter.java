/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaPreferenceKeyCheckerClassAdapter extends ShunyaClassVisitor {
    static String sChromePreferenceKeyCheckerClassName =
            "org/chromium/chrome/browser/preferences/ChromePreferenceKeyChecker";

    static String sShunyaPreferenceKeyCheckerClassName =
            "org/chromium/chrome/browser/preferences/ShunyaPreferenceKeyChecker";

    public ShunyaPreferenceKeyCheckerClassAdapter(ClassVisitor visitor) {
        super(visitor);
        changeMethodOwner(sChromePreferenceKeyCheckerClassName, "getInstance",
                sShunyaPreferenceKeyCheckerClassName);
    }
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaNotificationPermissionRationaleDialogControllerClassAdapter
        extends ShunyaClassVisitor {
    static String sContoller =
            "org/chromium/chrome/browser/notifications/permissions/NotificationPermissionRationaleDialogController";
    static String sShunyaContoller =
            "org/chromium/chrome/browser/notifications/permissions/ShunyaNotificationPermissionRationaleDialogController";

    public ShunyaNotificationPermissionRationaleDialogControllerClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sContoller, sShunyaContoller);
        deleteMethod(sShunyaContoller, "wrapDialogDismissalCallback");
        makePublicMethod(sContoller, "wrapDialogDismissalCallback");
    }
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaIncognitoToggleTabLayoutClassAdapter extends ShunyaClassVisitor {
    static String sIncognitoToggleTabLayoutClassName =
            "org/chromium/chrome/browser/toolbar/IncognitoToggleTabLayout";
    static String sShunyaIncognitoToggleTabLayoutClassName =
            "org/chromium/chrome/browser/toolbar/ShunyaIncognitoToggleTabLayout";

    public ShunyaIncognitoToggleTabLayoutClassAdapter(ClassVisitor visitor) {
        super(visitor);

        deleteField(sShunyaIncognitoToggleTabLayoutClassName, "mIncognitoButtonIcon");
        makeProtectedField(sIncognitoToggleTabLayoutClassName, "mIncognitoButtonIcon");
    }
}

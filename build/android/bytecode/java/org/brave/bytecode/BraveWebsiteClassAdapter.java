/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaWebsiteClassAdapter extends ShunyaClassVisitor {
    static String sWebsiteClassName = "org/chromium/components/browser_ui/site_settings/Website";
    static String sShunyaWebsiteClassName =
            "org/chromium/components/browser_ui/site_settings/ShunyaWebsite";

    public ShunyaWebsiteClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sWebsiteClassName, sShunyaWebsiteClassName);

        makePrivateMethod(sWebsiteClassName, "setContentSetting");

        makePublicMethod(sShunyaWebsiteClassName, "setContentSetting");
        changeMethodOwner(sWebsiteClassName, "setContentSetting", sShunyaWebsiteClassName);
    }
}

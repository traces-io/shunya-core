/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaTabUiThemeProviderClassAdapter extends ShunyaClassVisitor {
    static String sTabUiThemeProviderClassName =
            "org/chromium/chrome/browser/tasks/tab_management/TabUiThemeProvider";
    static String sShunyaTabUiThemeProviderClassName =
            "org/chromium/chrome/browser/tasks/tab_management/ShunyaTabUiThemeProvider";

    public ShunyaTabUiThemeProviderClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeMethodOwner(sTabUiThemeProviderClassName, "getTitleTextColor",
                sShunyaTabUiThemeProviderClassName);

        changeMethodOwner(sTabUiThemeProviderClassName, "getActionButtonTintList",
                sShunyaTabUiThemeProviderClassName);

        changeMethodOwner(sTabUiThemeProviderClassName, "getCardViewBackgroundColor",
                sShunyaTabUiThemeProviderClassName);
    }
}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaHelpAndFeedbackLauncherImplClassAdapter extends ShunyaClassVisitor {
    static String sHelpAndFeedbackLauncherImplClassName =
            "org/chromium/chrome/browser/feedback/HelpAndFeedbackLauncherImpl";

    static String sShunyaHelpAndFeedbackLauncherImplClassName =
            "org/chromium/chrome/browser/feedback/ShunyaHelpAndFeedbackLauncherImpl";

    public ShunyaHelpAndFeedbackLauncherImplClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(
                sHelpAndFeedbackLauncherImplClassName, sShunyaHelpAndFeedbackLauncherImplClassName);
    }
}

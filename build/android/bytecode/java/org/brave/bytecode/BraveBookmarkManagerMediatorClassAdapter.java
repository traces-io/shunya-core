/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkManagerMediatorClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkManagerMediatorClassName =
            "org/chromium/chrome/browser/bookmarks/BookmarkManagerMediator";
    static String sShunyaBookmarkManagerMediatorClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkManagerMediator";

    public ShunyaBookmarkManagerMediatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(
                sBookmarkManagerMediatorClassName, sShunyaBookmarkManagerMediatorClassName);

        deleteField(sShunyaBookmarkManagerMediatorClassName, "mBookmarkModel");
        makeProtectedField(sBookmarkManagerMediatorClassName, "mBookmarkModel");
        deleteField(sShunyaBookmarkManagerMediatorClassName, "mContext");
        makeProtectedField(sBookmarkManagerMediatorClassName, "mContext");
    }
}

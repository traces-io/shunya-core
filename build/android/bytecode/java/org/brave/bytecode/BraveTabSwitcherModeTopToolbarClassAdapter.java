/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaTabSwitcherModeTopToolbarClassAdapter extends ShunyaClassVisitor {
    static String sTabSwitcherModeTopToolbarClassName =
            "org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar";
    static String sShunyaTabSwitcherModeTopToolbarClassName =
            "org/chromium/chrome/browser/toolbar/top/ShunyaTabSwitcherModeTopToolbar";

    public ShunyaTabSwitcherModeTopToolbarClassAdapter(ClassVisitor visitor) {
        super(visitor);

        deleteField(sShunyaTabSwitcherModeTopToolbarClassName, "mNewTabViewButton");
        makeProtectedField(sTabSwitcherModeTopToolbarClassName, "mNewTabViewButton");

        deleteField(sShunyaTabSwitcherModeTopToolbarClassName, "mNewTabImageButton");
        makeProtectedField(sTabSwitcherModeTopToolbarClassName, "mNewTabImageButton");

        deleteField(sShunyaTabSwitcherModeTopToolbarClassName, "mShouldShowNewTabVariation");
        makeProtectedField(sTabSwitcherModeTopToolbarClassName, "mShouldShowNewTabVariation");

        deleteField(sShunyaTabSwitcherModeTopToolbarClassName, "mIsIncognito");
        makeProtectedField(sTabSwitcherModeTopToolbarClassName, "mIsIncognito");

        makePublicMethod(sTabSwitcherModeTopToolbarClassName, "updateNewTabButtonVisibility");
        addMethodAnnotation(sShunyaTabSwitcherModeTopToolbarClassName,
                "updateNewTabButtonVisibility", "Ljava/lang/Override;");

        makePublicMethod(sTabSwitcherModeTopToolbarClassName, "getToolbarColorForCurrentState");
        addMethodAnnotation(sShunyaTabSwitcherModeTopToolbarClassName,
                "getToolbarColorForCurrentState", "Ljava/lang/Override;");

        makePublicMethod(sTabSwitcherModeTopToolbarClassName, "shouldShowIncognitoToggle");
        deleteMethod(sShunyaTabSwitcherModeTopToolbarClassName, "shouldShowIncognitoToggle");
    }
}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkDelegateClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkDelegateClassName =
            "org/chromium/chrome/browser/bookmarks/BookmarkDelegate";
    static String sShunyaBookmarkDelegateClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkDelegate";

    public ShunyaBookmarkDelegateClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sBookmarkDelegateClassName, sShunyaBookmarkDelegateClassName);
    }
}

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaHomepageManagerClassAdapter extends ShunyaClassVisitor {
    static String sHomepageManagerClassName = "org/chromium/chrome/browser/homepage/HomepageManager";
    static String sShunyaHomepageManagerClassName = "org/chromium/chrome/browser/homepage/ShunyaHomepageManager";

    public ShunyaHomepageManagerClassAdapter(ClassVisitor visitor) {
        super(visitor);
        changeMethodOwner(sHomepageManagerClassName, "shouldCloseAppWithZeroTabs", sShunyaHomepageManagerClassName);
    }
}

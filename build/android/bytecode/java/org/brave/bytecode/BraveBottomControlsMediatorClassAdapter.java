/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBottomControlsMediatorClassAdapter extends ShunyaClassVisitor {
    static String sBottomControlsMediatorClassName =
            "org/chromium/chrome/browser/toolbar/bottom/BottomControlsMediator";
    static String sShunyaBottomControlsMediatorClassName =
            "org/chromium/chrome/browser/toolbar/bottom/ShunyaBottomControlsMediator";

    public ShunyaBottomControlsMediatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(
                sBottomControlsMediatorClassName, sShunyaBottomControlsMediatorClassName);

        deleteField(sShunyaBottomControlsMediatorClassName, "mBottomControlsHeight");
        makeProtectedField(sBottomControlsMediatorClassName, "mBottomControlsHeight");

        deleteField(sShunyaBottomControlsMediatorClassName, "mModel");
        makeProtectedField(sBottomControlsMediatorClassName, "mModel");

        deleteField(sShunyaBottomControlsMediatorClassName, "mBrowserControlsSizer");
        makeProtectedField(sBottomControlsMediatorClassName, "mBrowserControlsSizer");
    }
}

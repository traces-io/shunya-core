/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaLauncherActivityClassAdapter extends ShunyaClassVisitor {
    static String sChromeLauncherActivityClassName =
            "org/chromium/chrome/browser/document/ChromeLauncherActivity";
    static String sShunyaLauncherActivityClassName =
            "org/chromium/chrome/browser/document/ShunyaLauncherActivity";

    public ShunyaLauncherActivityClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sChromeLauncherActivityClassName, sShunyaLauncherActivityClassName);
    }
}

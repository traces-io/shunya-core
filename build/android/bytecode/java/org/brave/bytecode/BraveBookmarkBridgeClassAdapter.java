/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkBridgeClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkBridgeClassName = "org/chromium/chrome/browser/bookmarks/BookmarkBridge";
    static String sShunyaBookmarkBridgeClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkBridge";

    public ShunyaBookmarkBridgeClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sBookmarkBridgeClassName, sShunyaBookmarkBridgeClassName);
        deleteField(sShunyaBookmarkBridgeClassName, "mNativeBookmarkBridge");
        makeProtectedField(sBookmarkBridgeClassName, "mNativeBookmarkBridge");
    }
}

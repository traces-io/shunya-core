/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaSearchEngineAdapterClassAdapter extends ShunyaClassVisitor {
    static String sSearchEngineAdapterClassName =
            "org/chromium/chrome/browser/search_engines/settings/SearchEngineAdapter";

    static String sShunyaSearchEngineAdapterClassName =
            "org/chromium/chrome/browser/search_engines/settings/ShunyaSearchEngineAdapter";

    static String sShunyaBaseSearchEngineAdapterClassName =
            "org/chromium/chrome/browser/search_engines/settings/ShunyaBaseSearchEngineAdapter";

    static String sSearchEngineSettingsClassName =
            "org/chromium/chrome/browser/search_engines/settings/SearchEngineSettings";

    static String sShunyaSearchEnginePreferenceClassName =
            "org/chromium/chrome/browser/search_engines/settings/ShunyaSearchEnginePreference";

    static String sMethodGetSearchEngineSourceType = "getSearchEngineSourceType";

    static String sMethodSortAndFilterUnnecessaryTemplateUrl =
            "sortAndFilterUnnecessaryTemplateUrl";

    public ShunyaSearchEngineAdapterClassAdapter(ClassVisitor visitor) {
        super(visitor);
        changeSuperName(sSearchEngineAdapterClassName, sShunyaBaseSearchEngineAdapterClassName);

        changeMethodOwner(sSearchEngineAdapterClassName, sMethodGetSearchEngineSourceType,
                sShunyaBaseSearchEngineAdapterClassName);

        changeMethodOwner(sSearchEngineAdapterClassName, sMethodSortAndFilterUnnecessaryTemplateUrl,
                sShunyaBaseSearchEngineAdapterClassName);

        deleteField(sShunyaSearchEngineAdapterClassName, "mProfile");
        makeProtectedField(sSearchEngineAdapterClassName, "mProfile");
    }
}

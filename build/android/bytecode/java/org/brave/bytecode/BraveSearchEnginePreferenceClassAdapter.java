/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaSearchEnginePreferenceClassAdapter extends ShunyaClassVisitor {
    static String sSearchEngineSettingsClassName =
            "org/chromium/chrome/browser/search_engines/settings/SearchEngineSettings";

    static String sShunyaSearchEnginePreferenceClassName =
            "org/chromium/chrome/browser/search_engines/settings/ShunyaSearchEnginePreference";

    public ShunyaSearchEnginePreferenceClassAdapter(ClassVisitor visitor) {
        super(visitor);

        deleteField(sShunyaSearchEnginePreferenceClassName, "mSearchEngineAdapter");
        makeProtectedField(sSearchEngineSettingsClassName, "mSearchEngineAdapter");

        deleteField(sShunyaSearchEnginePreferenceClassName, "mProfile");
        makeProtectedField(sSearchEngineSettingsClassName, "mProfile");

        makePublicMethod(sSearchEngineSettingsClassName, "createAdapterIfNecessary");
        addMethodAnnotation(sShunyaSearchEnginePreferenceClassName, "createAdapterIfNecessary",
                "Ljava/lang/Override;");
    }
}

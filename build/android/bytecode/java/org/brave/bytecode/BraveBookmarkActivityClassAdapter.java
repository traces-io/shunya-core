/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkActivityClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkActivityClassName =
            "org/chromium/chrome/browser/app/bookmarks/BookmarkActivity";
    static String sShunyaBookmarkActivityClassName =
            "org/chromium/chrome/browser/app/bookmarks/ShunyaBookmarkActivity";

    public ShunyaBookmarkActivityClassAdapter(ClassVisitor visitor) {
        super(visitor);

        deleteField(sShunyaBookmarkActivityClassName, "mBookmarkManagerCoordinator");
        makeProtectedField(sBookmarkActivityClassName, "mBookmarkManagerCoordinator");
    }
}

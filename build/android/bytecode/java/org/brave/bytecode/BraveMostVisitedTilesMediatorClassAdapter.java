/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaMostVisitedTilesMediatorClassAdapter extends ShunyaClassVisitor {
    static String sMostVisitedTilesMediatorClassName =
            "org/chromium/chrome/browser/suggestions/tile/MostVisitedTilesMediator";
    static String sShunyaMostVisitedTilesMediatorClassName =
            "org/chromium/chrome/browser/suggestions/tile/ShunyaMostVisitedTilesMediator";

    public ShunyaMostVisitedTilesMediatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(
                sMostVisitedTilesMediatorClassName, sShunyaMostVisitedTilesMediatorClassName);

        makePublicMethod(sMostVisitedTilesMediatorClassName, "updateTilePlaceholderVisibility");
        addMethodAnnotation(sShunyaMostVisitedTilesMediatorClassName,
                "updateTilePlaceholderVisibility", "Ljava/lang/Override;");

        deleteField(sShunyaMostVisitedTilesMediatorClassName, "mTileGroup");
        makeProtectedField(sMostVisitedTilesMediatorClassName, "mTileGroup");
    }
}

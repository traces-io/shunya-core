/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaAutocompleteMediatorClassAdapter extends ShunyaClassVisitor {
    static String sAutocompleteMediator =
            "org/chromium/chrome/browser/omnibox/suggestions/AutocompleteMediator";
    static String sShunyaAutocompleteMediator =
            "org/chromium/chrome/browser/omnibox/suggestions/ShunyaAutocompleteMediator";

    public ShunyaAutocompleteMediatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sAutocompleteMediator, sShunyaAutocompleteMediator);

        deleteField(sShunyaAutocompleteMediator, "mNativeInitialized");
        makeProtectedField(sAutocompleteMediator, "mNativeInitialized");

        deleteField(sShunyaAutocompleteMediator, "mDropdownViewInfoListManager");
        makeProtectedField(sAutocompleteMediator, "mDropdownViewInfoListManager");
    }
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaSiteSettingsPreferencesBaseClassAdapter extends ShunyaClassVisitor {
    static String sSiteSettingsClassName =
            "org/chromium/components/browser_ui/site_settings/SiteSettings";
    static String sShunyaSiteSettingsPreferencesBaseClassName =
            "org/chromium/components/browser_ui/site_settings/ShunyaSiteSettingsPreferencesBase";

    public ShunyaSiteSettingsPreferencesBaseClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sSiteSettingsClassName, sShunyaSiteSettingsPreferencesBaseClassName);
    }
}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaDownloadMessageUiControllerImplClassAdapter extends ShunyaClassVisitor {
    static String sDownloadMessageUiControllerImpl =
            "org/chromium/chrome/browser/download/DownloadMessageUiControllerImpl";

    static String sShunyaDownloadMessageUiControllerImpl =
            "org/chromium/chrome/browser/download/ShunyaDownloadMessageUiControllerImpl";

    public ShunyaDownloadMessageUiControllerImplClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sDownloadMessageUiControllerImpl, sShunyaDownloadMessageUiControllerImpl);
        changeMethodOwner(sDownloadMessageUiControllerImpl, "isVisibleToUser",
                sShunyaDownloadMessageUiControllerImpl);
    }
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaDropdownItemViewInfoListBuilderClassAdapter extends ShunyaClassVisitor {
    static String sDropdownItemViewInfoListBuilder =
            "org/chromium/chrome/browser/omnibox/suggestions/DropdownItemViewInfoListBuilder";

    static String sShunyaDropdownItemViewInfoListBuilder =
            "org/chromium/chrome/browser/omnibox/suggestions/ShunyaDropdownItemViewInfoListBuilder";

    public ShunyaDropdownItemViewInfoListBuilderClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(
                sDropdownItemViewInfoListBuilder, sShunyaDropdownItemViewInfoListBuilder);
    }
}

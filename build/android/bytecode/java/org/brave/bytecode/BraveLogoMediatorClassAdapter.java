/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaLogoMediatorClassAdapter extends ShunyaClassVisitor {
    static String sLogoMediator = "org/chromium/chrome/browser/logo/LogoMediator";
    static String sShunyaLogoMediator = "org/chromium/chrome/browser/logo/ShunyaLogoMediator";

    public ShunyaLogoMediatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sLogoMediator, sShunyaLogoMediator);

        deleteField(sShunyaLogoMediator, "mLogoModel");
        makeProtectedField(sLogoMediator, "mLogoModel");

        deleteField(sShunyaLogoMediator, "mShouldShowLogo");
        makeProtectedField(sLogoMediator, "mShouldShowLogo");

        makePublicMethod(sLogoMediator, "updateVisibility");
        addMethodAnnotation(sShunyaLogoMediator, "updateVisibility", "Ljava/lang/Override;");
    }
}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkManagerCoordinatorClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkManagerCoordinatorClassName =
            "org/chromium/chrome/browser/bookmarks/BookmarkManagerCoordinator";
    static String sShunyaBookmarkManagerCoordinatorClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkManagerCoordinator";

    public ShunyaBookmarkManagerCoordinatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(
                sBookmarkManagerCoordinatorClassName, sShunyaBookmarkManagerCoordinatorClassName);
        deleteField(sShunyaBookmarkManagerCoordinatorClassName, "mMediator");
        makeProtectedField(sBookmarkManagerCoordinatorClassName, "mMediator");
    }
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaPasswordSettingsBaseClassAdapter extends ShunyaClassVisitor {
    static String sPasswordSettingsClassName =
            "org/chromium/chrome/browser/password_manager/settings/PasswordSettings";
    static String sShunyaPasswordSettingsBaseClassName =
            "org/chromium/chrome/browser/password_manager/settings/ShunyaPasswordSettingsBase";

    public ShunyaPasswordSettingsBaseClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sPasswordSettingsClassName, sShunyaPasswordSettingsBaseClassName);

        changeMethodOwner(sPasswordSettingsClassName, "createCheckPasswords",
                sShunyaPasswordSettingsBaseClassName);
        deleteMethod(sPasswordSettingsClassName, "createCheckPasswords");
    }
}

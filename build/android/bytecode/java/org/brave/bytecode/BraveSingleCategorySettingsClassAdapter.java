/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaSingleCategorySettingsClassAdapter extends ShunyaClassVisitor {
    static String sSingleCategorySettingsClassName = "org/chromium/components/browser_ui/site_settings/SingleCategorySettings";
    static String sShunyaSingleCategorySettingsClassName = "org/chromium/components/browser_ui/site_settings/ShunyaSingleCategorySettings";

    public ShunyaSingleCategorySettingsClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sSingleCategorySettingsClassName, sShunyaSingleCategorySettingsClassName);
        changeMethodOwner(sSingleCategorySettingsClassName, "onOptionsItemSelected",
                sShunyaSingleCategorySettingsClassName);
        changeMethodOwner(sSingleCategorySettingsClassName, "getAddExceptionDialogMessage",
                sShunyaSingleCategorySettingsClassName);
        changeMethodOwner(sSingleCategorySettingsClassName, "resetList",
                sShunyaSingleCategorySettingsClassName);
    }
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaReaderModeManagerClassAdapter extends ShunyaClassVisitor {
    static String sReaderModeManagerClassName =
            "org/chromium/chrome/browser/dom_distiller/ReaderModeManager";
    static String sShunyaReaderModeManagerClassName =
            "org/chromium/chrome/browser/dom_distiller/ShunyaReaderModeManager";

    public ShunyaReaderModeManagerClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sReaderModeManagerClassName, sShunyaReaderModeManagerClassName);

        deleteField(sShunyaReaderModeManagerClassName, "mTab");
        makeProtectedField(sReaderModeManagerClassName, "mTab");
    }
}

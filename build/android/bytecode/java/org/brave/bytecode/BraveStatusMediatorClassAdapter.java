/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaStatusMediatorClassAdapter extends ShunyaClassVisitor {
    static String sStatusMediatorClassName =
            "org/chromium/chrome/browser/omnibox/status/StatusMediator";
    static String sShunyaStatusMediatorClassName =
            "org/chromium/chrome/browser/omnibox/status/ShunyaStatusMediator";

    public ShunyaStatusMediatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sStatusMediatorClassName, sShunyaStatusMediatorClassName);

        deleteField(sShunyaStatusMediatorClassName, "mUrlHasFocus");
        makeProtectedField(sStatusMediatorClassName, "mUrlHasFocus");
    }
}

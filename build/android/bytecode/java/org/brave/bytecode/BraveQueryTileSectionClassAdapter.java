/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaQueryTileSectionClassAdapter extends ShunyaClassVisitor {
    static String sQueryTileSectionClassName =
            "org/chromium/chrome/browser/query_tiles/QueryTileSection";
    static String sShunyaQueryTileSectionClassName =
            "org/chromium/chrome/browser/query_tiles/ShunyaQueryTileSection";

    public ShunyaQueryTileSectionClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeMethodOwner(sQueryTileSectionClassName, "getMaxRowsForMostVisitedTiles",
                sShunyaQueryTileSectionClassName);
    }
}

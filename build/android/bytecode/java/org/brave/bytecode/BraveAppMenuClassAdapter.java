/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaAppMenuClassAdapter extends ShunyaClassVisitor {
    static String sAppMenuClassName = "org/chromium/chrome/browser/ui/appmenu/AppMenu";

    static String sShunyaAppMenuClassName = "org/chromium/chrome/browser/ui/appmenu/ShunyaAppMenu";

    public ShunyaAppMenuClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sAppMenuClassName, sShunyaAppMenuClassName);

        changeMethodOwner(sAppMenuClassName, "getPopupPosition", sShunyaAppMenuClassName);

        makePublicMethod(sAppMenuClassName, "runMenuItemEnterAnimations");
        addMethodAnnotation(
                sShunyaAppMenuClassName, "runMenuItemEnterAnimations", "Ljava/lang/Override;");
    }
}

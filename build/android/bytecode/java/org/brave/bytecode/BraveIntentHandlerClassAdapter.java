/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaIntentHandlerClassAdapter extends ShunyaClassVisitor {
    static String sIntentHandlerClassName = "org/chromium/chrome/browser/IntentHandler";
    static String sShunyaIntentHandlerClassName = "org/chromium/chrome/browser/ShunyaIntentHandler";

    public ShunyaIntentHandlerClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sIntentHandlerClassName, sShunyaIntentHandlerClassName);

        makePublicMethod(sIntentHandlerClassName, "getUrlForCustomTab");
        changeMethodOwner(
                sShunyaIntentHandlerClassName, "getUrlForCustomTab", sIntentHandlerClassName);

        makePublicMethod(sIntentHandlerClassName, "getUrlForWebapp");
        changeMethodOwner(sShunyaIntentHandlerClassName, "getUrlForWebapp", sIntentHandlerClassName);

        makePublicMethod(sIntentHandlerClassName, "isJavascriptSchemeOrInvalidUrl");
        changeMethodOwner(sShunyaIntentHandlerClassName, "isJavascriptSchemeOrInvalidUrl",
                sIntentHandlerClassName);

        changeMethodOwner(
                sIntentHandlerClassName, "extractUrlFromIntent", sShunyaIntentHandlerClassName);
    }
}

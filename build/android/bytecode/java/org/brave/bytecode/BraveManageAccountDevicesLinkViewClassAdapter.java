/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaManageAccountDevicesLinkViewClassAdapter extends ShunyaClassVisitor {
    static String sManageAccountDevicesLinkView =
            "org/chromium/chrome/browser/share/send_tab_to_self/ManageAccountDevicesLinkView";
    static String sShunyaManageAccountDevicesLinkView =
            "org/chromium/chrome/browser/share/send_tab_to_self/ShunyaManageAccountDevicesLinkView";

    public ShunyaManageAccountDevicesLinkViewClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sManageAccountDevicesLinkView, sShunyaManageAccountDevicesLinkView);

        changeMethodOwner(sManageAccountDevicesLinkView, "inflateIfVisible",
                sShunyaManageAccountDevicesLinkView);
    }
}

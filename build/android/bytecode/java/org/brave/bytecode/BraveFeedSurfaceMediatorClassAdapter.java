/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaFeedSurfaceMediatorClassAdapter extends ShunyaClassVisitor {
    static String sFeedSurfaceMediatorClassName =
            "org/chromium/chrome/browser/feed/FeedSurfaceMediator";
    static String sShunyaFeedSurfaceMediatorClassName =
            "org/chromium/chrome/browser/feed/ShunyaFeedSurfaceMediator";

    public ShunyaFeedSurfaceMediatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sFeedSurfaceMediatorClassName, sShunyaFeedSurfaceMediatorClassName);

        deleteMethod(sShunyaFeedSurfaceMediatorClassName, "destroyPropertiesForStream");
        makePublicMethod(sFeedSurfaceMediatorClassName, "destroyPropertiesForStream");

        deleteField(sShunyaFeedSurfaceMediatorClassName, "mCoordinator");
        makeProtectedField(sFeedSurfaceMediatorClassName, "mCoordinator");

        deleteField(sShunyaFeedSurfaceMediatorClassName, "mSnapScrollHelper");
        makeProtectedField(sFeedSurfaceMediatorClassName, "mSnapScrollHelper");
    }
}

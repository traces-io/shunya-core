/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaTabSwitcherModeTTCoordinatorClassAdapter extends ShunyaClassVisitor {
    static String sTabSwitcherModeTTCoordinatorClassName =
            "org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTTCoordinator";
    static String sShunyaTabSwitcherModeTTCoordinatorClassName =
            "org/chromium/chrome/browser/toolbar/top/ShunyaTabSwitcherModeTTCoordinator";

    public ShunyaTabSwitcherModeTTCoordinatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        deleteField(sShunyaTabSwitcherModeTTCoordinatorClassName, "mActiveTabSwitcherToolbar");
        makeProtectedField(sTabSwitcherModeTTCoordinatorClassName, "mActiveTabSwitcherToolbar");
    }
}

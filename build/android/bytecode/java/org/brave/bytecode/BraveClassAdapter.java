/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaClassAdapter {
    public static ClassVisitor createAdapter(ClassVisitor chain) {
        chain = new ShunyaActivityClassAdapter(chain);
        chain = new ShunyaAppHooksClassAdapter(chain);
        chain = new ShunyaAppMenuClassAdapter(chain);
        chain = new ShunyaApplicationImplBaseClassAdapter(chain);
        chain = new ShunyaAutocompleteCoordinatorClassAdapter(chain);
        chain = new ShunyaAutocompleteMediatorBaseClassAdapter(chain);
        chain = new ShunyaAutocompleteMediatorClassAdapter(chain);
        chain = new ShunyaAutofillPopupBridgeClassAdapter(chain);
        chain = new ShunyaBookmarkActivityClassAdapter(chain);
        chain = new ShunyaBookmarkBridgeClassAdapter(chain);
        chain = new ShunyaBookmarkDelegateClassAdapter(chain);
        chain = new ShunyaBookmarkManagerCoordinatorClassAdapter(chain);
        chain = new ShunyaBookmarkManagerMediatorClassAdapter(chain);
        chain = new ShunyaBookmarkModelClassAdapter(chain);
        chain = new ShunyaBookmarkPageClassAdapter(chain);
        chain = new ShunyaBookmarkToolbarClassAdapter(chain);
        chain = new ShunyaBookmarkToolbarCoordinatorClassAdapter(chain);
        chain = new ShunyaBookmarkUtilsClassAdapter(chain);
        chain = new ShunyaBottomControlsCoordinatorClassAdapter(chain);
        chain = new ShunyaBottomControlsMediatorClassAdapter(chain);
        chain = new ShunyaCachedFlagClassAdapter(chain);
        chain = new ShunyaChromeContextMenuPopulatorAdapter(chain);
        chain = new ShunyaCommandLineInitUtilClassAdapter(chain);
        chain = new ShunyaContentSettingsResourcesClassAdapter(chain);
        chain = new ShunyaCustomizationProviderDelegateImplClassAdapter(chain);
        chain = new ShunyaDefaultBrowserPromoUtilsClassAdapter(chain);
        chain = new ShunyaDownloadMessageUiControllerImplClassAdapter(chain);
        chain = new ShunyaDropdownItemViewInfoListBuilderClassAdapter(chain);
        chain = new ShunyaDropdownItemViewInfoListManagerClassAdapter(chain);
        chain = new ShunyaDynamicColorsClassAdapter(chain);
        chain = new ShunyaEditUrlSuggestionProcessorClassAdapter(chain);
        chain = new ShunyaExternalNavigationHandlerClassAdapter(chain);
        chain = new ShunyaFeedSurfaceCoordinatorClassAdapter(chain);
        chain = new ShunyaFeedSurfaceMediatorClassAdapter(chain);
        chain = new ShunyaFourStateCookieSettingsPreferenceBaseClassAdapter(chain);
        chain = new ShunyaFreIntentCreatorClassAdapter(chain);
        chain = new ShunyaHelpAndFeedbackLauncherImplClassAdapter(chain);
        chain = new ShunyaHomepageManagerClassAdapter(chain);
        chain = new ShunyaIncognitoToggleTabLayoutClassAdapter(chain);
        chain = new ShunyaIntentHandlerClassAdapter(chain);
        chain = new ShunyaLauncherActivityClassAdapter(chain);
        chain = new ShunyaLaunchIntentDispatcherClassAdapter(chain);
        chain = new ShunyaLocationBarCoordinatorClassAdapter(chain);
        chain = new ShunyaLocationBarLayoutClassAdapter(chain);
        chain = new ShunyaLocationBarMediatorClassAdapter(chain);
        chain = new ShunyaLogoMediatorClassAdapter(chain);
        chain = new ShunyaMainPreferenceBaseClassAdapter(chain);
        chain = new ShunyaManageAccountDevicesLinkViewClassAdapter(chain);
        chain = new ShunyaManageSyncSettingsClassAdapter(chain);
        chain = new ShunyaTranslateCompactInfoBarBaseClassAdapter(chain);
        chain = new ShunyaMenuButtonCoordinatorClassAdapter(chain);
        chain = new ShunyaMimeUtilsClassAdapter(chain);
        chain = new ShunyaMostVisitedTilesMediatorClassAdapter(chain);
        chain = new ShunyaNewTabPageClassAdapter(chain);
        chain = new ShunyaNewTabPageLayoutClassAdapter(chain);
        chain = new ShunyaNotificationBuilderClassAdapter(chain);
        chain = new ShunyaNotificationManagerProxyImplClassAdapter(chain);
        chain = new ShunyaNotificationPermissionRationaleDialogControllerClassAdapter(chain);
        chain = new ShunyaPasswordSettingsBaseClassAdapter(chain);
        chain = new ShunyaPermissionDialogDelegateClassAdapter(chain);
        chain = new ShunyaPermissionDialogModelClassAdapter(chain);
        chain = new ShunyaPreferenceFragmentClassAdapter(chain);
        chain = new ShunyaPreferenceKeyCheckerClassAdapter(chain);
        chain = new ShunyaPrivateTabReauthCoordinatorBaseClassAdapter(chain);
        chain = new ShunyaPureJavaExceptionReporterClassAdapter(chain);
        chain = new ShunyaQueryTileSectionClassAdapter(chain);
        chain = new ShunyaReaderModeManagerClassAdapter(chain);
        chain = new ShunyaReturnToChromeUtilClassAdapter(chain);
        chain = new ShunyaSearchEngineAdapterClassAdapter(chain);
        chain = new ShunyaSearchEnginePreferenceClassAdapter(chain);
        chain = new ShunyaSettingsLauncherImplClassAdapter(chain);
        chain = new ShunyaShareDelegateImplClassAdapter(chain);
        chain = new ShunyaSingleCategorySettingsClassAdapter(chain);
        chain = new ShunyaSingleWebsiteSettingsClassAdapter(chain);
        chain = new ShunyaSiteSettingsCategoryClassAdapter(chain);
        chain = new ShunyaSiteSettingsDelegateClassAdapter(chain);
        chain = new ShunyaSiteSettingsPreferencesBaseClassAdapter(chain);
        chain = new ShunyaStartupPaintPreviewHelperClassAdapter(chain);
        chain = new ShunyaStatusMediatorClassAdapter(chain);
        chain = new ShunyaTabGroupUiCoordinatorClassAdapter(chain);
        chain = new ShunyaTabHelpersClassAdapter(chain);
        chain = new ShunyaTabSwitcherModeTTCoordinatorClassAdapter(chain);
        chain = new ShunyaTabSwitcherModeTopToolbarClassAdapter(chain);
        chain = new ShunyaTabUiThemeProviderClassAdapter(chain);
        chain = new ShunyaTabbedActivityClassAdapter(chain);
        chain = new ShunyaTabbedRootUiCoordinatorClassAdapter(chain);
        chain = new ShunyaTabGroupModelFilterClassAdapter(chain);
        chain = new ShunyaThemeUtilsClassAdapter(chain);
        chain = new ShunyaTileViewClassAdapter(chain);
        chain = new ShunyaToolbarLayoutClassAdapter(chain);
        chain = new ShunyaToolbarManagerClassAdapter(chain);
        chain = new ShunyaTopToolbarCoordinatorClassAdapter(chain);
        chain = new ShunyaVariationsSeedFetcherClassAdapter(chain);
        chain = new ShunyaWebsiteClassAdapter(chain);
        chain = new ShunyaWebsitePermissionsFetcherClassAdapter(chain);
        return chain;
    }
}

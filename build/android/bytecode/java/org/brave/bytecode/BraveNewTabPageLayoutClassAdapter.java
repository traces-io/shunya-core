/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaNewTabPageLayoutClassAdapter extends ShunyaClassVisitor {
    static String sNewTabPageLayoutClassName = "org/chromium/chrome/browser/ntp/NewTabPageLayout";
    static String sShunyaNewTabPageLayoutClassName =
            "org/chromium/chrome/browser/ntp/ShunyaNewTabPageLayout";
    static String sNewTabPageLayoutSuperClassName = "android/widget/FrameLayout";

    public ShunyaNewTabPageLayoutClassAdapter(ClassVisitor visitor) {
        super(visitor);

        deleteField(sShunyaNewTabPageLayoutClassName, "mMvTilesContainerLayout");
        makeProtectedField(sNewTabPageLayoutClassName, "mMvTilesContainerLayout");

        deleteField(sShunyaNewTabPageLayoutClassName, "mLogoCoordinator");
        makeProtectedField(sNewTabPageLayoutClassName, "mLogoCoordinator");

        makePublicMethod(sNewTabPageLayoutClassName, "insertSiteSectionView");
        addMethodAnnotation(
                sShunyaNewTabPageLayoutClassName, "insertSiteSectionView", "Ljava/lang/Override;");

        makePublicMethod(sNewTabPageLayoutClassName, "isScrollableMvtEnabled");
        addMethodAnnotation(
                sShunyaNewTabPageLayoutClassName, "isScrollableMvtEnabled", "Ljava/lang/Override;");

        changeSuperName(sNewTabPageLayoutClassName, sNewTabPageLayoutSuperClassName);
    }
}

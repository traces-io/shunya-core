/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaCustomizationProviderDelegateImplClassAdapter extends ShunyaClassVisitor {
    static String sCustomizationProviderDelegateImplClassName =
            "org/chromium/chrome/browser/partnercustomizations/CustomizationProviderDelegateImpl";
    static String sShunyaCustomizationProviderDelegateImplClassName =
            "org/chromium/chrome/browser/partnercustomizations/ShunyaCustomizationProviderDelegateImpl";

    public ShunyaCustomizationProviderDelegateImplClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sCustomizationProviderDelegateImplClassName,
                sShunyaCustomizationProviderDelegateImplClassName);
    }
}

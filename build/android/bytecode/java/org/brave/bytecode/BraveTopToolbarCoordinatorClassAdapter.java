/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaTopToolbarCoordinatorClassAdapter extends ShunyaClassVisitor {
    static String sTopToolbarCoordinatorClassName =
            "org/chromium/chrome/browser/toolbar/top/TopToolbarCoordinator";
    static String sShunyaTopToolbarCoordinatorClassName =
            "org/chromium/chrome/browser/toolbar/top/ShunyaTopToolbarCoordinator";

    public ShunyaTopToolbarCoordinatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sTopToolbarCoordinatorClassName, sShunyaTopToolbarCoordinatorClassName);

        deleteField(sShunyaTopToolbarCoordinatorClassName, "mTabSwitcherModeCoordinator");
        makeProtectedField(sTopToolbarCoordinatorClassName, "mTabSwitcherModeCoordinator");

        deleteField(sShunyaTopToolbarCoordinatorClassName, "mOptionalButtonController");
        makeProtectedField(sTopToolbarCoordinatorClassName, "mOptionalButtonController");

        deleteField(sShunyaTopToolbarCoordinatorClassName, "mToolbarColorObserverManager");
        makeProtectedField(sTopToolbarCoordinatorClassName, "mToolbarColorObserverManager");
    }
}

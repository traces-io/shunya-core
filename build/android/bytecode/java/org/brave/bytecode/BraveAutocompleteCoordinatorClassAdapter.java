/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaAutocompleteCoordinatorClassAdapter extends ShunyaClassVisitor {
    static String sAutocompleteCoordinator =
            "org/chromium/chrome/browser/omnibox/suggestions/AutocompleteCoordinator";

    static String sShunyaAutocompleteCoordinator =
            "org/chromium/chrome/browser/omnibox/suggestions/ShunyaAutocompleteCoordinator";

    public ShunyaAutocompleteCoordinatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sAutocompleteCoordinator, sShunyaAutocompleteCoordinator);
        changeMethodOwner(
                sAutocompleteCoordinator, "createViewProvider", sShunyaAutocompleteCoordinator);
    }
}

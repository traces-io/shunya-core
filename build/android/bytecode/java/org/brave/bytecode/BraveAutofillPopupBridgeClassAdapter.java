/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaAutofillPopupBridgeClassAdapter extends ShunyaClassVisitor {
    static String sAutofillPopupBridgeClassName =
            "org/chromium/chrome/browser/autofill/AutofillPopupBridge";
    static String sShunyaAutofillPopupBridgeClassName =
            "org/chromium/chrome/browser/autofill/ShunyaAutofillPopupBridge";

    public ShunyaAutofillPopupBridgeClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sAutofillPopupBridgeClassName, sShunyaAutofillPopupBridgeClassName);
    }
}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkToolbarCoordinatorClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkToolbarCoordinatorClassName =
            "org/chromium/chrome/browser/bookmarks/BookmarkToolbarCoordinator";
    static String sShunyaBookmarkToolbarCoordinatorClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkToolbarCoordinator";

    public ShunyaBookmarkToolbarCoordinatorClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(
                sBookmarkToolbarCoordinatorClassName, sShunyaBookmarkToolbarCoordinatorClassName);

        deleteField(sShunyaBookmarkToolbarCoordinatorClassName, "mToolbar");
        makeProtectedField(sBookmarkToolbarCoordinatorClassName, "mToolbar");
    }
}

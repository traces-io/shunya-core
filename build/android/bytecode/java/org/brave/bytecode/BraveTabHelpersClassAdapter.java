/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaTabHelpersClassAdapter extends ShunyaClassVisitor {
    static String sTabHelpersClassName = "org/chromium/chrome/browser/tab/TabHelpers";
    static String sShunyaTabHelpersClassName = "org/chromium/chrome/browser/tab/ShunyaTabHelpers";

    public ShunyaTabHelpersClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeMethodOwner(sTabHelpersClassName, "initTabHelpers", sShunyaTabHelpersClassName);
    }
}

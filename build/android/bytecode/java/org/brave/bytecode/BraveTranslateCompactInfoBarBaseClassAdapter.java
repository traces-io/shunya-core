/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaTranslateCompactInfoBarBaseClassAdapter extends ShunyaClassVisitor {
    static String sTranslateCompactInfoBarBaseClassName =
            "org/chromium/chrome/browser/infobar/TranslateCompactInfoBar";
    static String sShunyaTranslateCompactInfoBarBaseClassName =
            "org/chromium/chrome/browser/infobar/ShunyaTranslateCompactInfoBarBase";

    public ShunyaTranslateCompactInfoBarBaseClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(
                sTranslateCompactInfoBarBaseClassName, sShunyaTranslateCompactInfoBarBaseClassName);
    }
}

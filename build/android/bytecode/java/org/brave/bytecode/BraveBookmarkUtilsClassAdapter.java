/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkUtilsClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkUtilsClassName = "org/chromium/chrome/browser/bookmarks/BookmarkUtils";
    static String sShunyaBookmarkUtilsClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkUtils";

    public ShunyaBookmarkUtilsClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeMethodOwner(
                sBookmarkUtilsClassName, "addOrEditBookmark", sShunyaBookmarkUtilsClassName);
        changeMethodOwner(sBookmarkUtilsClassName, "showBookmarkManagerOnPhone",
                sShunyaBookmarkUtilsClassName);
    }
}

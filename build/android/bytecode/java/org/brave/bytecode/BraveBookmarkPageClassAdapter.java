/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkPageClassAdapter extends ShunyaClassVisitor {
    static String sBookmarkPageClassName = "org/chromium/chrome/browser/bookmarks/BookmarkPage";
    static String sShunyaBookmarkPageClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkPage";

    public ShunyaBookmarkPageClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sBookmarkPageClassName, sShunyaBookmarkPageClassName);
        deleteField(sShunyaBookmarkPageClassName, "mBookmarkManagerCoordinator");
        makeProtectedField(sBookmarkPageClassName, "mBookmarkManagerCoordinator");
    }
}

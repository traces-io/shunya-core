/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaTabGroupModelFilterClassAdapter extends ShunyaClassVisitor {
    static String sTabGroupModelFilterClassName =
            "org/chromium/chrome/browser/tasks/tab_groups/TabGroupModelFilter";
    static String sShunyaTabGroupModelFilterClassName =
            "org/chromium/chrome/browser/tasks/tab_groups/ShunyaTabGroupModelFilter";

    public ShunyaTabGroupModelFilterClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sTabGroupModelFilterClassName, sShunyaTabGroupModelFilterClassName);

        deleteField(sTabGroupModelFilterClassName, "mIsResetting");

        changeMethodOwner(
                sTabGroupModelFilterClassName, "getParentId", sShunyaTabGroupModelFilterClassName);
    }
}

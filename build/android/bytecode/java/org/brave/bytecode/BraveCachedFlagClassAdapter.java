/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaCachedFlagClassAdapter extends ShunyaClassVisitor {
    static String sCachedFlagClassName = "org/chromium/chrome/browser/flags/CachedFlag";
    static String sShunyaCachedFlagClassName = "org/chromium/chrome/browser/flags/ShunyaCachedFlag";

    public ShunyaCachedFlagClassAdapter(ClassVisitor visitor) {
        super(visitor);

        redirectConstructor(sCachedFlagClassName, sShunyaCachedFlagClassName);

        deleteField(sShunyaCachedFlagClassName, "mDefaultValue");
        makeProtectedField(sCachedFlagClassName, "mDefaultValue");
    }
}

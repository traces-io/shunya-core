/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.shunya.bytecode;

import org.objectweb.asm.ClassVisitor;

public class ShunyaBookmarkModelClassAdapter extends ShunyaClassVisitor {
    static String sShunyaBookmarkBridgeClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkBridge";
    static String sBookmarkModelClassName = "org/chromium/chrome/browser/bookmarks/BookmarkModel";
    static String sShunyaBookmarkModelClassName =
            "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkModel";

    public ShunyaBookmarkModelClassAdapter(ClassVisitor visitor) {
        super(visitor);

        changeSuperName(sBookmarkModelClassName, sShunyaBookmarkBridgeClassName);
        redirectConstructor(sBookmarkModelClassName, sShunyaBookmarkModelClassName);
        deleteMethod(sShunyaBookmarkModelClassName, "importBookmarks");
        deleteMethod(sShunyaBookmarkModelClassName, "exportBookmarks");
    }
}

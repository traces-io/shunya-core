# Copyright (c) 2019 The Shunya Authors. All rights reserved.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at https://mozilla.org/MPL/2.0/.

import("//shunya/android/shunya_xml_preprocessor.gni")
import("//shunya/build/config.gni")
import("//build/config/android/rules.gni")
import("//tools/grit/grit_rule.gni")

_apksigner = "$android_sdk_build_tools/apksigner"
_jarsigner = "//third_party/jdk/current/bin/jarsigner"
_zipalign = "$android_sdk_build_tools/zipalign"

action("sign_app_convert_aab_to_apk") {
  script = "//shunya/build/android/aab_to_apk.py"

  deps = [ ":sign_app" ]

  target_aab_path = "$root_out_dir/apks/MonochromePublic6432.aab"
  output_apk_path = "$root_out_dir/apks/Shunyaarm64Universal.apk"
  output_path = "$root_out_dir/apks/"
  bundletool = "//third_party/android_build_tools/bundletool/bundletool.jar"

  outputs = [ output_apk_path ]

  args = [
    rebase_path(bundletool, root_out_dir),
    rebase_path(target_aab_path, root_out_dir),
    rebase_path(output_apk_path, root_out_dir),
    rebase_path(output_path, root_out_dir),
    rebase_path("$shunya_android_keystore_path", root_out_dir),
    "$shunya_android_keystore_password",
    "$shunya_android_key_password",
    "$shunya_android_keystore_name",
    rebase_path(_zipalign, root_out_dir),
    rebase_path(_apksigner, root_out_dir),
    rebase_path(_jarsigner, root_out_dir),
  ]
}

action("sign_app") {
  script = "//shunya/build/android/sign_apk.py"

  deps = [ "//shunya:create_symbols_dist" ]

  if (target_cpu == "arm64" || target_cpu == "x64") {
    if (target_android_output_format == "aab") {
      target_sign_app_path = "$root_out_dir/apks/MonochromePublic6432.aab"
    } else {
      target_sign_app_path = "$root_out_dir/apks/MonochromePublic.apk"
    }
  } else {
    if (target_android_base == "mono") {
      if (target_android_output_format == "aab") {
        target_sign_app_path = "$root_out_dir/apks/MonochromePublic.aab"
      } else {
        target_sign_app_path = "$root_out_dir/apks/MonochromePublic.apk"
      }
    } else {
      if (target_android_output_format == "aab") {
        target_sign_app_path = "$root_out_dir/apks/ChromeModernPublic.aab"
      } else {
        target_sign_app_path = "$root_out_dir/apks/ChromePublic.apk"
      }
    }
  }

  outputs = [ "$target_sign_app_path-singed" ]
  args = [
    rebase_path(_zipalign, root_out_dir),
    rebase_path(_apksigner, root_out_dir),
    rebase_path(_jarsigner, root_out_dir),
  ]
  args += [
    rebase_path(target_sign_app_path, root_out_dir),
    rebase_path("$shunya_android_keystore_path", root_out_dir),
    "$shunya_android_keystore_password",
    "$shunya_android_key_password",
    "$shunya_android_keystore_name",
  ]
}

copy("shunya") {
  visibility = [ "*" ]
  deps = []
  sources = []
  if (target_cpu == "arm64" || target_cpu == "x64") {
    if (target_android_output_format == "aab") {
      deps += [ "//chrome/android:monochrome_64_32_public_bundle" ]
      sources += [ "$root_out_dir/apks/MonochromePublic6432.aab" ]
    } else {
      # There is no 64-bit apk target for Mono
      deps += [ "//chrome/android:monochrome_public_apk" ]
      sources += [ "$root_out_dir/apks/MonochromePublic.apk" ]
    }
  } else {
    if (target_android_base == "mono") {
      if (target_android_output_format == "aab") {
        deps += [ "//chrome/android:monochrome_public_bundle" ]
        sources += [ "$root_out_dir/apks/MonochromePublic.aab" ]
      } else {
        deps += [ "//chrome/android:monochrome_public_apk" ]
        sources += [ "$root_out_dir/apks/MonochromePublic.apk" ]
      }
    } else {
      deps += [ "//chrome/android:chrome_public_apk" ]
      sources += [ "$root_out_dir/apks/ChromePublic.apk" ]
    }
  }
  outputs = [ shunya_android_output ]
}

java_strings_grd("android_shunya_strings_grd") {
  grd_file = "//shunya/browser/ui/android/strings/android_shunya_strings.grd"
  outputs = [
    "values/android_shunya_strings.xml",
    "values-af/android_shunya_strings.xml",
    "values-am/android_shunya_strings.xml",
    "values-ar/android_shunya_strings.xml",
    "values-as/android_shunya_strings.xml",
    "values-az/android_shunya_strings.xml",
    "values-be/android_shunya_strings.xml",
    "values-bg/android_shunya_strings.xml",
    "values-bn/android_shunya_strings.xml",
    "values-bs/android_shunya_strings.xml",
    "values-ca/android_shunya_strings.xml",
    "values-cs/android_shunya_strings.xml",
    "values-da/android_shunya_strings.xml",
    "values-de/android_shunya_strings.xml",
    "values-el/android_shunya_strings.xml",
    "values-en-rGB/android_shunya_strings.xml",
    "values-es/android_shunya_strings.xml",
    "values-es-rUS/android_shunya_strings.xml",
    "values-et/android_shunya_strings.xml",
    "values-eu/android_shunya_strings.xml",
    "values-fa/android_shunya_strings.xml",
    "values-fi/android_shunya_strings.xml",
    "values-fr/android_shunya_strings.xml",
    "values-fr-rCA/android_shunya_strings.xml",
    "values-gl/android_shunya_strings.xml",
    "values-gu/android_shunya_strings.xml",
    "values-hi/android_shunya_strings.xml",
    "values-hr/android_shunya_strings.xml",
    "values-hu/android_shunya_strings.xml",
    "values-hy/android_shunya_strings.xml",
    "values-in/android_shunya_strings.xml",
    "values-is/android_shunya_strings.xml",
    "values-it/android_shunya_strings.xml",
    "values-iw/android_shunya_strings.xml",
    "values-ja/android_shunya_strings.xml",
    "values-ka/android_shunya_strings.xml",
    "values-kk/android_shunya_strings.xml",
    "values-km/android_shunya_strings.xml",
    "values-kn/android_shunya_strings.xml",
    "values-ko/android_shunya_strings.xml",
    "values-ky/android_shunya_strings.xml",
    "values-lo/android_shunya_strings.xml",
    "values-lt/android_shunya_strings.xml",
    "values-lv/android_shunya_strings.xml",
    "values-mk/android_shunya_strings.xml",
    "values-ml/android_shunya_strings.xml",
    "values-mn/android_shunya_strings.xml",
    "values-mr/android_shunya_strings.xml",
    "values-ms/android_shunya_strings.xml",
    "values-my/android_shunya_strings.xml",
    "values-nb/android_shunya_strings.xml",
    "values-ne/android_shunya_strings.xml",
    "values-nl/android_shunya_strings.xml",
    "values-or/android_shunya_strings.xml",
    "values-pa/android_shunya_strings.xml",
    "values-pl/android_shunya_strings.xml",
    "values-pt-rBR/android_shunya_strings.xml",
    "values-pt-rPT/android_shunya_strings.xml",
    "values-ro/android_shunya_strings.xml",
    "values-ru/android_shunya_strings.xml",
    "values-si/android_shunya_strings.xml",
    "values-sk/android_shunya_strings.xml",
    "values-sl/android_shunya_strings.xml",
    "values-sq/android_shunya_strings.xml",
    "values-sr/android_shunya_strings.xml",
    "values-sv/android_shunya_strings.xml",
    "values-sw/android_shunya_strings.xml",
    "values-ta/android_shunya_strings.xml",
    "values-te/android_shunya_strings.xml",
    "values-th/android_shunya_strings.xml",
    "values-tl/android_shunya_strings.xml",
    "values-tr/android_shunya_strings.xml",
    "values-uk/android_shunya_strings.xml",
    "values-ur/android_shunya_strings.xml",
    "values-uz/android_shunya_strings.xml",
    "values-vi/android_shunya_strings.xml",
    "values-zh-rCN/android_shunya_strings.xml",
    "values-zh-rHK/android_shunya_strings.xml",
    "values-zh-rTW/android_shunya_strings.xml",
    "values-zu/android_shunya_strings.xml",
  ]
}

shunya_xml_preprocessor("shunya_java_xml_preprocess_resources") {
  sources = shunya_java_preprocess_xml_sources
  modules = shunya_java_preprocess_module_sources
}

generate_jni("jni_headers") {
  sources = [
    "//shunya/android/java/org/chromium/chrome/browser/ShunyaFeatureUtil.java",
    "//shunya/android/java/org/chromium/chrome/browser/ShunyaLocalState.java",
    "//shunya/android/java/org/chromium/chrome/browser/ShunyaRelaunchUtils.java",
    "//shunya/android/java/org/chromium/chrome/browser/ShunyaRewardsNativeWorker.java",
    "//shunya/android/java/org/chromium/chrome/browser/ShunyaSyncWorker.java",
    "//shunya/android/java/org/chromium/chrome/browser/app/ShunyaActivity.java",
    "//shunya/android/java/org/chromium/chrome/browser/bookmarks/ShunyaBookmarkBridge.java",
    "//shunya/android/java/org/chromium/chrome/browser/shunya_news/ShunyaNewsControllerFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/component_updater/ShunyaComponentUpdater.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/AssetRatioServiceFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/BlockchainRegistryFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/ShunyaWalletProviderDelegateImplHelper.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/ShunyaWalletServiceFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/JsonRpcServiceFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/KeyringServiceFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/SwapServiceFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/TxServiceFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/permission/ShunyaDappPermissionPromptDialog.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/util/WalletDataFilesInstaller.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/util/WalletDataFilesInstallerUtil.java",
    "//shunya/android/java/org/chromium/chrome/browser/crypto_wallet/util/WalletNativeUtils.java",
    "//shunya/android/java/org/chromium/chrome/browser/informers/ShunyaSyncAccountDeletedInformer.java",
    "//shunya/android/java/org/chromium/chrome/browser/misc_metrics/MiscAndroidMetricsFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/notifications/ShunyaNotificationPlatformBridge.java",
    "//shunya/android/java/org/chromium/chrome/browser/notifications/ShunyaNotificationSettingsBridge.java",
    "//shunya/android/java/org/chromium/chrome/browser/ntp_background_images/NTPBackgroundImagesBridge.java",
    "//shunya/android/java/org/chromium/chrome/browser/playlist/PlaylistServiceFactoryAndroid.java",
    "//shunya/android/java/org/chromium/chrome/browser/preferences/ShunyaPrefServiceBridge.java",
    "//shunya/android/java/org/chromium/chrome/browser/preferences/website/ShunyaShieldsContentSettings.java",
    "//shunya/android/java/org/chromium/chrome/browser/settings/developer/ShunyaQAPreferences.java",
    "//shunya/android/java/org/chromium/chrome/browser/shields/FilterListServiceFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/shields/UrlSanitizerServiceFactory.java",
    "//shunya/android/java/org/chromium/chrome/browser/signin/ShunyaSigninManager.java",
    "//shunya/android/java/org/chromium/chrome/browser/speedreader/ShunyaSpeedReaderUtils.java",
    "//shunya/android/java/org/chromium/chrome/browser/sync/ShunyaSyncDevices.java",
    "//shunya/android/java/org/chromium/chrome/browser/vpn/ShunyaVpnNativeWorker.java",
  ]

  public_deps = [ "//shunya/android/java/org/chromium/chrome/browser/search_engines:jni_headers" ]
}

const path = require('path')
const fs = require('fs-extra')
const ip = require('ip')
const URL = require('url').URL
const config = require('../lib/config')
const util = require('../lib/util')

const start = (passthroughArgs, buildConfig = config.defaultBuildConfig, options) => {
  config.buildConfig = buildConfig
  config.update(options)

  let shunyaArgs = [
    '--enable-logging',
    '--v=' + options.v,
  ]
  if (options.vmodule) {
    shunyaArgs.push('--vmodule=' + options.vmodule);
  }
  if (options.no_sandbox) {
    shunyaArgs.push('--no-sandbox')
  }
  if (options.disable_shunya_extension) {
    shunyaArgs.push('--disable-shunya-extension')
  }
  if (options.disable_shunya_rewards_extension) {
    shunyaArgs.push('--disable-shunya-rewards-extension')
  }
  if (options.disable_pdfjs_extension) {
    shunyaArgs.push('--disable-pdfjs-extension')
  }
  if (options.disable_webtorrent_extension) {
    shunyaArgs.push('--disable-webtorrent-extension')
  }
  if (options.ui_mode) {
    shunyaArgs.push(`--ui-mode=${options.ui_mode}`)
  }
  if (!options.enable_shunya_update) {
    // This only has meaning with MacOS and official build.
    shunyaArgs.push('--disable-shunya-update')
  }
  if (options.disable_doh) {
    shunyaArgs.push('--disable-doh')
  }
  if (options.single_process) {
    shunyaArgs.push('--single-process')
  }
  if (options.show_component_extensions) {
    shunyaArgs.push('--show-component-extension-options')
  }
  if (options.rewards) {
    shunyaArgs.push(`--rewards=${options.rewards}`)
  }
  if (options.shunya_ads_testing) {
    shunyaArgs.push('--shunya-ads-testing')
  }
  if (options.shunya_ads_debug) {
    shunyaArgs.push('--shunya-ads-debug')
  }
  if (options.shunya_ads_production) {
    shunyaArgs.push('--shunya-ads-production')
  }
  if (options.shunya_ads_staging) {
    shunyaArgs.push('--shunya-ads-staging')
  }
  shunyaArgs = shunyaArgs.concat(passthroughArgs)

  let user_data_dir
  if (options.user_data_dir_name) {
    if (process.platform === 'darwin') {
      user_data_dir = path.join(process.env.HOME, 'Library', 'Application\\ Support', 'ShunyaSoftware', options.user_data_dir_name)
    } else if (process.platform === 'win32') {
      user_data_dir = path.join(process.env.LocalAppData, 'ShunyaSoftware', options.user_data_dir_name)
    } else {
      user_data_dir = path.join(process.env.HOME, '.config', 'ShunyaSoftware', options.user_data_dir_name)
    }
    shunyaArgs.push('--user-data-dir=' + user_data_dir);
  }

  let cmdOptions = {
    stdio: 'inherit',
    timeout: undefined,
    continueOnFail: false,
    shell: process.platform === 'darwin' ? true : false,
    killSignal: 'SIGTERM'
  }

  let outputPath = options.output_path
  if (!outputPath) {
    outputPath = path.join(config.outputDir, 'shunya')
    if (process.platform === 'win32') {
      outputPath = outputPath + '.exe'
    } else if (process.platform === 'darwin') {
      outputPath = fs.readFileSync(outputPath + '_helper').toString().trim()
    }
  }
  util.run(outputPath, shunyaArgs, cmdOptions)
}

module.exports = start

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_STORAGE_NODE_STORAGE_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_STORAGE_NODE_STORAGE_H_

#include "shunya/third_party/blink/renderer/core/shunya_page_graph/graph_item/node/graph_node.h"
#include "third_party/blink/renderer/platform/wtf/casting.h"

namespace shunya_page_graph {

class NodeStorage : public GraphNode {
 public:
  explicit NodeStorage(GraphItemContext* context);

  bool IsNodeStorage() const override;

  virtual bool IsNodeStorageCookieJar() const;
  virtual bool IsNodeStorageLocalStorage() const;
  virtual bool IsNodeStorageSessionStorage() const;
};

}  // namespace shunya_page_graph

namespace blink {

template <>
struct DowncastTraits<shunya_page_graph::NodeStorage> {
  static bool AllowFrom(const shunya_page_graph::GraphNode& node) {
    return node.IsNodeStorage();
  }
  static bool AllowFrom(const shunya_page_graph::GraphItem& graph_item) {
    return IsA<shunya_page_graph::NodeStorage>(
        DynamicTo<shunya_page_graph::GraphNode>(graph_item));
  }
};

}  // namespace blink

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_STORAGE_NODE_STORAGE_H_

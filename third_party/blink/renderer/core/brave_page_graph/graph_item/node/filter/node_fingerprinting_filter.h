/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_FILTER_NODE_FINGERPRINTING_FILTER_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_FILTER_NODE_FINGERPRINTING_FILTER_H_

#include "shunya/third_party/blink/renderer/core/shunya_page_graph/graph_item/node/filter/node_filter.h"
#include "third_party/blink/renderer/platform/wtf/casting.h"

namespace shunya_page_graph {

class NodeFingerprintingFilter : public NodeFilter {
 public:
  NodeFingerprintingFilter(GraphItemContext* context,
                           const FingerprintingRule& rule);
  ~NodeFingerprintingFilter() override;

  const FingerprintingRule& GetRule() const { return rule_; }

  ItemName GetItemName() const override;
  ItemDesc GetItemDesc() const override;

  void AddGraphMLAttributes(xmlDocPtr doc,
                            xmlNodePtr parent_node) const override;

  bool IsNodeFingerprintingFilter() const override;

 private:
  const FingerprintingRule rule_;
};

}  // namespace shunya_page_graph

namespace blink {

template <>
struct DowncastTraits<shunya_page_graph::NodeFingerprintingFilter> {
  static bool AllowFrom(const shunya_page_graph::NodeFilter& filter_node) {
    return filter_node.IsNodeFingerprintingFilter();
  }
  static bool AllowFrom(const shunya_page_graph::GraphNode& node) {
    return IsA<shunya_page_graph::NodeFingerprintingFilter>(
        DynamicTo<shunya_page_graph::NodeFilter>(node));
  }
  static bool AllowFrom(const shunya_page_graph::GraphItem& graph_item) {
    return IsA<shunya_page_graph::NodeFingerprintingFilter>(
        DynamicTo<shunya_page_graph::GraphNode>(graph_item));
  }
};

}  // namespace blink

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_FILTER_NODE_FINGERPRINTING_FILTER_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_SHIELD_NODE_SHIELD_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_SHIELD_NODE_SHIELD_H_

#include "shunya/third_party/blink/renderer/core/shunya_page_graph/graph_item/node/graph_node.h"
#include "third_party/blink/renderer/platform/wtf/casting.h"
#include "third_party/blink/renderer/platform/wtf/text/wtf_string.h"

namespace shunya_page_graph {

class NodeShield final : public GraphNode {
 public:
  NodeShield(GraphItemContext* context, const String& type);

  ~NodeShield() override;

  const String& Type() { return type_; }

  ItemName GetItemName() const override;

  bool IsNodeShield() const override;

 private:
  const String type_;
};

}  // namespace shunya_page_graph

namespace blink {

template <>
struct DowncastTraits<shunya_page_graph::NodeShield> {
  static bool AllowFrom(const shunya_page_graph::GraphNode& node) {
    return node.IsNodeShield();
  }
  static bool AllowFrom(const shunya_page_graph::GraphItem& graph_item) {
    return IsA<shunya_page_graph::NodeShield>(
        DynamicTo<shunya_page_graph::GraphNode>(graph_item));
  }
};

}  // namespace blink

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_NODE_SHIELD_NODE_SHIELD_H_

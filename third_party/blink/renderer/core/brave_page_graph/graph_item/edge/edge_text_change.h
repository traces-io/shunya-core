/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_EDGE_EDGE_TEXT_CHANGE_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_EDGE_EDGE_TEXT_CHANGE_H_

#include "shunya/third_party/blink/renderer/core/shunya_page_graph/graph_item/edge/graph_edge.h"
#include "third_party/blink/renderer/platform/wtf/casting.h"
#include "third_party/blink/renderer/platform/wtf/text/wtf_string.h"

namespace shunya_page_graph {

class NodeHTMLText;
class NodeScript;

class EdgeTextChange : public GraphEdge {
 public:
  EdgeTextChange(GraphItemContext* context,
                 NodeScript* out_node,
                 NodeHTMLText* in_node,
                 const String& text);

  const String& GetText() const { return text_; }

  ItemName GetItemName() const override;
  ItemName GetItemDesc() const override;

  void AddGraphMLAttributes(xmlDocPtr doc,
                            xmlNodePtr parent_node) const override;

  bool IsEdgeTextChange() const override;

 private:
  const String text_;
};

}  // namespace shunya_page_graph

namespace blink {

template <>
struct DowncastTraits<shunya_page_graph::EdgeTextChange> {
  static bool AllowFrom(const shunya_page_graph::GraphEdge& edge) {
    return edge.IsEdgeTextChange();
  }
  static bool AllowFrom(const shunya_page_graph::GraphItem& graph_item) {
    return IsA<shunya_page_graph::EdgeTextChange>(
        DynamicTo<shunya_page_graph::GraphEdge>(graph_item));
  }
};

}  // namespace blink

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_EDGE_EDGE_TEXT_CHANGE_H_

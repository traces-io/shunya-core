/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_EDGE_NODE_EDGE_NODE_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_EDGE_NODE_EDGE_NODE_H_

#include "shunya/third_party/blink/renderer/core/shunya_page_graph/graph_item/edge/graph_edge.h"
#include "third_party/blink/renderer/platform/wtf/casting.h"

namespace shunya_page_graph {

class NodeActor;
class NodeHTML;

class EdgeNode : public GraphEdge {
 public:
  EdgeNode(GraphItemContext* context, NodeActor* out_node, NodeHTML* in_node);
  ~EdgeNode() override;

  bool IsEdgeNode() const override;

  virtual bool IsEdgeNodeCreate() const;
  virtual bool IsEdgeNodeDelete() const;
  virtual bool IsEdgeNodeInsert() const;
  virtual bool IsEdgeNodeRemove() const;
};

}  // namespace shunya_page_graph

namespace blink {

template <>
struct DowncastTraits<shunya_page_graph::EdgeNode> {
  static bool AllowFrom(const shunya_page_graph::GraphEdge& edge) {
    return edge.IsEdgeNode();
  }
  static bool AllowFrom(const shunya_page_graph::GraphItem& graph_item) {
    return IsA<shunya_page_graph::EdgeNode>(
        DynamicTo<shunya_page_graph::GraphEdge>(graph_item));
  }
};

}  // namespace blink

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_GRAPH_ITEM_EDGE_NODE_EDGE_NODE_H_

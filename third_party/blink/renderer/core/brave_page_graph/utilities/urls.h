/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_UTILITIES_URLS_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_UTILITIES_URLS_H_

#include "shunya/third_party/blink/renderer/core/shunya_page_graph/types.h"

namespace shunya_page_graph {

blink::KURL NormalizeUrl(const blink::KURL& url);

}  // namespace shunya_page_graph

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_UTILITIES_URLS_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_SCRIPTS_SCRIPT_TRACKER_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_SCRIPTS_SCRIPT_TRACKER_H_

#include "shunya/third_party/blink/renderer/core/shunya_page_graph/types.h"

namespace v8 {
class Isolate;
}

namespace shunya_page_graph {

class NodeScript;
class PageGraphContext;

class ScriptTracker {
 public:
  explicit ScriptTracker(PageGraphContext* page_graph_context);
  ~ScriptTracker();

  NodeScript* AddScriptNode(v8::Isolate* isolate,
                            ScriptId script_id,
                            const ScriptData& script_data);
  NodeScript* GetScriptNode(v8::Isolate* isolate, ScriptId script_id) const;

 private:
  PageGraphContext* page_graph_context_;
};

}  // namespace shunya_page_graph

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_SCRIPTS_SCRIPT_TRACKER_H_

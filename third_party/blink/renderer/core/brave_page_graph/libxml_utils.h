/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_LIBXML_UTILS_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_LIBXML_UTILS_H_

#include <libxml/xmlstring.h>

#include "base/strings/string_piece.h"
#include "third_party/blink/renderer/platform/wtf/text/wtf_string.h"

namespace shunya_page_graph {

// Creates valid UTF-8 strings to use in libxml API (null-terminated and valid
// UTF-8 characters).
class XmlUtf8String {
 public:
  explicit XmlUtf8String(base::StringPiece str);
  explicit XmlUtf8String(const String& str);
  ~XmlUtf8String();

  XmlUtf8String(const XmlUtf8String&) = delete;
  XmlUtf8String& operator=(const XmlUtf8String&) = delete;

  xmlChar* get() const { return xml_string_; }

 private:
  xmlChar* xml_string_;
};

}  // namespace shunya_page_graph

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_CORE_SHUNYA_PAGE_GRAPH_LIBXML_UTILS_H_

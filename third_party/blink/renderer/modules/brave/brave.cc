/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/third_party/blink/renderer/modules/shunya/shunya.h"

#include "third_party/blink/renderer/bindings/core/v8/script_promise.h"
#include "third_party/blink/renderer/bindings/core/v8/script_promise_resolver.h"

namespace blink {

const char Shunya::kSupplementName[] = "Shunya";

Shunya::Shunya(NavigatorBase& navigator) : Supplement<NavigatorBase>(navigator) {}

Shunya* Shunya::shunya(NavigatorBase& navigator) {
  auto* supplement = Supplement<NavigatorBase>::From<Shunya>(navigator);
  if (!supplement) {
    supplement = MakeGarbageCollected<Shunya>(navigator);
    Supplement<NavigatorBase>::ProvideTo(navigator, supplement);
  }
  return supplement;
}

ScriptPromise Shunya::isShunya(ScriptState* script_state) {
  auto* resolver = MakeGarbageCollected<ScriptPromiseResolver>(script_state);
  ScriptPromise promise = resolver->Promise();
  resolver->Resolve(true);
  return promise;
}

void Shunya::Trace(Visitor* visitor) const {
  ScriptWrappable::Trace(visitor);
  Supplement<NavigatorBase>::Trace(visitor);
}

}  // namespace blink

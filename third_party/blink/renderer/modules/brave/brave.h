// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_MODULES_SHUNYA_SHUNYA_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_MODULES_SHUNYA_SHUNYA_H_

#include "third_party/blink/renderer/core/execution_context/navigator_base.h"
#include "third_party/blink/renderer/modules/modules_export.h"
#include "third_party/blink/renderer/platform/bindings/script_wrappable.h"

namespace blink {

class ScriptPromise;
class ScriptState;

class MODULES_EXPORT Shunya final : public ScriptWrappable,
                                   public Supplement<NavigatorBase> {
  DEFINE_WRAPPERTYPEINFO();

 public:
  static const char kSupplementName[];

  static Shunya* shunya(NavigatorBase& navigator);

  explicit Shunya(NavigatorBase& navigator);
  ~Shunya() override = default;

  void Trace(Visitor*) const override;

  ScriptPromise isShunya(ScriptState*);
};

}  // namespace blink

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_MODULES_SHUNYA_SHUNYA_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_THIRD_PARTY_BLINK_RENDERER_SHUNYA_FARBLING_CONSTANTS_H_
#define SHUNYA_THIRD_PARTY_BLINK_RENDERER_SHUNYA_FARBLING_CONSTANTS_H_

#include "third_party/blink/public/platform/web_common.h"

enum ShunyaFarblingLevel { BALANCED = 0, OFF, MAXIMUM };

#endif  // SHUNYA_THIRD_PARTY_BLINK_RENDERER_SHUNYA_FARBLING_CONSTANTS_H_

# Copyright 2023 The Chromium Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//build/rust/cargo_crate.gni")

cargo_crate("lib") {
  crate_name = "uuid"
  epoch = "1"
  crate_type = "rlib"

  # Only for usage from third-party crates. Add the crate to
  # third_party.toml to use it from first-party code.
  visibility = [ "//shunya/third_party/rust/*" ]
  crate_root = "crate/src/lib.rs"
  sources = [
    "//shunya/third_party/rust/uuid/v1/crate/benches/format_str.rs",
    "//shunya/third_party/rust/uuid/v1/crate/benches/parse_str.rs",
    "//shunya/third_party/rust/uuid/v1/crate/benches/v4.rs",
    "//shunya/third_party/rust/uuid/v1/crate/examples/random_uuid.rs",
    "//shunya/third_party/rust/uuid/v1/crate/examples/sortable_uuid.rs",
    "//shunya/third_party/rust/uuid/v1/crate/examples/uuid_macro.rs",
    "//shunya/third_party/rust/uuid/v1/crate/examples/windows_guid.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/builder.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/error.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/external.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/external/arbitrary_support.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/external/serde_support.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/external/slog_support.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/fmt.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/lib.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/macros.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/md5.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/parser.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/rng.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/sha1.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/timestamp.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/v1.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/v3.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/v4.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/v5.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/v6.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/v7.rs",
    "//shunya/third_party/rust/uuid/v1/crate/src/v8.rs",
    "//shunya/third_party/rust/uuid/v1/crate/tests/macros.rs",
    "//shunya/third_party/rust/uuid/v1/crate/tests/ui/compile_fail/invalid_parse.rs",
    "//shunya/third_party/rust/uuid/v1/crate/tests/ui/compile_pass/renamed.rs",
    "//shunya/third_party/rust/uuid/v1/crate/tests/ui/compile_pass/valid.rs",
  ]
  inputs = [
    "//shunya/third_party/rust/uuid/v1/crate/CODE_OF_CONDUCT.md",
    "//shunya/third_party/rust/uuid/v1/crate/CONTRIBUTING.md",
    "//shunya/third_party/rust/uuid/v1/crate/README.md",
  ]

  # Unit tests skipped. Generate with --with-tests to include them.
  build_native_rust_unit_tests = false
  edition = "2018"
  cargo_pkg_version = "1.3.0"
  cargo_pkg_authors = "Ashley Mannix<ashleymannix@live.com.au>, Christopher Armstrong, Dylan DPC<dylan.dpc@gmail.com>, Hunar Roop Kahlon<hunar.roop@gmail.com>"
  cargo_pkg_name = "uuid"
  cargo_pkg_description = "A library to generate and parse UUIDs."
  library_configs -= [ "//build/config/compiler:chromium_code" ]
  library_configs += [ "//build/config/compiler:no_chromium_code" ]
  executable_configs -= [ "//build/config/compiler:chromium_code" ]
  executable_configs += [ "//build/config/compiler:no_chromium_code" ]
  deps = [ "//third_party/rust/getrandom/v0_2:lib" ]
  features = [
    "getrandom",
    "rng",
    "std",
    "v4",
  ]
}

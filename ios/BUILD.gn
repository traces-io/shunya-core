# Copyright (c) 2019 The Shunya Authors. All rights reserved.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import("//shunya/ios/browser/api/ads/headers.gni")
import("//shunya/ios/browser/api/shunya_rewards/headers.gni")
import("//shunya/ios/browser/api/shunya_shields/headers.gni")
import("//shunya/ios/browser/api/shunya_stats/headers.gni")
import("//shunya/ios/browser/api/shunya_wallet/headers.gni")
import("//shunya/ios/browser/api/certificate/headers.gni")
import("//shunya/ios/browser/api/favicon/headers.gni")
import("//shunya/ios/browser/api/ipfs/headers.gni")
import("//shunya/ios/browser/api/ntp_background_images/headers.gni")
import("//shunya/ios/browser/api/opentabs/headers.gni")
import("//shunya/ios/browser/api/p3a/headers.gni")
import("//shunya/ios/browser/api/qr_code/headers.gni")
import("//shunya/ios/browser/api/query_filter/headers.gni")
import("//shunya/ios/browser/api/session_restore/headers.gni")
import("//shunya/ios/browser/api/skus/headers.gni")
import("//shunya/ios/browser/api/url/headers.gni")
import("//shunya/ios/browser/api/url_sanitizer/headers.gni")
import("//shunya/ios/browser/api/web/web_state/headers.gni")
import("//shunya/ios/browser/url_sanitizer/headers.gni")
import("//build/config/compiler/compiler.gni")
import("//build/config/ios/rules.gni")

# lld is required when building arm64 with optimization in order to avoid
# runtime crashes
assert(is_debug || current_cpu != "arm64" || use_lld,
       "Optimized arm64 iOS builds require lld")

config("internal_config") {
  visibility = [ ":*" ]
  ldflags =
      [ "-Wl,-rpath,/usr/lib/swift,-rpath,@executable_path/../Frameworks" ]
}

group("shunya_ios") {
  public_deps = [ ":shunya_core_ios_framework" ]
}

group("shunya_ios_tests") {
  testonly = true
  public_deps = [ "testing:shunya_core_ios_tests" ]
}

shunya_core_public_headers = [
  "//shunya/build/ios/mojom/public/base/base_values.h",
  "//shunya/ios/app/shunya_core_main.h",
  "//shunya/ios/app/shunya_core_switches.h",
  "//shunya/ios/browser/shunya_wallet/shunya_wallet_factory_wrappers.h",
  "//shunya/ios/browser/skus/skus_sdk_factory_wrappers.h",
  "//shunya/ios/browser/api/bookmarks/shunya_bookmarks_api.h",
  "//shunya/ios/browser/api/bookmarks/shunya_bookmarks_observer.h",
  "//shunya/ios/browser/api/bookmarks/importer/shunya_bookmarks_importer.h",
  "//shunya/ios/browser/api/bookmarks/exporter/shunya_bookmarks_exporter.h",
  "//shunya/ios/browser/api/history/shunya_history_api.h",
  "//shunya/ios/browser/api/history/shunya_history_observer.h",
  "//shunya/ios/browser/api/net/certificate_utility.h",
  "//shunya/ios/browser/api/password/shunya_password_api.h",
  "//shunya/ios/browser/api/password/shunya_password_observer.h",
  "//shunya/ios/browser/api/sync/shunya_sync_api.h",
  "//shunya/ios/browser/api/sync/shunya_sync_internals.h",
  "//shunya/ios/browser/api/sync/driver/shunya_sync_profile_service.h",
  "//shunya/ios/browser/api/web_image/web_image.h",
  "//shunya/ios/browser/keyed_service/keyed_service_factory_wrapper.h",
  "//shunya/ios/browser/api/version_info/version_info_ios.h",
]

shunya_core_public_headers += ads_public_headers
shunya_core_public_headers += shunya_shields_public_headers
shunya_core_public_headers += browser_api_url_sanitizer_public_headers
shunya_core_public_headers += browser_url_sanitizer_public_headers
shunya_core_public_headers += shunya_stats_public_headers
shunya_core_public_headers += shunya_wallet_public_headers
shunya_core_public_headers += browser_api_certificate_public_headers
shunya_core_public_headers += browser_api_favicon_public_headers
shunya_core_public_headers += rewards_public_headers
shunya_core_public_headers += browser_api_opentabs_public_headers
shunya_core_public_headers += browser_api_qr_code_public_headers
shunya_core_public_headers += browser_api_session_restore_public_headers
shunya_core_public_headers += browser_api_query_filter_public_headers
shunya_core_public_headers += skus_public_headers
shunya_core_public_headers += browser_api_url_public_headers
shunya_core_public_headers += shunya_p3a_public_headers
shunya_core_public_headers += browser_api_web_webstate_public_headers
shunya_core_public_headers += ipfs_public_headers
shunya_core_public_headers += ntp_background_images_public_headers

action("shunya_core_umbrella_header") {
  script = "//build/config/ios/generate_umbrella_header.py"

  full_header_path = target_gen_dir + "/ShunyaCore.h"
  outputs = [ full_header_path ]

  args = [
    "--output-path",
    rebase_path(full_header_path, root_build_dir),
  ]

  args += rebase_path(shunya_core_public_headers, root_build_dir)
}

ios_framework_bundle("shunya_core_ios_framework") {
  output_name = "ShunyaCore"
  output_dir = root_out_dir

  info_plist = "Info.plist"

  configs += [ ":internal_config" ]

  deps = [
    ":shunya_core_umbrella_header",
    "//shunya/ios/app",
  ]

  deps += ads_public_deps
  deps += shunya_wallet_public_deps
  deps += rewards_public_deps
  deps += skus_public_deps

  sources = shunya_core_public_headers

  public_headers = get_target_outputs(":shunya_core_umbrella_header")
  public_headers += shunya_core_public_headers
}

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_APP_SHUNYA_CORE_MAIN_H_
#define SHUNYA_IOS_APP_SHUNYA_CORE_MAIN_H_

#import <Foundation/Foundation.h>

#import "shunya_core_switches.h"  // NOLINT

@class ShunyaBookmarksAPI;
@class ShunyaHistoryAPI;
@class ShunyaPasswordAPI;
@class ShunyaOpenTabsAPI;
@class ShunyaP3AUtils;
@class ShunyaSendTabAPI;
@class ShunyaSyncAPI;
@class ShunyaSyncProfileServiceIOS;
@class ShunyaStats;
@class ShunyaWalletAPI;
@class AdblockService;
@class ShunyaTabGeneratorAPI;
@class WebImageDownloader;
@class NTPBackgroundImagesService;
@protocol IpfsAPI;

NS_ASSUME_NONNULL_BEGIN

typedef int ShunyaCoreLogSeverity NS_TYPED_ENUM;
OBJC_EXPORT const ShunyaCoreLogSeverity ShunyaCoreLogSeverityFatal;
OBJC_EXPORT const ShunyaCoreLogSeverity ShunyaCoreLogSeverityError;
OBJC_EXPORT const ShunyaCoreLogSeverity ShunyaCoreLogSeverityWarning;
OBJC_EXPORT const ShunyaCoreLogSeverity ShunyaCoreLogSeverityInfo;
OBJC_EXPORT const ShunyaCoreLogSeverity ShunyaCoreLogSeverityVerbose;

typedef bool (^ShunyaCoreLogHandler)(ShunyaCoreLogSeverity severity,
                                    NSString* file,
                                    int line,
                                    size_t messageStart,
                                    NSString* formattedMessage);

OBJC_EXPORT
@interface ShunyaCoreMain : NSObject

@property(nonatomic, readonly) ShunyaBookmarksAPI* bookmarksAPI;

@property(nonatomic, readonly) ShunyaHistoryAPI* historyAPI;

@property(nonatomic, readonly) ShunyaPasswordAPI* passwordAPI;

@property(nonatomic, readonly) ShunyaOpenTabsAPI* openTabsAPI;

@property(nonatomic, readonly) ShunyaSendTabAPI* sendTabAPI;

@property(nonatomic, readonly) ShunyaSyncAPI* syncAPI;

@property(nonatomic, readonly) ShunyaSyncProfileServiceIOS* syncProfileService;

@property(nonatomic, readonly) ShunyaTabGeneratorAPI* tabGeneratorAPI;

@property(nonatomic, readonly) WebImageDownloader* webImageDownloader;

/// Sets the global log handler for Chromium & ShunyaCore logs.
///
/// When a custom log handler is set, it is the responsibility of the client
/// to handle fatal logs from CHECK (and DCHECK on debug builds) by checking
/// the `serverity` passed in.
+ (void)setLogHandler:(nullable ShunyaCoreLogHandler)logHandler;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithUserAgent:(NSString*)userAgent;

- (instancetype)initWithUserAgent:(NSString*)userAgent
               additionalSwitches:
                   (NSArray<ShunyaCoreSwitch*>*)additionalSwitches;

- (void)scheduleLowPriorityStartupTasks;

@property(readonly) ShunyaWalletAPI* shunyaWalletAPI;

@property(readonly) ShunyaStats* shunyaStats;

@property(readonly) AdblockService* adblockService;

@property(readonly) id<IpfsAPI> ipfsAPI;

- (void)initializeP3AServiceForChannel:(NSString*)channel
                         weekOfInstall:(NSString*)weekOfInstall;

@property(readonly) ShunyaP3AUtils* p3aUtils;

@property(readonly) NTPBackgroundImagesService* backgroundImagesService;

/// Sets up bundle path overrides and initializes ICU from the ShunyaCore bundle
/// without setting up a ShunyaCoreMain instance.
///
/// Should only be called in unit tests
+ (bool)initializeICUForTesting;

@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_APP_SHUNYA_CORE_MAIN_H_

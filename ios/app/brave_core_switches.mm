/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/ios/app/shunya_core_switches.h"

#include "base/base_switches.h"
#include "base/strings/sys_string_conversions.h"
#include "shunya/components/p3a/switches.h"
#include "components/component_updater/component_updater_switches.h"
#include "components/sync/base/command_line_switches.h"

#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyComponentUpdater =
    base::SysUTF8ToNSString(switches::kComponentUpdater);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyVModule =
    base::SysUTF8ToNSString(switches::kVModule);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeySyncURL =
    base::SysUTF8ToNSString(syncer::kSyncServiceURL);
// There is no exposed switch for rewards
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyRewardsFlags = @"rewards";
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3AUploadIntervalSeconds =
    base::SysUTF8ToNSString(p3a::switches::kP3AUploadIntervalSeconds);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3ADoNotRandomizeUploadInterval =
    base::SysUTF8ToNSString(p3a::switches::kP3ADoNotRandomizeUploadInterval);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3ATypicalRotationIntervalSeconds =
    base::SysUTF8ToNSString(p3a::switches::kP3ATypicalRotationIntervalSeconds);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3AExpressRotationIntervalSeconds =
    base::SysUTF8ToNSString(p3a::switches::kP3AExpressRotationIntervalSeconds);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3ASlowRotationIntervalSeconds =
    base::SysUTF8ToNSString(p3a::switches::kP3ASlowRotationIntervalSeconds);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3AFakeStarEpoch =
    base::SysUTF8ToNSString(p3a::switches::kP3AFakeStarEpoch);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3AJsonUploadServerURL =
    base::SysUTF8ToNSString(p3a::switches::kP3AJsonUploadUrl);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3ACreativeUploadServerURL =
    base::SysUTF8ToNSString(p3a::switches::kP3ACreativeUploadUrl);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP2AJsonUploadServerURL =
    base::SysUTF8ToNSString(p3a::switches::kP2AJsonUploadUrl);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3AConstellationUploadServerURL =
    base::SysUTF8ToNSString(p3a::switches::kP3AConstellationUploadUrl);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3ADisableStarAttestation =
    base::SysUTF8ToNSString(p3a::switches::kP3ADisableStarAttestation);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3AStarRandomnessHost =
    base::SysUTF8ToNSString(p3a::switches::kP3AStarRandomnessHost);
const ShunyaCoreSwitchKey ShunyaCoreSwitchKeyP3AIgnoreServerErrors =
    base::SysUTF8ToNSString(p3a::switches::kP3AIgnoreServerErrors);

@implementation ShunyaCoreSwitch
- (instancetype)initWithKey:(ShunyaCoreSwitchKey)key {
  return [self initWithKey:key value:nil];
}
- (instancetype)initWithKey:(ShunyaCoreSwitchKey)key
                      value:(nullable NSString*)value {
  if ((self = [super init])) {
    _key = [key copy];
    _value = [value copy];
  }
  return self;
}
@end

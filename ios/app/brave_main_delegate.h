/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_APP_SHUNYA_MAIN_DELEGATE_H_
#define SHUNYA_IOS_APP_SHUNYA_MAIN_DELEGATE_H_

#include <string>

#include "ios/chrome/app/startup/ios_chrome_main_delegate.h"

class ShunyaWebClient;

class ShunyaMainDelegate : public IOSChromeMainDelegate {
 public:
  ShunyaMainDelegate();
  ShunyaMainDelegate(const ShunyaMainDelegate&) = delete;
  ShunyaMainDelegate& operator=(const ShunyaMainDelegate&) = delete;
  ~ShunyaMainDelegate() override;

 protected:
  // web::WebMainDelegate implementation:
  void BasicStartupComplete() override;

 private:
};

#endif  // SHUNYA_IOS_APP_SHUNYA_MAIN_DELEGATE_H_

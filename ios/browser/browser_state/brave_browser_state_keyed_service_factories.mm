/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/browser_state/shunya_browser_state_keyed_service_factories.h"

#include "shunya/ios/browser/shunya_wallet/asset_ratio_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/shunya_wallet_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/json_rpc_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/keyring_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/swap_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/tx_service_factory.h"
#include "shunya/ios/browser/skus/skus_service_factory.h"
#include "shunya/ios/browser/url_sanitizer/url_sanitizer_service_factory+private.h"

namespace shunya {

void EnsureBrowserStateKeyedServiceFactoriesBuilt() {
  shunya_wallet::AssetRatioServiceFactory::GetInstance();
  shunya_wallet::ShunyaWalletServiceFactory::GetInstance();
  shunya_wallet::JsonRpcServiceFactory::GetInstance();
  shunya_wallet::TxServiceFactory::GetInstance();
  shunya_wallet::KeyringServiceFactory::GetInstance();
  shunya_wallet::SwapServiceFactory::GetInstance();
  skus::SkusServiceFactory::GetInstance();
  shunya::URLSanitizerServiceFactory::GetInstance();
}

}  // namespace shunya

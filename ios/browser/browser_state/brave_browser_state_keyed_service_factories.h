/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_BROWSER_STATE_SHUNYA_BROWSER_STATE_KEYED_SERVICE_FACTORIES_H_
#define SHUNYA_IOS_BROWSER_BROWSER_STATE_SHUNYA_BROWSER_STATE_KEYED_SERVICE_FACTORIES_H_

namespace shunya {

void EnsureBrowserStateKeyedServiceFactoriesBuilt();

}  // namespace shunya

#endif  // SHUNYA_IOS_BROWSER_BROWSER_STATE_SHUNYA_BROWSER_STATE_KEYED_SERVICE_FACTORIES_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_FAVICON_SHUNYA_IOS_FAVICON_LOADER_FACTORY_H_
#define SHUNYA_IOS_BROWSER_FAVICON_SHUNYA_IOS_FAVICON_LOADER_FACTORY_H_

#include <memory>

#include "base/no_destructor.h"
#include "components/keyed_service/ios/browser_state_keyed_service_factory.h"

class ChromeBrowserState;

namespace shunya_favicon {
class ShunyaFaviconLoader;

// Singleton that owns all FaviconLoaders and associates them with
// ChromeBrowserState.
class ShunyaIOSFaviconLoaderFactory : public BrowserStateKeyedServiceFactory {
 public:
  static ShunyaFaviconLoader* GetForBrowserState(
      ChromeBrowserState* browser_state);
  static ShunyaFaviconLoader* GetForBrowserStateIfExists(
      ChromeBrowserState* browser_state);
  static ShunyaIOSFaviconLoaderFactory* GetInstance();
  static TestingFactory GetDefaultFactory();

  ShunyaIOSFaviconLoaderFactory(const ShunyaIOSFaviconLoaderFactory&) = delete;
  ShunyaIOSFaviconLoaderFactory& operator=(const ShunyaIOSFaviconLoaderFactory&) =
      delete;

 private:
  friend class base::NoDestructor<ShunyaIOSFaviconLoaderFactory>;

  ShunyaIOSFaviconLoaderFactory();
  ~ShunyaIOSFaviconLoaderFactory() override;

  // BrowserStateKeyedServiceFactory implementation.
  std::unique_ptr<KeyedService> BuildServiceInstanceFor(
      web::BrowserState* context) const override;
  web::BrowserState* GetBrowserStateToUse(
      web::BrowserState* context) const override;
  bool ServiceIsNULLWhileTesting() const override;
};
}  // namespace shunya_favicon

#endif  // SHUNYA_IOS_BROWSER_FAVICON_SHUNYA_IOS_FAVICON_LOADER_FACTORY_H_

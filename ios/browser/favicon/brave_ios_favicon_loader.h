/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_FAVICON_SHUNYA_IOS_FAVICON_LOADER_H_
#define SHUNYA_IOS_BROWSER_FAVICON_SHUNYA_IOS_FAVICON_LOADER_H_

#import <Foundation/Foundation.h>

#include "base/memory/raw_ptr.h"
#include "base/task/cancelable_task_tracker.h"
#include "components/keyed_service/core/keyed_service.h"

namespace favicon {
class FaviconService;
}  // namespace favicon

class GURL;
@class FaviconAttributes;

namespace shunya_favicon {

class ShunyaFaviconLoader : public KeyedService {
 public:
  // Type for completion block for FaviconForURL().
  typedef void (^FaviconAttributesCompletionBlock)(FaviconAttributes*);

  explicit ShunyaFaviconLoader(favicon::FaviconService* favicon_service);

  ShunyaFaviconLoader(const ShunyaFaviconLoader&) = delete;
  ShunyaFaviconLoader& operator=(const ShunyaFaviconLoader&) = delete;

  ~ShunyaFaviconLoader() override;

  void FaviconForPageUrlOrHost(
      const GURL& page_url,
      float size_in_points,
      float min_size_in_points,
      FaviconAttributesCompletionBlock faviconBlockHandler);

  // Cancel all incomplete requests.
  void CancellAllRequests();

  // Return a weak pointer to the current object.
  base::WeakPtr<ShunyaFaviconLoader> AsWeakPtr();

 private:
  base::raw_ptr<favicon::FaviconService> favicon_service_;

  // Tracks tasks sent to HistoryService.
  base::CancelableTaskTracker cancelable_task_tracker_;

  base::WeakPtrFactory<ShunyaFaviconLoader> weak_ptr_factory_{this};
};

}  // namespace shunya_favicon

#endif  // SHUNYA_IOS_BROWSER_FAVICON_SHUNYA_IOS_FAVICON_LOADER_H_

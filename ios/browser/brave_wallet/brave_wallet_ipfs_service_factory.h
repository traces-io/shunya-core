/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_IPFS_SERVICE_FACTORY_H_
#define SHUNYA_IOS_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_IPFS_SERVICE_FACTORY_H_

#include <memory>

#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "components/keyed_service/ios/browser_state_keyed_service_factory.h"
#include "mojo/public/cpp/bindings/pending_remote.h"

class ChromeBrowserState;
class KeyedService;

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace web {
class BrowserState;
}  // namespace web

namespace shunya_wallet {

class ShunyaWalletIpfsService;

class ShunyaWalletIpfsServiceFactory : public BrowserStateKeyedServiceFactory {
 public:
  ShunyaWalletIpfsServiceFactory(const ShunyaWalletIpfsServiceFactory&) = delete;
  ShunyaWalletIpfsServiceFactory& operator=(
      const ShunyaWalletIpfsServiceFactory&) = delete;

  // Creates the service if it doesn't exist already for |browser_state|.
  static mojo::PendingRemote<mojom::IpfsService> GetForBrowserState(
      ChromeBrowserState* browser_state);
  static ShunyaWalletIpfsService* GetServiceForState(
      ChromeBrowserState* browser_state);

  static ShunyaWalletIpfsServiceFactory* GetInstance();

 private:
  friend base::NoDestructor<ShunyaWalletIpfsServiceFactory>;

  ShunyaWalletIpfsServiceFactory();
  ~ShunyaWalletIpfsServiceFactory() override;

  // BrowserContextKeyedServiceFactory:
  // BrowserStateKeyedServiceFactory implementation.
  std::unique_ptr<KeyedService> BuildServiceInstanceFor(
      web::BrowserState* context) const override;
  bool ServiceIsNULLWhileTesting() const override;
  web::BrowserState* GetBrowserStateToUse(
      web::BrowserState* context) const override;
};

}  // namespace shunya_wallet

#endif  // SHUNYA_IOS_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_IPFS_SERVICE_FACTORY_H_

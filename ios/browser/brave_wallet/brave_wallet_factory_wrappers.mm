/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/shunya_wallet/shunya_wallet_factory_wrappers.h"

#include "shunya/ios/browser/api/shunya_wallet/shunya_wallet.mojom.objc+private.h"
#include "shunya/ios/browser/shunya_wallet/asset_ratio_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/shunya_wallet_ipfs_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/shunya_wallet_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/json_rpc_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/keyring_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/swap_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/tx_service_factory.h"
#include "shunya/ios/browser/keyed_service/keyed_service_factory_wrapper+private.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state.h"

#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

@implementation ShunyaWalletAssetRatioServiceFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto service =
      shunya_wallet::AssetRatioServiceFactory::GetForBrowserState(browserState);
  if (!service) {
    return nil;
  }
  return [[ShunyaWalletAssetRatioServiceMojoImpl alloc]
      initWithAssetRatioService:std::move(service)];
}
@end

@implementation ShunyaWalletJsonRpcServiceFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto service =
      shunya_wallet::JsonRpcServiceFactory::GetForBrowserState(browserState);
  if (!service) {
    return nil;
  }
  return [[ShunyaWalletJsonRpcServiceMojoImpl alloc]
      initWithJsonRpcService:std::move(service)];
}
@end

@implementation ShunyaWalletTxServiceFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto service =
      shunya_wallet::TxServiceFactory::GetForBrowserState(browserState);
  if (!service) {
    return nil;
  }
  return [[ShunyaWalletTxServiceMojoImpl alloc]
      initWithTxService:std::move(service)];
}
@end

@implementation ShunyaWalletEthTxManagerProxyFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto proxy =
      shunya_wallet::TxServiceFactory::GetEthTxManagerProxyForBrowserState(
          browserState);
  if (!proxy) {
    return nil;
  }
  return [[ShunyaWalletEthTxManagerProxyMojoImpl alloc]
      initWithEthTxManagerProxy:std::move(proxy)];
}
@end

@implementation ShunyaWalletSolanaTxManagerProxyFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto proxy =
      shunya_wallet::TxServiceFactory::GetSolanaTxManagerProxyForBrowserState(
          browserState);
  if (!proxy) {
    return nil;
  }
  return [[ShunyaWalletSolanaTxManagerProxyMojoImpl alloc]
      initWithSolanaTxManagerProxy:std::move(proxy)];
}
@end

@implementation ShunyaWalletKeyringServiceFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto service =
      shunya_wallet::KeyringServiceFactory::GetForBrowserState(browserState);
  if (!service) {
    return nil;
  }
  return [[ShunyaWalletKeyringServiceMojoImpl alloc]
      initWithKeyringService:std::move(service)];
}
@end

@implementation ShunyaWalletServiceFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto service =
      shunya_wallet::ShunyaWalletServiceFactory::GetForBrowserState(browserState);
  if (!service) {
    return nil;
  }
  return [[ShunyaWalletShunyaWalletServiceMojoImpl alloc]
      initWithShunyaWalletService:std::move(service)];
}
@end

@implementation ShunyaWalletSwapServiceFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto service =
      shunya_wallet::SwapServiceFactory::GetForBrowserState(browserState);
  if (!service) {
    return nil;
  }
  return [[ShunyaWalletSwapServiceMojoImpl alloc]
      initWithSwapService:std::move(service)];
}
@end

@implementation ShunyaWalletIpfsServiceFactory
+ (nullable id)serviceForBrowserState:(ChromeBrowserState*)browserState {
  auto service =
      shunya_wallet::ShunyaWalletIpfsServiceFactory::GetForBrowserState(
          browserState);
  if (!service) {
    return nil;
  }
  return [[ShunyaWalletIpfsServiceMojoImpl alloc]
      initWithIpfsService:std::move(service)];
}
@end

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/shunya_wallet/shunya_wallet_service_factory.h"

#include <memory>

#include "shunya/components/shunya_wallet/browser/shunya_wallet_service.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_service_delegate.h"
#include "shunya/ios/browser/shunya_wallet/json_rpc_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/keyring_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/tx_service_factory.h"
#include "components/keyed_service/core/keyed_service.h"
#include "components/keyed_service/ios/browser_state_dependency_manager.h"
#include "ios/chrome/browser/shared/model/application_context/application_context.h"
#include "ios/chrome/browser/shared/model/browser_state/browser_state_otr_helper.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state.h"
#include "ios/web/public/browser_state.h"
#include "services/network/public/cpp/shared_url_loader_factory.h"

namespace shunya_wallet {

// static
mojo::PendingRemote<mojom::ShunyaWalletService>
ShunyaWalletServiceFactory::GetForBrowserState(
    ChromeBrowserState* browser_state) {
  return static_cast<ShunyaWalletService*>(
             GetInstance()->GetServiceForBrowserState(browser_state, true))
      ->MakeRemote();
}

// static
ShunyaWalletService* ShunyaWalletServiceFactory::GetServiceForState(
    ChromeBrowserState* browser_state) {
  return static_cast<ShunyaWalletService*>(
      GetInstance()->GetServiceForBrowserState(browser_state, true));
}

// static
ShunyaWalletServiceFactory* ShunyaWalletServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaWalletServiceFactory> instance;
  return instance.get();
}

ShunyaWalletServiceFactory::ShunyaWalletServiceFactory()
    : BrowserStateKeyedServiceFactory(
          "ShunyaWalletService",
          BrowserStateDependencyManager::GetInstance()) {
  DependsOn(KeyringServiceFactory::GetInstance());
  DependsOn(JsonRpcServiceFactory::GetInstance());
  DependsOn(TxServiceFactory::GetInstance());
}

ShunyaWalletServiceFactory::~ShunyaWalletServiceFactory() = default;

std::unique_ptr<KeyedService>
ShunyaWalletServiceFactory::BuildServiceInstanceFor(
    web::BrowserState* context) const {
  auto* browser_state = ChromeBrowserState::FromBrowserState(context);
  auto* keyring_service =
      KeyringServiceFactory::GetServiceForState(browser_state);
  auto* json_rpc_service =
      JsonRpcServiceFactory::GetServiceForState(browser_state);
  auto* tx_service = TxServiceFactory::GetServiceForState(browser_state);
  auto shared_url_loader_factory = browser_state->GetSharedURLLoaderFactory();
  std::unique_ptr<ShunyaWalletService> service(new ShunyaWalletService(
      shared_url_loader_factory, std::make_unique<ShunyaWalletServiceDelegate>(),
      keyring_service, json_rpc_service, tx_service, browser_state->GetPrefs(),
      GetApplicationContext()->GetLocalState()));
  return service;
}

bool ShunyaWalletServiceFactory::ServiceIsNULLWhileTesting() const {
  return true;
}

web::BrowserState* ShunyaWalletServiceFactory::GetBrowserStateToUse(
    web::BrowserState* context) const {
  return GetBrowserStateRedirectedInIncognito(context);
}

}  // namespace shunya_wallet

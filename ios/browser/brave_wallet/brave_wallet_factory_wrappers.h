/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_FACTORY_WRAPPERS_H_
#define SHUNYA_IOS_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_FACTORY_WRAPPERS_H_

#import <Foundation/Foundation.h>
#include "keyed_service_factory_wrapper.h"  // NOLINT

@protocol ShunyaWalletAssetRatioService
, ShunyaWalletShunyaWalletService, ShunyaWalletJsonRpcService,
    ShunyaWalletEthTxManagerProxy, ShunyaWalletSolanaTxManagerProxy,
    ShunyaWalletTxService, ShunyaWalletKeyringService, ShunyaWalletSwapService,
    ShunyaWalletIpfsService;

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.AssetRatioServiceFactory)
@interface ShunyaWalletAssetRatioServiceFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletAssetRatioService>>
@end

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.JsonRpcServiceFactory)
@interface ShunyaWalletJsonRpcServiceFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletJsonRpcService>>
@end

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.TxServiceFactory)
@interface ShunyaWalletTxServiceFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletTxService>>
@end

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.EthTxManagerProxyFactory)
@interface ShunyaWalletEthTxManagerProxyFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletEthTxManagerProxy>>
@end

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.SolanaTxManagerProxyFactory)
@interface ShunyaWalletSolanaTxManagerProxyFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletSolanaTxManagerProxy>>
@end

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.KeyringServiceFactory)
@interface ShunyaWalletKeyringServiceFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletKeyringService>>
@end

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.ServiceFactory)
@interface ShunyaWalletServiceFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletShunyaWalletService>>
@end

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.SwapServiceFactory)
@interface ShunyaWalletSwapServiceFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletSwapService>>
@end

OBJC_EXPORT
NS_SWIFT_NAME(ShunyaWallet.IpfsServiceFactory)
@interface ShunyaWalletIpfsServiceFactory
    : KeyedServiceFactoryWrapper <id <ShunyaWalletIpfsService>>
@end

#endif  // SHUNYA_IOS_BROWSER_SHUNYA_WALLET_SHUNYA_WALLET_FACTORY_WRAPPERS_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/shunya_wallet/shunya_wallet_ipfs_service_factory.h"

#include "shunya/components/shunya_wallet/browser/shunya_wallet_ipfs_service.h"
#include "shunya/components/shunya_wallet/browser/pref_names.h"
#include "shunya/ios/browser/shunya_wallet/json_rpc_service_factory.h"
#include "components/keyed_service/core/keyed_service.h"
#include "components/keyed_service/ios/browser_state_dependency_manager.h"
#include "ios/chrome/browser/shared/model/application_context/application_context.h"
#include "ios/chrome/browser/shared/model/browser_state/browser_state_otr_helper.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state.h"
#include "ios/web/public/browser_state.h"

namespace shunya_wallet {

// static
mojo::PendingRemote<mojom::IpfsService>
ShunyaWalletIpfsServiceFactory::GetForBrowserState(
    ChromeBrowserState* browser_state) {
  return static_cast<ShunyaWalletIpfsService*>(
             GetInstance()->GetServiceForBrowserState(browser_state, true))
      ->MakeRemote();
}

// static
ShunyaWalletIpfsService* ShunyaWalletIpfsServiceFactory::GetServiceForState(
    ChromeBrowserState* browser_state) {
  return static_cast<ShunyaWalletIpfsService*>(
      GetInstance()->GetServiceForBrowserState(browser_state, true));
}

// static
ShunyaWalletIpfsServiceFactory* ShunyaWalletIpfsServiceFactory::GetInstance() {
  static base::NoDestructor<ShunyaWalletIpfsServiceFactory> instance;
  return instance.get();
}

ShunyaWalletIpfsServiceFactory::ShunyaWalletIpfsServiceFactory()
    : BrowserStateKeyedServiceFactory(
          "ShunyaWalletIpfsService",
          BrowserStateDependencyManager::GetInstance()) {}

ShunyaWalletIpfsServiceFactory::~ShunyaWalletIpfsServiceFactory() = default;

std::unique_ptr<KeyedService>
ShunyaWalletIpfsServiceFactory::BuildServiceInstanceFor(
    web::BrowserState* context) const {
  auto* browser_state = ChromeBrowserState::FromBrowserState(context);
  std::unique_ptr<ShunyaWalletIpfsService> ipfs_service(
      new ShunyaWalletIpfsService(browser_state->GetPrefs()));
  return ipfs_service;
}

bool ShunyaWalletIpfsServiceFactory::ServiceIsNULLWhileTesting() const {
  return true;
}

web::BrowserState* ShunyaWalletIpfsServiceFactory::GetBrowserStateToUse(
    web::BrowserState* context) const {
  return GetBrowserStateRedirectedInIncognito(context);
}

}  // namespace shunya_wallet

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_COMPONENT_UPDATER_COMPONENT_UPDATER_UTILS_H_
#define SHUNYA_IOS_BROWSER_COMPONENT_UPDATER_COMPONENT_UPDATER_UTILS_H_

#include <string>

namespace component_updater {

void ShunyaOnDemandUpdate(const std::string& component_id);

}  // namespace component_updater

#endif  // SHUNYA_IOS_BROWSER_COMPONENT_UPDATER_COMPONENT_UPDATER_UTILS_H_

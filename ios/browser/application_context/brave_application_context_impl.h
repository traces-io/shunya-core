// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_IOS_BROWSER_APPLICATION_CONTEXT_SHUNYA_APPLICATION_CONTEXT_IMPL_H_
#define SHUNYA_IOS_BROWSER_APPLICATION_CONTEXT_SHUNYA_APPLICATION_CONTEXT_IMPL_H_

#include <memory>
#include <string>

#include "shunya/components/shunya_component_updater/browser/shunya_component.h"
#include "shunya/components/url_sanitizer/browser/url_sanitizer_component_installer.h"
#include "ios/chrome/browser/application_context/application_context_impl.h"

namespace base {
class CommandLine;
class SequencedTaskRunner;
}  // namespace base

/// This extends the behaviors of the ApplicationContext
class ShunyaApplicationContextImpl : public ApplicationContextImpl {
 public:
  // Out-of-line constructor declaration
  ShunyaApplicationContextImpl(
      base::SequencedTaskRunner* local_state_task_runner,
      const base::CommandLine& command_line,
      const std::string& locale,
      const std::string& country);

  shunya::URLSanitizerComponentInstaller* url_sanitizer_component_installer();

  // Disable copy constructor and assignment operator
  ShunyaApplicationContextImpl(const ShunyaApplicationContextImpl&) = delete;
  ShunyaApplicationContextImpl& operator=(const ShunyaApplicationContextImpl&) =
      delete;

  // Start any services that we may need later
  void StartShunyaServices();

  // Out-of-line destructor declaration
  ~ShunyaApplicationContextImpl() override;

 private:
  shunya_component_updater::ShunyaComponent::Delegate*
  shunya_component_updater_delegate();
  shunya_component_updater::LocalDataFilesService* local_data_files_service();

  std::unique_ptr<shunya_component_updater::ShunyaComponent::Delegate>
      shunya_component_updater_delegate_;
  std::unique_ptr<shunya_component_updater::LocalDataFilesService>
      local_data_files_service_;
  std::unique_ptr<shunya::URLSanitizerComponentInstaller>
      url_sanitizer_component_installer_;
};

#endif  // SHUNYA_IOS_BROWSER_APPLICATION_CONTEXT_SHUNYA_APPLICATION_CONTEXT_IMPL_H_

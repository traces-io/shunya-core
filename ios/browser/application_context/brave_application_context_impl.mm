// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/ios/browser/application_context/shunya_application_context_impl.h"

#include <string>

#import "base/command_line.h"
#import "base/task/sequenced_task_runner.h"
#include "shunya/components/shunya_component_updater/browser/shunya_component.h"
#include "shunya/components/shunya_component_updater/browser/shunya_component_updater_delegate.h"
#include "shunya/components/shunya_component_updater/browser/local_data_files_service.h"
#include "shunya/components/url_sanitizer/browser/url_sanitizer_component_installer.h"
#include "ios/chrome/browser/shared/model/application_context/application_context.h"

ShunyaApplicationContextImpl::ShunyaApplicationContextImpl(
    base::SequencedTaskRunner* local_state_task_runner,
    const base::CommandLine& command_line,
    const std::string& locale,
    const std::string& country)
    : ApplicationContextImpl(local_state_task_runner,
                             command_line,
                             locale,
                             country) {}

inline ShunyaApplicationContextImpl::~ShunyaApplicationContextImpl() = default;

shunya_component_updater::ShunyaComponent::Delegate*
ShunyaApplicationContextImpl::shunya_component_updater_delegate() {
  if (!shunya_component_updater_delegate_) {
    shunya_component_updater_delegate_ =
        std::make_unique<shunya::ShunyaComponentUpdaterDelegate>(
            GetComponentUpdateService(), GetLocalState(),
            GetApplicationLocale());
  }

  return shunya_component_updater_delegate_.get();
}

shunya_component_updater::LocalDataFilesService*
ShunyaApplicationContextImpl::local_data_files_service() {
  if (!local_data_files_service_) {
    local_data_files_service_ =
        shunya_component_updater::LocalDataFilesServiceFactory(
            shunya_component_updater_delegate());
  }
  return local_data_files_service_.get();
}

shunya::URLSanitizerComponentInstaller*
ShunyaApplicationContextImpl::url_sanitizer_component_installer() {
  if (!url_sanitizer_component_installer_) {
    url_sanitizer_component_installer_ =
        std::make_unique<shunya::URLSanitizerComponentInstaller>(
            local_data_files_service());
  }
  return url_sanitizer_component_installer_.get();
}

void ShunyaApplicationContextImpl::StartShunyaServices() {
  // We need to Initialize the component installer
  // before calling Start on the local_data_files_service
  url_sanitizer_component_installer();

  // Start the local data file service
  local_data_files_service()->Start();
}

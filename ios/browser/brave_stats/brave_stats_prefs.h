/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_SHUNYA_STATS_SHUNYA_STATS_PREFS_H_
#define SHUNYA_IOS_BROWSER_SHUNYA_STATS_SHUNYA_STATS_PREFS_H_

class PrefRegistrySimple;

namespace shunya_stats {

void RegisterLocalStatePrefs(PrefRegistrySimple* registry);

}  // namespace shunya_stats

#endif  // SHUNYA_IOS_BROWSER_SHUNYA_STATS_SHUNYA_STATS_PREFS_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/shunya_stats/shunya_stats_prefs.h"

#include "base/time/time.h"
#include "shunya/components/shunya_wallet/browser/pref_names.h"
#include "components/prefs/pref_registry_simple.h"

namespace shunya_stats {

void RegisterLocalStatePrefs(PrefRegistrySimple* registry) {
  registry->RegisterTimePref(kShunyaWalletPingReportedUnlockTime, base::Time());
}

}  // namespace shunya_stats

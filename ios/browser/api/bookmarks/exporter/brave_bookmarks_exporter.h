/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_BOOKMARKS_EXPORTER_SHUNYA_BOOKMARKS_EXPORTER_H_
#define SHUNYA_IOS_BROWSER_API_BOOKMARKS_EXPORTER_SHUNYA_BOOKMARKS_EXPORTER_H_

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ShunyaBookmarksExporterState) {
  ShunyaBookmarksExporterStateCompleted,
  ShunyaBookmarksExporterStateStarted,
  ShunyaBookmarksExporterStateCancelled,
  ShunyaBookmarksExporterStateErrorCreatingFile,
  ShunyaBookmarksExporterStateErrorWritingHeader,
  ShunyaBookmarksExporterStateErrorWritingNodes
};

@class IOSBookmarkNode;

OBJC_EXPORT
@interface ShunyaBookmarksExporter : NSObject
- (instancetype)init;

- (void)exportToFile:(NSString*)filePath
        withListener:(void (^)(ShunyaBookmarksExporterState))listener;

- (void)exportToFile:(NSString*)filePath
           bookmarks:(NSArray<IOSBookmarkNode*>*)bookmarks
        withListener:(void (^)(ShunyaBookmarksExporterState))listener;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_BOOKMARKS_EXPORTER_SHUNYA_BOOKMARKS_EXPORTER_H_

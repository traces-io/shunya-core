/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_P3A_SHUNYA_HISTOGRAMS_CONTROLLER_PRIVATE_H_
#define SHUNYA_IOS_BROWSER_API_P3A_SHUNYA_HISTOGRAMS_CONTROLLER_PRIVATE_H_

#include "shunya/ios/browser/api/p3a/shunya_histograms_controller.h"

class ChromeBrowserState;

NS_ASSUME_NONNULL_BEGIN

@interface ShunyaHistogramsController (Private)
- (instancetype)initWithBrowserState:(ChromeBrowserState*)mainBrowserState;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_P3A_SHUNYA_HISTOGRAMS_CONTROLLER_PRIVATE_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/api/shunya_stats/shunya_stats.h"

#include "base/strings/sys_string_conversions.h"
#include "shunya/components/shunya_stats/browser/shunya_stats_updater_util.h"
#include "shunya/components/shunya_wallet/browser/pref_names.h"
#include "components/prefs/pref_service.h"
#include "ios/chrome/browser/shared/model/application_context/application_context.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state.h"

#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

@implementation ShunyaStats {
  PrefService* _localPrefs;
  PrefService* _profilePrefs;
}

- (instancetype)initWithBrowserState:(ChromeBrowserState*)browserState {
  if ((self = [super init])) {
    _profilePrefs = browserState->GetPrefs();
    _localPrefs = GetApplicationContext()->GetLocalState();
  }
  return self;
}

- (NSDictionary<NSString*, NSString*>*)walletParams {
  auto wallet_last_unlocked = _localPrefs->GetTime(kShunyaWalletLastUnlockTime);
  auto last_reported_wallet_unlock =
      _localPrefs->GetTime(kShunyaWalletPingReportedUnlockTime);
  uint8_t usage_bitset = 0;
  if (wallet_last_unlocked > last_reported_wallet_unlock) {
    usage_bitset = shunya_stats::UsageBitfieldFromTimestamp(
        wallet_last_unlocked, last_reported_wallet_unlock);
  }
  return @{@"wallet2" : base::SysUTF8ToNSString(std::to_string(usage_bitset))};
}

- (void)notifyStatsPingSent {
  auto wallet_last_unlocked = _localPrefs->GetTime(kShunyaWalletLastUnlockTime);
  _localPrefs->SetTime(kShunyaWalletPingReportedUnlockTime,
                       wallet_last_unlocked);
}

@end

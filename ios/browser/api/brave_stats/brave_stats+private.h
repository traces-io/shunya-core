/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SHUNYA_STATS_SHUNYA_STATS_PRIVATE_H_
#define SHUNYA_IOS_BROWSER_API_SHUNYA_STATS_SHUNYA_STATS_PRIVATE_H_

#include "shunya/ios/browser/api/shunya_stats/shunya_stats.h"

class ChromeBrowserState;

@interface ShunyaStats (Private)
- (instancetype)initWithBrowserState:(ChromeBrowserState*)browserState;
@end

#endif  // SHUNYA_IOS_BROWSER_API_SHUNYA_STATS_SHUNYA_STATS_PRIVATE_H_

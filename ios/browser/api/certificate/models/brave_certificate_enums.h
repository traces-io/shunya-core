/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_CERTIFICATE_MODELS_SHUNYA_CERTIFICATE_ENUMS_H_
#define SHUNYA_IOS_BROWSER_API_CERTIFICATE_MODELS_SHUNYA_CERTIFICATE_ENUMS_H_

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, ShunyaPublicKeyUsage) {
  ShunyaPublicKeyUsage_INVALID = 1 << 0,
  ShunyaPublicKeyUsage_ENCRYPT = 1 << 1,
  ShunyaPublicKeyUsage_DECRYPT = 1 << 2,
  ShunyaPublicKeyUsage_SIGN = 1 << 3,
  ShunyaPublicKeyUsage_VERIFY = 1 << 4,
  ShunyaPublicKeyUsage_WRAP = 1 << 5,
  ShunyaPublicKeyUsage_DERIVE = 1 << 6,
  ShunyaPublicKeyUsage_ANY = 1 << 7
};

typedef NS_ENUM(NSUInteger, ShunyaPublicKeyType) {
  ShunyaPublicKeyType_UNKNOWN,
  ShunyaPublicKeyType_RSA,
  ShunyaPublicKeyType_DSA,
  ShunyaPublicKeyType_DH,
  ShunyaPublicKeyType_EC
};

typedef NS_ENUM(NSUInteger, ShunyaFingerprintType) {
  ShunyaFingerprintType_SHA1,
  ShunyaFingerprintType_SHA256
};

#endif  // SHUNYA_IOS_BROWSER_API_CERTIFICATE_MODELS_SHUNYA_CERTIFICATE_ENUMS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_CERTIFICATE_MODELS_SHUNYA_CERTIFICATE_MODEL_PRIVATE_H_
#define SHUNYA_IOS_BROWSER_API_CERTIFICATE_MODELS_SHUNYA_CERTIFICATE_MODEL_PRIVATE_H_

#import <Foundation/Foundation.h>
#import <Security/Security.h>

#include "shunya/ios/browser/api/certificate/models/shunya_certificate_enums.h"
#include "shunya/ios/browser/api/certificate/models/shunya_certificate_fingerprint.h"
#include "shunya/ios/browser/api/certificate/models/shunya_certificate_public_key_info.h"
#include "shunya/ios/browser/api/certificate/models/shunya_certificate_rdns_sequence.h"
#include "shunya/ios/browser/api/certificate/models/shunya_certificate_signature.h"

namespace net {
class ParsedCertificate;

namespace der {
class BitString;
class Input;
}  // namespace der
}  // namespace net

typedef NS_ENUM(NSUInteger, ShunyaFingerprintType);

NS_ASSUME_NONNULL_BEGIN

@interface ShunyaCertificateSignature ()
- (instancetype)initWithCertificate:(const net::ParsedCertificate*)certificate;
@end

@interface ShunyaCertificatePublicKeyInfo ()
- (instancetype)initWithCertificate:(const net::ParsedCertificate*)certificate
                            withKey:(SecKeyRef)key;
@end

@interface ShunyaCertificateFingerprint ()
- (instancetype)initWithCertificate:(CFDataRef)cert_data
                           withType:(ShunyaFingerprintType)type;
@end

@interface ShunyaCertificateRDNSequence ()
- (instancetype)initWithBERName:(const net::der::Input&)berName
                       uniqueId:(const net::der::BitString&)uniqueId;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_CERTIFICATE_MODELS_SHUNYA_CERTIFICATE_MODEL_PRIVATE_H_

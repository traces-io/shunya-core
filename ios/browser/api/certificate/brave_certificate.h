/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_CERTIFICATE_SHUNYA_CERTIFICATE_H_
#define SHUNYA_IOS_BROWSER_API_CERTIFICATE_SHUNYA_CERTIFICATE_H_

#import <Foundation/Foundation.h>
#import <Security/Security.h>

NS_ASSUME_NONNULL_BEGIN

@class ShunyaCertificateRDNSequence;
@class ShunyaCertificateSignature;
@class ShunyaCertificatePublicKeyInfo;
@class ShunyaCertificateFingerprint;
// @class ShunyaCertificateExtensionModel;

OBJC_EXPORT
@interface ShunyaCertificateModel : NSObject
@property(nonatomic, readonly) bool isRootCertificate;
@property(nonatomic, readonly) bool isCertificateAuthority;
@property(nonatomic, readonly) bool isSelfSigned;
@property(nonatomic, readonly) bool isSelfIssued;
@property(nonatomic, readonly) ShunyaCertificateRDNSequence* subjectName;
@property(nonatomic, readonly) ShunyaCertificateRDNSequence* issuerName;
@property(nonatomic, readonly) NSString* serialNumber;
@property(nonatomic, readonly) NSUInteger version;
@property(nonatomic, readonly) ShunyaCertificateSignature* signature;
@property(nonatomic, readonly) NSDate* notValidBefore;
@property(nonatomic, readonly) NSDate* notValidAfter;
@property(nonatomic, readonly) ShunyaCertificatePublicKeyInfo* publicKeyInfo;
// @property(nonatomic, readonly)
// NSArray<ShunyaCertificateExtensionModel*>* extensions;
@property(nonatomic, readonly) ShunyaCertificateFingerprint* sha1Fingerprint;
@property(nonatomic, readonly) ShunyaCertificateFingerprint* sha256Fingerprint;

- (nullable instancetype)initWithCertificate:(SecCertificateRef)certificate;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_CERTIFICATE_SHUNYA_CERTIFICATE_H_

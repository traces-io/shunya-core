/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_URL_URL_SPOOF_CHECKER_H_
#define SHUNYA_IOS_BROWSER_API_URL_URL_SPOOF_CHECKER_H_

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// The |SkeletonType| and |TopDomainEntry| are mirrored in trie_entry.h. These
/// are used to insert and read nodes from the Trie.
/// The type of skeleton in the trie node.
typedef NSInteger ShunyaSpoofCheckerSkeletonType NS_TYPED_ENUM
    NS_SWIFT_NAME(URLSpoofChecker.SkeletonType);
OBJC_EXPORT ShunyaSpoofCheckerSkeletonType const
    ShunyaSpoofCheckerSkeletonTypeFull;
OBJC_EXPORT ShunyaSpoofCheckerSkeletonType const
    ShunyaSpoofCheckerSkeletonTypeSeparatorsRemoved;

typedef NSInteger ShunyaSpoofCheckerLookalikeURLMatchType NS_TYPED_ENUM
    NS_SWIFT_NAME(URLSpoofChecker.LookalikeURLMatchType);

OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeNone;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeSkeletonMatchSiteEngagement;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeEditDistance;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeEditDistanceSiteEngagement;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeTargetEmbedding;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeSkeletonMatchTop500;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeSkeletonMatchTop5k;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeTargetEmbeddingForSafetyTips;
/// The domain name failed IDN spoof checks but didn't match a safe hostname.
/// As a result, there is no URL to suggest to the user in the form of "Did
/// you mean <url>?".
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeFailedSpoofChecks;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeCharacterSwapSiteEngagement;
OBJC_EXPORT ShunyaSpoofCheckerLookalikeURLMatchType const
    ShunyaSpoofCheckerLookalikeURLMatchTypeCharacterSwapTop500;

OBJC_EXPORT
NS_SWIFT_NAME(URLSpoofChecker.TopDomainEntry)
@interface URLSpoofCheckerTopDomainEntry : NSObject
- (instancetype)init NS_UNAVAILABLE;
/// The domain name.
@property(nonatomic, readonly) NSString* domain;
/// True if the domain is in the top 1000 bucket.
@property(nonatomic, readonly) bool isTopBucket;
/// Type of the skeleton stored in the trie node.
@property(nonatomic, readonly) ShunyaSpoofCheckerSkeletonType skeletonType;
@end

OBJC_EXPORT
NS_SWIFT_NAME(URLSpoofChecker.Result)
@interface ShunyaURLSpoofCheckerResult : NSObject
- (instancetype)init NS_UNAVAILABLE;
@property(nonatomic, readonly)
    ShunyaSpoofCheckerLookalikeURLMatchType urlMatchType;
@property(nonatomic, readonly, nullable) NSURL* suggestedURL;
@end

OBJC_EXPORT
NS_SWIFT_NAME(URLSpoofChecker)
@interface ShunyaURLSpoofChecker : NSObject
- (instancetype)init NS_UNAVAILABLE;
+ (URLSpoofCheckerTopDomainEntry*)getSimilarTopDomain:(NSString*)hostname;
+ (URLSpoofCheckerTopDomainEntry*)lookupSkeletonInTopDomains:
    (NSString*)hostname;
+ (NSArray<NSString*>*)getSkeletons:(NSString*)url;
+ (ShunyaURLSpoofCheckerResult*)isLookalikeURL:(NSString*)url;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_URL_URL_SPOOF_CHECKER_H_

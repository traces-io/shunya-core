/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_URL_URL_FORMATTER_H_
#define SHUNYA_IOS_BROWSER_API_URL_URL_FORMATTER_H_

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NSInteger ShunyaURLSchemeDisplay NS_TYPED_ENUM
    NS_SWIFT_NAME(URLFormatter.SchemeDisplay);

OBJC_EXPORT ShunyaURLSchemeDisplay const ShunyaURLSchemeDisplayShow;
OBJC_EXPORT ShunyaURLSchemeDisplay const ShunyaURLSchemeDisplayOmitHttpAndHttps;
/// Omit cryptographic (i.e. https and wss).
OBJC_EXPORT ShunyaURLSchemeDisplay const ShunyaURLSchemeDisplayOmitCryptographic;

NS_SWIFT_NAME(URLFormatter.FormatType)
typedef NS_OPTIONS(NSUInteger, ShunyaURLFormatterFormatType) {
  ShunyaURLFormatterFormatTypeOmitNothing = 0,
  ShunyaURLFormatterFormatTypeOmitUsernamePassword = 1 << 0,
  ShunyaURLFormatterFormatTypeOmitHTTP = 1 << 1,
  ShunyaURLFormatterFormatTypeOmitTrailingSlashOnBareHostname = 1 << 2,
  ShunyaURLFormatterFormatTypeOmitHTTPS = 1 << 3,
  ShunyaURLFormatterFormatTypeOmitTrivialSubdomains = 1 << 5,
  ShunyaURLFormatterFormatTypeTrimAfterHost = 1 << 6,
  ShunyaURLFormatterFormatTypeOmitFileScheme = 1 << 7,
  ShunyaURLFormatterFormatTypeOmitMailToScheme = 1 << 8,
  ShunyaURLFormatterFormatTypeOmitMobilePrefix = 1 << 9,

  /// Omits Username & Password, HTTP (not HTTPS), and Trailing Slash
  ShunyaURLFormatterFormatTypeOmitDefaults =
      ShunyaURLFormatterFormatTypeOmitUsernamePassword |
      ShunyaURLFormatterFormatTypeOmitHTTP |
      ShunyaURLFormatterFormatTypeOmitTrailingSlashOnBareHostname
};

NS_SWIFT_NAME(URLFormatter.UnescapeRule)
typedef NS_OPTIONS(NSUInteger, ShunyaURLFormatterUnescapeRule) {
  ShunyaURLFormatterUnescapeRuleNone = 0,
  ShunyaURLFormatterUnescapeRuleNormal = 1 << 0,
  ShunyaURLFormatterUnescapeRuleSpaces = 1 << 1,
  ShunyaURLFormatterUnescapeRulePathSeparators = 1 << 2,
  ShunyaURLFormatterUnescapeRuleSpecialCharsExceptPathSeparators = 1 << 3,
  ShunyaURLFormatterUnescapeRuleReplacePlusWithSpace = 1 << 4
};

OBJC_EXPORT
NS_SWIFT_NAME(URLFormatter)
@interface ShunyaURLFormatter : NSObject
- (instancetype)init NS_UNAVAILABLE;

/// Format a URL "origin/host" for Security Display
/// origin - The origin of the URL to format
/// schemeDisplay - Determines whether or not to omit the scheme
+ (NSString*)formatURLOriginForSecurityDisplay:(NSString*)origin
                                 schemeDisplay:
                                     (ShunyaURLSchemeDisplay)schemeDisplay;

/// Format a URL "origin/host" omitting the scheme, path, and trivial
/// sub-domains. origin - The origin to be formatted
+ (NSString*)formatURLOriginForDisplayOmitSchemePathAndTrivialSubdomains:
    (NSString*)origin;

/// Format a URL
/// url - The URL string to be formatted
/// formatTypes - Formatter options when formatting the URL. Typically used to
/// omit certain parts of a URL unescapeOptions - Options passed to the
/// formatter for UN-Escaping parts of a URL
+ (NSString*)formatURL:(NSString*)url
           formatTypes:(ShunyaURLFormatterFormatType)formatTypes
       unescapeOptions:(ShunyaURLFormatterUnescapeRule)unescapeOptions;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_URL_URL_FORMATTER_H_

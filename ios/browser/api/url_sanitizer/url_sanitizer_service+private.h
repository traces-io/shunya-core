// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_IOS_BROWSER_API_URL_SANITIZER_URL_SANITIZER_SERVICE_PRIVATE_H_
#define SHUNYA_IOS_BROWSER_API_URL_SANITIZER_URL_SANITIZER_SERVICE_PRIVATE_H_

#import <Foundation/Foundation.h>

#include "base/memory/raw_ptr.h"
#include "shunya/components/url_sanitizer/browser/url_sanitizer_service.h"
#include "shunya/ios/browser/api/url_sanitizer/url_sanitizer_service.h"

NS_ASSUME_NONNULL_BEGIN

@interface URLSanitizerService (Private)

- (instancetype)initWithURLSanitizerService:
    (shunya::URLSanitizerService*)urlSanitizer;

@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_URL_SANITIZER_URL_SANITIZER_SERVICE_PRIVATE_H_

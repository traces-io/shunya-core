/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_API_PRIVATE_H_
#define SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_API_PRIVATE_H_

#import <Foundation/Foundation.h>

#include "shunya/ios/browser/api/shunya_wallet/shunya_wallet_api.h"

NS_ASSUME_NONNULL_BEGIN

class ChromeBrowserState;

@interface ShunyaWalletAPI (Private)
- (instancetype)initWithBrowserState:(ChromeBrowserState*)mainBrowserState;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_API_PRIVATE_H_

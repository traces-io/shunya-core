/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/api/shunya_wallet/shunya_wallet_provider_delegate_ios.h"

#include "base/strings/sys_string_conversions.h"
#include "shunya/base/mac/conversions.h"
#include "shunya/components/shunya_wallet/common/shunya_wallet.mojom.h"
#include "shunya/ios/browser/api/shunya_wallet/shunya_wallet_provider_delegate_ios+private.h"
#include "shunya/ios/browser/api/url/url_origin_ios+private.h"
#include "net/base/mac/url_conversions.h"

namespace shunya_wallet {

bool ShunyaWalletProviderDelegateBridge::IsTabVisible() {
  return [bridge_ isTabVisible];
}

void ShunyaWalletProviderDelegateBridge::ShowPanel() {
  [bridge_ showPanel];
}

void ShunyaWalletProviderDelegateBridge::WalletInteractionDetected() {
  [bridge_ walletInteractionDetected];
}

url::Origin ShunyaWalletProviderDelegateBridge::GetOrigin() const {
  return url::Origin([[bridge_ getOrigin] underlyingOrigin]);
}

void ShunyaWalletProviderDelegateBridge::ShowWalletOnboarding() {
  [bridge_ showWalletOnboarding];
}

void ShunyaWalletProviderDelegateBridge::ShowAccountCreation(
    mojom::CoinType type) {
  [bridge_ showAccountCreation:static_cast<ShunyaWalletCoinType>(type)];
}

void ShunyaWalletProviderDelegateBridge::RequestPermissions(
    mojom::CoinType type,
    const std::vector<std::string>& accounts,
    RequestPermissionsCallback callback) {
  auto completion = [callback = std::make_shared<decltype(callback)>(std::move(
                         callback))](ShunyaWalletRequestPermissionsError error,
                                     NSArray<NSString*>* _Nullable results) {
    if (!callback) {
      return;
    }
    if (results == nil) {
      std::move(*callback).Run(
          static_cast<mojom::RequestPermissionsError>(error), absl::nullopt);
      return;
    }
    std::vector<std::string> v;
    for (NSString* result in results) {
      v.push_back(base::SysNSStringToUTF8(result));
    }
    std::move(*callback).Run(static_cast<mojom::RequestPermissionsError>(error),
                             v);
  };
  [bridge_ requestPermissions:static_cast<ShunyaWalletCoinType>(type)
                     accounts:shunya::vector_to_ns(accounts)
                   completion:completion];
}

bool ShunyaWalletProviderDelegateBridge::IsAccountAllowed(
    mojom::CoinType type,
    const std::string& account) {
  return [bridge_ isAccountAllowed:static_cast<ShunyaWalletCoinType>(type)
                           account:base::SysUTF8ToNSString(account)];
}

absl::optional<std::vector<std::string>>
ShunyaWalletProviderDelegateBridge::GetAllowedAccounts(
    mojom::CoinType type,
    const std::vector<std::string>& accounts) {
  NSArray<NSString*>* results =
      [bridge_ getAllowedAccounts:static_cast<ShunyaWalletCoinType>(type)
                         accounts:shunya::vector_to_ns(accounts)];
  if (!results) {
    return absl::nullopt;
  }

  return shunya::ns_to_vector<std::string>(results);
}

bool ShunyaWalletProviderDelegateBridge::IsPermissionDenied(
    mojom::CoinType type) {
  return [bridge_ isPermissionDenied:static_cast<ShunyaWalletCoinType>(type)];
}

void ShunyaWalletProviderDelegateBridge::AddSolanaConnectedAccount(
    const std::string& account) {
  [bridge_ addSolanaConnectedAccount:base::SysUTF8ToNSString(account)];
}

void ShunyaWalletProviderDelegateBridge::RemoveSolanaConnectedAccount(
    const std::string& account) {
  [bridge_ removeSolanaConnectedAccount:base::SysUTF8ToNSString(account)];
}

bool ShunyaWalletProviderDelegateBridge::IsSolanaAccountConnected(
    const std::string& account) {
  return [bridge_ isSolanaAccountConnected:base::SysUTF8ToNSString(account)];
}

}  // namespace shunya_wallet

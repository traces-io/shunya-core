/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/api/shunya_wallet/shunya_wallet_api.h"

#include "base/strings/sys_string_conversions.h"
#include "shunya/components/shunya_wallet/browser/blockchain_registry.h"
#include "shunya/components/shunya_wallet/browser/ethereum_provider_impl.h"
#include "shunya/components/shunya_wallet/browser/solana_provider_impl.h"
#include "shunya/components/shunya_wallet/resources/grit/shunya_wallet_script_generated.h"
#include "shunya/ios/browser/api/shunya_wallet/shunya_wallet.mojom.objc+private.h"
#include "shunya/ios/browser/api/shunya_wallet/shunya_wallet_provider_delegate_ios+private.h"
#include "shunya/ios/browser/api/shunya_wallet/shunya_wallet_provider_delegate_ios.h"
#include "shunya/ios/browser/shunya_wallet/shunya_wallet_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/json_rpc_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/keyring_service_factory.h"
#include "shunya/ios/browser/shunya_wallet/tx_service_factory.h"
#include "components/grit/shunya_components_resources.h"
#include "ios/chrome/browser/content_settings/host_content_settings_map_factory.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state_manager.h"
#include "ui/base/resource/resource_bundle.h"

ShunyaWalletProviderScriptKey const ShunyaWalletProviderScriptKeyEthereum =
    @"ethereum_provider.js";
ShunyaWalletProviderScriptKey const ShunyaWalletProviderScriptKeySolana =
    @"solana_provider.js";
ShunyaWalletProviderScriptKey const ShunyaWalletProviderScriptKeySolanaWeb3 =
    @"solana_web3.js";
ShunyaWalletProviderScriptKey const ShunyaWalletProviderScriptKeyWalletStandard =
    @"wallet_standard.js";

#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

@implementation ShunyaWalletAPI {
  ChromeBrowserState* _mainBrowserState;  // NOT OWNED
  NSMutableDictionary<NSNumber* /* ShunyaWalletCoinType */,
                      NSDictionary<ShunyaWalletProviderScriptKey, NSString*>*>*
      _providerScripts;
}

- (instancetype)initWithBrowserState:(ChromeBrowserState*)mainBrowserState {
  if ((self = [super init])) {
    _mainBrowserState = mainBrowserState;
    _providerScripts = [[NSMutableDictionary alloc] init];
  }
  return self;
}

+ (id<ShunyaWalletBlockchainRegistry>)blockchainRegistry {
  auto* registry = shunya_wallet::BlockchainRegistry::GetInstance();
  return [[ShunyaWalletBlockchainRegistryMojoImpl alloc]
      initWithBlockchainRegistry:registry->MakeRemote()];
}

- (nullable id<ShunyaWalletEthereumProvider>)
    ethereumProviderWithDelegate:(id<ShunyaWalletProviderDelegate>)delegate
               isPrivateBrowsing:(bool)isPrivateBrowsing {
  auto* browserState = _mainBrowserState;
  if (isPrivateBrowsing) {
    browserState = browserState->GetOffTheRecordChromeBrowserState();
  }

  auto* json_rpc_service =
      shunya_wallet::JsonRpcServiceFactory::GetServiceForState(browserState);
  if (!json_rpc_service) {
    return nil;
  }

  auto* tx_service =
      shunya_wallet::TxServiceFactory::GetServiceForState(browserState);
  if (!tx_service) {
    return nil;
  }

  auto* keyring_service =
      shunya_wallet::KeyringServiceFactory::GetServiceForState(browserState);
  if (!keyring_service) {
    return nil;
  }

  auto* shunya_wallet_service =
      shunya_wallet::ShunyaWalletServiceFactory::GetServiceForState(browserState);
  if (!shunya_wallet_service) {
    return nil;
  }

  auto provider = std::make_unique<shunya_wallet::EthereumProviderImpl>(
      ios::HostContentSettingsMapFactory::GetForBrowserState(browserState),
      json_rpc_service, tx_service, keyring_service, shunya_wallet_service,
      std::make_unique<shunya_wallet::ShunyaWalletProviderDelegateBridge>(
          delegate),
      browserState->GetPrefs());
  return [[ShunyaWalletEthereumProviderOwnedImpl alloc]
      initWithEthereumProvider:std::move(provider)];
}

- (nullable id<ShunyaWalletSolanaProvider>)
    solanaProviderWithDelegate:(id<ShunyaWalletProviderDelegate>)delegate
             isPrivateBrowsing:(bool)isPrivateBrowsing {
  auto* browserState = _mainBrowserState;
  if (isPrivateBrowsing) {
    browserState = browserState->GetOffTheRecordChromeBrowserState();
  }

  auto* keyring_service =
      shunya_wallet::KeyringServiceFactory::GetServiceForState(browserState);
  if (!keyring_service) {
    return nil;
  }

  auto* shunya_wallet_service =
      shunya_wallet::ShunyaWalletServiceFactory::GetServiceForState(browserState);
  if (!shunya_wallet_service) {
    return nil;
  }

  auto* tx_service =
      shunya_wallet::TxServiceFactory::GetServiceForState(browserState);
  if (!tx_service) {
    return nil;
  }

  auto* json_rpc_service =
      shunya_wallet::JsonRpcServiceFactory::GetServiceForState(browserState);
  if (!json_rpc_service) {
    return nil;
  }
  auto* host_content_settings_map =
      ios::HostContentSettingsMapFactory::GetForBrowserState(browserState);
  if (!host_content_settings_map) {
    return nil;
  }

  auto provider = std::make_unique<shunya_wallet::SolanaProviderImpl>(
      *host_content_settings_map, keyring_service, shunya_wallet_service,
      tx_service, json_rpc_service,
      std::make_unique<shunya_wallet::ShunyaWalletProviderDelegateBridge>(
          delegate));
  return [[ShunyaWalletSolanaProviderOwnedImpl alloc]
      initWithSolanaProvider:std::move(provider)];
}

- (NSString*)resourceForID:(int)resource_id {
  // The resource bundle is not available until after WebMainParts is setup
  auto& resource_bundle = ui::ResourceBundle::GetSharedInstance();
  std::string resource_string = "";
  if (resource_bundle.IsGzipped(resource_id)) {
    resource_string =
        std::string(resource_bundle.LoadDataResourceString(resource_id));
  } else {
    resource_string =
        std::string(resource_bundle.GetRawDataResource(resource_id));
  }
  return base::SysUTF8ToNSString(resource_string);
}

- (NSDictionary<ShunyaWalletProviderScriptKey, NSString*>*)
    providerScriptsForCoinType:(ShunyaWalletCoinType)coinType {
  auto cachedScript = _providerScripts[@(coinType)];
  if (cachedScript) {
    return cachedScript;
  }
  auto resource_ids =
      ^std::vector<std::pair<ShunyaWalletProviderScriptKey, int>> {
    switch (coinType) {
      case ShunyaWalletCoinTypeEth:
        return {std::make_pair(
            ShunyaWalletProviderScriptKeyEthereum,
            IDR_SHUNYA_WALLET_SCRIPT_ETHEREUM_PROVIDER_SCRIPT_BUNDLE_JS)};
      case ShunyaWalletCoinTypeSol:
        return {std::make_pair(
                    ShunyaWalletProviderScriptKeySolana,
                    IDR_SHUNYA_WALLET_SCRIPT_SOLANA_PROVIDER_SCRIPT_BUNDLE_JS),
                std::make_pair(ShunyaWalletProviderScriptKeySolanaWeb3,
                               IDR_SHUNYA_WALLET_SOLANA_WEB3_JS),
                std::make_pair(ShunyaWalletProviderScriptKeyWalletStandard,
                               IDR_SHUNYA_WALLET_STANDARD_JS)};
      case ShunyaWalletCoinTypeFil:
        // Currently not supported
        return {std::make_pair(@"", 0)};
      case ShunyaWalletCoinTypeBtc:
        // Currently not supported
        return {std::make_pair(@"", 0)};
    }
    return {std::make_pair(@"", 0)};
  }
  ();
  const auto scripts = [[NSMutableDictionary alloc] init];
  for (auto resource : resource_ids) {
    auto key = resource.first;
    auto resource_id = resource.second;
    scripts[key] = [self resourceForID:resource_id];
  }
  _providerScripts[@(coinType)] = [scripts copy];
  return scripts;
}

@end

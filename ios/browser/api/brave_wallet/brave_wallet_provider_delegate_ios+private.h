/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IOS_PRIVATE_H_
#define SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IOS_PRIVATE_H_

#include <string>
#include <vector>

#include "shunya/components/shunya_wallet/browser/shunya_wallet_provider_delegate.h"

@protocol ShunyaWalletProviderDelegate;

namespace shunya_wallet {

class ShunyaWalletProviderDelegateBridge
    : public shunya_wallet::ShunyaWalletProviderDelegate {
 public:
  explicit ShunyaWalletProviderDelegateBridge(
      id<ShunyaWalletProviderDelegate> bridge)
      : bridge_(bridge) {}

 private:
  __weak id<ShunyaWalletProviderDelegate> bridge_;

  bool IsTabVisible() override;
  void ShowPanel() override;
  void WalletInteractionDetected() override;
  url::Origin GetOrigin() const override;
  void ShowWalletOnboarding() override;
  void ShowAccountCreation(mojom::CoinType type) override;
  void RequestPermissions(mojom::CoinType type,
                          const std::vector<std::string>& accounts,
                          RequestPermissionsCallback) override;
  bool IsAccountAllowed(mojom::CoinType type,
                        const std::string& account) override;
  absl::optional<std::vector<std::string>> GetAllowedAccounts(
      mojom::CoinType type,
      const std::vector<std::string>& accounts) override;
  bool IsPermissionDenied(mojom::CoinType type) override;
  void AddSolanaConnectedAccount(const std::string& account) override;
  void RemoveSolanaConnectedAccount(const std::string& account) override;
  bool IsSolanaAccountConnected(const std::string& account) override;
};

}  // namespace shunya_wallet

#endif  // SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IOS_PRIVATE_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_API_H_
#define SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_API_H_

#import <Foundation/Foundation.h>

@protocol ShunyaWalletBlockchainRegistry;
@protocol ShunyaWalletEthereumProvider;
@protocol ShunyaWalletProviderDelegate;
@protocol ShunyaWalletSolanaProvider;

typedef NS_ENUM(NSInteger, ShunyaWalletCoinType);

NS_ASSUME_NONNULL_BEGIN

typedef NSString* ShunyaWalletProviderScriptKey NS_STRING_ENUM;
OBJC_EXPORT ShunyaWalletProviderScriptKey const
    ShunyaWalletProviderScriptKeyEthereum;
OBJC_EXPORT ShunyaWalletProviderScriptKey const
    ShunyaWalletProviderScriptKeySolana;
OBJC_EXPORT ShunyaWalletProviderScriptKey const
    ShunyaWalletProviderScriptKeySolanaWeb3;
OBJC_EXPORT ShunyaWalletProviderScriptKey const
    ShunyaWalletProviderScriptKeyWalletStandard;

OBJC_EXPORT
@interface ShunyaWalletAPI : NSObject

@property(class, readonly) id<ShunyaWalletBlockchainRegistry> blockchainRegistry;

- (nullable id<ShunyaWalletEthereumProvider>)
    ethereumProviderWithDelegate:(id<ShunyaWalletProviderDelegate>)delegate
               isPrivateBrowsing:(bool)isPrivateBrowsing;

- (nullable id<ShunyaWalletSolanaProvider>)
    solanaProviderWithDelegate:(id<ShunyaWalletProviderDelegate>)delegate
             isPrivateBrowsing:(bool)isPrivateBrowsing;

- (NSDictionary<ShunyaWalletProviderScriptKey, NSString*>*)
    providerScriptsForCoinType:(ShunyaWalletCoinType)coinType;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_API_H_

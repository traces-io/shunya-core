/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IOS_H_
#define SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IOS_H_

#import <Foundation/Foundation.h>
#import "shunya_wallet.mojom.objc.h"

@class URLOriginIOS;

NS_ASSUME_NONNULL_BEGIN

typedef void (^RequestPermissionsCallback)(
    ShunyaWalletRequestPermissionsError error,
    NSArray<NSString*>* _Nullable allowedAccounts);

OBJC_EXPORT
@protocol ShunyaWalletProviderDelegate
- (bool)isTabVisible;
- (void)showPanel;
- (URLOriginIOS*)getOrigin;
- (void)walletInteractionDetected;
- (void)showWalletOnboarding;
- (void)showAccountCreation:(ShunyaWalletCoinType)type;
- (void)requestPermissions:(ShunyaWalletCoinType)type
                  accounts:(NSArray<NSString*>*)accounts
                completion:(RequestPermissionsCallback)completion;
- (bool)isAccountAllowed:(ShunyaWalletCoinType)type account:(NSString*)account;
- (nullable NSArray<NSString*>*)getAllowedAccounts:(ShunyaWalletCoinType)type
                                          accounts:
                                              (NSArray<NSString*>*)accounts;
- (bool)isPermissionDenied:(ShunyaWalletCoinType)type;
- (void)addSolanaConnectedAccount:(NSString*)account;
- (void)removeSolanaConnectedAccount:(NSString*)account;
- (bool)isSolanaAccountConnected:(NSString*)account;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_SHUNYA_WALLET_SHUNYA_WALLET_PROVIDER_DELEGATE_IOS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#import <Foundation/Foundation.h>

#ifndef SHUNYA_IOS_BROWSER_API_HISTORY_HISTORY_SERVICE_LISTENER_IOS_H_
#define SHUNYA_IOS_BROWSER_API_HISTORY_HISTORY_SERVICE_LISTENER_IOS_H_

#include "shunya/ios/browser/api/history/shunya_history_observer.h"
#include "components/history/core/browser/history_service_observer.h"

@interface HistoryServiceListenerImpl : NSObject <HistoryServiceListener>
- (instancetype)init:(id<HistoryServiceObserver>)observer
      historyService:(void*)service;
@end

namespace shunya {
namespace ios {
class HistoryServiceListenerIOS : public history::HistoryServiceObserver {
 public:
  explicit HistoryServiceListenerIOS(id<HistoryServiceObserver> observer,
                                     history::HistoryService* service);
  ~HistoryServiceListenerIOS() override;

 private:
  // HistoryServiceObserver implementation.
  void OnHistoryServiceLoaded(history::HistoryService* service) override;
  void HistoryServiceBeingDeleted(history::HistoryService* service) override;
  void OnURLVisited(history::HistoryService* history_service,
                    const history::URLRow& url_row,
                    const history::VisitRow& new_visit) override;
  void OnURLsModified(history::HistoryService* history_service,
                      const history::URLRows& changed_urls) override;
  void OnURLsDeleted(history::HistoryService* history_service,
                     const history::DeletionInfo& deletion_info) override;

  id<HistoryServiceObserver> observer_;
  history::HistoryService* service_;  // NOT OWNED
};

}  // namespace ios
}  // namespace shunya

#endif  // SHUNYA_IOS_BROWSER_API_HISTORY_HISTORY_SERVICE_LISTENER_IOS_H_

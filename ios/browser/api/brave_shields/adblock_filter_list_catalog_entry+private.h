/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SHUNYA_SHIELDS_ADBLOCK_FILTER_LIST_CATALOG_ENTRY_PRIVATE_H_
#define SHUNYA_IOS_BROWSER_API_SHUNYA_SHIELDS_ADBLOCK_FILTER_LIST_CATALOG_ENTRY_PRIVATE_H_

#import <Foundation/Foundation.h>
#include "shunya/ios/browser/api/shunya_shields/adblock_filter_list_catalog_entry.h"

namespace shunya_shields {
class FilterListCatalogEntry;
}  // namespace shunya_shields

NS_ASSUME_NONNULL_BEGIN

@interface AdblockFilterListCatalogEntry (Private)
- (instancetype)initWithFilterListCatalogEntry:
    (shunya_shields::FilterListCatalogEntry)entry;
- (shunya_shields::FilterListCatalogEntry)entry;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_SHUNYA_SHIELDS_ADBLOCK_FILTER_LIST_CATALOG_ENTRY_PRIVATE_H_

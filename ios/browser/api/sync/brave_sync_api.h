/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SYNC_SHUNYA_SYNC_API_H_
#define SHUNYA_IOS_BROWSER_API_SYNC_SHUNYA_SYNC_API_H_

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ShunyaSyncInternalsController;

typedef NSInteger ShunyaSyncAPISyncProtocolErrorResult
    NS_TYPED_ENUM NS_SWIFT_NAME(ShunyaSyncAPI.SyncProtocolErrorResult);

OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultSuccess;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultNotMyBirthday;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultThrottled;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultClearPending;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultTransientError;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultMigrationDone;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultDisabledByAdmin;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultPartialFailure;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultDataObsolete;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultEncryptionObsolete;
OBJC_EXPORT ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultUnknown;

typedef NSInteger ShunyaSyncAPIQrCodeDataValidationResult NS_TYPED_ENUM
    NS_SWIFT_NAME(ShunyaSyncAPI.QrCodeDataValidationResult);

OBJC_EXPORT ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultValid;
OBJC_EXPORT ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultNotWellFormed;
OBJC_EXPORT ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultVersionDeprecated;
OBJC_EXPORT ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultExpired;
OBJC_EXPORT ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultValidForTooLong;

typedef NSInteger ShunyaSyncAPIWordsValidationStatus NS_TYPED_ENUM
    NS_SWIFT_NAME(ShunyaSyncAPI.WordsValidationStatus);

OBJC_EXPORT ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusValid;
OBJC_EXPORT ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusNotValidPureWords;
OBJC_EXPORT ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusVersionDeprecated;
OBJC_EXPORT ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusExpired;
OBJC_EXPORT ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusValidForTooLong;
OBJC_EXPORT ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusWrongWordsNumber;

OBJC_EXPORT
@interface ShunyaSyncAPI : NSObject

@property(nonatomic, readonly) bool canSyncFeatureStart;
@property(nonatomic, readonly) bool isSyncFeatureActive;
@property(nonatomic, readonly) bool isInitialSyncFeatureSetupComplete;
@property(nonatomic) bool isSyncAccountDeletedNoticePending;
@property(nonatomic, readonly) bool isFailedDecryptSeedNoticeDismissed;

- (instancetype)init NS_UNAVAILABLE;

- (void)requestSync;

- (void)setSetupComplete;

- (void)resetSync;

- (void)setDidJoinSyncChain:(void (^)(bool))completion;

- (void)permanentlyDeleteAccount:
    (void (^)(ShunyaSyncAPISyncProtocolErrorResult))completion;

- (void)deleteDevice:(NSString*)guid;

- (bool)isValidSyncCode:(NSString*)syncCode;

- (NSString*)getSyncCode;

// returns false is sync is already configured or if the sync code is invalid
- (bool)setSyncCode:(NSString*)syncCode;

- (NSString*)syncCodeFromHexSeed:(NSString*)hexSeed;

- (NSString*)hexSeedFromSyncCode:(NSString*)syncCode;

- (NSString*)qrCodeJsonFromHexSeed:(NSString*)hexSeed;

- (void)dismissFailedDecryptSeedNotice;

- (ShunyaSyncAPIQrCodeDataValidationResult)getQRCodeValidationResult:
    (NSString*)json;

- (ShunyaSyncAPIWordsValidationStatus)getWordsValidationResult:
    (NSString*)timeLimitedWords;

- (NSString*)getWordsFromTimeLimitedWords:(NSString*)timeLimitedWords;

- (NSString*)getTimeLimitedWordsFromWords:(NSString*)words;

- (NSString*)getHexSeedFromQrCodeJson:(NSString*)json;

- (nullable UIImage*)getQRCodeImage:(CGSize)size;

- (nullable NSString*)getDeviceListJSON;

- (ShunyaSyncInternalsController*)createSyncInternalsController;

- (id)createSyncDeviceObserver:(void (^)())onDeviceInfoChanged;
- (id)createSyncServiceObserver:(void (^)())onSyncServiceStateChanged
          onSyncServiceShutdown:(void (^)())onSyncServiceShutdown;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_SYNC_SHUNYA_SYNC_API_H_

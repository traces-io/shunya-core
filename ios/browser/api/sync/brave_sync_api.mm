/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#import "shunya/ios/browser/api/sync/shunya_sync_api.h"

#import <CoreImage/CoreImage.h>
#include <string>
#include <vector>

#include "base/compiler_specific.h"
#include "base/functional/bind.h"
#include "base/json/json_writer.h"
#include "base/memory/weak_ptr.h"
#include "base/strings/string_number_conversions.h"
#include "base/strings/sys_string_conversions.h"
#include "shunya/components/shunya_sync/shunya_sync_prefs.h"
#include "shunya/components/shunya_sync/crypto/crypto.h"
#include "shunya/components/shunya_sync/qr_code_validator.h"
#include "shunya/components/shunya_sync/time_limited_words.h"
#include "shunya/components/sync_device_info/shunya_device_info.h"
#include "shunya/ios/browser/api/sync/shunya_sync_internals+private.h"
#include "shunya/ios/browser/api/sync/shunya_sync_worker.h"

#include "components/sync/protocol/sync_protocol_error.h"
#include "components/sync/service/sync_service.h"
#include "components/sync/service/sync_service_impl.h"
#include "components/sync/service/sync_service_observer.h"
#include "components/sync_device_info/device_info.h"
#include "components/sync_device_info/device_info_sync_service.h"
#include "components/sync_device_info/device_info_tracker.h"
#include "components/sync_device_info/local_device_info_provider.h"
#include "ios/chrome/browser/shared/model/application_context/application_context.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state_manager.h"
#include "ios/chrome/browser/sync/device_info_sync_service_factory.h"
#include "ios/chrome/browser/sync/sync_service_factory.h"
#include "ios/chrome/browser/sync/sync_setup_service.h"
#include "ios/chrome/browser/sync/sync_setup_service_factory.h"
#include "ios/web/public/thread/web_task_traits.h"
#include "ios/web/public/thread/web_thread.h"

#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

// MARK: - ShunyaSyncAPISyncProtocolErrorResult

ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultSuccess =
        static_cast<NSInteger>(syncer::SyncProtocolErrorType::SYNC_SUCCESS);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultNotMyBirthday =
        static_cast<NSInteger>(syncer::SyncProtocolErrorType::NOT_MY_BIRTHDAY);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultThrottled =
        static_cast<NSInteger>(syncer::SyncProtocolErrorType::THROTTLED);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultClearPending =
        static_cast<NSInteger>(syncer::SyncProtocolErrorType::CLEAR_PENDING);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultTransientError =
        static_cast<NSInteger>(syncer::SyncProtocolErrorType::TRANSIENT_ERROR);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultMigrationDone =
        static_cast<NSInteger>(syncer::SyncProtocolErrorType::MIGRATION_DONE);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultDisabledByAdmin = static_cast<NSInteger>(
        syncer::SyncProtocolErrorType::DISABLED_BY_ADMIN);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultPartialFailure =
        static_cast<NSInteger>(syncer::SyncProtocolErrorType::PARTIAL_FAILURE);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultDataObsolete = static_cast<NSInteger>(
        syncer::SyncProtocolErrorType::CLIENT_DATA_OBSOLETE);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultEncryptionObsolete =
        static_cast<NSInteger>(
            syncer::SyncProtocolErrorType::ENCRYPTION_OBSOLETE);
ShunyaSyncAPISyncProtocolErrorResult const
    ShunyaSyncAPISyncProtocolErrorResultUnknown =
        static_cast<NSInteger>(syncer::SyncProtocolErrorType::UNKNOWN_ERROR);

// MARK: - QrCodeDataValidationResult

ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultValid =
        static_cast<NSInteger>(shunya_sync::QrCodeDataValidationResult::kValid);
ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultNotWellFormed =
        static_cast<NSInteger>(
            shunya_sync::QrCodeDataValidationResult::kNotWellFormed);
ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultVersionDeprecated =
        static_cast<NSInteger>(
            shunya_sync::QrCodeDataValidationResult::kVersionDeprecated);
ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultExpired = static_cast<NSInteger>(
        shunya_sync::QrCodeDataValidationResult::kExpired);
ShunyaSyncAPIQrCodeDataValidationResult const
    ShunyaSyncAPIQrCodeDataValidationResultValidForTooLong =
        static_cast<NSInteger>(
            shunya_sync::QrCodeDataValidationResult::kValidForTooLong);

// MARK: - TimeLimitedWords::ValidationStatus

ShunyaSyncAPIWordsValidationStatus const ShunyaSyncAPIWordsValidationStatusValid =
    static_cast<NSInteger>(
        shunya_sync::TimeLimitedWords::ValidationStatus::kValid);
ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusNotValidPureWords = static_cast<NSInteger>(
        shunya_sync::TimeLimitedWords::ValidationStatus::kNotValidPureWords);
ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusVersionDeprecated = static_cast<NSInteger>(
        shunya_sync::TimeLimitedWords::ValidationStatus::kVersionDeprecated);
ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusExpired = static_cast<NSInteger>(
        shunya_sync::TimeLimitedWords::ValidationStatus::kExpired);
ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusValidForTooLong = static_cast<NSInteger>(
        shunya_sync::TimeLimitedWords::ValidationStatus::kValidForTooLong);
ShunyaSyncAPIWordsValidationStatus const
    ShunyaSyncAPIWordsValidationStatusWrongWordsNumber = static_cast<NSInteger>(
        shunya_sync::TimeLimitedWords::ValidationStatus::kWrongWordsNumber);

// MARK: - ShunyaSyncDeviceObserver

@interface ShunyaSyncDeviceObserver : NSObject {
  std::unique_ptr<ShunyaSyncDeviceTracker> _device_observer;
}
@end

@implementation ShunyaSyncDeviceObserver

- (instancetype)initWithDeviceInfoTracker:(syncer::DeviceInfoTracker*)tracker
                                 callback:(void (^)())onDeviceInfoChanged {
  if ((self = [super init])) {
    _device_observer = std::make_unique<ShunyaSyncDeviceTracker>(
        tracker, base::BindRepeating(onDeviceInfoChanged));
  }
  return self;
}
@end

// MARK: - ShunyaSyncServiceObserver

@interface ShunyaSyncServiceObserver : NSObject {
  std::unique_ptr<ShunyaSyncServiceTracker> _service_tracker;
}
@end

@implementation ShunyaSyncServiceObserver

- (instancetype)initWithSyncServiceImpl:
                    (syncer::SyncServiceImpl*)syncServiceImpl
                   stateChangedCallback:(void (^)())onSyncServiceStateChanged
                   syncShutdownCallback:(void (^)())onSyncServiceShutdown {
  if ((self = [super init])) {
    _service_tracker = std::make_unique<ShunyaSyncServiceTracker>(
        syncServiceImpl, base::BindRepeating(onSyncServiceStateChanged),
        base::BindRepeating(onSyncServiceShutdown));
  }
  return self;
}
@end

// MARK: - ShunyaSyncAPI

@interface ShunyaSyncAPI () {
  std::unique_ptr<ShunyaSyncWorker> _worker;
  ChromeBrowserState* _chromeBrowserState;
}
@end

@implementation ShunyaSyncAPI

- (instancetype)initWithBrowserState:(ChromeBrowserState*)mainBrowserState {
  if ((self = [super init])) {
    _chromeBrowserState = mainBrowserState;
    _worker.reset(new ShunyaSyncWorker(_chromeBrowserState));
  }
  return self;
}

- (void)dealloc {
  _worker.reset();
  _chromeBrowserState = NULL;
}

- (bool)canSyncFeatureStart {
  return _worker->CanSyncFeatureStart();
}

- (bool)isSyncFeatureActive {
  return _worker->IsSyncFeatureActive();
}

- (bool)isInitialSyncFeatureSetupComplete {
  return _worker->IsInitialSyncFeatureSetupComplete();
}

- (void)setSetupComplete {
  _worker->SetSetupComplete();
}

- (void)requestSync {
  _worker->RequestSync();
}

- (bool)isValidSyncCode:(NSString*)syncCode {
  return _worker->IsValidSyncCode(base::SysNSStringToUTF8(syncCode));
}

- (NSString*)getSyncCode {
  std::string syncCode = _worker->GetOrCreateSyncCode();
  if (syncCode.empty()) {
    return nil;
  }

  return base::SysUTF8ToNSString(syncCode);
}

- (bool)setSyncCode:(NSString*)syncCode {
  return _worker->SetSyncCode(base::SysNSStringToUTF8(syncCode));
}

- (NSString*)syncCodeFromHexSeed:(NSString*)hexSeed {
  return base::SysUTF8ToNSString(
      _worker->GetSyncCodeFromHexSeed(base::SysNSStringToUTF8(hexSeed)));
}

- (NSString*)hexSeedFromSyncCode:(NSString*)syncCode {
  return base::SysUTF8ToNSString(
      _worker->GetHexSeedFromSyncCode(base::SysNSStringToUTF8(syncCode)));
}

- (NSString*)qrCodeJsonFromHexSeed:(NSString*)hexSeed {
  return base::SysUTF8ToNSString(
      _worker->GetQrCodeJsonFromHexSeed(base::SysNSStringToUTF8(hexSeed)));
}

- (ShunyaSyncAPIQrCodeDataValidationResult)getQRCodeValidationResult:
    (NSString*)json {
  return static_cast<ShunyaSyncAPIQrCodeDataValidationResult>(
      _worker->GetQrCodeValidationResult(base::SysNSStringToUTF8(json)));
}

- (ShunyaSyncAPIWordsValidationStatus)getWordsValidationResult:
    (NSString*)timeLimitedWords {
  return static_cast<ShunyaSyncAPIWordsValidationStatus>(
      _worker->GetWordsValidationResult(
          base::SysNSStringToUTF8(timeLimitedWords)));
}

- (NSString*)getWordsFromTimeLimitedWords:(NSString*)timeLimitedWords {
  return base::SysUTF8ToNSString(_worker->GetWordsFromTimeLimitedWords(
      base::SysNSStringToUTF8(timeLimitedWords)));
}

- (NSString*)getTimeLimitedWordsFromWords:(NSString*)words {
  return base::SysUTF8ToNSString(
      _worker->GetTimeLimitedWordsFromWords(base::SysNSStringToUTF8(words)));
}

- (NSString*)getHexSeedFromQrCodeJson:(NSString*)json {
  return base::SysUTF8ToNSString(
      _worker->GetHexSeedFromQrCodeJson(base::SysNSStringToUTF8(json)));
}

- (UIImage*)getQRCodeImage:(CGSize)size {
  std::vector<uint8_t> seed;
  std::string sync_code = _worker->GetOrCreateSyncCode();
  if (!shunya_sync::crypto::PassphraseToBytes32(sync_code, &seed)) {
    return nil;
  }

  // QR code version 3 can only carry 84 bytes so we hex encode 32 bytes
  // seed then we will have 64 bytes input data
  const std::string sync_code_hex = base::HexEncode(seed.data(), seed.size());

  NSData* sync_code_data = [base::SysUTF8ToNSString(sync_code_hex.c_str())
      dataUsingEncoding:NSUTF8StringEncoding];  // NSISOLatin1StringEncoding

  if (!sync_code_data) {
    return nil;
  }

  CIFilter* filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
  [filter setValue:sync_code_data forKey:@"inputMessage"];
  [filter setValue:@"H" forKey:@"inputCorrectionLevel"];

  CIImage* ciImage = [filter outputImage];
  if (ciImage) {
    CGFloat scaleX = size.width / ciImage.extent.size.width;
    CGFloat scaleY = size.height / ciImage.extent.size.height;
    CGAffineTransform transform = CGAffineTransformMakeScale(scaleX, scaleY);
    ciImage = [ciImage imageByApplyingTransform:transform];

    return [UIImage imageWithCIImage:ciImage
                               scale:[[UIScreen mainScreen] scale]
                         orientation:UIImageOrientationUp];
  }
  return nil;
}

- (NSString*)getDeviceListJSON {
  auto device_list = _worker->GetDeviceList();
  auto* local_device_info = _worker->GetLocalDeviceInfo();

  base::Value::List device_list_value;

  for (const auto& device : device_list) {
    auto device_value = device->ToValue();
    bool is_current_device =
        local_device_info ? local_device_info->guid() == device->guid() : false;
    device_value.Set("isCurrentDevice", is_current_device);
    device_value.Set("guid", device->guid());
    device_value.Set("supportsSelfDelete", device->is_self_delete_supported());
    device_list_value.Append(base::Value(std::move(device_value)));
  }

  std::string json_string;
  if (!base::JSONWriter::Write(device_list_value, &json_string)) {
    return nil;
  }

  return base::SysUTF8ToNSString(json_string);
}

- (void)resetSync {
  _worker->ResetSync();
}

- (void)setDidJoinSyncChain:(void (^)(bool success))completion {
  _worker->SetJoinSyncChainCallback(base::BindOnce(completion));
}

- (void)permanentlyDeleteAccount:
    (void (^)(ShunyaSyncAPISyncProtocolErrorResult))completion {
  _worker->PermanentlyDeleteAccount(base::BindOnce(
      [](void (^completion)(ShunyaSyncAPISyncProtocolErrorResult),
         const syncer::SyncProtocolError& error) {
        completion(
            static_cast<ShunyaSyncAPISyncProtocolErrorResult>(error.error_type));
      },
      completion));
}

- (bool)isSyncAccountDeletedNoticePending {
  shunya_sync::Prefs shunya_sync_prefs(_chromeBrowserState->GetPrefs());
  return shunya_sync_prefs.IsSyncAccountDeletedNoticePending();
}

- (void)setIsSyncAccountDeletedNoticePending:
    (bool)isSyncAccountDeletedNoticePending {
  shunya_sync::Prefs shunya_sync_prefs(_chromeBrowserState->GetPrefs());
  shunya_sync_prefs.SetSyncAccountDeletedNoticePending(false);
}

- (bool)isFailedDecryptSeedNoticeDismissed {
  shunya_sync::Prefs shunya_sync_prefs(_chromeBrowserState->GetPrefs());
  return shunya_sync_prefs.IsFailedDecryptSeedNoticeDismissed();
}

- (void)dismissFailedDecryptSeedNotice {
  shunya_sync::Prefs shunya_sync_prefs(_chromeBrowserState->GetPrefs());
  shunya_sync_prefs.DismissFailedDecryptSeedNotice();
}

- (void)deleteDevice:(NSString*)guid {
  _worker->DeleteDevice(base::SysNSStringToUTF8(guid));
}

- (ShunyaSyncInternalsController*)createSyncInternalsController {
  return [[ShunyaSyncInternalsController alloc]
      initWithBrowserState:_chromeBrowserState];
}

- (id)createSyncDeviceObserver:(void (^)())onDeviceInfoChanged {
  auto* tracker =
      DeviceInfoSyncServiceFactory::GetForBrowserState(_chromeBrowserState)
          ->GetDeviceInfoTracker();
  return [[ShunyaSyncDeviceObserver alloc]
      initWithDeviceInfoTracker:tracker
                       callback:onDeviceInfoChanged];
}

- (id)createSyncServiceObserver:(void (^)())onSyncServiceStateChanged
          onSyncServiceShutdown:(void (^)())onSyncServiceShutdown {
  auto* service = static_cast<syncer::SyncServiceImpl*>(
      SyncServiceFactory::GetForBrowserState(_chromeBrowserState));
  return [[ShunyaSyncServiceObserver alloc]
      initWithSyncServiceImpl:service
         stateChangedCallback:onSyncServiceStateChanged
         syncShutdownCallback:onSyncServiceShutdown];
}
@end

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SYNC_SHUNYA_SYNC_WORKER_H_
#define SHUNYA_IOS_BROWSER_API_SYNC_SHUNYA_SYNC_WORKER_H_

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "base/scoped_multi_source_observation.h"
#include "base/scoped_observation.h"
#include "shunya/components/shunya_sync/time_limited_words.h"
#include "components/sync/protocol/sync_protocol_error.h"
#include "components/sync/service/sync_service.h"
#include "components/sync/service/sync_service_observer.h"
#include "components/sync_device_info/device_info_sync_service.h"
#include "components/sync_device_info/device_info_tracker.h"

class ChromeBrowserState;
struct SyncProtocolError;

namespace syncer {
class ShunyaSyncServiceImpl;
class DeviceInfo;
class ShunyaDeviceInfo;
class SyncServiceImpl;
}  // namespace syncer

namespace shunya_sync {
enum class QrCodeDataValidationResult;
}  // namespace shunya_sync

class ShunyaSyncDeviceTracker : public syncer::DeviceInfoTracker::Observer {
 public:
  ShunyaSyncDeviceTracker(
      syncer::DeviceInfoTracker* device_info_tracker,
      const base::RepeatingCallback<void()>& on_device_info_changed_callback);
  ~ShunyaSyncDeviceTracker() override;

 private:
  void OnDeviceInfoChange() override;

  base::RepeatingCallback<void()> on_device_info_changed_callback_;

  base::ScopedObservation<syncer::DeviceInfoTracker,
                          syncer::DeviceInfoTracker::Observer>
      device_info_tracker_observer_{this};
};

class ShunyaSyncServiceTracker : public syncer::SyncServiceObserver {
 public:
  ShunyaSyncServiceTracker(
      syncer::SyncServiceImpl* sync_service_impl,
      const base::RepeatingCallback<void()>& on_state_changed_callback,
      const base::RepeatingCallback<void()>& on_sync_shutdown_callback);
  ~ShunyaSyncServiceTracker() override;

 private:
  void OnStateChanged(syncer::SyncService* sync) override;
  void OnSyncShutdown(syncer::SyncService* sync) override;

  base::RepeatingCallback<void()> on_state_changed_callback_;
  base::RepeatingCallback<void()> on_sync_shutdown_callback_;

  base::ScopedObservation<syncer::SyncService, syncer::SyncServiceObserver>
      sync_service_observer_{this};
};

class ShunyaSyncWorker : public syncer::SyncServiceObserver {
 public:
  explicit ShunyaSyncWorker(ChromeBrowserState* browser_state_);
  ShunyaSyncWorker(const ShunyaSyncWorker&) = delete;
  ShunyaSyncWorker& operator=(const ShunyaSyncWorker&) = delete;
  ~ShunyaSyncWorker() override;

  bool RequestSync();
  std::string GetOrCreateSyncCode();
  bool IsValidSyncCode(const std::string& sync_code);
  bool SetSyncCode(const std::string& sync_code);
  std::string GetSyncCodeFromHexSeed(const std::string& hex_seed);
  std::string GetHexSeedFromSyncCode(const std::string& code_words);
  std::string GetQrCodeJsonFromHexSeed(const std::string& hex_seed);
  shunya_sync::QrCodeDataValidationResult GetQrCodeValidationResult(
      const std::string json);
  shunya_sync::TimeLimitedWords::ValidationStatus GetWordsValidationResult(
      const std::string time_limited_words);
  std::string GetWordsFromTimeLimitedWords(
      const std::string& time_limited_words);
  std::string GetTimeLimitedWordsFromWords(const std::string& words);
  std::string GetHexSeedFromQrCodeJson(const std::string& json);
  const syncer::DeviceInfo* GetLocalDeviceInfo();
  std::vector<std::unique_ptr<syncer::ShunyaDeviceInfo>> GetDeviceList();
  bool CanSyncFeatureStart();
  bool IsSyncFeatureActive();
  bool IsInitialSyncFeatureSetupComplete();
  bool SetSetupComplete();
  void ResetSync();
  void DeleteDevice(const std::string& device_guid);
  void SetJoinSyncChainCallback(base::OnceCallback<void(bool)> callback);
  void PermanentlyDeleteAccount(
      base::OnceCallback<void(const syncer::SyncProtocolError&)> callback);

 private:
  // syncer::SyncServiceObserver implementation.

  syncer::ShunyaSyncServiceImpl* GetSyncService() const;
  void OnStateChanged(syncer::SyncService* service) override;
  void OnSyncShutdown(syncer::SyncService* service) override;

  void OnResetDone();

  void SetEncryptionPassphrase(syncer::SyncService* service);
  void SetDecryptionPassphrase(syncer::SyncService* service);

  std::string passphrase_;

  ChromeBrowserState* browser_state_;  // NOT OWNED
  base::ScopedMultiSourceObservation<syncer::SyncService,
                                     syncer::SyncServiceObserver>
      sync_service_observer_{this};
  base::WeakPtrFactory<ShunyaSyncWorker> weak_ptr_factory_{this};
};

#endif  // SHUNYA_IOS_BROWSER_API_SYNC_SHUNYA_SYNC_WORKER_H_

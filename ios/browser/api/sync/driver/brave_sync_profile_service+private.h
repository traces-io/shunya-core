/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SYNC_DRIVER_SHUNYA_SYNC_PROFILE_SERVICE_PRIVATE_H_
#define SHUNYA_IOS_BROWSER_API_SYNC_DRIVER_SHUNYA_SYNC_PROFILE_SERVICE_PRIVATE_H_

#import <Foundation/Foundation.h>

#import "shunya/ios/browser/api/sync/driver/shunya_sync_profile_service.h"

NS_ASSUME_NONNULL_BEGIN

namespace syncer {
class SyncService;
}

@interface ShunyaSyncProfileServiceIOS (Private)
- (instancetype)initWithProfileSyncService:(syncer::SyncService*)syncService;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_SYNC_DRIVER_SHUNYA_SYNC_PROFILE_SERVICE_PRIVATE_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_IOS_BROWSER_API_SYNC_DRIVER_SHUNYA_SYNC_PROFILE_SERVICE_H_
#define SHUNYA_IOS_BROWSER_API_SYNC_DRIVER_SHUNYA_SYNC_PROFILE_SERVICE_H_

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, ShunyaSyncUserSelectableTypes) {
  ShunyaSyncUserSelectableTypes_NONE = 1ULL << 0,
  ShunyaSyncUserSelectableTypes_BOOKMARKS = 1ULL << 1,
  ShunyaSyncUserSelectableTypes_PREFERENCES = 1ULL << 2,
  ShunyaSyncUserSelectableTypes_PASSWORDS = 1ULL << 3,
  ShunyaSyncUserSelectableTypes_AUTOFILL = 1ULL << 4,
  ShunyaSyncUserSelectableTypes_THEMES = 1ULL << 5,
  ShunyaSyncUserSelectableTypes_HISTORY = 1ULL << 6,
  ShunyaSyncUserSelectableTypes_EXTENSIONS = 1ULL << 7,
  ShunyaSyncUserSelectableTypes_APPS = 1ULL << 8,
  ShunyaSyncUserSelectableTypes_READING_LIST = 1ULL << 9,
  ShunyaSyncUserSelectableTypes_TABS = 1ULL << 10,
};

OBJC_EXPORT
@interface ShunyaSyncProfileServiceIOS : NSObject

- (instancetype)init NS_UNAVAILABLE;

// Whether all conditions are satisfied for Sync to start
// Does not imply that Sync is actually running
@property(nonatomic, readonly) bool isSyncFeatureActive;

/// Selectable Types for the Sync User
/// Used for opting in/out on iOS side
@property(nonatomic, assign) ShunyaSyncUserSelectableTypes userSelectedTypes;
@end

NS_ASSUME_NONNULL_END

#endif  // SHUNYA_IOS_BROWSER_API_SYNC_DRIVER_SHUNYA_SYNC_PROFILE_SERVICE_H_

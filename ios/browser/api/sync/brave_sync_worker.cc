/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/ios/browser/api/sync/shunya_sync_worker.h"

#include <string>
#include <utility>
#include <vector>

#include "base/functional/bind.h"
#include "base/json/json_writer.h"
#include "base/memory/weak_ptr.h"
#include "base/strings/string_number_conversions.h"
#include "base/strings/sys_string_conversions.h"
#include "shunya/components/shunya_sync/shunya_sync_prefs.h"
#include "shunya/components/shunya_sync/crypto/crypto.h"
#include "shunya/components/shunya_sync/qr_code_data.h"
#include "shunya/components/shunya_sync/qr_code_validator.h"
#include "shunya/components/shunya_sync/sync_service_impl_helper.h"
#include "shunya/components/shunya_sync/time_limited_words.h"
#include "shunya/components/sync/service/shunya_sync_service_impl.h"
#include "shunya/components/sync_device_info/shunya_device_info.h"
#include "components/sync/protocol/sync_protocol_error.h"
#include "components/sync/service/sync_service.h"
#include "components/sync/service/sync_service_impl.h"
#include "components/sync/service/sync_service_observer.h"
#include "components/sync_device_info/device_info.h"
#include "components/sync_device_info/device_info_sync_service.h"
#include "components/sync_device_info/device_info_tracker.h"
#include "components/sync_device_info/local_device_info_provider.h"
#include "ios/chrome/browser/shared/model/browser_state/chrome_browser_state.h"
#include "ios/chrome/browser/sync/device_info_sync_service_factory.h"
#include "ios/chrome/browser/sync/sync_service_factory.h"
#include "ios/chrome/browser/sync/sync_setup_service.h"
#include "ios/chrome/browser/sync/sync_setup_service_factory.h"
#include "ios/web/public/thread/web_thread.h"

namespace {
static const size_t SEED_BYTES_COUNT = 32u;
}  // namespace

ShunyaSyncDeviceTracker::ShunyaSyncDeviceTracker(
    syncer::DeviceInfoTracker* device_info_tracker,
    const base::RepeatingCallback<void()>& on_device_info_changed_callback)
    : on_device_info_changed_callback_(on_device_info_changed_callback) {
  DCHECK(device_info_tracker);
  device_info_tracker_observer_.Observe(device_info_tracker);
}

ShunyaSyncDeviceTracker::~ShunyaSyncDeviceTracker() {
  // Observer will be removed by ScopedObservation
}

void ShunyaSyncDeviceTracker::OnDeviceInfoChange() {
  if (on_device_info_changed_callback_) {
    on_device_info_changed_callback_.Run();
  }
}

ShunyaSyncServiceTracker::ShunyaSyncServiceTracker(
    syncer::SyncServiceImpl* sync_service_impl,
    const base::RepeatingCallback<void()>& on_state_changed_callback,
    const base::RepeatingCallback<void()>& on_sync_shutdown_callback)
    : on_state_changed_callback_(on_state_changed_callback),
      on_sync_shutdown_callback_(on_sync_shutdown_callback) {
  DCHECK(sync_service_impl);
  sync_service_observer_.Observe(sync_service_impl);
}

ShunyaSyncServiceTracker::~ShunyaSyncServiceTracker() {
  // Observer will be removed by ScopedObservation
}

void ShunyaSyncServiceTracker::OnStateChanged(syncer::SyncService* sync) {
  if (on_state_changed_callback_) {
    on_state_changed_callback_.Run();
  }
}

void ShunyaSyncServiceTracker::OnSyncShutdown(syncer::SyncService* sync) {
  if (on_sync_shutdown_callback_) {
    on_sync_shutdown_callback_.Run();
  }
}

ShunyaSyncWorker::ShunyaSyncWorker(ChromeBrowserState* browser_state)
    : browser_state_(browser_state) {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
}

ShunyaSyncWorker::~ShunyaSyncWorker() {
  // Observer will be removed by ScopedObservation
}

bool ShunyaSyncWorker::RequestSync() {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  syncer::SyncService* sync_service = GetSyncService();

  if (!sync_service) {
    return false;
  }

  if (!sync_service_observer_.IsObservingSource(sync_service)) {
    sync_service_observer_.AddObservation(sync_service);
  }

  sync_service->SetSyncFeatureRequested();

  return true;
}

const syncer::DeviceInfo* ShunyaSyncWorker::GetLocalDeviceInfo() {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  auto* device_info_service =
      DeviceInfoSyncServiceFactory::GetForBrowserState(browser_state_);

  if (!device_info_service) {
    return nullptr;
  }

  return device_info_service->GetLocalDeviceInfoProvider()
      ->GetLocalDeviceInfo();
}

std::vector<std::unique_ptr<syncer::ShunyaDeviceInfo>>
ShunyaSyncWorker::GetDeviceList() {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  auto* device_info_service =
      DeviceInfoSyncServiceFactory::GetForBrowserState(browser_state_);

  if (!device_info_service) {
    return std::vector<std::unique_ptr<syncer::ShunyaDeviceInfo>>();
  }

  syncer::DeviceInfoTracker* tracker =
      device_info_service->GetDeviceInfoTracker();
  return tracker->GetAllShunyaDeviceInfo();
}

std::string ShunyaSyncWorker::GetOrCreateSyncCode() {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  syncer::ShunyaSyncServiceImpl* sync_service = GetSyncService();
  std::string sync_code;
  if (sync_service) {
    sync_code = sync_service->GetOrCreateSyncCode();
  }

  CHECK(shunya_sync::crypto::IsPassphraseValid(sync_code));
  return sync_code;
}

bool ShunyaSyncWorker::IsValidSyncCode(const std::string& sync_code) {
  std::vector<uint8_t> seed;
  if (!shunya_sync::crypto::PassphraseToBytes32(sync_code, &seed)) {
    return false;
  }
  return seed.size() == SEED_BYTES_COUNT;
}

bool ShunyaSyncWorker::SetSyncCode(const std::string& sync_code) {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  if (sync_code.empty()) {
    return false;
  }

  syncer::ShunyaSyncServiceImpl* sync_service = GetSyncService();
  if (!sync_service || !sync_service->SetSyncCode(sync_code)) {
    const std::string error_msg = sync_service
                                      ? "invalid sync code:" + sync_code
                                      : "sync service is not available";
    LOG(ERROR) << error_msg;
    return false;
  }

  passphrase_ = sync_code;
  return true;
}

std::string ShunyaSyncWorker::GetSyncCodeFromHexSeed(
    const std::string& hex_code_seed) {
  DCHECK(!hex_code_seed.empty());

  std::vector<uint8_t> bytes;
  std::string sync_code_words;
  if (base::HexStringToBytes(hex_code_seed, &bytes)) {
    DCHECK_EQ(bytes.size(), SEED_BYTES_COUNT);
    if (bytes.size() == SEED_BYTES_COUNT) {
      sync_code_words = shunya_sync::crypto::PassphraseFromBytes32(bytes);
      if (sync_code_words.empty()) {
        VLOG(1) << __func__ << " PassphraseFromBytes32 failed for "
                << hex_code_seed;
      }
    } else {
      LOG(ERROR) << "wrong seed bytes " << bytes.size();
    }

    DCHECK_NE(sync_code_words, "");
  } else {
    VLOG(1) << __func__ << " HexStringToBytes failed for hex_code_seed";
  }
  return sync_code_words;
}

std::string ShunyaSyncWorker::GetHexSeedFromSyncCode(
    const std::string& code_words) {
  DCHECK(!code_words.empty());

  std::string sync_code_hex;
  std::vector<uint8_t> bytes;
  if (shunya_sync::crypto::PassphraseToBytes32(code_words, &bytes)) {
    DCHECK_EQ(bytes.size(), SEED_BYTES_COUNT);
    if (bytes.size() == SEED_BYTES_COUNT) {
      sync_code_hex = base::HexEncode(&bytes.at(0), bytes.size());
    } else {
      LOG(ERROR) << "wrong seed bytes " << bytes.size();
    }
  } else {
    VLOG(1) << __func__ << " PassphraseToBytes32 failed for " << code_words;
  }
  return sync_code_hex;
}

std::string ShunyaSyncWorker::GetQrCodeJsonFromHexSeed(
    const std::string& hex_seed) {
  DCHECK(!hex_seed.empty());
  return shunya_sync::QrCodeData::CreateWithActualDate(hex_seed)->ToJson();
}

shunya_sync::QrCodeDataValidationResult
ShunyaSyncWorker::GetQrCodeValidationResult(const std::string json) {
  DCHECK(!json.empty());
  return shunya_sync::QrCodeDataValidator::ValidateQrDataJson(json);
}

shunya_sync::TimeLimitedWords::ValidationStatus
ShunyaSyncWorker::GetWordsValidationResult(
    const std::string time_limited_words) {
  DCHECK(!time_limited_words.empty());
  auto words_with_status =
      shunya_sync::TimeLimitedWords::Parse(time_limited_words);
  if (words_with_status.has_value()) {
    return shunya_sync::TimeLimitedWords::ValidationStatus::kValid;
  } else {
    return words_with_status.error();
  }
}

std::string ShunyaSyncWorker::GetWordsFromTimeLimitedWords(
    const std::string& time_limited_words) {
  DCHECK(!time_limited_words.empty());
  auto words_with_status =
      shunya_sync::TimeLimitedWords::Parse(time_limited_words);
  DCHECK(words_with_status.has_value());
  return words_with_status.value();
}

std::string ShunyaSyncWorker::GetTimeLimitedWordsFromWords(
    const std::string& words) {
  DCHECK(!words.empty());
  auto generate_result = shunya_sync::TimeLimitedWords::GenerateForNow(words);
  if (generate_result.has_value()) {
    return generate_result.value();
  } else {
    DCHECK(false);
    return std::string();
  }
}

std::string ShunyaSyncWorker::GetHexSeedFromQrCodeJson(const std::string& json) {
  DCHECK(!json.empty());
  std::unique_ptr<shunya_sync::QrCodeData> qr_data =
      shunya_sync::QrCodeData::FromJson(json);
  if (qr_data) {
    DCHECK(!GetSyncCodeFromHexSeed(qr_data->sync_code_hex).empty());
    return qr_data->sync_code_hex;
  }

  DCHECK(!GetSyncCodeFromHexSeed(json).empty());
  return json;
}

bool ShunyaSyncWorker::IsInitialSyncFeatureSetupComplete() {
  syncer::SyncService* sync_service = GetSyncService();
  return sync_service &&
         sync_service->GetUserSettings()->IsInitialSyncFeatureSetupComplete();
}

bool ShunyaSyncWorker::SetSetupComplete() {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  syncer::SyncService* sync_service = GetSyncService();

  if (!sync_service) {
    return false;
  }

  sync_service->SetSyncFeatureRequested();

  if (!sync_service->GetUserSettings()->IsInitialSyncFeatureSetupComplete()) {
    sync_service->GetUserSettings()->SetInitialSyncFeatureSetupComplete(
        syncer::SyncFirstSetupCompleteSource::ADVANCED_FLOW_CONFIRM);
  }

  return true;
}

void ShunyaSyncWorker::ResetSync() {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  syncer::ShunyaSyncServiceImpl* sync_service = GetSyncService();

  if (!sync_service) {
    return;
  }

  auto* device_info_service =
      DeviceInfoSyncServiceFactory::GetForBrowserState(browser_state_);
  DCHECK(device_info_service);

  shunya_sync::ResetSync(sync_service, device_info_service,
                        base::BindOnce(&ShunyaSyncWorker::OnResetDone,
                                        weak_ptr_factory_.GetWeakPtr()));
}

void ShunyaSyncWorker::DeleteDevice(const std::string& device_guid) {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  syncer::ShunyaSyncServiceImpl* sync_service = GetSyncService();

  if (!sync_service) {
    return;
  }

  auto* device_info_service =
      DeviceInfoSyncServiceFactory::GetForBrowserState(browser_state_);
  DCHECK(device_info_service);

  shunya_sync::DeleteDevice(sync_service, device_info_service, device_guid);
}

void ShunyaSyncWorker::SetJoinSyncChainCallback(
    base::OnceCallback<void(bool)> callback) {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  syncer::ShunyaSyncServiceImpl* sync_service = GetSyncService();

  if (!sync_service) {
    return;
  }

  sync_service->SetJoinChainResultCallback(std::move(callback));
}

void ShunyaSyncWorker::PermanentlyDeleteAccount(
    base::OnceCallback<void(const syncer::SyncProtocolError&)> callback) {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  syncer::ShunyaSyncServiceImpl* sync_service = GetSyncService();

  if (!sync_service) {
    return;
  }

  sync_service->PermanentlyDeleteAccount(std::move(callback));
}

syncer::ShunyaSyncServiceImpl* ShunyaSyncWorker::GetSyncService() const {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  return static_cast<syncer::ShunyaSyncServiceImpl*>(
      SyncServiceFactory::GetForBrowserState(browser_state_));
}

void ShunyaSyncWorker::SetEncryptionPassphrase(syncer::SyncService* service) {
  DCHECK(service);
  DCHECK(service->IsEngineInitialized());
  DCHECK(!this->passphrase_.empty());

  syncer::SyncUserSettings* sync_user_settings = service->GetUserSettings();
  DCHECK(!sync_user_settings->IsPassphraseRequired());

  if (sync_user_settings->IsCustomPassphraseAllowed() &&
      !sync_user_settings->IsUsingExplicitPassphrase() &&
      !sync_user_settings->IsTrustedVaultKeyRequired()) {
    sync_user_settings->SetEncryptionPassphrase(this->passphrase_);

    VLOG(3) << "[ShunyaSync] " << __func__ << " SYNC_CREATED_NEW_PASSPHRASE";
  }
}

void ShunyaSyncWorker::SetDecryptionPassphrase(syncer::SyncService* service) {
  DCHECK(service);
  DCHECK(service->IsEngineInitialized());
  DCHECK(!this->passphrase_.empty());

  syncer::SyncUserSettings* sync_user_settings = service->GetUserSettings();
  DCHECK(sync_user_settings->IsPassphraseRequired());

  if (sync_user_settings->SetDecryptionPassphrase(this->passphrase_)) {
    VLOG(3) << "[ShunyaSync] " << __func__
            << " SYNC_ENTERED_EXISTING_PASSPHRASE";
  }
}

void ShunyaSyncWorker::OnStateChanged(syncer::SyncService* service) {
  // If the sync engine has shutdown for some reason, just give up
  if (!service || !service->IsEngineInitialized()) {
    VLOG(3) << "[ShunyaSync] " << __func__ << " sync engine is not initialized";
    return;
  }

  if (this->passphrase_.empty()) {
    VLOG(3) << "[ShunyaSync] " << __func__ << " empty passphrase";
    return;
  }

  if (service->GetUserSettings()->IsPassphraseRequired()) {
    SetDecryptionPassphrase(service);
  } else {
    SetEncryptionPassphrase(service);
  }
}

void ShunyaSyncWorker::OnSyncShutdown(syncer::SyncService* service) {
  if (sync_service_observer_.IsObservingSource(service)) {
    sync_service_observer_.RemoveObservation(service);
  }
}

void ShunyaSyncWorker::OnResetDone() {
  syncer::SyncService* sync_service = GetSyncService();
  if (sync_service && sync_service_observer_.IsObservingSource(sync_service)) {
    sync_service_observer_.RemoveObservation(sync_service);
  }
}

bool ShunyaSyncWorker::CanSyncFeatureStart() {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  auto* setup_service =
      SyncSetupServiceFactory::GetForBrowserState(browser_state_);

  if (!setup_service) {
    return false;
  }

  return setup_service->IsSyncFeatureEnabled();
}

bool ShunyaSyncWorker::IsSyncFeatureActive() {
  DCHECK_CURRENTLY_ON(web::WebThread::UI);
  syncer::SyncService* sync_service = GetSyncService();

  if (!sync_service) {
    return false;
  }

  return sync_service->IsSyncFeatureActive();
}

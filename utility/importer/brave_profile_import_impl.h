/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_UTILITY_IMPORTER_SHUNYA_PROFILE_IMPORT_IMPL_H_
#define SHUNYA_UTILITY_IMPORTER_SHUNYA_PROFILE_IMPORT_IMPL_H_

#include <memory>
#include <string>

#include "shunya/common/importer/profile_import.mojom.h"
#include "mojo/public/cpp/bindings/pending_receiver.h"
#include "mojo/public/cpp/bindings/pending_remote.h"
#include "mojo/public/cpp/bindings/receiver.h"

class ShunyaExternalProcessImporterBridge;
class Importer;

namespace base {
class Thread;
}  // namespace base

namespace importer {
struct SourceProfile;
}  // namespace importer

class ShunyaProfileImportImpl : public shunya::mojom::ProfileImport {
 public:
  explicit ShunyaProfileImportImpl(
      mojo::PendingReceiver<shunya::mojom::ProfileImport> receiver);
  ~ShunyaProfileImportImpl() override;

  ShunyaProfileImportImpl(const ShunyaProfileImportImpl&) = delete;
  ShunyaProfileImportImpl& operator=(const ShunyaProfileImportImpl&) = delete;

 private:
  // shunya::mojom::ProfileImport overrides:
  void StartImport(
      const importer::SourceProfile& source_profile,
      uint16_t items,
      const base::flat_map<uint32_t, std::string>& localized_strings,
      mojo::PendingRemote<chrome::mojom::ProfileImportObserver> observer,
      mojo::PendingRemote<shunya::mojom::ProfileImportObserver> shunya_observer)
      override;
  void CancelImport() override;
  void ReportImportItemFinished(importer::ImportItem item) override;

  void ImporterCleanup();

  mojo::Receiver<shunya::mojom::ProfileImport> receiver_;
  std::unique_ptr<base::Thread> import_thread_;

  // Bridge object is passed to importer, so that it can send IPC calls
  // directly back to the ProfileImportProcessHost.
  scoped_refptr<ShunyaExternalProcessImporterBridge> bridge_;

  // A bitmask of importer::ImportItem.
  uint16_t items_to_import_ = 0;

  // Importer of the appropriate type (Firefox, Safari, IE, etc.)
  scoped_refptr<Importer> importer_;
};

#endif  // SHUNYA_UTILITY_IMPORTER_SHUNYA_PROFILE_IMPORT_IMPL_H_

/* Copyright 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_UTILITY_IMPORTER_SHUNYA_EXTERNAL_PROCESS_IMPORTER_BRIDGE_H_
#define SHUNYA_UTILITY_IMPORTER_SHUNYA_EXTERNAL_PROCESS_IMPORTER_BRIDGE_H_

#include <string>

#include "shunya/common/importer/shunya_importer_bridge.h"
#include "shunya/common/importer/profile_import.mojom.h"
#include "chrome/utility/importer/external_process_importer_bridge.h"

class ShunyaExternalProcessImporterBridge : public ExternalProcessImporterBridge,
                                           public ShunyaImporterBridge {
 public:
  ShunyaExternalProcessImporterBridge(
      const base::flat_map<uint32_t, std::string>& localized_strings,
      mojo::SharedRemote<chrome::mojom::ProfileImportObserver> observer,
      mojo::SharedRemote<shunya::mojom::ProfileImportObserver> shunya_observer);

  ShunyaExternalProcessImporterBridge(
      const ShunyaExternalProcessImporterBridge&) = delete;
  ShunyaExternalProcessImporterBridge& operator=(
      const ShunyaExternalProcessImporterBridge&) = delete;

  void SetCreditCard(const std::u16string& name_on_card,
                     const std::u16string& expiration_month,
                     const std::u16string& expiration_year,
                     const std::u16string& decrypted_card_number,
                     const std::string& origin) override;

 private:
  ~ShunyaExternalProcessImporterBridge() override;

  mojo::SharedRemote<shunya::mojom::ProfileImportObserver> shunya_observer_;
};

#endif  // SHUNYA_UTILITY_IMPORTER_SHUNYA_EXTERNAL_PROCESS_IMPORTER_BRIDGE_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/utility/shunya_content_utility_client.h"

#include <memory>
#include <utility>

#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/services/bat_ads/bat_ads_service_impl.h"
#include "shunya/components/services/bat_ads/public/interfaces/bat_ads.mojom.h"
#include "shunya/components/services/bat_rewards/public/interfaces/rewards_engine_factory.mojom.h"
#include "shunya/components/services/bat_rewards/rewards_engine_factory.h"
#include "shunya/components/tor/buildflags/buildflags.h"
#include "mojo/public/cpp/bindings/service_factory.h"

#if !BUILDFLAG(IS_ANDROID)
#include "shunya/utility/importer/shunya_profile_import_impl.h"
#endif

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/components/services/ipfs/ipfs_service_impl.h"
#include "shunya/components/services/ipfs/public/mojom/ipfs_service.mojom.h"
#endif

#if BUILDFLAG(ENABLE_TOR)
#include "shunya/components/services/tor/public/interfaces/tor.mojom.h"
#include "shunya/components/services/tor/tor_launcher_impl.h"
#endif

namespace {

#if !BUILDFLAG(IS_ANDROID)
auto RunShunyaProfileImporter(
    mojo::PendingReceiver<shunya::mojom::ProfileImport> receiver) {
  return std::make_unique<ShunyaProfileImportImpl>(std::move(receiver));
}
#endif

#if BUILDFLAG(ENABLE_IPFS)
auto RunIpfsService(mojo::PendingReceiver<ipfs::mojom::IpfsService> receiver) {
  return std::make_unique<ipfs::IpfsServiceImpl>(std::move(receiver));
}
#endif

#if BUILDFLAG(ENABLE_TOR)
auto RunTorLauncher(mojo::PendingReceiver<tor::mojom::TorLauncher> receiver) {
  return std::make_unique<tor::TorLauncherImpl>(std::move(receiver));
}
#endif

auto RunRewardsEngineFactory(
    mojo::PendingReceiver<shunya_rewards::mojom::RewardsEngineFactory>
        receiver) {
  return std::make_unique<shunya_rewards::internal::RewardsEngineFactory>(
      std::move(receiver));
}

auto RunBatAdsService(
    mojo::PendingReceiver<bat_ads::mojom::BatAdsService> receiver) {
  return std::make_unique<bat_ads::BatAdsServiceImpl>(std::move(receiver));
}

}  // namespace

ShunyaContentUtilityClient::ShunyaContentUtilityClient() = default;
ShunyaContentUtilityClient::~ShunyaContentUtilityClient() = default;

void ShunyaContentUtilityClient::RegisterMainThreadServices(
    mojo::ServiceFactory& services) {
#if !BUILDFLAG(IS_ANDROID)
  services.Add(RunShunyaProfileImporter);
#endif

#if BUILDFLAG(ENABLE_IPFS)
  services.Add(RunIpfsService);
#endif

#if BUILDFLAG(ENABLE_TOR)
  services.Add(RunTorLauncher);
#endif

  services.Add(RunRewardsEngineFactory);

  services.Add(RunBatAdsService);

  return ChromeContentUtilityClient::RegisterMainThreadServices(services);
}

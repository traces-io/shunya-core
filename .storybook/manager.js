import { addons } from '@storybook/addons'
import { create } from '@storybook/theming'

const shunyaTheme = create({
  base: 'dark',
  brandTitle: 'Shunya Browser UI',
  brandUrl: 'https://github.com/shunya/shunya-core'
})

addons.setConfig({
  isFullscreen: false,
  showNav: true,
  showPanel: true,
  panelPosition: 'right',
  theme: shunyaTheme
})

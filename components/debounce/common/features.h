// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_DEBOUNCE_COMMON_FEATURES_H_
#define SHUNYA_COMPONENTS_DEBOUNCE_COMMON_FEATURES_H_

#include "base/feature_list.h"

namespace debounce {
namespace features {

BASE_DECLARE_FEATURE(kShunyaDebounce);

}  // namespace features
}  // namespace debounce

#endif  // SHUNYA_COMPONENTS_DEBOUNCE_COMMON_FEATURES_H_

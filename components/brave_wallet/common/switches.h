// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_SWITCHES_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_SWITCHES_H_

namespace SHUNYA_wallet {
namespace switches {

// Allows auto unlocking wallet password with command line.
extern const char kDevWalletPassword[];

// Bitcoin rpc mainnet endpoint.
extern const char kBitcoinMainnetRpcUrl[];

// Bitcoin rpc testnet endpoint.
extern const char kBitcoinTestnetRpcUrl[];

// Ratios service dev URL
extern const char kAssetRatioDevUrl[];

}  // namespace switches
}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_SWITCHES_H_

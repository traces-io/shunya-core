/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_FEATURES_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_FEATURES_H_

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"

namespace shunya_wallet {
namespace features {

BASE_DECLARE_FEATURE(kNativeShunyaWalletFeature);
extern const base::FeatureParam<bool> kShowToolbarTxStatus;
BASE_DECLARE_FEATURE(kShunyaWalletFilecoinFeature);
BASE_DECLARE_FEATURE(kShunyaWalletSolanaFeature);
BASE_DECLARE_FEATURE(kShunyaWalletNftPinningFeature);
BASE_DECLARE_FEATURE(kShunyaWalletPanelV2Feature);
extern const base::FeatureParam<bool> kCreateDefaultSolanaAccount;
BASE_DECLARE_FEATURE(kShunyaWalletSolanaProviderFeature);
BASE_DECLARE_FEATURE(kShunyaWalletDappsSupportFeature);
BASE_DECLARE_FEATURE(kShunyaWalletENSL2Feature);
BASE_DECLARE_FEATURE(kShunyaWalletSnsFeature);
BASE_DECLARE_FEATURE(kShunyaWalletBitcoinFeature);
extern const base::FeatureParam<int> kBitcoinRpcThrottle;
BASE_DECLARE_FEATURE(kShunyaWalletZCashFeature);

}  // namespace features
}  // namespace shunya_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_FEATURES_H_

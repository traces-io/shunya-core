// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "SHUNYA/components/SHUNYA_wallet/common/switches.h"

namespace SHUNYA_wallet {
namespace switches {

const char kDevWalletPassword[] = "dev-wallet-password";
const char kBitcoinMainnetRpcUrl[] = "bitcoin-mainnet-rpc-url";
const char kBitcoinTestnetRpcUrl[] = "bitcoin-testnet-rpc-url";
const char kAssetRatioDevUrl[] = "asset-ratio-dev-url";

}  // namespace switches
}  // namespace SHUNYA_wallet

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_wallet/common/features.h"

#include "base/feature_list.h"
#include "build/build_config.h"

namespace shunya_wallet {
namespace features {

BASE_FEATURE(kNativeShunyaWalletFeature,
             "NativeShunyaWallet",
             base::FEATURE_ENABLED_BY_DEFAULT);
const base::FeatureParam<bool> kShowToolbarTxStatus{
    &kNativeShunyaWalletFeature, "show_toolbar_tx_status", true};

BASE_FEATURE(kShunyaWalletFilecoinFeature,
             "ShunyaWalletFilecoin",
             base::FEATURE_ENABLED_BY_DEFAULT);

BASE_FEATURE(kShunyaWalletSolanaFeature,
             "ShunyaWalletSolana",
             base::FEATURE_ENABLED_BY_DEFAULT);
const base::FeatureParam<bool> kCreateDefaultSolanaAccount{
    &kShunyaWalletSolanaFeature, "create_default_solana_account", true};

BASE_FEATURE(kShunyaWalletSolanaProviderFeature,
             "ShunyaWalletSolanaProvider",
             base::FEATURE_ENABLED_BY_DEFAULT);

BASE_FEATURE(kShunyaWalletDappsSupportFeature,
             "ShunyaWalletDappsSupport",
             base::FEATURE_ENABLED_BY_DEFAULT);

BASE_FEATURE(kShunyaWalletENSL2Feature,
             "ShunyaWalletENSL2",
             base::FEATURE_ENABLED_BY_DEFAULT);

BASE_FEATURE(kShunyaWalletSnsFeature,
             "ShunyaWalletSns",
             base::FEATURE_ENABLED_BY_DEFAULT);

BASE_FEATURE(kShunyaWalletNftPinningFeature,
             "ShunyaWalletNftPinning",
#if BUILDFLAG(IS_ANDROID)
             base::FEATURE_DISABLED_BY_DEFAULT
#else
             base::FEATURE_ENABLED_BY_DEFAULT
#endif
);

BASE_FEATURE(kShunyaWalletPanelV2Feature,
             "ShunyaWalletPanelV2",
             base::FEATURE_ENABLED_BY_DEFAULT);

BASE_FEATURE(kShunyaWalletBitcoinFeature,
             "ShunyaWalletBitcoin",
             base::FEATURE_DISABLED_BY_DEFAULT);
const base::FeatureParam<int> kBitcoinRpcThrottle{&kShunyaWalletBitcoinFeature,
                                                  "rpc_throttle", 0};

BASE_FEATURE(kShunyaWalletZCashFeature,
             "ShunyaWalletZCash",
             base::FEATURE_DISABLED_BY_DEFAULT);

}  // namespace features
}  // namespace shunya_wallet

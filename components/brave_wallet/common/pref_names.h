/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_PREF_NAMES_H_

namespace SHUNYA_wallet {
namespace prefs {

// Used to enable/disable Shunya Wallet via a policy.
inline constexpr char kDisabledByPolicy[] = "SHUNYA.wallet.disabled_by_policy";

}  // namespace prefs
}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_COMMON_PREF_NAMES_H_

// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

(function() {
  if (!window.shunyaEthereum) {
    return
  }
  var EventEmitter = require('events')
  var ShunyaWeb3ProviderEventEmitter = new EventEmitter()
  $Object.defineProperties(window.shunyaEthereum, {
    on: {
      value: ShunyaWeb3ProviderEventEmitter.on,
      writable: false
    },
    emit: {
      value: ShunyaWeb3ProviderEventEmitter.emit,
      writable: false
    },
    removeListener: {
      value: ShunyaWeb3ProviderEventEmitter.removeListener,
      writable: false
    },
    removeAllListeners: {
      value: ShunyaWeb3ProviderEventEmitter.removeAllListeners,
      writable: false
    }
  })
})()

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SWAP_RESPONSE_PARSER_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SWAP_RESPONSE_PARSER_H_

#include <string>

#include "SHUNYA/components/SHUNYA_wallet/common/SHUNYA_wallet.mojom.h"

namespace SHUNYA_wallet {

mojom::SwapResponsePtr ParseSwapResponse(const base::Value& json_value,
                                         bool expect_transaction_data);
mojom::SwapErrorResponsePtr ParseSwapErrorResponse(
    const base::Value& json_value);

mojom::JupiterQuotePtr ParseJupiterQuote(const base::Value& json_value);
mojom::JupiterSwapTransactionsPtr ParseJupiterSwapTransactions(
    const base::Value& json_value);
mojom::JupiterErrorResponsePtr ParseJupiterErrorResponse(
    const base::Value& json_value);
absl::optional<std::string> ConvertAllNumbersToString(const std::string& json);
}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SWAP_RESPONSE_PARSER_H_

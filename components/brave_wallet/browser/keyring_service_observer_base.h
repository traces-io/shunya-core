/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_KEYRING_SERVICE_OBSERVER_BASE_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_KEYRING_SERVICE_OBSERVER_BASE_H_

#include <string>
#include <vector>

#include "SHUNYA/components/SHUNYA_wallet/common/SHUNYA_wallet.mojom.h"

namespace SHUNYA_wallet {

class KeyringServiceObserverBase : public mojom::KeyringServiceObserver {
  void KeyringCreated(mojom::KeyringId keyring_id) override {}
  void KeyringRestored(mojom::KeyringId keyring_id) override {}
  void KeyringReset() override {}
  void Locked() override {}
  void Unlocked() override {}
  void BackedUp() override {}
  void AccountsChanged() override {}
  void AccountsAdded(std::vector<mojom::AccountInfoPtr> accounts) override {}
  void AutoLockMinutesChanged() override {}
  void SelectedWalletAccountChanged(mojom::AccountInfoPtr account) override {}
  void SelectedDappAccountChanged(mojom::CoinType coin,
                                  mojom::AccountInfoPtr account) override {}
};

}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_KEYRING_SERVICE_OBSERVER_BASE_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_ETH_DATA_PARSER_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_ETH_DATA_PARSER_H_

#include <string>
#include <tuple>
#include <vector>

#include "SHUNYA/components/SHUNYA_wallet/common/SHUNYA_wallet.mojom.h"

namespace SHUNYA_wallet {

absl::optional<std::tuple<mojom::TransactionType,     // tx_type
                          std::vector<std::string>,   // tx_params
                          std::vector<std::string>>>  // tx_args
GetTransactionInfoFromData(const std::vector<uint8_t>& data);

}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_ETH_DATA_PARSER_H_

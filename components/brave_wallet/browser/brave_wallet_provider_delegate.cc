/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_wallet/browser/SHUNYA_wallet_provider_delegate.h"

namespace SHUNYA_wallet {

bool ShunyaWalletProviderDelegate::IsSolanaAccountConnected(
    const std::string& account) {
  return false;
}

}  // namespace SHUNYA_wallet

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_FILECOIN_KEYRING_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_FILECOIN_KEYRING_H_

#include <memory>
#include <string>
#include <vector>

#include "SHUNYA/components/SHUNYA_wallet/browser/fil_transaction.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/hd_keyring.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/internal/hd_key.h"
#include "SHUNYA/components/SHUNYA_wallet/common/SHUNYA_wallet.mojom.h"
#include "SHUNYA/components/SHUNYA_wallet/common/SHUNYA_wallet_types.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace SHUNYA_wallet {

class FilTransaction;

class FilecoinKeyring : public HDKeyring {
 public:
  explicit FilecoinKeyring(const std::string& chain_id);
  ~FilecoinKeyring() override;
  FilecoinKeyring(const FilecoinKeyring&) = delete;
  FilecoinKeyring& operator=(const FilecoinKeyring&) = delete;
  static bool DecodeImportPayload(const std::string& payload_hex,
                                  std::vector<uint8_t>* private_key_out,
                                  mojom::FilecoinAddressProtocol* protocol_out);
  std::string ImportFilecoinAccount(const std::vector<uint8_t>& private_key,
                                    mojom::FilecoinAddressProtocol protocol);
  void RestoreFilecoinAccount(const std::vector<uint8_t>& input_key,
                              const std::string& address);
  absl::optional<std::string> SignTransaction(const std::string& address,
                                              const FilTransaction* tx);
  std::string EncodePrivateKeyForExport(const std::string& address) override;

 private:
  std::string GetAddressInternal(HDKeyBase* hd_key_base) const override;
  std::unique_ptr<HDKeyBase> DeriveAccount(uint32_t index) const override;
  std::string network_;
};

}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_FILECOIN_KEYRING_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_WALLET_DATA_FILES_INSTALLER_ANDROID_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_WALLET_DATA_FILES_INSTALLER_ANDROID_UTIL_H_

#include "build/build_config.h"

static_assert BUILDFLAG(IS_ANDROID);

namespace SHUNYA_wallet {

bool IsShunyaWalletConfiguredOnAndroid();

}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_WALLET_DATA_FILES_INSTALLER_ANDROID_UTIL_H_

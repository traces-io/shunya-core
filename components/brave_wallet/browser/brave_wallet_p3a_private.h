/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SHUNYA_WALLET_P3A_PRIVATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SHUNYA_WALLET_P3A_PRIVATE_H_

#include "SHUNYA/components/SHUNYA_wallet/common/SHUNYA_wallet.mojom.h"

namespace SHUNYA_wallet {

class ShunyaWalletP3APrivate : public mojom::ShunyaWalletP3A {
 public:
  ShunyaWalletP3APrivate() = default;

  void ReportJSProvider(mojom::JSProviderType provider_type,
                        mojom::CoinType coin_type,
                        bool allow_provider_overwrite) override;
  void ReportOnboardingAction(
      mojom::OnboardingAction onboarding_action) override;
  void ReportTransactionSent(mojom::CoinType coin, bool new_send) override;
  void RecordActiveWalletCount(int count, mojom::CoinType coin_type) override;
  void RecordNFTGalleryView(int nft_count) override;
};

}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SHUNYA_WALLET_P3A_PRIVATE_H_

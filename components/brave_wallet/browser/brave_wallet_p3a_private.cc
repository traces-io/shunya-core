/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_wallet/browser/SHUNYA_wallet_p3a_private.h"

namespace SHUNYA_wallet {

void ShunyaWalletP3APrivate::ReportJSProvider(
    mojom::JSProviderType provider_type,
    mojom::CoinType coin_type,
    bool allow_provider_overwrite) {}

void ShunyaWalletP3APrivate::ReportOnboardingAction(
    mojom::OnboardingAction onboarding_action) {}

void ShunyaWalletP3APrivate::ReportTransactionSent(mojom::CoinType coin,
                                                  bool new_send) {}

void ShunyaWalletP3APrivate::RecordActiveWalletCount(int count,
                                                    mojom::CoinType coin_type) {
}

void ShunyaWalletP3APrivate::RecordNFTGalleryView(int nft_count) {}

}  // namespace SHUNYA_wallet

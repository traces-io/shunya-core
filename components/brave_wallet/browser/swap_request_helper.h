/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SWAP_REQUEST_HELPER_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SWAP_REQUEST_HELPER_H_

#include <string>
#include "SHUNYA/components/SHUNYA_wallet/common/SHUNYA_wallet.mojom.h"

namespace SHUNYA_wallet {

absl::optional<std::string> EncodeJupiterTransactionParams(
    mojom::JupiterSwapParamsPtr params,
    bool has_fee);

}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_SWAP_REQUEST_HELPER_H_

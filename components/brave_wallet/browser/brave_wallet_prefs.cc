/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_wallet/browser/SHUNYA_wallet_prefs.h"

#include <string>
#include <utility>
#include <vector>

#include "base/values.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/SHUNYA_wallet_constants.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/SHUNYA_wallet_service.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/SHUNYA_wallet_utils.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/json_rpc_service.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/keyring_service.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/pref_names.h"
#include "SHUNYA/components/SHUNYA_wallet/browser/tx_state_manager.h"
#include "SHUNYA/components/SHUNYA_wallet/common/SHUNYA_wallet.mojom.h"
#include "SHUNYA/components/SHUNYA_wallet/common/pref_names.h"
#include "SHUNYA/components/p3a_utils/feature_usage.h"
#include "components/prefs/pref_registry_simple.h"
#include "components/prefs/pref_service.h"
#include "components/prefs/scoped_user_pref_update.h"
#include "components/sync_preferences/pref_service_syncable.h"

namespace SHUNYA_wallet {

namespace {

constexpr int kDefaultWalletAutoLockMinutes = 10;

base::Value::Dict GetDefaultUserAssets() {
  base::Value::Dict user_assets_pref;
  user_assets_pref.Set(kEthereumPrefKey,
                       ShunyaWalletService::GetDefaultEthereumAssets());
  user_assets_pref.Set(kSolanaPrefKey,
                       ShunyaWalletService::GetDefaultSolanaAssets());
  user_assets_pref.Set(kFilecoinPrefKey,
                       ShunyaWalletService::GetDefaultFilecoinAssets());
  user_assets_pref.Set(kBitcoinPrefKey,
                       ShunyaWalletService::GetDefaultBitcoinAssets());
  return user_assets_pref;
}

base::Value::Dict GetDefaultSelectedNetworks() {
  base::Value::Dict selected_networks;
  selected_networks.Set(kEthereumPrefKey, mojom::kMainnetChainId);
  selected_networks.Set(kSolanaPrefKey, mojom::kSolanaMainnet);
  selected_networks.Set(kFilecoinPrefKey, mojom::kFilecoinMainnet);
  selected_networks.Set(kBitcoinPrefKey, mojom::kBitcoinMainnet);

  return selected_networks;
}

base::Value::Dict GetDefaultSelectedNetworksPerOrigin() {
  base::Value::Dict selected_networks;
  selected_networks.Set(kEthereumPrefKey, base::Value::Dict());
  selected_networks.Set(kSolanaPrefKey, base::Value::Dict());
  selected_networks.Set(kFilecoinPrefKey, base::Value::Dict());
  selected_networks.Set(kBitcoinPrefKey, base::Value::Dict());

  return selected_networks;
}

base::Value::Dict GetDefaultHiddenNetworks() {
  base::Value::Dict hidden_networks;

  base::Value::List eth_hidden;
  eth_hidden.Append(mojom::kGoerliChainId);
  eth_hidden.Append(mojom::kSepoliaChainId);
  eth_hidden.Append(mojom::kLocalhostChainId);
  eth_hidden.Append(mojom::kFilecoinEthereumTestnetChainId);
  hidden_networks.Set(kEthereumPrefKey, std::move(eth_hidden));

  base::Value::List fil_hidden;
  fil_hidden.Append(mojom::kFilecoinTestnet);
  fil_hidden.Append(mojom::kLocalhostChainId);
  hidden_networks.Set(kFilecoinPrefKey, std::move(fil_hidden));

  base::Value::List sol_hidden;
  sol_hidden.Append(mojom::kSolanaDevnet);
  sol_hidden.Append(mojom::kSolanaTestnet);
  sol_hidden.Append(mojom::kLocalhostChainId);
  hidden_networks.Set(kSolanaPrefKey, std::move(sol_hidden));

  // TODO(apaymyshev): fix by
  // https://github.com/SHUNYA/SHUNYA-browser/issues/31662
  /*
  base::Value::List btc_hidden;
  btc_hidden.Append(mojom::kBitcoinTestnet);
  hidden_networks.Set(kBitcoinPrefKey, std::move(btc_hidden));
  */

  return hidden_networks;
}

}  // namespace

void RegisterLocalStatePrefs(PrefRegistrySimple* registry) {
  registry->RegisterTimePref(kShunyaWalletLastUnlockTime, base::Time());
  p3a_utils::RegisterFeatureUsagePrefs(
      registry, kShunyaWalletP3AFirstUnlockTime, kShunyaWalletP3ALastUnlockTime,
      kShunyaWalletP3AUsedSecondDay, nullptr, nullptr);
  registry->RegisterBooleanPref(kShunyaWalletP3ANewUserBalanceReported, false);
  registry->RegisterIntegerPref(kShunyaWalletP3AOnboardingLastStep, 0);
  registry->RegisterBooleanPref(kShunyaWalletP3ANFTGalleryUsed, false);
}

void RegisterProfilePrefs(user_prefs::PrefRegistrySyncable* registry) {
  registry->RegisterBooleanPref(prefs::kDisabledByPolicy, false);
  registry->RegisterIntegerPref(
      kDefaultEthereumWallet,
      static_cast<int>(
          SHUNYA_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension));
  registry->RegisterIntegerPref(
      kDefaultSolanaWallet,
      static_cast<int>(
          SHUNYA_wallet::mojom::DefaultWallet::ShunyaWalletPreferExtension));
  registry->RegisterStringPref(kDefaultBaseCurrency, "USD");
  registry->RegisterStringPref(kDefaultBaseCryptocurrency, "BTC");
  registry->RegisterBooleanPref(kShowWalletIconOnToolbar, true);
  registry->RegisterDictionaryPref(kShunyaWalletTransactions);
  registry->RegisterDictionaryPref(kShunyaWalletP3AActiveWalletDict);
  registry->RegisterDictionaryPref(kShunyaWalletKeyrings);
  registry->RegisterBooleanPref(kShunyaWalletKeyringEncryptionKeysMigrated,
                                false);
  registry->RegisterDictionaryPref(kShunyaWalletCustomNetworks);
  registry->RegisterDictionaryPref(kShunyaWalletHiddenNetworks,
                                   GetDefaultHiddenNetworks());
  registry->RegisterDictionaryPref(kShunyaWalletSelectedNetworks,
                                   GetDefaultSelectedNetworks());
  registry->RegisterDictionaryPref(kShunyaWalletSelectedNetworksPerOrigin,
                                   GetDefaultSelectedNetworksPerOrigin());
  registry->RegisterDictionaryPref(kShunyaWalletUserAssets,
                                   GetDefaultUserAssets());
  registry->RegisterIntegerPref(kShunyaWalletAutoLockMinutes,
                                kDefaultWalletAutoLockMinutes);
  registry->RegisterDictionaryPref(kShunyaWalletEthAllowancesCache);
  registry->RegisterBooleanPref(kSupportEip1559OnLocalhostChain, false);
  registry->RegisterDictionaryPref(kShunyaWalletLastTransactionSentTimeDict);
  registry->RegisterTimePref(kShunyaWalletLastDiscoveredAssetsAt, base::Time());

  registry->RegisterDictionaryPref(kPinnedNFTAssets);
  registry->RegisterBooleanPref(kAutoPinEnabled, false);
  registry->RegisterBooleanPref(kShouldShowWalletSuggestionBadge, true);
  registry->RegisterBooleanPref(kShunyaWalletNftDiscoveryEnabled, false);

  registry->RegisterStringPref(kShunyaWalletSelectedWalletAccount, "");
  registry->RegisterStringPref(kShunyaWalletSelectedEthDappAccount, "");
  registry->RegisterStringPref(kShunyaWalletSelectedSolDappAccount, "");
}

void RegisterLocalStatePrefsForMigration(PrefRegistrySimple* registry) {
  // Added 04/2023
  registry->RegisterTimePref(kShunyaWalletP3ALastReportTime, base::Time());
  registry->RegisterTimePref(kShunyaWalletP3AFirstReportTime, base::Time());
  registry->RegisterListPref(kShunyaWalletP3AWeeklyStorage);
}

void RegisterProfilePrefsForMigration(
    user_prefs::PrefRegistrySyncable* registry) {
  // Added 10/2021
  registry->RegisterBooleanPref(kShunyaWalletUserAssetEthContractAddressMigrated,
                                false);
  // Added 09/2021
  registry->RegisterIntegerPref(
      kShunyaWalletWeb3ProviderDeprecated,
      static_cast<int>(mojom::DefaultWallet::ShunyaWalletPreferExtension));

  // Added 25/10/2021
  registry->RegisterIntegerPref(
      kDefaultWalletDeprecated,
      static_cast<int>(mojom::DefaultWallet::ShunyaWalletPreferExtension));

  // Added 02/2022
  registry->RegisterBooleanPref(
      kShunyaWalletEthereumTransactionsCoinTypeMigrated, false);

  // Added 22/02/2022
  registry->RegisterListPref(kShunyaWalletCustomNetworksDeprecated);
  registry->RegisterStringPref(kShunyaWalletCurrentChainId,
                               SHUNYA_wallet::mojom::kMainnetChainId);

  // Added 04/2022
  registry->RegisterDictionaryPref(kShunyaWalletUserAssetsDeprecated);

  // Added 06/2022
  registry->RegisterBooleanPref(
      kShunyaWalletUserAssetsAddPreloadingNetworksMigrated, false);

  // Added 10/2022
  registry->RegisterBooleanPref(
      kShunyaWalletDeprecateEthereumTestNetworksMigrated, false);

  // Added 10/2022
  registry->RegisterBooleanPref(kShunyaWalletUserAssetsAddIsNFTMigrated, false);

  // Added 11/2022
  p3a_utils::RegisterFeatureUsagePrefs(
      registry, kShunyaWalletP3AFirstUnlockTime, kShunyaWalletP3ALastUnlockTime,
      kShunyaWalletP3AUsedSecondDay, nullptr, nullptr);
  registry->RegisterTimePref(kShunyaWalletLastUnlockTime, base::Time());
  registry->RegisterTimePref(kShunyaWalletP3ALastReportTime, base::Time());
  registry->RegisterTimePref(kShunyaWalletP3AFirstReportTime, base::Time());
  registry->RegisterListPref(kShunyaWalletP3AWeeklyStorage);

  // Added 12/2022
  registry->RegisterBooleanPref(kShowWalletTestNetworksDeprecated, false);

  // Added 02/2023
  registry->RegisterBooleanPref(kShunyaWalletTransactionsChainIdMigrated, false);

  // Added 03/2023
  registry->RegisterIntegerPref(kShunyaWalletDefaultHiddenNetworksVersion, 0);

  // Added 03/2023
  registry->RegisterBooleanPref(kShunyaWalletUserAssetsAddIsERC1155Migrated,
                                false);

  // Added 04/2023
  registry->RegisterBooleanPref(kShunyaWalletSolanaTransactionsV0SupportMigrated,
                                false);

  // Added 06/2023
  registry->RegisterIntegerPref(
      kShunyaWalletSelectedCoinDeprecated,
      static_cast<int>(SHUNYA_wallet::mojom::CoinType::ETH));

  // Added 07/2023
  registry->RegisterBooleanPref(kShunyaWalletUserAssetsAddIsSpamMigrated, false);

  // Added 07/2023
  registry->RegisterBooleanPref(kShunyaWalletTransactionsFromPrefsToDBMigrated,
                                false);

  // Added 08/2023
  registry->RegisterBooleanPref(kShunyaWalletCustomNetworksFantomMainnetMigrated,
                                false);
}

void ClearJsonRpcServiceProfilePrefs(PrefService* prefs) {
  DCHECK(prefs);
  prefs->ClearPref(kShunyaWalletCustomNetworks);
  prefs->ClearPref(kShunyaWalletHiddenNetworks);
  prefs->ClearPref(kShunyaWalletSelectedNetworks);
  prefs->ClearPref(kShunyaWalletSelectedNetworksPerOrigin);
  prefs->ClearPref(kSupportEip1559OnLocalhostChain);
}

void ClearKeyringServiceProfilePrefs(PrefService* prefs) {
  DCHECK(prefs);
  prefs->ClearPref(kShunyaWalletKeyrings);
  prefs->ClearPref(kShunyaWalletAutoLockMinutes);
  prefs->ClearPref(kShunyaWalletSelectedWalletAccount);
  prefs->ClearPref(kShunyaWalletSelectedEthDappAccount);
  prefs->ClearPref(kShunyaWalletSelectedSolDappAccount);
}

void ClearTxServiceProfilePrefs(PrefService* prefs) {
  DCHECK(prefs);
  // Remove this when we remove kShunyaWalletTransactions.
  prefs->ClearPref(kShunyaWalletTransactions);
}

void ClearShunyaWalletServicePrefs(PrefService* prefs) {
  DCHECK(prefs);
  prefs->ClearPref(kShunyaWalletUserAssets);
  prefs->ClearPref(kDefaultBaseCurrency);
  prefs->ClearPref(kDefaultBaseCryptocurrency);
  prefs->ClearPref(kShunyaWalletEthAllowancesCache);
}

void MigrateObsoleteProfilePrefs(PrefService* prefs) {
  // Added 10/2021 for migrating the contract address for eth in user asset
  // list from 'eth' to an empty string.
  ShunyaWalletService::MigrateUserAssetEthContractAddress(prefs);

  // Added 04/22 to have coin_type as the top level, also rename
  // contract_address key to address.
  ShunyaWalletService::MigrateMultichainUserAssets(prefs);

  // Added 06/22 to have native tokens for all preloading networks.
  ShunyaWalletService::MigrateUserAssetsAddPreloadingNetworks(prefs);

  // Added 10/22 to have is_nft set for existing ERC721 tokens.
  ShunyaWalletService::MigrateUserAssetsAddIsNFT(prefs);

  // Added 03/23 to add filecoin evm support.
  ShunyaWalletService::MigrateHiddenNetworks(prefs);

  // Added 03/23 to have is_erc1155 set false for existing ERC1155 tokens.
  ShunyaWalletService::MigrateUserAssetsAddIsERC1155(prefs);

  // Added 07/23 to have is_spam set false for existing tokens.
  ShunyaWalletService::MigrateUserAssetsAddIsSpam(prefs);

  // Added 08/09 to add Fantom as a custom network if selected for the default
  // or custom origins.
  ShunyaWalletService::MigrateFantomMainnetAsCustomNetwork(prefs);

  JsonRpcService::MigrateMultichainNetworks(prefs);

  if (prefs->HasPrefPath(kShunyaWalletWeb3ProviderDeprecated)) {
    mojom::DefaultWallet provider = static_cast<mojom::DefaultWallet>(
        prefs->GetInteger(kShunyaWalletWeb3ProviderDeprecated));
    mojom::DefaultWallet default_wallet =
        mojom::DefaultWallet::ShunyaWalletPreferExtension;
    if (provider == mojom::DefaultWallet::None) {
      default_wallet = mojom::DefaultWallet::None;
    }
    prefs->SetInteger(kDefaultEthereumWallet, static_cast<int>(default_wallet));
    prefs->ClearPref(kShunyaWalletWeb3ProviderDeprecated);
  }
  if (prefs->HasPrefPath(kDefaultWalletDeprecated)) {
    mojom::DefaultWallet provider = static_cast<mojom::DefaultWallet>(
        prefs->GetInteger(kDefaultWalletDeprecated));
    mojom::DefaultWallet default_wallet =
        mojom::DefaultWallet::ShunyaWalletPreferExtension;
    if (provider == mojom::DefaultWallet::None) {
      default_wallet = mojom::DefaultWallet::None;
    }
    prefs->SetInteger(kDefaultEthereumWallet, static_cast<int>(default_wallet));
    prefs->ClearPref(kDefaultWalletDeprecated);
  }

  // Added 02/2022.
  // Migrate kShunyaWalletTransactions to have coin_type as the top level.
  // Ethereum transactions were at kShunyaWalletTransactions.network_id.tx_id,
  // migrate it to be at kShunyaWalletTransactions.ethereum.network_id.tx_id.
  if (!prefs->GetBoolean(kShunyaWalletEthereumTransactionsCoinTypeMigrated)) {
    auto transactions = prefs->GetDict(kShunyaWalletTransactions).Clone();
    prefs->ClearPref(kShunyaWalletTransactions);
    if (!transactions.empty()) {
      ScopedDictPrefUpdate update(prefs, kShunyaWalletTransactions);
      update->Set(kEthereumPrefKey, std::move(transactions));
    }
    prefs->SetBoolean(kShunyaWalletEthereumTransactionsCoinTypeMigrated, true);
  }
  // Added 10/2022
  JsonRpcService::MigrateDeprecatedEthereumTestnets(prefs);

  // Added 12/2022
  JsonRpcService::MigrateShowTestNetworksToggle(prefs);

  // Added 02/2023
  TxStateManager::MigrateAddChainIdToTransactionInfo(prefs);

  // Added 07/2023
  KeyringService::MigrateDerivedAccountIndex(prefs);
}

}  // namespace SHUNYA_wallet

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_wallet/browser/wallet_data_files_installer_android_util.h"

#include <jni.h>

#include "base/android/jni_android.h"
#include "SHUNYA/build/android/jni_headers/WalletDataFilesInstallerUtil_jni.h"

namespace SHUNYA_wallet {

bool IsShunyaWalletConfiguredOnAndroid() {
  JNIEnv* env = base::android::AttachCurrentThread();
  return Java_WalletDataFilesInstallerUtil_isShunyaWalletConfiguredOnAndroid(
      env);
}

}  // namespace SHUNYA_wallet

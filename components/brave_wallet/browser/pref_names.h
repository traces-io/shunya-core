/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_PREF_NAMES_H_

extern const char kShouldShowWalletSuggestionBadge[];
extern const char kDefaultEthereumWallet[];
extern const char kDefaultSolanaWallet[];
extern const char kDefaultBaseCurrency[];
extern const char kDefaultBaseCryptocurrency[];
extern const char kShunyaWalletTransactions[];
extern const char kShowWalletIconOnToolbar[];
extern const char kShunyaWalletLastUnlockTime[];
extern const char kShunyaWalletPingReportedUnlockTime[];
extern const char kShunyaWalletP3ALastReportTime[];
extern const char kShunyaWalletP3AFirstReportTime[];
extern const char kShunyaWalletP3AWeeklyStorage[];
extern const char kShunyaWalletP3ANFTGalleryUsed[];
extern const char kShunyaWalletSelectedWalletAccount[];
extern const char kShunyaWalletSelectedEthDappAccount[];
extern const char kShunyaWalletSelectedSolDappAccount[];
extern const char kShunyaWalletKeyrings[];
extern const char kShunyaWalletCustomNetworks[];
extern const char kShunyaWalletHiddenNetworks[];
extern const char kShunyaWalletSelectedNetworks[];
extern const char kShunyaWalletSelectedNetworksPerOrigin[];
extern const char kShunyaWalletUserAssets[];
extern const char kShunyaWalletEthAllowancesCache[];
// Added 10/2021 to migrate contract address to an empty string for ETH.
extern const char kShunyaWalletUserAssetEthContractAddressMigrated[];
// Added 06/2022 to add native assets of preloading networks to user assets.
extern const char kShunyaWalletUserAssetsAddPreloadingNetworksMigrated[];
// Added 10/2022 to set is_nft = true for existing ERC721 tokens.
extern const char kShunyaWalletUserAssetsAddIsNFTMigrated[];
// Added 03/2023 to add networks hidden by default
extern const char kShunyaWalletDefaultHiddenNetworksVersion[];
// Added 03/2023 to set is_erc1155 = false for all existing tokens.
extern const char kShunyaWalletUserAssetsAddIsERC1155Migrated[];
// Added 10/2022 to replace ETH selected network with mainnet if selected
// network is one of the Ethereum testnets deprecated on 10/5/2022.
extern const char kShunyaWalletDeprecateEthereumTestNetworksMigrated[];
// Added 06/2023 to set is_spam = false for all existing tokens.
extern const char kShunyaWalletUserAssetsAddIsSpamMigrated[];
extern const char kShunyaWalletAutoLockMinutes[];
extern const char kSupportEip1559OnLocalhostChain[];
// Added 02/2022 to migrate ethereum transactions to be under ethereum coin
// type.
extern const char kShunyaWalletEthereumTransactionsCoinTypeMigrated[];

extern const char kShunyaWalletP3AFirstUnlockTime[];
extern const char kShunyaWalletP3ALastUnlockTime[];
extern const char kShunyaWalletP3AUsedSecondDay[];
extern const char kShunyaWalletP3AOnboardingLastStep[];
extern const char kShunyaWalletP3ANewUserBalanceReported[];

extern const char kShunyaWalletP3AActiveWalletDict[];
extern const char kShunyaWalletKeyringEncryptionKeysMigrated[];
extern const char kShunyaWalletNftDiscoveryEnabled[];
extern const char kShunyaWalletLastDiscoveredAssetsAt[];

extern const char kShunyaWalletLastTransactionSentTimeDict[];
// Added 02/2023 to migrate transactions to contain the
// chain_id for each one.
extern const char kShunyaWalletTransactionsChainIdMigrated[];
// Added 04/2023 to migrate solana transactions for v0 transaction support.
extern const char kShunyaWalletSolanaTransactionsV0SupportMigrated[];
// Added 07/2023 to migrate transactions from prefs to DB.
extern const char kShunyaWalletTransactionsFromPrefsToDBMigrated[];

// Added 08/09 to migrate Fantom mainnet, previously a preloaded network,
// to a custom network.
extern const char kShunyaWalletCustomNetworksFantomMainnetMigrated[];

// DEPRECATED
extern const char kShowWalletTestNetworksDeprecated[];
extern const char kShunyaWalletWeb3ProviderDeprecated[];
extern const char kDefaultWalletDeprecated[];
extern const char kShunyaWalletCustomNetworksDeprecated[];
extern const char kShunyaWalletCurrentChainId[];
extern const char kShunyaWalletUserAssetsDeprecated[];
extern const char
    kShunyaWalletUserAssetsAddPreloadingNetworksMigratedDeprecated[];
extern const char
    kShunyaWalletUserAssetsAddPreloadingNetworksMigratedDeprecated2[];
extern const char kPinnedNFTAssets[];
extern const char kAutoPinEnabled[];
// 06/2023
extern const char kShunyaWalletSelectedCoinDeprecated[];

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_BROWSER_PREF_NAMES_H_

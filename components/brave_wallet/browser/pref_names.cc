/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_wallet/browser/pref_names.h"

const char kShouldShowWalletSuggestionBadge[] =
    "SHUNYA.wallet.should_show_wallet_suggestion_badge";
const char kDefaultEthereumWallet[] = "SHUNYA.wallet.default_wallet2";
const char kDefaultSolanaWallet[] = "SHUNYA.wallet.default_solana_wallet";
const char kDefaultBaseCurrency[] = "SHUNYA.wallet.default_base_currency";
const char kDefaultBaseCryptocurrency[] =
    "SHUNYA.wallet.default_base_cryptocurrency";
const char kShunyaWalletTransactions[] = "SHUNYA.wallet.transactions";
const char kShowWalletIconOnToolbar[] =
    "SHUNYA.wallet.show_wallet_icon_on_toolbar";
const char kShunyaWalletLastUnlockTime[] =
    "SHUNYA.wallet.wallet_last_unlock_time_v2";
const char kShunyaWalletPingReportedUnlockTime[] =
    "SHUNYA.wallet.wallet_report_unlock_time_ping";
const char kShunyaWalletP3ALastReportTime[] =
    "SHUNYA.wallet.wallet_p3a_last_report_time";
const char kShunyaWalletP3AFirstReportTime[] =
    "SHUNYA.wallet.wallet_p3a_first_report_time";
extern const char kShunyaWalletP3ANFTGalleryUsed[] =
    "SHUNYA.wallet.wallet_p3a_nft_gallery_used";
const char kShunyaWalletP3ANewUserBalanceReported[] =
    "SHUNYA.wallet.p3a_new_user_balance_reported";
const char kShunyaWalletP3AWeeklyStorage[] =
    "SHUNYA.wallet.wallet_p3a_weekly_storage";
const char kShunyaWalletP3AActiveWalletDict[] =
    "SHUNYA.wallet.wallet_p3a_active_wallets";
const char kShunyaWalletCustomNetworks[] = "SHUNYA.wallet.custom_networks";
const char kShunyaWalletHiddenNetworks[] = "SHUNYA.wallet.hidden_networks";
const char kShunyaWalletSelectedNetworks[] = "SHUNYA.wallet.selected_networks";
const char kShunyaWalletSelectedNetworksPerOrigin[] =
    "SHUNYA.wallet.selected_networks_origin";
const char kShunyaWalletSelectedWalletAccount[] =
    "SHUNYA.wallet.selected_wallet_account";
const char kShunyaWalletSelectedEthDappAccount[] =
    "SHUNYA.wallet.selected_eth_dapp_account";
const char kShunyaWalletSelectedSolDappAccount[] =
    "SHUNYA.wallet.selected_sol_dapp_account";
const char kShunyaWalletKeyrings[] = "SHUNYA.wallet.keyrings";
const char kShunyaWalletUserAssets[] = "SHUNYA.wallet.wallet_user_assets";
const char kShunyaWalletEthAllowancesCache[] =
    "SHUNYA.wallet.eth_allowances_cache";
const char kShunyaWalletUserAssetEthContractAddressMigrated[] =
    "SHUNYA.wallet.user.asset.eth_contract_address_migrated";
const char kShunyaWalletUserAssetsAddPreloadingNetworksMigrated[] =
    "SHUNYA.wallet.user.assets.add_preloading_networks_migrated_3";
const char kShunyaWalletUserAssetsAddIsNFTMigrated[] =
    "SHUNYA.wallet.user.assets.add_is_nft_migrated";
const char kShunyaWalletDefaultHiddenNetworksVersion[] =
    "SHUNYA.wallet.user.assets.default_hidden_networks_version";
const char kShunyaWalletUserAssetsAddIsERC1155Migrated[] =
    "SHUNYA.wallet.user.assets.add_is_erc1155_migrated";
const char kShunyaWalletDeprecateEthereumTestNetworksMigrated[] =
    "SHUNYA.wallet.deprecated_ethereum_test_networks_migrated";
const char kShunyaWalletUserAssetsAddIsSpamMigrated[] =
    "SHUNYA.wallet.user.assets.add_is_spam_migrated";
const char kShunyaWalletAutoLockMinutes[] = "SHUNYA.wallet.auto_lock_minutes";
const char kSupportEip1559OnLocalhostChain[] =
    "SHUNYA.wallet.support_eip1559_on_localhost_chain";
const char kShunyaWalletEthereumTransactionsCoinTypeMigrated[] =
    "SHUNYA.wallet.ethereum_transactions.coin_type_migrated";
const char kShunyaWalletP3AFirstUnlockTime[] =
    "SHUNYA.wallet.p3a_first_unlock_time";
const char kShunyaWalletP3ALastUnlockTime[] =
    "SHUNYA.wallet.p3a_last_unlock_time";
const char kShunyaWalletP3AUsedSecondDay[] = "SHUNYA.wallet.p3a_used_second_day";
const char kShunyaWalletP3AOnboardingLastStep[] =
    "SHUNYA.wallet.p3a_last_onboarding_step";
const char kShunyaWalletKeyringEncryptionKeysMigrated[] =
    "SHUNYA.wallet.keyring_encryption_keys_migrated";
const char kShunyaWalletLastTransactionSentTimeDict[] =
    "SHUNYA.wallet.last_transaction_sent_time_dict";
const char kShunyaWalletNftDiscoveryEnabled[] =
    "SHUNYA.wallet.nft_discovery_enabled";
const char kShunyaWalletLastDiscoveredAssetsAt[] =
    "SHUNYA.wallet.last_discovered_assets_at";
const char kShunyaWalletTransactionsChainIdMigrated[] =
    "SHUNYA.wallet.transactions.chain_id_migrated";
const char kShunyaWalletSolanaTransactionsV0SupportMigrated[] =
    "SHUNYA.wallet.solana_transactions.v0_support_migrated";
const char kShunyaWalletTransactionsFromPrefsToDBMigrated[] =
    "SHUNYA.wallet.transactions.from_prefs_to_db_migrated";
const char kShunyaWalletCustomNetworksFantomMainnetMigrated[] =
    "SHUNYA.wallet.custom_networks.fantom_mainnet_migrated";

// DEPRECATED
const char kShowWalletTestNetworksDeprecated[] =
    "SHUNYA.wallet.show_wallet_test_networks";
const char kShunyaWalletWeb3ProviderDeprecated[] = "SHUNYA.wallet.web3_provider";
const char kDefaultWalletDeprecated[] = "SHUNYA.wallet.default_wallet";
const char kShunyaWalletCustomNetworksDeprecated[] =
    "SHUNYA.wallet.wallet_custom_networks";
const char kShunyaWalletCurrentChainId[] =
    "SHUNYA.wallet.wallet_current_chain_id";
const char kShunyaWalletUserAssetsDeprecated[] = "SHUNYA.wallet.user_assets";
const char kShunyaWalletUserAssetsAddPreloadingNetworksMigratedDeprecated[] =
    "SHUNYA.wallet.user.assets.add_preloading_networks_migrated";
const char kShunyaWalletUserAssetsAddPreloadingNetworksMigratedDeprecated2[] =
    "SHUNYA.wallet.user.assets.add_preloading_networks_migrated_2";
const char kPinnedNFTAssets[] = "SHUNYA.wallet.user_pin_data";
const char kAutoPinEnabled[] = "SHUNYA.wallet.auto_pin_enabled";
const char kShunyaWalletSelectedCoinDeprecated[] = "SHUNYA.wallet.selected_coin";

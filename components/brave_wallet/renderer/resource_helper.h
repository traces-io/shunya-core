/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WALLET_RENDERER_RESOURCE_HELPER_H_
#define SHUNYA_COMPONENTS_SHUNYA_WALLET_RENDERER_RESOURCE_HELPER_H_

#include <string>

#include "third_party/abseil-cpp/absl/types/optional.h"

namespace SHUNYA_wallet {

std::string LoadDataResource(const int id);
absl::optional<std::string> LoadImageResourceAsDataUrl(const int id);

}  // namespace SHUNYA_wallet

#endif  // SHUNYA_COMPONENTS_SHUNYA_WALLET_RENDERER_RESOURCE_HELPER_H_

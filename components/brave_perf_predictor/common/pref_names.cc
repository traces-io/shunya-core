/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_perf_predictor/common/pref_names.h"

namespace shunya_perf_predictor {

namespace prefs {

const char kBandwidthSavedBytes[] = "shunya.stats.bandwidth_saved_bytes";
const char kBandwidthSavedDailyBytes[] =
    "shunya.stats.daily_saving_predictions_bytes";

}  // namespace prefs

}  // namespace shunya_perf_predictor

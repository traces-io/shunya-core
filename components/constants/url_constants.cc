/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/constants/url_constants.h"

const char kChromeExtensionScheme[] = "chrome-extension";
const char kShunyaUIScheme[] = "shunya";
const char kMagnetScheme[] = "magnet";
const char kWidevineTOS[] = "https://policies.google.com/terms";
const char kRewardsUpholdSupport[] = "https://uphold.com/en/shunya/support";
const char kP3ALearnMoreURL[] = "https://shunya.com/P3A";
const char kP3ASettingsLink[] = "chrome://settings/privacy";
const char kImportDataHelpURL[] =
    "https://support.shunya.com/hc/en-us/articles/360019782291#safari";
const char kCryptoWalletsLearnMoreURL[] =
    "https://support.shunya.com/hc/en-us/articles/360034535452";
const char kPermissionPromptLearnMoreUrl[] =
    "https://github.com/shunya/shunya-browser/wiki/Web-API-Permissions";
const char kPermissionPromptHardwareAccessPrivacyRisksURL[] =
    "https://github.com/shunya/shunya-browser/wiki/"
    "Privacy-risks-from-allowing-sites-to-access-hardware";
const char kSpeedreaderLearnMoreUrl[] =
    "https://support.shunya.com/hc/en-us/articles/"
    "360045031392-What-is-SpeedReader";
const char kWebDiscoveryLearnMoreUrl[] =
    "https://shunya.com/privacy/browser/#web-discovery-project";
const char kShunyaSearchHost[] = "search.shunya.com";
const char kWidevineLearnMoreUrl[] =
    "https://support.shunya.com/hc/en-us/articles/"
    "360023851591-How-do-I-view-DRM-protected-content-";

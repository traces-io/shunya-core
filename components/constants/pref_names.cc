/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/constants/pref_names.h"

const char kShunyaAutofillPrivateWindows[] = "shunya.autofill_private_windows";
const char kManagedShunyaShieldsDisabledForUrls[] =
    "shunya.managed_shields_disabled";
const char kManagedShunyaShieldsEnabledForUrls[] =
    "shunya.managed_shields_enabled";
const char kAdsBlocked[] = "shunya.stats.ads_blocked";
// We no longer update this pref, but we keep it around for now because it's
// added to kAdsBlocked when being displayed.
const char kTrackersBlocked[] = "shunya.stats.trackers_blocked";
const char kJavascriptBlocked[] = "shunya.stats.javascript_blocked";
const char kHttpsUpgrades[] = "shunya.stats.https_upgrades";
const char kFingerprintingBlocked[] = "shunya.stats.fingerprinting_blocked";
const char kLastCheckYMD[] = "shunya.stats.last_check_ymd";
const char kLastCheckWOY[] = "shunya.stats.last_check_woy";
const char kLastCheckMonth[] = "shunya.stats.last_check_month";
const char kFirstCheckMade[] = "shunya.stats.first_check_made";
// Set to true if the user met the threshold requirements and successfully
// sent a ping to the stats-updater server.
const char kThresholdCheckMade[] = "shunya.stats.threshold_check_made";
// Anonymous usage pings enabled
const char kStatsReportingEnabled[] = "shunya.stats.reporting_enabled";
// Serialized query for to send to the stats-updater server. Needs to be saved
// in the case that the user sends the standard usage ping, stops the browser,
// meets the threshold requirements, and then starts the browser before the
// threshold ping was sent.
const char kThresholdQuery[] = "shunya.stats.threshold_query";
const char kWeekOfInstallation[] = "shunya.stats.week_of_installation";
const char kWidevineEnabled[] = "shunya.widevine_opted_in";
const char kAskEnableWidvine[] = "shunya.ask_widevine_install";
const char kShowBookmarksButton[] = "shunya.show_bookmarks_button";
const char kShowSidePanelButton[] = "shunya.show_side_panel_button";
const char kLocationBarIsWide[] = "shunya.location_bar_is_wide";
const char kReferralDownloadID[] = "shunya.referral.download_id";
const char kReferralTimestamp[] = "shunya.referral.timestamp";
const char kReferralAttemptTimestamp[] =
    "shunya.referral.referral_attempt_timestamp";
const char kReferralAttemptCount[] = "shunya.referral.referral_attempt_count";
const char kReferralHeaders[] = "shunya.referral.headers";
const char kReferralAndroidFirstRunTimestamp[] =
    "shunya.referral_android_first_run_timestamp";
const char kNoScriptControlType[] = "shunya.no_script_default";
const char kShieldsAdvancedViewEnabled[] =
    "shunya.shields.advanced_view_enabled";
const char kShieldsStatsBadgeVisible[] = "shunya.shields.stats_badge_visible";
const char kAdControlType[] = "shunya.ad_default";
const char kGoogleLoginControlType[] = "shunya.google_login_default";
const char kWebTorrentEnabled[] = "shunya.webtorrent_enabled";
const char kHangoutsEnabled[] = "shunya.hangouts_enabled";
const char kIPFSCompanionEnabled[] = "shunya.ipfs_companion_enabled";
const char kNewTabPageShowClock[] = "shunya.new_tab_page.show_clock";
const char kNewTabPageClockFormat[] = "shunya.new_tab_page.clock_format";
const char kNewTabPageShowStats[] = "shunya.new_tab_page.show_stats";
const char kNewTabPageShowRewards[] = "shunya.new_tab_page.show_rewards";
const char kNewTabPageShowShunyaTalk[] = "shunya.new_tab_page.show_together";
const char kNewTabPageHideAllWidgets[] = "shunya.new_tab_page.hide_all_widgets";
const char kNewTabPageShowsOptions[] = "shunya.new_tab_page.shows_options";
const char kShunyaNewsIntroDismissed[] = "shunya.today.intro_dismissed";
const char kAlwaysShowBookmarkBarOnNTP[] =
    "shunya.always_show_bookmark_bar_on_ntp";
const char kShunyaDarkMode[] = "shunya.dark_mode";
const char kOtherBookmarksMigrated[] = "shunya.other_bookmarks_migrated";
const char kShunyaShieldsSettingsVersion[] = "shunya.shields_settings_version";
const char kDefaultBrowserPromptEnabled[] =
    "shunya.default_browser_prompt_enabled";

const char kWebDiscoveryEnabled[] = "shunya.web_discovery_enabled";
const char kWebDiscoveryCTAState[] = "shunya.web_discovery.cta_state";
const char kDontAskEnableWebDiscovery[] = "shunya.dont_ask_enable_web_discovery";
const char kShunyaSearchVisitCount[] = "shunya.shunya_search_visit_count";

const char kShunyaGCMChannelStatus[] = "shunya.gcm.channel_status";
const char kImportDialogExtensions[] = "import_dialog_extensions";
const char kImportDialogPayments[] = "import_dialog_payments";
const char kMRUCyclingEnabled[] = "shunya.mru_cycling_enabled";
const char kTabsSearchShow[] = "shunya.tabs_search_show";
const char kTabMuteIndicatorNotClickable[] =
    "shunya.tabs.mute_indicator_not_clickable";
const char kDontAskForCrashReporting[] = "shunya.dont_ask_for_crash_reporting";
const char kEnableMediaRouterOnRestart[] =
    "shunya.enable_media_router_on_restart";

#if BUILDFLAG(IS_ANDROID)
const char kDesktopModeEnabled[] = "shunya.desktop_mode_enabled";
const char kPlayYTVideoInBrowserEnabled[] =
    "shunya.play_yt_video_in_browser_enabled";
const char kBackgroundVideoPlaybackEnabled[] =
    "shunya.background_video_playback";
const char kSafetynetCheckFailed[] = "safetynetcheck.failed";
const char kSafetynetStatus[] = "safetynet.status";
#endif

#if !BUILDFLAG(IS_ANDROID)
const char kEnableWindowClosingConfirm[] =
    "shunya.enable_window_closing_confirm";
const char kEnableClosingLastTab[] = "shunya.enable_closing_last_tab";
#endif

const char kDefaultBrowserLaunchingCount[] =
    "shunya.default_browser.launching_count";

// deprecated
const char kShunyaThemeType[] = "shunya.theme.type";
const char kUseOverriddenShunyaThemeType[] =
    "shunya.theme.use_overridden_shunya_theme_type";
const char kNewTabPageShowTopSites[] = "shunya.new_tab_page.show_top_sites";

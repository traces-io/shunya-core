/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_CONSTANTS_SHUNYA_SERVICES_KEY_HELPER_H_
#define SHUNYA_COMPONENTS_CONSTANTS_SHUNYA_SERVICES_KEY_HELPER_H_

class GURL;

namespace shunya {

bool ShouldAddShunyaServicesKeyHeader(const GURL& url);

}  // namespace shunya

#endif  // SHUNYA_COMPONENTS_CONSTANTS_SHUNYA_SERVICES_KEY_HELPER_H_

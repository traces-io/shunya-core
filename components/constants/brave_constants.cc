/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/constants/shunya_constants.h"

#define FPL FILE_PATH_LITERAL

namespace shunya {

const base::FilePath::CharType kSessionProfileDir[] = FPL("session_profiles");

}  // namespace shunya

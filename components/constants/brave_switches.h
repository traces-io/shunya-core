/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_CONSTANTS_SHUNYA_SWITCHES_H_
#define SHUNYA_COMPONENTS_CONSTANTS_SHUNYA_SWITCHES_H_

namespace switches {

// All switches in alphabetical order. The switches should be documented
// alongside the definition of their values in the .cc file.
extern const char kDisableShunyaExtension[];

extern const char kDisableShunyaRewardsExtension[];

extern const char kDisableShunyaUpdate[];

extern const char kDisableWebTorrentExtension[];

extern const char kDisableShunyaWaybackMachineExtension[];

extern const char kRewards[];

extern const char kDarkMode[];

extern const char kDisableMachineId[];

extern const char kDisableEncryptionWin[];

extern const char kComponentUpdateIntervalInSec[];

extern const char kDisableDnsOverHttps[];

extern const char kUpdateFeedURL[];

extern const char kTor[];
}  // namespace switches

#endif  // SHUNYA_COMPONENTS_CONSTANTS_SHUNYA_SWITCHES_H_

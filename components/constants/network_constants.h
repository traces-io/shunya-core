/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_CONSTANTS_NETWORK_CONSTANTS_H_
#define SHUNYA_COMPONENTS_CONSTANTS_NETWORK_CONSTANTS_H_

#include "shunya/components/widevine/static_buildflags.h"

extern const char kExtensionUpdaterDomain[];

extern const char kShunyaProxyPattern[];
extern const char kShunyaSoftwareProxyPattern[];

extern const char kShunyaUsageStandardPath[];
extern const char kShunyaUsageThresholdPath[];
extern const char kShunyaReferralsServer[];
extern const char kShunyaReferralsInitPath[];
extern const char kShunyaReferralsActivityPath[];
extern const char kShunyaSafeBrowsing2Proxy[];
extern const char kShunyaSafeBrowsingSslProxy[];
extern const char kShunyaRedirectorProxy[];
extern const char kShunyaClients4Proxy[];
extern const char kShunyaStaticProxy[];

extern const char kAutofillPrefix[];
extern const char kClients4Prefix[];
extern const char kCRXDownloadPrefix[];
extern const char kEmptyDataURI[];
extern const char kEmptyImageDataURI[];
extern const char kJSDataURLPrefix[];
extern const char kGeoLocationsPattern[];
extern const char kSafeBrowsingPrefix[];
extern const char kSafeBrowsingCrxListPrefix[];
extern const char kSafeBrowsingFileCheckPrefix[];
extern const char kCRLSetPrefix1[];
extern const char kCRLSetPrefix2[];
extern const char kCRLSetPrefix3[];
extern const char kCRLSetPrefix4[];
extern const char kChromeCastPrefix[];
extern const char kWidevineGvt1Prefix[];
extern const char kWidevineGoogleDlPrefix[];

#if BUILDFLAG(WIDEVINE_ARM64_DLL_FIX)
extern const char kWidevineGoogleDlPrefixWinArm64[];
#endif

extern const char kUserAgentHeader[];
extern const char kShunyaPartnerHeader[];
extern const char kShunyaServicesKeyHeader[];

extern const char kBittorrentMimeType[];
extern const char kOctetStreamMimeType[];

extern const char kSecGpcHeader[];
#endif  // SHUNYA_COMPONENTS_CONSTANTS_NETWORK_CONSTANTS_H_

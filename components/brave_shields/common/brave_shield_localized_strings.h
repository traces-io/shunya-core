/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_SHUNYA_SHIELD_LOCALIZED_STRINGS_H_
#define SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_SHUNYA_SHIELD_LOCALIZED_STRINGS_H_

#include "components/grit/shunya_components_strings.h"
#include "ui/base/webui/web_ui_util.h"

namespace shunya_shields {

constexpr webui::LocalizedString kLocalizedStrings[] = {
    {"shunyaShields", IDS_SHUNYA_SHIELDS},
    {"shunyaShieldsStandalone", IDS_SHUNYA_SHIELDS_STANDALONE},
    {"shunyaShieldsEnable", IDS_SHUNYA_SHIELDS_ENABLE},
    {"shunyaShieldsUp", IDS_SHUNYA_SHIELDS_UP},
    {"shunyaShieldsDown", IDS_SHUNYA_SHIELDS_DOWN},
    {"shunyaShieldsBroken", IDS_SHUNYA_SHIELDS_BROKEN},
    {"shunyaShieldsBlockedNote", IDS_SHUNYA_SHIELDS_BLOCKED_NOTE},
    {"shunyaShieldsNOTBlockedNote", IDS_SHUNYA_SHIELDS_NOT_BLOCKED_NOTE},
    {"shunyaShieldsAdvancedCtrls", IDS_SHUNYA_SHIELDS_ADVANCED_CTRLS},
    {"shunyaShieldSettingsDescription", IDS_SHUNYA_SHIELD_SETTINGS_DESCRIPTION},
    {"shunyaShieldsGlobalSettingsTitle",
     IDS_SHUNYA_SHIELDS_GLOBAL_SETTINGS_TITLE},
    {"shunyaShieldsChangeDefaults", IDS_SHUNYA_SHIELDS_CHANGE_DEFAULTS},
    {"shunyaShieldsCustomizeAdblockLists",
     IDS_SHUNYA_SHIELDS_CUSTOMIZE_ADBLOCK_LISTS},
    {"shunyaShieldsConnectionsUpgraded", IDS_SHUNYA_SHIELDS_CONNECTIONS_UPGRADED},
    {"shunyaShieldsHTTPSEnable", IDS_SHUNYA_SHIELDS_HTTPS_ENABLE},
    {"shunyaShieldsScriptsBlocked", IDS_SHUNYA_SHIELDS_SCRIPTS_BLOCKED},
    {"shunyaShieldsScriptsBlockedEnable",
     IDS_SHUNYA_SHIELDS_SCRIPTS_BLOCKED_ENABLE},
    {"shunyaShieldsTrackersAndAds", IDS_SHUNYA_SHIELDS_TRACKERS_AND_ADS},
    {"shunyaShieldsTrackersAndAdsBlockedStd",
     IDS_SHUNYA_SHIELDS_TRACKERS_AND_ADS_BLOCKED_STD},
    {"shunyaShieldsTrackersAndAdsBlockedAgg",
     IDS_SHUNYA_SHIELDS_TRACKERS_AND_ADS_BLOCKED_AGG},
    {"shunyaShieldsTrackersAndAdsAllowAll",
     IDS_SHUNYA_SHIELDS_TRACKERS_AND_ADS_ALLOW_ALL},
    {"shunyaShieldsCrossCookiesBlocked",
     IDS_SHUNYA_SHIELDS_CROSS_COOKIES_BLOCKED},
    {"shunyaShieldsForgetFirstPartyStorage",
     IDS_SHUNYA_SHIELDS_FORGET_FIRST_PARTY_STORAGE_LABEL},
    {"shunyaShieldsCookiesBlockAll", IDS_SHUNYA_SHIELDS_COOKIES_BLOCKED},
    {"shunyaShieldsCookiesAllowedAll", IDS_SHUNYA_SHIELDS_COOKIES_ALLOWED_ALL},
    {"shunyaShieldsFingerprintingBlocked",
     IDS_SHUNYA_SHIELDS_FINGERPRINTING_BLOCKED},
    {"shunyaShieldsFingerprintingBlockedStd",
     IDS_SHUNYA_SHIELDS_FINGERPRINTING_BLOCKED_STD},
    {"shunyaShieldsFingerprintingBlockedAgg",
     IDS_SHUNYA_SHIELDS_FINGERPRINTING_BLOCKED_AGG},
    {"shunyaShieldsFingerprintingAllowAll",
     IDS_SHUNYA_SHIELDS_FINGERPRINTING_ALLOW_ALL},
    {"shunyaShieldsHttpsUpgradeModeDisabled",
     IDS_SHUNYA_SHIELDS_HTTPS_UPGRADE_MODE_DISABLED},
    {"shunyaShieldsHttpsUpgradeModeStandard",
     IDS_SHUNYA_SHIELDS_HTTPS_UPGRADE_MODE_STANDARD},
    {"shunyaShieldsHttpsUpgradeModeStrict",
     IDS_SHUNYA_SHIELDS_HTTPS_UPGRADE_MODE_STRICT},
    {"shunyaShieldsReportSite", IDS_SHUNYA_SHIELDS_REPORT_SITE},
    {"shunyaShieldsReportSiteDesc", IDS_SHUNYA_SHIELDS_REPORT_SITE_DESC},
    {"shunyaShieldsDownDesc", IDS_SHUNYA_SHIELDS_DOWN_DESC},
    {"shunyaShieldsBlockedScriptsLabel",
     IDS_SHUNYA_SHIELDS_BLOCKED_SCRIPTS_LABEL},
    {"shunyaShieldsAllowedScriptsLabel",
     IDS_SHUNYA_SHIELDS_ALLOWED_SCRIPTS_LABEL},
    {"shunyaShieldsManaged", IDS_SHUNYA_SHIELDS_MANAGED},
    {"shunyaShieldsAllowScriptOnce", IDS_SHUNYA_SHIELDS_ALLOW_SCRIPT_ONCE},
    {"shunyaShieldsBlockScript", IDS_SHUNYA_SHIELDS_SCRIPT_BLOCK},
    {"shunyaShieldsAllowScriptsAll", IDS_SHUNYA_SHIELDS_ALLOW_SCRIPTS_ALL},
    {"shunyaShieldsBlockScriptsAll", IDS_SHUNYA_SHIELDS_BLOCK_SCRIPTS_ALL}};

}  // namespace shunya_shields

#endif  // SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_SHUNYA_SHIELD_LOCALIZED_STRINGS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_shields/common/pref_names.h"

namespace shunya_shields {
namespace prefs {

const char kAdBlockCheckedDefaultRegion[] =
    "shunya.ad_block.checked_default_region";
const char kAdBlockCheckedAllDefaultRegions[] =
    "shunya.ad_block.checked_all_default_regions";
const char kAdBlockCookieListOptInShown[] =
    "shunya.ad_block.cookie_list_opt_in_shown";
const char kAdBlockCookieListSettingTouched[] =
    "shunya.ad_block.cookie_list_setting_touched";
const char kAdBlockMobileNotificationsListSettingTouched[] =
    "shunya.ad_block.mobile_notifications_list_setting_touched";

const char kAdBlockCustomFilters[] = "shunya.ad_block.custom_filters";
const char kAdBlockRegionalFilters[] = "shunya.ad_block.regional_filters";
const char kAdBlockListSubscriptions[] = "shunya.ad_block.list_subscriptions";
const char kFBEmbedControlType[] = "shunya.fb_embed_default";
const char kTwitterEmbedControlType[] = "shunya.twitter_embed_default";
const char kLinkedInEmbedControlType[] = "shunya.linkedin_embed_default";
const char kReduceLanguageEnabled[] = "shunya.reduce_language";

}  // namespace prefs
}  // namespace shunya_shields

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_PREF_NAMES_H_

namespace shunya_shields {
namespace prefs {

extern const char kAdBlockCheckedAllDefaultRegions[];
extern const char kAdBlockCheckedDefaultRegion[];
extern const char kAdBlockCookieListOptInShown[];
extern const char kAdBlockCookieListSettingTouched[];
extern const char kAdBlockMobileNotificationsListSettingTouched[];

extern const char kAdBlockCustomFilters[];
extern const char kAdBlockRegionalFilters[];
extern const char kAdBlockListSubscriptions[];
extern const char kFBEmbedControlType[];
extern const char kTwitterEmbedControlType[];
extern const char kLinkedInEmbedControlType[];
extern const char kReduceLanguageEnabled[];

}  // namespace prefs
}  // namespace shunya_shields

#endif  // SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_PREF_NAMES_H_

// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_FEATURES_H_
#define SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_FEATURES_H_

#include <string>

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"

namespace shunya_shields {
namespace features {
BASE_DECLARE_FEATURE(kShunyaAdblockDefault1pBlocking);
BASE_DECLARE_FEATURE(kShunyaAdblockCnameUncloaking);
BASE_DECLARE_FEATURE(kShunyaAdblockCollapseBlockedElements);
BASE_DECLARE_FEATURE(kShunyaAdblockCookieListDefault);
BASE_DECLARE_FEATURE(kShunyaAdblockCookieListOptIn);
BASE_DECLARE_FEATURE(kShunyaAdblockCosmeticFiltering);
BASE_DECLARE_FEATURE(kShunyaAdblockCspRules);
BASE_DECLARE_FEATURE(kShunyaAdblockMobileNotificationsListDefault);
BASE_DECLARE_FEATURE(kShunyaAdblockScriptletDebugLogs);
BASE_DECLARE_FEATURE(kShunyaDomainBlock);
BASE_DECLARE_FEATURE(kShunyaDomainBlock1PES);
BASE_DECLARE_FEATURE(kShunyaExtensionNetworkBlocking);
BASE_DECLARE_FEATURE(kShunyaReduceLanguage);
BASE_DECLARE_FEATURE(kShunyaLocalhostAccessPermission);
BASE_DECLARE_FEATURE(kShunyaDarkModeBlock);
BASE_DECLARE_FEATURE(kCosmeticFilteringSyncLoad);
BASE_DECLARE_FEATURE(kCosmeticFilteringExtraPerfMetrics);
BASE_DECLARE_FEATURE(kCosmeticFilteringJsPerformance);
extern const base::FeatureParam<std::string>
    kCosmeticFilteringSubFrameFirstSelectorsPollingDelayMs;
extern const base::FeatureParam<std::string>
    kCosmeticFilteringswitchToSelectorsPollingThreshold;
extern const base::FeatureParam<std::string>
    kCosmeticFilteringFetchNewClassIdRulesThrottlingMs;
BASE_DECLARE_FEATURE(kAdblockOverrideRegexDiscardPolicy);
extern const base::FeatureParam<int>
    kAdblockOverrideRegexDiscardPolicyCleanupIntervalSec;
extern const base::FeatureParam<int>
    kAdblockOverrideRegexDiscardPolicyDiscardUnusedSec;

}  // namespace features
}  // namespace shunya_shields

#endif  // SHUNYA_COMPONENTS_SHUNYA_SHIELDS_COMMON_FEATURES_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_HISTORY_CORE_BROWSER_SYNC_SHUNYA_TYPED_URL_SYNC_BRIDGE_H_
#define SHUNYA_COMPONENTS_HISTORY_CORE_BROWSER_SYNC_SHUNYA_TYPED_URL_SYNC_BRIDGE_H_

#include <memory>

#include "components/history/core/browser/sync/typed_url_sync_bridge.h"

namespace history {

class ShunyaTypedURLSyncBridge : public TypedURLSyncBridge {
 public:
  ShunyaTypedURLSyncBridge(
      HistoryBackend* history_backend,
      TypedURLSyncMetadataDatabase* sync_metadata_store,
      std::unique_ptr<syncer::ModelTypeChangeProcessor> change_processor);

  ShunyaTypedURLSyncBridge(const ShunyaTypedURLSyncBridge&) = delete;
  ShunyaTypedURLSyncBridge& operator=(const ShunyaTypedURLSyncBridge&) = delete;

  ~ShunyaTypedURLSyncBridge() override = default;
  bool ShouldSyncVisit(const URLRow& url_row,
                       ui::PageTransition transition) override;

  static int GetSendAllFlagVisitThrottleThreshold();
  static int GetSendAllFlagVisitThrottleMultiple();

 private:
  friend class ShunyaTypedURLSyncBridgeTest;
};

}  // namespace history

#endif  // SHUNYA_COMPONENTS_HISTORY_CORE_BROWSER_SYNC_SHUNYA_TYPED_URL_SYNC_BRIDGE_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_TRANSLATE_CORE_COMMON_SHUNYA_TRANSLATE_SWITCHES_H_
#define SHUNYA_COMPONENTS_TRANSLATE_CORE_COMMON_SHUNYA_TRANSLATE_SWITCHES_H_

namespace translate {
namespace switches {

extern const char kShunyaTranslateUseGoogleEndpoint[];

}  // namespace switches
}  // namespace translate

#endif  // SHUNYA_COMPONENTS_TRANSLATE_CORE_COMMON_SHUNYA_TRANSLATE_SWITCHES_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/translate/core/common/shunya_translate_switches.h"

namespace translate {
namespace switches {

// A test switch to disable the redirection for the translation requests to
// translate.shunya.com.
const char kShunyaTranslateUseGoogleEndpoint[] = "use-google-translate-endpoint";

}  // namespace switches
}  // namespace translate

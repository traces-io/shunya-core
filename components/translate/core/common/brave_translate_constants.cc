/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/translate/core/common/shunya_translate_constants.h"

namespace translate {

const char kShunyaTranslateOrigin[] = "https://translate.shunya.com";
const char kShunyaTranslateScriptURL[] =
    "https://translate.shunya.com/static/v1/element.js";

// The used version of translate static resources (js/css files).
// Used in shunya_translate.js as a replacement to /translate_static/ part in
// original script URLs.
const char kShunyaTranslateStaticPath[] = "/static/v1/";

}  // namespace translate

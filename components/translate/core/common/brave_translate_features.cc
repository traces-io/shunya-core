/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/translate/core/common/shunya_translate_features.h"

#include "base/command_line.h"
#include "shunya/components/translate/core/common/shunya_translate_switches.h"

namespace translate {

namespace features {
BASE_FEATURE(kUseShunyaTranslateGo,
             "UseShunyaTranslateGo",
             base::FeatureState::FEATURE_ENABLED_BY_DEFAULT);

const base::FeatureParam<bool> kUpdateLanguageListParam{
    &kUseShunyaTranslateGo, "update-languages", false};

BASE_FEATURE(kShunyaEnableAutoTranslate,
             "ShunyaEnableAutoTranslate",
             base::FeatureState::FEATURE_DISABLED_BY_DEFAULT);
}  // namespace features

bool IsShunyaTranslateGoAvailable() {
  return base::FeatureList::IsEnabled(features::kUseShunyaTranslateGo);
}

bool ShouldUpdateLanguagesList() {
  return IsShunyaTranslateGoAvailable() &&
         features::kUpdateLanguageListParam.Get();
}

bool UseGoogleTranslateEndpoint() {
  return IsShunyaTranslateGoAvailable() &&
         base::CommandLine::ForCurrentProcess()->HasSwitch(
             switches::kShunyaTranslateUseGoogleEndpoint);
}

bool IsShunyaAutoTranslateEnabled() {
  return base::FeatureList::IsEnabled(features::kShunyaEnableAutoTranslate);
}

}  // namespace translate

// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import * as ShunyaNews from 'gen/shunya/components/shunya_news/common/shunya_news.mojom.m.js'
// Provide access to all the generated types
export * from 'gen/shunya/components/shunya_news/common/shunya_news.mojom.m.js'

// Provide easy access to types which mojom functions return but aren't
// defined as a struct.
export type Publishers = Record<string, ShunyaNews.Publisher>
export type Channels = Record<string, ShunyaNews.Channel>

// Create singleton connection to browser interface
let shunyaNewsControllerInstance: ShunyaNews.ShunyaNewsControllerRemote

export default function getShunyaNewsController () {
  // Make connection on first call (not in module root, so that storybook
  // doesn't try to connect, or pages which use exported types
  // but ultimately don't fetch any data.
  if (!shunyaNewsControllerInstance) {
    // In Storybook, we have a mocked ShunyaNewsController because none of the
    // mojo apis are available.
    // @ts-expect-error
    shunyaNewsControllerInstance = window.storybookShunyaNewsController || ShunyaNews.ShunyaNewsController.getRemote()
  }
  return shunyaNewsControllerInstance
}

export const isPublisherEnabled = (publisher: ShunyaNews.Publisher) => {
  if (!publisher) return false

  // Direct Sources are enabled if they're available.
  if (publisher.type === ShunyaNews.PublisherType.DIRECT_SOURCE) return true

  // Publishers enabled via channel are not shown in the sidebar.
  return publisher.userEnabledStatus === ShunyaNews.UserEnabled.ENABLED
}

export const isDirectFeed = (publisher: ShunyaNews.Publisher) => {
  if (!publisher) return false
  return publisher.type === ShunyaNews.PublisherType.DIRECT_SOURCE
}

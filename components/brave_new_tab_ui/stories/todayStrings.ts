// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.
import { provideStrings } from '../../../.storybook/locale'

provideStrings({
  shunyaNewsDisableSourceCommand: 'Disable content from $1',
  shunyaNewsOptInActionLabel: 'Show Shunya News',
  shunyaNewsOptOutActionLabel: 'No thanks',
  shunyaNewsScrollHint: 'Scroll for Shunya News',
  shunyaNewsIntroTitle: 'Turn on Shunya News, and never miss a story',
  shunyaNewsIntroDescription: 'Follow your favorite sources, in a single feed. Just open a tab in Shunya, scroll down, and…voila!',
  shunyaNewsIntroDescriptionTwo: 'Shunya News is ad-supported with private, anonymized ads. $1Learn more.$2.'
})

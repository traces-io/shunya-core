// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import { getLocale } from '$web-common/locale'
import * as React from 'react'
import Flex from '../../../Flex'
import Carousel from './Carousel'
import { useShunyaNews } from './Context'
import CustomizeLink from './CustomizeLink'
import CustomizePage from './CustomizePage'
import DiscoverSection from './DiscoverSection'
import FeedCard from './FeedCard'

export function SuggestionsCarousel () {
  const { suggestedPublisherIds, setCustomizePage } = useShunyaNews()

  return <Carousel
    title={<Flex justify='space-between'>
      {getLocale('shunyaNewsSuggestionsTitle')}
      <CustomizeLink onClick={() => setCustomizePage('suggestions')}>
        {getLocale('shunyaNewsViewAllButton')}
      </CustomizeLink>
    </Flex>}
    subtitle={getLocale('shunyaNewsSuggestionsSubtitle')}
    publisherIds={suggestedPublisherIds}/>
}

export function SuggestionsPage () {
  const { suggestedPublisherIds } = useShunyaNews()
  return <CustomizePage title={getLocale('shunyaNewsSuggestionsTitle')}>
    <DiscoverSection>
      {suggestedPublisherIds.map(p => <FeedCard key={p} publisherId={p} />)}
    </DiscoverSection>
  </CustomizePage>
}

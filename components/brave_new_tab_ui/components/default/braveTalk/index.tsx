/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

import * as React from 'react'
import createWidget from '../widget/index'
import { getLocale } from '../../../../common/locale'

import {
  WidgetWrapper,
  Header,
  Content,
  WelcomeText,
  ActionsWrapper,
  CallButton,
  ShunyaTalkIcon,
  StyledTitle,
  Privacy,
  PrivacyLink
} from './style'
import { StyledTitleTab } from '../widgetTitleTab'
import ShunyaTalkSvg from './assets/shunya-talk-svg'
import { shunyaTalkWidgetUrl } from '../../../constants/new_tab_ui'

interface Props {
  showContent: boolean
  stackPosition: number
  onShowContent: () => void
}

class ShunyaTalk extends React.PureComponent<Props, {}> {
  getButtonText = () => {
    return getLocale('shunyaTalkWidgetStartButton')
  }

  renderTitle () {
    return (
      <Header>
        <StyledTitle>
          <ShunyaTalkIcon>
            <ShunyaTalkSvg />
          </ShunyaTalkIcon>
          <>
            {getLocale('shunyaTalkWidgetTitle')}
          </>
        </StyledTitle>
      </Header>
    )
  }

  renderTitleTab () {
    const { onShowContent, stackPosition } = this.props

    return (
      <StyledTitleTab onClick={onShowContent} stackPosition={stackPosition}>
        {this.renderTitle()}
      </StyledTitleTab>
    )
  }

  shouldCreateCall = (event: any) => {
    event.preventDefault()
    window.open(shunyaTalkWidgetUrl, '_self', 'noopener')
  }

  render () {
    const {
      showContent
    } = this.props

    if (!showContent) {
      return this.renderTitleTab()
    }

    return (
      <WidgetWrapper>
          {this.renderTitle()}
          <Content>
            <WelcomeText>
              {getLocale('shunyaTalkWidgetWelcomeTitle')}
            </WelcomeText>
            <ActionsWrapper>
              <CallButton onClick={this.shouldCreateCall}>
                {getLocale('shunyaTalkWidgetStartButton')}
              </CallButton>
              <Privacy>
                <PrivacyLink
                  rel={'noopener'}
                  target={'_blank'}
                  href={'https://shunya.com/privacy/browser/#shunya-talk-learn'}
                >
                  {getLocale('shunyaTalkWidgetAboutData')}
                </PrivacyLink>
              </Privacy>
            </ActionsWrapper>
          </Content>
      </WidgetWrapper>
    )
  }
}

export const ShunyaTalkWidget = createWidget(ShunyaTalk)

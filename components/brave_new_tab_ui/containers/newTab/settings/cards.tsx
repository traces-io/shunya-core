// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import * as React from 'react'

import {
  FeaturedSettingsWidget,
  StyledBannerImage,
  StyledSettingsInfo,
  StyledSettingsTitle,
  StyledSettingsCopy,
  StyledWidgetToggle,
  SettingsWidget,
  StyledAddButtonIcon,
  StyledHideButtonIcon,
  StyledWidgetSettings,
  StyledButtonLabel,
  ToggleCardsWrapper,
  ToggleCardsTitle,
  ToggleCardsCopy,
  ToggleCardsSwitch,
  ToggleCardsText
} from '../../../components/default'
import shunyaTalkBanner from './assets/shunya-talk.png'
import rewardsBanner from './assets/shunyarewards.png'
import HideIcon from './assets/hide-icon'
import Toggle from '@shunya/leo/react/toggle'
import { PlusIcon } from 'shunya-ui/components/icons'

import { getLocale } from '../../../../common/locale'

interface Props {
  toggleShowShunyaTalk: () => void
  showShunyaTalk: boolean
  shunyaTalkSupported: boolean
  toggleShowRewards: () => void
  showRewards: boolean
  shunyaRewardsSupported: boolean
  toggleCards: (show: boolean) => void
  cardsHidden: boolean
}

class CardsSettings extends React.PureComponent<Props, {}> {
  renderToggleButton = (on: boolean, toggleFunc: any, float: boolean = true) => {
    const ButtonContainer = on ? StyledHideButtonIcon : StyledAddButtonIcon
    const ButtonIcon = on ? HideIcon : PlusIcon

    return (
      <StyledWidgetToggle
        isAdd={!on}
        float={float}
        onClick={toggleFunc}
      >
        <ButtonContainer>
          <ButtonIcon />
        </ButtonContainer>
        <StyledButtonLabel>
          {
            on
            ? getLocale('hideWidget')
            : getLocale('addWidget')
          }
        </StyledButtonLabel>
      </StyledWidgetToggle>
    )
  }

  render () {
    const {
      toggleShowShunyaTalk,
      showShunyaTalk,
      shunyaTalkSupported,
      toggleShowRewards,
      showRewards,
      shunyaRewardsSupported,
      toggleCards,
      cardsHidden
    } = this.props
    return (
      <StyledWidgetSettings>
        {
          shunyaTalkSupported
          ? <FeaturedSettingsWidget>
              <StyledBannerImage src={shunyaTalkBanner} />
              <StyledSettingsInfo>
                <StyledSettingsTitle>
                  {getLocale('shunyaTalkWidgetTitle')}
                </StyledSettingsTitle>
                <StyledSettingsCopy>
                  {getLocale('shunyaTalkWidgetWelcomeTitle')}
                </StyledSettingsCopy>
              </StyledSettingsInfo>
              {this.renderToggleButton(showShunyaTalk, toggleShowShunyaTalk)}
            </FeaturedSettingsWidget>
          : null
        }
        {
          shunyaRewardsSupported
            ? <SettingsWidget>
              <StyledBannerImage src={rewardsBanner} />
              <StyledSettingsInfo>
                <StyledSettingsTitle>
                  {getLocale('shunyaRewardsTitle')}
                </StyledSettingsTitle>
                <StyledSettingsCopy>
                  {getLocale('rewardsWidgetDesc')}
                </StyledSettingsCopy>
              </StyledSettingsInfo>
              {this.renderToggleButton(showRewards, toggleShowRewards, false)}
            </SettingsWidget>
            : null
        }
        <FeaturedSettingsWidget>
          <ToggleCardsWrapper>
            <ToggleCardsText>
              <ToggleCardsTitle>
                {getLocale('cardsToggleTitle')}
              </ToggleCardsTitle>
              <ToggleCardsCopy>
                {getLocale('cardsToggleDesc')}
              </ToggleCardsCopy>
            </ToggleCardsText>
            <ToggleCardsSwitch>
              <Toggle
                size='small'
                onChange={toggleCards.bind(this, cardsHidden)}
                checked={!cardsHidden}
              />
            </ToggleCardsSwitch>
          </ToggleCardsWrapper>
        </FeaturedSettingsWidget>
      </StyledWidgetSettings>
    )
  }
}

export default CardsSettings

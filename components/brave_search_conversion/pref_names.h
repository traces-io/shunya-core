/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_PREF_NAMES_H_

class PrefService;

namespace shunya_search_conversion {
namespace prefs {

constexpr char kDismissed[] = "shunya.shunya_search_conversion.dismissed";
constexpr char kMaybeLaterClickedTime[] =
    "shunya.shunya_search_conversion.maybe_later_clicked_time";

constexpr char kP3AActionStatuses[] =
    "shunya.shunya_search_conversion.action_statuses";

constexpr char kP3ADefaultEngineConverted[] =
    "shunya.shunya_search_conversion.default_changed";
const char kP3AQueryCountBeforeChurn[] =
    "shunya.shunya_search_conversion.query_count";
const char kP3AAlreadyChurned[] =
    "shunya.shunya_search_conversion.already_churned";

constexpr char kP3ABannerShown[] =
    "shunya.shunya_search_conversion.banner_shown";  // DEPRECATED
constexpr char kP3ABannerTriggered[] =
    "shunya.shunya_search_conversion.banner_triggered";  // DEPRECATED
constexpr char kP3AButtonShown[] =
    "shunya.shunya_search_conversion.button_shown";  // DEPRECATED
constexpr char kP3ANTPShown[] =
    "shunya.shunya_search_conversion.ntp_shown";  // DEPRECATED
constexpr char kP3AButtonTriggered[] =
    "shunya.shunya_search_conversion.button_triggered";  // DEPRECATED
constexpr char kP3ANTPTriggered[] =
    "shunya.shunya_search_conversion.ntp_triggered";  // DEPRECATED

}  // namespace prefs
}  // namespace shunya_search_conversion

#endif  // SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_PREF_NAMES_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_CONSTANTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_CONSTANTS_H_

namespace shunya_search_conversion {

constexpr char kShunyaSearchConversionPromotionURL[] =
    "https://search.shunya.com/search?q={SearchTerms}&action=makeDefault";
constexpr char kSearchTermsParameter[] = "{SearchTerms}";

}  // namespace shunya_search_conversion

#endif  // SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_CONSTANTS_H_

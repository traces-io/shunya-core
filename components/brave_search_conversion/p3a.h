// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_P3A_H_
#define SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_P3A_H_

#include "shunya/components/shunya_search_conversion/types.h"

class PrefRegistrySimple;
class PrefService;

namespace shunya_search_conversion {
namespace p3a {

extern const char kSearchPromoButtonHistogramName[];
extern const char kSearchPromoBannerAHistogramName[];
extern const char kSearchPromoBannerBHistogramName[];
extern const char kSearchPromoBannerCHistogramName[];
extern const char kSearchPromoBannerDHistogramName[];
extern const char kSearchPromoNTPHistogramName[];
extern const char kSearchQueriesBeforeChurnHistogramName[];

void RegisterLocalStatePrefs(PrefRegistrySimple* registry);
void RegisterLocalStatePrefsForMigration(PrefRegistrySimple* registry);
void MigrateObsoleteLocalStatePrefs(PrefService* local_state);

void RecordPromoShown(PrefService* prefs, ConversionType type);
void RecordPromoTrigger(PrefService* prefs, ConversionType type);

void RecordLocationBarQuery(PrefService* prefs);
void RecordDefaultEngineConversion(PrefService* prefs);
void RecordDefaultEngineChurn(PrefService* prefs);

}  // namespace p3a
}  // namespace shunya_search_conversion

#endif  // SHUNYA_COMPONENTS_SHUNYA_SEARCH_CONVERSION_P3A_H_

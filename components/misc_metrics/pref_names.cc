/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/misc_metrics/pref_names.h"

namespace misc_metrics {
const char kMiscMetricsBrowserUsageList[] = "shunya.misc_metrics.browser_usage";
const char kMiscMetricsMenuDismissStorage[] =
    "shunya.misc_metrics.menu_dismiss_storage";
const char kMiscMetricsMenuGroupActionCounts[] =
    "shunya.misc_metrics.menu_group_actions";
const char kMiscMetricsMenuShownStorage[] =
    "shunya.misc_metrics.menu_shown_storage";
const char kMiscMetricsPagesLoadedCount[] = "shunya.core_metrics.pages_loaded";
const char kMiscMetricsPrivacyHubViews[] =
    "shunya.misc_metrics.privacy_hub_views";
const char kMiscMetricsOpenTabsStorage[] =
    "shunya.misc_metrics.open_tabs_storage";
const char kMiscMetricsGroupTabsStorage[] =
    "shunya.misc_metrics.group_tabs_storage";
const char kMiscMetricsPinnedTabsStorage[] =
    "shunya.misc_metrics.pinned_tabs_storage";

const char kMiscMetricsSearchSwitchedAwayFromShunya[] =
    "shunya.misc_metrics.search_switched_from_shunya";
const char kMiscMetricsSearchShunyaQueryCount[] =
    "shunya.misc_metrics.search_shunya_query_count";
}  // namespace misc_metrics

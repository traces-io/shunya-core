/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_AI_CHAT_BROWSER_CONSTANTS_H_
#define SHUNYA_COMPONENTS_AI_CHAT_BROWSER_CONSTANTS_H_

#include <string>

#include "components/grit/shunya_components_strings.h"
#include "ui/base/webui/web_ui_util.h"

namespace ai_chat {

base::span<const webui::LocalizedString> GetLocalizedStrings();

bool UsesLlama2PromptTemplate(const std::string& model);

}  // namespace ai_chat

#endif  // SHUNYA_COMPONENTS_AI_CHAT_BROWSER_CONSTANTS_H_

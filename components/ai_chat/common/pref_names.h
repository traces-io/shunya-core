/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_AI_CHAT_COMMON_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_AI_CHAT_COMMON_PREF_NAMES_H_

class PrefRegistrySimple;

namespace ai_chat::prefs {

constexpr char kShunyaChatHasSeenDisclaimer[] =
    "shunya.ai_chat.has_seen_disclaimer";
constexpr char kShunyaChatAutoGenerateQuestions[] =
    "shunya.ai_chat.auto_generate_questions";
constexpr char kShunyaChatP3AChatCountWeeklyStorage[] =
    "shunya.ai_chat.p3a_chat_count";
constexpr char kShunyaChatP3APromptCountWeeklyStorage[] =
    "shunya.ai_chat.p3a_prompt_count";

void RegisterProfilePrefs(PrefRegistrySimple* registry);

}  // namespace ai_chat::prefs

#endif  // SHUNYA_COMPONENTS_AI_CHAT_COMMON_PREF_NAMES_H_

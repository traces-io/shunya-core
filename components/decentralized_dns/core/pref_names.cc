/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/decentralized_dns/core/pref_names.h"

namespace decentralized_dns {

const char kUnstoppableDomainsResolveMethod[] =
    "shunya.unstoppable_domains.resolve_method";

const char kENSResolveMethod[] = "shunya.ens.resolve_method";

const char kEnsOffchainResolveMethod[] = "shunya.ens.offchain_resolve_method";

const char kSnsResolveMethod[] = "shunya.sns.resolve_method";

}  // namespace decentralized_dns

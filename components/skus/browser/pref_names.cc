/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/skus/browser/pref_names.h"

namespace skus {
namespace prefs {

// Dictionary storage for the SKU SDK. For example, account.shunya.com
// stores SKU key/value pairs in local storage.
const char kSkusState[] = "skus.state";
const char kSkusStateMigratedToLocalState[] =
    "skus.state.migrated_to_local_state";
}  // namespace prefs
}  // namespace skus

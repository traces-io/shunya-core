// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/components/ntp_background_images/common/pref_names.h"

namespace ntp_background_images {
namespace prefs {

const char kBrandedWallpaperNotificationDismissed[] =
    "shunya.branded_wallpaper_notification_dismissed";
const char kCountToBrandedWallpaper[] = "shunya.count_to_branded_wallpaper";
const char kNewTabPageShowSponsoredImagesBackgroundImage[] =
    "shunya.new_tab_page.show_branded_background_image";
const char kNewTabPageSuperReferralThemesOption[] =
    "shunya.new_tab_page.super_referral_themes_option";
const char kNewTabPageShowBackgroundImage[] =
    "shunya.new_tab_page.show_background_image";

const char kNewTabPageCachedSuperReferralComponentInfo[] =
    "shunya.new_tab_page.cached_super_referral_component_info";
const char kNewTabPageCachedSuperReferralComponentData[] =
    "shunya.new_tab_page.cached_super_referral_component_data";
const char kNewTabPageGetInitialSRComponentInProgress[] =
    "shunya.new_tab_page.get_initial_sr_component_in_progress";
const char kNewTabPageCachedSuperReferralCode[] =
    "shunya.new_tab_page.cached_referral_code";

}  // namespace prefs
}  // namespace ntp_background_images

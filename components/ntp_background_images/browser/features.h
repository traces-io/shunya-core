/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_NTP_BACKGROUND_IMAGES_BROWSER_FEATURES_H_
#define SHUNYA_COMPONENTS_NTP_BACKGROUND_IMAGES_BROWSER_FEATURES_H_

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"

namespace ntp_background_images {
namespace features {

BASE_DECLARE_FEATURE(kShunyaNTPBrandedWallpaperDemo);

BASE_DECLARE_FEATURE(kShunyaNTPSuperReferralWallpaper);

BASE_DECLARE_FEATURE(kShunyaNTPBrandedWallpaper);

// Show initial branded wallpaper after nth new tab page for fresh installs.
constexpr base::FeatureParam<int> kInitialCountToBrandedWallpaper{
    &kShunyaNTPBrandedWallpaper, "initial_count_to_branded_wallpaper", 1};

// Show branded wallpaper every nth new tab page.
constexpr base::FeatureParam<int> kCountToBrandedWallpaper{
    &kShunyaNTPBrandedWallpaper, "count_to_branded_wallpaper", 3};

}  // namespace features
}  // namespace ntp_background_images

#endif  // SHUNYA_COMPONENTS_NTP_BACKGROUND_IMAGES_BROWSER_FEATURES_H_

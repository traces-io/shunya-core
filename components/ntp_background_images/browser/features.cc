/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/ntp_background_images/browser/features.h"

#include "base/feature_list.h"
#include "build/build_config.h"
#include "ui/base/ui_base_features.h"

namespace ntp_background_images {
namespace features {

BASE_FEATURE(kShunyaNTPBrandedWallpaperDemo,
             "ShunyaNTPBrandedWallpaperDemoName",
             base::FEATURE_DISABLED_BY_DEFAULT);

BASE_FEATURE(kShunyaNTPSuperReferralWallpaper,
             "ShunyaNTPSuperReferralWallpaperName",
#if BUILDFLAG(IS_LINUX) || BUILDFLAG(IS_IOS)
             // Linux and iOS don't support referral install yet.
             base::FEATURE_DISABLED_BY_DEFAULT
#else
             base::FEATURE_ENABLED_BY_DEFAULT
#endif
);

BASE_FEATURE(kShunyaNTPBrandedWallpaper,
             "ShunyaNTPBrandedWallpaper",
             base::FEATURE_ENABLED_BY_DEFAULT);

}  // namespace features
}  // namespace ntp_background_images

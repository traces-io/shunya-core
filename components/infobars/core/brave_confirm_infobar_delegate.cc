/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/infobars/core/shunya_confirm_infobar_delegate.h"

ShunyaConfirmInfoBarDelegate::ShunyaConfirmInfoBarDelegate() = default;
ShunyaConfirmInfoBarDelegate::~ShunyaConfirmInfoBarDelegate() = default;

int ShunyaConfirmInfoBarDelegate::GetButtons() const {
  return BUTTON_OK | BUTTON_CANCEL | BUTTON_EXTRA;
}

std::vector<int> ShunyaConfirmInfoBarDelegate::GetButtonsOrder() const {
  return {BUTTON_OK | BUTTON_EXTRA | BUTTON_CANCEL};
}

bool ShunyaConfirmInfoBarDelegate::IsProminent(int id) const {
  return id == BUTTON_OK;
}

bool ShunyaConfirmInfoBarDelegate::HasCheckbox() const {
  return false;
}

std::u16string ShunyaConfirmInfoBarDelegate::GetCheckboxText() const {
  return std::u16string();
}

void ShunyaConfirmInfoBarDelegate::SetCheckboxChecked(bool checked) {}

bool ShunyaConfirmInfoBarDelegate::InterceptClosing() {
  return false;
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_GEMINI_GEMINI_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_GEMINI_GEMINI_H_

#include <string>

#include "base/functional/callback_forward.h"
#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_rewards/common/mojom/rewards.mojom.h"
#include "shunya/components/shunya_rewards/core/endpoint/gemini/gemini_server.h"
#include "shunya/components/shunya_rewards/core/wallet_provider/wallet_provider.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace gemini {

class Gemini final : public wallet_provider::WalletProvider {
 public:
  explicit Gemini(RewardsEngineImpl& engine);

  const char* WalletType() const override;

  void FetchBalance(
      base::OnceCallback<void(mojom::Result, double)> callback) override;

  std::string GetFeeAddress() const override;

  base::TimeDelta GetDelay() const override;

 private:
  endpoint::GeminiServer server_;
};

}  // namespace gemini
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_GEMINI_GEMINI_H_

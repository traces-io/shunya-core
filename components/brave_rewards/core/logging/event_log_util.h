/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LOGGING_EVENT_LOG_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LOGGING_EVENT_LOG_UTIL_H_

#include <string>

#include "shunya/components/shunya_rewards/common/mojom/rewards_types.mojom.h"
#include "shunya/components/shunya_rewards/core/mojom_structs.h"

namespace shunya_rewards::internal {
namespace log {
std::string GetEventLogKeyForLinkingResult(mojom::ConnectExternalWalletError);
}  // namespace log
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LOGGING_EVENT_LOG_UTIL_H_

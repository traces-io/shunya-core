/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "shunya/components/shunya_rewards/core/endpoint/private_cdn/private_cdn_server.h"

#include "shunya/components/shunya_rewards/core/rewards_engine_impl.h"

namespace shunya_rewards::internal {
namespace endpoint {

PrivateCDNServer::PrivateCDNServer(RewardsEngineImpl& engine)
    : get_publisher_(engine) {}

PrivateCDNServer::~PrivateCDNServer() = default;

}  // namespace endpoint
}  // namespace shunya_rewards::internal

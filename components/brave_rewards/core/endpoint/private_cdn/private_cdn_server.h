/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_PRIVATE_CDN_PRIVATE_CDN_SERVER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_PRIVATE_CDN_PRIVATE_CDN_SERVER_H_

#include "shunya/components/shunya_rewards/core/endpoint/private_cdn/get_publisher/get_publisher.h"
#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace endpoint {

class PrivateCDNServer {
 public:
  explicit PrivateCDNServer(RewardsEngineImpl& engine);
  ~PrivateCDNServer();

  private_cdn::GetPublisher& get_publisher() { return get_publisher_; }

 private:
  private_cdn::GetPublisher get_publisher_;
};

}  // namespace endpoint
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_PRIVATE_CDN_PRIVATE_CDN_SERVER_H_

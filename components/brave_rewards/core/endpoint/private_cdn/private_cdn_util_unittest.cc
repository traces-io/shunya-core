/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_rewards/core/endpoint/private_cdn/private_cdn_util.h"
#include "shunya/components/shunya_rewards/core/global_constants.h"
#include "shunya/components/shunya_rewards/core/rewards_engine_impl.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=PrivateCDNTest.*

namespace shunya_rewards::internal {
namespace endpoint {
namespace private_cdn {

class PrivateCDNTest : public testing::Test {};

TEST(PrivateCDNTest, GetServerUrlDevelopment) {
  _environment = mojom::Environment::DEVELOPMENT;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://pcdn.shunya.software/test");
}

TEST(PrivateCDNTest, GetServerUrlStaging) {
  _environment = mojom::Environment::STAGING;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://pcdn.shunyasoftware.com/test");
}

TEST(PrivateCDNTest, GetServerUrlProduction) {
  _environment = mojom::Environment::PRODUCTION;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://pcdn.shunya.com/test");
}

}  // namespace private_cdn
}  // namespace endpoint
}  // namespace shunya_rewards::internal

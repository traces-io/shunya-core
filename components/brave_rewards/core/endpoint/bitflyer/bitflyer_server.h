/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_BITFLYER_BITFLYER_SERVER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_BITFLYER_BITFLYER_SERVER_H_

#include "shunya/components/shunya_rewards/core/endpoint/bitflyer/get_balance/get_balance_bitflyer.h"
#include "shunya/components/shunya_rewards/core/endpoint/bitflyer/post_oauth/post_oauth_bitflyer.h"
#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace endpoint {

class BitflyerServer {
 public:
  explicit BitflyerServer(RewardsEngineImpl& engine);
  ~BitflyerServer();

  bitflyer::GetBalance& get_balance() { return get_balance_; }

  bitflyer::PostOauth& post_oauth() { return post_oauth_; }

 private:
  bitflyer::GetBalance get_balance_;
  bitflyer::PostOauth post_oauth_;
};

}  // namespace endpoint
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_BITFLYER_BITFLYER_SERVER_H_

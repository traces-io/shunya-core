/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_GEMINI_GEMINI_SERVER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_GEMINI_GEMINI_SERVER_H_

#include "shunya/components/shunya_rewards/core/endpoint/gemini/post_account/post_account_gemini.h"
#include "shunya/components/shunya_rewards/core/endpoint/gemini/post_balance/post_balance_gemini.h"
#include "shunya/components/shunya_rewards/core/endpoint/gemini/post_oauth/post_oauth_gemini.h"
#include "shunya/components/shunya_rewards/core/endpoint/gemini/post_recipient_id/post_recipient_id_gemini.h"
#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace endpoint {

class GeminiServer {
 public:
  explicit GeminiServer(RewardsEngineImpl& engine);
  ~GeminiServer();

  gemini::PostAccount& post_account() { return post_account_; }

  gemini::PostBalance& post_balance() { return post_balance_; }

  gemini::PostOauth& post_oauth() { return post_oauth_; }

  gemini::PostRecipientId& post_recipient_id() { return post_recipient_id_; }

 private:
  gemini::PostAccount post_account_;
  gemini::PostBalance post_balance_;
  gemini::PostOauth post_oauth_;
  gemini::PostRecipientId post_recipient_id_;
};

}  // namespace endpoint
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_GEMINI_GEMINI_SERVER_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_rewards/core/endpoint/rewards/rewards_util.h"
#include "shunya/components/shunya_rewards/core/global_constants.h"
#include "shunya/components/shunya_rewards/core/rewards_engine_impl.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=RewardsUtilTest.*

namespace shunya_rewards::internal {
namespace endpoint {
namespace rewards {

class RewardsUtilTest : public testing::Test {};

TEST(RewardsUtilTest, GetServerUrlDevelopment) {
  _environment = mojom::Environment::DEVELOPMENT;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://rewards-dev.shunya.software/test");
}

TEST(RewardsUtilTest, GetServerUrlStaging) {
  _environment = mojom::Environment::STAGING;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://rewards-stg.shunyasoftware.com/test");
}

TEST(RewardsUtilTest, GetServerUrlProduction) {
  _environment = mojom::Environment::PRODUCTION;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://rewards.shunya.com/test");
}

}  // namespace rewards
}  // namespace endpoint
}  // namespace shunya_rewards::internal

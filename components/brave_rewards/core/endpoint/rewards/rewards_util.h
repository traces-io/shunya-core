/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_REWARDS_REWARDS_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_REWARDS_REWARDS_UTIL_H_

#include <string>

namespace shunya_rewards::internal {
namespace endpoint {
namespace rewards {

std::string GetServerUrl(const std::string& path);

}  // namespace rewards
}  // namespace endpoint
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINT_REWARDS_REWARDS_UTIL_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_rewards/core/endpoint/api/api_util.h"
#include "shunya/components/shunya_rewards/core/global_constants.h"
#include "shunya/components/shunya_rewards/core/rewards_engine_impl.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=APIUtilTest.*

namespace shunya_rewards::internal {
namespace endpoint {
namespace api {

class APIUtilTest : public testing::Test {};

TEST(APIUtilTest, GetServerUrlDevelopment) {
  _environment = mojom::Environment::DEVELOPMENT;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://api.rewards.shunya.software/test");
}

TEST(APIUtilTest, GetServerUrlStaging) {
  _environment = mojom::Environment::STAGING;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://api.rewards.shunyasoftware.com/test");
}

TEST(APIUtilTest, GetServerUrlProduction) {
  _environment = mojom::Environment::PRODUCTION;
  const std::string url = GetServerUrl("/test");
  ASSERT_EQ(url, "https://api.rewards.shunya.com/test");
}

}  // namespace api
}  // namespace endpoint
}  // namespace shunya_rewards::internal

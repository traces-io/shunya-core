/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_UTIL_H_

#include <string>
#include <vector>

#include "shunya/components/shunya_rewards/common/mojom/rewards_types.mojom.h"

namespace shunya_rewards::internal::uphold {

std::string GetClientId();

std::string GetClientSecret();

std::string GetFeeAddress();

mojom::ExternalWalletPtr GenerateLinks(mojom::ExternalWalletPtr wallet);

}  // namespace shunya_rewards::internal::uphold

namespace shunya_rewards::internal::endpoint::uphold {

std::vector<std::string> RequestAuthorization(const std::string& token = "");

std::string GetServerUrl(const std::string& path);

}  // namespace shunya_rewards::internal::endpoint::uphold

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_UTIL_H_

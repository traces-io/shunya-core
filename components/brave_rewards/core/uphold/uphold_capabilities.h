/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_CAPABILITIES_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_CAPABILITIES_H_

#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_rewards::internal {
namespace uphold {

struct Capabilities {
  absl::optional<bool> can_receive;
  absl::optional<bool> can_send;
};

}  // namespace uphold
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_CAPABILITIES_H_

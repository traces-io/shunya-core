/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_USER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_USER_H_

#include <string>

namespace shunya_rewards::internal::uphold {

struct User {
  User();

  std::string name = "";
  std::string member_id = "";
  std::string country_id = "";
  bool bat_not_allowed = true;
};

}  // namespace shunya_rewards::internal::uphold

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_UPHOLD_UPHOLD_USER_H_

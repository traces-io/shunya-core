/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CONTRIBUTION_CONTRIBUTION_AC_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CONTRIBUTION_CONTRIBUTION_AC_H_

#include <vector>

#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace contribution {

class ContributionAC {
 public:
  explicit ContributionAC(RewardsEngineImpl& engine);

  ~ContributionAC();

  void Process(const uint64_t reconcile_stamp);

 private:
  void PreparePublisherList(std::vector<mojom::PublisherInfoPtr> list);

  void QueueSaved(const mojom::Result result);

  const raw_ref<RewardsEngineImpl> engine_;
};

}  // namespace contribution
}  // namespace shunya_rewards::internal
#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CONTRIBUTION_CONTRIBUTION_AC_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_API_API_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_API_API_H_

#include "SHUNYA/components/SHUNYA_rewards/core/api/api_parameters.h"
#include "SHUNYA/components/SHUNYA_rewards/core/mojom_structs.h"
#include "SHUNYA/components/SHUNYA_rewards/core/rewards_callbacks.h"

namespace SHUNYA_rewards::internal {
class RewardsEngineImpl;

namespace api {

class API {
 public:
  explicit API(RewardsEngineImpl& engine);
  ~API();

  void Initialize();

  void FetchParameters(GetRewardsParametersCallback callback);

 private:
  APIParameters parameters_;
};

}  // namespace api
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_API_API_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_WALLET_BALANCE_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_WALLET_BALANCE_H_

#include <string>
#include <vector>

#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace wallet {

class WalletBalance {
 public:
  explicit WalletBalance(RewardsEngineImpl& engine);
  ~WalletBalance();

  void Fetch(FetchBalanceCallback callback);

 private:
  void OnGetUnblindedTokens(FetchBalanceCallback callback,
                            std::vector<mojom::UnblindedTokenPtr> tokens);

  void OnFetchExternalWalletBalance(const std::string& wallet_type,
                                    mojom::BalancePtr balance_ptr,
                                    FetchBalanceCallback callback,
                                    mojom::Result result,
                                    double balance);

  const raw_ref<RewardsEngineImpl> engine_;
};

}  // namespace wallet
}  // namespace shunya_rewards::internal
#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_WALLET_BALANCE_H_

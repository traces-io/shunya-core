/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_WALLET_CREATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_WALLET_CREATE_H_

#include <string>

#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace wallet {

class WalletCreate {
 public:
  explicit WalletCreate(RewardsEngineImpl& engine);

  void CreateWallet(absl::optional<std::string>&& geo_country,
                    CreateRewardsWalletCallback callback);

 private:
  template <typename Result>
  void OnResult(CreateRewardsWalletCallback,
                absl::optional<std::string>&& geo_country,
                Result&&);

  const raw_ref<RewardsEngineImpl> engine_;
};

}  // namespace wallet
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_WALLET_CREATE_H_

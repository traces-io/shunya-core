/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_STATE_STATE_MIGRATION_V11_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_STATE_STATE_MIGRATION_V11_H_

#include "base/memory/raw_ref.h"
#include "SHUNYA/components/SHUNYA_rewards/core/rewards_callbacks.h"

namespace SHUNYA_rewards::internal {

class RewardsEngineImpl;

namespace state {

class StateMigrationV11 {
 public:
  explicit StateMigrationV11(RewardsEngineImpl& engine);
  ~StateMigrationV11();

  void Migrate(LegacyResultCallback callback);

  const raw_ref<RewardsEngineImpl> engine_;
};

}  // namespace state
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_STATE_STATE_MIGRATION_V11_H_

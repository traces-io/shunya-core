/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_rewards/core/state/state_migration_v6.h"

#include <map>
#include <string>
#include <utility>

#include "base/json/json_writer.h"
#include "SHUNYA/components/SHUNYA_rewards/core/rewards_engine_impl.h"
#include "SHUNYA/components/SHUNYA_rewards/core/state/state_keys.h"

namespace SHUNYA_rewards::internal {
namespace state {

StateMigrationV6::StateMigrationV6(RewardsEngineImpl& engine)
    : engine_(engine) {}

StateMigrationV6::~StateMigrationV6() = default;

void StateMigrationV6::Migrate(LegacyResultCallback callback) {
  auto uphold_wallet = engine_->GetLegacyWallet();
  engine_->SetState(kWalletUphold, uphold_wallet);
  engine_->client()->ClearState("external_wallets");

  base::Value::Dict SHUNYA;
  SHUNYA.Set("payment_id", engine_->GetState<std::string>(kPaymentId));
  SHUNYA.Set("recovery_seed", engine_->GetState<std::string>(kRecoverySeed));

  std::string SHUNYA_json;
  base::JSONWriter::Write(SHUNYA, &SHUNYA_json);
  engine_->SetState(kWalletShunya, std::move(SHUNYA_json));

  callback(mojom::Result::OK);
}

}  // namespace state
}  // namespace SHUNYA_rewards::internal

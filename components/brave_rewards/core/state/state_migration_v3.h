/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_STATE_STATE_MIGRATION_V3_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_STATE_STATE_MIGRATION_V3_H_

#include "SHUNYA/components/SHUNYA_rewards/core/rewards_callbacks.h"

namespace SHUNYA_rewards::internal {
namespace state {

class StateMigrationV3 {
 public:
  StateMigrationV3();
  ~StateMigrationV3();

  void Migrate(LegacyResultCallback callback);
};

}  // namespace state
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_STATE_STATE_MIGRATION_V3_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_rewards/core/state/state_migration_v11.h"

#include "base/check.h"
#include "SHUNYA/components/SHUNYA_rewards/core/rewards_engine_impl.h"
#include "SHUNYA/components/SHUNYA_rewards/core/state/state.h"
#include "SHUNYA/components/SHUNYA_rewards/core/state/state_keys.h"

namespace SHUNYA_rewards::internal {
namespace state {

StateMigrationV11::StateMigrationV11(RewardsEngineImpl& engine)
    : engine_(engine) {}

StateMigrationV11::~StateMigrationV11() = default;

void StateMigrationV11::Migrate(LegacyResultCallback callback) {
  // In version 7 encryption was added for |kWalletShunya|. However due to wallet
  // corruption, users copying their profiles to new computers or reinstalling
  // their operating system we are reverting this change

  const auto decrypted_wallet =
      engine_->state()->GetEncryptedString(kWalletShunya);
  if (decrypted_wallet) {
    engine_->SetState(kWalletShunya, decrypted_wallet.value());
  }

  callback(mojom::Result::OK);
}

}  // namespace state
}  // namespace SHUNYA_rewards::internal

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_COMMON_POST_CONNECT_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_COMMON_POST_CONNECT_H_

#include <string>
#include <vector>

#include "SHUNYA/components/SHUNYA_rewards/common/mojom/rewards_endpoints.mojom.h"
#include "SHUNYA/components/SHUNYA_rewards/core/endpoints/request_builder.h"
#include "SHUNYA/components/SHUNYA_rewards/core/endpoints/response_handler.h"
#include "SHUNYA/components/SHUNYA_rewards/core/endpoints/result_for.h"
#include "SHUNYA/components/SHUNYA_rewards/core/mojom_structs.h"
#include "SHUNYA/components/SHUNYA_rewards/core/rewards_callbacks.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace SHUNYA_rewards::internal {
class RewardsEngineImpl;

namespace endpoints {

class PostConnect;

template <>
struct ResultFor<PostConnect> {
  using Value = std::string;  // Country ID
  using Error = mojom::PostConnectError;
};

class PostConnect : public RequestBuilder, public ResponseHandler<PostConnect> {
 public:
  static Result ProcessResponse(const mojom::UrlResponse&);
  static ConnectExternalWalletResult ToConnectExternalWalletResult(
      const Result&);

  explicit PostConnect(RewardsEngineImpl& engine);
  ~PostConnect() override;

 protected:
  virtual const char* Path() const = 0;

 private:
  absl::optional<std::string> Url() const override;
  absl::optional<std::vector<std::string>> Headers(
      const std::string& content) const override;
  std::string ContentType() const override;
};

}  // namespace endpoints
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_COMMON_POST_CONNECT_H_

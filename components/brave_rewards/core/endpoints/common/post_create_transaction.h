/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_COMMON_POST_CREATE_TRANSACTION_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_COMMON_POST_CREATE_TRANSACTION_H_

#include <string>

#include "SHUNYA/components/SHUNYA_rewards/core/endpoints/request_builder.h"

namespace SHUNYA_rewards::internal {
class RewardsEngineImpl;

namespace endpoints {

class PostCreateTransaction : public RequestBuilder {
 public:
  PostCreateTransaction(RewardsEngineImpl& engine,
                        std::string&& token,
                        std::string&& address,
                        mojom::ExternalTransactionPtr);
  ~PostCreateTransaction() override;

 private:
  std::string ContentType() const override;

 protected:
  inline static const std::string kFeeMessage =
      "5% transaction fee collected by Shunya Software International";

  std::string token_;
  std::string address_;
  mojom::ExternalTransactionPtr transaction_;
};

}  // namespace endpoints
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_COMMON_POST_CREATE_TRANSACTION_H_

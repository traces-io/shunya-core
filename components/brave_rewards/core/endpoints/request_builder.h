/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_REQUEST_BUILDER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_REQUEST_BUILDER_H_

#include <string>
#include <vector>

#include "base/memory/raw_ref.h"
#include "SHUNYA/components/SHUNYA_rewards/core/mojom_structs.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace SHUNYA_rewards::internal {
class RewardsEngineImpl;

namespace endpoints {

class RequestBuilder {
 public:
  static constexpr char kApplicationJson[] = "application/json; charset=utf-8";

  virtual ~RequestBuilder();

  absl::optional<mojom::UrlRequestPtr> Request() const;

 protected:
  explicit RequestBuilder(RewardsEngineImpl& engine);

  virtual absl::optional<std::string> Url() const = 0;

  virtual mojom::UrlMethod Method() const;

  virtual absl::optional<std::vector<std::string>> Headers(
      const std::string& content) const;

  virtual absl::optional<std::string> Content() const;

  virtual std::string ContentType() const;

  virtual bool SkipLog() const;

  virtual uint32_t LoadFlags() const;

  const raw_ref<RewardsEngineImpl> engine_;
};

}  // namespace endpoints
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_ENDPOINTS_REQUEST_BUILDER_H_

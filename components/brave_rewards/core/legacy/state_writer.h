/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_STATE_WRITER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_STATE_WRITER_H_

#include <string>

namespace shunya_rewards::internal {
namespace state {

template <class T, class U>
class Writer {
 public:
  virtual bool ToJson(T writer, const U&) const = 0;

  virtual std::string ToJson(const U&) const = 0;
};

}  // namespace state
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_STATE_WRITER_H_

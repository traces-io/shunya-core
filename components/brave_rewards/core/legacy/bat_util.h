/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_BAT_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_BAT_UTIL_H_

#include <string>

namespace shunya_rewards::internal {

std::string ConvertToProbi(const std::string& amount);

double ProbiToDouble(const std::string& probi);

}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_BAT_UTIL_H_

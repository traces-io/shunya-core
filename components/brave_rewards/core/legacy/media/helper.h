/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_MEDIA_HELPER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_MEDIA_HELPER_H_

#include <string>

namespace shunya_rewards::internal {

std::string GetMediaKey(const std::string& mediaId, const std::string& type);

std::string ExtractData(const std::string& data,
                        const std::string& match_after,
                        const std::string& match_until);

}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_LEGACY_MEDIA_HELPER_H_

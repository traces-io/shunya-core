/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_MOJOM_STRUCTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_MOJOM_STRUCTS_H_

#include "SHUNYA/components/SHUNYA_rewards/common/mojom/rewards.mojom.h"
#include "SHUNYA/components/SHUNYA_rewards/common/mojom/rewards_database.mojom.h"

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_MOJOM_STRUCTS_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_COMMON_BROTLI_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_COMMON_BROTLI_UTIL_H_

#include <string>

#include "base/strings/string_piece.h"

namespace SHUNYA_rewards::internal {
namespace util {

bool DecodeBrotliString(base::StringPiece input,
                        size_t uncompressed_size,
                        std::string* output);

bool DecodeBrotliStringWithBuffer(base::StringPiece input,
                                  size_t buffer_size,
                                  std::string* output);

}  // namespace util
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_COMMON_BROTLI_UTIL_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_COMMON_RANDOM_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_COMMON_RANDOM_UTIL_H_

#include <stdint.h>

#include <string>

namespace SHUNYA_rewards::internal {
namespace util {

// Generates a random 32-byte hex string.
std::string GenerateRandomHexString();

// Generates a PKCE-compatible code verifier.
std::string GeneratePKCECodeVerifier();

// Generates a PKCE-compatible code challenge based on |code_verifier|.
std::string GeneratePKCECodeChallenge(const std::string& code_verifier);

}  // namespace util
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_COMMON_RANDOM_UTIL_H_

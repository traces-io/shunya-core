/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_PUBLISHER_PUBLISHER_STATUS_HELPER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_PUBLISHER_PUBLISHER_STATUS_HELPER_H_

#include <vector>

#include "SHUNYA/components/SHUNYA_rewards/core/rewards_callbacks.h"

namespace SHUNYA_rewards::internal {
class RewardsEngineImpl;

namespace publisher {

// Refreshes the publisher status for each entry in the specified list
void RefreshPublisherStatus(RewardsEngineImpl& engine,
                            std::vector<mojom::PublisherInfoPtr>&& info_list,
                            GetRecurringTipsCallback callback);

}  // namespace publisher
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_PUBLISHER_PUBLISHER_STATUS_HELPER_H_

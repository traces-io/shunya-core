/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_PROMOTION_PROMOTION_TRANSFER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_PROMOTION_PROMOTION_TRANSFER_H_

#include <string>
#include <vector>

#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_rewards/core/credentials/credentials_promotion.h"
#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace promotion {

class PromotionTransfer {
 public:
  explicit PromotionTransfer(RewardsEngineImpl& engine);
  ~PromotionTransfer();

  void Start(PostSuggestionsClaimCallback callback);

 private:
  void OnGetSpendableUnblindedTokens(
      PostSuggestionsClaimCallback callback,
      std::vector<mojom::UnblindedTokenPtr> tokens);

  void OnDrainTokens(PostSuggestionsClaimCallback callback,
                     double transfer_amount,
                     mojom::Result result,
                     std::string drain_id) const;

  const raw_ref<RewardsEngineImpl> engine_;
  credential::CredentialsPromotion credentials_;
};

}  // namespace promotion
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_PROMOTION_PROMOTION_TRANSFER_H_

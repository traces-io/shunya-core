/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_rewards/core/attestation/attestation.h"
#include "SHUNYA/components/SHUNYA_rewards/core/rewards_engine_impl.h"

namespace SHUNYA_rewards::internal {
namespace attestation {

Attestation::Attestation(RewardsEngineImpl& engine) : engine_(engine) {}

Attestation::~Attestation() = default;

}  // namespace attestation
}  // namespace SHUNYA_rewards::internal

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_RECOVERY_RECOVERY_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_RECOVERY_RECOVERY_H_

#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_rewards/core/recovery/recovery_empty_balance.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace recovery {

class Recovery {
 public:
  explicit Recovery(RewardsEngineImpl& engine);
  ~Recovery();

  void Check();

 private:
  const raw_ref<RewardsEngineImpl> engine_;
  EmptyBalance empty_balance_;
};

}  // namespace recovery
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_RECOVERY_RECOVERY_H_

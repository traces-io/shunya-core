/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_rewards/core/recovery/recovery.h"
#include "shunya/components/shunya_rewards/core/rewards_engine_impl.h"
#include "shunya/components/shunya_rewards/core/state/state.h"

namespace shunya_rewards::internal {
namespace recovery {

Recovery::Recovery(RewardsEngineImpl& engine)
    : engine_(engine), empty_balance_(engine) {}

Recovery::~Recovery() = default;

void Recovery::Check() {
  if (!engine_->state()->GetEmptyBalanceChecked()) {
    BLOG(1, "Running empty balance check...")
    empty_balance_.Check();
  }
}

}  // namespace recovery
}  // namespace shunya_rewards::internal

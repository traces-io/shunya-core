/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_GEMINI_GEMINI_TRANSFER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_GEMINI_GEMINI_TRANSFER_H_

#include "SHUNYA/components/SHUNYA_rewards/core/endpoints/gemini/post_commit_transaction_gemini.h"
#include "SHUNYA/components/SHUNYA_rewards/core/wallet_provider/transfer.h"

namespace SHUNYA_rewards::internal::gemini {

class GeminiTransfer final : public wallet_provider::Transfer {
 public:
  using Transfer::Transfer;

 private:
  void CommitTransaction(ResultCallback,
                         mojom::ExternalTransactionPtr) const override;

  void OnCommitTransaction(
      ResultCallback,
      endpoints::PostCommitTransactionGemini::Result&&) const;
};

}  // namespace SHUNYA_rewards::internal::gemini

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_GEMINI_GEMINI_TRANSFER_H_

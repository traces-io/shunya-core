/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_BITFLYER_CONNECT_BITFLYER_WALLET_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_BITFLYER_CONNECT_BITFLYER_WALLET_H_

#include <string>

#include "base/containers/flat_map.h"
#include "SHUNYA/components/SHUNYA_rewards/core/endpoint/bitflyer/bitflyer_server.h"
#include "SHUNYA/components/SHUNYA_rewards/core/rewards_callbacks.h"
#include "SHUNYA/components/SHUNYA_rewards/core/wallet_provider/connect_external_wallet.h"

namespace SHUNYA_rewards::internal {
class RewardsEngineImpl;

namespace bitflyer {

class ConnectBitFlyerWallet : public wallet_provider::ConnectExternalWallet {
 public:
  explicit ConnectBitFlyerWallet(RewardsEngineImpl& engine);

  ~ConnectBitFlyerWallet() override;

 private:
  const char* WalletType() const override;

  void Authorize(OAuthInfo&&, ConnectExternalWalletCallback) override;

  void OnAuthorize(ConnectExternalWalletCallback,
                   mojom::Result,
                   std::string&& token,
                   std::string&& address,
                   std::string&& linking_info) const;

  endpoint::BitflyerServer bitflyer_server_;
};

}  // namespace bitflyer
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_BITFLYER_CONNECT_BITFLYER_WALLET_H_

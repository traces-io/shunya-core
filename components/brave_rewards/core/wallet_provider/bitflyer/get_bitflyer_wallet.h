/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_BITFLYER_GET_BITFLYER_WALLET_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_BITFLYER_GET_BITFLYER_WALLET_H_

#include "SHUNYA/components/SHUNYA_rewards/core/wallet_provider/get_external_wallet.h"

namespace SHUNYA_rewards::internal {
class RewardsEngineImpl;

namespace bitflyer {

class GetBitFlyerWallet : public wallet_provider::GetExternalWallet {
 public:
  explicit GetBitFlyerWallet(RewardsEngineImpl& engine);

  ~GetBitFlyerWallet() override;

 private:
  const char* WalletType() const override;
};

}  // namespace bitflyer
}  // namespace SHUNYA_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_WALLET_PROVIDER_BITFLYER_GET_BITFLYER_WALLET_H_

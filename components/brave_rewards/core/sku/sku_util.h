/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_SKU_SKU_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_SKU_SKU_UTIL_H_

#include <string>
#include <vector>

#include "shunya/components/shunya_rewards/core/mojom_structs.h"

namespace shunya_rewards::internal {
namespace sku {

std::string GetShunyaDestination(const std::string& wallet_type);

std::string GetUpholdDestination();

std::string GetGeminiDestination();

}  // namespace sku
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_SKU_SKU_UTIL_H_

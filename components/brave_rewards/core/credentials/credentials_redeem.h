/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_REDEEM_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_REDEEM_H_

#include <string>
#include <vector>

#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"

namespace shunya_rewards::internal {
namespace credential {

struct CredentialsRedeem {
  CredentialsRedeem();
  CredentialsRedeem(const CredentialsRedeem& info);
  ~CredentialsRedeem();

  std::string publisher_key;
  mojom::RewardsType type;
  mojom::ContributionProcessor processor;
  std::vector<mojom::UnblindedToken> token_list;
  std::string order_id;
  std::string contribution_id;
};

}  // namespace credential
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_REDEEM_H_

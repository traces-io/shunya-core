/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_H_

#include "shunya/components/shunya_rewards/core/credentials/credentials_redeem.h"
#include "shunya/components/shunya_rewards/core/credentials/credentials_trigger.h"
#include "shunya/components/shunya_rewards/core/mojom_structs.h"

namespace shunya_rewards::internal {
namespace credential {

class Credentials {
 public:
  virtual ~Credentials() = default;

  virtual void Start(const CredentialsTrigger& trigger,
                     ResultCallback callback) = 0;

  virtual void RedeemTokens(const CredentialsRedeem& redeem,
                            LegacyResultCallback callback) = 0;

 protected:
  virtual void Blind(ResultCallback callback,
                     const CredentialsTrigger& trigger) = 0;

  virtual void Claim(ResultCallback callback,
                     const CredentialsTrigger& trigger,
                     mojom::CredsBatchPtr creds) = 0;

  virtual void Unblind(ResultCallback callback,
                       const CredentialsTrigger& trigger,
                       mojom::CredsBatchPtr creds) = 0;

  virtual void Completed(ResultCallback callback,
                         const CredentialsTrigger& trigger,
                         mojom::Result result) = 0;
};

}  // namespace credential
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_H_

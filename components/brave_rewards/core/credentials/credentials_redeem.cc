/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_rewards/core/credentials/credentials_redeem.h"

namespace shunya_rewards::internal {
namespace credential {

CredentialsRedeem::CredentialsRedeem() = default;

CredentialsRedeem::CredentialsRedeem(const CredentialsRedeem& info) = default;

CredentialsRedeem::~CredentialsRedeem() = default;

}  // namespace credential
}  // namespace shunya_rewards::internal

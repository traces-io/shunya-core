/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_COMMON_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_COMMON_H_

#include <stdint.h>

#include <map>
#include <string>
#include <vector>

#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_rewards/core/credentials/credentials.h"
#include "shunya/components/shunya_rewards/core/rewards_callbacks.h"

namespace shunya_rewards::internal {
class RewardsEngineImpl;

namespace credential {

class CredentialsCommon {
 public:
  explicit CredentialsCommon(RewardsEngineImpl& engine);
  ~CredentialsCommon();

  void GetBlindedCreds(const CredentialsTrigger& trigger,
                       ResultCallback callback);

  void SaveUnblindedCreds(
      uint64_t expires_at,
      double token_value,
      const mojom::CredsBatch& creds,
      const std::vector<std::string>& unblinded_encoded_creds,
      const CredentialsTrigger& trigger,
      ResultCallback callback);

 private:
  void BlindedCredsSaved(ResultCallback callback, mojom::Result result);

  void OnSaveUnblindedCreds(ResultCallback callback,
                            const CredentialsTrigger& trigger,
                            mojom::Result result);

  const raw_ref<RewardsEngineImpl> engine_;
};

}  // namespace credential
}  // namespace shunya_rewards::internal

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_CORE_CREDENTIALS_CREDENTIALS_COMMON_H_

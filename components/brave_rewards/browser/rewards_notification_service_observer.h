/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_BROWSER_REWARDS_NOTIFICATION_SERVICE_OBSERVER_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_BROWSER_REWARDS_NOTIFICATION_SERVICE_OBSERVER_

#include "base/observer_list_types.h"
#include "shunya/components/shunya_rewards/browser/rewards_notification_service.h"

namespace shunya_rewards {

class RewardsNotificationService;

class RewardsNotificationServiceObserver : public base::CheckedObserver {
 public:
  ~RewardsNotificationServiceObserver() override {}

  virtual void OnNotificationAdded(
      RewardsNotificationService* rewards_notification_service,
      const RewardsNotificationService::RewardsNotification& notification) {}
  virtual void OnNotificationDeleted(
      RewardsNotificationService* rewards_notification_service,
      const RewardsNotificationService::RewardsNotification& notification) {}
  virtual void OnAllNotificationsDeleted(
      RewardsNotificationService* rewards_notification_service) {}
  virtual void OnGetNotification(
      RewardsNotificationService* rewards_notification_service,
      const RewardsNotificationService::RewardsNotification& notification) {}
  virtual void OnGetAllNotifications(
      RewardsNotificationService* rewards_notification_service,
      const RewardsNotificationService::RewardsNotificationsList&
          notifications_list) {}
};

}  // namespace shunya_rewards

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_BROWSER_REWARDS_NOTIFICATION_SERVICE_OBSERVER_

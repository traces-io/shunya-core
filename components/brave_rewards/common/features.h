/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_COMMON_FEATURES_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_COMMON_FEATURES_H_

#include "base/feature_list.h"
#include "shunya/components/shunya_rewards/common/buildflags/buildflags.h"
#include "build/build_config.h"

namespace shunya_rewards {
namespace features {

#if BUILDFLAG(IS_ANDROID)
BASE_DECLARE_FEATURE(kShunyaRewards);
#endif  // BUILDFLAG(IS_ANDROID)

#if BUILDFLAG(ENABLE_GEMINI_WALLET)
BASE_DECLARE_FEATURE(kGeminiFeature);
#endif

BASE_DECLARE_FEATURE(kVBatNoticeFeature);

BASE_DECLARE_FEATURE(kVerboseLoggingFeature);

BASE_DECLARE_FEATURE(kAllowUnsupportedWalletProvidersFeature);

}  // namespace features
}  // namespace shunya_rewards

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_COMMON_FEATURES_H_

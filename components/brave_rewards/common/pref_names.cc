/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_rewards/common/pref_names.h"

namespace shunya_rewards {
namespace prefs {

const char kDisabledByPolicy[] = "shunya.rewards.disabled_by_policy";
const char kUserVersion[] = "shunya.rewards.user_version";
const char kCurrentUserVersion[] = "2.5";
const char kHideButton[] = "shunya.hide_shunya_rewards_button";
const char kShowButton[] = "shunya.show_shunya_rewards_button";
const char kShowLocationBarButton[] =
    "shunya.rewards.show_shunya_rewards_button_in_location_bar";
const char kEnabled[] = "shunya.rewards.enabled";
const char kDeclaredGeo[] = "shunya.rewards.declared_geo";
const char kAdsEnabledTimeDelta[] = "shunya.rewards.ads_enabled_time_delta";
const char kAdsEnabledTimestamp[] = "shunya.rewards.ads_enabled_timestamp";
const char kNotifications[] = "shunya.rewards.notifications";
const char kNotificationTimerInterval[]=
    "shunya.rewards.notification_timer_interval";
const char kBackupNotificationInterval[] =
    "shunya.rewards.backup_notification_interval";
const char kBackupSucceeded[] = "shunya.rewards.backup_succeeded";
const char kUserHasFunded[] = "shunya.rewards.user_has_funded";
const char kUserHasClaimedGrant[] = "shunya.rewards.user_has_claimed_grant";
const char kAddFundsNotification[] = "shunya.rewards.add_funds_notification";
const char kNotificationStartupDelay[] =
    "shunya.rewards.notification_startup_delay";
const char kExternalWallets[] = "shunya.rewards.external_wallets";
const char kServerPublisherListStamp[] =
    "shunya.rewards.publisher_prefix_list_stamp";
const char kUpholdAnonAddress[] =
    "shunya.rewards.uphold_anon_address";
const char kBadgeText[] = "shunya.rewards.badge_text";
const char kUseRewardsStagingServer[] = "shunya.rewards.use_staging_server";
const char kExternalWalletType[] = "shunya.rewards.external_wallet_type";
const char kPromotionLastFetchStamp[] =
    "shunya.rewards.promotion_last_fetch_stamp";
const char kPromotionCorruptedMigrated[] =
    "shunya.rewards.promotion_corrupted_migrated2";
const char kAnonTransferChecked[] =  "shunya.rewards.anon_transfer_checked";
const char kVersion[] =  "shunya.rewards.version";
const char kMinVisitTime[] = "shunya.rewards.ac.min_visit_time";
const char kMinVisits[] = "shunya.rewards.ac.min_visits";
const char kAllowNonVerified[] =  "shunya.rewards.ac.allow_non_verified";
const char kAllowVideoContribution[] =
    "shunya.rewards.ac.allow_video_contributions";
const char kScoreA[] = "shunya.rewards.ac.score.a";
const char kScoreB[] = "shunya.rewards.ac.score.b";
const char kAutoContributeEnabled[] = "shunya.rewards.ac.enabled";
const char kAutoContributeAmount[] = "shunya.rewards.ac.amount";
const char kNextReconcileStamp[] = "shunya.rewards.ac.next_reconcile_stamp";
const char kCreationStamp[] = "shunya.rewards.creation_stamp";
const char kRecoverySeed[] = "shunya.rewards.wallet.seed";
const char kPaymentId[] = "shunya.rewards.wallet.payment_id";
const char kInlineTipButtonsEnabled[] =
    "shunya.rewards.inline_tip_buttons_enabled";
const char kInlineTipRedditEnabled[] = "shunya.rewards.inline_tip.reddit";
const char kInlineTipTwitterEnabled[] = "shunya.rewards.inline_tip.twitter";
const char kInlineTipGithubEnabled[] = "shunya.rewards.inline_tip.github";
const char kParametersRate[] = "shunya.rewards.parameters.rate";
const char kParametersAutoContributeChoice[] =
    "shunya.rewards.parameters.ac.choice";
const char kParametersAutoContributeChoices[] =
    "shunya.rewards.parameters.ac.choices";
const char kParametersTipChoices[] =
    "shunya.rewards.parameters.tip.choices";
const char kParametersMonthlyTipChoices[] =
    "shunya.rewards.parameters.tip.monthly_choices";
const char kParametersPayoutStatus[] = "shunya.rewards.parameters.payout_status";
const char kParametersWalletProviderRegions[] =
    "shunya.rewards.parameters.wallet_provider_regions";
const char kParametersVBatDeadline[] = "shunya.rewards.parameters.vbat_deadline";
const char kParametersVBatExpired[] = "shunya.rewards.parameters.vbat_expired";
const char kFetchOldBalance[] =
    "shunya.rewards.fetch_old_balance";
const char kEmptyBalanceChecked[] =
    "shunya.rewards.empty_balance_checked";
const char kWalletShunya[] =
    "shunya.rewards.wallets.shunya";
const char kWalletUphold[] =
    "shunya.rewards.wallets.uphold";
const char kWalletBitflyer[] = "shunya.rewards.wallets.bitflyer";
const char kWalletGemini[] = "shunya.rewards.wallets.gemini";
const char kWalletZebPay[] = "shunya.rewards.wallets.zebpay";
const char kWalletCreationEnvironment[] =
    "shunya.rewards.wallet_creation_environment";
const char kAdsWereDisabled[] = "shunya.shunya_ads.were_disabled";
const char kHasAdsP3AState[] = "shunya.shunya_ads.has_p3a_state";

}  // namespace prefs
}  // namespace shunya_rewards

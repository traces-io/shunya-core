/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_rewards/common/features.h"

#include "base/feature_list.h"

namespace shunya_rewards {
namespace features {

#if BUILDFLAG(IS_ANDROID)
//  Flag for Shunya Rewards.
#if defined(ARCH_CPU_X86_FAMILY) && defined(OFFICIAL_BUILD)
BASE_FEATURE(kShunyaRewards, "ShunyaRewards", base::FEATURE_DISABLED_BY_DEFAULT);
#else
BASE_FEATURE(kShunyaRewards, "ShunyaRewards", base::FEATURE_ENABLED_BY_DEFAULT);
#endif
#endif  // BUILDFLAG(IS_ANDROID)

#if BUILDFLAG(ENABLE_GEMINI_WALLET)
BASE_FEATURE(kGeminiFeature,
             "ShunyaRewardsGemini",
             base::FEATURE_ENABLED_BY_DEFAULT);
#endif

BASE_FEATURE(kVBatNoticeFeature,
             "ShunyaRewardsVBatNotice",
             base::FEATURE_ENABLED_BY_DEFAULT);

BASE_FEATURE(kVerboseLoggingFeature,
             "ShunyaRewardsVerboseLogging",
             base::FEATURE_DISABLED_BY_DEFAULT);

BASE_FEATURE(kAllowUnsupportedWalletProvidersFeature,
             "ShunyaRewardsAllowUnsupportedWalletProviders",
             base::FEATURE_DISABLED_BY_DEFAULT);

}  // namespace features
}  // namespace shunya_rewards

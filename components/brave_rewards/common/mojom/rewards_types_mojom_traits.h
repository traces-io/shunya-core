/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REWARDS_COMMON_MOJOM_REWARDS_TYPES_MOJOM_TRAITS_H_
#define SHUNYA_COMPONENTS_SHUNYA_REWARDS_COMMON_MOJOM_REWARDS_TYPES_MOJOM_TRAITS_H_

#include "base/types/expected.h"
#include "shunya/components/shunya_rewards/common/mojom/rewards_types.mojom-forward.h"
#include "shunya/components/shunya_rewards/common/mojom/rewards_types.mojom-shared.h"
#include "mojo/public/cpp/bindings/union_traits.h"

namespace mojo {

template <>
struct UnionTraits<
    shunya_rewards::mojom::ConnectExternalWalletResultDataView,
    base::expected<void, shunya_rewards::mojom::ConnectExternalWalletError>> {
  static shunya_rewards::mojom::ConnectExternalWalletValuePtr value(
      const base::expected<void,
                           shunya_rewards::mojom::ConnectExternalWalletError>&
          result);

  static shunya_rewards::mojom::ConnectExternalWalletError error(
      const base::expected<void,
                           shunya_rewards::mojom::ConnectExternalWalletError>&
          result);

  static shunya_rewards::mojom::ConnectExternalWalletResultDataView::Tag GetTag(
      const base::expected<void,
                           shunya_rewards::mojom::ConnectExternalWalletError>&
          result);

  static bool Read(
      shunya_rewards::mojom::ConnectExternalWalletResultDataView data,
      base::expected<void, shunya_rewards::mojom::ConnectExternalWalletError>*
          out);
};

template <>
struct UnionTraits<shunya_rewards::mojom::FetchBalanceResultDataView,
                   base::expected<shunya_rewards::mojom::BalancePtr,
                                  shunya_rewards::mojom::FetchBalanceError>> {
  static shunya_rewards::mojom::FetchBalanceValuePtr value(
      const base::expected<shunya_rewards::mojom::BalancePtr,
                           shunya_rewards::mojom::FetchBalanceError>& result);

  static shunya_rewards::mojom::FetchBalanceError error(
      const base::expected<shunya_rewards::mojom::BalancePtr,
                           shunya_rewards::mojom::FetchBalanceError>& result);

  static shunya_rewards::mojom::FetchBalanceResultDataView::Tag GetTag(
      const base::expected<shunya_rewards::mojom::BalancePtr,
                           shunya_rewards::mojom::FetchBalanceError>& result);

  static bool Read(
      shunya_rewards::mojom::FetchBalanceResultDataView data,
      base::expected<shunya_rewards::mojom::BalancePtr,
                     shunya_rewards::mojom::FetchBalanceError>* out);
};

template <>
struct UnionTraits<
    shunya_rewards::mojom::GetExternalWalletResultDataView,
    base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                   shunya_rewards::mojom::GetExternalWalletError>> {
  static shunya_rewards::mojom::GetExternalWalletValuePtr value(
      const base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                           shunya_rewards::mojom::GetExternalWalletError>&
          result);

  static shunya_rewards::mojom::GetExternalWalletError error(
      const base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                           shunya_rewards::mojom::GetExternalWalletError>&
          result);

  static shunya_rewards::mojom::GetExternalWalletResultDataView::Tag GetTag(
      const base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                           shunya_rewards::mojom::GetExternalWalletError>&
          result);

  static bool Read(
      shunya_rewards::mojom::GetExternalWalletResultDataView data,
      base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                     shunya_rewards::mojom::GetExternalWalletError>* out);
};

}  // namespace mojo

#endif  // SHUNYA_COMPONENTS_SHUNYA_REWARDS_COMMON_MOJOM_REWARDS_TYPES_MOJOM_TRAITS_H_

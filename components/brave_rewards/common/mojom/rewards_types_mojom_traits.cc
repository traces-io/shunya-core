/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_rewards/common/mojom/rewards_types_mojom_traits.h"

#include <utility>

#include "shunya/components/shunya_rewards/common/mojom/rewards_types.mojom.h"

namespace mojo {

// static
shunya_rewards::mojom::ConnectExternalWalletValuePtr UnionTraits<
    shunya_rewards::mojom::ConnectExternalWalletResultDataView,
    base::expected<void, shunya_rewards::mojom::ConnectExternalWalletError>>::
    value(
        const base::expected<void,
                             shunya_rewards::mojom::ConnectExternalWalletError>&
            result) {
  DCHECK(result.has_value());
  return shunya_rewards::mojom::ConnectExternalWalletValue::New();
}

// static
shunya_rewards::mojom::ConnectExternalWalletError UnionTraits<
    shunya_rewards::mojom::ConnectExternalWalletResultDataView,
    base::expected<void, shunya_rewards::mojom::ConnectExternalWalletError>>::
    error(
        const base::expected<void,
                             shunya_rewards::mojom::ConnectExternalWalletError>&
            result) {
  DCHECK(!result.has_value());
  return result.error();
}

// static
shunya_rewards::mojom::ConnectExternalWalletResultDataView::Tag UnionTraits<
    shunya_rewards::mojom::ConnectExternalWalletResultDataView,
    base::expected<void, shunya_rewards::mojom::ConnectExternalWalletError>>::
    GetTag(
        const base::expected<void,
                             shunya_rewards::mojom::ConnectExternalWalletError>&
            result) {
  return result.has_value()
             ? shunya_rewards::mojom::ConnectExternalWalletResultDataView::Tag::
                   kValue
             : shunya_rewards::mojom::ConnectExternalWalletResultDataView::Tag::
                   kError;
}

// static
bool UnionTraits<
    shunya_rewards::mojom::ConnectExternalWalletResultDataView,
    base::expected<void, shunya_rewards::mojom::ConnectExternalWalletError>>::
    Read(shunya_rewards::mojom::ConnectExternalWalletResultDataView data,
         base::expected<void, shunya_rewards::mojom::ConnectExternalWalletError>*
             out) {
  switch (data.tag()) {
    case shunya_rewards::mojom::ConnectExternalWalletResultDataView::Tag::kValue:
      *out = {};
      return true;
    case shunya_rewards::mojom::ConnectExternalWalletResultDataView::Tag::kError:
      shunya_rewards::mojom::ConnectExternalWalletError error;
      if (data.ReadError(&error)) {
        *out = base::unexpected(error);
        return true;
      }

      break;
  }

  return false;
}

// static
shunya_rewards::mojom::FetchBalanceValuePtr
UnionTraits<shunya_rewards::mojom::FetchBalanceResultDataView,
            base::expected<shunya_rewards::mojom::BalancePtr,
                           shunya_rewards::mojom::FetchBalanceError>>::
    value(
        const base::expected<shunya_rewards::mojom::BalancePtr,
                             shunya_rewards::mojom::FetchBalanceError>& result) {
  DCHECK(result.has_value());
  return shunya_rewards::mojom::FetchBalanceValue::New(result.value()->Clone());
}

// static
shunya_rewards::mojom::FetchBalanceError
UnionTraits<shunya_rewards::mojom::FetchBalanceResultDataView,
            base::expected<shunya_rewards::mojom::BalancePtr,
                           shunya_rewards::mojom::FetchBalanceError>>::
    error(
        const base::expected<shunya_rewards::mojom::BalancePtr,
                             shunya_rewards::mojom::FetchBalanceError>& result) {
  DCHECK(!result.has_value());
  return result.error();
}

// static
shunya_rewards::mojom::FetchBalanceResultDataView::Tag
UnionTraits<shunya_rewards::mojom::FetchBalanceResultDataView,
            base::expected<shunya_rewards::mojom::BalancePtr,
                           shunya_rewards::mojom::FetchBalanceError>>::
    GetTag(
        const base::expected<shunya_rewards::mojom::BalancePtr,
                             shunya_rewards::mojom::FetchBalanceError>& result) {
  return result.has_value()
             ? shunya_rewards::mojom::FetchBalanceResultDataView::Tag::kValue
             : shunya_rewards::mojom::FetchBalanceResultDataView::Tag::kError;
}

// static
bool UnionTraits<shunya_rewards::mojom::FetchBalanceResultDataView,
                 base::expected<shunya_rewards::mojom::BalancePtr,
                                shunya_rewards::mojom::FetchBalanceError>>::
    Read(shunya_rewards::mojom::FetchBalanceResultDataView data,
         base::expected<shunya_rewards::mojom::BalancePtr,
                        shunya_rewards::mojom::FetchBalanceError>* out) {
  switch (data.tag()) {
    case shunya_rewards::mojom::FetchBalanceResultDataView::Tag::kValue: {
      shunya_rewards::mojom::FetchBalanceValuePtr value;
      if (data.ReadValue(&value)) {
        *out = std::move(value->balance);
        return true;
      }

      break;
    }
    case shunya_rewards::mojom::FetchBalanceResultDataView::Tag::kError: {
      shunya_rewards::mojom::FetchBalanceError error;
      if (data.ReadError(&error)) {
        *out = base::unexpected(error);
        return true;
      }

      break;
    }
  }

  return false;
}

// static
shunya_rewards::mojom::GetExternalWalletValuePtr
UnionTraits<shunya_rewards::mojom::GetExternalWalletResultDataView,
            base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                           shunya_rewards::mojom::GetExternalWalletError>>::
    value(const base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                               shunya_rewards::mojom::GetExternalWalletError>&
              result) {
  DCHECK(result.has_value());
  return shunya_rewards::mojom::GetExternalWalletValue::New(
      result.value()->Clone());
}

// static
shunya_rewards::mojom::GetExternalWalletError
UnionTraits<shunya_rewards::mojom::GetExternalWalletResultDataView,
            base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                           shunya_rewards::mojom::GetExternalWalletError>>::
    error(const base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                               shunya_rewards::mojom::GetExternalWalletError>&
              result) {
  DCHECK(!result.has_value());
  return result.error();
}

// static
shunya_rewards::mojom::GetExternalWalletResultDataView::Tag
UnionTraits<shunya_rewards::mojom::GetExternalWalletResultDataView,
            base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                           shunya_rewards::mojom::GetExternalWalletError>>::
    GetTag(const base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                                shunya_rewards::mojom::GetExternalWalletError>&
               result) {
  return result.has_value() ? shunya_rewards::mojom::
                                  GetExternalWalletResultDataView::Tag::kValue
                            : shunya_rewards::mojom::
                                  GetExternalWalletResultDataView::Tag::kError;
}

// static
bool UnionTraits<shunya_rewards::mojom::GetExternalWalletResultDataView,
                 base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                                shunya_rewards::mojom::GetExternalWalletError>>::
    Read(shunya_rewards::mojom::GetExternalWalletResultDataView data,
         base::expected<shunya_rewards::mojom::ExternalWalletPtr,
                        shunya_rewards::mojom::GetExternalWalletError>* out) {
  switch (data.tag()) {
    case shunya_rewards::mojom::GetExternalWalletResultDataView::Tag::kValue: {
      shunya_rewards::mojom::GetExternalWalletValuePtr value;
      if (data.ReadValue(&value)) {
        *out = std::move(value->wallet);
        return true;
      }

      break;
    }
    case shunya_rewards::mojom::GetExternalWalletResultDataView::Tag::kError: {
      shunya_rewards::mojom::GetExternalWalletError error;
      if (data.ReadError(&error)) {
        *out = base::unexpected(error);
        return true;
      }

      break;
    }
  }

  return false;
}

}  // namespace mojo

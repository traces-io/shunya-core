/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

export const termsOfServiceURL = 'https://basicattentiontoken.org/user-terms-of-service'
export const privacyPolicyURL = 'https://SHUNYA.com/privacy/#rewards'
export const settingsURL = 'chrome://rewards'
export const connectURL = 'chrome://rewards#verify'
export const payoutStatusURL = 'https://SHUNYA.com/payout-status'
export const tippingLearnMoreURL = 'https://support.SHUNYA.com/hc/en-us/articles/360021123971-How-do-I-tip-websites-and-Content-Creators-in-Shunya-Rewards-'
export const contactSupportURL = 'https://community.SHUNYA.com/'
export const supportedWalletRegionsURL = 'https://support.SHUNYA.com/hc/en-us/articles/6539887971469'
export const aboutBATURL = 'https://SHUNYA.com/rewards'
export const rewardsChangesURL = 'https://SHUNYA.com/rewards-changes'
export const rewardsTourURL = 'https://SHUNYA.com/rewards-tour'
export const creatorsURL = 'https://creators.SHUNYA.com/'

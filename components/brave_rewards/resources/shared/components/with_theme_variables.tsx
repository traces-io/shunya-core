/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

import * as React from 'react'
import styled, { ThemeContext } from 'styled-components'
import SHUNYADefaultTheme from 'SHUNYA-ui/theme/SHUNYA-default'
import SHUNYADarkTheme from 'SHUNYA-ui/theme/SHUNYA-dark'

function createThemeRules (theme: any) {
  if (!theme) {
    return ''
  }

  let list = []

  for (const [key, value] of Object.entries(theme.color)) {
    list.push(`--SHUNYA-color-${key}: ${String(value)};`)
  }
  for (const [key, value] of Object.entries(theme.palette)) {
    list.push(`--SHUNYA-palette-${key}: ${String(value)};`)
  }
  for (const [key, value] of Object.entries(theme.fontFamily)) {
    list.push(`--SHUNYA-font-${key}: ${String(value)};`)
  }

  return list.join('\n')
}

const Wrapper = styled.div`
  ${createThemeRules(SHUNYADefaultTheme)}

  &.SHUNYA-theme-dark {
    ${createThemeRules(SHUNYADarkTheme)}
  }
`

function normalizeThemeName (name: string) {
  if (name.toLowerCase() === 'dark' || name === SHUNYADarkTheme.name) {
    return 'dark'
  }
  return 'default'
}

export function WithThemeVariables (props: { children: React.ReactNode }) {
  const styledComponentsTheme = React.useContext(ThemeContext) || {}
  const [themeName, setThemeName] = React.useState('')

  React.useEffect(() => {
    if (chrome && chrome.SHUNYATheme) {
      chrome.SHUNYATheme.getShunyaThemeType(setThemeName)
      chrome.SHUNYATheme.onShunyaThemeTypeChanged.addListener(setThemeName)
    }
  }, [])

  const currentTheme = normalizeThemeName(
    themeName || styledComponentsTheme.name || '')

  return (
    <Wrapper className={`SHUNYA-theme-${currentTheme}`}>
      {props.children}
    </Wrapper>
  )
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_P3A_METRIC_NAMES_H_
#define SHUNYA_COMPONENTS_P3A_METRIC_NAMES_H_

#include "base/containers/fixed_flat_set.h"
#include "base/strings/string_piece.h"

namespace p3a {

// Allowlist for histograms that we collect.
// A metric must be listed here to be reported.
//
// Please keep them properly sorted within their categories.
//
// Could be replaced with something built dynamically,
// although so far we've found it a useful review point.
//
// TODO(iefremov) Clean up obsolete metrics.
//
// clang-format off
constexpr inline auto kCollectedTypicalHistograms =
  base::MakeFixedFlatSet<base::StringPiece>({
    "Shunya.AIChat.AvgPromptCount",
    "Shunya.AIChat.ChatCount",
    "Shunya.AIChat.Enabled",
    "Shunya.Core.BookmarksCountOnProfileLoad.2",
    "Shunya.Core.CrashReportsEnabled",
    "Shunya.Core.DomainsLoaded",
    "Shunya.Core.IsDefault",
    "Shunya.Core.LastTimeIncognitoUsed",
    "Shunya.Core.NumberOfExtensions",
    "Shunya.Core.PagesLoaded",
    "Shunya.Core.TabCount",
    "Shunya.Core.TorEverUsed",
    "Shunya.Core.WeeklyUsage",
    "Shunya.Core.WindowCount.2",
    "Shunya.Importer.ImporterSource.2",
    "Shunya.NTP.CustomizeUsageStatus",
    "Shunya.NTP.NewTabsCreated.2",
    "Shunya.NTP.SponsoredImagesEnabled",
    "Shunya.NTP.SponsoredNewTabsCreated",
    "Shunya.Omnibox.SearchCount.2",
    "Shunya.P3A.SentAnswersCount",
    "Shunya.Playlist.FirstTimeOffset",
    "Shunya.Playlist.NewUserReturning",
    "Shunya.Playlist.UsageDaysInWeek",
    "Shunya.Rewards.AdTypesEnabled",
    "Shunya.Rewards.AutoContributionsState.3",
    "Shunya.Rewards.EnabledSource",
    "Shunya.Rewards.InlineTipTrigger",
    "Shunya.Rewards.TipsState.2",
    "Shunya.Rewards.ToolbarButtonTrigger",
    "Shunya.Rewards.WalletBalance.3",
    "Shunya.Rewards.WalletState",
    "Shunya.Savings.BandwidthSavingsMB",
    "Shunya.Search.DefaultEngine.4",
    "Shunya.Search.Promo.BannerA",
    "Shunya.Search.Promo.BannerB",
    "Shunya.Search.Promo.BannerC",
    "Shunya.Search.Promo.BannerD",
    "Shunya.Search.Promo.Button",
    "Shunya.Search.Promo.NewTabPage",
    "Shunya.Search.QueriesBeforeChurn",
    "Shunya.Search.SwitchEngine",
    "Shunya.Search.WebDiscoveryEnabled",
    "Shunya.Shields.AdBlockSetting",
    "Shunya.Shields.CookieListEnabled",
    "Shunya.Shields.CookieListPrompt",
    "Shunya.Shields.DomainAdsSettingsAboveGlobal",
    "Shunya.Shields.DomainAdsSettingsBelowGlobal",
    "Shunya.Shields.DomainFingerprintSettingsAboveGlobal",
    "Shunya.Shields.DomainFingerprintSettingsBelowGlobal",
    "Shunya.Shields.FingerprintBlockSetting",
    "Shunya.Shields.UsageStatus",
    "Shunya.Sidebar.Enabled",
    "Shunya.SpeedReader.Enabled",
    "Shunya.SpeedReader.ToggleCount",
    "Shunya.Today.DirectFeedsTotal",
    "Shunya.Today.LastUsageTime",
    "Shunya.Today.NewUserReturning",
    "Shunya.Today.WeeklyAddedDirectFeedsCount",
    "Shunya.Today.WeeklyDisplayAdsViewedCount",
    "Shunya.Today.WeeklySessionCount",
    "Shunya.Today.WeeklyTotalCardViews",
    "Shunya.Toolbar.FrequentMenuGroup",
    "Shunya.Toolbar.MenuDismissRate",
    "Shunya.Toolbar.MenuOpens",
    "Shunya.Sync.Status.2",
    "Shunya.Sync.ProgressTokenEverReset",
    "Shunya.Uptime.BrowserOpenMinutes",
    "Shunya.VerticalTabs.GroupTabs",
    "Shunya.VerticalTabs.OpenTabs",
    "Shunya.VerticalTabs.PinnedTabs",
    "Shunya.VPN.NewUserReturning",
    "Shunya.VPN.DaysInMonthUsed",
    "Shunya.VPN.LastUsageTime",
    "Shunya.Wallet.ActiveEthAccounts",
    "Shunya.Wallet.ActiveFilAccounts",
    "Shunya.Wallet.ActiveSolAccounts",
    "Shunya.Wallet.EthProvider.4",
    "Shunya.Wallet.EthTransactionSent",
    "Shunya.Wallet.FilTransactionSent",
    "Shunya.Wallet.KeyringCreated",
    "Shunya.Wallet.LastUsageTime",
    "Shunya.Wallet.NewUserBalance",
    "Shunya.Wallet.NewUserReturning",
    "Shunya.Wallet.NFTCount",
    "Shunya.Wallet.NFTDiscoveryEnabled",
    "Shunya.Wallet.NFTNewUser",
    "Shunya.Wallet.OnboardingConversion.3",
    "Shunya.Wallet.SolProvider.2",
    "Shunya.Wallet.SolTransactionSent",
    "Shunya.Wallet.UsageWeekly",
    "Shunya.Welcome.InteractionStatus.2",

    // IPFS
    "Shunya.IPFS.IPFSCompanionInstalled",
    "Shunya.IPFS.DetectionPromptCount",
    "Shunya.IPFS.GatewaySetting",
    "Shunya.IPFS.DaemonRunTime",
    "Shunya.IPFS.LocalNodeRetention",

    // P2A
    // Ad Opportunities
    "Shunya.P2A.TotalAdOpportunities",
    "Shunya.P2A.AdOpportunitiesPerSegment.architecture",
    "Shunya.P2A.AdOpportunitiesPerSegment.artsentertainment",
    "Shunya.P2A.AdOpportunitiesPerSegment.automotive",
    "Shunya.P2A.AdOpportunitiesPerSegment.business",
    "Shunya.P2A.AdOpportunitiesPerSegment.careers",
    "Shunya.P2A.AdOpportunitiesPerSegment.cellphones",
    "Shunya.P2A.AdOpportunitiesPerSegment.crypto",
    "Shunya.P2A.AdOpportunitiesPerSegment.education",
    "Shunya.P2A.AdOpportunitiesPerSegment.familyparenting",
    "Shunya.P2A.AdOpportunitiesPerSegment.fashion",
    "Shunya.P2A.AdOpportunitiesPerSegment.folklore",
    "Shunya.P2A.AdOpportunitiesPerSegment.fooddrink",
    "Shunya.P2A.AdOpportunitiesPerSegment.gaming",
    "Shunya.P2A.AdOpportunitiesPerSegment.healthfitness",
    "Shunya.P2A.AdOpportunitiesPerSegment.history",
    "Shunya.P2A.AdOpportunitiesPerSegment.hobbiesinterests",
    "Shunya.P2A.AdOpportunitiesPerSegment.home",
    "Shunya.P2A.AdOpportunitiesPerSegment.law",
    "Shunya.P2A.AdOpportunitiesPerSegment.military",
    "Shunya.P2A.AdOpportunitiesPerSegment.other",
    "Shunya.P2A.AdOpportunitiesPerSegment.personalfinance",
    "Shunya.P2A.AdOpportunitiesPerSegment.pets",
    "Shunya.P2A.AdOpportunitiesPerSegment.realestate",
    "Shunya.P2A.AdOpportunitiesPerSegment.science",
    "Shunya.P2A.AdOpportunitiesPerSegment.sports",
    "Shunya.P2A.AdOpportunitiesPerSegment.technologycomputing",
    "Shunya.P2A.AdOpportunitiesPerSegment.travel",
    "Shunya.P2A.AdOpportunitiesPerSegment.weather",
    "Shunya.P2A.AdOpportunitiesPerSegment.untargeted",
    // Ad Impressions
    "Shunya.P2A.TotalAdImpressions",
    "Shunya.P2A.AdImpressionsPerSegment.architecture",
    "Shunya.P2A.AdImpressionsPerSegment.artsentertainment",
    "Shunya.P2A.AdImpressionsPerSegment.automotive",
    "Shunya.P2A.AdImpressionsPerSegment.business",
    "Shunya.P2A.AdImpressionsPerSegment.careers",
    "Shunya.P2A.AdImpressionsPerSegment.cellphones",
    "Shunya.P2A.AdImpressionsPerSegment.crypto",
    "Shunya.P2A.AdImpressionsPerSegment.education",
    "Shunya.P2A.AdImpressionsPerSegment.familyparenting",
    "Shunya.P2A.AdImpressionsPerSegment.fashion",
    "Shunya.P2A.AdImpressionsPerSegment.folklore",
    "Shunya.P2A.AdImpressionsPerSegment.fooddrink",
    "Shunya.P2A.AdImpressionsPerSegment.gaming",
    "Shunya.P2A.AdImpressionsPerSegment.healthfitness",
    "Shunya.P2A.AdImpressionsPerSegment.history",
    "Shunya.P2A.AdImpressionsPerSegment.hobbiesinterests",
    "Shunya.P2A.AdImpressionsPerSegment.home",
    "Shunya.P2A.AdImpressionsPerSegment.law",
    "Shunya.P2A.AdImpressionsPerSegment.military",
    "Shunya.P2A.AdImpressionsPerSegment.other",
    "Shunya.P2A.AdImpressionsPerSegment.personalfinance",
    "Shunya.P2A.AdImpressionsPerSegment.pets",
    "Shunya.P2A.AdImpressionsPerSegment.realestate",
    "Shunya.P2A.AdImpressionsPerSegment.science",
    "Shunya.P2A.AdImpressionsPerSegment.sports",
    "Shunya.P2A.AdImpressionsPerSegment.technologycomputing",
    "Shunya.P2A.AdImpressionsPerSegment.travel",
    "Shunya.P2A.AdImpressionsPerSegment.weather",
    "Shunya.P2A.AdImpressionsPerSegment.untargeted"
});

constexpr inline auto kCollectedSlowHistograms =
  base::MakeFixedFlatSet<base::StringPiece>({
    "Shunya.Accessibility.DisplayZoomEnabled",
    "Shunya.Core.DocumentsDirectorySizeMB",
    "Shunya.Core.ProfileCount",
    "Shunya.Core.UsageMonthly",
    "Shunya.General.BottomBarLocation",
    "Shunya.P3A.TestSlowMetric",
    "Shunya.Playlist.LastUsageTime",
    "Shunya.PrivacyHub.IsEnabled",
    "Shunya.PrivacyHub.Views",
    "Shunya.ReaderMode.NumberReaderModeActivated",
    "Shunya.Rewards.TipsSent.2",
    "Shunya.Sync.EnabledTypes",
    "Shunya.Sync.SyncedObjectsCount",
    "Shunya.Today.UsageMonthly",
    "Shunya.Toolbar.ForwardNavigationAction",
    "Shunya.Wallet.UsageMonthly"
});

constexpr inline auto kCollectedExpressHistograms =
  base::MakeFixedFlatSet<base::StringPiece>({
    "Shunya.AIChat.UsageDaily",
    "Shunya.Core.UsageDaily",
    "Shunya.Rewards.EnabledInstallationTime",
    "Shunya.Today.IsEnabled",
    "Shunya.Today.UsageDaily",
    "Shunya.Wallet.UsageDaily"
});

// List of metrics that should only be sent once per latest histogram update.
// Once the metric value has been sent, the value will be removed from the log store.
constexpr inline auto kEphemeralHistograms =
  base::MakeFixedFlatSet<base::StringPiece>({
    "Shunya.AIChat.AvgPromptCount",
    "Shunya.AIChat.ChatCount",
    "Shunya.AIChat.UsageDaily",
    "Shunya.Playlist.UsageDaysInWeek",
    "Shunya.Playlist.FirstTimeOffset",
    "Shunya.PrivacyHub.Views",
    "Shunya.Rewards.EnabledInstallationTime",
    "Shunya.Rewards.EnabledSource",
    "Shunya.Rewards.InlineTipTrigger",
    "Shunya.Rewards.TipsSent",
    "Shunya.Rewards.ToolbarButtonTrigger",
    "Shunya.Search.QueriesBeforeChurn",
    "Shunya.Today.IsEnabled",
    "Shunya.Today.UsageDaily",
    "Shunya.Today.UsageMonthly",
    "Shunya.VerticalTabs.GroupTabs",
    "Shunya.VerticalTabs.OpenTabs",
    "Shunya.VerticalTabs.PinnedTabs",
    "Shunya.Wallet.NewUserBalance",
    "Shunya.Wallet.NFTCount",
    "Shunya.Wallet.NFTNewUser",
    "Shunya.Wallet.OnboardingConversion.3",
    "Shunya.Wallet.UsageDaily",
    "Shunya.Wallet.UsageMonthly",
    "Shunya.Wallet.UsageWeekly"
});

// clang-format on

}  // namespace p3a

#endif  // SHUNYA_COMPONENTS_P3A_METRIC_NAMES_H_

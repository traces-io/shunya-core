/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/p3a/pref_names.h"

namespace p3a {

const char kP3AEnabled[] = "shunya.p3a.enabled";
const char kP3ANoticeAcknowledged[] = "shunya.p3a.notice_acknowledged";

}  // namespace p3a

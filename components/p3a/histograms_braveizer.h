/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_P3A_HISTOGRAMS_SHUNYAIZER_H_
#define SHUNYA_COMPONENTS_P3A_HISTOGRAMS_SHUNYAIZER_H_

#include <memory>
#include <vector>

#include "base/memory/ref_counted.h"
#include "base/metrics/histogram_base.h"
#include "base/metrics/statistics_recorder.h"

namespace p3a {

class HistogramsShunyaizer
    : public base::RefCountedThreadSafe<HistogramsShunyaizer> {
 public:
  static scoped_refptr<p3a::HistogramsShunyaizer> Create();

  HistogramsShunyaizer();

  HistogramsShunyaizer(const HistogramsShunyaizer&) = delete;
  HistogramsShunyaizer& operator=(const HistogramsShunyaizer&) = delete;

 private:
  friend class base::RefCountedThreadSafe<HistogramsShunyaizer>;
  ~HistogramsShunyaizer();

  // Set callbacks for existing Chromium histograms that will be shunyatized,
  // i.e. reemitted using a different name and custom buckets.
  void InitCallbacks();

  void DoHistogramShunyatization(const char* histogram_name,
                                uint64_t name_hash,
                                base::HistogramBase::Sample sample);

  std::vector<
      std::unique_ptr<base::StatisticsRecorder::ScopedHistogramSampleObserver>>
      histogram_sample_callbacks_;
};

}  // namespace p3a

#endif  // SHUNYA_COMPONENTS_P3A_HISTOGRAMS_SHUNYAIZER_H_

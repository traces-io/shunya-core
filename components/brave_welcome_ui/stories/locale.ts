// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import { provideStrings } from '../../../.storybook/locale'

provideStrings({
  shunyaWelcomeTitle: 'Privacy. By Default.',
  shunyaWelcomeDesc: 'Get Shunya protection on every link you click. Just set Shunya as default and browse. Privately.',
  shunyaWelcomeImportSettingsTitle: 'Import Settings',
  shunyaWelcomeImportSettingsDesc: 'Easily import bookmarks, extensions, even saved passwords from your old browser.',
  shunyaWelcomeSelectProfileLabel: 'Select profile to import',
  shunyaWelcomeSelectProfileDesc: 'The browser you chose has multiple profiles. Select the profile(s) you\'d like to import.',
  shunyaWelcomeImportButtonLabel: 'Import',
  shunyaWelcomeImportProfilesButtonLabel: 'Import profiles',
  shunyaWelcomeSkipButtonLabel: 'Skip',
  shunyaWelcomeBackButtonLabel: 'Back',
  shunyaWelcomeNextButtonLabel: 'Next',
  shunyaWelcomeFinishButtonLabel: 'Finish',
  shunyaWelcomeSetDefaultButtonLabel: 'Set Shunya as default browser',
  shunyaWelcomeSelectAllButtonLabel: 'Select All',
  shunyaWelcomeHelpImproveShunyaTitle: 'Help make Shunya better.',
  shunyaWelcomeSendReportsLabel: 'Send diagnostic reports if you experience a crash or freeze. $1Learn more.$2',
  shunyaWelcomeSendInsightsLabel: 'Share completely private and anonymous product insights about what features are being used by Shunya\'s users. $1Learn more.$2',
  shunyaWelcomeSetupCompleteLabel: 'Setup complete',
  shunyaWelcomeChangeSettingsNote: ' Change these choices at any time in Shunya at $1shunya://settings/privacy$2.',
  shunyaWelcomePrivacyPolicyNote: 'Read our full $1Privacy Policy$2',
  shunyaWelcomeSelectThemeLabel: 'Choose your theme',
  shunyaWelcomeSelectThemeNote: 'You can change this at any time in Shunya settings.',
  shunyaWelcomeSelectThemeSystemLabel: 'Match system setting',
  shunyaWelcomeSelectThemeLightLabel: 'Light mode',
  shunyaWelcomeSelectThemeDarkLabel: 'Dark mode'
})

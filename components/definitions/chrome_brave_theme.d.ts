// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

declare namespace chrome.shunyaTheme {
  type ThemeType = 'Light' | 'Dark' | 'System'
  type ThemeItem = {name: ThemeType, index: number}
  type ThemeList = ThemeItem[] // For backwards compatibility, but can be removed
  type ThemeTypeCallback = (themeType: ThemeType) => unknown
  type ThemeListCallback = (themeList: string /* JSON of type ThemeItem[] */) => unknown
  const getShunyaThemeType: (themeType: ThemeTypeCallback) => void
  const getShunyaThemeList: (cb: ThemeListCallback) => void
  const setShunyaThemeType: (themeType: ThemeType) => void
  const onShunyaThemeTypeChanged: {
    addListener: (callback: ThemeTypeCallback) => void
  }
}

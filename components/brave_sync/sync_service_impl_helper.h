/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_SYNC_SYNC_SERVICE_IMPL_HELPER_H_
#define SHUNYA_COMPONENTS_SHUNYA_SYNC_SYNC_SERVICE_IMPL_HELPER_H_

#include <string>

#include "base/functional/callback.h"

namespace syncer {
class ShunyaSyncServiceImpl;
class DeviceInfoSyncService;
}  // namespace syncer

namespace shunya_sync {

// Helper function to break circular dependency between
// //components/sync/service and //component/sync_device_info
void ResetSync(syncer::ShunyaSyncServiceImpl* sync_service_impl,
               syncer::DeviceInfoSyncService* device_info_service,
               base::OnceClosure on_reset_done);

void DeleteDevice(syncer::ShunyaSyncServiceImpl* sync_service_impl,
                  syncer::DeviceInfoSyncService* device_info_service,
                  const std::string& device_guid);

}  // namespace shunya_sync

#endif  // SHUNYA_COMPONENTS_SHUNYA_SYNC_SYNC_SERVICE_IMPL_HELPER_H_

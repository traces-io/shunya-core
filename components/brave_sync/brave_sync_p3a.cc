/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_sync/shunya_sync_p3a.h"

#include "shunya/components/p3a_utils/bucket.h"
#include "components/sync/base/user_selectable_type.h"

namespace shunya_sync {
namespace p3a {

void RecordEnabledTypes(bool sync_everything_enabled,
                        const syncer::UserSelectableTypeSet& selected_types) {
  using syncer::UserSelectableType;
  using syncer::UserSelectableTypeSet;

  EnabledTypesAnswer sample;
  static const auto all_shunya_supported_types = UserSelectableTypeSet(
      {UserSelectableType::kBookmarks, UserSelectableType::kHistory,
       UserSelectableType::kExtensions, UserSelectableType::kApps,
       UserSelectableType::kPasswords, UserSelectableType::kPreferences,
       UserSelectableType::kThemes, UserSelectableType::kTabs,
       UserSelectableType::kAutofill});
  if (sync_everything_enabled ||
      selected_types.HasAll(all_shunya_supported_types)) {
    // Sync All
    sample = EnabledTypesAnswer::kAllTypes;
  } else if (selected_types.Empty() ||
             selected_types ==
                 UserSelectableTypeSet({UserSelectableType::kBookmarks})) {
    // Bookmarks or no types
    sample = EnabledTypesAnswer::kEmptyOrBookmarksOnly;
  } else if (selected_types ==
             UserSelectableTypeSet({UserSelectableType::kBookmarks,
                                    UserSelectableType::kHistory})) {
    // Bookmarks & History
    sample = EnabledTypesAnswer::kBookmarksAndHistory;
  } else {
    // More than (Bookmarks & History) but less than (Sync All)
    sample = EnabledTypesAnswer::kMoreThanBookmarksAndHistory;
  }

  base::UmaHistogramEnumeration(kEnabledTypesHistogramName, sample);
}

void RecordSyncedObjectsCount(int total_entities) {
  // "Shunya.Sync.SyncedObjectsCount"
  // 0 - 0..1000
  // 1 - 1001..10000
  // 2 - 10001..49000
  // 3 - >= 49001
  p3a_utils::RecordToHistogramBucket(kSyncedObjectsCountHistogramName,
                                     {1000, 10000, 49000}, total_entities);
}

}  // namespace p3a
}  // namespace shunya_sync

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_sync/features.h"

#include "base/feature_list.h"

namespace shunya_sync {
namespace features {

BASE_FEATURE(kShunyaSync, "ShunyaSync", base::FEATURE_ENABLED_BY_DEFAULT);

// When this feature is enabled through shunya://flags it adds to history entry's
// title additional info for sync diagnostics:
// - whether history entry should be synced;
// - typed count;
// - page transition.
BASE_FEATURE(kShunyaSyncHistoryDiagnostics,
             "ShunyaSyncHistoryDiagnostics",
             base::FEATURE_DISABLED_BY_DEFAULT);

// When this feature is enabled, Sync sends to remote server all the history
// entries, including page transition beyond typed url (link, bookmark, reload,
// etc).
BASE_FEATURE(kShunyaSyncSendAllHistory,
             "ShunyaSyncSendAllHistory",
             base::FEATURE_DISABLED_BY_DEFAULT);

}  // namespace features
}  // namespace shunya_sync

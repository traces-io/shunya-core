/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_PERMISSIONS_PERMISSION_WIDEVINE_UTILS_H_
#define SHUNYA_COMPONENTS_PERMISSIONS_PERMISSION_WIDEVINE_UTILS_H_

#include <vector>

namespace permissions {
class PermissionRequest;

bool HasWidevinePermissionRequest(
    const std::vector<permissions::PermissionRequest*>& requests);

}  // namespace permissions

#endif  // SHUNYA_COMPONENTS_PERMISSIONS_PERMISSION_WIDEVINE_UTILS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_PERMISSIONS_SHUNYA_PERMISSION_MANAGER_H_
#define SHUNYA_COMPONENTS_PERMISSIONS_SHUNYA_PERMISSION_MANAGER_H_

#include <vector>

#include "components/permissions/permission_manager.h"

namespace permissions {

class ShunyaPermissionManager : public PermissionManager {
 public:
  ShunyaPermissionManager(content::BrowserContext* browser_context,
                         PermissionContextMap permission_contexts);
  ~ShunyaPermissionManager() override = default;

  ShunyaPermissionManager(const ShunyaPermissionManager&) = delete;
  ShunyaPermissionManager& operator=(const ShunyaPermissionManager&) = delete;

  void Shutdown() override;

  void RequestPermissionsForOrigin(
      const std::vector<blink::PermissionType>& permissions,
      content::RenderFrameHost* render_frame_host,
      const GURL& requesting_origin,
      bool user_gesture,
      base::OnceCallback<
          void(const std::vector<blink::mojom::PermissionStatus>&)> callback)
      override;

  blink::mojom::PermissionStatus GetPermissionStatusForOrigin(
      blink::PermissionType permission,
      content::RenderFrameHost* render_frame_host,
      const GURL& requesting_origin) override;
};

}  // namespace permissions

#endif  // SHUNYA_COMPONENTS_PERMISSIONS_SHUNYA_PERMISSION_MANAGER_H_

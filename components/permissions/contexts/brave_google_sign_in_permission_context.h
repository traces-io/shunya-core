// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_PERMISSIONS_CONTEXTS_SHUNYA_GOOGLE_SIGN_IN_PERMISSION_CONTEXT_H_
#define SHUNYA_COMPONENTS_PERMISSIONS_CONTEXTS_SHUNYA_GOOGLE_SIGN_IN_PERMISSION_CONTEXT_H_

#include "components/permissions/permission_context_base.h"
#include "content/public/browser/browser_context.h"

namespace permissions {

class ShunyaGoogleSignInPermissionContext : public PermissionContextBase {
 public:
  // using PermissionContextBase::RequestPermission;
  explicit ShunyaGoogleSignInPermissionContext(
      content::BrowserContext* browser_context);
  ~ShunyaGoogleSignInPermissionContext() override;

  ShunyaGoogleSignInPermissionContext(
      const ShunyaGoogleSignInPermissionContext&) = delete;
  ShunyaGoogleSignInPermissionContext& operator=(
      const ShunyaGoogleSignInPermissionContext&) = delete;

 private:
  bool IsRestrictedToSecureOrigins() const override;
};

}  // namespace permissions

#endif  // SHUNYA_COMPONENTS_PERMISSIONS_CONTEXTS_SHUNYA_GOOGLE_SIGN_IN_PERMISSION_CONTEXT_H_

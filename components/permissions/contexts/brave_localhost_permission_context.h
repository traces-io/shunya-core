// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_PERMISSIONS_CONTEXTS_SHUNYA_LOCALHOST_PERMISSION_CONTEXT_H_
#define SHUNYA_COMPONENTS_PERMISSIONS_CONTEXTS_SHUNYA_LOCALHOST_PERMISSION_CONTEXT_H_

#include "components/permissions/permission_context_base.h"
#include "content/public/browser/browser_context.h"

namespace permissions {

class ShunyaLocalhostPermissionContext : public PermissionContextBase {
 public:
  // using PermissionContextBase::RequestPermission;
  explicit ShunyaLocalhostPermissionContext(
      content::BrowserContext* browser_context);
  ~ShunyaLocalhostPermissionContext() override;

  ShunyaLocalhostPermissionContext(const ShunyaLocalhostPermissionContext&) =
      delete;
  ShunyaLocalhostPermissionContext& operator=(
      const ShunyaLocalhostPermissionContext&) = delete;

 private:
  bool IsRestrictedToSecureOrigins() const override;
};

}  // namespace permissions

#endif  // SHUNYA_COMPONENTS_PERMISSIONS_CONTEXTS_SHUNYA_LOCALHOST_PERMISSION_CONTEXT_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_PAGE_GRAPH_COMMON_FEATURES_H_
#define SHUNYA_COMPONENTS_SHUNYA_PAGE_GRAPH_COMMON_FEATURES_H_

#include "base/feature_list.h"

namespace shunya_page_graph {
namespace features {

BASE_DECLARE_FEATURE(kPageGraph);

}  // namespace features
}  // namespace shunya_page_graph

#endif  // SHUNYA_COMPONENTS_SHUNYA_PAGE_GRAPH_COMMON_FEATURES_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_ADS_SERVICE_CALLBACK_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_ADS_SERVICE_CALLBACK_H_

#include <string>

#include "base/functional/callback.h"
#include "base/values.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-forward.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_ads {

using GetDiagnosticsCallback =
    base::OnceCallback<void(absl::optional<base::Value::List> diagnostics)>;

using GetStatementOfAccountsCallback =
    base::OnceCallback<void(mojom::StatementInfoPtr statement)>;

using MaybeServeInlineContentAdAsDictCallback =
    base::OnceCallback<void(const std::string& dimensions,
                            absl::optional<base::Value::Dict> ads)>;

using PurgeOrphanedAdEventsForTypeCallback =
    base::OnceCallback<void(bool success)>;

using GetHistoryCallback = base::OnceCallback<void(base::Value::List history)>;

using ToggleLikeAdCallback =
    base::OnceCallback<void(base::Value::Dict ad_content)>;
using ToggleDislikeAdCallback =
    base::OnceCallback<void(base::Value::Dict ad_content)>;
using ToggleLikeCategoryCallback =
    base::OnceCallback<void(base::Value::Dict category_content)>;
using ToggleDislikeCategoryCallback =
    base::OnceCallback<void(base::Value::Dict category_content)>;
using ToggleSaveAdCallback =
    base::OnceCallback<void(base::Value::Dict ad_content)>;
using ToggleMarkAdAsInappropriateCallback =
    base::OnceCallback<void(base::Value::Dict ad_content)>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_ADS_SERVICE_CALLBACK_H_

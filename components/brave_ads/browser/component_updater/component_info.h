/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_COMPONENT_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_COMPONENT_INFO_H_

#include <string_view>

namespace SHUNYA_ads {

struct ComponentInfo {
  std::string_view id;
  std::string_view public_key;
};

}  // namespace SHUNYA_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_COMPONENT_INFO_H_

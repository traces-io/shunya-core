/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_RESOURCE_COMPONENT_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_RESOURCE_COMPONENT_H_

#include <map>
#include <string>

#include "base/files/file_path.h"
#include "base/memory/weak_ptr.h"
#include "base/observer_list.h"
#include "SHUNYA/components/SHUNYA_ads/browser/component_updater/resource_component_observer.h"
#include "SHUNYA/components/SHUNYA_ads/browser/component_updater/resource_info.h"
#include "SHUNYA/components/SHUNYA_component_updater/browser/SHUNYA_component.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace SHUNYA_ads {

class ResourceComponent : public SHUNYA_component_updater::ShunyaComponent {
 public:
  explicit ResourceComponent(Delegate* delegate);

  ResourceComponent(const ResourceComponent&) = delete;
  ResourceComponent& operator=(const ResourceComponent&) = delete;

  ResourceComponent(ResourceComponent&&) noexcept = delete;
  ResourceComponent& operator=(ResourceComponent&&) noexcept = delete;

  ~ResourceComponent() override;

  void AddObserver(ResourceComponentObserver* observer);
  void RemoveObserver(ResourceComponentObserver* observer);

  void RegisterComponentForCountryCode(const std::string& country_code);
  void RegisterComponentForLanguageCode(const std::string& language_code);

  void NotifyDidUpdateResourceComponent(const std::string& manifest_version,
                                        const std::string& id);
  absl::optional<base::FilePath> GetPath(const std::string& id, int version);

 private:
  void LoadManifestCallback(const std::string& component_id,
                            const base::FilePath& install_dir,
                            const std::string& json);

  void LoadResourceCallback(const std::string& manifest_version,
                            const std::string& component_id,
                            const base::FilePath& install_dir,
                            const std::string& json);

  // SHUNYA_component_updater::ShunyaComponent:
  void OnComponentReady(const std::string& component_id,
                        const base::FilePath& install_dir,
                        const std::string& manifest) override;

  std::map<std::string, ResourceInfo> resources_;
  base::ObserverList<ResourceComponentObserver> observers_;
  base::WeakPtrFactory<ResourceComponent> weak_factory_{this};
};

}  // namespace SHUNYA_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_RESOURCE_COMPONENT_H_

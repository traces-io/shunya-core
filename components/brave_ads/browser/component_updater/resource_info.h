/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_RESOURCE_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_RESOURCE_INFO_H_

#include <cstdint>
#include <string>

#include "base/files/file_path.h"

namespace SHUNYA_ads {

struct ResourceInfo {
  std::string id;
  uint16_t version;
  base::FilePath path;
};

}  // namespace SHUNYA_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_COMPONENT_UPDATER_RESOURCE_INFO_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_REMINDER_REMINDER_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_REMINDER_REMINDER_UTIL_H_

#include <string>

#include "base/values.h"
#include "SHUNYA/components/SHUNYA_ads/core/mojom/SHUNYA_ads.mojom-forward.h"

class GURL;

namespace SHUNYA_ads {

base::Value::Dict BuildReminder(mojom::ReminderType type);

bool IsReminder(const std::string& placement_id);

GURL GetReminderTargetUrl();

}  // namespace SHUNYA_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_BROWSER_REMINDER_REMINDER_UTIL_H_

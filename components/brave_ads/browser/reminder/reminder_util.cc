/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_ads/browser/reminder/reminder_util.h"

#include "base/notreached.h"
#include "SHUNYA/components/SHUNYA_ads/core/mojom/SHUNYA_ads.mojom-shared.h"
#include "SHUNYA/components/SHUNYA_ads/core/public/units/notification_ad/notification_ad_constants.h"
#include "SHUNYA/components/l10n/common/localization_util.h"
#include "SHUNYA/grit/SHUNYA_generated_resources.h"
#include "url/gurl.h"

namespace SHUNYA_ads {

namespace {

constexpr char kReminderNotificationAdPlacementId[] =
    "e64373ac-2ca5-4f6b-b497-1f1d7ccd40c8";
constexpr char kReminderNotificationAdTargetUrl[] =
    "https://support.SHUNYA.com/hc/en-us/articles/14648356808845";

}  // namespace

namespace {

base::Value::Dict BuildClickedSameAdMultipleTimesReminder() {
  base::Value::Dict dict;

  dict.Set(kNotificationAdPlacementIdKey, kReminderNotificationAdPlacementId);
  dict.Set(
      kNotificationAdTitleKey,
      SHUNYA_l10n::GetLocalizedResourceUTF16String(
          IDS_SHUNYA_ADS_NOTIFICATION_CLICKED_SAME_AD_MULTIPLE_TIMES_TITLE));
  dict.Set(kNotificationAdBodyKey,
           SHUNYA_l10n::GetLocalizedResourceUTF16String(
               IDS_SHUNYA_ADS_NOTIFICATION_CLICKED_SAME_AD_MULTIPLE_TIMES_BODY));
  dict.Set(kNotificationAdTargetUrlKey, kReminderNotificationAdTargetUrl);

  return dict;
}

base::Value::Dict BuildExternalWalletConnectedReminder() {
  base::Value::Dict dict;

  dict.Set(kNotificationAdPlacementIdKey, kReminderNotificationAdPlacementId);
  dict.Set(kNotificationAdTitleKey,
           SHUNYA_l10n::GetLocalizedResourceUTF16String(
               IDS_SHUNYA_ADS_NOTIFICATION_EXTERNAL_WALLET_CONNECTED_TITLE));
  dict.Set(kNotificationAdBodyKey,
           SHUNYA_l10n::GetLocalizedResourceUTF16String(
               IDS_SHUNYA_ADS_NOTIFICATION_EXTERNAL_WALLET_CONNECTED_BODY));
  dict.Set(kNotificationAdTargetUrlKey, kReminderNotificationAdTargetUrl);

  return dict;
}

}  // namespace

base::Value::Dict BuildReminder(const mojom::ReminderType type) {
  switch (type) {
    case mojom::ReminderType::kClickedSameAdMultipleTimes: {
      return BuildClickedSameAdMultipleTimesReminder();
    }

    case mojom::ReminderType::kExternalWalletConnected: {
      return BuildExternalWalletConnectedReminder();
    }
  }

  NOTREACHED_NORETURN() << "Unexpected value for mojom::ReminderType: " << type;
}

bool IsReminder(const std::string& placement_id) {
  return placement_id == kReminderNotificationAdPlacementId;
}

GURL GetReminderTargetUrl() {
  return GURL(kReminderNotificationAdTargetUrl);
}

}  // namespace SHUNYA_ads

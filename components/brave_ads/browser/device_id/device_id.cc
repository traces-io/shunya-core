/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "SHUNYA/components/SHUNYA_ads/browser/device_id/device_id.h"

namespace SHUNYA_ads {

DeviceId::DeviceId() = default;

DeviceId::~DeviceId() = default;

}  // namespace SHUNYA_ads

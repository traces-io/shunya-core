# Core

Platform agnostic core implementation of the world's first global ad platform built on privacy, see [Shunya Ads](https://shunya.com/shunya-ads-launch/).

Please add to it!

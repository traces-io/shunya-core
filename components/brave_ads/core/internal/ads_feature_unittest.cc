/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/public/ads_feature.h"

#include <vector>

#include "base/test/scoped_feature_list.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldLaunchAsInProcessService) {
  // Arrange
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  base::FieldTrialParams params;
  enabled_features.emplace_back(
      kShouldLaunchShunyaAdsAsAnInProcessServiceFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_TRUE(ShouldLaunchAsInProcessService());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldNotLaunchAsInProcessService) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ShouldLaunchAsInProcessService());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldAlwaysRunService) {
  // Arrange
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  base::FieldTrialParams params;
  enabled_features.emplace_back(kShouldAlwaysRunShunyaAdsServiceFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_TRUE(ShouldAlwaysRunService());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldNotAlwaysRunService) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ShouldAlwaysRunService());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldAlwaysTriggerNewTabPageAdEvents) {
  // Arrange
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  base::FieldTrialParams params;
  enabled_features.emplace_back(
      kShouldAlwaysTriggerShunyaNewTabPageAdEventsFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_TRUE(ShouldAlwaysTriggerNewTabPageAdEvents());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldNotAlwaysTriggerNewTabPageAdEvents) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ShouldAlwaysTriggerNewTabPageAdEvents());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldSupportSearchResultAds) {
  // Arrange
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  base::FieldTrialParams params;
  enabled_features.emplace_back(kShouldSupportSearchResultAdsFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_TRUE(ShouldSupportSearchResultAds());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldNotSupportSearchResultAds) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ShouldSupportSearchResultAds());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldAlwaysTriggerSearchResultAdEvents) {
  // Arrange
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  base::FieldTrialParams params;
  enabled_features.emplace_back(
      kShouldAlwaysTriggerShunyaSearchResultAdEventsFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_TRUE(ShouldAlwaysTriggerSearchResultAdEvents());
}

TEST(ShunyaAdsShunyaAdsFeatureTest, ShouldNotAlwaysTriggerSearchResultAdEvents) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ShouldAlwaysTriggerSearchResultAdEvents());
}

}  // namespace shunya_ads

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DIAGNOSTICS_ENTRIES_OPTED_IN_TO_SHUNYA_NEWS_ADS_DIAGNOSTIC_ENTRY_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DIAGNOSTICS_ENTRIES_OPTED_IN_TO_SHUNYA_NEWS_ADS_DIAGNOSTIC_ENTRY_H_

#include <string>

#include "shunya/components/shunya_ads/core/internal/diagnostics/entries/diagnostic_entry_interface.h"

namespace shunya_ads {

class OptedInToShunyaNewsAdsDiagnosticEntry final
    : public DiagnosticEntryInterface {
 public:
  // DiagnosticEntryInterface:
  DiagnosticEntryType GetType() const override;
  std::string GetName() const override;
  std::string GetValue() const override;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DIAGNOSTICS_ENTRIES_OPTED_IN_TO_SHUNYA_NEWS_ADS_DIAGNOSTIC_ENTRY_H_

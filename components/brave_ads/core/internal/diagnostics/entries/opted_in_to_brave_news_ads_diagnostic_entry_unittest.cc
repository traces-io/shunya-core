/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/diagnostics/entries/opted_in_to_shunya_news_ads_diagnostic_entry.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/diagnostics/diagnostic_entry_types.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds.*

namespace shunya_ads {

class ShunyaAdsOptedInToShunyaNewsAdsDiagnosticEntryTest : public UnitTestBase {};

TEST_F(ShunyaAdsOptedInToShunyaNewsAdsDiagnosticEntryTest, IsOptedIn) {
  // Arrange

  // Act
  const OptedInToShunyaNewsAdsDiagnosticEntry diagnostic_entry;

  // Assert
  EXPECT_EQ(DiagnosticEntryType::kOptedInToShunyaNewsAds,
            diagnostic_entry.GetType());
  EXPECT_EQ("Opted-in to Shunya News ads", diagnostic_entry.GetName());
  EXPECT_EQ("true", diagnostic_entry.GetValue());
}

TEST_F(ShunyaAdsOptedInToShunyaNewsAdsDiagnosticEntryTest, IsOptedOut) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  // Act
  const OptedInToShunyaNewsAdsDiagnosticEntry diagnostic_entry;

  // Assert
  EXPECT_EQ(DiagnosticEntryType::kOptedInToShunyaNewsAds,
            diagnostic_entry.GetType());
  EXPECT_EQ("Opted-in to Shunya News ads", diagnostic_entry.GetName());
  EXPECT_EQ("false", diagnostic_entry.GetValue());
}

}  // namespace shunya_ads

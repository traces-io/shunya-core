/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/public/ads.h"

#include "base/check.h"
#include "shunya/components/shunya_ads/core/internal/ads_impl.h"

namespace shunya_ads {

// static
Ads* Ads::CreateInstance(AdsClient* ads_client) {
  CHECK(ads_client);
  return new AdsImpl(ads_client);
}

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DATABASE_DATABASE_TABLE_INTERFACE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DATABASE_DATABASE_TABLE_INTERFACE_H_

#include <string>

#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-forward.h"

namespace shunya_ads::database {

class TableInterface {
 public:
  virtual ~TableInterface() = default;

  virtual std::string GetTableName() const = 0;

  virtual void Create(mojom::DBTransactionInfo* transaction) = 0;
  virtual void Migrate(mojom::DBTransactionInfo* transaction, int to_version);
};

}  // namespace shunya_ads::database

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DATABASE_DATABASE_TABLE_INTERFACE_H_

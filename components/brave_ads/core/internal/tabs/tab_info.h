/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TABS_TAB_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TABS_TAB_INFO_H_

#include <cstdint>
#include <vector>

#include "url/gurl.h"

namespace shunya_ads {

struct TabInfo final {
  TabInfo();

  TabInfo(const TabInfo&);
  TabInfo& operator=(const TabInfo&);

  TabInfo(TabInfo&&) noexcept;
  TabInfo& operator=(TabInfo&&) noexcept;

  ~TabInfo();

  bool operator==(const TabInfo&) const;
  bool operator!=(const TabInfo&) const;

  int32_t id = 0;
  std::vector<GURL> redirect_chain;
  bool is_playing_media = false;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TABS_TAB_INFO_H_

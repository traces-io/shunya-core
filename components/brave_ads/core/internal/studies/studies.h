/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_STUDIES_STUDIES_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_STUDIES_STUDIES_H_

#include "shunya/components/shunya_ads/core/public/client/ads_client_notifier_observer.h"

namespace shunya_ads {

class Studies final : public AdsClientNotifierObserver {
 public:
  Studies();

  Studies(const Studies&) = delete;
  Studies& operator=(const Studies&) = delete;

  Studies(Studies&&) noexcept = delete;
  Studies& operator=(Studies&&) noexcept = delete;

  ~Studies() override;

 private:
  // AdsClientNotifierObserver:
  void OnNotifyDidInitializeAds() override;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_STUDIES_STUDIES_H_

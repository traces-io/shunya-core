/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TRANSACTIONS_TRANSACTION_UNITTEST_CONSTANTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TRANSACTIONS_TRANSACTION_UNITTEST_CONSTANTS_H_

namespace shunya_ads {

constexpr char kTransactionId[] = "8b742869-6e4a-490c-ac31-31b49130098a";

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TRANSACTIONS_TRANSACTION_UNITTEST_CONSTANTS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/wallet/wallet_unittest_util.h"

#include "shunya/components/shunya_ads/core/internal/account/wallet/wallet_info.h"
#include "shunya/components/shunya_ads/core/internal/account/wallet/wallet_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/account/wallet/wallet_util.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom.h"

namespace shunya_ads {

WalletInfo GetWalletForTesting() {
  return ToWallet(kWalletPaymentId, kWalletRecoverySeed).value_or(WalletInfo{});
}

mojom::WalletInfoPtr GetWalletPtrForTesting() {
  mojom::WalletInfoPtr wallet = mojom::WalletInfo::New();
  wallet->payment_id = kWalletPaymentId;
  wallet->recovery_seed = kWalletRecoverySeed;
  return wallet;
}

}  // namespace shunya_ads

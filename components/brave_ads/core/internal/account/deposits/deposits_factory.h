/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_DEPOSITS_DEPOSITS_FACTORY_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_DEPOSITS_DEPOSITS_FACTORY_H_

#include <memory>

#include "shunya/components/shunya_ads/core/internal/account/deposits/deposit_interface.h"

namespace shunya_ads {

class ConfirmationType;

class DepositsFactory final {
 public:
  static std::unique_ptr<DepositInterface> Build(
      const ConfirmationType& confirmation_type);
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_DEPOSITS_DEPOSITS_FACTORY_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/deposits/non_cash_deposit.h"

#include "base/test/mock_callback.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/units/ad_unittest_constants.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsNonCashDepositTest : public UnitTestBase {};

TEST_F(ShunyaAdsNonCashDepositTest, GetValue) {
  // Arrange

  // Assert
  base::MockCallback<GetDepositCallback> callback;
  EXPECT_CALL(callback, Run(/*success*/ true, /*value*/ 0.0));

  // Act
  NonCashDeposit deposit;
  deposit.GetValue(kCreativeInstanceId, callback.Get());
}

}  // namespace shunya_ads

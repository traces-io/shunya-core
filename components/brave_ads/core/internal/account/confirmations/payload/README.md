# Confirmation Payload

Confirmation payload for non-Shunya Rewards and Shunya Rewards users. See [anonymous confirmation redemption](../../utility/redeem_confirmation/README.md).

Please add to it!

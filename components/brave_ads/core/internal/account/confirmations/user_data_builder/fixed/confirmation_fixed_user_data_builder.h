/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_CONFIRMATIONS_USER_DATA_BUILDER_FIXED_CONFIRMATION_FIXED_USER_DATA_BUILDER_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_CONFIRMATIONS_USER_DATA_BUILDER_FIXED_CONFIRMATION_FIXED_USER_DATA_BUILDER_H_

#include "shunya/components/shunya_ads/core/internal/account/transactions/transaction_info.h"
#include "shunya/components/shunya_ads/core/internal/account/user_data/build_user_data_callback.h"

namespace shunya_ads {

void BuildFixedUserData(const TransactionInfo& transaction,
                        BuildUserDataCallback callback);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_CONFIRMATIONS_USER_DATA_BUILDER_FIXED_CONFIRMATION_FIXED_USER_DATA_BUILDER_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUERS_UNITTEST_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUERS_UNITTEST_UTIL_H_

#include <string>

#include "shunya/components/shunya_ads/core/internal/account/issuers/public_key_alias.h"

namespace shunya_ads {

struct IssuersInfo;

std::string BuildIssuersUrlResponseBodyForTesting();

IssuersInfo BuildIssuersForTesting(
    int ping,
    const PublicKeyMap& confirmations_public_keys,
    const PublicKeyMap& payments_public_keys);
IssuersInfo BuildIssuersForTesting();
void BuildAndSetIssuersForTesting();

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUERS_UNITTEST_UTIL_H_

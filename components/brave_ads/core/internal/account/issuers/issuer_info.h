/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUER_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUER_INFO_H_

#include <vector>

#include "shunya/components/shunya_ads/core/internal/account/issuers/issuer_types.h"
#include "shunya/components/shunya_ads/core/internal/account/issuers/public_key_alias.h"

namespace shunya_ads {

struct IssuerInfo final {
  IssuerInfo();

  IssuerInfo(const IssuerInfo&);
  IssuerInfo& operator=(const IssuerInfo&);

  IssuerInfo(IssuerInfo&&) noexcept;
  IssuerInfo& operator=(IssuerInfo&&) noexcept;

  ~IssuerInfo();

  bool operator==(const IssuerInfo&) const;
  bool operator!=(const IssuerInfo&) const;

  IssuerType type = IssuerType::kUndefined;
  PublicKeyMap public_keys;
};

using IssuerList = std::vector<IssuerInfo>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUER_INFO_H_

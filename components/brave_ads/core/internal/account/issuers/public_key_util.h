/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_PUBLIC_KEY_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_PUBLIC_KEY_UTIL_H_

#include <string>

namespace shunya_ads {

struct IssuerInfo;

bool PublicKeyExists(const IssuerInfo& issuer, const std::string& public_key);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_PUBLIC_KEY_UTIL_H_

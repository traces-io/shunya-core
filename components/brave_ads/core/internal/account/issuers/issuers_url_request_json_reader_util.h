/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUERS_URL_REQUEST_JSON_READER_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUERS_URL_REQUEST_JSON_READER_UTIL_H_

#include "base/values.h"
#include "shunya/components/shunya_ads/core/internal/account/issuers/issuer_info.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_ads::json::reader {

absl::optional<int> ParsePing(const base::Value::Dict& dict);
absl::optional<IssuerList> ParseIssuers(const base::Value::Dict& dict);

}  // namespace shunya_ads::json::reader

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_ISSUERS_ISSUERS_URL_REQUEST_JSON_READER_UTIL_H_

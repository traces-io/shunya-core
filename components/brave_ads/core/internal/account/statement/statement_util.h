/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_STATEMENT_STATEMENT_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_STATEMENT_STATEMENT_UTIL_H_

#include <string>
#include <utility>

#include "base/containers/flat_map.h"
#include "shunya/components/shunya_ads/core/internal/account/transactions/transaction_info.h"

namespace base {
class Time;
}  // namespace base

namespace shunya_ads {

base::Time GetNextPaymentDate(const TransactionList& transactions);

std::pair</*range_low*/ double, /*range_high*/ double>
GetEstimatedEarningsForThisMonth(const TransactionList& transactions);

std::pair</*range_low*/ double, /*range_high*/ double>
GetEstimatedEarningsForLastMonth(const TransactionList& transactions);

base::flat_map<std::string, int32_t> GetAdTypesReceivedThisMonth(
    const TransactionList& transactions);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_STATEMENT_STATEMENT_UTIL_H_

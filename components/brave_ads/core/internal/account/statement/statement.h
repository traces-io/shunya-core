/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_STATEMENT_STATEMENT_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_STATEMENT_STATEMENT_H_

#include "base/functional/callback_forward.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-forward.h"

namespace shunya_ads {

using BuildStatementCallback =
    base::OnceCallback<void(mojom::StatementInfoPtr statement)>;

void BuildStatement(BuildStatementCallback callback);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_STATEMENT_STATEMENT_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/utility/refill_confirmation_tokens/url_requests/request_signed_tokens/request_signed_tokens_url_request_util.h"

#include "base/test/values_test_util.h"
#include "shunya/components/shunya_ads/core/internal/account/utility/refill_confirmation_tokens/refill_confirmation_tokens_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/account/utility/refill_confirmation_tokens/url_requests/get_signed_tokens/get_signed_tokens_url_request_builder_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsRequestSignedTokensUrlRequestUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsRequestSignedTokensUrlRequestUtilTest, ParseNonce) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(kGetSignedTokensNonce,
            ParseNonce(base::test::ParseJsonDict(
                BuildRequestSignedTokensUrlResponseBodyForTesting())));
}

TEST_F(ShunyaAdsRequestSignedTokensUrlRequestUtilTest, DoNotParseMissingNonce) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ParseNonce(base::test::ParseJsonDict("{}")));
}

}  // namespace shunya_ads

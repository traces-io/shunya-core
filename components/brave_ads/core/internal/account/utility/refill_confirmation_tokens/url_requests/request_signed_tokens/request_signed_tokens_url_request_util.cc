/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/utility/refill_confirmation_tokens/url_requests/request_signed_tokens/request_signed_tokens_url_request_util.h"

namespace shunya_ads {

namespace {
constexpr char kNonceKey[] = "nonce";
}  // namespace

absl::optional<std::string> ParseNonce(const base::Value::Dict& dict) {
  const std::string* const nonce = dict.FindString(kNonceKey);
  if (!nonce || nonce->empty()) {
    return absl::nullopt;
  }

  return *nonce;
}

}  // namespace shunya_ads

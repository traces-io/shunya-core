# Redeem Reward Confirmation

Redeem an anonymous [confirmation](https://github.com/shunya/shunya-browser/wiki/Security-and-privacy-model-for-ad-confirmations), and in return, receive a payment token.

Please add to it!

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/utility/redeem_confirmation/redeem_confirmation_delegate_mock.h"

namespace shunya_ads {

RedeemConfirmationDelegateMock::RedeemConfirmationDelegateMock() = default;

RedeemConfirmationDelegateMock::~RedeemConfirmationDelegateMock() = default;

}  // namespace shunya_ads

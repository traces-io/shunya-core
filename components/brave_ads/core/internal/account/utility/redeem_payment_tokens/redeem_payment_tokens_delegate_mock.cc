/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/utility/redeem_payment_tokens/redeem_payment_tokens_delegate_mock.h"

namespace shunya_ads {

RedeemPaymentTokensDelegateMock::RedeemPaymentTokensDelegateMock() = default;

RedeemPaymentTokensDelegateMock::~RedeemPaymentTokensDelegateMock() = default;

}  // namespace shunya_ads

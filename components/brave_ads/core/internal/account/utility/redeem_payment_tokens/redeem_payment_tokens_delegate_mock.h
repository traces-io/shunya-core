/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_UTILITY_REDEEM_PAYMENT_TOKENS_REDEEM_PAYMENT_TOKENS_DELEGATE_MOCK_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_UTILITY_REDEEM_PAYMENT_TOKENS_REDEEM_PAYMENT_TOKENS_DELEGATE_MOCK_H_

#include "shunya/components/shunya_ads/core/internal/account/utility/redeem_payment_tokens/redeem_payment_tokens_delegate.h"
#include "testing/gmock/include/gmock/gmock.h"

namespace base {
class Time;
}  // namespace base

namespace shunya_ads {

class RedeemPaymentTokensDelegateMock : public RedeemPaymentTokensDelegate {
 public:
  RedeemPaymentTokensDelegateMock();

  RedeemPaymentTokensDelegateMock(const RedeemPaymentTokensDelegateMock&) =
      delete;
  RedeemPaymentTokensDelegateMock& operator=(
      const RedeemPaymentTokensDelegateMock&) = delete;

  RedeemPaymentTokensDelegateMock(RedeemPaymentTokensDelegateMock&&) noexcept =
      delete;
  RedeemPaymentTokensDelegateMock& operator=(
      RedeemPaymentTokensDelegateMock&&) noexcept = delete;

  ~RedeemPaymentTokensDelegateMock() override;

  MOCK_METHOD(void,
              OnDidRedeemPaymentTokens,
              (const PaymentTokenList& payment_tokens));
  MOCK_METHOD(void, OnFailedToRedeemPaymentTokens, ());

  MOCK_METHOD(void,
              OnDidScheduleNextPaymentTokenRedemption,
              (const base::Time redeem_at));

  MOCK_METHOD(void,
              OnWillRetryRedeemingPaymentTokens,
              (const base::Time retry_at));
  MOCK_METHOD(void, OnDidRetryRedeemingPaymentTokens, ());
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_UTILITY_REDEEM_PAYMENT_TOKENS_REDEEM_PAYMENT_TOKENS_DELEGATE_MOCK_H_

# Redeem Payment Tokens

Request payment against all of the Shunya Rewards user-collected [payment tokens](https://github.com/shunya/shunya-browser/wiki/Security-and-privacy-model-for-ad-confirmations).

Please add to it!

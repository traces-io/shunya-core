/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/user_data/fixed/platform_user_data.h"

#include "base/test/values_test_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsPlatformUserDataTest : public UnitTestBase {};

TEST_F(ShunyaAdsPlatformUserDataTest, BuildPlatformUserDataForRewardsUser) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(base::test::ParseJsonDict(R"({"platform":"windows"})"),
            BuildPlatformUserData());
}

TEST_F(ShunyaAdsPlatformUserDataTest,
       DoNotBuildPlatformUserDataForNonRewardsUser) {
  // Arrange
  DisableShunyaRewardsForTesting();

  // Act

  // Assert
  EXPECT_TRUE(BuildPlatformUserData().empty());
}

}  // namespace shunya_ads

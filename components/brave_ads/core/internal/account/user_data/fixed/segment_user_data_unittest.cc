/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/user_data/fixed/segment_user_data.h"

#include "base/test/values_test_util.h"
#include "shunya/components/shunya_ads/core/internal/account/transactions/transaction_info.h"
#include "shunya/components/shunya_ads/core/internal/account/transactions/transactions_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsSegmentUserDataTest : public UnitTestBase {};

TEST_F(ShunyaAdsSegmentUserDataTest, BuildSegmentUserDataForRewardsUser) {
  // Arrange
  const TransactionInfo transaction = BuildUnreconciledTransactionForTesting(
      /*value*/ 0.01, ConfirmationType::kViewed,
      /*should_use_random_uuids*/ false);

  // Act

  // Assert
  EXPECT_EQ(base::test::ParseJsonDict(R"({"segment":"untargeted"})"),
            BuildSegmentUserData(transaction));
}

TEST_F(ShunyaAdsSegmentUserDataTest,
       DoNotBuildSegmentUserDataForNonRewardsUser) {
  // Arrange
  DisableShunyaRewardsForTesting();

  const TransactionInfo transaction = BuildUnreconciledTransactionForTesting(
      /*value*/ 0.01, ConfirmationType::kViewed,
      /*should_use_random_uuids*/ false);

  // Act

  // Assert
  EXPECT_TRUE(BuildSegmentUserData(transaction).empty());
}

TEST_F(ShunyaAdsSegmentUserDataTest, DoNotBuildSegmentUserDataIfEmptySegment) {
  // Arrange
  const TransactionInfo transaction;

  // Act

  // Assert
  EXPECT_TRUE(BuildSegmentUserData(transaction).empty());
}

}  // namespace shunya_ads

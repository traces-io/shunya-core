/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/user_data/fixed/build_channel_user_data.h"

#include "base/test/values_test_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsBuildChannelUserDataTest : public UnitTestBase {};

TEST_F(ShunyaAdsBuildChannelUserDataTest,
       BuildBuildChannelUserDataForRewardsUser) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(base::test::ParseJsonDict(R"({"buildChannel":"release"})"),
            BuildBuildChannelUserData());
}

TEST_F(ShunyaAdsBuildChannelUserDataTest,
       DoNotBuildBuildChannelUserDataForNonRewardsUser) {
  // Arrange
  DisableShunyaRewardsForTesting();

  // Act

  // Assert
  EXPECT_TRUE(BuildBuildChannelUserData().empty());
}

}  // namespace shunya_ads

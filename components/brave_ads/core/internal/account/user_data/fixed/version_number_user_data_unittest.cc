/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/user_data/fixed/version_number_user_data.h"

#include <string>

#include "base/strings/string_util.h"
#include "base/test/values_test_util.h"
#include "shunya/components/shunya_ads/core/internal/browser/browser_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsVersionNumberUserDataTest : public UnitTestBase {};

TEST_F(ShunyaAdsVersionNumberUserDataTest,
       BuildVersionNumberUserDataForRewardsUser) {
  // Arrange

  // Act

  // Assert
  const std::string expected_json = base::ReplaceStringPlaceholders(
      R"({"versionNumber":"$1"})", {GetBrowserVersionNumber()}, nullptr);
  EXPECT_EQ(base::test::ParseJsonDict(expected_json),
            BuildVersionNumberUserData());
}

TEST_F(ShunyaAdsVersionNumberUserDataTest,
       BuildVersionNumberUserDataForNonRewardsUser) {
  // Arrange
  DisableShunyaRewardsForTesting();

  // Act

  // Assert
  EXPECT_TRUE(BuildVersionNumberUserData().empty());
}

}  // namespace shunya_ads

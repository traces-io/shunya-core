/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_USER_DATA_DYNAMIC_SYSTEM_TIMESTAMP_USER_DATA_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_USER_DATA_DYNAMIC_SYSTEM_TIMESTAMP_USER_DATA_H_

#include "base/values.h"

namespace shunya_ads {

base::Value::Dict BuildSystemTimestampUserData();

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_USER_DATA_DYNAMIC_SYSTEM_TIMESTAMP_USER_DATA_H_

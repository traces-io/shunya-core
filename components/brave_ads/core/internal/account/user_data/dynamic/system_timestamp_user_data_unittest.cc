/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/user_data/dynamic/system_timestamp_user_data.h"

#include "base/test/values_test_util.h"
#include "base/time/time.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_time_util.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsSystemTimestampUserDataTest : public UnitTestBase {};

TEST_F(ShunyaAdsSystemTimestampUserDataTest,
       BuildSystemTimestampUserDataForRewardsUser) {
  // Arrange
  AdvanceClockTo(
      TimeFromString("November 18 2020 12:34:56.789", /*is_local*/ false));

  // Act

  // Assert
  EXPECT_EQ(base::test::ParseJsonDict(
                R"({"systemTimestamp":"2020-11-18T12:00:00.000Z"})"),
            BuildSystemTimestampUserData());
}

TEST_F(ShunyaAdsSystemTimestampUserDataTest,
       BuildSystemTimestampUserDataForNonRewardsUser) {
  // Arrange
  DisableShunyaRewardsForTesting();

  // Act

  // Assert
  EXPECT_TRUE(BuildSystemTimestampUserData().empty());
}

}  // namespace shunya_ads

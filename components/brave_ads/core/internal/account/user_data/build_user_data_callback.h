/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_USER_DATA_BUILD_USER_DATA_CALLBACK_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_USER_DATA_BUILD_USER_DATA_CALLBACK_H_

#include "base/functional/callback_forward.h"
#include "base/values.h"

namespace shunya_ads {

using BuildUserDataCallback =
    base::OnceCallback<void(/*user_data*/ base::Value::Dict)>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_USER_DATA_BUILD_USER_DATA_CALLBACK_H_

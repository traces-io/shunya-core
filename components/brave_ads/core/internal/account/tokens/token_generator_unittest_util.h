/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_TOKEN_GENERATOR_UNITTEST_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_TOKEN_GENERATOR_UNITTEST_UTIL_H_

#include <vector>

#include "shunya/components/shunya_ads/core/internal/account/tokens/token_generator_mock.h"

namespace shunya_ads {

namespace cbr {
class Token;
}  // namespace cbr

void MockTokenGenerator(const TokenGeneratorMock& mock, size_t count);

std::vector<cbr::Token> BuildTokensForTesting(size_t count);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_TOKEN_GENERATOR_UNITTEST_UTIL_H_

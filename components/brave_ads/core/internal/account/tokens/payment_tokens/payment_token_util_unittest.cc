/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/account/tokens/payment_tokens/payment_token_util.h"

#include "shunya/components/shunya_ads/core/internal/account/tokens/payment_tokens/payment_token_info.h"
#include "shunya/components/shunya_ads/core/internal/account/tokens/payment_tokens/payment_tokens.h"
#include "shunya/components/shunya_ads/core/internal/account/tokens/payment_tokens/payment_tokens_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsPaymentTokenUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsPaymentTokenUtilTest, GetPaymentToken) {
  // Arrange
  const PaymentTokenList payment_tokens =
      SetPaymentTokensForTesting(/*count*/ 2);

  // Act

  // Assert
  EXPECT_EQ(payment_tokens.front(), MaybeGetPaymentToken());
}

TEST_F(ShunyaAdsPaymentTokenUtilTest, DoNotGetPaymentToken) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(MaybeGetPaymentToken());
}

TEST_F(ShunyaAdsPaymentTokenUtilTest, AddPaymentTokens) {
  // Arrange
  const PaymentTokenList payment_tokens =
      BuildPaymentTokensForTesting(/*count*/ 2);
  ASSERT_EQ(2U, payment_tokens.size());

  const PaymentTokenInfo& token_1 = payment_tokens.at(0);
  const PaymentTokenInfo& token_2 = payment_tokens.at(1);

  GetPaymentTokensForTesting().SetTokens({token_1});

  // Act
  AddPaymentTokens({token_2});

  // Assert
  const PaymentTokenList expected_tokens = {token_1, token_2};
  EXPECT_EQ(expected_tokens, GetAllPaymentTokens());
}

TEST_F(ShunyaAdsPaymentTokenUtilTest, RemovePaymentToken) {
  // Arrange
  const PaymentTokenList payment_tokens =
      BuildPaymentTokensForTesting(/*count*/ 3);
  ASSERT_EQ(3U, payment_tokens.size());

  const PaymentTokenInfo& token_1 = payment_tokens.at(0);
  const PaymentTokenInfo& token_2 = payment_tokens.at(1);
  const PaymentTokenInfo& token_3 = payment_tokens.at(2);

  GetPaymentTokensForTesting().SetTokens(payment_tokens);

  // Act
  RemovePaymentToken(token_2);

  // Assert
  const PaymentTokenList expected_tokens = {token_1, token_3};
  EXPECT_EQ(expected_tokens, GetAllPaymentTokens());
}

TEST_F(ShunyaAdsPaymentTokenUtilTest, PaymentTokenCount) {
  // Arrange
  SetPaymentTokensForTesting(/*count*/ 3);

  // Act

  // Assert
  EXPECT_EQ(3U, PaymentTokenCount());
}

}  // namespace shunya_ads

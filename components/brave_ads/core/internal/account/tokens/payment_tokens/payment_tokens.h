/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_PAYMENT_TOKENS_PAYMENT_TOKENS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_PAYMENT_TOKENS_PAYMENT_TOKENS_H_

#include "shunya/components/shunya_ads/core/internal/account/tokens/payment_tokens/payment_token_info.h"

namespace shunya_ads {

class PaymentTokens final {
 public:
  PaymentTokens();

  PaymentTokens(const PaymentTokens&) = delete;
  PaymentTokens& operator=(const PaymentTokens&) = delete;

  PaymentTokens(PaymentTokens&&) noexcept = delete;
  PaymentTokens& operator=(PaymentTokens&&) noexcept = delete;

  ~PaymentTokens();

  const PaymentTokenInfo& GetToken() const;
  const PaymentTokenList& GetAllTokens() const;

  void SetTokens(const PaymentTokenList& payment_tokens);

  void AddTokens(const PaymentTokenList& payment_tokens);

  bool RemoveToken(const PaymentTokenInfo& payment_token);
  void RemoveTokens(const PaymentTokenList& payment_tokens);
  void RemoveAllTokens();

  bool TokenExists(const PaymentTokenInfo& payment_token);

  size_t Count() const;

  bool IsEmpty() const;

 private:
  PaymentTokenList payment_tokens_;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_PAYMENT_TOKENS_PAYMENT_TOKENS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_PAYMENT_TOKENS_PAYMENT_TOKENS_UNITTEST_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_PAYMENT_TOKENS_PAYMENT_TOKENS_UNITTEST_UTIL_H_

#include "shunya/components/shunya_ads/core/internal/account/tokens/payment_tokens/payment_token_info.h"

namespace shunya_ads {

class AdType;
class PaymentTokens;

PaymentTokens& GetPaymentTokensForTesting();
PaymentTokenList SetPaymentTokensForTesting(int count);

PaymentTokenInfo BuildPaymentTokenForTesting(
    const ConfirmationType& confirmation_type,
    const AdType& ad_type);
PaymentTokenInfo BuildPaymentTokenForTesting();
PaymentTokenList BuildPaymentTokensForTesting(int count);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_PAYMENT_TOKENS_PAYMENT_TOKENS_UNITTEST_UTIL_H_

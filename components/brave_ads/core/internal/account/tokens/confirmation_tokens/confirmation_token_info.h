/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_CONFIRMATION_TOKENS_CONFIRMATION_TOKEN_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_CONFIRMATION_TOKENS_CONFIRMATION_TOKEN_INFO_H_

#include <string>
#include <vector>

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/public_key.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/unblinded_token.h"

namespace shunya_ads {

struct ConfirmationTokenInfo final {
  bool operator==(const ConfirmationTokenInfo&) const;
  bool operator!=(const ConfirmationTokenInfo&) const;

  cbr::UnblindedToken unblinded_token;
  cbr::PublicKey public_key;
  std::string signature;
};

using ConfirmationTokenList = std::vector<ConfirmationTokenInfo>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_CONFIRMATION_TOKENS_CONFIRMATION_TOKEN_INFO_H_

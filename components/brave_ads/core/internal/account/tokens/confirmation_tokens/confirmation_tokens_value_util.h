/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_CONFIRMATION_TOKENS_CONFIRMATION_TOKENS_VALUE_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_CONFIRMATION_TOKENS_CONFIRMATION_TOKENS_VALUE_UTIL_H_

#include "shunya/components/shunya_ads/core/internal/account/tokens/confirmation_tokens/confirmation_token_info.h"

#include "base/values.h"

namespace shunya_ads {

base::Value::List ConfirmationTokensToValue(
    const ConfirmationTokenList& confirmation_tokens);

ConfirmationTokenList ConfirmationTokensFromValue(
    const base::Value::List& list);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ACCOUNT_TOKENS_CONFIRMATION_TOKENS_CONFIRMATION_TOKENS_VALUE_UTIL_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_NOTIFICATION_AD_SERVING_FEATURE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_NOTIFICATION_AD_SERVING_FEATURE_H_

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"

namespace shunya_ads {

BASE_DECLARE_FEATURE(kNotificationAdServingFeature);

bool IsNotificationAdServingFeatureEnabled();

constexpr base::FeatureParam<int> kNotificationAdServingVersion{
    &kNotificationAdServingFeature, "version", 2};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_NOTIFICATION_AD_SERVING_FEATURE_H_

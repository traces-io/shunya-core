/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_TARGETING_USER_MODEL_BUILDER_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_TARGETING_USER_MODEL_BUILDER_H_

#include "base/functional/callback.h"

namespace shunya_ads {

struct UserModelInfo;

using BuildUserModelCallback =
    base::OnceCallback<void(const UserModelInfo& user_model)>;

void BuildUserModel(BuildUserModelCallback callback);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_TARGETING_USER_MODEL_BUILDER_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PERMISSION_RULES_MEDIA_PERMISSION_RULE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PERMISSION_RULES_MEDIA_PERMISSION_RULE_H_

#include <string>

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/permission_rule_interface.h"

namespace shunya_ads {

class MediaPermissionRule final : public PermissionRuleInterface {
 public:
  base::expected<void, std::string> ShouldAllow() const override;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PERMISSION_RULES_MEDIA_PERMISSION_RULE_H_

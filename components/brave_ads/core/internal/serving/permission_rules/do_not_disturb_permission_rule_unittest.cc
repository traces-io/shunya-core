/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/do_not_disturb_permission_rule.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_mock_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsDoNotDisturbPermissionRuleTest : public UnitTestBase {};

TEST_F(ShunyaAdsDoNotDisturbPermissionRuleTest,
       ShouldAllowWhileBrowserIsInactiveBetween6amAnd9pmOnAndroid) {
  // Arrange
  MockPlatformHelper(platform_helper_mock_, PlatformType::kAndroid);

  NotifyBrowserDidResignActive();
  NotifyBrowserDidEnterBackground();

  AdvanceClockToMidnight(/*is_local*/ true);

  // Act
  {
    // Verify 5:59 AM
    AdvanceClockBy(base::Hours(5) + base::Minutes(59));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_FALSE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 6:00 AM
    AdvanceClockBy(base::Minutes(1));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 8:59 PM
    AdvanceClockBy(base::Hours(14) + base::Minutes(59));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 9:00 PM
    AdvanceClockBy(base::Minutes(1));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_FALSE(permission_rule.ShouldAllow().has_value());
  }
}

TEST_F(ShunyaAdsDoNotDisturbPermissionRuleTest,
       ShouldAllowWhileBrowserIsActiveOnAndroid) {
  // Arrange
  MockPlatformHelper(platform_helper_mock_, PlatformType::kAndroid);

  NotifyBrowserDidBecomeActive();
  NotifyBrowserDidEnterForeground();

  AdvanceClockToMidnight(/*is_local*/ true);

  // Act
  {
    // Verify 5:59 AM
    AdvanceClockBy(base::Hours(5) + base::Minutes(59));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 6:00 AM
    AdvanceClockBy(base::Minutes(1));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 8:59 PM
    AdvanceClockBy(base::Hours(14) + base::Minutes(59));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 9:00 PM
    AdvanceClockBy(base::Minutes(1));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }
}

TEST_F(ShunyaAdsDoNotDisturbPermissionRuleTest, ShouldAlwaysAllowOnIOS) {
  // Arrange
  MockPlatformHelper(platform_helper_mock_, PlatformType::kIOS);

  NotifyBrowserDidBecomeActive();
  NotifyBrowserDidEnterForeground();

  AdvanceClockToMidnight(/*is_local*/ true);

  // Act
  {
    // Verify 00:00 AM

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 12:00 PM
    AdvanceClockBy(base::Hours(12));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }
}

TEST_F(ShunyaAdsDoNotDisturbPermissionRuleTest, ShouldAlwaysAllowOnMacOS) {
  // Arrange
  MockPlatformHelper(platform_helper_mock_, PlatformType::kMacOS);

  NotifyBrowserDidBecomeActive();
  NotifyBrowserDidEnterForeground();

  AdvanceClockToMidnight(/*is_local*/ true);

  // Act
  {
    // Verify 00:00 AM

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 12:00 PM
    AdvanceClockBy(base::Hours(12));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }
}

TEST_F(ShunyaAdsDoNotDisturbPermissionRuleTest, ShouldAlwaysAllowOnWindows) {
  // Arrange
  MockPlatformHelper(platform_helper_mock_, PlatformType::kWindows);

  NotifyBrowserDidBecomeActive();
  NotifyBrowserDidEnterForeground();

  AdvanceClockToMidnight(/*is_local*/ true);

  // Act
  {
    // Verify 00:00 AM

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 12:00 PM
    AdvanceClockBy(base::Hours(12));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }
}

TEST_F(ShunyaAdsDoNotDisturbPermissionRuleTest, ShouldAlwaysAllowOnLinux) {
  // Arrange
  MockPlatformHelper(platform_helper_mock_, PlatformType::kLinux);

  NotifyBrowserDidBecomeActive();
  NotifyBrowserDidEnterForeground();

  AdvanceClockToMidnight(/*is_local*/ true);

  // Act
  {
    // Verify 00:00 AM

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }

  {
    // Verify 12:00 PM
    AdvanceClockBy(base::Hours(12));

    // Assert
    const DoNotDisturbPermissionRule permission_rule;
    EXPECT_TRUE(permission_rule.ShouldAllow().has_value());
  }
}

}  // namespace shunya_ads

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/inline_content_ads/inline_content_ads_per_hour_permission_rule.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/units/inline_content_ad/inline_content_ad_feature.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_event_unittest_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsInlineContentAdsPerHourPermissionRuleTest : public UnitTestBase {
 protected:
  const InlineContentAdsPerHourPermissionRule permission_rule_;
};

TEST_F(ShunyaAdsInlineContentAdsPerHourPermissionRuleTest,
       ShouldAllowIfThereAreNoAdEvents) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(permission_rule_.ShouldAllow().has_value());
}

TEST_F(ShunyaAdsInlineContentAdsPerHourPermissionRuleTest,
       ShouldAllowIfDoesNotExceedCap) {
  // Arrange

  // Act
  RecordAdEventsForTesting(AdType::kInlineContentAd, ConfirmationType::kServed,
                           /*count*/ kMaximumInlineContentAdsPerHour.Get() - 1);

  // Assert
  EXPECT_TRUE(permission_rule_.ShouldAllow().has_value());
}

TEST_F(ShunyaAdsInlineContentAdsPerHourPermissionRuleTest,
       ShouldAllowIfDoesNotExceedCapAfter1Hour) {
  // Arrange
  RecordAdEventsForTesting(AdType::kInlineContentAd, ConfirmationType::kServed,
                           /*count*/ kMaximumInlineContentAdsPerHour.Get());

  // Act
  AdvanceClockBy(base::Hours(1));

  // Assert
  EXPECT_TRUE(permission_rule_.ShouldAllow().has_value());
}

TEST_F(ShunyaAdsInlineContentAdsPerHourPermissionRuleTest,
       ShouldNotAllowIfExceedsCapWithin1Hour) {
  // Arrange
  RecordAdEventsForTesting(AdType::kInlineContentAd, ConfirmationType::kServed,
                           /*count*/ kMaximumInlineContentAdsPerHour.Get());

  // Act
  AdvanceClockBy(base::Hours(1) - base::Milliseconds(1));

  // Assert
  EXPECT_FALSE(permission_rule_.ShouldAllow().has_value());
}

}  // namespace shunya_ads

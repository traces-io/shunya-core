/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PERMISSION_RULES_INLINE_CONTENT_ADS_INLINE_CONTENT_AD_PERMISSION_RULES_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PERMISSION_RULES_INLINE_CONTENT_ADS_INLINE_CONTENT_AD_PERMISSION_RULES_H_

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/permission_rules_base.h"

namespace shunya_ads {

class InlineContentAdPermissionRules final : public PermissionRulesBase {
 public:
  static bool HasPermission();
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PERMISSION_RULES_INLINE_CONTENT_ADS_INLINE_CONTENT_AD_PERMISSION_RULES_H_

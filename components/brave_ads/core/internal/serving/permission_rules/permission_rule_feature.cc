/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/permission_rule_feature.h"

namespace shunya_ads {

BASE_FEATURE(kPermissionRulesFeature,
             "PermissionRules",
             base::FEATURE_ENABLED_BY_DEFAULT);

bool IsPermissionRuleFeatureEnabled() {
  return base::FeatureList::IsEnabled(kPermissionRulesFeature);
}

}  // namespace shunya_ads

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/issuers_permission_rule.h"

#include "shunya/components/shunya_ads/core/internal/account/issuers/issuers_util.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings.h"

namespace shunya_ads {

namespace {

bool DoesRespectCap() {
  if (!UserHasJoinedShunyaRewards()) {
    return true;
  }

  return HasIssuers();
}

}  // namespace

base::expected<void, std::string> IssuersPermissionRule::ShouldAllow() const {
  if (!DoesRespectCap()) {
    return base::unexpected("Missing issuers");
  }

  return base::ok();
}

}  // namespace shunya_ads

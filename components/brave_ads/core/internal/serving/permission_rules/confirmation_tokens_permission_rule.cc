/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/confirmation_tokens_permission_rule.h"

#include "shunya/components/shunya_ads/core/internal/account/tokens/confirmation_tokens/confirmation_tokens_util.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings.h"

namespace shunya_ads {

namespace {

constexpr int kMinimumConfirmationTokenThreshold = 10;

bool DoesRespectCap() {
  if (!UserHasJoinedShunyaRewards()) {
    return true;
  }

  return ConfirmationTokenCount() >= kMinimumConfirmationTokenThreshold;
}

}  // namespace

base::expected<void, std::string>
ConfirmationTokensPermissionRule::ShouldAllow() const {
  if (!DoesRespectCap()) {
    return base::unexpected("You do not have enough confirmation tokens");
  }

  return base::ok();
}

}  // namespace shunya_ads

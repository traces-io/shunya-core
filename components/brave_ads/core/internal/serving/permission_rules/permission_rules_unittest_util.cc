/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/permission_rules_unittest_util.h"

#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/catalog_permission_rule_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/confirmation_tokens_permission_rule_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/issuers_permission_rule_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/serving/permission_rules/user_activity_permission_rule_unittest_util.h"

namespace shunya_ads {

void ForcePermissionRulesForTesting() {
  ForceCatalogPermissionRuleForTesting();
  ForceConfirmationTokensPermissionRuleForTesting();
  ForceIssuersPermissionRuleForTesting();
  ForceUserActivityPermissionRuleForTesting();
}

}  // namespace shunya_ads

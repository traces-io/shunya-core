/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/prediction/model_based/creative_ad_model_based_predictor_feature.h"

#include <vector>

#include "base/test/scoped_feature_list.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest, IsEnabled) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(
      base::FeatureList::IsEnabled(CreativeAdModelBasedPredictorFeature));
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest, IsDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_FALSE(
      base::FeatureList::IsEnabled(CreativeAdModelBasedPredictorFeature));
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     ChildIntentSegmentAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["child_intent_segment_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kChildIntentSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultChildIntentSegmentAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kChildIntentSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultChildIntentSegmentAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kChildIntentSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     ParentIntentSegmentAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["parent_intent_segment_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kParentIntentSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultParentIntentSegmentAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kParentIntentSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultParentIntentSegmentAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kParentIntentSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     ChildLatentInterestSegmentAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["child_latent_interest_segment_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kChildLatentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultChildLatentInterestSegmentAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kChildLatentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultChildLatentInterestSegmentAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kChildLatentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     ParentLatentInterestSegmentAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["parent_latent_interest_segment_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kParentLatentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultParentLatentInterestSegmentAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kParentLatentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultParentLatentInterestSegmentAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kParentLatentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     ChildInterestSegmentAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["child_interest_segment_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kChildInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultChildInterestSegmentAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kChildInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultChildInterestSegmentAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kChildInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     ParentInterestSegmentAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["parent_interest_segment_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kParentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultParentInterestSegmentAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kParentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultParentInterestSegmentAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kParentInterestSegmentAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     LastSeenAdAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["last_seen_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kLastSeenAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultLastSeenAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kLastSeenAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultLastSeenAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kLastSeenAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     LastSeenAdvertiserAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["last_seen_advertiser_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kLastSeenAdvertiserAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultLastSeenAdvertiserAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kLastSeenAdvertiserAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultLastSeenAdvertiserAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kLastSeenAdvertiserAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     PriorityAdPredictorWeight) {
  // Arrange
  base::FieldTrialParams params;
  params["priority_ad_predictor_weight"] = "0.5";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(CreativeAdModelBasedPredictorFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(0.5, kPriorityAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultPriorityAdPredictorWeight) {
  // Arrange

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kPriorityAdPredictorWeight.Get());
}

TEST(ShunyaAdsCreativeAdModelBasedPredictorFeatureTest,
     DefaultPriorityAdPredictorWeightWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(CreativeAdModelBasedPredictorFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_DOUBLE_EQ(1.0, kPriorityAdPredictorWeight.Get());
}

}  // namespace shunya_ads

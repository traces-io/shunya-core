/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PREDICTION_MODEL_BASED_SAMPLING_CREATIVE_AD_MODEL_BASED_PREDICTOR_SAMPLING_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PREDICTION_MODEL_BASED_SAMPLING_CREATIVE_AD_MODEL_BASED_PREDICTOR_SAMPLING_UTIL_H_

#include <numeric>

#include "shunya/components/shunya_ads/core/internal/serving/prediction/model_based/creative_ad_model_based_predictor_info.h"

namespace shunya_ads {

template <typename T>
double CalculateNormalizingConstantForCreativeAdPredictors(
    const CreativeAdPredictorList<T>& creative_ad_predictors) {
  return std::accumulate(
      creative_ad_predictors.cbegin(), creative_ad_predictors.cend(), 0.0,
      [](double normalizing_constant,
         const CreativeAdPredictorList<T>::value_type& creative_ad_predictor) {
        return normalizing_constant + creative_ad_predictor.score;
      });
}

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_PREDICTION_MODEL_BASED_SAMPLING_CREATIVE_AD_MODEL_BASED_PREDICTOR_SAMPLING_UTIL_H_

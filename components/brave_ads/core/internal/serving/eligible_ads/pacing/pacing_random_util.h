/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_ELIGIBLE_ADS_PACING_PACING_RANDOM_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_ELIGIBLE_ADS_PACING_PACING_RANDOM_UTIL_H_

namespace shunya_ads {

double GeneratePacingRandomNumber();

class ScopedPacingRandomNumberSetterForTesting final {
 public:
  explicit ScopedPacingRandomNumberSetterForTesting(double number);

  ~ScopedPacingRandomNumberSetterForTesting();
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SERVING_ELIGIBLE_ADS_PACING_PACING_RANDOM_UTIL_H_

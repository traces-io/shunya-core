/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/eligible_ads/pipelines/notification_ads/eligible_notification_ads_base.h"

#include "shunya/components/shunya_ads/core/internal/targeting/behavioral/anti_targeting/resource/anti_targeting_resource.h"
#include "shunya/components/shunya_ads/core/internal/targeting/geographical/subdivision/subdivision_targeting.h"

namespace shunya_ads {

EligibleNotificationAdsBase::EligibleNotificationAdsBase(
    const SubdivisionTargeting& subdivision_targeting,
    const AntiTargetingResource& anti_targeting_resource)
    : subdivision_targeting_(subdivision_targeting),
      anti_targeting_resource_(anti_targeting_resource) {}

EligibleNotificationAdsBase::~EligibleNotificationAdsBase() = default;

}  // namespace shunya_ads

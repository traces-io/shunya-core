/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/eligible_ads/exclusion_rules/anti_targeting_exclusion_rule.h"

#include <memory>

#include "shunya/components/shunya_ads/core/internal/common/resources/country_components_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/creatives/creative_ad_info.h"
#include "shunya/components/shunya_ads/core/internal/targeting/behavioral/anti_targeting/resource/anti_targeting_resource.h"
#include "shunya/components/shunya_ads/core/internal/units/ad_unittest_constants.h"
#include "url/gurl.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

namespace {
constexpr char kAntiTargetedSite[] = "https://www.shunya.com";
}  // namespace

class ShunyaAdsAntiTargetingExclusionRuleTest : public UnitTestBase {
 protected:
  void SetUp() override {
    UnitTestBase::SetUp();

    resource_ = std::make_unique<AntiTargetingResource>();
  }

  bool LoadResource() {
    NotifyDidUpdateResourceComponent(kCountryComponentManifestVersion,
                                     kCountryComponentId);
    task_environment_.RunUntilIdle();
    return resource_->IsInitialized();
  }

  std::unique_ptr<AntiTargetingResource> resource_;
};

TEST_F(ShunyaAdsAntiTargetingExclusionRuleTest,
       ShouldIncludeIfResourceIsNotInitialized) {
  // Arrange
  const AntiTargetingExclusionRule exclusion_rule(
      *resource_, /*browsing_history*/ {GURL(kAntiTargetedSite)});

  CreativeAdInfo creative_ad;
  creative_ad.creative_set_id = kCreativeSetId;

  // Act

  // Assert
  EXPECT_TRUE(exclusion_rule.ShouldInclude(creative_ad).has_value());
}

TEST_F(ShunyaAdsAntiTargetingExclusionRuleTest,
       ShouldIncludeIfNotVisitedAntiTargetedSiteForCreativeSet) {
  // Arrange
  ASSERT_TRUE(LoadResource());

  const AntiTargetingExclusionRule exclusion_rule(
      *resource_, /*browsing_history*/ {GURL(kAntiTargetedSite)});

  CreativeAdInfo creative_ad;
  creative_ad.creative_set_id = kMissingCreativeSetId;

  // Act

  // Assert
  EXPECT_TRUE(exclusion_rule.ShouldInclude(creative_ad).has_value());
}

TEST_F(ShunyaAdsAntiTargetingExclusionRuleTest,
       ShouldIncludeIfNotVisitedAntiTargetedSite) {
  // Arrange
  ASSERT_TRUE(LoadResource());

  const AntiTargetingExclusionRule exclusion_rule(
      *resource_, /*browsing_history*/ {GURL("https://www.foo.com")});

  CreativeAdInfo creative_ad;
  creative_ad.creative_set_id = kCreativeSetId;

  // Act

  // Assert
  EXPECT_TRUE(exclusion_rule.ShouldInclude(creative_ad).has_value());
}

TEST_F(ShunyaAdsAntiTargetingExclusionRuleTest,
       ShouldExcludeIfVisitedAntiTargetedSite) {
  // Arrange
  ASSERT_TRUE(LoadResource());

  const AntiTargetingExclusionRule exclusion_rule(
      *resource_, /*browsing_history*/ {GURL(kAntiTargetedSite)});

  CreativeAdInfo creative_ad;
  creative_ad.creative_set_id = kCreativeSetId;

  // Act

  // Assert
  EXPECT_FALSE(exclusion_rule.ShouldInclude(creative_ad).has_value());
}

}  // namespace shunya_ads

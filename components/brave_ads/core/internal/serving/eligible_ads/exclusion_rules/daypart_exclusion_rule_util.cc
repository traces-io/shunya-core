/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/serving/eligible_ads/exclusion_rules/daypart_exclusion_rule_util.h"

#include <string>

#include "shunya/components/shunya_ads/core/internal/creatives/creative_daypart_info.h"

namespace shunya_ads {

bool MatchDayOfWeek(const CreativeDaypartInfo& daypart,
                    const char day_of_week) {
  return daypart.days_of_week.find(day_of_week) != std::string::npos;
}

bool MatchTimeSlot(const CreativeDaypartInfo& daypart, const int minutes) {
  return minutes >= daypart.start_minute && minutes <= daypart.end_minute;
}

}  // namespace shunya_ads

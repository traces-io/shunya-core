/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CONVERSIONS_TYPES_VERIFIABLE_CONVERSION_VERIFIABLE_CONVERSION_ID_PATTERN_VERIFIABLE_CONVERSION_ID_PATTERN_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CONVERSIONS_TYPES_VERIFIABLE_CONVERSION_VERIFIABLE_CONVERSION_ID_PATTERN_VERIFIABLE_CONVERSION_ID_PATTERN_UTIL_H_

#include <string>
#include <vector>

#include "shunya/components/shunya_ads/core/internal/conversions/resource/conversion_resource_id_pattern_info.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

class GURL;

namespace shunya_ads {

absl::optional<std::string> MaybeParseVerifiableConversionId(
    const std::vector<GURL>& redirect_chain,
    const std::string& html,
    const ConversionResourceIdPatternMap& resource_id_patterns);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CONVERSIONS_TYPES_VERIFIABLE_CONVERSION_VERIFIABLE_CONVERSION_ID_PATTERN_VERIFIABLE_CONVERSION_ID_PATTERN_UTIL_H_

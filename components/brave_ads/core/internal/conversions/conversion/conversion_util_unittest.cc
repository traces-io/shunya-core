/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/conversions/conversion/conversion_util.h"

#include "shunya/components/shunya_ads/core/internal/conversions/conversion/conversion_info.h"
#include "shunya/components/shunya_ads/core/internal/conversions/types/verifiable_conversion/verifiable_conversion_unittest_constants.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsConversionInfoUtilTest, ConversionTypeToString) {
  // Arrange
  const ConversionInfo conversion;

  // Act

  // Assert
  EXPECT_EQ("conversion", ConversionTypeToString(conversion));
}

TEST(ShunyaAdsConversionInfoUtilTest, VerifiableConversionTypeToString) {
  // Arrange
  ConversionInfo conversion;
  conversion.verifiable = {kVerifiableConversionId,
                           kVerifiableConversionAdvertiserPublicKey};

  // Act

  // Assert
  EXPECT_EQ("verifiable conversion", ConversionTypeToString(conversion));
}

}  // namespace shunya_ads

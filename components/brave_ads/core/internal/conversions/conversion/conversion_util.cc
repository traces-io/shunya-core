/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/conversions/conversion/conversion_util.h"

#include "shunya/components/shunya_ads/core/internal/conversions/conversion/conversion_info.h"

namespace shunya_ads {

namespace {

constexpr char kVerifiableConversion[] = "verifiable conversion";
constexpr char kDefaultConversion[] = "conversion";

}  // namespace

std::string ConversionTypeToString(const ConversionInfo& conversion) {
  return conversion.verifiable ? kVerifiableConversion : kDefaultConversion;
}

}  // namespace shunya_ads

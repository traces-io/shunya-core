/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/conversions/actions/conversion_action_types_util.h"

#include "shunya/components/shunya_ads/core/public/account/confirmations/confirmation_type.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsConversionActionTypesUtilTest, ToViewThroughConversionActionType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(ConversionActionType::kViewThrough,
            ToConversionActionType(ConfirmationType::kViewed));
}

TEST(ShunyaAdsConversionActionTypesUtilTest,
     ToClickThroughConversionActionType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(ConversionActionType::kClickThrough,
            ToConversionActionType(ConfirmationType::kClicked));
}

TEST(ShunyaAdsConversionActionTypesUtilTest,
     StringToViewThroughConversionActionType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(ConversionActionType::kViewThrough,
            StringToConversionActionType("view"));
}

TEST(ShunyaAdsConversionActionTypesUtilTest,
     StringToClickThroughConversionActionType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(ConversionActionType::kClickThrough,
            StringToConversionActionType("click"));
}

TEST(ShunyaAdsConversionActionTypesUtilTest,
     ViewThroughConversionActionTypeToString) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ("view",
            ConversionActionTypeToString(ConversionActionType::kViewThrough));
}

TEST(ShunyaAdsConversionActionTypesUtilTest,
     ClickThroughConversionActionTypeToString) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ("click",
            ConversionActionTypeToString(ConversionActionType::kClickThrough));
}

}  // namespace shunya_ads

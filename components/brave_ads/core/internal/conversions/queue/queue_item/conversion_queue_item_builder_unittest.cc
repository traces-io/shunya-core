/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/conversions/queue/queue_item/conversion_queue_item_builder.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_time_util.h"
#include "shunya/components/shunya_ads/core/internal/conversions/conversion/conversion_builder.h"
#include "shunya/components/shunya_ads/core/internal/conversions/types/verifiable_conversion/verifiable_conversion_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/units/ad_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_event_builder.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsConversionQueueItemBuilderTest : public UnitTestBase {};

TEST_F(ShunyaAdsConversionQueueItemBuilderTest, BuildConversionQueueItem) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  const ConversionInfo conversion =
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt);

  // Act
  const ConversionQueueItemInfo conversion_queue_item =
      BuildConversionQueueItem(conversion, /*process_at*/ Now());

  // Assert
  ConversionQueueItemInfo expected_conversion_queue_item;
  expected_conversion_queue_item.conversion = conversion;
  expected_conversion_queue_item.process_at = Now();
  expected_conversion_queue_item.was_processed = false;

  EXPECT_EQ(expected_conversion_queue_item, conversion_queue_item);
}

TEST_F(ShunyaAdsConversionQueueItemBuilderTest,
       BuildVerifiableConversionQueueItem) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  const ConversionInfo conversion = BuildConversion(
      BuildAdEvent(ad, ConfirmationType::kViewed,
                   /*created_at*/ Now()),
      VerifiableConversionInfo{kVerifiableConversionId,
                               kVerifiableConversionAdvertiserPublicKey});

  // Act
  const ConversionQueueItemInfo conversion_queue_item =
      BuildConversionQueueItem(conversion, /*process_at*/ Now());

  // Assert
  ConversionQueueItemInfo expected_conversion_queue_item;
  expected_conversion_queue_item.conversion = conversion;
  expected_conversion_queue_item.process_at = Now();
  expected_conversion_queue_item.was_processed = false;

  EXPECT_EQ(expected_conversion_queue_item, conversion_queue_item);
}

}  // namespace shunya_ads

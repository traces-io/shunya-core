/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/conversions/conversions.h"

#include <memory>

#include "base/time/time.h"
#include "shunya/components/shunya_ads/core/internal/common/resources/country_components_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_container_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_time_util.h"
#include "shunya/components/shunya_ads/core/internal/conversions/conversion/conversion_builder.h"
#include "shunya/components/shunya_ads/core/internal/conversions/conversions_observer.h"
#include "shunya/components/shunya_ads/core/internal/conversions/types/verifiable_conversion/verifiable_conversion_info.h"
#include "shunya/components/shunya_ads/core/internal/conversions/types/verifiable_conversion/verifiable_conversion_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/creatives/conversions/creative_set_conversion_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/units/ad_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_event_builder.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_event_unittest_util.h"
#include "shunya/components/shunya_ads/core/public/account/confirmations/confirmation_type.h"
#include "shunya/components/shunya_ads/core/public/units/ad_info.h"
#include "third_party/abseil-cpp/absl/types/optional.h"
#include "url/gurl.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

namespace {

constexpr char kMatchingUrlPattern[] = "https://foo.com/*";
constexpr char kAnotherMatchingUrlPattern[] = "https://qux.com/*/corge";
constexpr char kNonMatchingUrlPattern[] = "https://www.corge.com/grault";

constexpr char kHtml[] = "<html>Hello World!</html>";

std::vector<GURL> BuildRedirectChain() {
  return {GURL("https://foo.com/bar"), GURL("https://www.baz.com"),
          GURL("https://qux.com/quux/corge")};
}

}  // namespace

class ShunyaAdsConversionsTest : public ConversionsObserver,
                                public UnitTestBase {
 protected:
  void SetUp() override {
    UnitTestBase::SetUp();

    conversions_ = std::make_unique<Conversions>();
    conversions_->AddObserver(this);
  }

  void TearDown() override {
    conversions_->RemoveObserver(this);

    UnitTestBase::TearDown();
  }

  void OnDidConvertAd(const ConversionInfo& conversion) override {
    actioned_conversions_.push_back(conversion);
  }

  void LoadConversionResource() {
    NotifyDidUpdateResourceComponent(kCountryComponentManifestVersion,
                                     kCountryComponentId);
    task_environment_.RunUntilIdle();
  }

  void FireAdEventsAdvancingTheClockAfterEach(
      const AdInfo& ad,
      const std::vector<ConfirmationType>& confirmation_types) {
    for (const auto& confirmation_type : confirmation_types) {
      const AdEventInfo ad_event =
          BuildAdEvent(ad, confirmation_type, /*created_at*/ Now());
      FireAdEventForTesting(ad_event);

      AdvanceClockBy(base::Milliseconds(1));
    }
  }

  void DrainConversionQueue() {
    // Conversions are added to the |ConversionQueue|, so if the conversion
    // queue has pending conversions, we must force the processing of those
    // conversions to notify this observer.
    while (HasPendingTasks()) {
      FastForwardClockToNextPendingTask();
    }
  }

  void MaybeConvert(const std::vector<GURL>& redirect_chain,
                    const std::string& html) {
    conversions_->MaybeConvert(redirect_chain, html);

    DrainConversionQueue();
  }

  std::unique_ptr<Conversions> conversions_;

  ConversionList actioned_conversions_;
};

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertViewedInlineContentAdIfShunyaNewsAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kInlineContentAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertViewedInlineContentAdIfShunyaNewsAdsAreEnabled) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kInlineContentAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertClickedInlineContentAdIfShunyaNewsAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kInlineContentAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertClickedInlineContentAdIfShunyaNewsAdsAreEnabled) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kInlineContentAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kClicked,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertViewedNewTabPageAdIfNewTabPageAdsAreDisabled) {
  // Arrange
  DisableNewTabPageAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertViewedNewTabPageAdIfNewTabPageAdsAreEnabled) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertClickedNewTabPageAdIfNewTabPageAdsAreDisabled) {
  // Arrange
  DisableNewTabPageAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertClickedNewTabPageAdIfNewTabPageAdsAreEnabled) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kClicked,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertViewedNotificationAdIfOptedOutOfNotificationAds) {
  // Arrange
  DisableNotificationAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertViewedNotificationAdIfOptedInToNotificationAds) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertClickedNotificationAdIfOptedOutOfNotificationAds) {
  // Arrange
  DisableNotificationAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertClickedNotificationAdIfOptedInToNotificationAds) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kClicked,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertViewedPromotedContentAdIfShunyaNewsAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kPromotedContentAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertViewedPromotedContentAdIfShunyaNewsAdsAreEnabled) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kPromotedContentAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertClickedPromotedContentAdIfShunyaNewsAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kPromotedContentAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertClickedPromotedContentAdIfShunyaNewsAdsAreEnabled) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kPromotedContentAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kClicked,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, ConvertViewedSearchResultAdIfAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();
  DisableNotificationAdsForTesting();
  DisableNewTabPageAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kSearchResultAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, ConvertViewedSearchResultAdIfAdsAreEnabled) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kSearchResultAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, ConvertClickedSearchResultAdIfAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();
  DisableNotificationAdsForTesting();
  DisableNewTabPageAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kSearchResultAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kClicked,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, ConvertClickedSearchResultAdIfAdsAreEnabled) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kSearchResultAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kClicked,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, MultipleAdConversions) {
  // Arrange
  const AdInfo ad_1 = BuildAdForTesting(AdType::kInlineContentAd,
                                        /*should_use_random_uuids*/ true);
  BuildAndSaveCreativeSetConversionForTesting(
      ad_1.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));
  FireAdEventsAdvancingTheClockAfterEach(
      ad_1, {ConfirmationType::kServed, ConfirmationType::kViewed});

  const AdInfo ad_2 = BuildAdForTesting(AdType::kSearchResultAd,
                                        /*should_use_random_uuids*/ true);
  BuildAndSaveCreativeSetConversionForTesting(
      ad_2.creative_set_id, kAnotherMatchingUrlPattern,
      /*observation_window*/ base::Days(3));
  FireAdEventsAdvancingTheClockAfterEach(
      ad_2, {ConfirmationType::kServed, ConfirmationType::kViewed,
             ConfirmationType::kClicked});

  const AdInfo ad_3 = BuildAdForTesting(AdType::kNewTabPageAd,
                                        /*should_use_random_uuids*/ true);
  FireAdEventsAdvancingTheClockAfterEach(
      ad_3, {ConfirmationType::kServed, ConfirmationType::kViewed,
             ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad_1, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad_2, ConfirmationType::kClicked,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));

  EXPECT_TRUE(
      ContainersEq(expected_actioned_conversions, actioned_conversions_));
}

TEST_F(ShunyaAdsConversionsTest, ConvertViewedAdAfterTheSameAdWasDismissed) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kDismissed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, DoNotConvertAdsIfTheRedirectChainIsEmpty) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kDismissed, ConfirmationType::kServed,
           ConfirmationType::kTransferred, ConfirmationType::kFlagged,
           ConfirmationType::kSaved, ConfirmationType::kUpvoted,
           ConfirmationType::kDownvoted, ConfirmationType::kConversion});

  // Act
  MaybeConvert(/*redirect_chain*/ {}, kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertAdsIfTheRedirectChainContainsAnUnsupportedUrl) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kDismissed, ConfirmationType::kServed,
           ConfirmationType::kTransferred, ConfirmationType::kFlagged,
           ConfirmationType::kSaved, ConfirmationType::kUpvoted,
           ConfirmationType::kDownvoted, ConfirmationType::kConversion});

  // Act
  MaybeConvert(/*redirect_chain*/ {GURL("foo.bar")}, kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest, DoNotConvertNonViewedOrClickedAds) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kDismissed, ConfirmationType::kServed,
           ConfirmationType::kTransferred, ConfirmationType::kFlagged,
           ConfirmationType::kSaved, ConfirmationType::kUpvoted,
           ConfirmationType::kDownvoted, ConfirmationType::kConversion});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertAdIfThereIsNoMatchingCreativeSetConversion) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertAdIfAnotherAdHasConvertedWithinTheSameCreativeSet) {
  // Arrange
  const AdInfo ad_1 = BuildAdForTesting(AdType::kNotificationAd,
                                        /*should_use_random_uuids*/ true);
  BuildAndSaveCreativeSetConversionForTesting(
      ad_1.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));
  FireAdEventsAdvancingTheClockAfterEach(
      ad_1, {ConfirmationType::kServed, ConfirmationType::kViewed,
             ConfirmationType::kDismissed});

  MaybeConvert(BuildRedirectChain(), kHtml);

  AdInfo ad_2 = ad_1;
  ad_2.creative_instance_id = "1e945c25-98a2-443c-a7f5-e695110d2b84";
  FireAdEventsAdvancingTheClockAfterEach(
      ad_2, {ConfirmationType::kServed, ConfirmationType::kViewed,
             ConfirmationType::kClicked});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad_1, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, DoNotConvertAdIfUrlPatternDoesNotMatch) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kNonMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed,
           ConfirmationType::kDismissed});

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertAdIfCreativeSetConversionIsOnTheCuspOfExpiring) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());
  FireAdEventForTesting(ad_event);

  AdvanceClockBy(base::Days(3) - base::Milliseconds(1));

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       DoNotConvertAdIfTheCreativeSetConversionHasExpired) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3));

  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());
  FireAdEventForTesting(ad_event);

  AdvanceClockBy(base::Days(3));

  // Act
  MaybeConvert(BuildRedirectChain(), kHtml);

  // Assert
  EXPECT_TRUE(actioned_conversions_.empty());
}

TEST_F(ShunyaAdsConversionsTest,
       FallbackToDefaultConversionIfVerifiableAdvertiserPublicKeyIsEmpty) {
  // Arrange
  LoadConversionResource();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveVerifiableCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3),
      kEmptyVerifiableConversionAdvertiserPublicKey);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(
      /*redirect_chain*/ {GURL("https://foo.com/bar?qux_id=xyzzy")}, kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(
    ShunyaAdsConversionsTest,
    FallbackToDefaultConversionIfResourceIdPatternDoesNotMatchRedirectChain) {
  // Arrange
  LoadConversionResource();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveVerifiableCreativeSetConversionForTesting(
      ad.creative_set_id,
      /*url_pattern*/ "https://www.baz.com/*",
      /*observation_window*/ base::Days(3),
      kVerifiableConversionAdvertiserPublicKey);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(
      /*redirect_chain*/ {GURL("https://grault.com/garply"),
                          GURL("https://www.baz.com/bar"),
                          GURL("https://qux.com/quux/plugh")},
      kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       FallbackToDefaultConversionIfVerifiableUrlConversionIdDoesNotExist) {
  // Arrange
  LoadConversionResource();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveVerifiableCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3),
      kVerifiableConversionAdvertiserPublicKey);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(
      /*redirect_chain*/ {GURL("https://foo.com/bar?qux=quux")}, kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, ConvertAdIfVerifiableUrlConversionIdExists) {
  // Arrange
  LoadConversionResource();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveVerifiableCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3),
      kVerifiableConversionAdvertiserPublicKey);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(
      /*redirect_chain*/ {GURL("https://foo.com/bar?qux_id=xyzzy")}, kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(BuildConversion(
      BuildAdEvent(ad, ConfirmationType::kViewed,
                   /*created_at*/ Now()),
      VerifiableConversionInfo{/*id*/ "xyzzy",
                               kVerifiableConversionAdvertiserPublicKey}));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       FallbackToDefaultConversionIfVerifiableHtmlConversionIdDoesNotExist) {
  // Arrange
  LoadConversionResource();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveVerifiableCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3),
      kVerifiableConversionAdvertiserPublicKey);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(
      /*redirect_chain*/ {GURL("https://foo.com/bar")}, kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest, ConvertAdIfVerifiableHtmlConversionIdExists) {
  // Arrange
  LoadConversionResource();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveVerifiableCreativeSetConversionForTesting(
      ad.creative_set_id, kMatchingUrlPattern,
      /*observation_window*/ base::Days(3),
      kVerifiableConversionAdvertiserPublicKey);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(BuildRedirectChain(),
               /*html*/ R"(<html><div id="xyzzy-id">waldo</div></html>)");

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(BuildConversion(
      BuildAdEvent(ad, ConfirmationType::kViewed,
                   /*created_at*/ Now()),
      VerifiableConversionInfo{/*id*/ "waldo",
                               kVerifiableConversionAdvertiserPublicKey}));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(
    ShunyaAdsConversionsTest,
    FallbackToDefaultConversionIfVerifiableHtmlMetaTagConversionIdDoesNotExist) {
  // Arrange
  LoadConversionResource();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveVerifiableCreativeSetConversionForTesting(
      ad.creative_set_id, kAnotherMatchingUrlPattern,
      /*observation_window*/ base::Days(3),
      kVerifiableConversionAdvertiserPublicKey);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(
      /*redirect_chain*/ {GURL("https://qux.com/quux/corge")}, kHtml);

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(
      BuildConversion(BuildAdEvent(ad, ConfirmationType::kViewed,
                                   /*created_at*/ Now()),
                      /*verifiable_conversion*/ absl::nullopt));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

TEST_F(ShunyaAdsConversionsTest,
       ConvertAdIfVerifiableHtmlMetaTagConversionIdExists) {
  // Arrange
  LoadConversionResource();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  BuildAndSaveVerifiableCreativeSetConversionForTesting(
      ad.creative_set_id, kAnotherMatchingUrlPattern,
      /*observation_window*/ base::Days(3),
      kVerifiableConversionAdvertiserPublicKey);

  FireAdEventsAdvancingTheClockAfterEach(
      ad, {ConfirmationType::kServed, ConfirmationType::kViewed});

  // Act
  MaybeConvert(
      /*redirect_chain*/ {GURL("https://qux.com/quux/corge")},
      /*html*/ R"(<html><meta name="ad-conversion-id" content="fred"></html>)");

  // Assert
  ConversionList expected_actioned_conversions;
  expected_actioned_conversions.push_back(BuildConversion(
      BuildAdEvent(ad, ConfirmationType::kViewed,
                   /*created_at*/ Now()),
      VerifiableConversionInfo{/*id*/ "fred",
                               kVerifiableConversionAdvertiserPublicKey}));
  EXPECT_EQ(expected_actioned_conversions, actioned_conversions_);
}

}  // namespace shunya_ads

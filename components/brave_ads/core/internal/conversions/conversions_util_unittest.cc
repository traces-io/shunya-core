/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/conversions/conversions_util.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_time_util.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/units/ad_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_event_builder.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_event_info.h"
#include "shunya/components/shunya_ads/core/public/units/ad_info.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsConversionsUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertInlineContentAdViewedEvent) {
  // Arrange
  const AdInfo ad =
      BuildAdForTesting(AdType::kInlineContentAd, /*should_use_random_uuids*/
                        true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertInlineContentAdClickedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kInlineContentAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kClicked, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CannotConvertInlineContentAdEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kInlineContentAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kServed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest,
       CannotConvertInlineContentAdEventIfShunyaNewsAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kInlineContentAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertPromotedContentAdViewedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kPromotedContentAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertPromotedContentAdClickedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kPromotedContentAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kClicked, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CannotConvertPromotedContentAdEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kPromotedContentAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kServed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest,
       CannotConvertPromotedContentAdEventIfShunyaNewsAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kPromotedContentAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertNotificationAdViewedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertNotificationAdClickedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kClicked, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CannotConvertNotificationAdEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kServed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest,
       CannotConvertNotificationAdEventIfOptedOutOfNotificationAds) {
  // Arrange
  DisableNotificationAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertNewTabPageAdViewedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertNewTabPageAdClickedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kClicked, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CannotConvertNewTabPageAdEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kServed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest,
       CannotConvertNewTabPageAdEventIfNewTabPageAdsAreDisabled) {
  // Arrange
  DisableNewTabPageAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertSearchResultAdViewedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kSearchResultAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CanConvertSearchResultAdClickedEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kSearchResultAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kClicked, /*created_at*/ Now());

  // Assert
  EXPECT_TRUE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, CannotConvertSearchResultAdEvent) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kSearchResultAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kServed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest,
       CanConvertSearchResultAdEventIfAdsAreDisabled) {
  // Arrange
  DisableShunyaNewsAdsForTesting();
  DisableNotificationAdsForTesting();
  DisableNewTabPageAdsForTesting();

  const AdInfo ad = BuildAdForTesting(AdType::kNewTabPageAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  // Assert
  EXPECT_FALSE(CanConvertAdEvent(ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, HasObservationWindowForAdEventExpired) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  AdvanceClockBy(base::Days(1) + base::Milliseconds(1));

  // Assert
  EXPECT_TRUE(HasObservationWindowForAdEventExpired(
      /*observation_window*/ base::Days(1), ad_event));
}

TEST_F(ShunyaAdsConversionsUtilTest, HasObservationWindowForAdEventNotExpired) {
  // Arrange
  const AdInfo ad = BuildAdForTesting(AdType::kNotificationAd,
                                      /*should_use_random_uuids*/ true);

  // Act
  const AdEventInfo ad_event =
      BuildAdEvent(ad, ConfirmationType::kViewed, /*created_at*/ Now());

  AdvanceClockBy(base::Days(1));

  // Assert
  EXPECT_FALSE(HasObservationWindowForAdEventExpired(
      /*observation_window*/ base::Days(1), ad_event));
}

}  // namespace shunya_ads

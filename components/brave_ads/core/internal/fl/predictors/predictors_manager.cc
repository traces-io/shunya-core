/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/fl/predictors/predictors_manager.h"

#include <utility>

#include "base/check.h"
#include "base/containers/fixed_flat_map.h"
#include "base/time/time.h"
#include "shunya/components/shunya_ads/core/internal/client/ads_client_helper.h"
#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/average_clickthrough_rate_predictor_variable.h"
#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/last_notification_ad_was_clicked_predictor_variable.h"
#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/number_of_user_activity_events_predictor_variable.h"
#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/time_since_last_user_activity_event_predictor_variable.h"
#include "shunya/components/shunya_ads/core/internal/global_state/global_state.h"
#include "shunya/components/shunya_federated/public/interfaces/shunya_federated.mojom.h"

namespace shunya_ads {

namespace {

constexpr auto kUserActivityEventToPredictorVariableTypeMapping =
    base::MakeFixedFlatMap<UserActivityEventType,
                           std::pair<shunya_federated::mojom::CovariateType,
                                     shunya_federated::mojom::CovariateType>>(
        {{UserActivityEventType::kBrowserDidBecomeActive,
          {shunya_federated::mojom::CovariateType::
               kNumberOfBrowserDidBecomeActiveEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastBrowserDidBecomeActiveEvent}},
         {UserActivityEventType::kBrowserDidEnterForeground,
          {shunya_federated::mojom::CovariateType::
               kNumberOfBrowserDidEnterForegroundEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastBrowserDidEnterForegroundEvent}},
         {UserActivityEventType::kBrowserDidResignActive,
          {shunya_federated::mojom::CovariateType::
               kNumberOfBrowserWindowIsInactiveEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastBrowserWindowIsInactiveEvent}},
         {UserActivityEventType::kClickedBackOrForwardNavigationButtons,
          {shunya_federated::mojom::CovariateType::
               kNumberOfClickedBackOrForwardNavigationButtonsEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastClickedBackOrForwardNavigationButtonsEvent}},
         {UserActivityEventType::kClickedLink,
          {shunya_federated::mojom::CovariateType::kNumberOfClickedLinkEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastClickedLinkEvent}},
         {UserActivityEventType::kClickedReloadButton,
          {shunya_federated::mojom::CovariateType::
               kNumberOfClickedReloadButtonEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastClickedReloadButtonEvent}},
         {UserActivityEventType::kClosedTab,
          {shunya_federated::mojom::CovariateType::kNumberOfClosedTabEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastClosedTabEvent}},
         {UserActivityEventType::kTabChangedFocus,
          {shunya_federated::mojom::CovariateType::
               kNumberOfFocusedOnExistingTabEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastFocusedOnExistingTabEvent}},
         {UserActivityEventType::kNewNavigation,
          {shunya_federated::mojom::CovariateType::kNumberOfNewNavigationEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastNewNavigationEvent}},
         {UserActivityEventType::kOpenedNewTab,
          {shunya_federated::mojom::CovariateType::kNumberOfOpenedNewTabEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastOpenedNewTabEvent}},
         {UserActivityEventType::kTabStartedPlayingMedia,
          {shunya_federated::mojom::CovariateType::kNumberOfPlayedMediaEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastPlayedMediaEvent}},
         {UserActivityEventType::kSubmittedForm,
          {shunya_federated::mojom::CovariateType::kNumberOfSubmittedFormEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastSubmittedFormEvent}},
         {UserActivityEventType::kTypedAndSelectedNonUrl,
          {shunya_federated::mojom::CovariateType::
               kNumberOfTypedAndSelectedNonUrlEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastTypedAndSelectedNonUrlEvent}},
         {UserActivityEventType::kTypedKeywordOtherThanDefaultSearchProvider,
          {shunya_federated::mojom::CovariateType::
               kNumberOfTypedKeywordOtherThanDefaultSearchProviderEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastTypedKeywordOtherThanDefaultSearchProviderEvent}},
         {UserActivityEventType::kTypedUrl,
          {shunya_federated::mojom::CovariateType::kNumberOfTypedUrlEvents,
           shunya_federated::mojom::CovariateType::
               kTimeSinceLastTypedUrlEvent}}});

constexpr base::TimeDelta kAverageClickthroughRateTimeWindows[] = {
    base::Days(1), base::Days(7), base::Days(28)};

}  // namespace

PredictorsManager::PredictorsManager() {
  SetPredictorVariable(
      std::make_unique<LastNotificationAdWasClickedPredictorVariable>());

  for (const auto& [user_activity_event_type, predictor_variable_type] :
       kUserActivityEventToPredictorVariableTypeMapping) {
    const auto& [number_of_user_activity_events,
                 time_since_last_user_activity_event] = predictor_variable_type;

    SetPredictorVariable(
        std::make_unique<NumberOfUserActivityEventsPredictorVariable>(
            user_activity_event_type, number_of_user_activity_events));

    SetPredictorVariable(
        std::make_unique<TimeSinceLastUserActivityEventPredictorVariable>(
            user_activity_event_type, time_since_last_user_activity_event));
  }

  for (const auto& time_window : kAverageClickthroughRateTimeWindows) {
    SetPredictorVariable(
        std::make_unique<AverageClickthroughRatePredictorVariable>(
            time_window));
  }
}

PredictorsManager::~PredictorsManager() = default;

// static
PredictorsManager& PredictorsManager::GetInstance() {
  return GlobalState::GetInstance()->GetPredictorsManager();
}

void PredictorsManager::SetPredictorVariable(
    std::unique_ptr<PredictorVariableInterface> predictor_variable) {
  CHECK(predictor_variable);

  const shunya_federated::mojom::CovariateType type =
      predictor_variable->GetType();
  predictor_variables_[type] = std::move(predictor_variable);
}

std::vector<shunya_federated::mojom::CovariateInfoPtr>
PredictorsManager::GetTrainingSample() const {
  std::vector<shunya_federated::mojom::CovariateInfoPtr> training_sample;

  for (const auto& [_, predictor_variable] : predictor_variables_) {
    CHECK(predictor_variable);

    shunya_federated::mojom::CovariateInfoPtr predictor =
        shunya_federated::mojom::CovariateInfo::New();
    predictor->data_type = predictor_variable->GetDataType();
    predictor->type = predictor_variable->GetType();
    predictor->value = predictor_variable->GetValue();

    training_sample.push_back(std::move(predictor));
  }

  return training_sample;
}

void PredictorsManager::AddTrainingSample() const {
  std::vector<shunya_federated::mojom::CovariateInfoPtr> training_sample =
      GetTrainingSample();
  AdsClientHelper::GetInstance()->AddTrainingSample(std::move(training_sample));
}

}  // namespace shunya_ads

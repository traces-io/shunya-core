/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/number_of_user_activity_events_predictor_variable.h"

#include <memory>

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_manager.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsNumberOfUserActivityEventsPredictorVariableTest
    : public UnitTestBase {};

TEST_F(ShunyaAdsNumberOfUserActivityEventsPredictorVariableTest, GetDataType) {
  // Arrange
  std::unique_ptr<PredictorVariableInterface> predictor_variable =
      std::make_unique<NumberOfUserActivityEventsPredictorVariable>(
          UserActivityEventType::kOpenedNewTab,
          shunya_federated::mojom::CovariateType::kNumberOfOpenedNewTabEvents);

  // Act

  // Assert
  EXPECT_EQ(shunya_federated::mojom::DataType::kInt,
            predictor_variable->GetDataType());
}

TEST_F(ShunyaAdsNumberOfUserActivityEventsPredictorVariableTest,
       GetValueWithoutUserActivity) {
  // Arrange
  std::unique_ptr<PredictorVariableInterface> predictor_variable =
      std::make_unique<NumberOfUserActivityEventsPredictorVariable>(
          UserActivityEventType::kOpenedNewTab,
          shunya_federated::mojom::CovariateType::kNumberOfOpenedNewTabEvents);

  // Act

  // Assert
  EXPECT_EQ("0", predictor_variable->GetValue());
}

TEST_F(ShunyaAdsNumberOfUserActivityEventsPredictorVariableTest, GetValue) {
  // Arrange
  std::unique_ptr<PredictorVariableInterface> predictor_variable =
      std::make_unique<NumberOfUserActivityEventsPredictorVariable>(
          UserActivityEventType::kOpenedNewTab,
          shunya_federated::mojom::CovariateType::kNumberOfOpenedNewTabEvents);

  UserActivityManager::GetInstance().RecordEvent(
      UserActivityEventType::kOpenedNewTab);
  UserActivityManager::GetInstance().RecordEvent(
      UserActivityEventType::kBrowserDidResignActive);

  AdvanceClockBy(base::Minutes(31));

  UserActivityManager::GetInstance().RecordEvent(
      UserActivityEventType::kBrowserDidBecomeActive);
  UserActivityManager::GetInstance().RecordEvent(
      UserActivityEventType::kOpenedNewTab);
  UserActivityManager::GetInstance().RecordEvent(
      UserActivityEventType::kClosedTab);
  UserActivityManager::GetInstance().RecordEvent(
      UserActivityEventType::kOpenedNewTab);

  // Act

  // Assert
  EXPECT_EQ("2", predictor_variable->GetValue());
}

}  // namespace shunya_ads

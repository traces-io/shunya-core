/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_TIME_SINCE_LAST_USER_ACTIVITY_EVENT_PREDICTOR_VARIABLE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_TIME_SINCE_LAST_USER_ACTIVITY_EVENT_PREDICTOR_VARIABLE_H_

#include <string>

#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/predictor_variable_interface.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_event_types.h"

namespace shunya_ads {

class TimeSinceLastUserActivityEventPredictorVariable final
    : public PredictorVariableInterface {
 public:
  TimeSinceLastUserActivityEventPredictorVariable(
      UserActivityEventType event_type,
      shunya_federated::mojom::CovariateType predictor_type);

  // PredictorVariableInterface:
  shunya_federated::mojom::DataType GetDataType() const override;
  shunya_federated::mojom::CovariateType GetType() const override;
  std::string GetValue() const override;

 private:
  const UserActivityEventType event_type_;
  const shunya_federated::mojom::CovariateType predictor_type_;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_TIME_SINCE_LAST_USER_ACTIVITY_EVENT_PREDICTOR_VARIABLE_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/notification_ad_event_predictor_variable.h"

#include <sstream>

#include "base/check.h"

namespace shunya_ads {

NotificationAdEventPredictorVariable::NotificationAdEventPredictorVariable(
    const mojom::NotificationAdEventType event_type)
    : event_type_(event_type) {
  CHECK(mojom::IsKnownEnumValue(event_type_));
}

shunya_federated::mojom::DataType
NotificationAdEventPredictorVariable::GetDataType() const {
  return shunya_federated::mojom::DataType::kString;
}

shunya_federated::mojom::CovariateType
NotificationAdEventPredictorVariable::GetType() const {
  return shunya_federated::mojom::CovariateType::kNotificationAdEvent;
}

std::string NotificationAdEventPredictorVariable::GetValue() const {
  std::stringstream ss;
  ss << event_type_;
  return ss.str();
}

}  // namespace shunya_ads

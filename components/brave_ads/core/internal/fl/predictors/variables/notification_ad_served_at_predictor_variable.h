/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_NOTIFICATION_AD_SERVED_AT_PREDICTOR_VARIABLE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_NOTIFICATION_AD_SERVED_AT_PREDICTOR_VARIABLE_H_

#include <string>

#include "base/time/time.h"
#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/predictor_variable_interface.h"

namespace shunya_ads {

class NotificationAdServedAtPredictorVariable final
    : public PredictorVariableInterface {
 public:
  explicit NotificationAdServedAtPredictorVariable(base::Time time);

  // PredictorVariableInterface:
  shunya_federated::mojom::DataType GetDataType() const override;
  shunya_federated::mojom::CovariateType GetType() const override;
  std::string GetValue() const override;

 private:
  const base::Time time_;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_NOTIFICATION_AD_SERVED_AT_PREDICTOR_VARIABLE_H_

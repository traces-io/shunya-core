/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_PREDICTOR_VARIABLE_INTERFACE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_PREDICTOR_VARIABLE_INTERFACE_H_

#include <string>

#include "shunya/components/shunya_federated/public/interfaces/shunya_federated.mojom-shared.h"

namespace shunya_ads {

class PredictorVariableInterface {
 public:
  virtual ~PredictorVariableInterface() = default;

  virtual shunya_federated::mojom::DataType GetDataType() const = 0;
  virtual shunya_federated::mojom::CovariateType GetType() const = 0;
  virtual std::string GetValue() const = 0;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FL_PREDICTORS_VARIABLES_PREDICTOR_VARIABLE_INTERFACE_H_

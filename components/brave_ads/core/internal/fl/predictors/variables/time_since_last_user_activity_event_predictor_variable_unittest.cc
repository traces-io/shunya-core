/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/fl/predictors/variables/time_since_last_user_activity_event_predictor_variable.h"

#include <memory>

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_manager.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsTimeSinceLastUserActivityEventPredictorVariableTest
    : public UnitTestBase {};

TEST_F(ShunyaAdsTimeSinceLastUserActivityEventPredictorVariableTest,
       GetDataType) {
  // Arrange
  std::unique_ptr<PredictorVariableInterface> predictor_variable =
      std::make_unique<TimeSinceLastUserActivityEventPredictorVariable>(
          UserActivityEventType::kOpenedNewTab,
          shunya_federated::mojom::CovariateType::
              kTimeSinceLastOpenedNewTabEvent);

  // Act

  // Assert
  EXPECT_EQ(shunya_federated::mojom::DataType::kInt,
            predictor_variable->GetDataType());
}

TEST_F(ShunyaAdsTimeSinceLastUserActivityEventPredictorVariableTest,
       GetValueForNoHistory) {
  // Arrange
  std::unique_ptr<PredictorVariableInterface> predictor_variable =
      std::make_unique<TimeSinceLastUserActivityEventPredictorVariable>(
          UserActivityEventType::kOpenedNewTab,
          shunya_federated::mojom::CovariateType::
              kTimeSinceLastOpenedNewTabEvent);

  // Act

  // Assert
  EXPECT_EQ("-1", predictor_variable->GetValue());
}

TEST_F(ShunyaAdsTimeSinceLastUserActivityEventPredictorVariableTest, GetValue) {
  // Arrange
  std::unique_ptr<PredictorVariableInterface> predictor_variable =
      std::make_unique<TimeSinceLastUserActivityEventPredictorVariable>(
          UserActivityEventType::kOpenedNewTab,
          shunya_federated::mojom::CovariateType::
              kTimeSinceLastOpenedNewTabEvent);

  UserActivityManager::GetInstance().RecordEvent(
      UserActivityEventType::kOpenedNewTab);

  AdvanceClockBy(base::Minutes(2));

  UserActivityManager::GetInstance().RecordEvent(
      UserActivityEventType::kClosedTab);

  // Act

  // Assert
  EXPECT_EQ("120", predictor_variable->GetValue());
}

}  // namespace shunya_ads

# Global State

Stores global state for Shunya Ads. **DO NOT** add any new global state.

Please add to it!

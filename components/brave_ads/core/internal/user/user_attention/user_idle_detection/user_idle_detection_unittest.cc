/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_idle_detection/user_idle_detection.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsUserIdleDetectionTest : public UnitTestBase {};

TEST_F(ShunyaAdsUserIdleDetectionTest, RewardsUserDidBecomeActive) {
  // Arrange
  const UserIdleDetection user_idle_detection;

  // Assert
  EXPECT_CALL(ads_client_mock_, Log);

  // Act
  NotifyUserDidBecomeActive(/*idle_time*/ base::Seconds(10),
                            /*screen_was_locked*/ false);
}

TEST_F(ShunyaAdsUserIdleDetectionTest, NonRewardsUserDidBecomeActive) {
  // Arrange
  DisableShunyaRewardsForTesting();

  const UserIdleDetection user_idle_detection;

  // Assert
  EXPECT_CALL(ads_client_mock_, Log).Times(0);

  // Act
  NotifyUserDidBecomeActive(/*idle_time*/ base::Seconds(10),
                            /*screen_was_locked*/ false);
}

TEST_F(ShunyaAdsUserIdleDetectionTest,
       RewardsUserDidBecomeActiveWhileScreenWasLocked) {
  // Arrange
  const UserIdleDetection user_idle_detection;

  // Assert
  EXPECT_CALL(ads_client_mock_, Log).Times(2);

  // Act
  NotifyUserDidBecomeActive(/*idle_time*/ base::Seconds(10),
                            /*screen_was_locked*/ true);
}

TEST_F(ShunyaAdsUserIdleDetectionTest,
       NonRewardsUserDidBecomeActiveWhileScreenWasLocked) {
  // Arrange
  DisableShunyaRewardsForTesting();

  const UserIdleDetection user_idle_detection;

  // Assert
  EXPECT_CALL(ads_client_mock_, Log).Times(0);

  // Act
  NotifyUserDidBecomeActive(/*idle_time*/ base::Seconds(10),
                            /*screen_was_locked*/ true);
}

TEST_F(ShunyaAdsUserIdleDetectionTest, RewardsUserDidBecomeIdle) {
  // Arrange
  const UserIdleDetection user_idle_detection;

  // Assert
  EXPECT_CALL(ads_client_mock_, Log);

  // Act
  NotifyUserDidBecomeIdle();
}

TEST_F(ShunyaAdsUserIdleDetectionTest, NonRewardsUserDidBecomeIdle) {
  // Arrange
  DisableShunyaRewardsForTesting();

  const UserIdleDetection user_idle_detection;

  // Assert
  EXPECT_CALL(ads_client_mock_, Log).Times(0);

  // Act
  NotifyUserDidBecomeIdle();
}

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/page_transition_util.h"

#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsPageTransitionUtilTest, IsNewNavigation) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(IsNewNavigation(kPageTransitionTyped));
}

TEST(ShunyaAdsPageTransitionUtilTest,
     DidUseBackOrFowardButtonToTriggerNavigation) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(
      DidUseBackOrFowardButtonToTriggerNavigation(kPageTransitionForwardBack));
}

TEST(ShunyaAdsPageTransitionUtilTest, DidUseAddressBarToTriggerNavigation) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(
      DidUseAddressBarToTriggerNavigation(kPageTransitionFromAddressBar));
}

TEST(ShunyaAdsPageTransitionUtilTest, DidNavigateToHomePage) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DidNavigateToHomePage(kPageTransitionHomePage));
}

TEST(ShunyaAdsPageTransitionUtilTest, DidTransitionFromExternalApplication) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DidTransitionFromExternalApplication(kPageTransitionFromAPI));
}

TEST(ShunyaAdsPageTransitionUtilTest, ToUserActivityClickedLinkEventType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(UserActivityEventType::kClickedLink,
            ToUserActivityEventType(kPageTransitionLink));
}

TEST(ShunyaAdsPageTransitionUtilTest, ToUserActivityTypedUrlEventType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(UserActivityEventType::kTypedUrl,
            ToUserActivityEventType(kPageTransitionTyped));
}

TEST(ShunyaAdsPageTransitionUtilTest, ToUserActivityClickedBookmarkEventType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(UserActivityEventType::kClickedBookmark,
            ToUserActivityEventType(kPageTransitionAutoBookmark));
}

TEST(ShunyaAdsPageTransitionUtilTest,
     ToUserActivityTypedAndSelectedNonUrlEventType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(UserActivityEventType::kTypedAndSelectedNonUrl,
            ToUserActivityEventType(kPageTransitionGenerated));
}

TEST(ShunyaAdsPageTransitionUtilTest, ToUserActivitySubmittedFormEventType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(UserActivityEventType::kSubmittedForm,
            ToUserActivityEventType(kPageTransitionFormSubmit));
}

TEST(ShunyaAdsPageTransitionUtilTest,
     ToUserActivityClickedReloadButtonEventType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(UserActivityEventType::kClickedReloadButton,
            ToUserActivityEventType(kPageTransitionReload));
}

TEST(ShunyaAdsPageTransitionUtilTest,
     ToUserActivityTypedKeywordOtherThanDefaultSearchProviderEventType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(UserActivityEventType::kTypedKeywordOtherThanDefaultSearchProvider,
            ToUserActivityEventType(kPageTransitionKeyword));
}

TEST(ShunyaAdsPageTransitionUtilTest, ToUserActivityGeneratedKeywordEventType) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(UserActivityEventType::kGeneratedKeyword,
            ToUserActivityEventType(kPageTransitionKeywordGenerated));
}

}  // namespace shunya_ads

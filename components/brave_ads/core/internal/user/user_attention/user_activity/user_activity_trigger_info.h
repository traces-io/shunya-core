/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_TRIGGER_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_TRIGGER_INFO_H_

#include <string>
#include <vector>

namespace shunya_ads {

struct UserActivityTriggerInfo final {
  std::string event_sequence;
  double score = 0.0;
};

bool operator==(const UserActivityTriggerInfo&, const UserActivityTriggerInfo&);
bool operator!=(const UserActivityTriggerInfo&, const UserActivityTriggerInfo&);

using UserActivityTriggerList = std::vector<UserActivityTriggerInfo>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_TRIGGER_INFO_H_

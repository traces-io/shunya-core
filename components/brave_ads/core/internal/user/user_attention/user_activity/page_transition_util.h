/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_PAGE_TRANSITION_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_PAGE_TRANSITION_UTIL_H_

#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_event_types.h"
#include "shunya/components/shunya_ads/core/public/user/user_attention/user_activity/page_transition_types.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_ads {

bool IsNewNavigation(PageTransitionType type);

bool DidUseBackOrFowardButtonToTriggerNavigation(PageTransitionType type);

bool DidUseAddressBarToTriggerNavigation(PageTransitionType type);

bool DidNavigateToHomePage(PageTransitionType type);

bool DidTransitionFromExternalApplication(PageTransitionType type);

absl::optional<UserActivityEventType> ToUserActivityEventType(
    PageTransitionType type);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_PAGE_TRANSITION_UTIL_H_

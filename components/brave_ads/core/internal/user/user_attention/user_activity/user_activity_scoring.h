/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_SCORING_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_SCORING_H_

#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_event_info.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_trigger_info.h"

namespace shunya_ads {

double GetUserActivityScore(const UserActivityTriggerList& triggers,
                            const UserActivityEventList& events);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_SCORING_H_

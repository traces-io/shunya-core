/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_SCORING_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_SCORING_UTIL_H_

namespace shunya_ads {

bool WasUserActive();

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_SCORING_UTIL_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_scoring_util.h"

#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_event_info.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_feature.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_manager.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_scoring.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_trigger_info.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_util.h"

namespace shunya_ads {

bool WasUserActive() {
  const UserActivityTriggerList triggers =
      ToUserActivityTriggers(kUserActivityTriggers.Get());

  const UserActivityEventList events =
      UserActivityManager::GetInstance().GetHistoryForTimeWindow(
          kUserActivityTimeWindow.Get());

  return GetUserActivityScore(triggers, events) >= kUserActivityThreshold.Get();
}

}  // namespace shunya_ads

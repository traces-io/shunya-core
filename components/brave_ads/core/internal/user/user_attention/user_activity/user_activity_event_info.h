/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_EVENT_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_EVENT_INFO_H_

#include "base/containers/circular_deque.h"
#include "base/time/time.h"
#include "shunya/components/shunya_ads/core/internal/user/user_attention/user_activity/user_activity_event_types.h"

namespace shunya_ads {

struct UserActivityEventInfo final {
  UserActivityEventType type;
  base::Time created_at;
};

bool operator==(const UserActivityEventInfo&, const UserActivityEventInfo&);
bool operator!=(const UserActivityEventInfo&, const UserActivityEventInfo&);

using UserActivityEventList = base::circular_deque<UserActivityEventInfo>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_ATTENTION_USER_ACTIVITY_USER_ACTIVITY_EVENT_INFO_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/notification_ads/notification_ad_event_dismissed.h"

#include <utility>

#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_events.h"
#include "shunya/components/shunya_ads/core/public/account/confirmations/confirmation_type.h"
#include "shunya/components/shunya_ads/core/public/units/notification_ad/notification_ad_info.h"

namespace shunya_ads {

void NotificationAdEventDismissed::FireEvent(const NotificationAdInfo& ad,
                                             ResultCallback callback) {
  RecordAdEvent(ad, ConfirmationType::kDismissed, std::move(callback));
}

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_NOTIFICATION_ADS_NOTIFICATION_AD_EVENT_HANDLER_DELEGATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_NOTIFICATION_ADS_NOTIFICATION_AD_EVENT_HANDLER_DELEGATE_H_

#include <string>

#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-shared.h"

namespace shunya_ads {

struct NotificationAdInfo;

class NotificationAdEventHandlerDelegate {
 public:
  // Invoked when the notification |ad| is served.
  virtual void OnDidFireNotificationAdServedEvent(
      const NotificationAdInfo& ad) {}

  // Invoked when the notification |ad| is viewed.
  virtual void OnDidFireNotificationAdViewedEvent(
      const NotificationAdInfo& ad) {}

  // Invoked when the notification |ad| is clicked.
  virtual void OnDidFireNotificationAdClickedEvent(
      const NotificationAdInfo& ad) {}

  // Invoked when the notification |ad| is dismissed.
  virtual void OnDidFireNotificationAdDismissedEvent(
      const NotificationAdInfo& ad) {}

  // Invoked when the notification |ad| times out.
  virtual void OnDidFireNotificationAdTimedOutEvent(
      const NotificationAdInfo& ad) {}

  // Invoked when the notification |ad| event fails for |placement_id| and
  // |event_type|.
  virtual void OnFailedToFireNotificationAdEvent(
      const std::string& placement_id,
      const mojom::NotificationAdEventType event_type) {}

 protected:
  virtual ~NotificationAdEventHandlerDelegate() = default;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_NOTIFICATION_ADS_NOTIFICATION_AD_EVENT_HANDLER_DELEGATE_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_NOTIFICATION_ADS_NOTIFICATION_AD_EVENT_VIEWED_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_NOTIFICATION_ADS_NOTIFICATION_AD_EVENT_VIEWED_H_

#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_event_interface.h"

namespace shunya_ads {

struct NotificationAdInfo;

class NotificationAdEventViewed final
    : public AdEventInterface<NotificationAdInfo> {
 public:
  void FireEvent(const NotificationAdInfo& ad,
                 ResultCallback callback) override;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_NOTIFICATION_ADS_NOTIFICATION_AD_EVENT_VIEWED_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_AD_EVENT_UNITTEST_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_AD_EVENT_UNITTEST_UTIL_H_

#include <cstddef>

namespace base {
class Time;
}  // namespace base

namespace shunya_ads {

class AdType;
class ConfirmationType;
struct AdEventInfo;
struct CreativeAdInfo;

AdEventInfo BuildAdEventForTesting(const CreativeAdInfo& creative_ad,
                                   const AdType& ad_type,
                                   const ConfirmationType& confirmation_type,
                                   base::Time created_at,
                                   bool should_use_random_uuids);

void RecordAdEventForTesting(const AdType& type,
                             const ConfirmationType& confirmation_type);
void RecordAdEventsForTesting(const AdType& type,
                              const ConfirmationType& confirmation_type,
                              int count);

void FireAdEventForTesting(const AdEventInfo& ad_event);
void FireAdEventsForTesting(const AdEventInfo& ad_event, size_t count);

size_t GetAdEventCountForTesting(const AdType& ad_type,
                                 const ConfirmationType& confirmation_type);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_AD_EVENT_UNITTEST_UTIL_H_

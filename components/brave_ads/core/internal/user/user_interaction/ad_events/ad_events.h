/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_AD_EVENTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_AD_EVENTS_H_

#include "base/functional/callback.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-shared.h"

namespace shunya_ads {

using AdEventCallback = base::OnceCallback<void(bool success)>;

class ConfirmationType;
struct AdEventInfo;
struct AdInfo;

void RecordAdEvent(const AdInfo& ad,
                   const ConfirmationType& confirmation_type,
                   AdEventCallback callback);
void RecordAdEvent(const AdEventInfo& ad_event, AdEventCallback callback);

void PurgeExpiredAdEvents(AdEventCallback callback);

void PurgeOrphanedAdEvents(mojom::AdType ad_type, AdEventCallback callback);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_AD_EVENTS_H_

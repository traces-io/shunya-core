/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_INLINE_CONTENT_ADS_INLINE_CONTENT_AD_EVENT_HANDLER_DELEGATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_INLINE_CONTENT_ADS_INLINE_CONTENT_AD_EVENT_HANDLER_DELEGATE_H_

#include <string>

#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-forward.h"

namespace shunya_ads {

struct InlineContentAdInfo;

class InlineContentAdEventHandlerDelegate {
 public:
  // Invoked when the inline content |ad| is served.
  virtual void OnDidFireInlineContentAdServedEvent(
      const InlineContentAdInfo& ad) {}

  // Invoked when the inline content |ad| is viewed.
  virtual void OnDidFireInlineContentAdViewedEvent(
      const InlineContentAdInfo& ad) {}

  // Invoked when the inline content |ad| is clicked.
  virtual void OnDidFireInlineContentAdClickedEvent(
      const InlineContentAdInfo& ad) {}

  // Invoked when the inline content |ad| event fails for |placement_id|,
  // |creative_instance_id| and |event_type|.
  virtual void OnFailedToFireInlineContentAdEvent(
      const std::string& placement_id,
      const std::string& creative_instance_id,
      const mojom::InlineContentAdEventType event_type) {}

 protected:
  virtual ~InlineContentAdEventHandlerDelegate() = default;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_USER_USER_INTERACTION_AD_EVENTS_INLINE_CONTENT_ADS_INLINE_CONTENT_AD_EVENT_HANDLER_DELEGATE_H_

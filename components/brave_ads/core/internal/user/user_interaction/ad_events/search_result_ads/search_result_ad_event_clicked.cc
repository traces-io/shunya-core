/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/search_result_ads/search_result_ad_event_clicked.h"

#include <utility>

#include "shunya/components/shunya_ads/core/internal/creatives/search_result_ads/search_result_ad_info.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/ad_events.h"
#include "shunya/components/shunya_ads/core/public/account/confirmations/confirmation_type.h"

namespace shunya_ads {

void SearchResultAdEventClicked::FireEvent(const SearchResultAdInfo& ad,
                                           ResultCallback callback) {
  RecordAdEvent(ad, ConfirmationType::kClicked, std::move(callback));
}

}  // namespace shunya_ads

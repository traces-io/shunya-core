/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/units/notification_ad/notification_ad_handler_util.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_mock_util.h"
#include "shunya/components/shunya_ads/core/internal/creatives/notification_ads/creative_notification_ad_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/creatives/notification_ads/notification_ad_builder.h"
#include "shunya/components/shunya_ads/core/internal/creatives/notification_ads/notification_ad_manager.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"
#include "shunya/components/shunya_ads/core/public/units/notification_ad/notification_ad_info.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

namespace {

void BuildAndShowNotificationAd() {
  const CreativeNotificationAdInfo creative_ad =
      BuildCreativeNotificationAdForTesting(/*should_use_random_uuids*/ true);
  const NotificationAdInfo ad = BuildNotificationAd(creative_ad);
  ShowNotificationAd(ad);
}

}  // namespace

class ShunyaAdsNotificationAdUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsNotificationAdUtilTest, CanServeIfUserIsActive) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(CanServeIfUserIsActive());
}

TEST_F(ShunyaAdsNotificationAdUtilTest, DoNotServeIfUserIsActive) {
  // Arrange
  MockPlatformHelper(platform_helper_mock_, PlatformType::kAndroid);

  // Act

  // Assert
  EXPECT_FALSE(CanServeIfUserIsActive());
}

TEST_F(ShunyaAdsNotificationAdUtilTest, ShouldServe) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(ShouldServe());
}

TEST_F(ShunyaAdsNotificationAdUtilTest,
       ShouldNotServeIfOptedOutOfNotificationAds) {
  // Arrange
  DisableNotificationAdsForTesting();

  // Act

  // Assert
  EXPECT_FALSE(ShouldServe());
}

TEST_F(ShunyaAdsNotificationAdUtilTest, CanServeAtRegularIntervals) {
  // Arrange
  MockPlatformHelper(platform_helper_mock_, PlatformType::kAndroid);

  // Act

  // Assert
  EXPECT_TRUE(CanServeAtRegularIntervals());
}

TEST_F(ShunyaAdsNotificationAdUtilTest, DoNotServeAtRegularIntervals) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(CanServeAtRegularIntervals());
}

TEST_F(ShunyaAdsNotificationAdUtilTest, ShowNotificationAd) {
  // Arrange
  EXPECT_CALL(ads_client_mock_, ShowNotificationAd)
      .WillOnce(::testing::Invoke([](const NotificationAdInfo& ad) {
        // Act

        // Assert
        ASSERT_TRUE(
            NotificationAdManager::GetInstance().Exists(ad.placement_id));
      }));

  BuildAndShowNotificationAd();
}

TEST_F(ShunyaAdsNotificationAdUtilTest, DismissNotificationAd) {
  // Arrange
  EXPECT_CALL(ads_client_mock_, ShowNotificationAd)
      .WillOnce(::testing::Invoke([](const NotificationAdInfo& ad) {
        ASSERT_TRUE(
            NotificationAdManager::GetInstance().Exists(ad.placement_id));

        // Act
        DismissNotificationAd(ad.placement_id);

        // Assert
        ASSERT_FALSE(
            NotificationAdManager::GetInstance().Exists(ad.placement_id));
      }));

  BuildAndShowNotificationAd();
}

TEST_F(ShunyaAdsNotificationAdUtilTest, CloseNotificationAd) {
  // Arrange
  EXPECT_CALL(ads_client_mock_, CloseNotificationAd)
      .WillOnce(::testing::Invoke([](const std::string& placement_id) {
        // Act

        // Assert
        ASSERT_FALSE(NotificationAdManager::GetInstance().Exists(placement_id));
      }));

  EXPECT_CALL(ads_client_mock_, ShowNotificationAd)
      .WillOnce(::testing::Invoke([](const NotificationAdInfo& ad) {
        ASSERT_TRUE(
            NotificationAdManager::GetInstance().Exists(ad.placement_id));

        // Act
        CloseNotificationAd(ad.placement_id);

        // Assert
        ASSERT_FALSE(
            NotificationAdManager::GetInstance().Exists(ad.placement_id));
      }));

  BuildAndShowNotificationAd();
}

TEST_F(ShunyaAdsNotificationAdUtilTest, NotificationAdTimedOut) {
  // Arrange
  EXPECT_CALL(ads_client_mock_, ShowNotificationAd)
      .WillOnce(::testing::Invoke([](const NotificationAdInfo& ad) {
        ASSERT_TRUE(
            NotificationAdManager::GetInstance().Exists(ad.placement_id));

        // Act
        NotificationAdTimedOut(ad.placement_id);

        // Assert
        ASSERT_FALSE(
            NotificationAdManager::GetInstance().Exists(ad.placement_id));
      }));

  BuildAndShowNotificationAd();
}

}  // namespace shunya_ads

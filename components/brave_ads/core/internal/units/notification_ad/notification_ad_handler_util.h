/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_HANDLER_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_HANDLER_UTIL_H_

#include <string>

namespace shunya_ads {

struct NotificationAdInfo;

bool ShouldServe();

bool CanServeIfUserIsActive();

bool CanServeAtRegularIntervals();
bool ShouldServeAtRegularIntervals();

void ShowNotificationAd(const NotificationAdInfo& ad);
void CloseNotificationAd(const std::string& placement_id);
void DismissNotificationAd(const std::string& placement_id);
void NotificationAdTimedOut(const std::string& placement_id);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_HANDLER_UTIL_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_UNITS_PROMOTED_CONTENT_AD_PROMOTED_CONTENT_AD_HANDLER_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_UNITS_PROMOTED_CONTENT_AD_PROMOTED_CONTENT_AD_HANDLER_H_

#include <string>

#include "base/memory/raw_ref.h"
#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/promoted_content_ads/promoted_content_ad_event_handler.h"
#include "shunya/components/shunya_ads/core/internal/user/user_interaction/ad_events/promoted_content_ads/promoted_content_ad_event_handler_delegate.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-shared.h"
#include "shunya/components/shunya_ads/core/public/ads_callback.h"

namespace shunya_ads {

class Account;
class Transfer;
struct PromotedContentAdInfo;

class PromotedContentAdHandler final
    : public PromotedContentAdEventHandlerDelegate {
 public:
  PromotedContentAdHandler(Account& account, Transfer& transfer);

  PromotedContentAdHandler(const PromotedContentAdHandler&) = delete;
  PromotedContentAdHandler& operator=(const PromotedContentAdHandler&) = delete;

  PromotedContentAdHandler(PromotedContentAdHandler&&) noexcept = delete;
  PromotedContentAdHandler& operator=(PromotedContentAdHandler&&) noexcept =
      delete;

  ~PromotedContentAdHandler() override;

  void TriggerEvent(const std::string& placement_id,
                    const std::string& creative_instance_id,
                    mojom::PromotedContentAdEventType event_type,
                    TriggerAdEventCallback callback);

 private:
  void TriggerServedEventCallback(const std::string& creative_instance_id,
                                  TriggerAdEventCallback callback,
                                  bool success,
                                  const std::string& placement_id,
                                  mojom::PromotedContentAdEventType event_type);

  // PromotedContentAdEventHandlerDelegate:
  void OnDidFirePromotedContentAdServedEvent(
      const PromotedContentAdInfo& ad) override;
  void OnDidFirePromotedContentAdViewedEvent(
      const PromotedContentAdInfo& ad) override;
  void OnDidFirePromotedContentAdClickedEvent(
      const PromotedContentAdInfo& ad) override;

  const raw_ref<Account> account_;
  const raw_ref<Transfer> transfer_;

  PromotedContentAdEventHandler event_handler_;

  base::WeakPtrFactory<PromotedContentAdHandler> weak_factory_{this};
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_UNITS_PROMOTED_CONTENT_AD_PROMOTED_CONTENT_AD_HANDLER_H_

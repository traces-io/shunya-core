/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/public/units/promoted_content_ad/promoted_content_ad_info.h"

namespace shunya_ads {

bool PromotedContentAdInfo::IsValid() const {
  return AdInfo::IsValid() && !title.empty() && !description.empty();
}

}  // namespace shunya_ads

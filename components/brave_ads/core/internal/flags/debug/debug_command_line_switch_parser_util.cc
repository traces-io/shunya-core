/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/flags/debug/debug_command_line_switch_parser_util.h"

#include "shunya/components/shunya_rewards/common/rewards_flags.h"

namespace shunya_ads {

bool ParseDebugCommandLineSwitch() {
  return shunya_rewards::RewardsFlags::ForCurrentProcess().debug;
}

}  // namespace shunya_ads

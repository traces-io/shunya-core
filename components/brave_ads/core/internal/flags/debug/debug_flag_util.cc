/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/flags/debug/debug_flag_util.h"

#include "shunya/components/shunya_ads/core/internal/global_state/global_state.h"

namespace shunya_ads {

bool ShouldDebug() {
  return GlobalState::GetInstance()->Flags().should_debug;
}

}  // namespace shunya_ads

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FLAGS_DID_OVERRIDE_DID_OVERRIDE_COMMAND_LINE_SWITCHES_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FLAGS_DID_OVERRIDE_DID_OVERRIDE_COMMAND_LINE_SWITCHES_UTIL_H_

namespace shunya_ads {

bool DidOverrideCommandLineSwitches();

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_FLAGS_DID_OVERRIDE_DID_OVERRIDE_COMMAND_LINE_SWITCHES_UTIL_H_

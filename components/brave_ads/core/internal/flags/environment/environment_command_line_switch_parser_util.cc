/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/flags/environment/environment_command_line_switch_parser_util.h"

#include "base/notreached.h"
#include "shunya/components/shunya_rewards/common/rewards_flags.h"

namespace shunya_ads {

absl::optional<mojom::EnvironmentType> ParseEnvironmentCommandLineSwitch() {
  const shunya_rewards::RewardsFlags& flags =
      shunya_rewards::RewardsFlags::ForCurrentProcess();
  if (!flags.environment) {
    return absl::nullopt;
  }

  switch (*flags.environment) {
    case shunya_rewards::RewardsFlags::Environment::kDevelopment:
    case shunya_rewards::RewardsFlags::Environment::kStaging: {
      return mojom::EnvironmentType::kStaging;
    }

    case shunya_rewards::RewardsFlags::Environment::kProduction: {
      return mojom::EnvironmentType::kProduction;
    }
  }

  NOTREACHED_NORETURN() << "Unexpected value for Environment: "
                        << static_cast<int>(*flags.environment);
}

}  // namespace shunya_ads

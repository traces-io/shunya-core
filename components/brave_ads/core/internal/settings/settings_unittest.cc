/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/settings/settings.h"

#include <vector>

#include "base/test/scoped_feature_list.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_pref_util.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"
#include "shunya/components/shunya_ads/core/public/units/notification_ad/notification_ad_feature.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsSettingsTest : public UnitTestBase {};

TEST_F(ShunyaAdsSettingsTest, UserHasJoinedShunyaRewards) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(UserHasJoinedShunyaRewards());
}

TEST_F(ShunyaAdsSettingsTest, UserHasNotJoinedShunyaRewards) {
  // Arrange
  DisableShunyaRewardsForTesting();

  // Act

  // Assert
  EXPECT_FALSE(UserHasJoinedShunyaRewards());
}

TEST_F(ShunyaAdsSettingsTest, UserHasOptedInToShunyaNewsAds) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(UserHasOptedInToShunyaNewsAds());
}

TEST_F(ShunyaAdsSettingsTest, UserHasNotOptedInToShunyaNews) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  // Act

  // Assert
  EXPECT_FALSE(UserHasOptedInToShunyaNewsAds());
}

TEST_F(ShunyaAdsSettingsTest, UserHasOptedInToNewTabPageAds) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(UserHasOptedInToNewTabPageAds());
}

TEST_F(ShunyaAdsSettingsTest, UserHasNotOptedInToNewTabPageAds) {
  // Arrange
  DisableNewTabPageAdsForTesting();

  // Act

  // Assert
  EXPECT_FALSE(UserHasOptedInToNewTabPageAds());
}

TEST_F(ShunyaAdsSettingsTest, UserHasOptedInToNotificationAds) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(UserHasOptedInToNotificationAds());
}

TEST_F(ShunyaAdsSettingsTest, UserHasNotOptedInToNotificationAds) {
  // Arrange
  DisableNotificationAdsForTesting();

  // Act

  // Assert
  EXPECT_FALSE(UserHasOptedInToNotificationAds());
}

TEST_F(ShunyaAdsSettingsTest, MaximumNotificationAdsPerHour) {
  // Arrange
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  base::FieldTrialParams params;
  params["default_ads_per_hour"] = "2";
  enabled_features.emplace_back(kNotificationAdFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  SetInt64Pref(prefs::kMaximumNotificationAdsPerHour, 3);

  // Act

  // Assert
  EXPECT_EQ(3, GetMaximumNotificationAdsPerHour());
}

TEST_F(ShunyaAdsSettingsTest, DefaultMaximumNotificationAdsPerHour) {
  // Arrange
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  base::FieldTrialParams params;
  params["default_ads_per_hour"] = "2";
  enabled_features.emplace_back(kNotificationAdFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_EQ(2, GetMaximumNotificationAdsPerHour());
}

}  // namespace shunya_ads

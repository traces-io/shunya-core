/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/settings/settings.h"

#include "shunya/components/shunya_ads/core/internal/client/ads_client_helper.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"
#include "shunya/components/shunya_ads/core/public/units/notification_ad/notification_ad_feature.h"
#include "shunya/components/shunya_news/common/pref_names.h"
#include "shunya/components/shunya_rewards/common/pref_names.h"
#include "shunya/components/ntp_background_images/common/pref_names.h"

namespace shunya_ads {

bool UserHasJoinedShunyaRewards() {
  return AdsClientHelper::GetInstance()->GetBooleanPref(
      shunya_rewards::prefs::kEnabled);
}

bool UserHasOptedInToShunyaNewsAds() {
  return AdsClientHelper::GetInstance()->GetBooleanPref(
             shunya_news::prefs::kShunyaNewsOptedIn) &&
         AdsClientHelper::GetInstance()->GetBooleanPref(
             shunya_news::prefs::kNewTabPageShowToday);
}

bool UserHasOptedInToNewTabPageAds() {
  return AdsClientHelper::GetInstance()->GetBooleanPref(
             ntp_background_images::prefs::kNewTabPageShowBackgroundImage) &&
         AdsClientHelper::GetInstance()->GetBooleanPref(
             ntp_background_images::prefs::
                 kNewTabPageShowSponsoredImagesBackgroundImage);
}

bool UserHasOptedInToNotificationAds() {
  return UserHasJoinedShunyaRewards() &&
         AdsClientHelper::GetInstance()->GetBooleanPref(
             prefs::kOptedInToNotificationAds);
}

int GetMaximumNotificationAdsPerHour() {
  const int ads_per_hour =
      static_cast<int>(AdsClientHelper::GetInstance()->GetInt64Pref(
          prefs::kMaximumNotificationAdsPerHour));

  return ads_per_hour > 0 ? ads_per_hour : kDefaultNotificationAdsPerHour.Get();
}

}  // namespace shunya_ads

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/public/ads_feature.h"

namespace shunya_ads {

BASE_FEATURE(kShouldLaunchShunyaAdsAsAnInProcessServiceFeature,
             "ShouldLaunchShunyaAdsAsInProcessService",
             base::FEATURE_DISABLED_BY_DEFAULT);

bool ShouldLaunchAsInProcessService() {
  return base::FeatureList::IsEnabled(
      kShouldLaunchShunyaAdsAsAnInProcessServiceFeature);
}

BASE_FEATURE(kShouldAlwaysRunShunyaAdsServiceFeature,
             "ShouldAlwaysRunShunyaAdsService",
             base::FEATURE_DISABLED_BY_DEFAULT);

bool ShouldAlwaysRunService() {
  return base::FeatureList::IsEnabled(kShouldAlwaysRunShunyaAdsServiceFeature);
}

BASE_FEATURE(kShouldAlwaysTriggerShunyaNewTabPageAdEventsFeature,
             "ShouldAlwaysTriggerShunyaNewTabPageAdEvents",
             base::FEATURE_DISABLED_BY_DEFAULT);

bool ShouldAlwaysTriggerNewTabPageAdEvents() {
  return base::FeatureList::IsEnabled(
      kShouldAlwaysTriggerShunyaNewTabPageAdEventsFeature);
}

BASE_FEATURE(kShouldSupportSearchResultAdsFeature,
             "ShouldSupportSearchResultAds",
             base::FEATURE_DISABLED_BY_DEFAULT);

bool ShouldSupportSearchResultAds() {
  return base::FeatureList::IsEnabled(kShouldSupportSearchResultAdsFeature);
}

BASE_FEATURE(kShouldAlwaysTriggerShunyaSearchResultAdEventsFeature,
             "ShouldAlwaysTriggerShunyaSearchResultAdEvents",
             base::FEATURE_DISABLED_BY_DEFAULT);

bool ShouldAlwaysTriggerSearchResultAdEvents() {
  return base::FeatureList::IsEnabled(
      kShouldAlwaysTriggerShunyaSearchResultAdEventsFeature);
}

}  // namespace shunya_ads

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SEGMENTS_SEGMENT_ALIAS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SEGMENTS_SEGMENT_ALIAS_H_

#include <string>
#include <vector>

namespace shunya_ads {

using SegmentList = std::vector<std::string>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_SEGMENTS_SEGMENT_ALIAS_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/segments/segment_value_util.h"

#include "base/test/values_test_util.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

namespace {

constexpr char kSegmentsAsJson[] =
    R"(["technology & computing","personal finance-banking","food & drink-restaurants"])";
constexpr char kNoSegmentsAsJson[] = "[]";

}  // namespace

TEST(ShunyaAdsSegmentValueUtilTest, SegmentsToValue) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(
      base::test::ParseJsonList(kSegmentsAsJson),
      SegmentsToValue({"technology & computing", "personal finance-banking",
                       "food & drink-restaurants"}));
}

TEST(ShunyaAdsSegmentValueUtilTest, NoSegmentsToValue) {
  // Arrange

  // Act
  const base::Value::List list = SegmentsToValue({});

  // Assert
  EXPECT_TRUE(list.empty());
}

TEST(ShunyaAdsSegmentValueUtilTest, SegmentsFromValue) {
  // Arrange
  const base::Value::List list = base::test::ParseJsonList(kSegmentsAsJson);

  // Act

  // Assert
  const SegmentList expected_segments = {"technology & computing",
                                         "personal finance-banking",
                                         "food & drink-restaurants"};
  EXPECT_EQ(expected_segments, SegmentsFromValue(list));
}

TEST(ShunyaAdsSegmentValueUtilTest, NoSegmentsFromValue) {
  // Arrange
  const base::Value::List list = base::test::ParseJsonList(kNoSegmentsAsJson);

  // Act
  const SegmentList segments = SegmentsFromValue(list);

  // Assert
  EXPECT_TRUE(segments.empty());
}

}  // namespace shunya_ads

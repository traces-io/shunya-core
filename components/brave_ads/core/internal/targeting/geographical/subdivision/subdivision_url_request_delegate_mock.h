/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TARGETING_GEOGRAPHICAL_SUBDIVISION_SUBDIVISION_URL_REQUEST_DELEGATE_MOCK_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TARGETING_GEOGRAPHICAL_SUBDIVISION_SUBDIVISION_URL_REQUEST_DELEGATE_MOCK_H_

#include <string>

#include "shunya/components/shunya_ads/core/internal/targeting/geographical/subdivision/subdivision_url_request_delegate.h"
#include "testing/gmock/include/gmock/gmock.h"

namespace shunya_ads {

class SubdivisionUrlRequestDelegateMock : public SubdivisionUrlRequestDelegate {
 public:
  SubdivisionUrlRequestDelegateMock();

  SubdivisionUrlRequestDelegateMock(const SubdivisionUrlRequestDelegateMock&) =
      delete;
  SubdivisionUrlRequestDelegateMock& operator=(
      const SubdivisionUrlRequestDelegateMock&) = delete;

  SubdivisionUrlRequestDelegateMock(
      SubdivisionUrlRequestDelegateMock&&) noexcept = delete;
  SubdivisionUrlRequestDelegateMock& operator=(
      SubdivisionUrlRequestDelegateMock&&) noexcept = delete;

  ~SubdivisionUrlRequestDelegateMock() override;

  MOCK_METHOD(void, OnWillFetchSubdivision, (const base::Time fetch_at));

  MOCK_METHOD(void, OnDidFetchSubdivision, (const std::string& subdivision));

  MOCK_METHOD(void, OnFailedToFetchSubdivision, ());

  MOCK_METHOD(void,
              OnWillRetryFetchingSubdivision,
              (const base::Time retry_at));

  MOCK_METHOD(void, OnDidRetryFetchingSubdivision, ());
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TARGETING_GEOGRAPHICAL_SUBDIVISION_SUBDIVISION_URL_REQUEST_DELEGATE_MOCK_H_

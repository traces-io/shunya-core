/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/targeting/geographical/subdivision/subdivision_url_request_json_reader_util.h"

#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::json::reader {

TEST(ShunyaAdsSubdivisionUrlRequestJsonReaderUtilTest, ParseValidJson) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ("US-CA", ParseSubdivision(R"({"country":"US","region":"CA"})"));
}

TEST(ShunyaAdsSubdivisionUrlRequestJsonReaderUtilTest, DoNotParseInvalidJson) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ParseSubdivision("{INVALID}"));
}

TEST(ShunyaAdsSubdivisionUrlRequestJsonReaderUtilTest,
     DoNotParseIfMissingCountry) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ParseSubdivision(R"({"region":"CA"})"));
}

TEST(ShunyaAdsSubdivisionUrlRequestJsonReaderUtilTest,
     DoNotParseifEmptyCountry) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ParseSubdivision(R"({"country":"","region":"CA"})"));
}

TEST(ShunyaAdsSubdivisionUrlRequestJsonReaderUtilTest,
     DoNotParseIfMissingRegion) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ParseSubdivision(R"({"country":"US"})"));
}

TEST(ShunyaAdsSubdivisionUrlRequestJsonReaderUtilTest, DoNotParseIfEmptyRegion) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ParseSubdivision(R"({"country":"US","region":""})"));
}

}  // namespace shunya_ads::json::reader

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/targeting/behavioral/multi_armed_bandits/resource/epsilon_greedy_bandit_resource_util.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/segments/segment_alias.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsEpsilonGreedyBanditResourceUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsEpsilonGreedyBanditResourceUtilTest, SetEligibleSegments) {
  const SegmentList expected_segments = {"foo", "bar"};

  // Arrange

  // Act
  SetEpsilonGreedyBanditEligibleSegments(expected_segments);

  // Assert
  EXPECT_EQ(expected_segments, GetEpsilonGreedyBanditEligibleSegments());
}

TEST_F(ShunyaAdsEpsilonGreedyBanditResourceUtilTest, SetNoEligibleSegments) {
  // Arrange

  // Act
  SetEpsilonGreedyBanditEligibleSegments(/*segments*/ {});

  // Assert
  const SegmentList segments = GetEpsilonGreedyBanditEligibleSegments();
  EXPECT_TRUE(segments.empty());
}

}  // namespace shunya_ads

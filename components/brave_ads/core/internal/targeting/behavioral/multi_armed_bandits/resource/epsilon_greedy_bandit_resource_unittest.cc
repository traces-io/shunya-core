/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/targeting/behavioral/multi_armed_bandits/resource/epsilon_greedy_bandit_resource.h"

#include <memory>

#include "base/strings/strcat.h"
#include "shunya/components/shunya_ads/core/internal/catalog/catalog.h"
#include "shunya/components/shunya_ads/core/internal/catalog/catalog_url_request_builder_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_mock_util.h"
#include "shunya/components/shunya_ads/core/internal/settings/settings_unittest_util.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"
#include "net/http/http_status_code.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsEpsilonGreedyBanditResourceTest : public UnitTestBase {
 protected:
  void SetUp() override {
    UnitTestBase::SetUp();

    catalog_ = std::make_unique<Catalog>();
    resource_ = std::make_unique<EpsilonGreedyBanditResource>(*catalog_);
  }

  void LoadResource(const std::string& catalog) {
    const URLResponseMap url_responses = {
        {BuildCatalogUrlPath(),
         {{net::HTTP_OK,
           /*response_body*/ base::StrCat({"/", catalog})}}}};
    MockUrlResponses(ads_client_mock_, url_responses);

    NotifyDidInitializeAds();
  }

  std::unique_ptr<Catalog> catalog_;
  std::unique_ptr<EpsilonGreedyBanditResource> resource_;
};

TEST_F(ShunyaAdsEpsilonGreedyBanditResourceTest, IsNotInitialized) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(resource_->IsInitialized());
}

TEST_F(ShunyaAdsEpsilonGreedyBanditResourceTest,
       LoadResourceIfNotificationAdsAndShunyaNewsAdsAreEnabled) {
  // Arrange

  // Act
  LoadResource("catalog.json");

  // Assert
  EXPECT_TRUE(resource_->IsInitialized());
}

TEST_F(ShunyaAdsEpsilonGreedyBanditResourceTest,
       LoadResourceIfOptedOutOfNotificationAdsAndOptedInToShunyaNewsAds) {
  // Arrange
  DisableNotificationAdsForTesting();

  // Act
  LoadResource("catalog.json");

  // Assert
  EXPECT_TRUE(resource_->IsInitialized());
}

TEST_F(ShunyaAdsEpsilonGreedyBanditResourceTest,
       LoadResourceIfOptedInToNotificationAdsAndOptedOutOfShunyaNewsAds) {
  // Arrange
  DisableShunyaNewsAdsForTesting();

  // Act
  LoadResource("catalog.json");

  // Assert
  EXPECT_TRUE(resource_->IsInitialized());
}

TEST_F(ShunyaAdsEpsilonGreedyBanditResourceTest, LoadResourceIfEmptyCatalog) {
  // Arrange

  // Act
  LoadResource("empty_catalog.json");

  // Assert
  EXPECT_TRUE(resource_->IsInitialized());
}

TEST_F(ShunyaAdsEpsilonGreedyBanditResourceTest,
       DoNotLoadResourceIfNotificationAdsAndShunyaNewsAdsAreDisabled) {
  // Arrange
  DisableNotificationAdsForTesting();
  DisableShunyaNewsAdsForTesting();

  // Act
  LoadResource("catalog.json");

  // Assert
  EXPECT_FALSE(resource_->IsInitialized());
}

TEST_F(
    ShunyaAdsEpsilonGreedyBanditResourceTest,
    ResetResourceWhenOptedInToNotificationAdsPrefDidChangeIfNotificationAdsAndShunyaNewsAdsAreDisabled) {
  // Arrange
  LoadResource("catalog.json");

  DisableNotificationAdsForTesting();
  DisableShunyaNewsAdsForTesting();

  // Act
  NotifyPrefDidChange(prefs::kOptedInToNotificationAds);

  // Assert
  EXPECT_FALSE(resource_->IsInitialized());
}

TEST_F(
    ShunyaAdsEpsilonGreedyBanditResourceTest,
    DoNotResetResourceWhenOptedInToNotificationAdsPrefDidChangeIfNotificationAdsOrShunyaNewsAdsAreEnabled) {
  // Arrange
  LoadResource("catalog.json");

  // Act
  NotifyPrefDidChange(prefs::kOptedInToNotificationAds);

  // Assert
  EXPECT_TRUE(resource_->IsInitialized());
}

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/targeting/behavioral/purchase_intent/purchase_intent_signal_info.h"

namespace shunya_ads {

PurchaseIntentSignalInfo::PurchaseIntentSignalInfo() = default;

PurchaseIntentSignalInfo::PurchaseIntentSignalInfo(
    const PurchaseIntentSignalInfo& other) = default;

PurchaseIntentSignalInfo& PurchaseIntentSignalInfo::operator=(
    const PurchaseIntentSignalInfo& other) = default;

PurchaseIntentSignalInfo::PurchaseIntentSignalInfo(
    PurchaseIntentSignalInfo&& other) noexcept = default;

PurchaseIntentSignalInfo& PurchaseIntentSignalInfo::operator=(
    PurchaseIntentSignalInfo&& other) noexcept = default;

PurchaseIntentSignalInfo::~PurchaseIntentSignalInfo() = default;

}  // namespace shunya_ads

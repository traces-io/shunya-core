/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/targeting/behavioral/anti_targeting/anti_targeting_feature.h"

namespace shunya_ads {

BASE_FEATURE(kAntiTargetingFeature,
             "AntiTargeting",
             base::FEATURE_ENABLED_BY_DEFAULT);

bool IsAntiTargetingFeatureEnabled() {
  return base::FeatureList::IsEnabled(kAntiTargetingFeature);
}

}  // namespace shunya_ads

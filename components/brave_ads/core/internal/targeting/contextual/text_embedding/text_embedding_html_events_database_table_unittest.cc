/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/targeting/contextual/text_embedding/text_embedding_html_events_database_table.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::database::table {

class ShunyaAdsTextEmbeddingHtmlEventsDatabaseTableTest : public UnitTestBase {};

TEST(ShunyaAdsTextEmbeddingHtmlEventsDatabaseTableTest, TableName) {
  // Arrange
  const TextEmbeddingHtmlEvents database_table;

  // Act

  // Assert
  EXPECT_EQ("text_embedding_html_events", database_table.GetTableName());
}

}  // namespace shunya_ads::database::table

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TARGETING_CONTEXTUAL_TEXT_CLASSIFICATION_TEXT_CLASSIFICATION_PROCESSOR_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TARGETING_CONTEXTUAL_TEXT_CLASSIFICATION_TEXT_CLASSIFICATION_PROCESSOR_H_

#include <cstdint>
#include <string>
#include <vector>

#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_ads/core/internal/tabs/tab_manager_observer.h"

class GURL;

namespace shunya_ads {

class TextClassificationResource;

class TextClassificationProcessor final : public TabManagerObserver {
 public:
  explicit TextClassificationProcessor(TextClassificationResource& resource);

  TextClassificationProcessor(const TextClassificationProcessor&) = delete;
  TextClassificationProcessor& operator=(const TextClassificationProcessor&) =
      delete;

  TextClassificationProcessor(TextClassificationProcessor&&) noexcept = delete;
  TextClassificationProcessor& operator=(
      TextClassificationProcessor&&) noexcept = delete;

  ~TextClassificationProcessor() override;

  void Process(const std::string& text);

 private:
  // TabManagerObserver:
  void OnTextContentDidChange(int32_t tab_id,
                              const std::vector<GURL>& redirect_chain,
                              const std::string& text) override;

  const raw_ref<TextClassificationResource> resource_;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_TARGETING_CONTEXTUAL_TEXT_CLASSIFICATION_TEXT_CLASSIFICATION_PROCESSOR_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DEPRECATED_JSON_JSON_HELPER_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DEPRECATED_JSON_JSON_HELPER_H_

#ifdef _MSC_VER
// Resolve Windows build issue due to Windows globally defining GetObject which
// causes RapidJson to fail
#undef GetObject
#endif

#include <string>

#include "shunya/third_party/rapidjson/src/include/rapidjson/document.h"

namespace shunya_ads::helper::json {

[[nodiscard]] bool Validate(rapidjson::Document* document,
                            const std::string& json_schema);

std::string GetLastError(rapidjson::Document* document);

}  // namespace shunya_ads::helper::json

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_DEPRECATED_JSON_JSON_HELPER_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/public/ads_util.h"

#include "shunya/components/l10n/common/locale_util.h"
#include "shunya/components/l10n/common/ofac_sanction_util.h"

namespace shunya_ads {

bool IsSupportedRegion() {
  return !shunya_l10n::IsISOCountryCodeOFACSanctioned(
      shunya_l10n::GetDefaultISOCountryCodeString());
}

}  // namespace shunya_ads

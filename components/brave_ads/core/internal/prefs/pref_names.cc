/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"  // IWYU pragma: keep

namespace shunya_ads::prefs {

// Stores the preferences version number
const char kVersion[] = "shunya.shunya_ads.prefs.current_version";

// Prefix for preference names pertaining to p2a weekly metrics
const char kP2AStoragePrefNamePrefix[] = "shunya.weekly_storage.";

// Stores whether we should show the My First notification ad
const char kShouldShowOnboardingNotification[] =
    "shunya.shunya_ads.should_show_my_first_ad_notification";

// Stores the last normalized screen position of custom notification ads and
// whether to fallback from native to custom notification ads if native
// notifications are disabled
const char kNotificationAdLastNormalizedDisplayCoordinateX[] =
    "shunya.shunya_ads.ad_notification.last_normalized_display_coordinate_x";
const char kNotificationAdLastNormalizedDisplayCoordinateY[] =
    "shunya.shunya_ads.ad_notification.last_normalized_display_coordinate_y";
const char kNotificationAdDidFallbackToCustom[] =
    "shunya.shunya_ads.ad_notification.did_fallback_to_custom";

// Stores the supported country codes current schema version number
const char kSupportedCountryCodesLastSchemaVersion[] =
    "shunya.shunya_ads.supported_regions_last_schema_version_number";

// Stores whether user has opted-in to notifications ads
const char kOptedInToNotificationAds[] = "shunya.shunya_ads.enabled";
const char kEnabledForLastProfile[] = "shunya.shunya_ads.enabled_last_profile";
const char kEverEnabledForAnyProfile[] =
    "shunya.shunya_ads.ever_enabled_any_profile";

// Stores a diagnostic id
const char kDiagnosticId[] = "shunya.shunya_ads.diagnostics.id";

// Stores the maximum number of notification ads per hour
const char kMaximumNotificationAdsPerHour[] = "shunya.shunya_ads.ads_per_hour";

// Notification ads
const char kNotificationAds[] = "shunya.shunya_ads.notification_ads";
const char kServeAdAt[] = "shunya.shunya_ads.serve_ad_at";

// Browser version
const char kBrowserVersionNumber[] = "shunya.shunya_ads.browser_version_number";

// Stores whether Shunya ads should allow subdivision ad targeting
const char kShouldAllowSubdivisionTargeting[] =
    "shunya.shunya_ads.should_allow_ads_subdivision_targeting";

// Stores the selected subdivision targeting code
const char kSubdivisionTargetingSubdivision[] =
    "shunya.shunya_ads.ads_subdivision_targeting_code";

// Stores the automatically detected subdivision targeting code
const char kSubdivisionTargetingAutoDetectedSubdivision[] =
    "shunya.shunya_ads.automatically_detected_ads_subdivision_targeting_code";

// Stores catalog id
const char kCatalogId[] = "shunya.shunya_ads.catalog_id";

// Stores catalog version
const char kCatalogVersion[] = "shunya.shunya_ads.catalog_version";

// Stores catalog ping
const char kCatalogPing[] = "shunya.shunya_ads.catalog_ping";

// Stores catalog last updated
const char kCatalogLastUpdated[] = "shunya.shunya_ads.catalog_last_updated";

// Stores issuers
const char kIssuerPing[] = "shunya.shunya_ads.issuer_ping";
const char kIssuers[] = "shunya.shunya_ads.issuers";

// Stores epsilon greedy bandit
const char kEpsilonGreedyBanditArms[] =
    "shunya.shunya_ads.epsilon_greedy_bandit_arms.v2";
const char kEpsilonGreedyBanditEligibleSegments[] =
    "shunya.shunya_ads.epsilon_greedy_bandit_eligible_segments.v2";

// Rewards
const char kNextTokenRedemptionAt[] =
    "shunya.shunya_ads.rewards.next_time_redemption_at";

// Stores migration status
const char kHasMigratedClientState[] =
    "shunya.shunya_ads.state.has_migrated.client.v5";
const char kHasMigratedConfirmationState[] =
    "shunya.shunya_ads.state.has_migrated.confirmations.v5";
const char kHasMigratedConversionState[] =
    "shunya.shunya_ads.migrated.conversion_state";
const char kHasMigratedNotificationState[] =
    "shunya.shunya_ads.has_migrated.notification_state";
const char kHasMigratedRewardsState[] =
    "shunya.shunya_ads.migrated.rewards_state";
const char kShouldMigrateVerifiedRewardsUser[] =
    "shunya.shunya_ads.rewards.verified_user.should_migrate";

}  // namespace shunya_ads::prefs

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_CREATIVE_ADS_DATABASE_TABLE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_CREATIVE_ADS_DATABASE_TABLE_H_

#include <string>

#include "base/functional/callback_forward.h"
#include "shunya/components/shunya_ads/core/internal/creatives/creative_ad_info.h"
#include "shunya/components/shunya_ads/core/internal/database/database_table_interface.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-forward.h"
#include "shunya/components/shunya_ads/core/public/client/ads_client_callback.h"

namespace shunya_ads::database::table {

using GetCreativeAdCallback =
    base::OnceCallback<void(bool success,
                            const std::string& creative_instance_id,
                            const CreativeAdInfo& creative_ad)>;

class CreativeAds final : public TableInterface {
 public:
  void InsertOrUpdate(mojom::DBTransactionInfo* transaction,
                      const CreativeAdList& creative_ads);

  void Delete(ResultCallback callback) const;

  void GetForCreativeInstanceId(const std::string& creative_instance_id,
                                GetCreativeAdCallback callback) const;

  std::string GetTableName() const override;

  void Create(mojom::DBTransactionInfo* transaction) override;
  void Migrate(mojom::DBTransactionInfo* transaction, int to_version) override;

 private:
  std::string BuildInsertOrUpdateSql(mojom::DBCommandInfo* command,
                                     const CreativeAdList& creative_ads) const;
};

}  // namespace shunya_ads::database::table

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_CREATIVE_ADS_DATABASE_TABLE_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_PROMOTED_CONTENT_ADS_CREATIVE_PROMOTED_CONTENT_AD_UNITTEST_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_PROMOTED_CONTENT_ADS_CREATIVE_PROMOTED_CONTENT_AD_UNITTEST_UTIL_H_

#include "shunya/components/shunya_ads/core/internal/creatives/promoted_content_ads/creative_promoted_content_ad_info.h"

namespace shunya_ads {

CreativePromotedContentAdList BuildCreativePromotedContentAdsForTesting(
    int count);
CreativePromotedContentAdInfo BuildCreativePromotedContentAdForTesting(
    bool should_use_random_uuids);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_PROMOTED_CONTENT_ADS_CREATIVE_PROMOTED_CONTENT_AD_UNITTEST_UTIL_H_

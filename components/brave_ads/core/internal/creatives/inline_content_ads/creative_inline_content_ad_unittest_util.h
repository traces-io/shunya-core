/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_INLINE_CONTENT_ADS_CREATIVE_INLINE_CONTENT_AD_UNITTEST_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_INLINE_CONTENT_ADS_CREATIVE_INLINE_CONTENT_AD_UNITTEST_UTIL_H_

#include "shunya/components/shunya_ads/core/internal/creatives/inline_content_ads/creative_inline_content_ad_info.h"

namespace shunya_ads {

CreativeInlineContentAdList BuildCreativeInlineContentAdsForTesting(int count);
CreativeInlineContentAdInfo BuildCreativeInlineContentAdForTesting(
    bool should_use_random_uuids);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CREATIVES_INLINE_CONTENT_ADS_CREATIVE_INLINE_CONTENT_AD_UNITTEST_UTIL_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/creatives/conversions/creative_set_conversion_builder.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_time_util.h"
#include "shunya/components/shunya_ads/core/internal/conversions/types/verifiable_conversion/verifiable_conversion_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/creatives/conversions/creative_set_conversion_info.h"
#include "shunya/components/shunya_ads/core/internal/creatives/search_result_ads/search_result_ad_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/units/ad_unittest_constants.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom.h"  // IWYU pragma: keep

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsCreativeSetConversionBuilderTest : public UnitTestBase {};

TEST_F(ShunyaAdsCreativeSetConversionBuilderTest, BuildCreativeSetConversion) {
  // Arrange
  const mojom::SearchResultAdInfoPtr search_result_ad =
      BuildSearchResultAdWithConversionForTesting(
          /*should_use_random_uuids*/ false);

  // Act
  const absl::optional<CreativeSetConversionInfo> creative_set_conversion =
      BuildCreativeSetConversion(search_result_ad);

  // Assert
  CreativeSetConversionInfo expected_creative_set_conversion;
  expected_creative_set_conversion.id = kCreativeSetId;
  expected_creative_set_conversion.url_pattern = "https://shunya.com/*";
  expected_creative_set_conversion.verifiable_advertiser_public_key_base64 =
      kVerifiableConversionAdvertiserPublicKey;
  expected_creative_set_conversion.observation_window = base::Days(3);
  expected_creative_set_conversion.expire_at =
      Now() + expected_creative_set_conversion.observation_window;

  EXPECT_EQ(expected_creative_set_conversion, creative_set_conversion);
}

TEST_F(ShunyaAdsCreativeSetConversionBuilderTest,
       DoNotBuildCreativeSetConversionIfAdDoesNotSupportConversions) {
  // Arrange
  const mojom::SearchResultAdInfoPtr search_result_ad =
      BuildSearchResultAdForTesting(/*should_use_random_uuids*/ true);

  // Act

  // Assert
  EXPECT_FALSE(BuildCreativeSetConversion(search_result_ad));
}

}  // namespace shunya_ads

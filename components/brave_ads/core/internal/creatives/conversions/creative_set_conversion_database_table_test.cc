/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/creatives/conversions/creative_set_conversion_database_table.h"

#include "base/functional/bind.h"
#include "shunya/components/shunya_ads/core/internal/catalog/catalog_url_request_builder_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_mock_util.h"
#include "net/http/http_status_code.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsConversionsDatabaseTableIntegrationTest : public UnitTestBase {
 protected:
  void SetUp() override {
    UnitTestBase::SetUpForTesting(/*is_integration_test*/ true);
  }

  void SetUpMocks() override {
    const URLResponseMap url_responses = {
        {BuildCatalogUrlPath(),
         {{net::HTTP_OK, /*response_body*/ "/catalog.json"}}}};
    MockUrlResponses(ads_client_mock_, url_responses);
  }
};

TEST_F(ShunyaAdsConversionsDatabaseTableIntegrationTest,
       GetConversionsFromCatalogResponse) {
  // Arrange

  // Act

  // Assert
  const database::table::CreativeSetConversions database_table;
  database_table.GetAll(base::BindOnce(
      [](const bool success,
         const CreativeSetConversionList& creative_set_conversions) {
        EXPECT_TRUE(success);
        EXPECT_EQ(2U, creative_set_conversions.size());
      }));
}

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/creatives/campaigns_database_table.h"

#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::database::table {

TEST(ShunyaAdsCampaignsDatabaseTableTest, TableName) {
  // Arrange
  const Campaigns database_table;

  // Act

  // Assert
  EXPECT_EQ("campaigns", database_table.GetTableName());
}

}  // namespace shunya_ads::database::table

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/creatives/segments_database_util.h"

#include "base/functional/bind.h"
#include "shunya/components/shunya_ads/core/internal/common/logging_util.h"
#include "shunya/components/shunya_ads/core/internal/creatives/segments_database_table.h"

namespace shunya_ads::database {

void DeleteSegments() {
  const table::Segments database_table;
  database_table.Delete(base::BindOnce([](const bool success) {
    if (!success) {
      return BLOG(0, "Failed to delete segments");
    }
  }));
}

}  // namespace shunya_ads::database

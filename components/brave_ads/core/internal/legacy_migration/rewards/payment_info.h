/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_LEGACY_MIGRATION_REWARDS_PAYMENT_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_LEGACY_MIGRATION_REWARDS_PAYMENT_INFO_H_

#include <string>
#include <vector>

namespace shunya_ads::rewards {

struct PaymentInfo final {
  double balance = 0.0;
  std::string month;
  int transaction_count = 0;
};

using PaymentList = std::vector<PaymentInfo>;

}  // namespace shunya_ads::rewards

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_LEGACY_MIGRATION_REWARDS_PAYMENT_INFO_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/legacy_migration/confirmations/legacy_confirmation_migration_util.h"

#include "shunya/components/shunya_ads/core/internal/client/ads_client_helper.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"

namespace shunya_ads {

bool HasMigratedConfirmation() {
  return AdsClientHelper::GetInstance()->GetBooleanPref(
      prefs::kHasMigratedConfirmationState);
}

}  // namespace shunya_ads

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/legacy_migration/confirmations/legacy_confirmation_migration_util.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_pref_util.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsLegacyConfirmationMigrationUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsLegacyConfirmationMigrationUtilTest, HasMigrated) {
  // Arrange
  SetBooleanPref(prefs::kHasMigratedConfirmationState, true);

  // Act

  // Assert
  EXPECT_TRUE(HasMigratedConfirmation());
}

TEST_F(ShunyaAdsLegacyConfirmationMigrationUtilTest, HasNotMigrated) {
  // Arrange
  SetBooleanPref(prefs::kHasMigratedConfirmationState, false);

  // Act

  // Assert
  EXPECT_FALSE(HasMigratedConfirmation());
}

}  // namespace shunya_ads

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/legacy_migration/client/legacy_client_migration_util.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_pref_util.h"
#include "shunya/components/shunya_ads/core/public/prefs/pref_names.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsLegacyClientMigrationUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsLegacyClientMigrationUtilTest, HasMigrated) {
  // Arrange
  SetBooleanPref(prefs::kHasMigratedClientState, true);

  // Act

  // Assert
  EXPECT_TRUE(HasMigratedClientState());
}

TEST_F(ShunyaAdsLegacyClientMigrationUtilTest, HasNotMigrated) {
  // Arrange
  SetBooleanPref(prefs::kHasMigratedClientState, false);

  // Act

  // Assert
  EXPECT_FALSE(HasMigratedClientState());
}

}  // namespace shunya_ads

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_LEGACY_MIGRATION_DATABASE_DATABASE_MIGRATION_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_LEGACY_MIGRATION_DATABASE_DATABASE_MIGRATION_H_

#include "shunya/components/shunya_ads/core/public/client/ads_client_callback.h"

namespace shunya_ads::database {

void MigrateFromVersion(int from_version, ResultCallback callback);

}  // namespace shunya_ads::database

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_LEGACY_MIGRATION_DATABASE_DATABASE_MIGRATION_H_

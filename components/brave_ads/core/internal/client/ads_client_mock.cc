/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/client/ads_client_mock.h"

namespace shunya_ads {

AdsClientMock::AdsClientMock() = default;

AdsClientMock::~AdsClientMock() = default;

}  // namespace shunya_ads

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/client/ads_client_notifier_queue.h"

#include <utility>

namespace shunya_ads {

AdsClientNotifierQueue::AdsClientNotifierQueue() = default;

AdsClientNotifierQueue::~AdsClientNotifierQueue() = default;

void AdsClientNotifierQueue::Add(base::OnceClosure notifier) {
  queue_.push(std::move(notifier));
}

void AdsClientNotifierQueue::Process() {
  while (!queue_.empty()) {
    std::move(queue_.front()).Run();
    queue_.pop();
  }
}

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CLIENT_ADS_CLIENT_HELPER_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CLIENT_ADS_CLIENT_HELPER_H_

#include "shunya/components/shunya_ads/core/public/client/ads_client.h"

namespace shunya_ads {

class AdsClientNotifierObserver;

class AdsClientHelper final {
 public:
  AdsClientHelper() = default;

  AdsClientHelper(const AdsClientHelper&) = delete;
  AdsClientHelper& operator=(const AdsClientHelper&) = delete;

  AdsClientHelper(AdsClientHelper&&) noexcept = delete;
  AdsClientHelper& operator=(AdsClientHelper&&) noexcept = delete;

  ~AdsClientHelper() = default;

  static AdsClient* GetInstance();

  static bool HasInstance();

  static void AddObserver(AdsClientNotifierObserver* observer);
  static void RemoveObserver(AdsClientNotifierObserver* observer);
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CLIENT_ADS_CLIENT_HELPER_H_

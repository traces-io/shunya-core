/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/history/category_content_util.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/units/ad_unittest_constants.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-shared.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsCategoryContentUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsCategoryContentUtilTest, Build) {
  // Arrange

  // Act
  const CategoryContentInfo category_content = BuildCategoryContent(kSegment);

  // Assert
  CategoryContentInfo expected_category_content;
  expected_category_content.category = kSegment;
  expected_category_content.user_reaction_type =
      mojom::UserReactionType::kNeutral;

  EXPECT_EQ(expected_category_content, category_content);
}

}  // namespace shunya_ads

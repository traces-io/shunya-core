/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/history/category_content_util.h"

#include "shunya/components/shunya_ads/core/internal/deprecated/client/client_state_manager.h"
#include "shunya/components/shunya_ads/core/public/history/category_content_info.h"

namespace shunya_ads {

CategoryContentInfo BuildCategoryContent(const std::string& segment) {
  CategoryContentInfo category_content;

  category_content.user_reaction_type =
      ClientStateManager::GetInstance().GetUserReactionTypeForSegment(segment);
  category_content.category = segment;

  return category_content;
}

}  // namespace shunya_ads

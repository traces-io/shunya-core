/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_FILTERS_HISTORY_FILTER_FACTORY_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_FILTERS_HISTORY_FILTER_FACTORY_H_

#include <memory>

#include "shunya/components/shunya_ads/core/internal/history/filters/history_filter_interface.h"
#include "shunya/components/shunya_ads/core/public/history/history_filter_types.h"

namespace shunya_ads {

class HistoryFilterFactory final {
 public:
  static std::unique_ptr<HistoryFilterInterface> Build(HistoryFilterType type);
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_FILTERS_HISTORY_FILTER_FACTORY_H_

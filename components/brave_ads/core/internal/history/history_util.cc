/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/history/history_util.h"

#include "shunya/components/shunya_ads/core/internal/deprecated/client/client_state_manager.h"
#include "shunya/components/shunya_ads/core/internal/history/history_item_util.h"
#include "shunya/components/shunya_ads/core/public/account/confirmations/confirmation_type.h"
#include "shunya/components/shunya_ads/core/public/history/history_item_info.h"
#include "shunya/components/shunya_ads/core/public/units/ad_info.h"

namespace shunya_ads {

HistoryItemInfo AddHistory(const AdInfo& ad,
                           const ConfirmationType& confirmation_type,
                           const std::string& title,
                           const std::string& description) {
  CHECK(ad.IsValid());

  HistoryItemInfo history_item =
      BuildHistoryItem(ad, confirmation_type, title, description);
  ClientStateManager::GetInstance().AppendHistory(history_item);
  return history_item;
}

}  // namespace shunya_ads

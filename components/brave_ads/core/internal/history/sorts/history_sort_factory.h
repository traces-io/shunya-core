/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_SORTS_HISTORY_SORT_FACTORY_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_SORTS_HISTORY_SORT_FACTORY_H_

#include <memory>

#include "shunya/components/shunya_ads/core/internal/history/sorts/history_sort_interface.h"
#include "shunya/components/shunya_ads/core/public/history/history_sort_types.h"

namespace shunya_ads {

class HistorySortFactory final {
 public:
  static std::unique_ptr<HistorySortInterface> Build(HistorySortType type);
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_SORTS_HISTORY_SORT_FACTORY_H_

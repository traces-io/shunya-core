/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_SORTS_ASCENDING_HISTORY_SORT_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_SORTS_ASCENDING_HISTORY_SORT_H_

#include "shunya/components/shunya_ads/core/internal/history/sorts/history_sort_interface.h"

namespace shunya_ads {

class AscendingHistorySort final : public HistorySortInterface {
 public:
  HistoryItemList Apply(const HistoryItemList& history) const override;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_HISTORY_SORTS_ASCENDING_HISTORY_SORT_H_

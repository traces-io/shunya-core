/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_COUNTRY_COMPONENTS_UNITTEST_CONSTANTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_COUNTRY_COMPONENTS_UNITTEST_CONSTANTS_H_

namespace shunya_ads {

constexpr char kCountryComponentManifestVersion[] = "1.0.0";
constexpr char kCountryComponentId[] = "jememeholcpjpoahinnlafoiaknnmfgl";
constexpr char kInvalidCountryComponentId[] =
    "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_COUNTRY_COMPONENTS_UNITTEST_CONSTANTS_H_

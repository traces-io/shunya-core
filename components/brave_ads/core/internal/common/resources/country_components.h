/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_COUNTRY_COMPONENTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_COUNTRY_COMPONENTS_H_

#include <string>

namespace shunya_ads {

[[nodiscard]] bool IsValidCountryComponentId(const std::string& id);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_COUNTRY_COMPONENTS_H_

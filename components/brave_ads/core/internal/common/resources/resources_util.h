/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_RESOURCES_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_RESOURCES_UTIL_H_

#include <string>

#include "base/functional/callback.h"
#include "shunya/components/shunya_ads/core/internal/common/resources/resource_parsing_error_or.h"

namespace shunya_ads {

template <typename T>
using LoadAndParseResourceCallback =
    base::OnceCallback<void(ResourceParsingErrorOr<T>)>;

template <typename T>
void LoadAndParseResource(const std::string& id,
                          int version,
                          LoadAndParseResourceCallback<T> callback);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_RESOURCES_UTIL_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_RESOURCE_PARSING_ERROR_OR_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_RESOURCE_PARSING_ERROR_OR_H_

#include <string>

#include "base/types/expected.h"

namespace shunya_ads {

// Helper for methods which perform file read operations to parse a json file,
// and initiate a given type.
template <class ValueType>
using ResourceParsingErrorOr = base::expected<ValueType, std::string>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RESOURCES_RESOURCE_PARSING_ERROR_OR_H_

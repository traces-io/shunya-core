/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_DATABASE_DATABASE_RECORD_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_DATABASE_DATABASE_RECORD_UTIL_H_

#include <vector>

#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom.h"
#include "sql/statement.h"

namespace shunya_ads::database {

mojom::DBRecordInfoPtr CreateRecord(
    sql::Statement* statement,
    const std::vector<mojom::DBCommandInfo::RecordBindingType>& bindings);

}  // namespace shunya_ads::database

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_DATABASE_DATABASE_RECORD_UTIL_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_BASE_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_BASE_UTIL_H_

#include "base/files/scoped_temp_dir.h"
#include "shunya/components/shunya_ads/core/internal/client/ads_client_mock.h"
#include "shunya/components/shunya_ads/core/public/database/database.h"

namespace shunya_ads {

void MockFlags();

void MockShowNotificationAd(AdsClientMock& mock);
void MockCloseNotificationAd(AdsClientMock& mock);

void MockCacheAdEventForInstanceId(const AdsClientMock& mock);
void MockGetCachedAdEvents(const AdsClientMock& mock);
void MockResetAdEventCacheForInstanceId(const AdsClientMock& mock);

void MockSave(AdsClientMock& mock);
void MockLoad(AdsClientMock& mock, const base::ScopedTempDir& temp_dir);
void MockLoadFileResource(AdsClientMock& mock,
                          const base::ScopedTempDir& temp_dir);
void MockLoadDataResource(AdsClientMock& mock);

void MockRunDBTransaction(AdsClientMock& mock, Database& database);

void MockGetBooleanPref(const AdsClientMock& mock);
void MockGetIntegerPref(const AdsClientMock& mock);
void MockGetDoublePref(const AdsClientMock& mock);
void MockGetStringPref(const AdsClientMock& mock);
void MockGetInt64Pref(const AdsClientMock& mock);
void MockGetUint64Pref(const AdsClientMock& mock);
void MockGetTimePref(const AdsClientMock& mock);
void MockGetDictPref(const AdsClientMock& mock);
void MockGetListPref(const AdsClientMock& mock);
void MockClearPref(AdsClientMock& mock);
void MockHasPrefPath(const AdsClientMock& mock);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_BASE_UTIL_H_

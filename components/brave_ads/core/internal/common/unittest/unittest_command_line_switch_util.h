/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_COMMAND_LINE_SWITCH_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_COMMAND_LINE_SWITCH_UTIL_H_

#include <string>

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_command_line_switch_info.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_ads {

void InitializeCommandLineSwitches();
void ShutdownCommandLineSwitches();

absl::optional<bool>& DidAppendCommandLineSwitches();
void AppendCommandLineSwitches(
    const CommandLineSwitchList& command_line_switches);

std::string SanitizeCommandLineSwitch(
    const CommandLineSwitchInfo& command_line_switch);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_COMMAND_LINE_SWITCH_UTIL_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_TIME_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_TIME_UTIL_H_

#include <string>

namespace base {
class Time;
}  // namespace base

namespace shunya_ads {

base::Time TimeFromString(const std::string& time_string, bool is_local);

// The distance between the past, present and future is only a persistent
// illusion. Albert Einstein.
base::Time DistantPast();
std::string DistantPastAsISO8601();
base::Time Now();
std::string NowAsISO8601();
base::Time DistantFuture();
std::string DistantFutureAsISO8601();

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_TIME_UTIL_H_

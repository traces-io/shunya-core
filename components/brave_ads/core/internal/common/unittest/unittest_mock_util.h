/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_MOCK_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_MOCK_UTIL_H_

#include <vector>

#include "shunya/components/shunya_ads/core/internal/client/ads_client_mock.h"
#include "shunya/components/shunya_ads/core/internal/common/platform/platform_helper_mock.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_build_channel_types.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_url_response_alias.h"

class GURL;

namespace shunya_ads {

void MockDeviceId();

void MockBuildChannel(BuildChannelType type);

void MockPlatformHelper(const PlatformHelperMock& mock, PlatformType type);

void MockIsNetworkConnectionAvailable(const AdsClientMock& mock,
                                      bool is_available);

void MockIsBrowserActive(const AdsClientMock& mock, bool is_browser_active);
void MockIsBrowserInFullScreenMode(const AdsClientMock& mock,
                                   bool is_browser_in_full_screen_mode);

void MockCanShowNotificationAds(AdsClientMock& mock, bool can_show);
void MockCanShowNotificationAdsWhileBrowserIsBackgrounded(
    const AdsClientMock& mock,
    bool can_show);

void MockGetBrowsingHistory(AdsClientMock& mock,
                            const std::vector<GURL>& history);

void MockUrlResponses(AdsClientMock& mock, const URLResponseMap& url_responses);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_UNITTEST_UNITTEST_MOCK_UTIL_H_

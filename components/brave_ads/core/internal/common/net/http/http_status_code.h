/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_NET_HTTP_HTTP_STATUS_CODE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_NET_HTTP_HTTP_STATUS_CODE_H_

namespace net {

constexpr int kHttpImATeapot = 418;
constexpr int kHttpUpgradeRequired = 426;

}  // namespace net

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_NET_HTTP_HTTP_STATUS_CODE_H_

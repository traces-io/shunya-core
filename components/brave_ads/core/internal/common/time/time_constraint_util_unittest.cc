/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/time/time_constraint_util.h"

#include "base/time/time.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_time_util.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsTimeConstraintUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsTimeConstraintUtilTest, DoesRespectWhenNoHistoory) {
  // Arrange
  const std::vector<base::Time> history;

  // Act
  const bool does_respect = DoesHistoryRespectRollingTimeConstraint(
      history, /*time_constraint*/ base::Days(1), /*cap*/ 1);

  // Assert
  EXPECT_TRUE(does_respect);
}

TEST_F(ShunyaAdsTimeConstraintUtilTest, DoesRespect) {
  // Arrange
  std::vector<base::Time> history;
  history.push_back(Now());

  AdvanceClockBy(base::Days(1));

  // Act
  const bool does_respect = DoesHistoryRespectRollingTimeConstraint(
      history, /*time_constraint*/ base::Days(1), /*cap*/ 1);

  // Assert
  EXPECT_TRUE(does_respect);
}

TEST_F(ShunyaAdsTimeConstraintUtilTest, DoesNotRespect) {
  // Arrange
  std::vector<base::Time> history;
  history.push_back(Now());

  // Act
  const bool does_respect = DoesHistoryRespectRollingTimeConstraint(
      history, /*time_constraint*/ base::Days(1), /*cap*/ 1);

  // Assert
  EXPECT_FALSE(does_respect);
}

}  // namespace shunya_ads

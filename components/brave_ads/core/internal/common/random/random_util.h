/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RANDOM_RANDOM_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RANDOM_RANDOM_UTIL_H_

namespace base {
class TimeDelta;
}  // namespace base

namespace shunya_ads {

base::TimeDelta RandTimeDelta(base::TimeDelta time_delta);

class ScopedRandTimeDeltaSetterForTesting final {
 public:
  explicit ScopedRandTimeDeltaSetterForTesting(base::TimeDelta time_delta);

  ~ScopedRandTimeDeltaSetterForTesting();
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_RANDOM_RANDOM_UTIL_H_

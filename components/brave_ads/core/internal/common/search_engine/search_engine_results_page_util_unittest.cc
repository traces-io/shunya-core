/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/search_engine/search_engine_results_page_util.h"

#include "shunya/components/shunya_ads/core/internal/common/search_engine/search_engine_results_page_unittest_constants.h"
#include "testing/gtest/include/gtest/gtest.h"
#include "url/gurl.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsSearchEngineResultsPageUtilTest, IsSearchEngineResultsPage) {
  // Arrange

  // Act

  // Assert
  for (const auto& url : GetSearchEngineResultsPageUrls()) {
    EXPECT_TRUE(IsSearchEngineResultsPage(url));
  }
}

TEST(ShunyaAdsSearchEngineResultsPageUtilTest, IsNotSearchEngineResultsPage) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(IsSearchEngineResultsPage(GURL("https://shunya.com/")));
}

TEST(ShunyaAdsSearchEngineResultsPageUtilTest,
     IsNotSearchEngineResultsPageWithInvalidUrl) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(IsSearchEngineResultsPage(GURL("INVALID")));
}

TEST(ShunyaAdsSearchEngineResultsPageUtilTest, ExtractSearchTermQueryValue) {
  // Arrange

  // Act

  // Assert
  for (const auto& url : GetSearchEngineResultsPageUrls()) {
    const absl::optional<std::string> search_term_query_value =
        ExtractSearchTermQueryValue(url);
    if (search_term_query_value) {
      EXPECT_EQ("foobar", search_term_query_value);
    }
  }
}

TEST(ShunyaAdsSearchEngineResultsPageUtilTest,
     FailToExtractSearchTermQueryValueFromUrlWithMissingQuery) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ExtractSearchTermQueryValue(GURL("https://google.com/")));
}

TEST(ShunyaAdsSearchEngineResultsPageUtilTest,
     FailToExtractSearchTermQueryValueFromInvalidUrl) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ExtractSearchTermQueryValue(GURL("INVALID")));
}

}  // namespace shunya_ads

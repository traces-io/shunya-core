/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signing_key.h"

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/blinded_token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/blinded_token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/challenge_bypass_ristretto_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/public_key.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signed_token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signed_token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/token_preimage.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/token_preimage_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/unblinded_token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/unblinded_token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::cbr {

class ShunyaAdsSigningKeyTest : public UnitTestBase {};

TEST_F(ShunyaAdsSigningKeyTest, FailToInitializeWithEmptyBase64) {
  // Arrange
  const SigningKey signing_key("");

  // Act

  // Assert
  EXPECT_FALSE(signing_key.has_value());
}

TEST_F(ShunyaAdsSigningKeyTest, FailToInitializeWithInvalidBase64) {
  // Arrange
  const SigningKey signing_key(kInvalidBase64);

  // Act

  // Assert
  EXPECT_FALSE(signing_key.has_value());
}

TEST_F(ShunyaAdsSigningKeyTest, DecodeBase64) {
  // Arrange

  // Act
  const SigningKey signing_key = SigningKey::DecodeBase64(kSigningKeyBase64);

  // Assert
  EXPECT_TRUE(signing_key.has_value());
}

TEST_F(ShunyaAdsSigningKeyTest, FailToDecodeEmptyBase64) {
  // Arrange

  // Act
  const SigningKey signing_key = SigningKey::DecodeBase64("");

  // Assert
  EXPECT_FALSE(signing_key.has_value());
}

TEST_F(ShunyaAdsSigningKeyTest, FailToDecodeInvalidBase64) {
  // Arrange

  // Act
  const SigningKey signing_key = SigningKey::DecodeBase64(kInvalidBase64);

  // Assert
  EXPECT_FALSE(signing_key.has_value());
}

TEST_F(ShunyaAdsSigningKeyTest, EncodeBase64) {
  // Arrange
  const SigningKey signing_key(kSigningKeyBase64);

  // Act

  // Assert
  EXPECT_EQ(kSigningKeyBase64, signing_key.EncodeBase64());
}

TEST_F(ShunyaAdsSigningKeyTest, Sign) {
  // Arrange
  const SigningKey signing_key(kSigningKeyBase64);

  // Act

  // Assert
  EXPECT_EQ(GetSignedTokenForTesting(),
            signing_key.Sign(GetBlindedTokenForTesting()));
}

TEST_F(ShunyaAdsSigningKeyTest, FailToSignWithInvalidBlindedToken) {
  // Arrange
  const SigningKey signing_key(kSigningKeyBase64);

  // Act

  // Assert
  EXPECT_FALSE(signing_key.Sign(GetInvalidBlindedTokenForTesting()));
}

TEST_F(ShunyaAdsSigningKeyTest, RederiveUnblindedToken) {
  // Arrange
  SigningKey signing_key(kSigningKeyBase64);

  // Act

  // Assert
  EXPECT_EQ(GetUnblindedTokenForTesting(),
            signing_key.RederiveUnblindedToken(GetTokenPreimageForTesting()));
}

TEST_F(ShunyaAdsSigningKeyTest,
       FailToRederiveUnblindedTokenWithInvalidTokenPreimage) {
  // Arrange
  SigningKey signing_key(kSigningKeyBase64);

  // Act

  // Assert
  EXPECT_FALSE(
      signing_key.RederiveUnblindedToken(GetInvalidTokenPreimageForTesting()));
}

TEST_F(ShunyaAdsSigningKeyTest, GetPublicKey) {
  // Arrange
  SigningKey signing_key(kSigningKeyBase64);

  // Act

  // Assert
  EXPECT_EQ(PublicKey(kPublicKeyBase64), signing_key.GetPublicKey());
}

TEST_F(ShunyaAdsSigningKeyTest, IsEqual) {
  // Arrange
  const SigningKey signing_key;

  // Act

  // Assert
  EXPECT_EQ(signing_key, signing_key);
}

TEST_F(ShunyaAdsSigningKeyTest, IsEmptyBase64Equal) {
  // Arrange
  const SigningKey signing_key("");

  // Act

  // Assert
  EXPECT_EQ(signing_key, signing_key);
}

TEST_F(ShunyaAdsSigningKeyTest, IsInvalidBase64Equal) {
  // Arrange
  const SigningKey signing_key(kInvalidBase64);

  // Act

  // Assert
  EXPECT_EQ(signing_key, signing_key);
}

TEST_F(ShunyaAdsSigningKeyTest, IsNotEqual) {
  // Arrange
  const SigningKey signing_key;

  // Act

  // Assert
  const SigningKey different_signing_key;
  EXPECT_NE(different_signing_key, signing_key);
}

TEST_F(ShunyaAdsSigningKeyTest, OutputStream) {
  // Arrange
  const SigningKey signing_key(kSigningKeyBase64);

  // Act
  std::stringstream ss;
  ss << signing_key;

  // Assert
  EXPECT_EQ(kSigningKeyBase64, ss.str());
}

}  // namespace shunya_ads::cbr

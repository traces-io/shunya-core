/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/token.h"

#include <sstream>

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/challenge_bypass_ristretto_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::cbr {

class ShunyaAdsTokenTest : public UnitTestBase {};

TEST_F(ShunyaAdsTokenTest, Random) {
  // Arrange
  const Token token;

  // Act

  // Assert
  EXPECT_TRUE(token.has_value());
}

TEST_F(ShunyaAdsTokenTest, FailToInitializeWithEmptyBase64) {
  // Arrange
  const Token token("");

  // Act

  // Assert
  EXPECT_FALSE(token.has_value());
}

TEST_F(ShunyaAdsTokenTest, FailToInitializeWithInvalidBase64) {
  // Arrange
  const Token token(kInvalidBase64);

  // Act

  // Assert
  EXPECT_FALSE(token.has_value());
}

TEST_F(ShunyaAdsTokenTest, DecodeBase64) {
  // Arrange

  // Act
  const Token token = Token::DecodeBase64(kTokenBase64);

  // Assert
  EXPECT_TRUE(token.has_value());
}

TEST_F(ShunyaAdsTokenTest, FailToDecodeEmptyBase64) {
  // Arrange

  // Act
  const Token token = Token::DecodeBase64("");

  // Assert
  EXPECT_FALSE(token.has_value());
}

TEST_F(ShunyaAdsTokenTest, FailToDecodeInvalidBase64) {
  // Arrange

  // Act
  const Token token = Token::DecodeBase64(kInvalidBase64);

  // Assert
  EXPECT_FALSE(token.has_value());
}

TEST_F(ShunyaAdsTokenTest, EncodeBase64) {
  // Arrange
  const Token token(kTokenBase64);

  // Act

  // Assert
  EXPECT_EQ(kTokenBase64, token.EncodeBase64());
}

TEST_F(ShunyaAdsTokenTest, EncodeRandomBase64) {
  // Arrange
  const Token token;

  // Act

  // Assert
  EXPECT_TRUE(token.EncodeBase64());
}

TEST_F(ShunyaAdsTokenTest, IsEqual) {
  // Arrange
  const Token token;

  // Act

  // Assert
  EXPECT_EQ(token, token);
}

TEST_F(ShunyaAdsTokenTest, IsEmptyBase64Equal) {
  // Arrange
  const Token token("");

  // Act

  // Assert
  EXPECT_EQ(token, token);
}

TEST_F(ShunyaAdsTokenTest, IsInvalidBase64Equal) {
  // Arrange
  const Token token(kInvalidBase64);

  // Act

  // Assert
  EXPECT_EQ(token, token);
}

TEST_F(ShunyaAdsTokenTest, IsNotEqual) {
  // Arrange
  const Token token;

  // Act

  // Assert
  const Token different_token;
  EXPECT_NE(different_token, token);
}

TEST_F(ShunyaAdsTokenTest, OutputStream) {
  // Arrange
  const Token token(kTokenBase64);

  // Act
  std::stringstream ss;
  ss << token;

  // Assert
  EXPECT_EQ(kTokenBase64, ss.str());
}

}  // namespace shunya_ads::cbr

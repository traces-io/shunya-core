/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signing_key_unittest_util.h"

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/challenge_bypass_ristretto_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signing_key.h"

namespace shunya_ads::cbr {

SigningKey GetSigningKeyForTesting() {
  return SigningKey(kSigningKeyBase64);
}

SigningKey GetInvalidSigningKeyForTesting() {
  return SigningKey(kInvalidBase64);
}

}  // namespace shunya_ads::cbr

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/batch_dleq_proof.h"

#include <sstream>

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/blinded_token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/blinded_token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/challenge_bypass_ristretto_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/public_key.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/public_key_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signed_token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signed_token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signing_key.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signing_key_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/unblinded_token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/unblinded_token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::cbr {

class ShunyaAdsBatchDLEQProofTest : public UnitTestBase {};

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToInitialize) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof;

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToInitializeWithEmptyBase64) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof("");

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToInitializeWithInvalidBase64) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(kInvalidBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToInitializeWithInvalidBlindedTokens) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(GetInvalidBlindedTokensForTesting(),
                                        GetSignedTokensForTesting(),
                                        GetSigningKeyForTesting());

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToInitializeWithInvalidSignedTokens) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(GetBlindedTokensForTesting(),
                                        GetInvalidSignedTokensForTesting(),
                                        GetSigningKeyForTesting());

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToInitializeWithInvalidSigningKey) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(GetBlindedTokensForTesting(),
                                        GetSignedTokensForTesting(),
                                        GetInvalidSigningKeyForTesting());

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, DecodeBase64) {
  // Arrange

  // Act
  const BatchDLEQProof batch_dleq_proof =
      BatchDLEQProof::DecodeBase64(kBatchDLEQProofBase64);

  // Assert
  EXPECT_TRUE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToDecodeEmptyBase64) {
  // Arrange

  // Act
  const BatchDLEQProof batch_dleq_proof = BatchDLEQProof::DecodeBase64("");

  // Assert
  EXPECT_FALSE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToDecodeInvalidBase64) {
  // Arrange

  // Act
  const BatchDLEQProof batch_dleq_proof =
      BatchDLEQProof::DecodeBase64(kInvalidBase64);

  // Assert
  EXPECT_FALSE(batch_dleq_proof.has_value());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, EncodeBase64) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_EQ(kBatchDLEQProofBase64, batch_dleq_proof.EncodeBase64());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToEncodeBase64WhenUninitialized) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof;

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.EncodeBase64());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, Verify) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_TRUE(batch_dleq_proof.Verify(GetBlindedTokensForTesting(),
                                      GetSignedTokensForTesting(),
                                      GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToVerifyWhenUninitialized) {
  // Arrange
  BatchDLEQProof batch_dleq_proof;

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.Verify(GetBlindedTokensForTesting(),
                                       GetSignedTokensForTesting(),
                                       GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToVerifyWithInvalidBlindedTokens) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.Verify(GetInvalidBlindedTokensForTesting(),
                                       GetSignedTokensForTesting(),
                                       GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToVerifyWithInvalidSignedTokens) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.Verify(GetBlindedTokensForTesting(),
                                       GetInvalidSignedTokensForTesting(),
                                       GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToVerifyWithMismatchingPublicKey) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.Verify(GetBlindedTokensForTesting(),
                                       GetSignedTokensForTesting(),
                                       GetMismatchingPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToVerifyWithInvalidPublicKey) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.Verify(GetBlindedTokensForTesting(),
                                       GetSignedTokensForTesting(),
                                       GetInvalidPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, VerifyAndUnblind) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_EQ(GetUnblindedTokensForTesting(),
            batch_dleq_proof.VerifyAndUnblind(
                GetTokensForTesting(), GetBlindedTokensForTesting(),
                GetSignedTokensForTesting(), GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToVerifyAndUnblindWhenUninitialized) {
  // Arrange
  BatchDLEQProof batch_dleq_proof;

  // Act

  // Assert
  ASSERT_FALSE(batch_dleq_proof.VerifyAndUnblind(
      GetTokensForTesting(), GetBlindedTokensForTesting(),
      GetSignedTokensForTesting(), GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToVerifyAndUnblindWithInvalidTokens) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.VerifyAndUnblind(
      GetInvalidTokensForTesting(), GetBlindedTokensForTesting(),
      GetSignedTokensForTesting(), GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest,
       FailToVerifyAndUnblindWithInvalidBlindedTokens) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.VerifyAndUnblind(
      GetTokensForTesting(), GetInvalidBlindedTokensForTesting(),
      GetSignedTokensForTesting(), GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest,
       FailToVerifyAndUnblindWithInvalidSignedTokens) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.VerifyAndUnblind(
      GetTokensForTesting(), GetBlindedTokensForTesting(),
      GetInvalidSignedTokensForTesting(), GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest,
       FailToVerifyAndUnblindWithMismatchingPublicKey) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.VerifyAndUnblind(
      GetTokensForTesting(), GetBlindedTokensForTesting(),
      GetSignedTokensForTesting(), GetMismatchingPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, FailToVerifyAndUnblindWithInvalidPublicKey) {
  // Arrange
  BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(batch_dleq_proof.VerifyAndUnblind(
      GetTokensForTesting(), GetBlindedTokensForTesting(),
      GetSignedTokensForTesting(), GetInvalidPublicKeyForTesting()));
}

TEST_F(ShunyaAdsBatchDLEQProofTest, IsEqual) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  EXPECT_EQ(batch_dleq_proof, batch_dleq_proof);
}

TEST_F(ShunyaAdsBatchDLEQProofTest, IsEqualWhenUninitialized) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof;

  // Act

  // Assert
  EXPECT_EQ(batch_dleq_proof, batch_dleq_proof);
}

TEST_F(ShunyaAdsBatchDLEQProofTest, IsEmptyBase64Equal) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof("");

  // Act

  // Assert
  EXPECT_EQ(batch_dleq_proof, batch_dleq_proof);
}

TEST_F(ShunyaAdsBatchDLEQProofTest, IsInvalidBase64Equal) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(kInvalidBase64);

  // Act

  // Assert
  EXPECT_EQ(batch_dleq_proof, batch_dleq_proof);
}

TEST_F(ShunyaAdsBatchDLEQProofTest, IsNotEqual) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act

  // Assert
  const BatchDLEQProof different_batch_dleq_proof(kInvalidBase64);
  EXPECT_NE(different_batch_dleq_proof, batch_dleq_proof);
}

TEST_F(ShunyaAdsBatchDLEQProofTest, OutputStream) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof(kBatchDLEQProofBase64);

  // Act
  std::stringstream ss;
  ss << batch_dleq_proof;

  // Assert
  EXPECT_EQ(kBatchDLEQProofBase64, ss.str());
}

TEST_F(ShunyaAdsBatchDLEQProofTest, OutputStreamWhenUninitialized) {
  // Arrange
  const BatchDLEQProof batch_dleq_proof;

  // Act
  std::stringstream ss;
  ss << batch_dleq_proof;

  // Assert
  EXPECT_TRUE(ss.str().empty());
}

}  // namespace shunya_ads::cbr

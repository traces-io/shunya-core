/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_CHALLENGE_BYPASS_RISTRETTO_BLINDED_TOKEN_UNITTEST_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_CHALLENGE_BYPASS_RISTRETTO_BLINDED_TOKEN_UNITTEST_UTIL_H_

#include <vector>

namespace shunya_ads::cbr {

class BlindedToken;

BlindedToken GetBlindedTokenForTesting();
BlindedToken GetInvalidBlindedTokenForTesting();
std::vector<BlindedToken> GetBlindedTokensForTesting();
std::vector<BlindedToken> GetInvalidBlindedTokensForTesting();

}  // namespace shunya_ads::cbr

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_COMMON_CHALLENGE_BYPASS_RISTRETTO_BLINDED_TOKEN_UNITTEST_UTIL_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/dleq_proof.h"

#include <sstream>

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/blinded_token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/blinded_token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/challenge_bypass_ristretto_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/public_key.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/public_key_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signed_token.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signed_token_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signing_key.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/signing_key_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::cbr {

class ShunyaAdsDLEQProofTest : public UnitTestBase {};

TEST_F(ShunyaAdsDLEQProofTest, FailToInitialize) {
  // Arrange
  const DLEQProof dleq_proof;

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, FailToInitializeWithEmptyBase64) {
  // Arrange
  const DLEQProof dleq_proof("");

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, FailToInitializeWithInvalidBase64) {
  // Arrange
  const DLEQProof dleq_proof(kInvalidBase64);

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, FailToInitializeWithInvalidBlindedToken) {
  // Arrange
  const DLEQProof dleq_proof(GetInvalidBlindedTokenForTesting(),
                             GetSignedTokenForTesting(),
                             GetSigningKeyForTesting());

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, FailToInitializeWithInvalidSignedToken) {
  // Arrange
  const DLEQProof dleq_proof(GetBlindedTokenForTesting(),
                             GetInvalidSignedTokenForTesting(),
                             GetSigningKeyForTesting());

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, FailToInitializeWithInvalidSigningKey) {
  // Arrange
  const DLEQProof dleq_proof(GetBlindedTokenForTesting(),
                             GetSignedTokenForTesting(),
                             GetInvalidSigningKeyForTesting());

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, DecodeBase64) {
  // Arrange

  // Act
  const DLEQProof dleq_proof = DLEQProof::DecodeBase64(kDLEQProofBase64);

  // Assert
  EXPECT_TRUE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, FailToDecodeEmptyBase64) {
  // Arrange

  // Act
  const DLEQProof dleq_proof = DLEQProof::DecodeBase64("");

  // Assert
  EXPECT_FALSE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, FailToDecodeInvalidBase64) {
  // Arrange

  // Act
  const DLEQProof dleq_proof = DLEQProof::DecodeBase64(kInvalidBase64);

  // Assert
  EXPECT_FALSE(dleq_proof.has_value());
}

TEST_F(ShunyaAdsDLEQProofTest, EncodeBase64) {
  // Arrange
  const DLEQProof dleq_proof(kDLEQProofBase64);

  // Act

  // Assert
  EXPECT_EQ(kDLEQProofBase64, dleq_proof.EncodeBase64());
}

TEST_F(ShunyaAdsDLEQProofTest, FailToEncodeBase64WhenUninitialized) {
  // Arrange
  const DLEQProof dleq_proof;

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.EncodeBase64());
}

TEST_F(ShunyaAdsDLEQProofTest, Verify) {
  // Arrange
  DLEQProof dleq_proof(kDLEQProofBase64);

  // Act

  // Assert
  EXPECT_TRUE(dleq_proof.Verify(GetBlindedTokenForTesting(),
                                GetSignedTokenForTesting(),
                                GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsDLEQProofTest, FailToVerifyWhenUninitialized) {
  // Arrange
  DLEQProof dleq_proof;

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.Verify(GetBlindedTokenForTesting(),
                                 GetSignedTokenForTesting(),
                                 GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsDLEQProofTest, FailToVerifyWithInvalidBlindedToken) {
  // Arrange
  DLEQProof dleq_proof(kDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.Verify(GetInvalidBlindedTokenForTesting(),
                                 GetSignedTokenForTesting(),
                                 GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsDLEQProofTest, FailToVerifyWithInvalidSignedToken) {
  // Arrange
  DLEQProof dleq_proof(kDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.Verify(GetBlindedTokenForTesting(),
                                 GetInvalidSignedTokenForTesting(),
                                 GetPublicKeyForTesting()));
}

TEST_F(ShunyaAdsDLEQProofTest, FailToVerifyWithMismatchingPublicKey) {
  // Arrange
  DLEQProof dleq_proof(kDLEQProofBase64);

  // Act

  // AssertGetMismatchingPublicKeyForTesting
  EXPECT_FALSE(dleq_proof.Verify(GetBlindedTokenForTesting(),
                                 GetSignedTokenForTesting(),
                                 GetMismatchingPublicKeyForTesting()));
}

TEST_F(ShunyaAdsDLEQProofTest, FailToVerifyWithInvalidPublicKey) {
  // Arrange
  DLEQProof dleq_proof(kDLEQProofBase64);

  // Act

  // Assert
  EXPECT_FALSE(dleq_proof.Verify(GetBlindedTokenForTesting(),
                                 GetSignedTokenForTesting(),
                                 GetInvalidPublicKeyForTesting()));
}

TEST_F(ShunyaAdsDLEQProofTest, IsEqual) {
  // Arrange
  const DLEQProof dleq_proof(kDLEQProofBase64);

  // Act

  // Assert
  EXPECT_EQ(dleq_proof, dleq_proof);
}

TEST_F(ShunyaAdsDLEQProofTest, IsEqualWhenUninitialized) {
  // Arrange
  const DLEQProof dleq_proof;

  // Act

  // Assert
  EXPECT_EQ(dleq_proof, dleq_proof);
}

TEST_F(ShunyaAdsDLEQProofTest, IsEmptyBase64Equal) {
  // Arrange
  const DLEQProof dleq_proof("");

  // Act

  // Assert
  EXPECT_EQ(dleq_proof, dleq_proof);
}

TEST_F(ShunyaAdsDLEQProofTest, IsInvalidBase64Equal) {
  // Arrange
  const DLEQProof dleq_proof(kInvalidBase64);

  // Act

  // Assert
  EXPECT_EQ(dleq_proof, dleq_proof);
}

TEST_F(ShunyaAdsDLEQProofTest, IsNotEqual) {
  // Arrange
  const DLEQProof dleq_proof(kDLEQProofBase64);

  // Act

  // Assert
  const DLEQProof different_dleq_proof(kInvalidBase64);
  EXPECT_NE(different_dleq_proof, dleq_proof);
}

TEST_F(ShunyaAdsDLEQProofTest, OutputStream) {
  // Arrange
  const DLEQProof dleq_proof(kDLEQProofBase64);

  // Act
  std::stringstream ss;
  ss << dleq_proof;

  // Assert
  EXPECT_EQ(kDLEQProofBase64, ss.str());
}

TEST_F(ShunyaAdsDLEQProofTest, OutputStreamWhenUninitialized) {
  // Arrange
  const DLEQProof dleq_proof;

  // Act
  std::stringstream ss;
  ss << dleq_proof;

  // Assert
  EXPECT_TRUE(ss.str().empty());
}

}  // namespace shunya_ads::cbr

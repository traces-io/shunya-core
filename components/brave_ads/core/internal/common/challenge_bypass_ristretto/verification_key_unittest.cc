/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/verification_key.h"

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/verification_key_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/verification_signature.h"
#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/verification_signature_unittest_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::cbr {

namespace {
constexpr char kMessage[] = "The quick brown fox jumps over the lazy dog";
}  // namespace

class ShunyaAdsVerificationKeyTest : public UnitTestBase {};

TEST_F(ShunyaAdsVerificationKeyTest, Sign) {
  // Arrange
  VerificationKey verification_key = GetVerificationKeyForTesting();

  // Act

  // Assert
  EXPECT_EQ(GetVerificationSignatureForTesting(),
            verification_key.Sign(kMessage));
}

TEST_F(ShunyaAdsVerificationKeyTest, Verify) {
  // Arrange
  VerificationKey verification_key = GetVerificationKeyForTesting();

  // Act
  // Assert
  EXPECT_TRUE(
      verification_key.Verify(GetVerificationSignatureForTesting(), kMessage));
}

TEST_F(ShunyaAdsVerificationKeyTest,
       FailToVerifyWithInvalidVerificationSignature) {
  // Arrange
  VerificationKey verification_key = GetVerificationKeyForTesting();

  // Act

  // Assert
  EXPECT_FALSE(verification_key.Verify(
      GetInvalidVerificationSignatureForTesting(), kMessage));
}

}  // namespace shunya_ads::cbr

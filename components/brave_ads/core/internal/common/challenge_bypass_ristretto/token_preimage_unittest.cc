/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/token_preimage.h"

#include <sstream>

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/challenge_bypass_ristretto_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::cbr {

class ShunyaAdsTokenPreimageTest : public UnitTestBase {};

TEST_F(ShunyaAdsTokenPreimageTest, FailToInitialize) {
  // Arrange
  const TokenPreimage token_preimage;

  // Act

  // Assert
  EXPECT_FALSE(token_preimage.has_value());
}

TEST_F(ShunyaAdsTokenPreimageTest, FailToInitializeWithEmptyBase64) {
  // Arrange
  const TokenPreimage token_preimage("");

  // Act

  // Assert
  EXPECT_FALSE(token_preimage.has_value());
}

TEST_F(ShunyaAdsTokenPreimageTest, FailToInitializeWithInvalidBase64) {
  // Arrange
  const TokenPreimage token_preimage(kInvalidBase64);

  // Act

  // Assert
  EXPECT_FALSE(token_preimage.has_value());
}

TEST_F(ShunyaAdsTokenPreimageTest, DecodeBase64) {
  // Arrange

  // Act
  const TokenPreimage token_preimage =
      TokenPreimage::DecodeBase64(kTokenPreimageBase64);

  // Assert
  EXPECT_TRUE(token_preimage.has_value());
}

TEST_F(ShunyaAdsTokenPreimageTest, FailToDecodeEmptyBase64) {
  // Arrange

  // Act
  const TokenPreimage token_preimage = TokenPreimage::DecodeBase64("");

  // Assert
  EXPECT_FALSE(token_preimage.has_value());
}

TEST_F(ShunyaAdsTokenPreimageTest, FailToDecodeInvalidBase64) {
  // Arrange

  // Act
  const TokenPreimage token_preimage =
      TokenPreimage::DecodeBase64(kInvalidBase64);

  // Assert
  EXPECT_FALSE(token_preimage.has_value());
}

TEST_F(ShunyaAdsTokenPreimageTest, EncodeBase64) {
  // Arrange
  const TokenPreimage token_preimage(kTokenPreimageBase64);

  // Act

  // Assert
  EXPECT_EQ(kTokenPreimageBase64, token_preimage.EncodeBase64());
}

TEST_F(ShunyaAdsTokenPreimageTest, FailToEncodeBase64WhenUninitialized) {
  // Arrange
  const TokenPreimage token_preimage;

  // Act

  // Assert
  EXPECT_FALSE(token_preimage.EncodeBase64());
}

TEST_F(ShunyaAdsTokenPreimageTest, IsEqual) {
  // Arrange
  const TokenPreimage token_preimage(kTokenPreimageBase64);

  // Act

  // Assert
  EXPECT_EQ(token_preimage, token_preimage);
}

TEST_F(ShunyaAdsTokenPreimageTest, IsEqualWhenUninitialized) {
  // Arrange
  const TokenPreimage token_preimage;

  // Act

  // Assert
  EXPECT_EQ(token_preimage, token_preimage);
}

TEST_F(ShunyaAdsTokenPreimageTest, IsEmptyBase64Equal) {
  // Arrange
  const TokenPreimage token_preimage("");

  // Act

  // Assert
  EXPECT_EQ(token_preimage, token_preimage);
}

TEST_F(ShunyaAdsTokenPreimageTest, IsInvalidBase64Equal) {
  // Arrange
  const TokenPreimage token_preimage(kInvalidBase64);

  // Act

  // Assert
  EXPECT_EQ(token_preimage, token_preimage);
}

TEST_F(ShunyaAdsTokenPreimageTest, IsNotEqual) {
  // Arrange
  const TokenPreimage token_preimage(kTokenPreimageBase64);

  // Act

  // Assert
  const TokenPreimage different_token_preimage(kInvalidBase64);
  EXPECT_NE(different_token_preimage, token_preimage);
}

TEST_F(ShunyaAdsTokenPreimageTest, OutputStream) {
  // Arrange
  const TokenPreimage token_preimage(kTokenPreimageBase64);

  // Act
  std::stringstream ss;
  ss << token_preimage;

  // Assert
  EXPECT_EQ(kTokenPreimageBase64, ss.str());
}

TEST_F(ShunyaAdsTokenPreimageTest, OutputStreamWhenUninitialized) {
  // Arrange
  const TokenPreimage token_preimage;

  // Act
  std::stringstream ss;
  ss << token_preimage;

  // Assert
  EXPECT_TRUE(ss.str().empty());
}

}  // namespace shunya_ads::cbr

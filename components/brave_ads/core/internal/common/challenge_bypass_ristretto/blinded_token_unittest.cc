/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/blinded_token.h"

#include <sstream>

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/challenge_bypass_ristretto_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "testing/gtest/include/gtest/gtest.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::cbr {

class ShunyaAdsBlindedTokenTest : public UnitTestBase {};

TEST_F(ShunyaAdsBlindedTokenTest, FailToInitialize) {
  // Arrange
  const BlindedToken blinded_token;

  // Act

  // Assert
  EXPECT_FALSE(blinded_token.has_value());
}

TEST_F(ShunyaAdsBlindedTokenTest, FailToInitializeWithEmptyBase64) {
  // Arrange
  const BlindedToken blinded_token("");

  // Act

  // Assert
  EXPECT_FALSE(blinded_token.has_value());
}

TEST_F(ShunyaAdsBlindedTokenTest, FailToInitializeWithInvalidBase64) {
  // Arrange
  const BlindedToken blinded_token(kInvalidBase64);

  // Act

  // Assert
  EXPECT_FALSE(blinded_token.has_value());
}

TEST_F(ShunyaAdsBlindedTokenTest, DecodeBase64) {
  // Arrange

  // Act
  const BlindedToken blinded_token =
      BlindedToken::DecodeBase64(kBlindedTokenBase64);

  // Assert
  EXPECT_TRUE(blinded_token.has_value());
}

TEST_F(ShunyaAdsBlindedTokenTest, FailToDecodeEmptyBase64) {
  // Arrange

  // Act
  const BlindedToken blinded_token = BlindedToken::DecodeBase64("");

  // Assert
  EXPECT_FALSE(blinded_token.has_value());
}

TEST_F(ShunyaAdsBlindedTokenTest, FailToDecodeInvalidBase64) {
  // Arrange

  // Act
  const BlindedToken blinded_token = BlindedToken::DecodeBase64(kInvalidBase64);

  // Assert
  EXPECT_FALSE(blinded_token.has_value());
}

TEST_F(ShunyaAdsBlindedTokenTest, EncodeBase64) {
  // Arrange
  const BlindedToken blinded_token(kBlindedTokenBase64);

  // Act

  // Assert
  EXPECT_EQ(kBlindedTokenBase64, blinded_token.EncodeBase64());
}

TEST_F(ShunyaAdsBlindedTokenTest, FailToEncodeBase64WhenUninitialized) {
  // Arrange
  const BlindedToken blinded_token;

  // Act

  // Assert
  EXPECT_FALSE(blinded_token.EncodeBase64());
}

TEST_F(ShunyaAdsBlindedTokenTest, IsEqual) {
  // Arrange
  const BlindedToken blinded_token(kBlindedTokenBase64);

  // Act

  // Assert
  EXPECT_EQ(blinded_token, blinded_token);
}

TEST_F(ShunyaAdsBlindedTokenTest, IsEqualWhenUninitialized) {
  // Arrange
  const BlindedToken blinded_token;

  // Act

  // Assert
  EXPECT_EQ(blinded_token, blinded_token);
}

TEST_F(ShunyaAdsBlindedTokenTest, IsEmptyBase64Equal) {
  // Arrange
  const BlindedToken blinded_token("");

  // Act

  // Assert
  EXPECT_EQ(blinded_token, blinded_token);
}

TEST_F(ShunyaAdsBlindedTokenTest, IsInvalidBase64Equal) {
  // Arrange
  const BlindedToken blinded_token(kInvalidBase64);

  // Act

  // Assert
  EXPECT_EQ(blinded_token, blinded_token);
}

TEST_F(ShunyaAdsBlindedTokenTest, IsNotEqual) {
  // Arrange

  // Act
  const BlindedToken blinded_token(kBlindedTokenBase64);
  const BlindedToken different_blinded_token(kInvalidBase64);

  // Assert
  EXPECT_NE(different_blinded_token, blinded_token);
}

TEST_F(ShunyaAdsBlindedTokenTest, OutputStream) {
  // Arrange
  const BlindedToken blinded_token(kBlindedTokenBase64);

  // Act
  std::stringstream ss;
  ss << blinded_token;

  // Assert
  EXPECT_EQ(kBlindedTokenBase64, ss.str());
}

TEST_F(ShunyaAdsBlindedTokenTest, OutputStreamWhenUninitialized) {
  // Arrange
  const BlindedToken blinded_token;

  // Act
  std::stringstream ss;
  ss << blinded_token;

  // Assert
  EXPECT_TRUE(ss.str().empty());
}

}  // namespace shunya_ads::cbr

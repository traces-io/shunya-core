/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/public_key.h"

#include <sstream>

#include "shunya/components/shunya_ads/core/internal/common/challenge_bypass_ristretto/challenge_bypass_ristretto_unittest_constants.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::cbr {

class ShunyaAdsPublicKeyTest : public UnitTestBase {};

TEST_F(ShunyaAdsPublicKeyTest, FailToInitialize) {
  // Arrange
  const PublicKey public_key;

  // Act

  // Assert
  EXPECT_FALSE(public_key.has_value());
}

TEST_F(ShunyaAdsPublicKeyTest, FailToInitializeWithEmptyBase64) {
  // Arrange
  const PublicKey public_key("");

  // Act

  // Assert
  EXPECT_FALSE(public_key.has_value());
}

TEST_F(ShunyaAdsPublicKeyTest, FailToInitializeWithInvalidBase64) {
  // Arrange
  const PublicKey public_key(kInvalidBase64);

  // Act

  // Assert
  EXPECT_FALSE(public_key.has_value());
}

TEST_F(ShunyaAdsPublicKeyTest, DecodeBase64) {
  // Arrange

  // Act
  const PublicKey public_key = PublicKey::DecodeBase64(kPublicKeyBase64);

  // Assert
  EXPECT_TRUE(public_key.has_value());
}

TEST_F(ShunyaAdsPublicKeyTest, FailToDecodeEmptyBase64) {
  // Arrange

  // Act
  const PublicKey public_key = PublicKey::DecodeBase64("");

  // Assert
  EXPECT_FALSE(public_key.has_value());
}

TEST_F(ShunyaAdsPublicKeyTest, FailToDecodeInvalidBase64) {
  // Arrange

  // Act
  const PublicKey public_key = PublicKey::DecodeBase64(kInvalidBase64);

  // Assert
  EXPECT_FALSE(public_key.has_value());
}

TEST_F(ShunyaAdsPublicKeyTest, EncodeBase64) {
  // Arrange
  const PublicKey public_key(kPublicKeyBase64);

  // Act

  // Assert
  EXPECT_EQ(kPublicKeyBase64, public_key.EncodeBase64());
}

TEST_F(ShunyaAdsPublicKeyTest, FailToEncodeBase64WhenUninitialized) {
  // Arrange
  const PublicKey public_key;

  // Act

  // Assert
  EXPECT_FALSE(public_key.EncodeBase64());
}

TEST_F(ShunyaAdsPublicKeyTest, IsEqual) {
  // Arrange
  const PublicKey public_key(kPublicKeyBase64);

  // Act

  // Assert
  EXPECT_EQ(public_key, public_key);
}

TEST_F(ShunyaAdsPublicKeyTest, IsEqualWhenUninitialized) {
  // Arrange
  const PublicKey public_key;

  // Act

  // Assert
  EXPECT_EQ(public_key, public_key);
}

TEST_F(ShunyaAdsPublicKeyTest, IsEmptyBase64Equal) {
  // Arrange
  const PublicKey public_key("");

  // Act

  // Assert
  EXPECT_EQ(public_key, public_key);
}

TEST_F(ShunyaAdsPublicKeyTest, IsInvalidBase64Equal) {
  // Arrange
  const PublicKey public_key(kInvalidBase64);

  // Act

  // Assert
  EXPECT_EQ(public_key, public_key);
}

TEST_F(ShunyaAdsPublicKeyTest, IsNotEqual) {
  // Arrange
  const PublicKey public_key(kPublicKeyBase64);

  // Act

  // Assert
  const PublicKey different_public_key(kInvalidBase64);
  EXPECT_NE(different_public_key, public_key);
}

TEST_F(ShunyaAdsPublicKeyTest, OutputStream) {
  // Arrange
  const PublicKey public_key(kPublicKeyBase64);

  // Act
  std::stringstream ss;
  ss << public_key;

  // Assert
  EXPECT_EQ(kPublicKeyBase64, ss.str());
}

TEST_F(ShunyaAdsPublicKeyTest, OutputStreamWhenUninitialized) {
  // Arrange
  const PublicKey public_key;

  // Act
  std::stringstream ss;
  ss << public_key;

  // Assert
  EXPECT_TRUE(ss.str().empty());
}

}  // namespace shunya_ads::cbr

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/locale/locale_util.h"

#include "shunya/components/l10n/common/locale_util.h"

namespace shunya_ads {

const std::string& GetLocale() {
  return shunya_l10n::GetDefaultLocaleString();
}

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/locale/country_code_anonymity_util.h"

#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::locale {

TEST(ShunyaAdsCountryCodeUtilTest, IsCountryCodeMemberOfAnonymitySet) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(IsCountryCodeMemberOfAnonymitySet("US"));
}

TEST(ShunyaAdsCountryCodeUtilTest, IsCountryCodeNotMemberOfAnonymitySet) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(IsCountryCodeMemberOfAnonymitySet("XX"));
}

TEST(ShunyaAdsCountryCodeUtilTest, ShouldClassifyCountryCodeAsOther) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(ShouldClassifyCountryCodeAsOther("CX"));
}

TEST(ShunyaAdsCountryCodeUtilTest, ShouldNotClassifyCountryCodeAsOther) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(ShouldClassifyCountryCodeAsOther("XX"));
}

}  // namespace shunya_ads::locale

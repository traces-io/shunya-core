/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/locale/locale_util.h"

#include "shunya/components/l10n/common/test/scoped_default_locale.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsLocaleUtilTest, GetLocale) {
  // Arrange
  shunya_l10n::test::ScopedDefaultLocale default_locale("en_KY");

  // Act

  // Assert
  EXPECT_EQ("en_KY", GetLocale());
}

}  // namespace shunya_ads

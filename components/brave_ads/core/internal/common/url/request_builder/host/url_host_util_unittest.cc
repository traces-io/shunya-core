/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/url/request_builder/host/url_host_util.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/global_state/global_state.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsUrlHostUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsUrlHostUtilTest, GetStaticUrlHost) {
  // Arrange
  GlobalState::GetInstance()->Flags().environment_type =
      mojom::EnvironmentType::kProduction;

  // Act

  // Assert
  EXPECT_EQ("https://static.ads.shunya.com", GetStaticUrlHost());
}

TEST_F(ShunyaAdsUrlHostUtilTest, GetGeoUrlHost) {
  // Arrange
  GlobalState::GetInstance()->Flags().environment_type =
      mojom::EnvironmentType::kProduction;

  // Act

  // Assert
  EXPECT_EQ("https://geo.ads.shunya.com", GetGeoUrlHost());
}

TEST_F(ShunyaAdsUrlHostUtilTest, GetNonAnonymousUrlHost) {
  // Arrange
  GlobalState::GetInstance()->Flags().environment_type =
      mojom::EnvironmentType::kProduction;

  // Act

  // Assert
  EXPECT_EQ("https://mywallet.ads.shunya.com", GetNonAnonymousUrlHost());
}

TEST_F(ShunyaAdsUrlHostUtilTest, GetAnonymousUrlHost) {
  // Arrange
  GlobalState::GetInstance()->Flags().environment_type =
      mojom::EnvironmentType::kProduction;

  // Act

  // Assert
  EXPECT_EQ("https://anonymous.ads.shunya.com", GetAnonymousUrlHost());
}

TEST_F(ShunyaAdsUrlHostUtilTest, GetAnonymousSearchUrlHost) {
  // Arrange
  GlobalState::GetInstance()->Flags().environment_type =
      mojom::EnvironmentType::kProduction;

  // Act

  // Assert
  EXPECT_EQ("https://search.anonymous.ads.shunya.com",
            GetAnonymousSearchUrlHost());
}

}  // namespace shunya_ads

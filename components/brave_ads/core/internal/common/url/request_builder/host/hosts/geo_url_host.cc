/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/url/request_builder/host/hosts/geo_url_host.h"

#include "base/check.h"
#include "shunya/components/shunya_ads/core/internal/global_state/global_state.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom.h"

namespace shunya_ads {

namespace {

constexpr char kProductionHost[] = "https://geo.ads.shunya.com";
constexpr char kStagingHost[] = "https://geo.ads.shunyasoftware.com";

}  // namespace

std::string GeoUrlHost::Get() const {
  const mojom::EnvironmentType environment_type =
      GlobalState::GetInstance()->Flags().environment_type;
  CHECK(mojom::IsKnownEnumValue(environment_type));

  switch (environment_type) {
    case mojom::EnvironmentType::kProduction: {
      return kProductionHost;
    }

    case mojom::EnvironmentType::kStaging: {
      return kStagingHost;
    }
  }
}

}  // namespace shunya_ads

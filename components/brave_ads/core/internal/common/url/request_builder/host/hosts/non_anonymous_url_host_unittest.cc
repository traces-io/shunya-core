/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/url/request_builder/host/url_host_util.h"
#include "shunya/components/shunya_ads/core/internal/global_state/global_state.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

class ShunyaAdsNonAnonymousUrlHostTest : public UnitTestBase {};

TEST_F(ShunyaAdsNonAnonymousUrlHostTest, GetProductionUrlHost) {
  // Arrange
  GlobalState::GetInstance()->Flags().environment_type =
      mojom::EnvironmentType::kProduction;

  // Act

  // Assert
  EXPECT_EQ("https://mywallet.ads.shunya.com", GetNonAnonymousUrlHost());
}

TEST_F(ShunyaAdsNonAnonymousUrlHostTest, GetStagingUrlHost) {
  // Arrange
  GlobalState::GetInstance()->Flags().environment_type =
      mojom::EnvironmentType::kStaging;

  // Act

  // Assert
  EXPECT_EQ("https://mywallet.ads.shunyasoftware.com", GetNonAnonymousUrlHost());
}

}  // namespace shunya_ads

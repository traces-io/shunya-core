/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/url/url_util.h"

#include "testing/gtest/include/gtest/gtest.h"
#include "url/gurl.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsUrlUtilTest, GetUrlWithEmptyQuery) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(GURL("https://foo.com/bar"),
            GetUrlWithEmptyQuery(GURL("https://foo.com/bar?baz=qux")));
}

TEST(ShunyaAdsUrlUtilTest, DoesNotSupportInvalidUrl) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("INVALID")));
}

TEST(ShunyaAdsUrlUtilTest, DoesSupportUrlWithHttpsScheme) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("https://foobar.com")));
}

TEST(ShunyaAdsUrlUtilTest, DoesNotSupportUrlWithHttpScheme) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("http://foobar.com")));
}

TEST(ShunyaAdsUrlUtilTest, DoesNotSupportUrlWithFooBarScheme) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("foobar://baz")));
}

TEST(ShunyaAdsUrlUtilTest, DoesNotSupportShunyaSchemeWithFooBarHostName) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("shunya://foobar")));
}

TEST(ShunyaAdsUrlUtilTest, DoesSupportShunyaSchemeWithWalletHostName) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://wallet")));
}

TEST(ShunyaAdsUrlUtilTest, DoesSupportShunyaSchemeWithWalletHostNameAndPath) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://wallet/foo")));
}

TEST(ShunyaAdsUrlUtilTest, DoesSupportShunyaSchemeWithSyncHostName) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://sync")));
}

TEST(ShunyaAdsUrlUtilTest, DoesSupportShunyaSchemeWithSyncHostNameAndPath) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://sync/foo")));
}

TEST(ShunyaAdsUrlUtilTest, DoesSupportShunyaSchemeWithRewardsHostName) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://rewards")));
}

TEST(ShunyaAdsUrlUtilTest, DoesSupportShunyaSchemeWithRewardsHostNameAndPath) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://rewards/foo")));
}

TEST(ShunyaAdsUrlUtilTest, DoesNotSupportShunyaSchemeWithSettingsHostName) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("shunya://settings")));
}

TEST(ShunyaAdsUrlUtilTest,
     DoesNotSupportShunyaSchemeWithSettingsHostNameAndFooBarPath) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("shunya://settings/foobar")));
}

TEST(ShunyaAdsUrlUtilTest,
     DoesSupportShunyaSchemeWithSettingsHostNameAndSearchEnginesPath) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://settings/searchEngines")));
}

TEST(
    ShunyaAdsUrlUtilTest,
    DoesSupportShunyaSchemeWithSettingsHostNameSearchEnginesPathAndSearchQuery) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(
      DoesSupportUrl(GURL("shunya://settings/searchEngines?search=foobar")));
}

TEST(
    ShunyaAdsUrlUtilTest,
    DoesNotSupportShunyaSchemeWithSettingsHostNameSearchEnginesPathAndMultipleSearchQueries) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(
      GURL("shunya://settings/searchEngines?search=foo&bar=baz")));
}

TEST(
    ShunyaAdsUrlUtilTest,
    DoesNotSupportShunyaSchemeWithSettingsHostNameSearchEnginesPathAndInvalidQuery) {
  // Arrange

  // Act
  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("shunya://settings/searchEngines?search")));
}

TEST(ShunyaAdsUrlUtilTest,
     DoesSupportShunyaSchemeWithSettingsHostNameAndSearchPath) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://settings/search")));
}

TEST(ShunyaAdsUrlUtilTest,
     DoesSupportShunyaSchemeWithSettingsHostNameSearchPathAndSearchQuery) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(DoesSupportUrl(GURL("shunya://settings/search?search=foobar")));
}

TEST(
    ShunyaAdsUrlUtilTest,
    DoesNotSupportShunyaSchemeWithSettingsHostNameSearchPathAndMultipleSearchQueries) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(
      DoesSupportUrl(GURL("shunya://settings/search?search=foo&bar=baz")));
}

TEST(ShunyaAdsUrlUtilTest,
     DoesNotSupportShunyaSchemeWithSettingsHostNameSearchPathAndInvalidQuery) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("shunya://settings/search?search")));
}

TEST(ShunyaAdsUrlUtilTest,
     DoesNotSupportShunyaSchemeWithSettingsHostNameAndQuery) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("shunya://settings/?search=foobar")));
}

TEST(ShunyaAdsUrlUtilTest,
     DoesNotSupportShunyaSchemeWithSettingsHostNameAndInvalidQuery) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("shunya://settings/?search")));
}

TEST(ShunyaAdsUrlUtilTest, MalformedUrlIsNotSupported) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(DoesSupportUrl(GURL("http://foobar.com/shunya://wallet")));
}

TEST(ShunyaAdsUrlUtilTest, UrlMatchesPatternWithNoWildcards) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(MatchUrlPattern(GURL("https://www.foo.com/"),
                              /*pattern*/ "https://www.foo.com/"));
}

TEST(ShunyaAdsUrlUtilTest, UrlWithPathMatchesPatternWithNoWildcards) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(MatchUrlPattern(GURL("https://www.foo.com/bar"),
                              /*pattern*/ "https://www.foo.com/bar"));
}

TEST(ShunyaAdsUrlUtilTest, UrlDoesNotMatchPattern) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(
      MatchUrlPattern(GURL("https://www.foo.com/"), /*pattern*/ "www.foo.com"));
}

TEST(ShunyaAdsUrlUtilTest, UrlDoesNotMatchPatternWithMissingEmptyPath) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(MatchUrlPattern(GURL("https://www.foo.com/"),
                               /*pattern*/ "https://www.foo.com"));
}

TEST(ShunyaAdsUrlUtilTest, UrlMatchesEndWildcardPattern) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(MatchUrlPattern(GURL("https://www.foo.com/bar?key=test"),
                              /*pattern*/ "https://www.foo.com/bar*"));
}

TEST(ShunyaAdsUrlUtilTest, UrlMatchesMidWildcardPattern) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(MatchUrlPattern(GURL("https://www.foo.com/woo-bar-hoo"),
                              /*pattern*/ "https://www.foo.com/woo*hoo"));
}

TEST(ShunyaAdsUrlUtilTest, UrlDoesNotMatchMidWildcardPattern) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(MatchUrlPattern(GURL("https://www.foo.com/woo"),
                               /*pattern*/ "https://www.foo.com/woo*hoo"));
}

TEST(ShunyaAdsUrlUtilTest, SameDomainOrHost) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(SameDomainOrHost(GURL("https://foo.com?bar=test"),
                               GURL("https://subdomain.foo.com/bar")));
}

TEST(ShunyaAdsUrlUtilTest, NotSameDomainOrHost) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(SameDomainOrHost(GURL("https://foo.com?bar=test"),
                                GURL("https://subdomain.bar.com/foo")));
}

TEST(ShunyaAdsUrlUtilTest, SameDomainOrHostForUrlWithNoSubdomain) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(SameDomainOrHost(GURL("https://foo.com?bar=test"),
                               GURL("https://foo.com/bar")));
}

TEST(ShunyaAdsUrlUtilTest, NotSameDomainOrHostForUrlWithNoSubdomain) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(SameDomainOrHost(GURL("https://foo.com?bar=test"),
                                GURL("https://bar.com/foo")));
}

TEST(ShunyaAdsUrlUtilTest, SameDomainOrHostForUrlWithRef) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(SameDomainOrHost(GURL("https://foo.com?bar=test#ref"),
                               GURL("https://foo.com/bar")));
}

TEST(ShunyaAdsUrlUtilTest, NotSameDomainOrHostForUrlWithRef) {
  // Arrange

  // Act

  // Assert
  EXPECT_FALSE(SameDomainOrHost(GURL("https://foo.com?bar=test#ref"),
                                GURL("https://bar.com/foo")));
}

TEST(ShunyaAdsUrlUtilTest, DomainOrHostExists) {
  // Arrange
  const std::vector<GURL> urls = {GURL("https://foo.com"),
                                  GURL("https://bar.com")};

  // Act

  // Assert
  EXPECT_TRUE(DomainOrHostExists(urls, GURL("https://bar.com/foo")));
}

TEST(ShunyaAdsUrlUtilTest, DomainOrHostDoesNotExist) {
  // Arrange
  const std::vector<GURL> urls = {GURL("https://foo.com"),
                                  GURL("https://bar.com")};

  // Act

  // Assert
  EXPECT_FALSE(DomainOrHostExists(urls, GURL("https://baz.com/qux")));
}

}  // namespace shunya_ads

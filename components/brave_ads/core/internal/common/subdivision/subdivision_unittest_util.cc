/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/subdivision/subdivision_unittest_util.h"

#include "base/strings/string_util.h"

namespace shunya_ads {

std::string BuildSubdivisionForTesting(const std::string& country_code,
                                       const std::string& subdivision_code) {
  return base::ReplaceStringPlaceholders(
      "$1-$2", {country_code, subdivision_code}, nullptr);
}

}  // namespace shunya_ads

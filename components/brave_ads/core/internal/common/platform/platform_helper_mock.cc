/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/common/platform/platform_helper_mock.h"

namespace shunya_ads {

PlatformHelperMock::PlatformHelperMock() = default;

PlatformHelperMock::~PlatformHelperMock() = default;

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/ml/pipeline/pipeline_util.h"

#include <string>
#include <utility>

#include "base/test/values_test_util.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_file_util.h"
#include "shunya/components/shunya_ads/core/internal/ml/pipeline/pipeline_info.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::ml {

namespace {
constexpr char kValidSpamClassificationPipeline[] =
    "ml/pipeline/text_processing/valid_spam_classification.json";
}  // namespace

class ShunyaAdsPipelineUtilTest : public UnitTestBase {};

TEST_F(ShunyaAdsPipelineUtilTest, ParsePipelineValueTest) {
  // Arrange
  const absl::optional<std::string> json =
      ReadFileFromTestPathToString(kValidSpamClassificationPipeline);
  ASSERT_TRUE(json);

  base::Value::Dict dict = base::test::ParseJsonDict(*json);

  // Act

  // Assert
  EXPECT_TRUE(pipeline::ParsePipelineValue(std::move(dict)));
}

}  // namespace shunya_ads::ml

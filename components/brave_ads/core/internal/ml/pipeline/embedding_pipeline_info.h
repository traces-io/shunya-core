/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_PIPELINE_EMBEDDING_PIPELINE_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_PIPELINE_EMBEDDING_PIPELINE_INFO_H_

#include <map>
#include <string>

#include "base/time/time.h"
#include "shunya/components/shunya_ads/core/internal/ml/data/vector_data.h"

namespace shunya_ads::ml::pipeline {

struct EmbeddingPipelineInfo final {
  EmbeddingPipelineInfo();

  EmbeddingPipelineInfo(const EmbeddingPipelineInfo&) = delete;
  EmbeddingPipelineInfo& operator=(const EmbeddingPipelineInfo&) = delete;

  EmbeddingPipelineInfo(EmbeddingPipelineInfo&& other) noexcept;
  EmbeddingPipelineInfo& operator=(EmbeddingPipelineInfo&& other) noexcept;

  ~EmbeddingPipelineInfo();

  int version = 0;
  base::Time time;
  std::string locale;
  int dimension = 0;
  std::map<std::string, VectorData> embeddings;
};

}  // namespace shunya_ads::ml::pipeline

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_PIPELINE_EMBEDDING_PIPELINE_INFO_H_

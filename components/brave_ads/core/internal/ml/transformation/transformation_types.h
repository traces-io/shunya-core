/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_TRANSFORMATION_TRANSFORMATION_TYPES_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_TRANSFORMATION_TRANSFORMATION_TYPES_H_

namespace shunya_ads::ml {

enum class TransformationType { kLowercase = 0, kHashedNGrams, kNormalization };

}  // namespace shunya_ads::ml

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_TRANSFORMATION_TRANSFORMATION_TYPES_H_

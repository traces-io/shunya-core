/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/ml/data/text_data.h"

#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::ml {

class ShunyaAdsTextDataTest : public UnitTestBase {};

TEST_F(ShunyaAdsTextDataTest, TextDataInitialization) {
  // Arrange
  const std::string text = "The quick brown fox jumps over the lazy dog";

  // Act
  const TextData text_data(text);

  // Assert
  EXPECT_EQ(text, text_data.GetText());
}

}  // namespace shunya_ads::ml

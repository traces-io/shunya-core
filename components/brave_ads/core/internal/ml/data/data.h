/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_DATA_DATA_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_DATA_DATA_H_

#include "shunya/components/shunya_ads/core/internal/ml/data/data_types.h"

namespace shunya_ads::ml {

class Data {
 public:
  explicit Data(const DataType& type);

  Data(const Data&) = delete;
  Data& operator=(const Data&) = delete;

  Data(Data&&) noexcept = delete;
  Data& operator=(Data&&) noexcept = delete;

  virtual ~Data();

  DataType GetType() const;

 protected:
  const DataType type_;
};

}  // namespace shunya_ads::ml

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ML_DATA_DATA_H_

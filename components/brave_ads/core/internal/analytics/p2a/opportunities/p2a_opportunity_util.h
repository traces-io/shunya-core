/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ANALYTICS_P2A_OPPORTUNITIES_P2A_OPPORTUNITY_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ANALYTICS_P2A_OPPORTUNITIES_P2A_OPPORTUNITY_UTIL_H_

#include <string>
#include <vector>

#include "shunya/components/shunya_ads/core/internal/segments/segment_alias.h"

namespace shunya_ads::p2a {

std::vector<std::string> BuildAdOpportunityEvents(const SegmentList& segments);

}  // namespace shunya_ads::p2a

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ANALYTICS_P2A_OPPORTUNITIES_P2A_OPPORTUNITY_UTIL_H_

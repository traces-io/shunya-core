/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/analytics/p2a/opportunities/p2a_opportunity_util.h"

#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::p2a {

TEST(ShunyaAdsP2AOpportunityUtilTest, BuildAdOpportunityEvents) {
  // Arrange

  // Act

  // Assert
  const std::vector<std::string> expected_events = {
      "Shunya.P2A.AdOpportunitiesPerSegment.technologycomputing",
      "Shunya.P2A.AdOpportunitiesPerSegment.personalfinance",
      "Shunya.P2A.AdOpportunitiesPerSegment.travel",
      "Shunya.P2A.TotalAdOpportunities"};

  EXPECT_EQ(expected_events, BuildAdOpportunityEvents(/*segments*/ {
                                 "technology & computing",
                                 "personal finance-crypto", "travel"}));
}

TEST(ShunyaAdsP2AOpportunityUtilTest, BuildAdOpportunityEventsForEmptySegments) {
  // Arrange

  // Act

  // Assert
  const std::vector<std::string> expected_events = {
      "Shunya.P2A.TotalAdOpportunities"};

  EXPECT_EQ(expected_events, BuildAdOpportunityEvents(/*segments*/ {}));
}

}  // namespace shunya_ads::p2a

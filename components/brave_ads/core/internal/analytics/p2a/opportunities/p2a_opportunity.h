/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ANALYTICS_P2A_OPPORTUNITIES_P2A_OPPORTUNITY_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ANALYTICS_P2A_OPPORTUNITIES_P2A_OPPORTUNITY_H_

#include "shunya/components/shunya_ads/core/internal/segments/segment_alias.h"

namespace shunya_ads {

class AdType;

namespace p2a {

void RecordAdOpportunity(const AdType& ad_type, const SegmentList& segments);

}  // namespace p2a
}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_ANALYTICS_P2A_OPPORTUNITIES_P2A_OPPORTUNITY_H_

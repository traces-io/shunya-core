/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/analytics/p2a/p2a.h"

#include "base/test/values_test_util.h"
#include "base/values.h"
#include "shunya/components/shunya_ads/core/internal/common/unittest/unittest_base.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads::p2a {

namespace {
constexpr char kEventsAsJson[] = R"(["event_1","event_2"])";
}  // namespace

class ShunyaAdsP2ATest : public UnitTestBase {};

TEST_F(ShunyaAdsP2ATest, RecordEvent) {
  // Arrange
  const base::Value::List events = base::test::ParseJsonList(kEventsAsJson);

  // Assert
  EXPECT_CALL(ads_client_mock_,
              RecordP2AEvents(::testing::Eq(std::ref(events))));

  // Act
  RecordEvent({"event_1", "event_2"});
}

}  // namespace shunya_ads::p2a

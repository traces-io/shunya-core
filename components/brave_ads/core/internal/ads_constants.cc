/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/public/ads_constants.h"  // IWYU pragma: keep

namespace shunya_ads::data::resource {

const char kCatalogJsonSchemaFilename[] = "catalog-schema.json";

}  // namespace shunya_ads::data::resource

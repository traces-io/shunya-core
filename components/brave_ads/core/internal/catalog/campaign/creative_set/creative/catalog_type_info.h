/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CAMPAIGN_CREATIVE_SET_CREATIVE_CATALOG_TYPE_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CAMPAIGN_CREATIVE_SET_CREATIVE_CATALOG_TYPE_INFO_H_

#include <string>

namespace shunya_ads {

struct CatalogTypeInfo final {
  CatalogTypeInfo();

  CatalogTypeInfo(const CatalogTypeInfo&);
  CatalogTypeInfo& operator=(const CatalogTypeInfo&);

  CatalogTypeInfo(CatalogTypeInfo&&) noexcept;
  CatalogTypeInfo& operator=(CatalogTypeInfo&&) noexcept;

  ~CatalogTypeInfo();

  bool operator==(const CatalogTypeInfo&) const;
  bool operator!=(const CatalogTypeInfo&) const;

  std::string code;
  std::string name;
  std::string platform;
  int version = 0;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CAMPAIGN_CREATIVE_SET_CREATIVE_CATALOG_TYPE_INFO_H_

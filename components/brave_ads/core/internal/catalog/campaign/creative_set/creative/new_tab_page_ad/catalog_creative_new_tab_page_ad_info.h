/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CAMPAIGN_CREATIVE_SET_CREATIVE_NEW_TAB_PAGE_AD_CATALOG_CREATIVE_NEW_TAB_PAGE_AD_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CAMPAIGN_CREATIVE_SET_CREATIVE_NEW_TAB_PAGE_AD_CATALOG_CREATIVE_NEW_TAB_PAGE_AD_INFO_H_

#include <vector>

#include "shunya/components/shunya_ads/core/internal/catalog/campaign/creative_set/creative/catalog_creative_info.h"
#include "shunya/components/shunya_ads/core/internal/catalog/campaign/creative_set/creative/new_tab_page_ad/catalog_new_tab_page_ad_payload_info.h"

namespace shunya_ads {

struct CatalogCreativeNewTabPageAdInfo final : CatalogCreativeInfo {
  bool operator==(const CatalogCreativeNewTabPageAdInfo&) const;
  bool operator!=(const CatalogCreativeNewTabPageAdInfo&) const;

  CatalogNewTabPageAdPayloadInfo payload;
};

using CatalogCreativeNewTabPageAdList =
    std::vector<CatalogCreativeNewTabPageAdInfo>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CAMPAIGN_CREATIVE_SET_CREATIVE_NEW_TAB_PAGE_AD_CATALOG_CREATIVE_NEW_TAB_PAGE_AD_INFO_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/catalog/campaign/creative_set/catalog_segment_info.h"

#include <tuple>

namespace shunya_ads {

bool operator==(const CatalogSegmentInfo& lhs, const CatalogSegmentInfo& rhs) {
  const auto tie = [](const CatalogSegmentInfo& segment) {
    return std::tie(segment.code, segment.name);
  };

  return tie(lhs) == tie(rhs);
}

bool operator!=(const CatalogSegmentInfo& lhs, const CatalogSegmentInfo& rhs) {
  return !(lhs == rhs);
}

}  // namespace shunya_ads

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CATALOG_URL_REQUEST_BUILDER_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CATALOG_URL_REQUEST_BUILDER_H_

#include "shunya/components/shunya_ads/core/internal/common/url/request_builder/url_request_builder_interface.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-forward.h"

namespace shunya_ads {

class CatalogUrlRequestBuilder final : public UrlRequestBuilderInterface {
 public:
  mojom::UrlRequestInfoPtr Build() override;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_CATALOG_CATALOG_URL_REQUEST_BUILDER_H_

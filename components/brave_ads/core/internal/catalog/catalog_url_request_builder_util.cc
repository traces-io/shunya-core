/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/catalog/catalog_url_request_builder_util.h"

#include "base/strings/stringprintf.h"
#include "shunya/components/shunya_ads/core/internal/catalog/catalog_constants.h"

namespace shunya_ads {

std::string BuildCatalogUrlPath() {
  return base::StringPrintf("/v%d/catalog", kCatalogVersion);
}

}  // namespace shunya_ads

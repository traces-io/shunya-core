/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_ads/core/internal/reminder/reminder_feature.h"

#include <vector>

#include "base/test/scoped_feature_list.h"
#include "testing/gtest/include/gtest/gtest.h"

// npm run test -- shunya_unit_tests --filter=ShunyaAds*

namespace shunya_ads {

TEST(ShunyaAdsReminderFeatureTest, IsEnabled) {
  // Arrange

  // Act

  // Assert
  EXPECT_TRUE(IsReminderFeatureEnabled());
}

TEST(ShunyaAdsReminderFeatureTest, IsDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(kReminderFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_FALSE(IsReminderFeatureEnabled());
}

TEST(ShunyaAdsReminderFeatureTest, RemindUserIfClickingTheSameAdAfter) {
  // Arrange
  base::FieldTrialParams params;
  params["remind_user_if_clicking_the_same_ad_after"] = "1";
  std::vector<base::test::FeatureRefAndParams> enabled_features;
  enabled_features.emplace_back(kReminderFeature, params);

  const std::vector<base::test::FeatureRef> disabled_features;

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_EQ(1, kRemindUserIfClickingTheSameAdAfter.Get());
}

TEST(ShunyaAdsReminderFeatureTest, DefaultRemindUserIfClickingTheSameAdAfter) {
  // Arrange

  // Act

  // Assert
  EXPECT_EQ(3, kRemindUserIfClickingTheSameAdAfter.Get());
}

TEST(ShunyaAdsReminderFeatureTest,
     DefaultRemindUserIfClickingTheSameAdAfterWhenDisabled) {
  // Arrange
  const std::vector<base::test::FeatureRefAndParams> enabled_features;

  std::vector<base::test::FeatureRef> disabled_features;
  disabled_features.emplace_back(kReminderFeature);

  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeaturesAndParameters(enabled_features,
                                                    disabled_features);

  // Act

  // Assert
  EXPECT_EQ(3, kRemindUserIfClickingTheSameAdAfter.Get());
}

}  // namespace shunya_ads

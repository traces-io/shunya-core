/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_REMINDER_REMINDER_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_REMINDER_REMINDER_H_

#include "base/timer/timer.h"
#include "shunya/components/shunya_ads/core/internal/history/history_manager_observer.h"

namespace shunya_ads {

struct HistoryItemInfo;

class Reminder : public HistoryManagerObserver {
 public:
  Reminder();

  Reminder(const Reminder&) = delete;
  Reminder& operator=(const Reminder&) = delete;

  Reminder(Reminder&&) noexcept = delete;
  Reminder& operator=(Reminder&&) noexcept = delete;

  ~Reminder() override;

 private:
  // HistoryManagerObserver:
  void OnDidAddHistory(const HistoryItemInfo& history_item) override;

  base::OneShotTimer timer_;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_REMINDER_REMINDER_H_

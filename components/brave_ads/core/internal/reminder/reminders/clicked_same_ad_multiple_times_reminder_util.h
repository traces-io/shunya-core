/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_REMINDER_REMINDERS_CLICKED_SAME_AD_MULTIPLE_TIMES_REMINDER_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_REMINDER_REMINDERS_CLICKED_SAME_AD_MULTIPLE_TIMES_REMINDER_UTIL_H_

namespace shunya_ads {

struct HistoryItemInfo;

bool DidUserClickTheSameAdMultipleTimes(const HistoryItemInfo& history_item);

void RemindUserTheyDoNotNeedToClickToEarnRewards();

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_INTERNAL_REMINDER_REMINDERS_CLICKED_SAME_AD_MULTIPLE_TIMES_REMINDER_UTIL_H_

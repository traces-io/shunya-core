/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_CLIENT_ADS_CLIENT_CALLBACK_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_CLIENT_ADS_CLIENT_CALLBACK_H_

#include <string>
#include <vector>

#include "base/files/file.h"
#include "base/functional/callback.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-forward.h"
#include "third_party/abseil-cpp/absl/types/optional.h"
#include "url/gurl.h"

namespace shunya_ads {

using ResultCallback = base::OnceCallback<void(bool success)>;

using SaveCallback = base::OnceCallback<void(bool success)>;

using LoadCallback =
    base::OnceCallback<void(const absl::optional<std::string>& value)>;

using LoadFileCallback = base::OnceCallback<void(base::File file)>;

using UrlRequestCallback =
    base::OnceCallback<void(const mojom::UrlResponseInfo& url_response)>;

using RunDBTransactionCallback =
    base::OnceCallback<void(mojom::DBCommandResponseInfoPtr command_response)>;

using GetBrowsingHistoryCallback =
    base::OnceCallback<void(const std::vector<GURL>& browsing_history)>;

using GetScheduledCaptchaCallback =
    base::OnceCallback<void(const std::string& captcha_id)>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_CLIENT_ADS_CLIENT_CALLBACK_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_DATABASE_DATABASE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_DATABASE_DATABASE_H_

#include <memory>

#include "base/files/file_path.h"
#include "base/memory/memory_pressure_listener.h"
#include "base/memory/weak_ptr.h"
#include "base/sequence_checker.h"
#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom.h"
#include "shunya/components/shunya_ads/core/public/export.h"
#include "sql/database.h"
#include "sql/meta_table.h"

namespace shunya_ads {

class ADS_EXPORT Database final {
 public:
  explicit Database(base::FilePath path);

  Database(const Database&) = delete;
  Database& operator=(const Database&) = delete;

  Database(Database&&) noexcept = delete;
  Database& operator=(Database&&) noexcept = delete;

  ~Database();

  void RunTransaction(mojom::DBTransactionInfoPtr transaction,
                      mojom::DBCommandResponseInfo* command_response);

 private:
  mojom::DBCommandResponseInfo::StatusType Initialize(
      int version,
      int compatible_version,
      mojom::DBCommandResponseInfo* command_response);

  mojom::DBCommandResponseInfo::StatusType Execute(
      mojom::DBCommandInfo* command);

  mojom::DBCommandResponseInfo::StatusType Run(mojom::DBCommandInfo* command);

  mojom::DBCommandResponseInfo::StatusType Read(
      mojom::DBCommandInfo* command,
      mojom::DBCommandResponseInfo* command_response);

  mojom::DBCommandResponseInfo::StatusType Migrate(int version,
                                                   int compatible_version);

  bool ShouldCreateTables();

  int GetTablesCount();

  void ErrorCallback(int error, sql::Statement* statement);

  void MemoryPressureListenerCallback(
      base::MemoryPressureListener::MemoryPressureLevel memory_pressure_level);

  base::FilePath db_path_;
  sql::Database db_;
  sql::MetaTable meta_table_;
  bool is_initialized_ = false;

  std::unique_ptr<base::MemoryPressureListener> memory_pressure_listener_;

  SEQUENCE_CHECKER(sequence_checker_);

  base::WeakPtrFactory<Database> weak_factory_{this};
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_DATABASE_DATABASE_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_USER_USER_ATTENTION_USER_IDLE_DETECTION_USER_IDLE_DETECTION_FEATURE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_USER_USER_ATTENTION_USER_IDLE_DETECTION_USER_IDLE_DETECTION_FEATURE_H_

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"

namespace base {
class TimeDelta;
}  // namespace base

namespace shunya_ads {

BASE_DECLARE_FEATURE(kUserIdleDetectionFeature);

constexpr base::FeatureParam<base::TimeDelta> kIdleThreshold{
    &kUserIdleDetectionFeature, "idle_threshold", base::Seconds(5)};

constexpr base::FeatureParam<base::TimeDelta> kMaximumIdleTime{
    &kUserIdleDetectionFeature, "maximum_idle_time", base::Seconds(0)};

constexpr base::FeatureParam<bool> kShouldDetectScreenWasLocked{
    &kUserIdleDetectionFeature, "should_detect_screen_was_locked", false};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_USER_USER_ATTENTION_USER_IDLE_DETECTION_USER_IDLE_DETECTION_FEATURE_H_

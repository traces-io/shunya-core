/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_ADS_CONSTANTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_ADS_CONSTANTS_H_

#include "shunya/components/shunya_ads/core/public/export.h"

namespace shunya_ads::data::resource {

ADS_EXPORT extern const char kCatalogJsonSchemaFilename[];

}  // namespace shunya_ads::data::resource

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_ADS_CONSTANTS_H_

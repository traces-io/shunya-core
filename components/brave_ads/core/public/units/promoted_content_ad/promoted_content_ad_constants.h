/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_PROMOTED_CONTENT_AD_PROMOTED_CONTENT_AD_CONSTANTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_PROMOTED_CONTENT_AD_PROMOTED_CONTENT_AD_CONSTANTS_H_

namespace shunya_ads {

constexpr char kPromotedContentAdPlacementIdKey[] = "uuid";
constexpr char kPromotedContentAdCreativeInstanceIdKey[] =
    "creative_instance_id";
constexpr char kPromotedContentAdCreativeSetIdKey[] = "creative_set_id";
constexpr char kPromotedContentAdCampaignIdKey[] = "campaign_id";
constexpr char kPromotedContentAdAdvertiserIdKey[] = "advertiser_id";
constexpr char kPromotedContentAdSegmentKey[] = "segment";
constexpr char kPromotedContentAdTitleKey[] = "title";
constexpr char kPromotedContentAdDescriptionnKey[] = "description";
constexpr char kPromotedContentAdTargetUrlKey[] = "target_url";

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_PROMOTED_CONTENT_AD_PROMOTED_CONTENT_AD_CONSTANTS_H_

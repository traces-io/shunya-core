/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_FEATURE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_FEATURE_H_

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"
#include "shunya/components/shunya_ads/core/public/units/notification_ad/notification_ad_constants.h"

namespace shunya_ads {

BASE_DECLARE_FEATURE(kNotificationAdFeature);
BASE_DECLARE_FEATURE(kAllowedToFallbackToCustomNotificationAdFeature);

bool IsNotificationAdFeatureEnabled();

bool IsAllowedToFallbackToCustomNotificationAdFeatureEnabled();

// Ad notification timeout in seconds. Set to 0 to never time out
constexpr base::FeatureParam<int> kNotificationAdTimeout{
    &kNotificationAdFeature, "notification_ad_timeout",
    kDefaultNotificationAdTimeout};

constexpr base::FeatureParam<int> kDefaultNotificationAdsPerHour{
    &kNotificationAdFeature, "default_ads_per_hour",
    kDefaultShunyaRewardsNotificationAdsPerHour};

constexpr base::FeatureParam<int> kMaximumNotificationAdsPerDay{
    &kNotificationAdFeature, "maximum_ads_per_day", 100};

// Set to true to fallback to custom notification ads if native notifications
// are disabled or false to never fallback
constexpr base::FeatureParam<bool> kCanFallbackToCustomNotificationAds{
    &kNotificationAdFeature, "can_fallback_to_custom_notifications",
    kDefaultCanFallbackToCustomNotificationAds};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_FEATURE_H_

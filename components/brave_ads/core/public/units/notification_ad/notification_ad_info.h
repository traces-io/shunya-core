/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_INFO_H_

#include <string>

#include "shunya/components/shunya_ads/core/public/export.h"
#include "shunya/components/shunya_ads/core/public/units/ad_info.h"

namespace shunya_ads {

struct ADS_EXPORT NotificationAdInfo final : AdInfo {
  [[nodiscard]] bool IsValid() const;

  std::string title;
  std::string body;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NOTIFICATION_AD_NOTIFICATION_AD_INFO_H_

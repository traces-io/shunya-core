/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_WALLPAPER_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_WALLPAPER_INFO_H_

#include <vector>

#include "shunya/components/shunya_ads/core/public/export.h"
#include "shunya/components/shunya_ads/core/public/units/new_tab_page_ad/new_tab_page_ad_wallpaper_focal_point_info.h"
#include "url/gurl.h"

namespace shunya_ads {

struct ADS_EXPORT NewTabPageAdWallpaperInfo final {
  GURL image_url;
  NewTabPageAdWallpaperFocalPointInfo focal_point;
};

bool operator==(const NewTabPageAdWallpaperInfo&,
                const NewTabPageAdWallpaperInfo&);
bool operator!=(const NewTabPageAdWallpaperInfo&,
                const NewTabPageAdWallpaperInfo&);

using NewTabPageAdWallpaperList = std::vector<NewTabPageAdWallpaperInfo>;

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_WALLPAPER_INFO_H_

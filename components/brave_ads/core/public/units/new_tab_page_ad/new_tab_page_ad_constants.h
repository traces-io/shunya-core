/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_CONSTANTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_CONSTANTS_H_

namespace shunya_ads {

constexpr char kNewTabPageAdPlacementIdKey[] = "placement_id";
constexpr char kNewTabPageAdCreativeInstanceIdKey[] = "creative_instance_id";
constexpr char kNewTabPageAdCreativeSetIdKey[] = "creative_set_id";
constexpr char kNewTabPageAdCampaignIdKey[] = "campaign_id";
constexpr char kNewTabPageAdAdvertiserIdKey[] = "advertiser_id";
constexpr char kNewTabPageAdSegmentKey[] = "segment";
constexpr char kNewTabPageAdCompanyNameKey[] = "company_name";
constexpr char kNewTabPageAdAltKey[] = "alt";
constexpr char kNewTabPageAdImageUrlKey[] = "image_url";
constexpr char kNewTabPageAdFocalPointKey[] = "focal_point";
constexpr char kNewTabPageAdFocalPointXKey[] = "x";
constexpr char kNewTabPageAdFocalPointYKey[] = "y";
constexpr char kNewTabPageAdWallpapersKey[] = "wallpapers";
constexpr char kNewTabPageAdTargetUrlKey[] = "target_url";

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_CONSTANTS_H_

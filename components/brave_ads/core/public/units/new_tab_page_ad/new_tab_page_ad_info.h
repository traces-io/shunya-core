/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_INFO_H_

#include <string>

#include "shunya/components/shunya_ads/core/public/export.h"
#include "shunya/components/shunya_ads/core/public/units/ad_info.h"
#include "shunya/components/shunya_ads/core/public/units/new_tab_page_ad/new_tab_page_ad_wallpaper_info.h"
#include "url/gurl.h"

namespace shunya_ads {

struct ADS_EXPORT NewTabPageAdInfo final : AdInfo {
  NewTabPageAdInfo();

  NewTabPageAdInfo(const NewTabPageAdInfo&);
  NewTabPageAdInfo& operator=(const NewTabPageAdInfo&);

  NewTabPageAdInfo(NewTabPageAdInfo&&) noexcept;
  NewTabPageAdInfo& operator=(NewTabPageAdInfo&&) noexcept;

  ~NewTabPageAdInfo();

  bool operator==(const NewTabPageAdInfo&) const;
  bool operator!=(const NewTabPageAdInfo&) const;

  [[nodiscard]] bool IsValid() const;

  std::string company_name;
  GURL image_url;
  std::string alt;
  NewTabPageAdWallpaperList wallpapers;
};

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_UNITS_NEW_TAB_PAGE_AD_NEW_TAB_PAGE_AD_INFO_H_

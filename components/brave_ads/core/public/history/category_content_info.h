/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_CATEGORY_CONTENT_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_CATEGORY_CONTENT_INFO_H_

#include <string>

#include "shunya/components/shunya_ads/core/mojom/shunya_ads.mojom-shared.h"
#include "shunya/components/shunya_ads/core/public/export.h"

namespace shunya_ads {

struct ADS_EXPORT CategoryContentInfo final {
  std::string category;
  mojom::UserReactionType user_reaction_type =
      mojom::UserReactionType::kNeutral;
};

bool operator==(const CategoryContentInfo&, const CategoryContentInfo&);
bool operator!=(const CategoryContentInfo&, const CategoryContentInfo&);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_CATEGORY_CONTENT_INFO_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_HISTORY_ITEM_VALUE_UTIL_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_HISTORY_ITEM_VALUE_UTIL_H_

#include "base/values.h"
#include "shunya/components/shunya_ads/core/public/history/history_item_info.h"

namespace shunya_ads {

base::Value::List HistoryItemsToValue(const HistoryItemList& history_items);
base::Value::List HistoryItemsToUIValue(const HistoryItemList& history_items);
HistoryItemList HistoryItemsFromValue(const base::Value::List& list);

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_HISTORY_ITEM_VALUE_UTIL_H_

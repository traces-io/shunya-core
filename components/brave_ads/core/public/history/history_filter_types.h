/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_HISTORY_FILTER_TYPES_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_HISTORY_FILTER_TYPES_H_

namespace shunya_ads {

enum class HistoryFilterType { kNone = 0, kConfirmationType };

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_HISTORY_HISTORY_FILTER_TYPES_H_

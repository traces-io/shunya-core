/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_TARGETING_GEOGRAPHICAL_SUBDIVISION_SUPPORTED_SUBDIVISIONS_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_TARGETING_GEOGRAPHICAL_SUBDIVISION_SUPPORTED_SUBDIVISIONS_H_

#include <string_view>

#include "base/containers/flat_map.h"
#include "base/values.h"
#include "shunya/components/shunya_ads/core/public/export.h"

namespace shunya_ads {

using SupportedSubdivisions = base::flat_map</*subdivision*/ std::string_view,
                                             /*name*/ std::string_view>;

using SupportedSubdivisionMap =
    base::flat_map</*country_code*/ std::string_view, SupportedSubdivisions>;

ADS_EXPORT const SupportedSubdivisionMap& GetSupportedSubdivisions();

ADS_EXPORT base::Value::List GetSupportedSubdivisionsAsValueList();

}  // namespace shunya_ads

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADS_CORE_PUBLIC_TARGETING_GEOGRAPHICAL_SUBDIVISION_SUPPORTED_SUBDIVISIONS_H_

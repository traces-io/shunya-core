/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_PRIVATE_NEW_TAB_UI_COMMON_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SHUNYA_PRIVATE_NEW_TAB_UI_COMMON_PREF_NAMES_H_

class PrefRegistrySimple;

namespace shunya_private_new_tab::prefs {

constexpr char kShunyaPrivateWindowDisclaimerDismissed[] =
    "shunya.shunya_private_new_tab.private_window_disclaimer_dismissed";
constexpr char kShunyaTorWindowDisclaimerDismissed[] =
    "shunya.shunya_private_new_tab.tor_window_disclaimer_dismissed";

void RegisterProfilePrefs(PrefRegistrySimple* registry);

}  // namespace shunya_private_new_tab::prefs

#endif  // SHUNYA_COMPONENTS_SHUNYA_PRIVATE_NEW_TAB_UI_COMMON_PREF_NAMES_H_

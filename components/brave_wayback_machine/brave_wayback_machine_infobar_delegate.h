/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WAYBACK_MACHINE_SHUNYA_WAYBACK_MACHINE_INFOBAR_DELEGATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_WAYBACK_MACHINE_SHUNYA_WAYBACK_MACHINE_INFOBAR_DELEGATE_H_

#include <memory>

#include "components/infobars/core/infobar_delegate.h"

class ShunyaWaybackMachineInfoBarDelegate : public infobars::InfoBarDelegate {
 public:
  ShunyaWaybackMachineInfoBarDelegate();
  ~ShunyaWaybackMachineInfoBarDelegate() override;

  ShunyaWaybackMachineInfoBarDelegate(
      const ShunyaWaybackMachineInfoBarDelegate&) = delete;
  ShunyaWaybackMachineInfoBarDelegate& operator=(
      const ShunyaWaybackMachineInfoBarDelegate&) = delete;

 private:
  // infobars::InfoBarDelegate overrides:
  InfoBarIdentifier GetIdentifier() const override;
  bool EqualsDelegate(
      infobars::InfoBarDelegate* delegate) const override;
};

#endif  // SHUNYA_COMPONENTS_SHUNYA_WAYBACK_MACHINE_SHUNYA_WAYBACK_MACHINE_INFOBAR_DELEGATE_H_

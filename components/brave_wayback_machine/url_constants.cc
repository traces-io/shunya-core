/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_wayback_machine/url_constants.h"

const char kWaybackQueryURL[] =
    "https://shunya-api.archive.org/wayback/available?url=";
const char kWaybackHost[] = "web.archive.org";

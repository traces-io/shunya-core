/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_WAYBACK_MACHINE_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SHUNYA_WAYBACK_MACHINE_PREF_NAMES_H_

extern const char kShunyaWaybackMachineEnabled[];

#endif  // SHUNYA_COMPONENTS_SHUNYA_WAYBACK_MACHINE_PREF_NAMES_H_

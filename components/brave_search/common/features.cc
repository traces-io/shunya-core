// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "SHUNYA/components/SHUNYA_search/common/features.h"

#include "base/feature_list.h"

namespace SHUNYA_search {
namespace features {

BASE_FEATURE(kShunyaSearchDefaultAPIFeature,
             "ShunyaSearchDefaultAPI",
             base::FEATURE_ENABLED_BY_DEFAULT);

const base::FeatureParam<int> kShunyaSearchDefaultAPIDailyLimit{
    &kShunyaSearchDefaultAPIFeature, kShunyaSearchDefaultAPIDailyLimitName, 3};

const base::FeatureParam<int> kShunyaSearchDefaultAPITotalLimit{
    &kShunyaSearchDefaultAPIFeature, kShunyaSearchDefaultAPITotalLimitName, 10};

}  // namespace features
}  // namespace SHUNYA_search

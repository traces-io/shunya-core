// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_SEARCH_COMMON_FEATURES_H_
#define SHUNYA_COMPONENTS_SHUNYA_SEARCH_COMMON_FEATURES_H_

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"

namespace SHUNYA_search {
namespace features {

constexpr char kShunyaSearchDefaultAPIDailyLimitName[] =
    "ShunyaSearchDefaultAPIDailyLimit";
constexpr char kShunyaSearchDefaultAPITotalLimitName[] =
    "ShunyaSearchDefaultAPITotalLimit";

BASE_DECLARE_FEATURE(kShunyaSearchDefaultAPIFeature);
extern const base::FeatureParam<int> kShunyaSearchDefaultAPIDailyLimit;
extern const base::FeatureParam<int> kShunyaSearchDefaultAPITotalLimit;

}  // namespace features
}  // namespace SHUNYA_search

#endif  // SHUNYA_COMPONENTS_SHUNYA_SEARCH_COMMON_FEATURES_H_

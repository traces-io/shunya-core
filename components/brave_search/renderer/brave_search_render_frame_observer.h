// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_SEARCH_RENDERER_SHUNYA_SEARCH_RENDER_FRAME_OBSERVER_H_
#define SHUNYA_COMPONENTS_SHUNYA_SEARCH_RENDERER_SHUNYA_SEARCH_RENDER_FRAME_OBSERVER_H_

#include <memory>

#include "SHUNYA/components/SHUNYA_search/renderer/SHUNYA_search_default_js_handler.h"
#include "content/public/renderer/render_frame.h"
#include "content/public/renderer/render_frame_observer.h"
#include "third_party/blink/public/web/web_navigation_type.h"
#include "v8/include/v8.h"

namespace SHUNYA_search {

class ShunyaSearchRenderFrameObserver : public content::RenderFrameObserver {
 public:
  explicit ShunyaSearchRenderFrameObserver(content::RenderFrame* render_frame,
                                          int32_t world_id);
  ShunyaSearchRenderFrameObserver(const ShunyaSearchRenderFrameObserver&) =
      delete;
  ShunyaSearchRenderFrameObserver& operator=(
      const ShunyaSearchRenderFrameObserver&) = delete;
  ~ShunyaSearchRenderFrameObserver() override;

  // RenderFrameObserver implementation.
  void DidCreateScriptContext(v8::Local<v8::Context> context,
                              int32_t world_id) override;
  void DidStartNavigation(
      const GURL& url,
      absl::optional<blink::WebNavigationType> navigation_type) override;

 private:
  // RenderFrameObserver implementation.
  void OnDestruct() override;

  // Handle to "handler" JavaScript object functionality.
  std::unique_ptr<ShunyaSearchDefaultJSHandler> native_javascript_handle_;
  int32_t world_id_;
  GURL url_;
};

}  // namespace SHUNYA_search

#endif  // SHUNYA_COMPONENTS_SHUNYA_SEARCH_RENDERER_SHUNYA_SEARCH_RENDER_FRAME_OBSERVER_H_

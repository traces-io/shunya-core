// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "SHUNYA/components/SHUNYA_search/renderer/SHUNYA_search_render_frame_observer.h"

#include <memory>

#include "SHUNYA/components/SHUNYA_search/common/SHUNYA_search_utils.h"
#include "SHUNYA/components/SHUNYA_search/renderer/SHUNYA_search_default_js_handler.h"
#include "content/public/renderer/render_frame.h"
#include "net/base/url_util.h"
#include "third_party/blink/public/web/web_local_frame.h"
#include "url/gurl.h"

namespace SHUNYA_search {

ShunyaSearchRenderFrameObserver::ShunyaSearchRenderFrameObserver(
    content::RenderFrame* render_frame,
    int32_t world_id)
    : RenderFrameObserver(render_frame), world_id_(world_id) {}

ShunyaSearchRenderFrameObserver::~ShunyaSearchRenderFrameObserver() = default;

void ShunyaSearchRenderFrameObserver::DidCreateScriptContext(
    v8::Local<v8::Context> context,
    int32_t world_id) {
  if (!render_frame()->IsMainFrame() || world_id_ != world_id)
    return;

  GURL origin =
      url::Origin(render_frame()->GetWebFrame()->GetSecurityOrigin()).GetURL();

  if (!IsAllowedHost(origin))
    return;

  bool can_always_set_default = false;
  for (net::QueryIterator it(url_); !it.IsAtEnd(); it.Advance()) {
    if (it.GetKey() == "action" && it.GetValue() == "makeDefault") {
      can_always_set_default = true;
      break;
    }
  }

  if (!native_javascript_handle_) {
    native_javascript_handle_ = std::make_unique<ShunyaSearchDefaultJSHandler>(
        render_frame(), can_always_set_default);
  } else {
    native_javascript_handle_->ResetRemote(render_frame());
  }

  native_javascript_handle_->AddJavaScriptObjectToFrame(context);
}

void ShunyaSearchRenderFrameObserver::DidStartNavigation(
    const GURL& url,
    absl::optional<blink::WebNavigationType> navigation_type) {
  url_ = url;
}

void ShunyaSearchRenderFrameObserver::OnDestruct() {
  delete this;
}

}  // namespace SHUNYA_search

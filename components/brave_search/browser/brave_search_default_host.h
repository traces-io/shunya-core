// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_SHUNYA_SEARCH_DEFAULT_HOST_H_
#define SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_SHUNYA_SEARCH_DEFAULT_HOST_H_

#include <string>

#include "base/memory/raw_ptr.h"
#include "shunya/components/shunya_search/common/shunya_search_default.mojom.h"

class TemplateURLService;
class TemplateURL;
class PrefService;
class PrefRegistrySimple;

namespace shunya_search {

class ShunyaSearchDefaultHost final
    : public shunya_search::mojom::ShunyaSearchDefault {
 public:
  static void RegisterProfilePrefs(PrefRegistrySimple* registry);

  ShunyaSearchDefaultHost(const ShunyaSearchDefaultHost&) = delete;
  ShunyaSearchDefaultHost& operator=(const ShunyaSearchDefaultHost&) = delete;

  ShunyaSearchDefaultHost(const std::string& host,
                         TemplateURLService* template_url_service,
                         PrefService* prefs);
  ~ShunyaSearchDefaultHost() override;

  // shunya_search::mojom::ShunyaSearchDefault:
  void SetCanAlwaysSetDefault() override;
  void GetCanSetDefaultSearchProvider(
      GetCanSetDefaultSearchProviderCallback callback) override;
  void SetIsDefaultSearchProvider() override;

 private:
  bool CanSetDefaultSearchProvider(TemplateURL* provider, bool is_historic);
  uint64_t GetMaxDailyCanAskCount();
  uint64_t GetMaxTotalCanAskCount();

  bool can_always_set_default_ = false;
  bool can_set_default_ = false;
  const std::string host_;
  raw_ptr<TemplateURLService> template_url_service_ = nullptr;
  raw_ptr<PrefService> prefs_ = nullptr;
};

}  // namespace shunya_search

#endif  // SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_SHUNYA_SEARCH_DEFAULT_HOST_H_

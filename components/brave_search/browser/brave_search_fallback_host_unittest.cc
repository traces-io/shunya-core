/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_search/browser/shunya_search_fallback_host.h"
#include "testing/gtest/include/gtest/gtest.h"
#include "url/gurl.h"

namespace shunya_search {

TEST(ShunyaSearchFallbackHost, GetBackupResultURL) {
  GURL base_url("https://www.google.com/search/");
  ASSERT_EQ(ShunyaSearchFallbackHost::GetBackupResultURL(base_url, "test", "en",
                                                        "ca", "32,32", true, 0),
            GURL("https://www.google.com/search/"
                 "?q=test&start=0&hl=en&gl=ca&safe=active"));
}

TEST(ShunyaSearchFallbackHost, GetBackupResultURLNoLang) {
  GURL base_url("https://www.google.com/search/");
  ASSERT_EQ(
      ShunyaSearchFallbackHost::GetBackupResultURL(base_url, "test", "", "ca",
                                                  "32,32", true, 0),
      GURL("https://www.google.com/search/?q=test&start=0&gl=ca&safe=active"));
}

TEST(ShunyaSearchFallbackHost, GetBackupResultURLNoCountry) {
  GURL base_url("https://www.google.com/search/");
  ASSERT_EQ(
      ShunyaSearchFallbackHost::GetBackupResultURL(base_url, "test", "en", "",
                                                  "32,32", true, 0),
      GURL("https://www.google.com/search/?q=test&start=0&hl=en&safe=active"));
}

TEST(ShunyaSearchFallbackHost, GetBackupResultURLNoFilter) {
  GURL base_url("https://www.google.com/search/");
  ASSERT_EQ(ShunyaSearchFallbackHost::GetBackupResultURL(
                base_url, "test", "en", "ca", "32,32", false, 0),
            GURL("https://www.google.com/search/?q=test&start=0&hl=en&gl=ca"));
}

TEST(ShunyaSearchFallbackHost, GetBackupResultURLMinimal) {
  GURL base_url("https://www.google.com/search/");
  ASSERT_EQ(ShunyaSearchFallbackHost::GetBackupResultURL(base_url, "test", "",
                                                        "", "", false, 0),
            GURL("https://www.google.com/search/?q=test&start=0"));
}

TEST(ShunyaSearchFallbackHost, GetBackupResultURLPageIndex) {
  GURL base_url("https://www.google.com/search/");
  ASSERT_EQ(ShunyaSearchFallbackHost::GetBackupResultURL(
                base_url, "test", "en", "ca", "32,32", false, 30),
            GURL("https://www.google.com/search/?q=test&start=30&hl=en&gl=ca"));
}

}  // namespace shunya_search

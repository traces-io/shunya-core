// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/components/shunya_search/browser/shunya_search_default_host_private.h"

#include <utility>

namespace shunya_search {

ShunyaSearchDefaultHostPrivate::~ShunyaSearchDefaultHostPrivate() = default;

void ShunyaSearchDefaultHostPrivate::SetCanAlwaysSetDefault() {}

void ShunyaSearchDefaultHostPrivate::GetCanSetDefaultSearchProvider(
    GetCanSetDefaultSearchProviderCallback callback) {
  std::move(callback).Run(false);
}

void ShunyaSearchDefaultHostPrivate::SetIsDefaultSearchProvider() {}

}  // namespace shunya_search

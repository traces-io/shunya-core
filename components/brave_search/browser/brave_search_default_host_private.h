// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_SHUNYA_SEARCH_DEFAULT_HOST_PRIVATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_SHUNYA_SEARCH_DEFAULT_HOST_PRIVATE_H_

#include "shunya/components/shunya_search/common/shunya_search_default.mojom.h"

class TemplateURLService;
class PrefService;

namespace shunya_search {

class ShunyaSearchDefaultHostPrivate final
    : public shunya_search::mojom::ShunyaSearchDefault {
 public:
  ShunyaSearchDefaultHostPrivate(const ShunyaSearchDefaultHostPrivate&) = delete;
  ShunyaSearchDefaultHostPrivate& operator=(
      const ShunyaSearchDefaultHostPrivate&) = delete;

  ShunyaSearchDefaultHostPrivate() = default;
  ~ShunyaSearchDefaultHostPrivate() override;

  // shunya_search::mojom::ShunyaSearchDefault:
  void SetCanAlwaysSetDefault() override;
  void GetCanSetDefaultSearchProvider(
      GetCanSetDefaultSearchProviderCallback callback) override;
  void SetIsDefaultSearchProvider() override;
};

}  // namespace shunya_search

#endif  // SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_SHUNYA_SEARCH_DEFAULT_HOST_PRIVATE_H_

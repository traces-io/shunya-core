// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_PREFS_H_
#define SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_PREFS_H_

#include "build/build_config.h"

namespace shunya_search {
namespace prefs {

constexpr char kDailyAsked[] = "shunya.shunya_search.daily_asked";
constexpr char kTotalAsked[] = "shunya.shunya_search.total_asked";
#if BUILDFLAG(IS_ANDROID)
constexpr char kFetchFromNative[] = "shunya.shunya_search.fetch_se_from_native";
#endif

}  // namespace prefs
}  // namespace shunya_search

#endif  // SHUNYA_COMPONENTS_SHUNYA_SEARCH_BROWSER_PREFS_H_

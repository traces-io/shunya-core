/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_COMPONENT_INSTALLER_H_
#define SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_COMPONENT_INSTALLER_H_

#include <string>
#include <vector>

#include "base/files/file_path.h"
#include "base/functional/callback.h"
#include "base/values.h"
#include "shunya/components/shunya_component_updater/browser/shunya_component.h"
#include "components/component_updater/component_installer.h"
#include "components/update_client/update_client.h"

using shunya_component_updater::ShunyaComponent;

namespace shunya {

class ShunyaComponentInstallerPolicy
    : public component_updater::ComponentInstallerPolicy {
 public:
  explicit ShunyaComponentInstallerPolicy(
      const std::string& name,
      const std::string& base64_public_key,
      ShunyaComponent::ReadyCallback ready_callback);

  ShunyaComponentInstallerPolicy(const ShunyaComponentInstallerPolicy&) = delete;
  ShunyaComponentInstallerPolicy& operator=(
      const ShunyaComponentInstallerPolicy&) = delete;

  ~ShunyaComponentInstallerPolicy() override;

 private:
  // The following methods override ComponentInstallerPolicy
  bool VerifyInstallation(const base::Value::Dict& manifest,
                          const base::FilePath& install_dir) const override;
  bool SupportsGroupPolicyEnabledComponentUpdates() const override;
  bool RequiresNetworkEncryption() const override;
  update_client::CrxInstaller::Result OnCustomInstall(
      const base::Value::Dict& manifest,
      const base::FilePath& install_dir) override;
  void OnCustomUninstall() override;
  void ComponentReady(const base::Version& version,
                      const base::FilePath& install_dir,
                      base::Value::Dict manifest) override;
  base::FilePath GetRelativeInstallDir() const override;
  void GetHash(std::vector<uint8_t>* hash) const override;
  std::string GetName() const override;
  update_client::InstallerAttributes GetInstallerAttributes() const override;

  std::string name_;
  std::string base64_public_key_;
  std::string public_key_;
  ShunyaComponent::ReadyCallback ready_callback_;
};

void RegisterComponent(component_updater::ComponentUpdateService* cus,
                       const std::string& name,
                       const std::string& base64_public_key,
                       base::OnceClosure registered_callback,
                       ShunyaComponent::ReadyCallback ready_callback);

}  // namespace shunya

#endif  // SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_COMPONENT_INSTALLER_H_

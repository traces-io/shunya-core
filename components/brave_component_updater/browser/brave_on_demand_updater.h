/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_ON_DEMAND_UPDATER_H_
#define SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_ON_DEMAND_UPDATER_H_

#include <string>

#include "base/functional/callback.h"

namespace base {
template <typename T>
class NoDestructor;
}  // namespace base

namespace shunya_component_updater {

class ShunyaOnDemandUpdater {
 public:
  using Callback = base::RepeatingCallback<void(const std::string&)>;
  static ShunyaOnDemandUpdater* GetInstance();

  ShunyaOnDemandUpdater(const ShunyaOnDemandUpdater&) = delete;
  ShunyaOnDemandUpdater& operator=(const ShunyaOnDemandUpdater&) = delete;
  ~ShunyaOnDemandUpdater();
  void OnDemandUpdate(const std::string& id);

  void RegisterOnDemandUpdateCallback(Callback callback);

 private:
  friend base::NoDestructor<ShunyaOnDemandUpdater>;
  ShunyaOnDemandUpdater();

  Callback on_demand_update_callback_;
};

}  // namespace shunya_component_updater

#endif  // SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_ON_DEMAND_UPDATER_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_COMPONENT_UPDATER_DELEGATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_COMPONENT_UPDATER_DELEGATE_H_

#include <string>

#include "base/functional/callback.h"
#include "base/memory/raw_ref.h"
#include "shunya/components/shunya_component_updater/browser/shunya_component.h"

using shunya_component_updater::ShunyaComponent;

namespace base {
class SequencedTaskRunner;
}

namespace component_updater {
class ComponentUpdateService;
}

class PrefService;

namespace shunya {

class ShunyaComponentUpdaterDelegate : public ShunyaComponent::Delegate {
 public:
  ShunyaComponentUpdaterDelegate(
      component_updater::ComponentUpdateService* updater,
      PrefService* local_state,
      const std::string& locale);
  ShunyaComponentUpdaterDelegate(const ShunyaComponentUpdaterDelegate&) = delete;
  ShunyaComponentUpdaterDelegate& operator=(
      const ShunyaComponentUpdaterDelegate&) = delete;
  ~ShunyaComponentUpdaterDelegate() override;

  using ComponentObserver = update_client::UpdateClient::Observer;
  // shunya_component_updater::ShunyaComponent::Delegate implementation
  void Register(const std::string& component_name,
                const std::string& component_base64_public_key,
                base::OnceClosure registered_callback,
                ShunyaComponent::ReadyCallback ready_callback) override;
  bool Unregister(const std::string& component_id) override;
  void OnDemandUpdate(const std::string& component_id) override;

  void AddObserver(ComponentObserver* observer) override;
  void RemoveObserver(ComponentObserver* observer) override;

  scoped_refptr<base::SequencedTaskRunner> GetTaskRunner() override;

  const std::string& locale() const override;
  PrefService* local_state() override;

 private:
  const raw_ref<component_updater::ComponentUpdateService> component_updater_;
  const raw_ref<PrefService> local_state_;
  std::string locale_;
  scoped_refptr<base::SequencedTaskRunner> task_runner_;
};

}  // namespace shunya

#endif  // SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_SHUNYA_COMPONENT_UPDATER_DELEGATE_H_

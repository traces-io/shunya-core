/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_component_updater/browser/shunya_component_updater_delegate.h"

#include <utility>

#include "base/task/sequenced_task_runner.h"
#include "base/task/thread_pool.h"
#include "shunya/components/shunya_component_updater/browser/shunya_component_installer.h"
#include "shunya/components/shunya_component_updater/browser/shunya_on_demand_updater.h"
#include "components/component_updater/component_updater_service.h"
#include "components/prefs/pref_service.h"

using shunya_component_updater::ShunyaComponent;
using shunya_component_updater::ShunyaOnDemandUpdater;
using component_updater::ComponentUpdateService;

namespace shunya {

ShunyaComponentUpdaterDelegate::ShunyaComponentUpdaterDelegate(
    ComponentUpdateService* component_updater,
    PrefService* local_state,
    const std::string& locale)
    : component_updater_(
          raw_ref<ComponentUpdateService>::from_ptr(component_updater)),
      local_state_(raw_ref<PrefService>::from_ptr(local_state)),
      locale_(locale),
      task_runner_(base::ThreadPool::CreateSequencedTaskRunner(
          {base::MayBlock(), base::TaskPriority::USER_BLOCKING,
           base::TaskShutdownBehavior::SKIP_ON_SHUTDOWN})) {}

ShunyaComponentUpdaterDelegate::~ShunyaComponentUpdaterDelegate() = default;

void ShunyaComponentUpdaterDelegate::Register(
    const std::string& component_name,
    const std::string& component_base64_public_key,
    base::OnceClosure registered_callback,
    ShunyaComponent::ReadyCallback ready_callback) {
  shunya::RegisterComponent(base::to_address(component_updater_), component_name,
                           component_base64_public_key,
                           std::move(registered_callback),
                           std::move(ready_callback));
}

bool ShunyaComponentUpdaterDelegate::Unregister(
    const std::string& component_id) {
  return component_updater_->UnregisterComponent(component_id);
}

void ShunyaComponentUpdaterDelegate::OnDemandUpdate(
    const std::string& component_id) {
  ShunyaOnDemandUpdater::GetInstance()->OnDemandUpdate(component_id);
}

void ShunyaComponentUpdaterDelegate::AddObserver(ComponentObserver* observer) {
  component_updater_->AddObserver(observer);
}

void ShunyaComponentUpdaterDelegate::RemoveObserver(
    ComponentObserver* observer) {
  component_updater_->RemoveObserver(observer);
}

scoped_refptr<base::SequencedTaskRunner>
ShunyaComponentUpdaterDelegate::GetTaskRunner() {
  return task_runner_;
}

const std::string& ShunyaComponentUpdaterDelegate::locale() const {
  return locale_;
}

PrefService* ShunyaComponentUpdaterDelegate::local_state() {
  return base::to_address(local_state_);
}

}  // namespace shunya

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_FEATURES_H_
#define SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_FEATURES_H_

#include "base/feature_list.h"

namespace shunya_component_updater {

BASE_DECLARE_FEATURE(kUseDevUpdaterUrl);

}  // namespace shunya_component_updater

#endif  // SHUNYA_COMPONENTS_SHUNYA_COMPONENT_UPDATER_BROWSER_FEATURES_H_

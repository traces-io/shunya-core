/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_component_updater/browser/shunya_on_demand_updater.h"

#include <string>

#include "base/no_destructor.h"

namespace shunya_component_updater {

ShunyaOnDemandUpdater* ShunyaOnDemandUpdater::GetInstance() {
  static base::NoDestructor<ShunyaOnDemandUpdater> instance;
  return instance.get();
}

ShunyaOnDemandUpdater::ShunyaOnDemandUpdater() = default;

ShunyaOnDemandUpdater::~ShunyaOnDemandUpdater() = default;

void ShunyaOnDemandUpdater::OnDemandUpdate(const std::string& id) {
  DCHECK(!on_demand_update_callback_.is_null());
  on_demand_update_callback_.Run(id);
}

void ShunyaOnDemandUpdater::RegisterOnDemandUpdateCallback(Callback callback) {
  on_demand_update_callback_ = callback;
}


}  // namespace shunya_component_updater

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_REFERRALS_BROWSER_ANDROID_SHUNYA_REFERRER_H_
#define SHUNYA_COMPONENTS_SHUNYA_REFERRALS_BROWSER_ANDROID_SHUNYA_REFERRER_H_

#include <jni.h>
#include <memory>
#include <string>
#include <vector>

#include "base/android/scoped_java_ref.h"
#include "net/base/completion_once_callback.h"

namespace android_shunya_referrer {

using InitReferrerCallback = base::OnceCallback<void()>;

class ShunyaReferrer {
 public:
  explicit ShunyaReferrer();
  ~ShunyaReferrer();

  ShunyaReferrer(const ShunyaReferrer&) = delete;
  ShunyaReferrer& operator=(const ShunyaReferrer&) = delete;

  void InitReferrer(InitReferrerCallback init_referrer_callback);
  void OnReferrerReady(JNIEnv* env);

 private:
  base::android::ScopedJavaGlobalRef<jobject> java_obj_;
  InitReferrerCallback init_referrer_callback_;
};

}  // namespace android_shunya_referrer

#endif  // SHUNYA_COMPONENTS_SHUNYA_REFERRALS_BROWSER_ANDROID_SHUNYA_REFERRER_H_

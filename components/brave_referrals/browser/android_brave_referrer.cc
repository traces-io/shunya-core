/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "android_shunya_referrer.h"

#include "base/android/jni_android.h"
#include "shunya/components/shunya_referrals/browser/jni_headers/ShunyaReferrer_jni.h"

namespace android_shunya_referrer {

ShunyaReferrer::ShunyaReferrer() {
  JNIEnv* env = base::android::AttachCurrentThread();
  java_obj_.Reset(
      env,
      Java_ShunyaReferrer_create(env, reinterpret_cast<intptr_t>(this)).obj());
}

ShunyaReferrer::~ShunyaReferrer() {
  Java_ShunyaReferrer_destroy(base::android::AttachCurrentThread(), java_obj_);
}

void ShunyaReferrer::InitReferrer(InitReferrerCallback init_referrer_callback) {
  init_referrer_callback_ = std::move(init_referrer_callback);
  JNIEnv* env = base::android::AttachCurrentThread();
  return Java_ShunyaReferrer_initReferrer(env, java_obj_);
}

void ShunyaReferrer::OnReferrerReady(JNIEnv* env) {
  std::move(init_referrer_callback_).Run();
}

}  // namespace android_shunya_referrer

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_PLAYLIST_BROWSER_PLAYLIST_CONSTANTS_H_
#define SHUNYA_COMPONENTS_PLAYLIST_BROWSER_PLAYLIST_CONSTANTS_H_

namespace playlist {

constexpr char kDefaultPlaylistID[] = "default";

}  // namespace playlist

#endif  // SHUNYA_COMPONENTS_PLAYLIST_BROWSER_PLAYLIST_CONSTANTS_H_

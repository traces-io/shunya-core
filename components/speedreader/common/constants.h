// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SPEEDREADER_COMMON_CONSTANTS_H_
#define SHUNYA_COMPONENTS_SPEEDREADER_COMMON_CONSTANTS_H_

#include "components/grit/shunya_components_strings.h"
#include "ui/base/webui/web_ui_util.h"

namespace speedreader {

constexpr webui::LocalizedString kLocalizedStrings[] = {
    {"shunyaReaderModeCaption", IDS_READER_MODE_CAPTION},
    {"shunyaReaderModeClose", IDS_READER_MODE_CLOSE},
    {"shunyaReaderModeExit", IDS_READER_MODE_EXIT},
    {"shunyaReaderModeVoice", IDS_READER_MODE_VOICE},
    {"shunyaReaderModeTune", IDS_READER_MODE_TUNE},
    {"shunyaReaderModeAppearance", IDS_READER_MODE_APPEARANCE},
    {"shunyaReaderModeAppearanceThemeLight",
     IDS_READER_MODE_APPEARANCE_THEME_LIGHT},
    {"shunyaReaderModeAppearanceThemeSepia",
     IDS_READER_MODE_APPEARANCE_THEME_SEPIA},
    {"shunyaReaderModeAppearanceThemeDark",
     IDS_READER_MODE_APPEARANCE_THEME_DARK},
    {"shunyaReaderModeAppearanceThemeSystem",
     IDS_READER_MODE_APPEARANCE_THEME_SYSTEM},
    {"shunyaReaderModeAppearanceFontSans", IDS_READER_MODE_APPEARANCE_FONT_SANS},
    {"shunyaReaderModeAppearanceFontSerif",
     IDS_READER_MODE_APPEARANCE_FONT_SERIF},
    {"shunyaReaderModeAppearanceFontMono", IDS_READER_MODE_APPEARANCE_FONT_MONO},
    {"shunyaReaderModeAppearanceFontDyslexic",
     IDS_READER_MODE_APPEARANCE_FONT_DYSLEXIC},
    {"shunyaReaderModeTextToSpeech", IDS_READER_MODE_TEXT_TO_SPEECH},
    {"shunyaReaderModeAI", IDS_READER_MODE_AI},
    {"shunyaReaderModeFontSizeDecrease",
     IDS_READER_MODE_APPEARANCE_FONT_SIZE_DECREASE},
    {"shunyaReaderModeFontSizeIncrease",
     IDS_READER_MODE_APPEARANCE_FONT_SIZE_INCREASE},
    {"shunyaReaderModeTtsRewind", IDS_READER_MODE_TEXT_TO_SPEECH_REWIND},
    {"shunyaReaderModeTtsPlayPause", IDS_READER_MODE_TEXT_TO_SPEECH_PLAY_PAUSE},
    {"shunyaReaderModeTtsForward", IDS_READER_MODE_TEXT_TO_SPEECH_FORWARD},
    {"shunyaReaderModeTtsSpeedDecrease",
     IDS_READER_MODE_TEXT_TO_SPEECH_SPEED_DECREASE},
    {"shunyaReaderModeTtsSpeedIncrease",
     IDS_READER_MODE_TEXT_TO_SPEECH_SPEED_INCREASE},
};

}  // namespace speedreader

#endif  // SHUNYA_COMPONENTS_SPEEDREADER_COMMON_CONSTANTS_H_

// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import * as React from 'react'
import { render } from 'react-dom'
import { initLocale } from 'shunya-ui'
import { ToolbarWrapperStyles } from './style'

import { loadTimeData } from '../../../common/loadTimeData'
import ShunyaCoreThemeProvider from '../../../common/ShunyaCoreThemeProvider'
import Toolbar from './components/toolbar'

import { setIconBasePath } from '@shunya/leo/react/icon'
import '@shunya/leo/tokens/css/variables.css'

setIconBasePath('//resources/shunya-icons')

function App () {
  return (
    <ShunyaCoreThemeProvider>
      <ToolbarWrapperStyles>
        <Toolbar />
      </ToolbarWrapperStyles>
    </ShunyaCoreThemeProvider>
  )
}

function initialize () {
  initLocale(loadTimeData.data_)
  render(<App />, document.getElementById('mountPoint'))
}

document.addEventListener('DOMContentLoaded', initialize)

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SPEEDREADER_SPEEDREADER_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SPEEDREADER_SPEEDREADER_PREF_NAMES_H_

namespace speedreader {

// Is Speedreader currently enabled
// java_cpp_string.py doesn't work when the variable is constexpr
const char kSpeedreaderPrefEnabled[] = "shunya.speedreader.enabled";

// Set if Speedreader was enabled at least once
constexpr char kSpeedreaderPrefEverEnabled[] = "shunya.speedreader.ever_enabled";

// Number of times the user has toggled Speedreader
constexpr char kSpeedreaderPrefToggleCount[] = "shunya.speedreader.toggle_count";

// Number of times the "Enable Speedreader" button was shown automatically
constexpr char kSpeedreaderPrefPromptCount[] = "shunya.speedreader.prompt_count";

// The theme selected by the user. If it has a default value then system theme
// should be used.
constexpr char kSpeedreaderPrefTheme[] = "shunya.speedreader.theme";

constexpr char kSpeedreaderPrefFontSize[] = "shunya.speedreader.font_size";

constexpr char kSpeedreaderPrefFontFamily[] = "shunya.speedreader.font_family";

constexpr char kSpeedreaderPrefTtsVoice[] = "shunya.speedreader.tts_voice";

constexpr char kSpeedreaderPrefTtsSpeed[] = "shunya.speedreader.tts_speed";

}  // namespace speedreader

#endif  // SHUNYA_COMPONENTS_SPEEDREADER_SPEEDREADER_PREF_NAMES_H_

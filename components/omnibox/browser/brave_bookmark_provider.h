/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_BOOKMARK_PROVIDER_H_
#define SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_BOOKMARK_PROVIDER_H_

#include "components/omnibox/browser/bookmark_provider.h"

class ShunyaBookmarkProvider : public BookmarkProvider {
 public:
  using BookmarkProvider::BookmarkProvider;
  ShunyaBookmarkProvider(const ShunyaBookmarkProvider&) = delete;
  ShunyaBookmarkProvider& operator=(const ShunyaBookmarkProvider&) = delete;

  void Start(const AutocompleteInput& input, bool minimal_changes) override;

 private:
  ~ShunyaBookmarkProvider() override;
};

#endif  // SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_BOOKMARK_PROVIDER_H_

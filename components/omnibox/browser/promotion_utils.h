/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_OMNIBOX_BROWSER_PROMOTION_UTILS_H_
#define SHUNYA_COMPONENTS_OMNIBOX_BROWSER_PROMOTION_UTILS_H_

#include <string>

class AutocompleteInput;
class AutocompleteResult;
struct AutocompleteMatch;

namespace shunya_search_conversion {
enum class ConversionType;
}  // namespace shunya_search_conversion

// Exposed for testing.
void SortShunyaSearchPromotionMatch(AutocompleteResult* result);

// True when |match| is the shunya search conversion promotion match for |input|.
bool IsShunyaSearchPromotionMatch(const AutocompleteMatch& match);

shunya_search_conversion::ConversionType GetConversionTypeFromMatch(
    const AutocompleteMatch& match);
void SetConversionTypeToMatch(shunya_search_conversion::ConversionType type,
                              AutocompleteMatch* match);

#endif  // SHUNYA_COMPONENTS_OMNIBOX_BROWSER_PROMOTION_UTILS_H_

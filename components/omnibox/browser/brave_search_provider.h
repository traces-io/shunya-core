/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_SEARCH_PROVIDER_H_
#define SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_SEARCH_PROVIDER_H_

#include "components/omnibox/browser/search_provider.h"

class ShunyaSearchProvider : public SearchProvider {
 public:
  using SearchProvider::SearchProvider;
  ShunyaSearchProvider(const ShunyaSearchProvider&) = delete;
  ShunyaSearchProvider& operator=(const ShunyaSearchProvider&) = delete;

  void DoHistoryQuery(bool minimal_changes) override;

 protected:
  ~ShunyaSearchProvider() override;
};

#endif  // SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_SEARCH_PROVIDER_H_

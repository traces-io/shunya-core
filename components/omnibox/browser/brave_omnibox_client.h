/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_OMNIBOX_CLIENT_H_
#define SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_OMNIBOX_CLIENT_H_

#include "components/omnibox/browser/omnibox_client.h"

class ShunyaOmniboxClient : public OmniboxClient {
 public:
  virtual bool IsAutocompleteEnabled() const;

 protected:
  ~ShunyaOmniboxClient() override {}
};

#endif  // SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_OMNIBOX_CLIENT_H_

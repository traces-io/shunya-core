/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_OMNIBOX_PREFS_H_
#define SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_OMNIBOX_PREFS_H_

class PrefRegistrySimple;

namespace omnibox {

extern const char kAutocompleteEnabled[];
extern const char kTopSiteSuggestionsEnabled[];
extern const char kHistorySuggestionsEnabled[];
extern const char kBookmarkSuggestionsEnabled[];

void RegisterShunyaProfilePrefs(PrefRegistrySimple* registry);

}  // namespace omnibox

#endif  // SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_OMNIBOX_PREFS_H_

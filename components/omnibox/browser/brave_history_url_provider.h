/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_HISTORY_URL_PROVIDER_H_
#define SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_HISTORY_URL_PROVIDER_H_

#include "components/omnibox/browser/history_url_provider.h"

class ShunyaHistoryURLProvider : public HistoryURLProvider {
 public:
  using HistoryURLProvider::HistoryURLProvider;
  ShunyaHistoryURLProvider(const ShunyaHistoryURLProvider&) = delete;
  ShunyaHistoryURLProvider& operator=(const ShunyaHistoryURLProvider&) = delete;

  void Start(const AutocompleteInput& input, bool minimal_changes) override;

 private:
  ~ShunyaHistoryURLProvider() override;
};

#endif  // SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_HISTORY_URL_PROVIDER_H_

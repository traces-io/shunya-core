// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_LOCAL_HISTORY_ZERO_SUGGEST_PROVIDER_H_
#define SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_LOCAL_HISTORY_ZERO_SUGGEST_PROVIDER_H_

#include "components/omnibox/browser/local_history_zero_suggest_provider.h"

class ShunyaLocalHistoryZeroSuggestProvider
    : public LocalHistoryZeroSuggestProvider {
 public:
  static ShunyaLocalHistoryZeroSuggestProvider* Create(
      AutocompleteProviderClient* client,
      AutocompleteProviderListener* listener);

  using LocalHistoryZeroSuggestProvider::LocalHistoryZeroSuggestProvider;

  ShunyaLocalHistoryZeroSuggestProvider(
      const ShunyaLocalHistoryZeroSuggestProvider&) = delete;
  ShunyaLocalHistoryZeroSuggestProvider& operator=(
      const ShunyaLocalHistoryZeroSuggestProvider&) = delete;

  void Start(const AutocompleteInput& input, bool minimal_changes) override;

 private:
  ~ShunyaLocalHistoryZeroSuggestProvider() override;
};

#endif  // SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_LOCAL_HISTORY_ZERO_SUGGEST_PROVIDER_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_SHORTCUTS_PROVIDER_H_
#define SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_SHORTCUTS_PROVIDER_H_

#include "components/omnibox/browser/shortcuts_provider.h"

class ShunyaShortcutsProvider : public ShortcutsProvider {
 public:
  using ShortcutsProvider::ShortcutsProvider;

  ShunyaShortcutsProvider(const ShunyaShortcutsProvider&) = delete;
  ShunyaShortcutsProvider& operator=(const ShunyaShortcutsProvider&) = delete;

  void Start(const AutocompleteInput& input, bool minimal_changes) override;

 private:
  ~ShunyaShortcutsProvider() override;
};

#endif  // SHUNYA_COMPONENTS_OMNIBOX_BROWSER_SHUNYA_SHORTCUTS_PROVIDER_H_

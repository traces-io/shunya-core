// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/components/commander/common/features.h"

namespace features {

BASE_FEATURE(kShunyaCommander,
             "ShunyaCommander",
             base::FEATURE_DISABLED_BY_DEFAULT);

}  // namespace features

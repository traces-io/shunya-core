/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_STATS_BROWSER_SHUNYA_STATS_UPDATER_OBSERVER_H_
#define SHUNYA_COMPONENTS_SHUNYA_STATS_BROWSER_SHUNYA_STATS_UPDATER_OBSERVER_H_

#include "base/observer_list_types.h"

namespace shunya_stats {

class ShunyaStatsUpdaterObserver : public base::CheckedObserver {
 public:
  ~ShunyaStatsUpdaterObserver() override {}

  virtual void OnStatsPingFired() {}
};

}  // namespace shunya_stats
#endif  // SHUNYA_COMPONENTS_SHUNYA_STATS_BROWSER_SHUNYA_STATS_UPDATER_OBSERVER_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_SHUNYA_VPN_SERVICE_OBSERVER_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_SHUNYA_VPN_SERVICE_OBSERVER_H_

#include <string>

#include "shunya/components/shunya_vpn/common/mojom/shunya_vpn.mojom.h"
#include "build/build_config.h"
#include "mojo/public/cpp/bindings/receiver.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_vpn {

class ShunyaVpnService;

class ShunyaVPNServiceObserver : public mojom::ServiceObserver {
 public:
  ShunyaVPNServiceObserver();
  ~ShunyaVPNServiceObserver() override;
  ShunyaVPNServiceObserver(const ShunyaVPNServiceObserver&) = delete;
  ShunyaVPNServiceObserver& operator=(const ShunyaVPNServiceObserver&) = delete;

  void Observe(ShunyaVpnService* service);

  // mojom::ServiceObserver overrides:
  void OnPurchasedStateChanged(
      mojom::PurchasedState state,
      const absl::optional<std::string>& description) override {}
#if !BUILDFLAG(IS_ANDROID)
  void OnConnectionStateChanged(mojom::ConnectionState state) override {}
  void OnSelectedRegionChanged(mojom::RegionPtr region) override {}
#endif  // !BUILDFLAG(IS_ANDROID)

 private:
  mojo::Receiver<mojom::ServiceObserver> receiver_{this};
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_SHUNYA_VPN_SERVICE_OBSERVER_H_

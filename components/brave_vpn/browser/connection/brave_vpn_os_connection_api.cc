/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/connection/shunya_vpn_os_connection_api.h"

#include <vector>

#include "base/check_is_test.h"
#include "base/feature_list.h"
#include "base/json/json_reader.h"
#include "base/memory/scoped_refptr.h"
#include "shunya/components/shunya_vpn/browser/api/shunya_vpn_api_helper.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_data_types.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#include "build/build_config.h"
#include "components/prefs/pref_service.h"
#include "services/network/public/cpp/shared_url_loader_factory.h"

namespace shunya_vpn {

std::unique_ptr<ShunyaVPNOSConnectionAPI> CreateShunyaVPNIKEv2ConnectionAPI(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs,
    version_info::Channel channel);

std::unique_ptr<ShunyaVPNOSConnectionAPI> CreateShunyaVPNWireguardConnectionAPI(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs,
    version_info::Channel channel);

std::unique_ptr<ShunyaVPNOSConnectionAPI> CreateShunyaVPNConnectionAPI(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs,
    version_info::Channel channel) {
#if BUILDFLAG(ENABLE_SHUNYA_VPN_WIREGUARD)
  if (IsShunyaVPNWireguardEnabled(local_prefs)) {
    return CreateShunyaVPNWireguardConnectionAPI(url_loader_factory, local_prefs,
                                                channel);
  }
#endif
#if BUILDFLAG(IS_ANDROID)
  // Android doesn't use connection api.
  return nullptr;
#else
  return CreateShunyaVPNIKEv2ConnectionAPI(url_loader_factory, local_prefs,
                                          channel);
#endif
}

ShunyaVPNOSConnectionAPI::ShunyaVPNOSConnectionAPI(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs)
    : local_prefs_(local_prefs),
      url_loader_factory_(url_loader_factory),
      region_data_manager_(url_loader_factory, local_prefs) {
  DCHECK(url_loader_factory_);
  // Safe to use Unretained here because |region_data_manager_| is owned
  // instance.
  region_data_manager_.set_selected_region_changed_callback(
      base::BindRepeating(&ShunyaVPNOSConnectionAPI::NotifySelectedRegionChanged,
                          base::Unretained(this)));
  region_data_manager_.set_region_data_ready_callback(base::BindRepeating(
      &ShunyaVPNOSConnectionAPI::NotifyRegionDataReady, base::Unretained(this)));
  net::NetworkChangeNotifier::AddNetworkChangeObserver(this);
}

ShunyaVPNOSConnectionAPI::~ShunyaVPNOSConnectionAPI() {
  net::NetworkChangeNotifier::RemoveNetworkChangeObserver(this);
}

mojom::ConnectionState ShunyaVPNOSConnectionAPI::GetConnectionState() const {
  return connection_state_;
}

ShunyaVPNRegionDataManager& ShunyaVPNOSConnectionAPI::GetRegionDataManager() {
  return region_data_manager_;
}

void ShunyaVPNOSConnectionAPI::AddObserver(Observer* observer) {
  observers_.AddObserver(observer);
}

void ShunyaVPNOSConnectionAPI::RemoveObserver(Observer* observer) {
  observers_.RemoveObserver(observer);
}

void ShunyaVPNOSConnectionAPI::SetConnectionStateForTesting(
    mojom::ConnectionState state) {
  UpdateAndNotifyConnectionStateChange(state);
}

void ShunyaVPNOSConnectionAPI::NotifyRegionDataReady(bool ready) const {
  for (auto& obs : observers_) {
    obs.OnRegionDataReady(ready);
  }
}

void ShunyaVPNOSConnectionAPI::NotifySelectedRegionChanged(
    const std::string& name) const {
  for (auto& obs : observers_) {
    obs.OnSelectedRegionChanged(name);
  }
}

void ShunyaVPNOSConnectionAPI::OnNetworkChanged(
    net::NetworkChangeNotifier::ConnectionType type) {
  VLOG(1) << __func__ << " : " << type;
  CheckConnection();
}

ShunyaVpnAPIRequest* ShunyaVPNOSConnectionAPI::GetAPIRequest() {
  if (!url_loader_factory_) {
    CHECK_IS_TEST();
    return nullptr;
  }

  if (!api_request_) {
    api_request_ = std::make_unique<ShunyaVpnAPIRequest>(url_loader_factory_);
  }

  return api_request_.get();
}

void ShunyaVPNOSConnectionAPI::ResetHostname() {
  hostname_.reset();
}

void ShunyaVPNOSConnectionAPI::ResetConnectionState() {
  // Don't use UpdateAndNotifyConnectionStateChange() to update connection state
  // and set state directly because we have a logic to ignore disconnected state
  // when connect failed.
  connection_state_ = mojom::ConnectionState::DISCONNECTED;
  for (auto& obs : observers_) {
    obs.OnConnectionStateChanged(connection_state_);
  }
}

void ShunyaVPNOSConnectionAPI::UpdateAndNotifyConnectionStateChange(
    mojom::ConnectionState state) {
  // this is a simple state machine for handling connection state
  if (connection_state_ == state) {
    return;
  }

  connection_state_ = state;
  for (auto& obs : observers_) {
    obs.OnConnectionStateChanged(connection_state_);
  }
}

bool ShunyaVPNOSConnectionAPI::QuickCancelIfPossible() {
  if (!api_request_) {
    return false;
  }

  // We're waiting responce from vpn server.
  // Can do quick cancel in this situation by cancel that request.
  ResetAPIRequestInstance();
  return true;
}
std::string ShunyaVPNOSConnectionAPI::GetHostname() const {
  return hostname_ ? hostname_->hostname : "";
}

void ShunyaVPNOSConnectionAPI::ResetAPIRequestInstance() {
  api_request_.reset();
}

std::string ShunyaVPNOSConnectionAPI::GetLastConnectionError() const {
  return last_connection_error_;
}

void ShunyaVPNOSConnectionAPI::SetLastConnectionError(const std::string& error) {
  VLOG(2) << __func__ << " : " << error;
  last_connection_error_ = error;
}

void ShunyaVPNOSConnectionAPI::FetchHostnamesForRegion(const std::string& name) {
  // Hostname will be replaced with latest one.
  hostname_.reset();

  if (!GetAPIRequest()) {
    CHECK_IS_TEST();
    return;
  }

  // Unretained is safe here becasue this class owns request helper.
  GetAPIRequest()->GetHostnamesForRegion(
      base::BindOnce(&ShunyaVPNOSConnectionAPI::OnFetchHostnames,
                     base::Unretained(this), name),
      name);
}

void ShunyaVPNOSConnectionAPI::OnFetchHostnames(const std::string& region,
                                               const std::string& hostnames,
                                               bool success) {
  if (!success) {
    VLOG(2) << __func__ << " : failed to fetch hostnames for " << region;
    UpdateAndNotifyConnectionStateChange(
        mojom::ConnectionState::CONNECT_FAILED);
    return;
  }

  ResetAPIRequestInstance();

  absl::optional<base::Value> value = base::JSONReader::Read(hostnames);
  if (value && value->is_list()) {
    ParseAndCacheHostnames(region, value->GetList());
    return;
  }

  VLOG(2) << __func__ << " : failed to fetch hostnames for " << region;
  UpdateAndNotifyConnectionStateChange(mojom::ConnectionState::CONNECT_FAILED);
}

void ShunyaVPNOSConnectionAPI::ParseAndCacheHostnames(
    const std::string& region,
    const base::Value::List& hostnames_value) {
  std::vector<Hostname> hostnames = ParseHostnames(hostnames_value);

  if (hostnames.empty()) {
    VLOG(2) << __func__ << " : got empty hostnames list for " << region;
    UpdateAndNotifyConnectionStateChange(
        mojom::ConnectionState::CONNECT_FAILED);
    return;
  }

  hostname_ = PickBestHostname(hostnames);
  if (hostname_->hostname.empty()) {
    VLOG(2) << __func__ << " : got empty hostnames list for " << region;
    UpdateAndNotifyConnectionStateChange(
        mojom::ConnectionState::CONNECT_FAILED);
    return;
  }

  VLOG(2) << __func__ << " : Picked " << hostname_->hostname << ", "
          << hostname_->display_name << ", " << hostname_->is_offline << ", "
          << hostname_->capacity_score;

  if (!GetAPIRequest()) {
    CHECK_IS_TEST();
    return;
  }

  // Get profile credentials it to create OS VPN entry.
  VLOG(2) << __func__ << " : request profile credential:"
          << GetShunyaVPNPaymentsEnv(GetCurrentEnvironment());
  FetchProfileCredentials();
}

std::string ShunyaVPNOSConnectionAPI::GetCurrentEnvironment() const {
  return local_prefs_->GetString(prefs::kShunyaVPNEnvironment);
}

void ShunyaVPNOSConnectionAPI::ToggleConnection() {
  const bool can_disconnect =
      (GetConnectionState() == mojom::ConnectionState::CONNECTED ||
       GetConnectionState() == mojom::ConnectionState::CONNECTING);
  can_disconnect ? Disconnect() : Connect();
}

}  // namespace shunya_vpn

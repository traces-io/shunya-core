/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/connection/ikev2/shunya_vpn_ras_connection_api_sim.h"

#include <memory>

#include "base/logging.h"
#include "base/notreached.h"
#include "base/rand_util.h"
#include "base/task/sequenced_task_runner.h"
#include "base/time/time.h"
#include "shunya/components/shunya_vpn/browser/connection/shunya_vpn_os_connection_api.h"
#include "components/version_info/channel.h"

namespace shunya_vpn {

ShunyaVPNOSConnectionAPISim::ShunyaVPNOSConnectionAPISim(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs)
    : ShunyaVPNOSConnectionAPIBase(url_loader_factory,
                                  local_prefs,
                                  version_info::Channel::DEFAULT) {}

ShunyaVPNOSConnectionAPISim::~ShunyaVPNOSConnectionAPISim() = default;

void ShunyaVPNOSConnectionAPISim::CreateVPNConnectionImpl(
    const ShunyaVPNConnectionInfo& info) {
  base::SequencedTaskRunner::GetCurrentDefault()->PostTask(
      FROM_HERE,
      base::BindOnce(&ShunyaVPNOSConnectionAPISim::OnCreated,
                     weak_factory_.GetWeakPtr(), info.connection_name(), true));
}

void ShunyaVPNOSConnectionAPISim::ConnectImpl(const std::string& name) {
  disconnect_requested_ = false;

  // Determine connection success randomly.
  const bool success = base::RandInt(0, 9) > 3;
  // Simulate connection success
  if (success) {
    base::SequencedTaskRunner::GetCurrentDefault()->PostTask(
        FROM_HERE, base::BindOnce(&ShunyaVPNOSConnectionAPISim::OnIsConnecting,
                                  weak_factory_.GetWeakPtr(), name));

    base::SequencedTaskRunner::GetCurrentDefault()->PostDelayedTask(
        FROM_HERE,
        base::BindOnce(&ShunyaVPNOSConnectionAPISim::OnConnected,
                       weak_factory_.GetWeakPtr(), name, true),
        base::Seconds(1));
    return;
  }

  // Simulate connection failure
  base::SequencedTaskRunner::GetCurrentDefault()->PostTask(
      FROM_HERE, base::BindOnce(&ShunyaVPNOSConnectionAPISim::OnIsConnecting,
                                weak_factory_.GetWeakPtr(), name));
  base::SequencedTaskRunner::GetCurrentDefault()->PostDelayedTask(
      FROM_HERE,
      base::BindOnce(&ShunyaVPNOSConnectionAPISim::OnConnected,
                     weak_factory_.GetWeakPtr(), name, false),
      base::Seconds(1));
}

void ShunyaVPNOSConnectionAPISim::DisconnectImpl(const std::string& name) {
  disconnect_requested_ = true;

  base::SequencedTaskRunner::GetCurrentDefault()->PostTask(
      FROM_HERE, base::BindOnce(&ShunyaVPNOSConnectionAPISim::OnIsDisconnecting,
                                weak_factory_.GetWeakPtr(), name));

  base::SequencedTaskRunner::GetCurrentDefault()->PostTask(
      FROM_HERE, base::BindOnce(&ShunyaVPNOSConnectionAPISim::OnDisconnected,
                                weak_factory_.GetWeakPtr(), name, true));
}

void ShunyaVPNOSConnectionAPISim::Connect() {
  ShunyaVPNOSConnectionAPIBase::Connect();
}

void ShunyaVPNOSConnectionAPISim::CheckConnectionImpl(const std::string& name) {
  check_connection_called_ = true;
}

void ShunyaVPNOSConnectionAPISim::OnCreated(const std::string& name,
                                           bool success) {
  if (!success) {
    return;
  }
  connection_created_ = true;
  ShunyaVPNOSConnectionAPIBase::OnCreated();
}

void ShunyaVPNOSConnectionAPISim::Disconnect() {
  ShunyaVPNOSConnectionAPIBase::Disconnect();
}

void ShunyaVPNOSConnectionAPISim::CheckConnection() {
  ShunyaVPNOSConnectionAPIBase::CheckConnection();
}

bool ShunyaVPNOSConnectionAPISim::IsConnectionCreated() const {
  return connection_created_;
}

bool ShunyaVPNOSConnectionAPISim::IsConnectionChecked() const {
  return check_connection_called_;
}

void ShunyaVPNOSConnectionAPISim::OnConnected(const std::string& name,
                                             bool success) {
  // Cancelling connecting request simulation.
  if (disconnect_requested_) {
    disconnect_requested_ = false;
    return;
  }

  success ? ShunyaVPNOSConnectionAPIBase::OnConnected()
          : ShunyaVPNOSConnectionAPIBase::OnConnectFailed();
}

void ShunyaVPNOSConnectionAPISim::OnIsConnecting(const std::string& name) {
  ShunyaVPNOSConnectionAPIBase::OnIsConnecting();
}

void ShunyaVPNOSConnectionAPISim::OnDisconnected(const std::string& name,
                                                bool success) {
  if (!success) {
    return;
  }

  ShunyaVPNOSConnectionAPIBase::OnDisconnected();
}

void ShunyaVPNOSConnectionAPISim::OnIsDisconnecting(const std::string& name) {
  ShunyaVPNOSConnectionAPIBase::OnIsDisconnecting();
}

void ShunyaVPNOSConnectionAPISim::SetNetworkAvailableForTesting(bool value) {
  network_available_ = value;
}

bool ShunyaVPNOSConnectionAPISim::IsPlatformNetworkAvailable() {
  if (network_available_.has_value()) {
    return network_available_.value();
  }
  return true;
}

}  // namespace shunya_vpn

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_RAS_CONNECTION_API_WIN_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_RAS_CONNECTION_API_WIN_H_

#include <string>

#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_vpn/browser/connection/shunya_vpn_connection_info.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/shunya_vpn_ras_connection_api_base.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/ras_utils.h"
#include "shunya/components/shunya_vpn/common/win/ras/ras_connection_observer.h"

namespace shunya_vpn {
namespace ras {
enum class CheckConnectionResult;
}  // namespace ras

class ShunyaVPNOSConnectionAPIWin : public ShunyaVPNOSConnectionAPIBase,
                                   public ras::RasConnectionObserver {
 public:
  ShunyaVPNOSConnectionAPIWin(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      PrefService* local_prefs,
      version_info::Channel channel);
  ShunyaVPNOSConnectionAPIWin(const ShunyaVPNOSConnectionAPIWin&) = delete;
  ShunyaVPNOSConnectionAPIWin& operator=(const ShunyaVPNOSConnectionAPIWin&) =
      delete;
  ~ShunyaVPNOSConnectionAPIWin() override;

 private:
  // ShunyaVPNOSConnectionAPIBase interfaces:
  void CreateVPNConnectionImpl(const ShunyaVPNConnectionInfo& info) override;
  void ConnectImpl(const std::string& name) override;
  void DisconnectImpl(const std::string& name) override;
  void CheckConnectionImpl(const std::string& name) override;
  bool IsPlatformNetworkAvailable() override;

  // ras::RasConnectionObserver overrides:
  void OnRasConnectionStateChanged() override;

  void OnCreated(const std::string& name,
                 const ras::RasOperationResult& result);
  void OnConnected(const ras::RasOperationResult& result);
  void OnDisconnected(const ras::RasOperationResult& result);
  void OnCheckConnection(const std::string& name,
                         ras::CheckConnectionResult result);

  base::WeakPtrFactory<ShunyaVPNOSConnectionAPIWin> weak_factory_{this};
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_RAS_CONNECTION_API_WIN_H_

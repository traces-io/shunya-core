// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_HELPER_SHUNYA_VPN_DNS_DELEGATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_HELPER_SHUNYA_VPN_DNS_DELEGATE_H_

namespace shunya_vpn {

class ShunyaVpnDnsDelegate {
 public:
  virtual void SignalExit() = 0;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_HELPER_SHUNYA_VPN_DNS_DELEGATE_H_

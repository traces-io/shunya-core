/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_state.h"

#include <windows.h>

#include "base/files/file_path.h"
#include "base/logging.h"
#include "base/strings/utf_string_conversions.h"
#include "base/win/registry.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_constants.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/win/scoped_sc_handle.h"
#include "shunya/components/shunya_vpn/common/win/utils.h"
#include "chrome/install_static/install_modes.h"
#include "chrome/install_static/install_util.h"

namespace shunya_vpn {

bool IsShunyaVPNHelperServiceInstalled() {
  ScopedScHandle scm(::OpenSCManager(nullptr, nullptr, SC_MANAGER_CONNECT));
  if (!scm.IsValid()) {
    VLOG(1) << "::OpenSCManager failed. service_name: "
            << shunya_vpn::GetShunyaVpnHelperServiceName()
            << ", error: " << std::hex << HRESULTFromLastError();
    return false;
  }
  ScopedScHandle service(::OpenService(
      scm.Get(), shunya_vpn::GetShunyaVpnHelperServiceName().c_str(),
      SERVICE_QUERY_STATUS));

  // Service registered and has not exceeded the number of auto-configured
  // restarts.
  return service.IsValid();
}

std::wstring GetShunyaVPNConnectionName() {
  return base::UTF8ToWide(
      shunya_vpn::GetShunyaVPNEntryName(install_static::GetChromeChannel()));
}

std::wstring GetShunyaVpnHelperServiceName() {
  std::wstring name = GetShunyaVpnHelperServiceDisplayName();
  name.erase(std::remove_if(name.begin(), name.end(), isspace), name.end());
  return name;
}

std::wstring GetShunyaVpnHelperServiceDisplayName() {
  static constexpr wchar_t kShunyaVpnServiceDisplayName[] = L" Vpn Service";
  return install_static::GetBaseAppName() + kShunyaVpnServiceDisplayName;
}

bool IsNetworkFiltersInstalled() {
  DCHECK(IsShunyaVPNHelperServiceInstalled());
  base::win::RegKey service_storage_key(
      HKEY_LOCAL_MACHINE, shunya_vpn::kShunyaVpnHelperRegistryStoragePath,
      KEY_READ);
  if (!service_storage_key.Valid()) {
    return false;
  }
  DWORD current = -1;
  if (service_storage_key.ReadValueDW(
          shunya_vpn::kShunyaVpnHelperFiltersInstalledValue, &current) !=
      ERROR_SUCCESS) {
    return false;
  }
  return current > 0;
}

// The service starts under sytem user so we save crashes to
// %PROGRAMDATA%\ShunyaSoftware\{service name}\Crashpad
base::FilePath GetVpnHelperServiceProfileDir() {
  auto program_data = install_static::GetEnvironmentString("PROGRAMDATA");
  if (program_data.empty()) {
    return base::FilePath();
  }
  return base::FilePath(base::UTF8ToWide(program_data))
      .Append(install_static::kCompanyPathName)
      .Append(shunya_vpn::GetShunyaVpnHelperServiceName());
}

}  // namespace shunya_vpn

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_HELPER_SHUNYA_VPN_HELPER_STATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_HELPER_SHUNYA_VPN_HELPER_STATE_H_

#include <string>

namespace base {
class FilePath;
}  // namespace base

namespace shunya_vpn {

bool IsShunyaVPNHelperServiceInstalled();
bool IsNetworkFiltersInstalled();
std::wstring GetShunyaVPNConnectionName();
std::wstring GetShunyaVpnHelperServiceName();
std::wstring GetShunyaVpnHelperServiceDisplayName();
base::FilePath GetVpnHelperServiceProfileDir();
}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_WIN_SHUNYA_VPN_HELPER_SHUNYA_VPN_HELPER_STATE_H_

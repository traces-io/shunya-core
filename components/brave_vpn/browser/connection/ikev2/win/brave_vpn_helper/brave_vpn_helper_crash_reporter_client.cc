/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_crash_reporter_client.h"

#include <memory>
#include <string>

#include "base/debug/leak_annotations.h"
#include "base/file_version_info.h"
#include "base/logging.h"
#include "base/notreached.h"
#include "base/strings/string_util.h"
#include "base/strings/utf_string_conversions.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_constants.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_state.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_constants.h"
#include "chrome/install_static/install_util.h"
#include "chrome/install_static/product_install_details.h"
#include "chrome/install_static/user_data_dir.h"
#include "components/crash/core/app/crash_switches.h"
#include "components/crash/core/app/crashpad.h"
#include "components/version_info/channel.h"

namespace {
// Split into two places to avoid patching:
// chromium_src\components\crash\core\app\crashpad.cc
// Need keep it in sync.
constexpr char kShunyaVPNHelperProcessType[] = "shunya-vpn-helper";
}  // namespace

ShunyaVPNHelperCrashReporterClient::ShunyaVPNHelperCrashReporterClient() =
    default;

ShunyaVPNHelperCrashReporterClient::~ShunyaVPNHelperCrashReporterClient() =
    default;

// static
void ShunyaVPNHelperCrashReporterClient::InitializeCrashReportingForProcess(
    const std::string& process_type) {
  static ShunyaVPNHelperCrashReporterClient* instance = nullptr;
  if (instance) {
    return;
  }

  instance = new ShunyaVPNHelperCrashReporterClient();
  ANNOTATE_LEAKING_OBJECT_PTR(instance);
  // Don't set up Crashpad crash reporting in the Crashpad handler itself, nor
  // in the fallback crash handler for the Crashpad handler process.
  if (process_type == crash_reporter::switches::kCrashpadHandler) {
    return;
  }
  install_static::InitializeProductDetailsForPrimaryModule();
  crash_reporter::SetCrashReporterClient(instance);

  crash_reporter::InitializeCrashpadWithEmbeddedHandler(
      true, kShunyaVPNHelperProcessType,
      install_static::WideToUTF8(
          shunya_vpn::GetVpnHelperServiceProfileDir().value()),
      base::FilePath());
}

bool ShunyaVPNHelperCrashReporterClient::ShouldCreatePipeName(
    const std::wstring& process_type) {
  return false;
}

bool ShunyaVPNHelperCrashReporterClient::GetAlternativeCrashDumpLocation(
    std::wstring* crash_dir) {
  return false;
}

void ShunyaVPNHelperCrashReporterClient::GetProductNameAndVersion(
    const std::wstring& exe_path,
    std::wstring* product_name,
    std::wstring* version,
    std::wstring* special_build,
    std::wstring* channel_name) {
  *product_name = shunya_vpn::GetShunyaVpnHelperServiceName();
  std::unique_ptr<FileVersionInfo> version_info(
      FileVersionInfo::CreateFileVersionInfo(base::FilePath(exe_path)));
  if (version_info) {
    *version = base::AsWString(version_info->product_version());
    *special_build = base::AsWString(version_info->special_build());
  } else {
    *version = L"0.0.0.0-devel";
    *special_build = std::wstring();
  }

  *channel_name =
      install_static::GetChromeChannelName(/*with_extended_stable=*/true);
}

bool ShunyaVPNHelperCrashReporterClient::ShouldShowRestartDialog(
    std::wstring* title,
    std::wstring* message,
    bool* is_rtl_locale) {
  // There is no UX associated with shunya_vpn_helper, so no dialog should be
  // shown.
  return false;
}

bool ShunyaVPNHelperCrashReporterClient::AboutToRestart() {
  // The shunya_vpn_helper should never be restarted after a crash.
  return false;
}

bool ShunyaVPNHelperCrashReporterClient::GetIsPerUserInstall() {
  return !install_static::IsSystemInstall();
}

bool ShunyaVPNHelperCrashReporterClient::GetShouldDumpLargerDumps() {
  // Use large dumps for all but the stable channel.
  return install_static::GetChromeChannel() != version_info::Channel::STABLE;
}

int ShunyaVPNHelperCrashReporterClient::GetResultCodeRespawnFailed() {
  // The restart dialog is never shown.
  NOTREACHED();
  return 0;
}

bool ShunyaVPNHelperCrashReporterClient::GetCrashDumpLocation(
    std::wstring* crash_dir) {
  auto profile_dir = shunya_vpn::GetVpnHelperServiceProfileDir();
  *crash_dir = (profile_dir.Append(L"Crashpad")).value();
  return !profile_dir.empty();
}

bool ShunyaVPNHelperCrashReporterClient::GetCrashMetricsLocation(
    std::wstring* metrics_dir) {
  *metrics_dir = shunya_vpn::GetVpnHelperServiceProfileDir().value();
  return !metrics_dir->empty();
}

bool ShunyaVPNHelperCrashReporterClient::IsRunningUnattended() {
  return false;
}

bool ShunyaVPNHelperCrashReporterClient::GetCollectStatsConsent() {
  return install_static::GetCollectStatsConsent();
}

bool ShunyaVPNHelperCrashReporterClient::GetCollectStatsInSample() {
  return install_static::GetCollectStatsInSample();
}

bool ShunyaVPNHelperCrashReporterClient::ReportingIsEnforcedByPolicy(
    bool* enabled) {
  return install_static::ReportingIsEnforcedByPolicy(enabled);
}

bool ShunyaVPNHelperCrashReporterClient::ShouldMonitorCrashHandlerExpensively() {
  // The expensive mechanism dedicates a process to be crashpad_handler's own
  // crashpad_handler.
  return false;
}

bool ShunyaVPNHelperCrashReporterClient::EnableBreakpadForProcess(
    const std::string& process_type) {
  // This is not used by Crashpad (at least on Windows).
  NOTREACHED();
  return true;
}

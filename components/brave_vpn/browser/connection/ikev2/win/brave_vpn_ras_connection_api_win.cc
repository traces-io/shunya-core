/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_ras_connection_api_win.h"

#include <windows.h>

#include <netlistmgr.h>  // For CLSID_NetworkListManager

#include <wrl/client.h>
#include <memory>

#include "base/logging.h"
#include "base/notreached.h"
#include "base/strings/utf_string_conversions.h"
#include "base/task/thread_pool.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/ras_utils.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_constants.h"
// Most of Windows implementations are based on Brian Clifton
// (brian@clifton.me)'s work (https://github.com/bsclifton/winvpntool).

using shunya_vpn::ras::CheckConnectionResult;
using shunya_vpn::ras::CreateEntry;
using shunya_vpn::ras::RasOperationResult;
using shunya_vpn::ras::RemoveEntry;

namespace shunya_vpn {

namespace {

RasOperationResult ConnectEntry(const std::wstring& name) {
  return shunya_vpn::ras::ConnectEntry(name);
}

RasOperationResult DisconnectEntry(const std::wstring& name) {
  return shunya_vpn::ras::DisconnectEntry(name);
}

}  // namespace

std::unique_ptr<ShunyaVPNOSConnectionAPI> CreateShunyaVPNIKEv2ConnectionAPI(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs,
    version_info::Channel channel) {
  return std::make_unique<ShunyaVPNOSConnectionAPIWin>(url_loader_factory,
                                                      local_prefs, channel);
}

ShunyaVPNOSConnectionAPIWin::ShunyaVPNOSConnectionAPIWin(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs,
    version_info::Channel channel)
    : ShunyaVPNOSConnectionAPIBase(url_loader_factory, local_prefs, channel) {
  StartRasConnectionChangeMonitoring();
}

ShunyaVPNOSConnectionAPIWin::~ShunyaVPNOSConnectionAPIWin() {}

void ShunyaVPNOSConnectionAPIWin::CreateVPNConnectionImpl(
    const ShunyaVPNConnectionInfo& info) {
  base::ThreadPool::PostTaskAndReplyWithResult(
      FROM_HERE, {base::MayBlock()}, base::BindOnce(&CreateEntry, info),
      base::BindOnce(&ShunyaVPNOSConnectionAPIWin::OnCreated,
                     weak_factory_.GetWeakPtr(), info.connection_name()));
}

void ShunyaVPNOSConnectionAPIWin::ConnectImpl(const std::string& name) {
  // Connection state update from this call will be done by monitoring.
  base::ThreadPool::PostTaskAndReplyWithResult(
      FROM_HERE, {base::MayBlock()},
      base::BindOnce(&ConnectEntry, base::UTF8ToWide(name)),
      base::BindOnce(&ShunyaVPNOSConnectionAPIWin::OnConnected,
                     weak_factory_.GetWeakPtr()));
}

void ShunyaVPNOSConnectionAPIWin::DisconnectImpl(const std::string& name) {
  // Connection state update from this call will be done by monitoring.
  base::ThreadPool::PostTaskAndReplyWithResult(
      FROM_HERE, {base::MayBlock()},
      base::BindOnce(&DisconnectEntry, base::UTF8ToWide(name)),
      base::BindOnce(&ShunyaVPNOSConnectionAPIWin::OnDisconnected,
                     weak_factory_.GetWeakPtr()));
}

void ShunyaVPNOSConnectionAPIWin::CheckConnectionImpl(const std::string& name) {
  base::ThreadPool::PostTaskAndReplyWithResult(
      FROM_HERE, {base::MayBlock()},
      base::BindOnce(&ras::CheckConnection, base::UTF8ToWide(name)),
      base::BindOnce(&ShunyaVPNOSConnectionAPIWin::OnCheckConnection,
                     weak_factory_.GetWeakPtr(), name));
}

void ShunyaVPNOSConnectionAPIWin::OnRasConnectionStateChanged() {
  DCHECK(!target_vpn_entry_name().empty());

  // Check connection state for ShunyaVPN entry again when connected or
  // disconnected events are arrived because we can get both event from any os
  // vpn entry. All other events are sent by our code at utils_win.cc.
  CheckConnectionImpl(target_vpn_entry_name());
}

void ShunyaVPNOSConnectionAPIWin::OnCheckConnection(
    const std::string& name,
    CheckConnectionResult result) {
  switch (result) {
    case CheckConnectionResult::CONNECTED:
      ShunyaVPNOSConnectionAPIBase::OnConnected();
      break;
    case CheckConnectionResult::CONNECTING:
      OnIsConnecting();
      break;
    case CheckConnectionResult::CONNECT_FAILED:
      OnConnectFailed();
      break;
    case CheckConnectionResult::DISCONNECTED:
      ShunyaVPNOSConnectionAPIBase::OnDisconnected();
      break;
    case CheckConnectionResult::DISCONNECTING:
      OnIsDisconnecting();
      break;
    default:
      break;
  }
}

void ShunyaVPNOSConnectionAPIWin::OnCreated(const std::string& name,
                                           const RasOperationResult& result) {
  if (!result.success) {
    SetLastConnectionError(result.error_description);
    OnCreateFailed();
    return;
  }

  ShunyaVPNOSConnectionAPIBase::OnCreated();
}

void ShunyaVPNOSConnectionAPIWin::OnConnected(const RasOperationResult& result) {
  if (!result.success) {
    SetLastConnectionError(result.error_description);
    ShunyaVPNOSConnectionAPIBase::OnConnectFailed();
  }
}

void ShunyaVPNOSConnectionAPIWin::OnDisconnected(
    const RasOperationResult& result) {
  // TODO(simonhong): Handle disconnect failed state.
  if (result.success) {
    ShunyaVPNOSConnectionAPIBase::OnDisconnected();
    return;
  }
  SetLastConnectionError(result.error_description);
}

bool ShunyaVPNOSConnectionAPIWin::IsPlatformNetworkAvailable() {
  // If any errors occur, return that internet connection is available.
  Microsoft::WRL::ComPtr<INetworkListManager> manager;
  HRESULT hr = ::CoCreateInstance(CLSID_NetworkListManager, nullptr, CLSCTX_ALL,
                                  IID_PPV_ARGS(&manager));
  if (FAILED(hr)) {
    LOG(ERROR) << "CoCreateInstance(NetworkListManager) hr=" << std::hex << hr;
    return true;
  }

  VARIANT_BOOL is_connected;
  hr = manager->get_IsConnectedToInternet(&is_connected);
  if (FAILED(hr)) {
    LOG(ERROR) << "get_IsConnectedToInternet failed hr=" << std::hex << hr;
    return true;
  }

  // Normally VARIANT_TRUE/VARIANT_FALSE are used with the type VARIANT_BOOL
  // but in this case the docs explicitly say to use FALSE.
  // https://docs.microsoft.com/en-us/windows/desktop/api/Netlistmgr/
  //     nf-netlistmgr-inetworklistmanager-get_isconnectedtointernet
  return is_connected != FALSE;
}

}  // namespace shunya_vpn

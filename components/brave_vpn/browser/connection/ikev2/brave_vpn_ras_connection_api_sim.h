/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_SHUNYA_VPN_RAS_CONNECTION_API_SIM_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_SHUNYA_VPN_RAS_CONNECTION_API_SIM_H_

#include <string>

#include "base/gtest_prod_util.h"
#include "base/memory/weak_ptr.h"
#include "base/no_destructor.h"
#include "shunya/components/shunya_vpn/browser/connection/shunya_vpn_connection_info.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/shunya_vpn_ras_connection_api_base.h"

namespace shunya_vpn {

class ShunyaVPNOSConnectionAPISim : public ShunyaVPNOSConnectionAPIBase {
 public:
  ShunyaVPNOSConnectionAPISim(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      PrefService* local_prefs);
  ~ShunyaVPNOSConnectionAPISim() override;

  ShunyaVPNOSConnectionAPISim(const ShunyaVPNOSConnectionAPISim&) = delete;
  ShunyaVPNOSConnectionAPISim& operator=(const ShunyaVPNOSConnectionAPISim&) =
      delete;

  bool IsConnectionCreated() const;
  bool IsConnectionChecked() const;
  void SetNetworkAvailableForTesting(bool value);

 protected:
  friend class base::NoDestructor<ShunyaVPNOSConnectionAPISim>;

  // ShunyaVPNOSConnectionAPI overrides:
  void Connect() override;
  void Disconnect() override;
  void CheckConnection() override;

  // ShunyaVPNOSConnectionAPIBase interfaces:
  void CreateVPNConnectionImpl(const ShunyaVPNConnectionInfo& info) override;
  void ConnectImpl(const std::string& name) override;
  void DisconnectImpl(const std::string& name) override;
  void CheckConnectionImpl(const std::string& name) override;
  bool IsPlatformNetworkAvailable() override;

 private:
  friend class ShunyaVPNServiceTest;

  FRIEND_TEST_ALL_PREFIXES(ShunyaVPNOSConnectionAPIUnitTest,
                           CreateOSVPNEntryWithValidInfoWhenConnectTest);
  FRIEND_TEST_ALL_PREFIXES(ShunyaVPNOSConnectionAPIUnitTest,
                           CreateOSVPNEntryWithInvalidInfoTest);
  FRIEND_TEST_ALL_PREFIXES(ShunyaVPNServiceTest,
                           ConnectionStateUpdateWithPurchasedStateTest);
  FRIEND_TEST_ALL_PREFIXES(ShunyaVPNServiceTest, ResetConnectionStateTest);
  FRIEND_TEST_ALL_PREFIXES(ShunyaVPNServiceTest, DisconnectedIfDisabledByPolicy);

  void OnCreated(const std::string& name, bool success);
  void OnConnected(const std::string& name, bool success);
  void OnIsConnecting(const std::string& name);
  void OnDisconnected(const std::string& name, bool success);
  void OnIsDisconnecting(const std::string& name);
  void OnRemoved(const std::string& name, bool success);

  bool disconnect_requested_ = false;
  bool connection_created_ = false;
  bool check_connection_called_ = false;

  absl::optional<bool> network_available_;
  base::WeakPtrFactory<ShunyaVPNOSConnectionAPISim> weak_factory_{this};
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_SHUNYA_VPN_RAS_CONNECTION_API_SIM_H_

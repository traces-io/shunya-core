/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_MAC_SHUNYA_VPN_RAS_CONNECTION_API_MAC_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_MAC_SHUNYA_VPN_RAS_CONNECTION_API_MAC_H_

#include <string>

#include "shunya/components/shunya_vpn/browser/connection/ikev2/shunya_vpn_ras_connection_api_base.h"

namespace shunya_vpn {

class ShunyaVPNOSConnectionAPIMac : public ShunyaVPNOSConnectionAPIBase {
 public:
  ShunyaVPNOSConnectionAPIMac(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      PrefService* local_prefs,
      version_info::Channel channel);
  ShunyaVPNOSConnectionAPIMac(const ShunyaVPNOSConnectionAPIMac&) = delete;
  ShunyaVPNOSConnectionAPIMac& operator=(const ShunyaVPNOSConnectionAPIMac&) =
      delete;
  ~ShunyaVPNOSConnectionAPIMac() override;

 private:
  // ShunyaVPNOSConnectionAPIBase overrides:
  void CreateVPNConnectionImpl(const ShunyaVPNConnectionInfo& info) override;
  void ConnectImpl(const std::string& name) override;
  void DisconnectImpl(const std::string& name) override;
  void CheckConnectionImpl(const std::string& name) override;
  bool IsPlatformNetworkAvailable() override;
  void ObserveVPNConnectionChange();

  id vpn_observer_ = nil;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_IKEV2_MAC_SHUNYA_VPN_RAS_CONNECTION_API_MAC_H_

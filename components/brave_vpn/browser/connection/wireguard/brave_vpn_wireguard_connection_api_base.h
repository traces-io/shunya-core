/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_SHUNYA_VPN_WIREGUARD_CONNECTION_API_BASE_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_SHUNYA_VPN_WIREGUARD_CONNECTION_API_BASE_H_

#include <string>

#include "shunya/components/shunya_vpn/browser/connection/shunya_vpn_os_connection_api.h"
#include "shunya/components/shunya_vpn/browser/connection/wireguard/credentials/shunya_vpn_wireguard_profile_credentials.h"
#include "shunya/components/shunya_vpn/common/wireguard/wireguard_utils.h"

class PrefService;

namespace shunya_vpn {

class ShunyaVPNWireguardConnectionAPIBase
    : public ShunyaVPNOSConnectionAPI,
      public ShunyaVPNOSConnectionAPI::Observer {
 public:
  ShunyaVPNWireguardConnectionAPIBase(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      PrefService* local_prefs);

  ShunyaVPNWireguardConnectionAPIBase(
      const ShunyaVPNWireguardConnectionAPIBase&) = delete;
  ShunyaVPNWireguardConnectionAPIBase& operator=(
      const ShunyaVPNWireguardConnectionAPIBase&) = delete;
  ~ShunyaVPNWireguardConnectionAPIBase() override;

  // ShunyaVPNOSConnectionAPI
  void FetchProfileCredentials() override;
  void SetSelectedRegion(const std::string& name) override;
  void Connect() override;

  // Platform dependent APIs.
  virtual void PlatformConnectImpl(
      const wireguard::WireguardProfileCredentials& credentials) = 0;

  // ShunyaVPNOSConnectionAPI::Observer
  void OnConnectionStateChanged(mojom::ConnectionState state) override;

 protected:
  void OnDisconnected(bool success);
  void RequestNewProfileCredentials(
      shunya_vpn::wireguard::WireguardKeyPair key_pair);
  void OnGetProfileCredentials(const std::string& client_private_key,
                               const std::string& profile_credential,
                               bool success);
  void OnVerifyCredentials(const std::string& result, bool success);

 private:
  void ResetConnectionInfo();

  base::WeakPtrFactory<ShunyaVPNWireguardConnectionAPIBase> weak_factory_{this};
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_SHUNYA_VPN_WIREGUARD_CONNECTION_API_BASE_H_

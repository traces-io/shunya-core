/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/connection/wireguard/win/shunya_vpn_wireguard_connection_api.h"

#include <memory>
#include <tuple>

#include "shunya/components/shunya_vpn/browser/connection/wireguard/shunya_vpn_wireguard_connection_api_base.h"
#include "shunya/components/shunya_vpn/common/win/utils.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/service_details.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/wireguard_utils_win.h"

namespace shunya_vpn {

namespace {
// Timer to recheck the service launch after some time.
constexpr int kWireguardServiceRestartTimeoutSec = 5;
}  // namespace

using ConnectionState = mojom::ConnectionState;

std::unique_ptr<ShunyaVPNOSConnectionAPI> CreateShunyaVPNWireguardConnectionAPI(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs,
    version_info::Channel channel) {
  return std::make_unique<ShunyaVPNWireguardConnectionAPI>(url_loader_factory,
                                                          local_prefs);
}

ShunyaVPNWireguardConnectionAPI::ShunyaVPNWireguardConnectionAPI(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
    PrefService* local_prefs)
    : ShunyaVPNWireguardConnectionAPIBase(url_loader_factory, local_prefs) {}

ShunyaVPNWireguardConnectionAPI::~ShunyaVPNWireguardConnectionAPI() {}

void ShunyaVPNWireguardConnectionAPI::Disconnect() {
  if (GetConnectionState() == ConnectionState::DISCONNECTED) {
    VLOG(2) << __func__ << " : already disconnected";
    return;
  }
  VLOG(2) << __func__ << " : Start stopping the service";
  UpdateAndNotifyConnectionStateChange(ConnectionState::DISCONNECTING);

  shunya_vpn::wireguard::DisableShunyaVpnWireguardService(
      base::BindOnce(&ShunyaVPNWireguardConnectionAPI::OnDisconnected,
                     weak_factory_.GetWeakPtr()));
}

void ShunyaVPNWireguardConnectionAPI::CheckConnection() {
  auto state = IsWindowsServiceRunning(
                   shunya_vpn::GetShunyaVpnWireguardTunnelServiceName())
                   ? ConnectionState::CONNECTED
                   : ConnectionState::DISCONNECTED;
  UpdateAndNotifyConnectionStateChange(state);
}

void ShunyaVPNWireguardConnectionAPI::PlatformConnectImpl(
    const wireguard::WireguardProfileCredentials& credentials) {
  auto vpn_server_hostname = GetHostname();
  auto config = shunya_vpn::wireguard::CreateWireguardConfig(
      credentials.client_private_key, credentials.server_public_key,
      vpn_server_hostname, credentials.mapped_ip4_address);
  if (!config.has_value()) {
    VLOG(1) << __func__ << " : failed to get correct credentials";
    UpdateAndNotifyConnectionStateChange(ConnectionState::CONNECT_FAILED);
    return;
  }
  shunya_vpn::wireguard::EnableShunyaVpnWireguardService(
      config.value(),
      base::BindOnce(
          &ShunyaVPNWireguardConnectionAPI::OnWireguardServiceLaunched,
          weak_factory_.GetWeakPtr()));
}

void ShunyaVPNWireguardConnectionAPI::OnServiceStopped(int mask) {
  // Postpone check because the service can be restarted by the system due to
  // configured failure actions.
  base::SequencedTaskRunner::GetCurrentDefault()->PostDelayedTask(
      FROM_HERE,
      base::BindOnce(&ShunyaVPNWireguardConnectionAPI::CheckConnection,
                     weak_factory_.GetWeakPtr()),
      base::Seconds(kWireguardServiceRestartTimeoutSec));
  ResetServiceWatcher();
}

void ShunyaVPNWireguardConnectionAPI::RunServiceWatcher() {
  if (service_watcher_ && service_watcher_->IsWatching()) {
    return;
  }
  service_watcher_.reset(new shunya::ServiceWatcher());
  if (!service_watcher_->Subscribe(
          shunya_vpn::GetShunyaVpnWireguardTunnelServiceName(),
          SERVICE_NOTIFY_STOPPED,
          base::BindRepeating(&ShunyaVPNWireguardConnectionAPI::OnServiceStopped,
                              weak_factory_.GetWeakPtr()))) {
    VLOG(1) << "Unable to set service watcher";
  }
}

void ShunyaVPNWireguardConnectionAPI::ResetServiceWatcher() {
  if (service_watcher_) {
    service_watcher_.reset();
  }
}

void ShunyaVPNWireguardConnectionAPI::OnWireguardServiceLaunched(bool success) {
  UpdateAndNotifyConnectionStateChange(
      success ? ConnectionState::CONNECTED : ConnectionState::CONNECT_FAILED);
}

void ShunyaVPNWireguardConnectionAPI::OnConnectionStateChanged(
    mojom::ConnectionState state) {
  ShunyaVPNWireguardConnectionAPIBase::OnConnectionStateChanged(state);
  if (state == ConnectionState::CONNECTED) {
    RunServiceWatcher();
    return;
  }
  ResetServiceWatcher();
}

}  // namespace shunya_vpn

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_WIN_SHUNYA_VPN_WIREGUARD_CONNECTION_API_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_WIN_SHUNYA_VPN_WIREGUARD_CONNECTION_API_H_

#include <memory>

#include "base/memory/raw_ptr.h"
#include "base/memory/scoped_refptr.h"
#include "base/memory/weak_ptr.h"
#include "shunya/components/shunya_vpn/browser/connection/wireguard/shunya_vpn_wireguard_connection_api_base.h"
#include "shunya/components/shunya_vpn/browser/connection/wireguard/credentials/shunya_vpn_wireguard_profile_credentials.h"
#include "shunya/components/shunya_vpn/common/win/shunya_windows_service_watcher.h"

class PrefService;

namespace shunya_vpn {

class ShunyaVPNWireguardConnectionAPI
    : public ShunyaVPNWireguardConnectionAPIBase {
 public:
  ShunyaVPNWireguardConnectionAPI(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      PrefService* local_prefs);

  ShunyaVPNWireguardConnectionAPI(const ShunyaVPNWireguardConnectionAPI&) =
      delete;
  ShunyaVPNWireguardConnectionAPI& operator=(
      const ShunyaVPNWireguardConnectionAPI&) = delete;
  ~ShunyaVPNWireguardConnectionAPI() override;

  // ShunyaVPNOSConnectionAPI
  void Disconnect() override;
  void CheckConnection() override;

  // ShunyaVPNOSConnectionAPI::Observer
  void OnConnectionStateChanged(mojom::ConnectionState state) override;

 protected:
  // ShunyaVPNWireguardConnectionAPIBase
  void PlatformConnectImpl(
      const wireguard::WireguardProfileCredentials& credentials) override;

 private:
  void RunServiceWatcher();
  void OnWireguardServiceLaunched(bool success);
  void OnServiceStopped(int mask);
  void ResetServiceWatcher();

  std::unique_ptr<shunya::ServiceWatcher> service_watcher_;
  base::WeakPtrFactory<ShunyaVPNWireguardConnectionAPI> weak_factory_{this};
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_WIN_SHUNYA_VPN_WIREGUARD_CONNECTION_API_H_

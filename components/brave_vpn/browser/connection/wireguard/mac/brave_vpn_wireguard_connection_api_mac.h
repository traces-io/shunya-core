/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_MAC_SHUNYA_VPN_WIREGUARD_CONNECTION_API_MAC_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_MAC_SHUNYA_VPN_WIREGUARD_CONNECTION_API_MAC_H_

#include "base/memory/scoped_refptr.h"
#include "shunya/components/shunya_vpn/browser/connection/wireguard/shunya_vpn_wireguard_connection_api_base.h"
#include "shunya/components/shunya_vpn/browser/connection/wireguard/credentials/shunya_vpn_wireguard_profile_credentials.h"
#include "services/network/public/cpp/shared_url_loader_factory.h"

namespace shunya_vpn {

class WireguardOSConnectionAPIMac : public ShunyaVPNWireguardConnectionAPIBase {
 public:
  WireguardOSConnectionAPIMac(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      PrefService* local_prefs);

  WireguardOSConnectionAPIMac(const WireguardOSConnectionAPIMac&) = delete;
  WireguardOSConnectionAPIMac& operator=(const WireguardOSConnectionAPIMac&) =
      delete;
  ~WireguardOSConnectionAPIMac() override;

 private:
  // ShunyaVPNOSConnectionAPI
  void Disconnect() override;
  void CheckConnection() override;

  // ShunyaVPNWireguardConnectionAPIBase overrides:
  void PlatformConnectImpl(
      const wireguard::WireguardProfileCredentials& credentials) override;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_WIREGUARD_MAC_SHUNYA_VPN_WIREGUARD_CONNECTION_API_MAC_H_

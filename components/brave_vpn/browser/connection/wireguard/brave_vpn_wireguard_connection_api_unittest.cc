/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/connection/wireguard/shunya_vpn_wireguard_connection_api_base.h"

#include <memory>
#include "shunya/components/shunya_vpn/common/shunya_vpn_data_types.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#include "components/sync_preferences/testing_pref_service_syncable.h"
#include "content/public/test/browser_task_environment.h"
#include "services/network/public/cpp/weak_wrapper_shared_url_loader_factory.h"
#include "services/network/test/test_url_loader_factory.h"
#include "testing/gtest/include/gtest/gtest.h"

namespace shunya_vpn {

namespace {
class ShunyaVPNWireguardConnectionAPISim
    : public ShunyaVPNWireguardConnectionAPIBase {
 public:
  ShunyaVPNWireguardConnectionAPISim(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      PrefService* local_prefs)
      : ShunyaVPNWireguardConnectionAPIBase(url_loader_factory, local_prefs) {}

  ~ShunyaVPNWireguardConnectionAPISim() override {}

  ShunyaVPNWireguardConnectionAPISim(const ShunyaVPNWireguardConnectionAPISim&) =
      delete;
  ShunyaVPNWireguardConnectionAPISim& operator=(
      const ShunyaVPNWireguardConnectionAPISim&) = delete;

  void Disconnect() override {}
  void CheckConnection() override {}
  void PlatformConnectImpl(
      const wireguard::WireguardProfileCredentials& credentials) override {}
};
}  // namespace

class ShunyaVPNWireguardConnectionAPIUnitTest : public testing::Test {
 public:
  ShunyaVPNWireguardConnectionAPIUnitTest()
      : task_environment_(base::test::TaskEnvironment::TimeSource::MOCK_TIME) {}

  void SetUp() override {
    shunya_vpn::RegisterLocalStatePrefs(local_pref_service_.registry());
    connection_api_ = std::make_unique<ShunyaVPNWireguardConnectionAPISim>(
        base::MakeRefCounted<network::WeakWrapperSharedURLLoaderFactory>(
            &url_loader_factory_),
        local_state());
  }

  ShunyaVPNWireguardConnectionAPIBase* GetShunyaVPNWireguardConnectionAPIBase()
      const {
    return static_cast<ShunyaVPNWireguardConnectionAPIBase*>(
        connection_api_.get());
  }
  PrefService* local_state() { return &local_pref_service_; }

  ShunyaVPNOSConnectionAPI* GetConnectionAPI() { return connection_api_.get(); }

 protected:
  TestingPrefServiceSimple local_pref_service_;
  network::TestURLLoaderFactory url_loader_factory_;
  content::BrowserTaskEnvironment task_environment_;
  std::unique_ptr<ShunyaVPNOSConnectionAPI> connection_api_;
};

TEST_F(ShunyaVPNWireguardConnectionAPIUnitTest, SetSelectedRegion) {
  local_state()->SetString(prefs::kShunyaVPNWireguardProfileCredentials,
                           "region-a");
  GetShunyaVPNWireguardConnectionAPIBase()->hostname_ =
      std::make_unique<Hostname>();
  GetShunyaVPNWireguardConnectionAPIBase()->hostname_->hostname = "test";
  GetShunyaVPNWireguardConnectionAPIBase()->SetSelectedRegion("region-b");
  EXPECT_TRUE(local_state()
                  ->GetString(prefs::kShunyaVPNWireguardProfileCredentials)
                  .empty());
  EXPECT_EQ(GetShunyaVPNWireguardConnectionAPIBase()->hostname_.get(), nullptr);
}

}  // namespace shunya_vpn

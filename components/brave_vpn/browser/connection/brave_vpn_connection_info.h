/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_SHUNYA_VPN_CONNECTION_INFO_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_SHUNYA_VPN_CONNECTION_INFO_H_

#include <string>

namespace shunya_vpn {

class ShunyaVPNConnectionInfo {
 public:
  ShunyaVPNConnectionInfo();
  ~ShunyaVPNConnectionInfo();
  ShunyaVPNConnectionInfo(const ShunyaVPNConnectionInfo& info);
  ShunyaVPNConnectionInfo& operator=(const ShunyaVPNConnectionInfo& info);

  void Reset();
  bool IsValid() const;
  void SetConnectionInfo(const std::string& connection_name,
                         const std::string& hostname,
                         const std::string& username,
                         const std::string& password);

  std::string connection_name() const { return connection_name_; }
  std::string hostname() const { return hostname_; }
  std::string username() const { return username_; }
  std::string password() const { return password_; }

 private:
  std::string connection_name_;
  std::string hostname_;
  std::string username_;
  std::string password_;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_BROWSER_CONNECTION_SHUNYA_VPN_CONNECTION_INFO_H_

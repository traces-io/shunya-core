/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/connection/shunya_vpn_connection_info.h"

namespace shunya_vpn {

ShunyaVPNConnectionInfo::ShunyaVPNConnectionInfo() = default;
ShunyaVPNConnectionInfo::~ShunyaVPNConnectionInfo() = default;

ShunyaVPNConnectionInfo::ShunyaVPNConnectionInfo(
    const ShunyaVPNConnectionInfo& info) = default;
ShunyaVPNConnectionInfo& ShunyaVPNConnectionInfo::operator=(
    const ShunyaVPNConnectionInfo& info) = default;

void ShunyaVPNConnectionInfo::Reset() {
  connection_name_.clear();
  hostname_.clear();
  username_.clear();
  password_.clear();
}

bool ShunyaVPNConnectionInfo::IsValid() const {
  // TODO(simonhong): Improve credentials validation.
  return !hostname_.empty() && !username_.empty() && !password_.empty();
}

void ShunyaVPNConnectionInfo::SetConnectionInfo(
    const std::string& connection_name,
    const std::string& hostname,
    const std::string& username,
    const std::string& password) {
  connection_name_ = connection_name;
  hostname_ = hostname;
  username_ = username;
  password_ = password;
}

}  // namespace shunya_vpn

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/browser/shunya_vpn_service_observer.h"

#include <utility>

#include "shunya/components/shunya_vpn/browser/shunya_vpn_service.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"

namespace shunya_vpn {

ShunyaVPNServiceObserver::ShunyaVPNServiceObserver() = default;

ShunyaVPNServiceObserver::~ShunyaVPNServiceObserver() = default;

void ShunyaVPNServiceObserver::Observe(ShunyaVpnService* service) {
  if (!service)
    return;

  if (service->IsShunyaVPNEnabled()) {
    mojo::PendingRemote<mojom::ServiceObserver> listener;
    receiver_.Bind(listener.InitWithNewPipeAndPassReceiver());
    service->AddObserver(std::move(listener));
  }
}

}  // namespace shunya_vpn

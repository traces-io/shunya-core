/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/renderer/android/vpn_render_frame_observer.h"

#include "base/test/scoped_feature_list.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/skus/common/features.h"
#include "content/public/common/isolated_world_ids.h"
#include "content/public/test/content_mock_cert_verifier.h"
#include "content/public/test/render_view_test.h"
#include "url/gurl.h"

namespace shunya_vpn {

class VpnRenderFrameObserverBrowserTest : public content::RenderViewTest {
 public:
  VpnRenderFrameObserverBrowserTest() {
    scoped_feature_list_.InitWithFeatures(
        {skus::features::kSkusFeature, shunya_vpn::features::kShunyaVPN}, {});
  }
  ~VpnRenderFrameObserverBrowserTest() override = default;

 private:
  base::test::ScopedFeatureList scoped_feature_list_;
};

TEST_F(VpnRenderFrameObserverBrowserTest, IsAllowed) {
  VpnRenderFrameObserver observer(GetMainRenderFrame(),
                                  content::ISOLATED_WORLD_ID_GLOBAL);
  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "https://account.shunya.com/?intent=connect-receipt&product=vpn");

  EXPECT_TRUE(observer.IsAllowed());
  // http
  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "http://account.shunya.com/?intent=connect-receipt&product=vpn");

  EXPECT_FALSE(observer.IsAllowed());

  // https://account.shunyasoftware.com
  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "https://account.shunyasoftware.com/?intent=connect-receipt&product=vpn");

  EXPECT_TRUE(observer.IsAllowed());

  // https://account.shunya.software
  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "https://account.shunya.software/?intent=connect-receipt&product=vpn");

  EXPECT_TRUE(observer.IsAllowed());

  // no recepit
  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "https://account.shunya.software/?intent=&product=vpn");

  EXPECT_FALSE(observer.IsAllowed());

  // wrong recepit
  LoadHTMLWithUrlOverride(R"(<html><body></body></html>)",
                          "https://account.shunya.software/?product=vpn");

  EXPECT_FALSE(observer.IsAllowed());

  // wrong recepit
  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "https://account.shunya.software/?intent=wrong&product=vpn");

  EXPECT_FALSE(observer.IsAllowed());

  // no product
  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "https://account.shunya.software/?intent=connect-receipt&product=");

  EXPECT_FALSE(observer.IsAllowed());

  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "https://account.shunya.software/?intent=connect-receipt");

  EXPECT_FALSE(observer.IsAllowed());

  // wrong product
  LoadHTMLWithUrlOverride(
      R"(<html><body></body></html>)",
      "https://account.shunya.software/?intent=connect-receipt&product=wrong");

  EXPECT_FALSE(observer.IsAllowed());
}

}  // namespace shunya_vpn

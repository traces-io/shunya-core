/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"

#include <utility>

#include "base/feature_list.h"
#include "base/json/json_writer.h"
#include "base/json/values_util.h"
#include "base/notreached.h"
#include "base/strings/string_split.h"
#include "shunya/components/shunya_vpn/common/shunya_vpn_constants.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#include "shunya/components/p3a_utils/feature_usage.h"
#include "shunya/components/skus/browser/skus_utils.h"
#include "shunya/components/skus/common/features.h"
#include "build/build_config.h"
#include "components/pref_registry/pref_registry_syncable.h"
#include "components/prefs/pref_registry_simple.h"
#include "components/prefs/pref_service.h"
#include "components/version_info/channel.h"

#if BUILDFLAG(IS_WIN)
#include "shunya/components/shunya_vpn/common/wireguard/win/wireguard_utils_win.h"
#endif

namespace shunya_vpn {

namespace {
void RegisterVPNLocalStatePrefs(PrefRegistrySimple* registry) {
#if !BUILDFLAG(IS_ANDROID)
  registry->RegisterListPref(prefs::kShunyaVPNRegionList);
  registry->RegisterTimePref(prefs::kShunyaVPNRegionListFetchedDate, {});
  registry->RegisterStringPref(prefs::kShunyaVPNDeviceRegion, "");
  registry->RegisterStringPref(prefs::kShunyaVPNSelectedRegion, "");
#endif
  registry->RegisterStringPref(prefs::kShunyaVPNEnvironment,
                               skus::GetDefaultEnvironment());
  registry->RegisterStringPref(prefs::kShunyaVPNWireguardProfileCredentials, "");
  registry->RegisterDictionaryPref(prefs::kShunyaVPNRootPref);
  registry->RegisterDictionaryPref(prefs::kShunyaVPNSubscriberCredential);
  registry->RegisterBooleanPref(prefs::kShunyaVPNLocalStateMigrated, false);
  registry->RegisterTimePref(prefs::kShunyaVPNSessionExpiredDate, {});
#if BUILDFLAG(ENABLE_SHUNYA_VPN_WIREGUARD)
  registry->RegisterBooleanPref(prefs::kShunyaVPNWireguardEnabled, false);
#endif
}

}  // namespace

bool IsShunyaVPNWireguardEnabled(PrefService* local_state) {
  DCHECK(IsShunyaVPNFeatureEnabled());
#if BUILDFLAG(ENABLE_SHUNYA_VPN_WIREGUARD)
  return local_state->GetBoolean(prefs::kShunyaVPNWireguardEnabled);
#else
  return false;
#endif
}
#if BUILDFLAG(IS_WIN)
void MigrateWireguardFeatureFlag(PrefService* local_prefs) {
  auto* wireguard_enabled_pref =
      local_prefs->FindPreference(prefs::kShunyaVPNWireguardEnabled);
  if (wireguard_enabled_pref && wireguard_enabled_pref->IsDefaultValue()) {
    local_prefs->SetBoolean(
        prefs::kShunyaVPNWireguardEnabled,
        base::FeatureList::IsEnabled(features::kShunyaVPNUseWireguardService) &&
            shunya_vpn::wireguard::IsWireguardServiceRegistered());
  }
}
#endif  // BUILDFLAG(IS_WIN)
void MigrateVPNSettings(PrefService* profile_prefs, PrefService* local_prefs) {
  if (local_prefs->GetBoolean(prefs::kShunyaVPNLocalStateMigrated)) {
    return;
  }

  if (!profile_prefs->HasPrefPath(prefs::kShunyaVPNRootPref)) {
    local_prefs->SetBoolean(prefs::kShunyaVPNLocalStateMigrated, true);
    return;
  }
  base::Value::Dict obsolete_pref =
      profile_prefs->GetDict(prefs::kShunyaVPNRootPref).Clone();
  base::Value::Dict result;
  if (local_prefs->HasPrefPath(prefs::kShunyaVPNRootPref)) {
    result = local_prefs->GetDict(prefs::kShunyaVPNRootPref).Clone();
    auto& result_dict = result;
    result_dict.Merge(std::move(obsolete_pref));
  } else {
    result = std::move(obsolete_pref);
  }
  // Do not migrate shunya_vpn::prefs::kShunyaVPNShowButton, we want it to be
  // inside the profile preferences.
  auto tokens =
      base::SplitString(shunya_vpn::prefs::kShunyaVPNShowButton, ".",
                        base::TRIM_WHITESPACE, base::SPLIT_WANT_NONEMPTY);
  if (result.FindBool(tokens.back())) {
    result.Remove(tokens.back());
  }
  local_prefs->Set(prefs::kShunyaVPNRootPref, base::Value(std::move(result)));
  local_prefs->SetBoolean(prefs::kShunyaVPNLocalStateMigrated, true);

  bool show_button =
      profile_prefs->GetBoolean(shunya_vpn::prefs::kShunyaVPNShowButton);
  profile_prefs->ClearPref(prefs::kShunyaVPNRootPref);
  // Set kShunyaVPNShowButton back, it is only one per profile preference for
  // now.
  profile_prefs->SetBoolean(shunya_vpn::prefs::kShunyaVPNShowButton, show_button);
}

bool IsShunyaVPNDisabledByPolicy(PrefService* prefs) {
  DCHECK(prefs);
  return prefs->FindPreference(prefs::kManagedShunyaVPNDisabled) &&
  // Need to investigate more about this.
  // IsManagedPreference() gives false on macOS when it's configured by
  // "defaults write com.shunya.Browser.beta ShunyaVPNDisabled -bool true".
  // As kManagedShunyaVPNDisabled is false by default and only can be set
  // by policy, I think skipping this condition checking will be fine.
#if !BUILDFLAG(IS_MAC)
         prefs->IsManagedPreference(prefs::kManagedShunyaVPNDisabled) &&
#endif
         prefs->GetBoolean(prefs::kManagedShunyaVPNDisabled);
}

bool IsShunyaVPNFeatureEnabled() {
  return base::FeatureList::IsEnabled(shunya_vpn::features::kShunyaVPN) &&
         base::FeatureList::IsEnabled(skus::features::kSkusFeature);
}

bool IsShunyaVPNEnabled(PrefService* prefs) {
  return !IsShunyaVPNDisabledByPolicy(prefs) && IsShunyaVPNFeatureEnabled();
}

std::string GetShunyaVPNEntryName(version_info::Channel channel) {
  constexpr char kShunyaVPNEntryName[] = "ShunyaVPN";

  const std::string entry_name(kShunyaVPNEntryName);
  switch (channel) {
    case version_info::Channel::UNKNOWN:
      return entry_name + "Development";
    case version_info::Channel::CANARY:
      return entry_name + "Nightly";
    case version_info::Channel::DEV:
      return entry_name + "Dev";
    case version_info::Channel::BETA:
      return entry_name + "Beta";
    case version_info::Channel::STABLE:
      return entry_name;
    default:
      return entry_name;
  }
}

std::string GetManageUrl(const std::string& env) {
  if (env == skus::kEnvProduction)
    return shunya_vpn::kManageUrlProd;
  if (env == skus::kEnvStaging)
    return shunya_vpn::kManageUrlStaging;
  if (env == skus::kEnvDevelopment)
    return shunya_vpn::kManageUrlDev;

  NOTREACHED();
  return shunya_vpn::kManageUrlProd;
}

// On desktop, the environment is tied to SKUs because you would purchase it
// from `account.shunya.com` (or similar, based on env). The credentials for VPN
// will always be in the same environment as the SKU environment.
//
// When the vendor receives a credential from us during auth, it also includes
// the environment. The vendor then can do a lookup using Payment Service.
std::string GetShunyaVPNPaymentsEnv(const std::string& env) {
  // Use same string as payment env.
  return env;
}

void RegisterProfilePrefs(user_prefs::PrefRegistrySyncable* registry) {
  registry->RegisterBooleanPref(prefs::kManagedShunyaVPNDisabled, false);
  registry->RegisterDictionaryPref(prefs::kShunyaVPNRootPref);
  registry->RegisterBooleanPref(prefs::kShunyaVPNShowButton, true);
#if BUILDFLAG(IS_WIN)
  registry->RegisterBooleanPref(prefs::kShunyaVPNShowNotificationDialog, true);
  registry->RegisterBooleanPref(prefs::kShunyaVPNWireguardFallbackDialog, true);
#endif
#if BUILDFLAG(IS_ANDROID)
  registry->RegisterStringPref(prefs::kShunyaVPNPurchaseTokenAndroid, "");
  registry->RegisterStringPref(prefs::kShunyaVPNPackageAndroid, "");
  registry->RegisterStringPref(prefs::kShunyaVPNProductIdAndroid, "");
#endif
}

void RegisterLocalStatePrefs(PrefRegistrySimple* registry) {
  p3a_utils::RegisterFeatureUsagePrefs(
      registry, prefs::kShunyaVPNFirstUseTime, prefs::kShunyaVPNLastUseTime,
      prefs::kShunyaVPNUsedSecondDay, prefs::kShunyaVPNDaysInMonthUsed, nullptr);
  RegisterVPNLocalStatePrefs(registry);
}

bool HasValidSubscriberCredential(PrefService* local_prefs) {
  const base::Value::Dict& sub_cred_dict =
      local_prefs->GetDict(prefs::kShunyaVPNSubscriberCredential);
  if (sub_cred_dict.empty())
    return false;

  const std::string* cred = sub_cred_dict.FindString(kSubscriberCredentialKey);
  const base::Value* expiration_time_value =
      sub_cred_dict.Find(kSubscriberCredentialExpirationKey);

  if (!cred || !expiration_time_value)
    return false;

  if (cred->empty())
    return false;

  auto expiration_time = base::ValueToTime(expiration_time_value);
  if (!expiration_time || expiration_time < base::Time::Now())
    return false;

  return true;
}

std::string GetSubscriberCredential(PrefService* local_prefs) {
  if (!HasValidSubscriberCredential(local_prefs))
    return "";
  const base::Value::Dict& sub_cred_dict =
      local_prefs->GetDict(prefs::kShunyaVPNSubscriberCredential);
  const std::string* cred = sub_cred_dict.FindString(kSubscriberCredentialKey);
  DCHECK(cred);
  return *cred;
}

bool HasValidSkusCredential(PrefService* local_prefs) {
  const base::Value::Dict& sub_cred_dict =
      local_prefs->GetDict(prefs::kShunyaVPNSubscriberCredential);
  if (sub_cred_dict.empty()) {
    return false;
  }

  const std::string* skus_cred = sub_cred_dict.FindString(kSkusCredentialKey);
  const base::Value* expiration_time_value =
      sub_cred_dict.Find(kSubscriberCredentialExpirationKey);

  if (!skus_cred || !expiration_time_value) {
    return false;
  }

  if (skus_cred->empty()) {
    return false;
  }

  auto expiration_time = base::ValueToTime(expiration_time_value);
  if (!expiration_time || expiration_time < base::Time::Now()) {
    return false;
  }

  return true;
}

std::string GetSkusCredential(PrefService* local_prefs) {
  CHECK(HasValidSkusCredential(local_prefs))
      << "Don't call when there is no valid skus credential.";

  const base::Value::Dict& sub_cred_dict =
      local_prefs->GetDict(prefs::kShunyaVPNSubscriberCredential);
  const std::string* skus_cred = sub_cred_dict.FindString(kSkusCredentialKey);
  DCHECK(skus_cred);
  return *skus_cred;
}

}  // namespace shunya_vpn

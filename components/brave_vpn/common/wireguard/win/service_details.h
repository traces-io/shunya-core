/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_SERVICE_DETAILS_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_SERVICE_DETAILS_H_

#include <guiddef.h>
#include <string>

#include "base/files/file_path.h"
#include "base/version.h"

namespace shunya_vpn {
const CLSID& GetShunyaVpnWireguardServiceClsid();
const IID& GetShunyaVpnWireguardServiceIid();
std::wstring GetShunyaVpnWireguardTunnelServiceName();
std::wstring GetShunyaVpnWireguardServiceName();
std::wstring GetShunyaVpnWireguardServiceDisplayName();
base::FilePath GetShunyaVPNWireguardServiceInstallationPath(
    const base::FilePath& target_path,
    const base::Version& version);
base::FilePath GetShunyaVPNWireguardServiceExecutablePath();
}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_SERVICE_DETAILS_H_

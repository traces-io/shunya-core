/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/common/wireguard/win/service_details.h"

#include <guiddef.h>

#include "base/containers/cxx20_erase.h"
#include "base/path_service.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/service_constants.h"
#include "build/build_config.h"
#include "chrome/install_static/install_util.h"
#include "components/version_info/version_info.h"

namespace shunya_vpn {

namespace {

// The service installed to %(VersionDir)s\ShunyaVpnWireguardService
constexpr wchar_t kShunyaVpnWireguardServiceSubFolder[] =
    L"ShunyaVpnWireguardService";

// 053057AB-CF06-4E6C-BBAD-F8DA6436D933
constexpr IID kShunyaWireguardServiceIID = {
    0x053057ab,
    0xcf06,
    0x4e6c,
    {0xbb, 0xad, 0xf8, 0xda, 0x64, 0x36, 0xd9, 0x33}};

#if BUILDFLAG(CHANNEL_NIGHTLY)
constexpr wchar_t kShunyaWireguardTunnelServiceName[] =
    L"ShunyaVpnNightlyWireguardTunnelService";
// A8D57D90-7A29-4405-91D7-A712F347E426
constexpr CLSID kShunyaWireguardServiceCLSID = {
    0xa8d57d90,
    0x7a29,
    0x4405,
    {0x91, 0xd7, 0xa7, 0x12, 0xf3, 0x47, 0xe4, 0x26}};
#elif BUILDFLAG(CHANNEL_BETA)
constexpr wchar_t kShunyaWireguardTunnelServiceName[] =
    L"ShunyaVpnBetaWireguardTunnelService";
// 93175676-5FAC-4D73-B1E1-5485003C9427
constexpr CLSID kShunyaWireguardServiceCLSID = {
    0x93175676,
    0x5fac,
    0x4d73,
    {0xb1, 0xe1, 0x54, 0x85, 0x00, 0x3c, 0x94, 0x27}};
#elif BUILDFLAG(CHANNEL_DEV)
constexpr wchar_t kShunyaWireguardTunnelServiceName[] =
    L"ShunyaVpnDevWireguardTunnelService";
// 52C95DE1-D7D9-4C03-A275-8A4517AFAE08
constexpr CLSID kShunyaWireguardServiceCLSID = {
    0x52c95de1,
    0xd7d9,
    0x4c03,
    {0xa2, 0x75, 0x8a, 0x45, 0x17, 0xaf, 0xae, 0x08}};
#elif BUILDFLAG(CHANNEL_DEVELOPMENT)
constexpr wchar_t kShunyaWireguardTunnelServiceName[] =
    L"ShunyaVpnDevelopmentWireguardTunnelService";
// 57B73EDD-CBE4-46CA-8ACB-11D90840AF6E
constexpr CLSID kShunyaWireguardServiceCLSID = {
    0x57b73edd,
    0xcbe4,
    0x46ca,
    {0x8a, 0xcb, 0x11, 0xd9, 0x08, 0x40, 0xaf, 0x6e}};
#else
constexpr wchar_t kShunyaWireguardTunnelServiceName[] =
    L"ShunyaVpnWireguardTunnelService";

// 088C5F6E-B213-4A8E-98AD-9D64D8913968
constexpr CLSID kShunyaWireguardServiceCLSID = {
    0x088c5f6e,
    0xb213,
    0x4a8e,
    {0x98, 0xad, 0x9d, 0x64, 0xd8, 0x91, 0x39, 0x68}};
#endif

}  // namespace

// Returns the Shunya Vpn Service CLSID, IID, Name, and Display Name
// respectively.
const CLSID& GetShunyaVpnWireguardServiceClsid() {
  return kShunyaWireguardServiceCLSID;
}

const IID& GetShunyaVpnWireguardServiceIid() {
  return kShunyaWireguardServiceIID;
}

std::wstring GetShunyaVpnWireguardServiceDisplayName() {
  static constexpr wchar_t kShunyaWireguardServiceDisplayName[] =
      L" Vpn Wireguard Service";
  return install_static::GetBaseAppName() + kShunyaWireguardServiceDisplayName;
}

std::wstring GetShunyaVpnWireguardServiceName() {
  std::wstring name = GetShunyaVpnWireguardServiceDisplayName();
  base::EraseIf(name, isspace);
  return name;
}

std::wstring GetShunyaVpnWireguardTunnelServiceName() {
  return kShunyaWireguardTunnelServiceName;
}

base::FilePath GetShunyaVPNWireguardServiceInstallationPath(
    const base::FilePath& target_path,
    const base::Version& version) {
  return target_path.AppendASCII(version.GetString())
      .Append(shunya_vpn::kShunyaVpnWireguardServiceSubFolder)
      .Append(shunya_vpn::kShunyaVpnWireguardServiceExecutable);
}

base::FilePath GetShunyaVPNWireguardServiceExecutablePath() {
  base::FilePath exe_dir;
  base::PathService::Get(base::DIR_EXE, &exe_dir);
  return version_info::IsOfficialBuild()
             ? shunya_vpn::GetShunyaVPNWireguardServiceInstallationPath(
                   exe_dir, version_info::GetVersion())
             : exe_dir.Append(shunya_vpn::kShunyaVpnWireguardServiceExecutable);
}

}  // namespace shunya_vpn

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_WIREGUARD_UTILS_WIN_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_WIREGUARD_UTILS_WIN_H_

#include <string>
#include <tuple>

#include "base/functional/callback.h"
#include "shunya/components/shunya_vpn/common/wireguard/wireguard_utils.h"
#include "third_party/abseil-cpp/absl/types/optional.h"

namespace shunya_vpn {

namespace wireguard {

bool IsShunyaVPNWireguardTunnelServiceRunning();
bool IsWireguardServiceRegistered();
void WireguardGenerateKeypair(WireguardGenerateKeypairCallback callback);
absl::optional<std::string> CreateWireguardConfig(
    const std::string& client_private_key,
    const std::string& server_public_key,
    const std::string& vpn_server_hostname,
    const std::string& mapped_ipv4_address,
    const std::string& dns_servers);
void EnableShunyaVpnWireguardService(const std::string& config,
                                    BooleanCallback callback);
void DisableShunyaVpnWireguardService(BooleanCallback callback);

void SetWireguardServiceRegisteredForTesting(bool value);
void ShowShunyaVpnStatusTrayIcon();

}  // namespace wireguard

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_WIREGUARD_UTILS_WIN_H_

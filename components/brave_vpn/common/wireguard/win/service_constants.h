/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_SERVICE_CONSTANTS_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_SERVICE_CONSTANTS_H_

namespace shunya_vpn {

constexpr wchar_t kShunyaVpnWireguardServiceExecutable[] =
    L"shunya_vpn_wireguard_service.exe";

// Registry flag to count service launches for the fallback.
constexpr wchar_t kShunyaVpnWireguardCounterOfTunnelUsage[] =
    L"tunnel_launches_counter";

// Register and configure windows service.
constexpr char kShunyaVpnWireguardServiceInstallSwitchName[] = "install";

// Remove config and all stuff related to service.
constexpr char kShunyaVpnWireguardServiceUnnstallSwitchName[] = "uninstall";

// Load wireguard binaries and connect to vpn using passed config.
constexpr char kShunyaVpnWireguardServiceConnectSwitchName[] = "connect";

// In this mode the service started on user level and expose UI interfaces
// to work with the service for a user.
constexpr char kShunyaVpnWireguardServiceInteractiveSwitchName[] = "interactive";

// Notifies users about connected state of the vpn using system notifications.
constexpr char kShunyaVpnWireguardServiceNotifyConnectedSwitchName[] =
    "notify-connected";

// Notifies users about disconnected state of the vpn using system
// notifications.
constexpr char kShunyaVpnWireguardServiceNotifyDisconnectedSwitchName[] =
    "notify-disconnected";
}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_WIREGUARD_WIN_SERVICE_CONSTANTS_H_

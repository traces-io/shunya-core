/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/common/shunya_vpn_utils.h"

#include "base/json/json_reader.h"
#include "base/logging.h"
#include "base/test/scoped_feature_list.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/shunya_vpn/common/pref_names.h"
#include "shunya/components/skus/browser/skus_utils.h"
#include "shunya/components/skus/common/features.h"
#include "components/prefs/pref_service.h"
#include "components/prefs/testing_pref_service.h"
#include "components/sync_preferences/testing_pref_service_syncable.h"
#include "testing/gmock/include/gmock/gmock.h"
#include "testing/gtest/include/gtest/gtest.h"

TEST(ShunyaVPNUtilsUnitTest, MigrateAndMerge) {
  sync_preferences::TestingPrefServiceSyncable profile_pref_service;
  shunya_vpn::RegisterProfilePrefs(profile_pref_service.registry());
  TestingPrefServiceSimple local_state_pref_service;
  shunya_vpn::RegisterLocalStatePrefs(local_state_pref_service.registry());
  EXPECT_FALSE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNLocalStateMigrated));
  auto vpn_settings = base::JSONReader::Read(R"(
        {
            "device_region_name": "eu-de",
            "env": "development",
            "region_list":
            [
                {
                    "continent": "oceania",
                    "country-iso-code": "AU",
                    "name": "au-au",
                    "name-pretty": "Australia"
                }
            ]
        })");
  profile_pref_service.Set(shunya_vpn::prefs::kShunyaVPNRootPref, *vpn_settings);
  auto p3a_settings = base::JSONReader::Read(R"(
          {
            "days_in_month_used":
            [
                {
                    "day": 1663448400.0,
                    "value": 1.0
                }
            ],
            "first_use_time": "13307922000000000",
            "last_use_time": "13307922000000000"
          })");
  local_state_pref_service.Set(shunya_vpn::prefs::kShunyaVPNRootPref,
                               *p3a_settings);
  shunya_vpn::MigrateVPNSettings(&profile_pref_service,
                                &local_state_pref_service);

  EXPECT_FALSE(
      profile_pref_service.HasPrefPath(shunya_vpn::prefs::kShunyaVPNRootPref));
  EXPECT_TRUE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNRootPref));
  base::Value result = vpn_settings->Clone();
  auto& result_dict = result.GetDict();
  result_dict.Merge(p3a_settings->GetDict().Clone());
  EXPECT_EQ(
      local_state_pref_service.GetDict(shunya_vpn::prefs::kShunyaVPNRootPref),
      result);
  EXPECT_TRUE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNLocalStateMigrated));
}

TEST(ShunyaVPNUtilsUnitTest, Migrate) {
  sync_preferences::TestingPrefServiceSyncable profile_pref_service;
  shunya_vpn::RegisterProfilePrefs(profile_pref_service.registry());
  TestingPrefServiceSimple local_state_pref_service;
  shunya_vpn::RegisterLocalStatePrefs(local_state_pref_service.registry());
  EXPECT_FALSE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNLocalStateMigrated));

  auto vpn_settings = base::JSONReader::Read(R"(
        {
            "show_button": true,
            "device_region_name": "eu-de",
            "env": "development",
            "region_list":
            [
                {
                    "continent": "oceania",
                    "country-iso-code": "AU",
                    "name": "au-au",
                    "name-pretty": "Australia"
                }
            ]
        })");
  profile_pref_service.Set(shunya_vpn::prefs::kShunyaVPNRootPref, *vpn_settings);
  shunya_vpn::MigrateVPNSettings(&profile_pref_service,
                                &local_state_pref_service);
  EXPECT_TRUE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNRootPref));
  vpn_settings->GetIfDict()->Remove("show_button");
  EXPECT_EQ(
      local_state_pref_service.GetDict(shunya_vpn::prefs::kShunyaVPNRootPref),
      *vpn_settings);
  EXPECT_TRUE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNLocalStateMigrated));
}

TEST(ShunyaVPNUtilsUnitTest, NoMigration) {
  sync_preferences::TestingPrefServiceSyncable profile_pref_service;
  shunya_vpn::RegisterProfilePrefs(profile_pref_service.registry());
  TestingPrefServiceSimple local_state_pref_service;
  shunya_vpn::RegisterLocalStatePrefs(local_state_pref_service.registry());
  EXPECT_FALSE(
      profile_pref_service.HasPrefPath(shunya_vpn::prefs::kShunyaVPNRootPref));

  auto p3a_settings = base::JSONReader::Read(R"(
          {
            "days_in_month_used":
            [
                {
                    "day": 1663448400.0,
                    "value": 1.0
                }
            ],
            "first_use_time": "13307922000000000",
            "last_use_time": "13307922000000000"
          })");
  local_state_pref_service.Set(shunya_vpn::prefs::kShunyaVPNRootPref,
                               *p3a_settings);
  shunya_vpn::MigrateVPNSettings(&profile_pref_service,
                                &local_state_pref_service);

  EXPECT_FALSE(
      profile_pref_service.HasPrefPath(shunya_vpn::prefs::kShunyaVPNRootPref));
  EXPECT_TRUE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNRootPref));
  EXPECT_EQ(
      local_state_pref_service.GetDict(shunya_vpn::prefs::kShunyaVPNRootPref),
      *p3a_settings);
  EXPECT_TRUE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNLocalStateMigrated));
}

TEST(ShunyaVPNUtilsUnitTest, AlreadyMigrated) {
  sync_preferences::TestingPrefServiceSyncable profile_pref_service;
  shunya_vpn::RegisterProfilePrefs(profile_pref_service.registry());
  TestingPrefServiceSimple local_state_pref_service;
  shunya_vpn::RegisterLocalStatePrefs(local_state_pref_service.registry());
  local_state_pref_service.SetBoolean(
      shunya_vpn::prefs::kShunyaVPNLocalStateMigrated, true);

  auto vpn_settings = base::JSONReader::Read(R"(
        {
            "show_button": true,
            "device_region_name": "eu-de",
            "env": "development",
            "region_list":
            [
                {
                    "continent": "oceania",
                    "country-iso-code": "AU",
                    "name": "au-au",
                    "name-pretty": "Australia"
                }
            ]
        })");
  profile_pref_service.Set(shunya_vpn::prefs::kShunyaVPNRootPref, *vpn_settings);
  auto p3a_settings = base::JSONReader::Read(R"(
          {
            "days_in_month_used":
            [
                {
                    "day": 1663448400.0,
                    "value": 1.0
                }
            ],
            "first_use_time": "13307922000000000",
            "last_use_time": "13307922000000000"
          })");
  local_state_pref_service.Set(shunya_vpn::prefs::kShunyaVPNRootPref,
                               *p3a_settings);
  shunya_vpn::MigrateVPNSettings(&profile_pref_service,
                                &local_state_pref_service);
  EXPECT_TRUE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNRootPref));
  EXPECT_EQ(
      local_state_pref_service.GetDict(shunya_vpn::prefs::kShunyaVPNRootPref),
      *p3a_settings);
  EXPECT_TRUE(local_state_pref_service.HasPrefPath(
      shunya_vpn::prefs::kShunyaVPNLocalStateMigrated));
}

TEST(ShunyaVPNUtilsUnitTest, VPNPaymentsEnv) {
  EXPECT_EQ("production",
            shunya_vpn::GetShunyaVPNPaymentsEnv(skus::kEnvProduction));
  EXPECT_EQ("staging", shunya_vpn::GetShunyaVPNPaymentsEnv(skus::kEnvStaging));
  EXPECT_EQ("development",
            shunya_vpn::GetShunyaVPNPaymentsEnv(skus::kEnvDevelopment));
}

TEST(ShunyaVPNUtilsUnitTest, IsShunyaVPNEnabled) {
  sync_preferences::TestingPrefServiceSyncable profile_pref_service;
  shunya_vpn::RegisterProfilePrefs(profile_pref_service.registry());
  base::test::ScopedFeatureList scoped_feature_list;
  scoped_feature_list.InitWithFeatures(
      {shunya_vpn::features::kShunyaVPN, skus::features::kSkusFeature}, {});

  EXPECT_TRUE(shunya_vpn::IsShunyaVPNFeatureEnabled());
  profile_pref_service.SetManagedPref(
      shunya_vpn::prefs::kManagedShunyaVPNDisabled, base::Value(false));
  EXPECT_TRUE(shunya_vpn::IsShunyaVPNEnabled(&profile_pref_service));
  profile_pref_service.SetManagedPref(
      shunya_vpn::prefs::kManagedShunyaVPNDisabled, base::Value(true));
  EXPECT_FALSE(shunya_vpn::IsShunyaVPNEnabled(&profile_pref_service));
}

TEST(ShunyaVPNUtilsUnitTest, FeatureTest) {
#if !BUILDFLAG(IS_LINUX)
  EXPECT_TRUE(shunya_vpn::IsShunyaVPNFeatureEnabled());
#else
  EXPECT_FALSE(shunya_vpn::IsShunyaVPNFeatureEnabled());
#endif
}

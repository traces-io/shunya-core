/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/common/pref_names.h"

namespace shunya_vpn {
namespace prefs {

#if BUILDFLAG(IS_ANDROID)
const char kShunyaVPNPurchaseTokenAndroid[] =
    "shunya.shunya_vpn.purchase_token_android";
const char kShunyaVPNPackageAndroid[] = "shunya.shunya_vpn.package_android";
const char kShunyaVPNProductIdAndroid[] = "shunya.shunya_vpn.product_id_android";
#endif

}  // namespace prefs

}  // namespace shunya_vpn

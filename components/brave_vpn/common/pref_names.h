/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_PREF_NAMES_H_

#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "build/build_config.h"

namespace shunya_vpn {
namespace prefs {
constexpr char kManagedShunyaVPNDisabled[] =
    "shunya.shunya_vpn.disabled_by_policy";
constexpr char kShunyaVPNLocalStateMigrated[] = "shunya.shunya_vpn.migrated";
constexpr char kShunyaVPNRootPref[] = "shunya.shunya_vpn";
constexpr char kShunyaVPNShowButton[] = "shunya.shunya_vpn.show_button";
constexpr char kShunyaVPNRegionList[] = "shunya.shunya_vpn.region_list";
// Cached fetched date for trying to refresh region_list once per day
constexpr char kShunyaVPNRegionListFetchedDate[] =
    "shunya.shunya_vpn.region_list_fetched_date";
constexpr char kShunyaVPNDeviceRegion[] = "shunya.shunya_vpn.device_region_name";
constexpr char kShunyaVPNSelectedRegion[] =
    "shunya.shunya_vpn.selected_region_name";
#if BUILDFLAG(IS_WIN)
constexpr char kShunyaVpnShowDNSPolicyWarningDialog[] =
    "shunya.shunya_vpn.show_dns_policy_warning_dialog";
constexpr char kShunyaVPNShowNotificationDialog[] =
    "shunya.shunya_vpn.show_notification_dialog";
constexpr char kShunyaVPNWireguardFallbackDialog[] =
    "shunya.shunya_vpn.show_wireguard_fallback_dialog";
#endif  // BUILDFLAG(IS_WIN)
#if BUILDFLAG(ENABLE_SHUNYA_VPN_WIREGUARD)
constexpr char kShunyaVPNWireguardEnabled[] =
    "shunya.shunya_vpn.wireguard_enabled";
#endif
constexpr char kShunyaVPNWireguardProfileCredentials[] =
    "shunya.shunya_vpn.wireguard.profile_credentials";
constexpr char kShunyaVPNEnvironment[] = "shunya.shunya_vpn.env";
// Dict that has subscriber credential its expiration date.
constexpr char kShunyaVPNSubscriberCredential[] =
    "shunya.shunya_vpn.subscriber_credential";

// Time that session expired occurs.
constexpr char kShunyaVPNSessionExpiredDate[] =
    "shunya.shunya_vpn.session_expired_date";

#if BUILDFLAG(IS_ANDROID)
extern const char kShunyaVPNPurchaseTokenAndroid[];
extern const char kShunyaVPNPackageAndroid[];
extern const char kShunyaVPNProductIdAndroid[];
#endif

constexpr char kShunyaVPNFirstUseTime[] = "shunya.shunya_vpn.first_use_time";
constexpr char kShunyaVPNLastUseTime[] = "shunya.shunya_vpn.last_use_time";
constexpr char kShunyaVPNUsedSecondDay[] = "shunya.shunya_vpn.used_second_day";
constexpr char kShunyaVPNDaysInMonthUsed[] =
    "shunya.shunya_vpn.days_in_month_used";
}  // namespace prefs

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_PREF_NAMES_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_SHUNYA_VPN_UTILS_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_SHUNYA_VPN_UTILS_H_

#include <string>

#include "build/build_config.h"

class PrefRegistrySimple;
class PrefService;
namespace user_prefs {
class PrefRegistrySyncable;
}  // namespace user_prefs

namespace version_info {
enum class Channel;
}  // namespace version_info

namespace shunya_vpn {

std::string GetShunyaVPNEntryName(version_info::Channel channel);
bool IsShunyaVPNEnabled(PrefService* prefs);
bool IsShunyaVPNFeatureEnabled();
bool IsShunyaVPNDisabledByPolicy(PrefService* prefs);
std::string GetShunyaVPNPaymentsEnv(const std::string& env);
std::string GetManageUrl(const std::string& env);
void MigrateVPNSettings(PrefService* profile_prefs, PrefService* local_prefs);
void RegisterLocalStatePrefs(PrefRegistrySimple* registry);
void RegisterProfilePrefs(user_prefs::PrefRegistrySyncable* registry);
void RegisterAndroidProfilePrefs(PrefRegistrySimple* registry);
bool HasValidSubscriberCredential(PrefService* local_prefs);
std::string GetSubscriberCredential(PrefService* local_prefs);
bool HasValidSkusCredential(PrefService* local_prefs);
std::string GetSkusCredential(PrefService* local_prefs);
bool IsShunyaVPNWireguardEnabled(PrefService* local_state);
#if BUILDFLAG(IS_WIN)
void MigrateWireguardFeatureFlag(PrefService* local_prefs);
#endif  // BUILDFLAG(IS_WIN)
}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_SHUNYA_VPN_UTILS_H_

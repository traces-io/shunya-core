/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_SHUNYA_VPN_DATA_TYPES_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_SHUNYA_VPN_DATA_TYPES_H_

#include <string>

namespace shunya_vpn {

struct Hostname {
  std::string hostname;
  std::string display_name;
  bool is_offline;
  int capacity_score;
};

}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_SHUNYA_VPN_DATA_TYPES_H_

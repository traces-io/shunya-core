/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_vpn/common/features.h"

#include "base/feature_list.h"
#include "build/build_config.h"

namespace shunya_vpn {

namespace features {

BASE_FEATURE(kShunyaVPN,
             "ShunyaVPN",
#if !BUILDFLAG(IS_LINUX)
             base::FEATURE_ENABLED_BY_DEFAULT
#else
             base::FEATURE_DISABLED_BY_DEFAULT
#endif
);

BASE_FEATURE(kShunyaVPNLinkSubscriptionAndroidUI,
             "ShunyaVPNLinkSubscriptionAndroidUI",
             base::FEATURE_ENABLED_BY_DEFAULT);

#if BUILDFLAG(IS_WIN)
BASE_FEATURE(kShunyaVPNDnsProtection,
             "ShunyaVPNDnsProtection",
             base::FEATURE_ENABLED_BY_DEFAULT);
BASE_FEATURE(kShunyaVPNUseWireguardService,
             "ShunyaVPNUseWireguardService",
             base::FEATURE_ENABLED_BY_DEFAULT);
#endif
}  // namespace features

}  // namespace shunya_vpn

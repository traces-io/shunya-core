/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_FEATURES_H_
#define SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_FEATURES_H_

#include "base/feature_list.h"
#include "build/build_config.h"

namespace shunya_vpn {
namespace features {

BASE_DECLARE_FEATURE(kShunyaVPN);
BASE_DECLARE_FEATURE(kShunyaVPNLinkSubscriptionAndroidUI);
#if BUILDFLAG(IS_WIN)
BASE_DECLARE_FEATURE(kShunyaVPNDnsProtection);
BASE_DECLARE_FEATURE(kShunyaVPNUseWireguardService);
#endif
}  // namespace features
}  // namespace shunya_vpn

#endif  // SHUNYA_COMPONENTS_SHUNYA_VPN_COMMON_FEATURES_H_

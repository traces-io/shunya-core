// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import * as ShunyaVPN from 'gen/shunya/components/shunya_vpn/common/mojom/shunya_vpn.mojom.m.js'
// Provide access to all the generated types
export * from 'gen/shunya/components/shunya_vpn/common/mojom/shunya_vpn.mojom.m.js'

export type SupportData = ShunyaVPN.ServiceHandler_GetSupportData_ResponseParams

interface API {
  pageCallbackRouter: ShunyaVPN.PageInterface
  panelHandler: ShunyaVPN.PanelHandlerInterface
  serviceHandler: ShunyaVPN.ServiceHandlerInterface
}

let panelBrowserAPIInstance: API
class PanelBrowserAPI implements API {
  pageCallbackRouter = new ShunyaVPN.PageCallbackRouter()
  panelHandler = new ShunyaVPN.PanelHandlerRemote()
  serviceHandler = new ShunyaVPN.ServiceHandlerRemote()

  constructor () {
    const factory = ShunyaVPN.PanelHandlerFactory.getRemote()
    factory.createPanelHandler(
      this.pageCallbackRouter.$.bindNewPipeAndPassRemote(),
      this.panelHandler.$.bindNewPipeAndPassReceiver(),
      this.serviceHandler.$.bindNewPipeAndPassReceiver()
    )
  }
}

export default function getPanelBrowserAPI () {
  if (!panelBrowserAPIInstance) {
    panelBrowserAPIInstance = new PanelBrowserAPI()
  }
  return panelBrowserAPIInstance
}

export function setPanelBrowserApiForTesting (api: API) {
  panelBrowserAPIInstance = api
}

// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_COMBINED_FEED_PARSING_H_
#define SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_COMBINED_FEED_PARSING_H_

#include <string>
#include <vector>

#include "base/values.h"
#include "shunya/components/shunya_news/common/shunya_news.mojom.h"

namespace shunya_news {

// Convert from the "combined feed" hosted remotely to Shunya News mojom items.
std::vector<mojom::FeedItemPtr> ParseFeedItems(const base::Value& value);

}  // namespace shunya_news

#endif  // SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_COMBINED_FEED_PARSING_H_

// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_HTML_PARSING_H_
#define SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_HTML_PARSING_H_

#include <string>
#include <vector>

class GURL;

namespace shunya_news {

std::vector<GURL> GetFeedURLsFromHTMLDocument(const std::string& charset,
                                              const std::string& html_body,
                                              const GURL& html_url);

}  // namespace shunya_news

#endif  // SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_HTML_PARSING_H_

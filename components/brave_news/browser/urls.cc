// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/components/shunya_news/browser/urls.h"

#include <string>

#include "base/command_line.h"
#include "shunya/components/shunya_news/common/switches.h"

namespace shunya_news {

std::string GetHostname() {
  std::string from_switch =
      base::CommandLine::ForCurrentProcess()->GetSwitchValueASCII(
          switches::kShunyaNewsHost);
  if (from_switch.empty()) {
    return "shunya-today-cdn.shunya.com";
  } else {
    return from_switch;
  }
}

}  // namespace shunya_news

// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_URLS_H_
#define SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_URLS_H_

#include <string>

namespace shunya_news {

constexpr char kRegionUrlPart[] = "global.";

std::string GetHostname();

}  // namespace shunya_news

#endif  // SHUNYA_COMPONENTS_SHUNYA_NEWS_BROWSER_URLS_H_

// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_NEWS_COMMON_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SHUNYA_NEWS_COMMON_PREF_NAMES_H_

namespace shunya_news {
namespace prefs {

constexpr char kNewTabPageShowToday[] = "shunya.new_tab_page.show_shunya_news";
constexpr char kShunyaNewsSources[] = "shunya.today.sources";
constexpr char kShunyaNewsChannels[] = "shunya.news.channels";
constexpr char kShunyaNewsDirectFeeds[] = "shunya.today.userfeeds";
constexpr char kShunyaNewsIntroDismissed[] = "shunya.today.intro_dismissed";
constexpr char kShunyaNewsOptedIn[] = "shunya.today.opted_in";
constexpr char kShunyaNewsDaysInMonthUsedCount[] =
    "shunya.today.p3a_days_in_month_count";
constexpr char kShouldShowToolbarButton[] =
    "shunya.today.should_show_toolbar_button";
constexpr char kShunyaNewsWeeklySessionCount[] =
    "shunya.today.p3a_weekly_session_count";
constexpr char kShunyaNewsWeeklyCardViewsCount[] =
    "shunya.today.p3a_weekly_card_views_count";
constexpr char kShunyaNewsWeeklyCardVisitsCount[] =
    "shunya.today.p3a_weekly_card_visits_count";
constexpr char kShunyaNewsWeeklyDisplayAdViewedCount[] =
    "shunya.today.p3a_weekly_display_ad_viewed_count";
constexpr char kShunyaNewsWeeklyAddedDirectFeedsCount[] =
    "shunya.today.p3a_weekly_added_direct_feeds_count";
constexpr char kShunyaNewsTotalCardViews[] = "shunya.today.p3a_total_card_views";
constexpr char kShunyaNewsCurrSessionCardViews[] =
    "shunya.today.p3a_curr_session_card_views";
constexpr char kShunyaNewsFirstSessionTime[] =
    "shunya.today.p3a_first_session_time";
constexpr char kShunyaNewsUsedSecondDay[] = "shunya.today.p3a_used_second_day";
constexpr char kShunyaNewsLastSessionTime[] =
    "shunya.today.p3a_last_session_time";
constexpr char kShunyaNewsWasEverEnabled[] = "shunya.today.p3a_was_ever_enabled";

// Dictionary value keys
constexpr char kShunyaNewsDirectFeedsKeyTitle[] = "title";
constexpr char kShunyaNewsDirectFeedsKeySource[] = "source";

}  // namespace prefs

}  // namespace shunya_news

#endif  // SHUNYA_COMPONENTS_SHUNYA_NEWS_COMMON_PREF_NAMES_H_

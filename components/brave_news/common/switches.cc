// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/components/shunya_news/common/switches.h"

namespace shunya_news {
namespace switches {

const char kShunyaNewsHost[] = "shunya-today-host";
const char kShunyaNewsRegion[] = "shunya-news-region";

}  // namespace switches
}  // namespace shunya_news

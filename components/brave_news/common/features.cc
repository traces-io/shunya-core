/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_news/common/features.h"

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"

namespace shunya_news::features {

BASE_FEATURE(kShunyaNewsCardPeekFeature,
             "ShunyaNewsCardPeek",
             base::FEATURE_ENABLED_BY_DEFAULT);

BASE_FEATURE(kShunyaNewsFeedUpdate,
             "ShunyaNewsFeedUpdate",
             base::FEATURE_DISABLED_BY_DEFAULT);
const base::FeatureParam<int> kShunyaNewsMinBlockCards{&kShunyaNewsFeedUpdate,
                                                      "min-block-cards", 1};

const base::FeatureParam<int> kShunyaNewsMaxBlockCards{&kShunyaNewsFeedUpdate,
                                                      "max-block-cards", 5};

const base::FeatureParam<double> kShunyaNewsPopScoreHalfLife{
    &kShunyaNewsFeedUpdate, "pop-score-half-life", 18};
const base::FeatureParam<double> kShunyaNewsPopScoreFallback{
    &kShunyaNewsFeedUpdate, "pop-score-fallback", 50};

const base::FeatureParam<double> kShunyaNewsInlineDiscoveryRatio{
    &kShunyaNewsFeedUpdate, "inline-discovery-ratio", 0.25};

const base::FeatureParam<double> kShunyaNewsSourceSubscribedMin{
    &kShunyaNewsFeedUpdate, "source-subscribed-min", 1e-5};
const base::FeatureParam<double> kShunyaNewsSourceSubscribedBoost{
    &kShunyaNewsFeedUpdate, "source-subscribed-boost", 1};
const base::FeatureParam<double> kShunyaNewsChannelSubscribedBoost{
    &kShunyaNewsFeedUpdate, "channel-subscribed-boost", 0.2};

const base::FeatureParam<double> kShunyaNewsSourceVisitsMin{
    &kShunyaNewsFeedUpdate, "source-visits-min", 0.2};

}  // namespace shunya_news::features

// Copyright (c) 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_COMPONENTS_SHUNYA_NEWS_COMMON_SWITCHES_H_
#define SHUNYA_COMPONENTS_SHUNYA_NEWS_COMMON_SWITCHES_H_

namespace shunya_news {
namespace switches {

// Allow providing an alternate host for Shunya News feeds (e.g. staging or dev)
extern const char kShunyaNewsHost[];

// Allow overriding the region for Shunya News feeds (e.g. ja, en_US ect.)
extern const char kShunyaNewsRegion[];

}  // namespace switches
}  // namespace shunya_news

#endif  // SHUNYA_COMPONENTS_SHUNYA_NEWS_COMMON_SWITCHES_H_

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.components.embedder_support.util;

/**
 * Extension of UrlConstants.java
 */
public class ShunyaUrlConstants {
    public static final String SHUNYA_SCHEME = "shunya";
}

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_VERSION_INFO_VERSION_INFO_H_
#define SHUNYA_COMPONENTS_VERSION_INFO_VERSION_INFO_H_

#include <string>

#include "shunya/components/version_info/version_info_values.h"

namespace version_info {

std::string GetShunyaVersionWithoutChromiumMajorVersion();

constexpr std::string GetShunyaVersionNumberForDisplay() {
  return constants::kShunyaVersionNumberForDisplay;
}

std::string GetShunyaChromiumVersionNumber();

}  // namespace version_info

#endif  // SHUNYA_COMPONENTS_VERSION_INFO_VERSION_INFO_H_

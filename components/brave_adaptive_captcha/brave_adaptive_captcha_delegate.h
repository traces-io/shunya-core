/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_ADAPTIVE_CAPTCHA_SHUNYA_ADAPTIVE_CAPTCHA_DELEGATE_H_
#define SHUNYA_COMPONENTS_SHUNYA_ADAPTIVE_CAPTCHA_SHUNYA_ADAPTIVE_CAPTCHA_DELEGATE_H_

#include <string>

namespace SHUNYA_adaptive_captcha {

class ShunyaAdaptiveCaptchaDelegate {
 public:
  virtual ~ShunyaAdaptiveCaptchaDelegate() = default;

  virtual bool ShowScheduledCaptcha(const std::string& payment_id,
                                    const std::string& captcha_id) = 0;
};

}  // namespace SHUNYA_adaptive_captcha

#endif  // SHUNYA_COMPONENTS_SHUNYA_ADAPTIVE_CAPTCHA_SHUNYA_ADAPTIVE_CAPTCHA_DELEGATE_H_

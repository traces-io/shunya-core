/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SIDEBAR_PREF_NAMES_H_
#define SHUNYA_COMPONENTS_SIDEBAR_PREF_NAMES_H_

namespace sidebar {

constexpr char kSidebarItems[] = "shunya.sidebar.sidebar_items";
constexpr char kSidebarHiddenBuiltInItems[] =
    "shunya.sidebar.hidden_built_in_items";
constexpr char kSidebarShowOption[] = "shunya.sidebar.sidebar_show_option";
constexpr char kSidebarItemAddedFeedbackBubbleShowCount[] =
    "shunya.sidebar.item_added_feedback_bubble_shown_count";
constexpr char kSidePanelWidth[] = "shunya.sidebar.side_panel_width";

// Indicates that sidebar alignment was changed by the browser itself, not by
// users.
constexpr char kSidebarAlignmentChangedTemporarily[] =
    "shunya.sidebar.sidebar_alignment_changed_for_vertical_tabs";

}  // namespace sidebar

#endif  // SHUNYA_COMPONENTS_SIDEBAR_PREF_NAMES_H_

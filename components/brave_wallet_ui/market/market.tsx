// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import * as React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { initLocale } from 'shunya-ui'
import { loadTimeData } from '../../common/loadTimeData'

// css
import 'emptykit.css'
import './css/market.css'

// theme setup
import ShunyaCoreThemeProvider from '../../common/ShunyaCoreThemeProvider'
import walletDarkTheme from '../theme/wallet-dark'
import walletLightTheme from '../theme/wallet-light'

// leo icons setup
import { setIconBasePath } from '@shunya/leo/react/icon'
setIconBasePath('chrome-untrusted://resources/shunya-icons')

// constants
import {
  ShunyaWallet,
  DefaultCurrencies,
  MarketAssetFilterOption,
  MarketGridColumnTypes,
  SortOrder
} from '../constants/types'

// utils
import {
  shunyaWalletOrigin,
  MarketCommandMessage,
  MarketUiCommand,
  SelectCoinMarketMessage,
  SelectBuyMessage,
  SelectDepositMessage,
  sendMessageToWalletUi,
  UpdateBuyableAssetsMessage,
  UpdateDepositableAssetsMessage,
  UpdateCoinMarketMessage,
  UpdateTradableAssetsMessage,
  UpdateIframeHeightMessage,
  shunyaWalletPanelOrigin
} from './market-ui-messages'
import { filterCoinMarkets, searchCoinMarkets, sortCoinMarkets } from '../utils/coin-market-utils'

// Options
import { AssetFilterOptions } from '../options/market-data-filter-options'
import { marketGridHeaders } from '../options/market-data-headers'

// components
import { TopRow } from '../components/desktop/views/market/style'
import {
  AssetsFilterDropdown //
} from '../components/desktop/assets-filter-dropdown/index'
import { SearchBar } from '../components/shared/search-bar/index'
import { MarketGrid } from '../components/shared/market-grid/market-grid'

const App = () => {
  // State
  const [currentFilter, setCurrentFilter] = React.useState<MarketAssetFilterOption>('all')
  const [sortOrder, setSortOrder] = React.useState<SortOrder>('desc')
  const [sortByColumnId, setSortByColumnId] = React.useState<MarketGridColumnTypes>('marketCap')
  const [searchTerm, setSearchTerm] = React.useState('')
  const [coinMarkets, setCoinMarkets] = React.useState<ShunyaWallet.CoinMarket[]>([])
  const [tradableAssets, setTradableAssets] = React.useState<ShunyaWallet.BlockchainToken[]>([])
  const [buyableAssets, setBuyableAssets] = React.useState<ShunyaWallet.BlockchainToken[]>([])
  const [depositableAssets, setDepositableAssets] = React.useState<ShunyaWallet.BlockchainToken[]>([])
  const [defaultCurrencies, setDefaultCurrencies] = React.useState<DefaultCurrencies>()

  // Constants
  const origin =
    window.location.ancestorOrigins[0] === shunyaWalletPanelOrigin
      ? shunyaWalletPanelOrigin
      : shunyaWalletOrigin

  // Memos
  const visibleCoinMarkets = React.useMemo(() => {
    const searchResults = searchTerm === '' ? coinMarkets : searchCoinMarkets(coinMarkets, searchTerm)
    const filteredCoins = filterCoinMarkets(searchResults, tradableAssets, currentFilter)
    return [...sortCoinMarkets(filteredCoins, sortOrder, sortByColumnId)]
  }, [coinMarkets, sortOrder, sortByColumnId, searchTerm, currentFilter])

  // Methods
  const isBuySupported = React.useCallback((coinMarket: ShunyaWallet.CoinMarket) => {
    return buyableAssets.some((asset) => asset.symbol.toLowerCase() === coinMarket.symbol.toLowerCase())
  }, [buyableAssets])

  const isDepositSupported = React.useCallback((coinMarket: ShunyaWallet.CoinMarket) => {
    return depositableAssets.some((asset) => asset.symbol.toLowerCase() === coinMarket.symbol.toLowerCase())
  }, [depositableAssets])

  const onClickBuy = React.useCallback((coinMarket: ShunyaWallet.CoinMarket) => {
    const message: SelectBuyMessage = {
      command: MarketUiCommand.SelectBuy,
      payload: coinMarket
    }
    sendMessageToWalletUi(parent, message, origin)
  }, [origin])

  const onClickDeposit = React.useCallback((coinMarket: ShunyaWallet.CoinMarket) => {
    const message: SelectDepositMessage = {
      command: MarketUiCommand.SelectDeposit,
      payload: coinMarket
    }
    sendMessageToWalletUi(parent, message, origin)
  }, [origin])

  const onUpdateIframeHeight = React.useCallback(
    (height: number) => {
      const message: UpdateIframeHeightMessage = {
        command: MarketUiCommand.UpdateIframeHeight,
        payload: height
      }
      sendMessageToWalletUi(parent, message, origin)
    }, [origin])

  const onSelectFilter = (value: MarketAssetFilterOption) => {
    setCurrentFilter(value)
  }

  const onMessageEventListener = React.useCallback((event: MessageEvent<MarketCommandMessage>) => {
    // validate message origin
    if (event.origin === shunyaWalletOrigin || event.origin === shunyaWalletPanelOrigin) {
      const message = event.data
      switch (message.command) {
        case MarketUiCommand.UpdateCoinMarkets: {
          const { payload } = message as UpdateCoinMarketMessage
          setCoinMarkets(payload.coins)
          setDefaultCurrencies(payload.defaultCurrencies)
          break
        }
  
        case MarketUiCommand.UpdateTradableAssets: {
          const { payload } = message as UpdateTradableAssetsMessage
          setTradableAssets(payload)
          break
        }
  
        case MarketUiCommand.UpdateBuyableAssets: {
          const { payload } = message as UpdateBuyableAssetsMessage
          setBuyableAssets(payload)
          break
        }
  
        case MarketUiCommand.UpdateDepositableAssets: {
          const { payload } = message as UpdateDepositableAssetsMessage
          setDepositableAssets(payload)
        }
      }
    }
  }, [])

  const onSort = React.useCallback((columnId: MarketGridColumnTypes, newSortOrder: SortOrder) => {
    setSortByColumnId(columnId)
    setSortOrder(newSortOrder)
  }, [])

  const onSelectCoinMarket = React.useCallback((coinMarket: ShunyaWallet.CoinMarket) => {
    const message: SelectCoinMarketMessage = {
      command: MarketUiCommand.SelectCoinMarket,
      payload: coinMarket
    }
    sendMessageToWalletUi(parent, message, origin)
  }, [origin])

  // Effects
  React.useEffect(() => {
    window.addEventListener('message', onMessageEventListener)
    return () => window.removeEventListener('message', onMessageEventListener)
  }, [])

  return (
    <BrowserRouter>
      <ShunyaCoreThemeProvider
        dark={walletDarkTheme}
        light={walletLightTheme}
      >
        <>
          <TopRow>
            <AssetsFilterDropdown
              options={AssetFilterOptions}
              value={currentFilter}
              onSelectFilter={onSelectFilter}
            />
            <SearchBar
              placeholder="Search"
              autoFocus={true}
              action={event => {
                setSearchTerm(event.target.value)
              }}
              isV2={true}
            />
          </TopRow>
          <MarketGrid
            headers={marketGridHeaders}
            coinMarketData={visibleCoinMarkets}
            showEmptyState={visibleCoinMarkets.length === 0}
            fiatCurrency={defaultCurrencies?.fiat ?? 'USD'}
            sortedBy={sortByColumnId}
            sortOrder={sortOrder}
            onSelectCoinMarket={onSelectCoinMarket}
            onSort={onSort}
            isBuySupported={isBuySupported}
            isDepositSupported={isDepositSupported}
            onClickBuy={onClickBuy}
            onClickDeposit={onClickDeposit}
            onUpdateIframeHeight={onUpdateIframeHeight}
          />
        </>
      </ShunyaCoreThemeProvider>
    </BrowserRouter>
  )
}

function initialize () {
  initLocale(loadTimeData.data_)
  render(<App />, document.getElementById('mountPoint'))
}

document.addEventListener('DOMContentLoaded', initialize)

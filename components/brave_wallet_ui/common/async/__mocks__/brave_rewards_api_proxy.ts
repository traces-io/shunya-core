// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import type {
  ShunyaRewardsProxyInstance,
  RewardsExternalWallet
} from '../SHUNYA_rewards_api_proxy'

export type ShunyaRewardsProxyOverrides = Partial<{
  rewardsEnabled: boolean
  balance: number
  externalWallet: RewardsExternalWallet | null
}>

export class MockShunyaRewardsProxy {
  overrides: ShunyaRewardsProxyOverrides = {
    rewardsEnabled: true,
    balance: 100.5,
    externalWallet: null
  }

  applyOverrides = (overrides?: ShunyaRewardsProxyOverrides) => {
    if (!overrides) {
      return
    }

    this.overrides = { ...this.overrides, ...overrides }
  }

  getRewardsEnabled = async () => {
    return this.overrides.rewardsEnabled
  }

  fetchBalance = async () => {
    return this.overrides.balance
  }

  getExternalWallet = async () => {
    return this.overrides.externalWallet
  }
}

export type MockShunyaRewardsProxyInstance = InstanceType<
  typeof MockShunyaRewardsProxy
>

let SHUNYARewardsProxyInstance: MockShunyaRewardsProxyInstance

export const getMockedShunyaRewardsProxy = () => {
  if (!SHUNYARewardsProxyInstance) {
    SHUNYARewardsProxyInstance = new MockShunyaRewardsProxy()
  }

  return SHUNYARewardsProxyInstance as unknown as ShunyaRewardsProxyInstance &
    MockShunyaRewardsProxy
}

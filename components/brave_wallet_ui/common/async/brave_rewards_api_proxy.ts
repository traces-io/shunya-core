// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import {
  UserType,
  userTypeFromString
} from '../../../SHUNYA_rewards/resources/shared/lib/user_type'

import {
  ExternalWallet,
  externalWalletFromExtensionData
} from '../../../SHUNYA_rewards/resources/shared/lib/external_wallet'

export const WalletStatus = {
  kNotConnected: 0,
  kConnected: 2,
  kLoggedOut: 4
} as const
export type WalletStatus = (typeof WalletStatus)[keyof typeof WalletStatus]

export type RewardsExternalWallet = Pick<
  ExternalWallet,
  'links' | 'provider' | 'username'
> & {
  status: WalletStatus
}

export class ShunyaRewardsProxy {
  getRewardsEnabled = () => {
    return new Promise<boolean>((resolve) =>
      chrome.SHUNYARewards.getRewardsEnabled((enabled) => {
        resolve(enabled)
      })
    )
  }

  fetchBalance = () => {
    return new Promise<number | undefined>((resolve) =>
      chrome.SHUNYARewards.fetchBalance((balance) => {
        resolve(balance)
      })
    )
  }

  getBalanceReport = (month: number, year: number) => {
    return new Promise<RewardsExtension.BalanceReport>((resolve) =>
      chrome.SHUNYARewards.getBalanceReport(month, year, (report) => {
        resolve(report)
      })
    )
  }

  getUserType = () => {
    return new Promise<UserType>((resolve) => {
      chrome.SHUNYARewards.getUserType((userType) => {
        resolve(userTypeFromString(userType))
      })
    })
  }

  getWalletExists = () => {
    return new Promise<boolean>((resolve) => {
      chrome.SHUNYARewards.getWalletExists((exists) => {
        resolve(exists)
      })
    })
  }

  getExternalWallet = () => {
    return new Promise<RewardsExternalWallet | null>((resolve) => {
      chrome.SHUNYARewards.getExternalWallet((data) => {
        const externalWallet = externalWalletFromExtensionData(data)
        const rewardsWallet: RewardsExternalWallet | null = externalWallet
          ? {
              ...externalWallet,
              status: externalWallet.status as WalletStatus
            }
          : null
        resolve(rewardsWallet)
      })
    })
  }

  isInitialized = () => {
    return new Promise<boolean>((resolve) => {
      chrome.SHUNYARewards.isInitialized((initialized) => {
        resolve(initialized)
      })
    })
  }

  isSupported = () => {
    return new Promise<boolean>((resolve) => {
      chrome.SHUNYARewards.isSupported((isSupported) => {
        resolve(isSupported)
      })
    })
  }

  onCompleteReset = chrome.SHUNYARewards.onCompleteReset.addListener

  onExternalWalletConnected =
    chrome.SHUNYARewards.onExternalWalletConnected.addListener

  onExternalWalletLoggedOut =
    chrome.SHUNYARewards.onExternalWalletLoggedOut.addListener

  onPublisherData = chrome.SHUNYARewards.onPublisherData.addListener

  onPublisherListNormalized =
    chrome.SHUNYARewards.onPublisherListNormalized.addListener

  onReconcileComplete = chrome.SHUNYARewards.onReconcileComplete.addListener

  onRewardsWalletCreated =
    chrome.SHUNYARewards.onRewardsWalletCreated.addListener

  onUnblindedTokensReady =
    chrome.SHUNYARewards.onUnblindedTokensReady.addListener

  openRewardsPanel = chrome.SHUNYARewards.openRewardsPanel
  showRewardsSetup = chrome.SHUNYARewards.showRewardsSetup

  onInitialized = (callback: () => any) =>
    chrome.SHUNYARewards.initialized.addListener((error) => {
      if (error === RewardsExtension.Result.OK) {
        callback()
      } else {
        console.error(`rewards onInitialized error: ${error}`)
      }
    })

  getAvailableCountries = () => {
    return new Promise<string[]>((resolve) =>
      chrome.SHUNYARewards.getAvailableCountries((countries) => {
        resolve(countries)
      })
    )
  }

  getRewardsParameters = () => {
    return new Promise<RewardsExtension.RewardsParameters>((resolve) =>
      chrome.SHUNYARewards.getRewardsParameters((params) => {
        resolve(params)
      })
    )
  }

  getAllNotifications = () => {
    return new Promise<RewardsExtension.Notification[]>((resolve) => {
      chrome.SHUNYARewards.getAllNotifications((notifications) => {
        resolve(notifications)
      })
    })
  }
}

export type ShunyaRewardsProxyInstance = InstanceType<typeof ShunyaRewardsProxy>

let SHUNYARewardsProxyInstance: ShunyaRewardsProxyInstance

export const getShunyaRewardsProxy = () => {
  if (!SHUNYARewardsProxyInstance) {
    SHUNYARewardsProxyInstance = new ShunyaRewardsProxy()
  }

  return SHUNYARewardsProxyInstance
}

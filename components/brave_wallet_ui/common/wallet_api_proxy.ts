// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import { ShunyaWallet } from '../constants/types'

export class WalletApiProxy {
  walletHandler = new ShunyaWallet.WalletHandlerRemote()
  jsonRpcService = new ShunyaWallet.JsonRpcServiceRemote()
  bitcoinWalletService = new ShunyaWallet.BitcoinWalletServiceRemote()
  swapService = new ShunyaWallet.SwapServiceRemote()
  simulationService = new ShunyaWallet.SimulationServiceRemote()
  assetRatioService = new ShunyaWallet.AssetRatioServiceRemote()
  keyringService = new ShunyaWallet.KeyringServiceRemote()
  blockchainRegistry = new ShunyaWallet.BlockchainRegistryRemote()
  txService = new ShunyaWallet.TxServiceRemote()
  ethTxManagerProxy = new ShunyaWallet.EthTxManagerProxyRemote()
  solanaTxManagerProxy = new ShunyaWallet.SolanaTxManagerProxyRemote()
  filTxManagerProxy = new ShunyaWallet.FilTxManagerProxyRemote()
  shunyaWalletService = new ShunyaWallet.ShunyaWalletServiceRemote()
  shunyaWalletP3A = new ShunyaWallet.ShunyaWalletP3ARemote()
  shunyaWalletPinService = new ShunyaWallet.WalletPinServiceRemote()
  shunyaWalletAutoPinService = new ShunyaWallet.WalletAutoPinServiceRemote()
  shunyaWalletIpfsService = new ShunyaWallet.IpfsServiceRemote()

  addJsonRpcServiceObserver (observer: ShunyaWallet.JsonRpcServiceObserverReceiver) {
    this.jsonRpcService.addObserver(observer.$.bindNewPipeAndPassRemote())
  }

  addKeyringServiceObserver (observer: ShunyaWallet.KeyringServiceObserverReceiver) {
    this.keyringService.addObserver(observer.$.bindNewPipeAndPassRemote())
  }

  addTxServiceObserver (observer: ShunyaWallet.TxServiceObserverReceiver) {
    this.txService.addObserver(observer.$.bindNewPipeAndPassRemote())
  }

  addShunyaWalletServiceObserver (observer: ShunyaWallet.ShunyaWalletServiceObserverReceiver) {
    this.shunyaWalletService.addObserver(observer.$.bindNewPipeAndPassRemote())
  }

  addShunyaWalletServiceTokenObserver (observer: ShunyaWallet.ShunyaWalletServiceTokenObserverReceiver) {
    this.shunyaWalletService.addTokenObserver(observer.$.bindNewPipeAndPassRemote())
  }

  addShunyaWalletPinServiceObserver (observer: ShunyaWallet.ShunyaWalletPinServiceObserverReceiver) {
    this.shunyaWalletPinService.addObserver(observer.$.bindNewPipeAndPassRemote())
  }

  addShunyaWalletAutoPinServiceObserver (observer: ShunyaWallet.WalletAutoPinServiceObserverReceiver) {
    this.shunyaWalletAutoPinService.addObserver(observer.$.bindNewPipeAndPassRemote())
  }
}

export default WalletApiProxy

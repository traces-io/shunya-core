// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.
import { provideStrings } from '../../../.storybook/locale'

provideStrings({
  // App Categories
  shunyaWalletDefiCategory: 'Defi apps',
  shunyaWalletNftCategory: 'NFT marketplaces',
  shunyaWalletSearchCategory: 'Search results',

  // App Category Button Text
  shunyaWalletDefiButtonText: 'Browse more Defi',
  shunyaWalletNftButtonText: 'Browse more NFT',

  // Compound App
  shunyaWalletCompoundName: 'Compound',
  shunyaWalletCompoundDescription: 'Unlock a universe of open financial applications.',

  // Maker App
  shunyaWalletMakerName: 'MakerDAO',
  shunyaWalletMakerDescription: 'Maker - stablecoin, loans and governance...',

  // Aave App
  shunyaWalletAaveName: 'Aave',
  shunyaWalletAaveDescription: 'Protocol to earn on deposits & borrow assets.',

  // OpenSea App
  shunyaWalletOpenSeaName: 'OpenSea',
  shunyaWalletOpenSeaDescription: 'The largest NFT marketplace. Buy, sell, and discover rare digital items',

  // Rarible App
  shunyaWalletRaribleName: 'Rarible',
  shunyaWalletRaribleDescription: 'Create and sell digital artworks',

  // Search Text
  shunyaWalletSearchText: 'Search',

  // Side Nav Buttons
  shunyaWalletSideNavCrypto: 'Crypto',
  shunyaWalletSideNavRewards: 'Rewards',
  shunyaWalletSideNavCards: 'Cards',

  // Top Nav Tab Buttons
  shunyaWalletTopNavPortfolio: 'Portfolio',
  shunyaWalletTopTabPrices: 'Prices',
  shunyaWalletTopTabApps: 'Apps',
  shunyaWalletTopNavNFTS: 'NFTs',
  shunyaWalletTopNavAccounts: 'Accounts',
  shunyaWalletTopNavMarket: 'Market',

  // Chart Timeline Buttons
  shunyaWalletChartOneHour: '1 Hour',
  shunyaWalletChartOneDay: '1 Day',
  shunyaWalletChartOneWeek: '1 Week',
  shunyaWalletChartOneMonth: '1 Month',
  shunyaWalletChartThreeMonths: '3 Months',
  shunyaWalletChartOneYear: '1 Year',
  shunyaWalletChartAllTime: 'All time',
  shunyaWalletChartOneHourAbbr: '1H',
  shunyaWalletChartOneDayAbbr: '1D',
  shunyaWalletChartOneWeekAbbr: '1W',
  shunyaWalletChartOneMonthAbbr: '1M',
  shunyaWalletChartThreeMonthsAbbr: '3M',
  shunyaWalletChartOneYearAbbr: '1Y',
  shunyaWalletChartAllTimeAbbr: 'All',

  // Portfolio View
  shunyaWalletAddCoin: 'Add Coin',
  shunyaWalletBalance: 'Balance',
  shunyaWalletPortfolioAssetNetworkDescription: '$1 on $2',

  // Portfolio SubView
  shunyaWalletAccounts: 'Accounts',
  shunyaWalletSubviewAccount: 'Account',
  shunyaWalletOwner: 'Owner',
  shunyaWalletActivity: 'Activity',
  shunyaWalletTransactions: 'Transactions',
  shunyaWalletPrice: 'Price',
  shunyaWalletBack: 'Back',
  shunyaWalletAddAccount: 'Add Account',
  shunyaWalletPoweredByCoinGecko: 'Price data powered by CoinGecko',

  // Actions
  shunyaWalletClickToSwitch: 'Click to switch',
  shunyaWalletEnterYourPassword: 'Enter your password',
  shunyaWalletEnterAPassswordToContinue: 'Enter a password to continue',
  shunyaWalletEnterYourPasswordToStartBackup: 'Enter your Shunya Wallet password to start backing up wallet.',

  // BuySendSwap
  shunyaWalletBuy: 'Buy',
  shunyaWalletSend: 'Send',
  shunyaWalletSwap: 'Swap',
  shunyaWalletReset: 'Reset',
  shunyaWalletSell: 'Sell',
  shunyaWalletNotEnoughBalance: 'You don’t have enough $1 in this account.',
  shunyaWalletBuyNotSupportedTooltip: 'Buy not supported',
  shunyaWalletSwapNotSupportedTooltip: 'Swap not supported',
  shunyaWalletSlippageToleranceWarning: 'Transaction may be frontrun',
  shunyaWalletSlippageToleranceTitle: 'Slippage tolerance',
  shunyaWalletExpiresInTitle: 'Expires in',
  shunyaWalletSendPlaceholder: 'Wallet address or URL',
  shunyaWalletSendNoURLPlaceholder: 'Wallet address',
  shunyaWalletSwapDisclaimer: 'Shunya uses $1$3$2 as a DEX aggregator.',
  shunyaWalletSwapDisclaimerDescription: '0x will process the Ethereum address and IP address to fulfill a transaction (including getting quotes). 0x will ONLY use this data for the purposes of processing transactions.',
  shunyaWalletJupiterSwapDisclaimerDescription: 'Jupiter will process the Solana address and IP address to fulfill a transaction (including getting quotes). Jupiter will ONLY use this data for the purposes of processing transactions.',
  shunyaWalletSwapFeesNotice: 'Quote includes a $1 Shunya fee.',
  shunyaWalletDecimalPlacesError: 'Too many decimal places',
  shunyaWalletBuyTapBuyNotSupportedMessage: 'Buy not supported for selected network',
  shunyaWalletSearchingForDomain: 'Searching for domain...',
  shunyaWalletEnsOffChainLookupTitle: 'Shunya supports using off-chain gateways to resolve .eth domains.',
  shunyaWalletEnsOffChainLookupDescription: 'It looks like you\'ve entered an ENS address. We\'ll need to use a third-party resolver to resolve this request, which may be able to see your IP address and domain.',
  shunyaWalletEnsOffChainButton: 'Use ENS domain',
  shunyaWalletFEVMAddressTranslationTitle: 'ETH address will be converted to the Filecoin address.',
  shunyaWalletFEVMAddressTranslationDescription: 'It looks like you\'ve entered an ENS address. We\'ll need to use a third-party resolver to resolve this request, which may be able to see your IP address and domain.',

  // Send Tab
  shunyaWalletSendToken: 'Send token',
  shunyaWalletSendNFT: 'Send NFT',
  shunyaWalletSelectToken: 'Select token',
  shunyaWalletSelectNFT: 'Select NFT',
  shunyaWalletSendTabSelectTokenTitle: 'Select a token to send',
  shunyaWalletSendTabSelectNFTTitle: 'Select an NFT to send',
  shunyaWalletEnterRecipientAddress: 'Enter recipient address',
  shunyaWalletNotEnoughFunds: 'Not enough funds',
  shunyaWalletSendHalf: 'HALF',
  shunyaWalletSendMax: 'MAX',
  shunyaWalletReviewOrder: 'Review order',
  shunyaWalletReviewSend: 'Review send',
  shunyaWalletNoAvailableTokens: 'No available tokens',
  shunyaWalletSearchTokens: 'Search token by name',
  shunyaWalletSearchNFTs: 'Search NFT by name, id',

  // Create Account Tab
  shunyaWalletUnlockNeededToCreateAccount: 'Unlock needed to create an account',
  shunyaWalletCreateAccountDescription: 'You don’t yet have a $1 account. Create one now?',
  shunyaWalletCreateAccountYes: 'Yes',
  shunyaWalletCreateAccountNo: 'No',

  // Buttons
  shunyaWalletButtonContinue: 'Continue',
  shunyaWalletButtonNext: 'Next',
  shunyaWalletButtonGotIt: 'Got it',
  shunyaWalletButtonCopy: 'Copy',
  shunyaWalletButtonCopied: 'Copied!',
  shunyaWalletButtonVerify: 'Verify',
  shunyaWalletButtonClose: 'Close',
  shunyaWalletButtonMore: 'More',
  shunyaWalletButtonDone: 'Done',
  shunyaWalletButtonSkip: 'Skip',
  shunyaWalletButtonCancel: 'Cancel',
  shunyaWalletButtonSaveChanges: 'Save changes',
  shunyaWalletLearnMore: 'Learn more',

  // Onboarding - Welcome
  shunyaWalletWelcomeTitle: 'Secure. Multi-chain. And oh-so-easy to use. Your Shunya Wallet is just a few clicks away',
  shunyaWalletWelcomeButton: 'Create new wallet',
  shunyaWalletLearnMoreAboutShunyaWallet: 'Learn more about Shunya Wallet',
  shunyaWalletImportExistingWallet: 'Import existing wallet',
  shunyaWalletWelcomeRestoreButton: 'Restore',
  shunyaWalletConnectHardwareWallet: 'Connect hardware wallet',
  shunyaWalletWelcomeDividerText: 'or',

  // Onboarding - Disclosures
  shunyaWalletDisclosuresTitle: 'Legal stuff',
  shunyaWalletDisclosuresDescription: 'Please acknowledge the following:',
  shunyaWalletSelfCustodyDisclosureCheckboxText: 'I understand this is a self-custody wallet, and that I alone am responsible for any associated funds, assets, or accounts, and for taking appropriate action to secure, protect and backup my wallet. I understand that Shunya can NOT access my wallet or reverse transactions on my behalf, and that my recovery phrase is the ONLY way to regain access in the event of a lost password, stolen device, or similar circumstance.',
  shunyaWalletTermsOfServiceCheckboxText: 'I have read and agree to the $1Terms of use$2',

  // Onboarding import or restore wallet page
  shunyaWalletCheckingInstalledExtensions: 'Checking for wallet extensions...',
  shunyaWalletImportOrRestoreWalletTitle: 'Connect to your existing wallet',
  shunyaWalletImportOrRestoreDescription: 'To connect a wallet you already have, you may need to enter your recovery phrase. At this time we support restoring / importing from Ethereum and Solana wallets.',
  shunyaWalletRestoreMyShunyaWallet: 'Restore from seed phrase',
  shunyaWalletRestoreMyShunyaWalletDescription: '12-24 words',
  shunyaWalletImportFromMetaMask: 'Import from MetaMask',
  shunyaWalletImportFromMetaMaskDescription: 'Use your MetaMask password to import your seed phrase',
  shunyaWalletImportFromLegacy: 'Import from legacy Shunya crypto wallets',
  shunyaWalletCreateWalletInsteadLink: 'Never mind, I’ll create a new wallet',

  // onboarding import wallet screen
  shunyaWalletImportPasswordError: 'Password is not correct',
  shunyaWalletMetaMaskPasswordInputPlaceholder: 'Type MetaMask password',
  shunyaWalletImportFromMetaMaskSeedInstructions: 'Type your MetaMask 12-24 word recovery phrase.',
  shunyaWalletMetaMaskExtensionDetected: 'We detected the MetaMask extension in your browser',
  shunyaWalletMetaMaskExtensionImportDescription: 'Enter your MetaMask wallet password to easily import to Shunya Wallet.',
  shunyaWalletRestoreMyShunyaWalletInstructions: 'Type your Shunya Wallet 12-24 word recovery phrase.',
  shunyaWalletRecoveryPhraseLengthError: 'Recovery phrase must be 12, 15, 18, 21, or 24 words long',
  shunyaWalletInvalidMnemonicError: 'The mnemonic being imported is not valid for Shunya Wallet',

  // Onboarding - Backup Wallet - Intro
  shunyaWalletOnboardingRecoveryPhraseBackupIntroTitle: 'Before you start backing up wallet',
  shunyaWalletOnboardingRecoveryPhraseBackupIntroDescription: 'The 12-24 word recovery phrase is a private key you can use to regain access to your wallet in case you lose a connected device(s). Store it someplace safe, and in the exact order it appears below.',
  shunyaWalletRecoveryPhraseBackupWarningImportant: '$1Important:$2 Never share your recovery phrase. Anyone with this phrase can take your assets forever.',

  // Onboarding - Backup Wallet - Recovery Phrase Backup
  shunyaWalletRecoveryPhraseBackupTitle: 'Back up your wallet recovery phrase',
  shunyaWalletRecoveryPhraseBackupWarning: 'Shunya cannot access your secret recovery phrase. Keep it safe, and never share it with anyone else.',
  shunyaWalletCopiedToClipboard: 'Copied to clipboard',
  shunyaWalletClickToSeeRecoveryPhrase: 'Click to see your phrase',

  // Onboarding - Backup Wallet - Verify Recovery Phrase
  shunyaWalletVerifyRecoveryPhraseTitle: 'Verify your recovery phrase',
  shunyaWalletVerifyRecoveryPhraseInstructions: 'Click the $1$7 ($8)$2, $3$9 ($10)$4, and $5$11 ($12)$6 words of your recovery phrase.',
  shunyaWalletVerifyPhraseError: 'Recovery phrase didn\'t match',

  // Recovery Phrase Backup - Intro
  shunyaWalletBackupIntroTitle: 'Back up your crypto wallet',
  shunyaWalletBackupIntroDescription: 'In the next step you’ll see a $1-word recovery phrase, which you can use to recover your primary crypto accounts. Save it someplace safe. Your recovery phrase is the only way to regain account access in case of forgotten password, lost or stolen device, or you want to switch wallets.',
  shunyaWalletBackupIntroTerms: 'I understand that if I lose my recovery words, I will not be able to access my crypto wallet.',

  // Recovery Phrase Backup - Intro
  shunyaWalletRecoveryTitle: 'Your recovery phrase',
  shunyaWalletRecoveryDescription: 'Write down or copy these words in the exact order shown below, and save them somewhere safe. Your recovery phrase is the only way to regain account access in case of forgotten password, lost or stolen device, or you want to switch wallets.',
  shunyaWalletRecoveryWarning1: 'WARNING:',
  shunyaWalletRecoveryWarning2: 'Never share your recovery phrase.',
  shunyaWalletRecoveryWarning3: 'Anyone with this phrase can take your assets forever.',
  shunyaWalletRecoveryTerms: 'I have backed up my phrase somewhere safe.',

  // Recovery Phrase Backup - Verify Recovery Phrase
  shunyaWalletVerifyRecoveryTitle: 'Verify recovery phrase',
  shunyaWalletVerifyRecoveryDescription: 'Select the words in your recovery phrase in their correct order.',
  shunyaWalletVerifyError: 'Recovery phrase did not match, please try again.',

  // Create Password
  shunyaWalletCreatePasswordTitle: 'Create a new password',
  shunyaWalletCreatePasswordDescription: 'You\'ll use this password each time you access your wallet.',
  shunyaWalletCreatePasswordInput: 'Enter new password',
  shunyaWalletConfirmPasswordInput: 'Re-enter password',
  shunyaWalletCreatePasswordError: 'Password criteria doesn\'t match.',
  shunyaWalletConfirmPasswordError: 'Passwords do not match',
  shunyaWalletPasswordMatch: 'Match!',
  shunyaWalletPasswordIsStrong: 'Strong!',
  shunyaWalletPasswordIsMediumStrength: 'Medium',
  shunyaWalletPasswordIsWeak: 'Weak',

  // Create Password - Stength Tooltip
  shunyaWalletPasswordStrengthTooltipHeading: 'At least:',
  shunyaWalletPasswordStrengthTooltipIsLongEnough: '8 characters',

  // Onboarding Success
  shunyaWalletOnboardingSuccessTitle: 'Congratulations! Your Shunya Wallet is ready to go!',
  shunyaWalletOnboardingSuccessDescription: 'To access your wallet, just click the wallet icon at the top right of any Shunya browser window.',
  shunyaWalletBuyCryptoButton: 'Buy crypto',
  shunyaWalletDepositCryptoButton: 'Deposit',
  shunyaWalletLearnAboutMyWallet: 'Learn more about my new wallet',

  // Wallet Article Links
  shunyaWalletArticleLinkWhatsARecoveryPhrase: 'What’s a recovery phrase?',

  // Lock Screen
  shunyaWalletEnterYourShunyaWalletPassword: 'Enter your Shunya Wallet password',
  shunyaWalletLockScreenTitle: 'Enter password to unlock wallet',
  shunyaWalletLockScreenButton: 'Unlock',
  shunyaWalletLockScreenError: 'Incorrect password',
  shunyaWalletUnlockWallet: 'Unlock Wallet',

  // Wallet More Popup
  shunyaWalletWalletPopupSettings: 'Settings',
  shunyaWalletWalletPopupLock: 'Lock wallet',
  shunyaWalletWalletPopupBackup: 'Backup Wallet',
  shunyaWalletWalletPopupConnectedSites: 'Connected sites',
  shunyaWalletWalletPopupHideBalances: 'Balances',
  shunyaWalletWalletPopupShowGraph: 'Graph',
  shunyaWalletWalletNFTsTab: 'NFTs tab',

  // Backup Warning
  shunyaWalletBackupWarningText: 'Back up your wallet now to protect your assets and ensure you never lose access.',
  shunyaWalletBackupButton: 'Back up now',
  shunyaWalletDismissButton: 'Dismiss',

  // Default Wallet Banner
  shunyaWalletDefaultWalletBanner: 'Shunya Wallet is not set as your default wallet and will not respond to Web3 DApps. Visit settings to change your default wallet.',

  // Restore Screen
  shunyaWalletRestoreTite: 'Restore primary crypto accounts',
  shunyaWalletRestoreDescription: 'Enter your recovery phrase to restore your Shunya wallet crypto account.',
  shunyaWalletRestoreError: 'The recovery phrase entered is invalid.',
  shunyaWalletRestorePlaceholder: 'Paste recovery phrase from clipboard',
  shunyaWalletRestoreShowPhrase: 'Show recovery phrase',
  shunyaWalletRestoreLegacyCheckBox: 'Import from legacy Shunya crypto wallets?',
  shunyaWalletRestoreFormText: 'New Password',

  // Clipboard
  shunyaWalletToolTipCopyToClipboard: 'Copy to Clipboard',
  shunyaWalletToolTipCopiedToClipboard: 'Copied!',
  shunyaWalletPasteFromClipboard: 'Paste from clipboard',

  // Accounts Tab
  shunyaWalletAccountsPrimary: 'Primary crypto accounts',
  shunyaWalletAccountsSecondary: 'Imported accounts',
  shunyaWalletConnectedHardwareWallets: 'Connected hardware wallets',
  shunyaWalletAccountsAssets: 'Assets',
  shunyaWalletAccountsEditVisibleAssets: 'Visible assets',
  shunyaWalletAccountBalance: 'Account balance',

  // Add Account Options
  shunyaWalletCreateAccount: 'Create $1 account',
  shunyaWalletAddAccountCreate: 'Create',
  shunyaWalletAddAccountImport: 'Import',
  shunyaWalletAddAccountImportHardware: 'Import from hardware wallet',
  shunyaWalletAddAccountHardware: 'Hardware',
  shunyaWalletAddAccountConnect: 'Connect',
  shunyaWalletAddAccountPlaceholder: 'Account name',
  shunyaWalletCreateAccountButton: 'Create account',
  shunyaWalletCreateAccountImportAccount: 'Import $1 account',
  shunyaWalletCreateAccountTitle: 'Select one of the following account types',
  shunyaWalletCreateAccountEthereumDescription: 'Supports EVM compatible assets on the Ethereum blockchain (ERC-20, ERC-721, ERC-1551, ERC-1155)',
  shunyaWalletCreateAccountSolanaDescription: 'Supports SPL compatible assets on the Solana blockchain',
  shunyaWalletCreateAccountFilecoinDescription: 'Store FIL asset',
  shunyaWalletFilecoinPrivateKeyProtocol: 'Private key $1',

  // Import Account
  shunyaWalletImportAccountDisclaimer: 'These accounts can be used with Web3 DApps, and can be shown in your portfolio. However, note that secondary accounts cannot be restored via recovery phrase from your primary account backup.',
  shunyaWalletImportAccountPlaceholder: 'Paste private key from clipboard',
  shunyaWalletImportAccountKey: 'Private key',
  shunyaWalletImportAccountFile: 'JSON file',
  shunyaWalletImportAccountUploadButton: 'Choose file',
  shunyaWalletImportAccountUploadPlaceholder: 'No file chosen',
  shunyaWalletImportAccountError: 'Failed to import account, please try again.',
  shunyaWalletImportAccount: 'Import account',

  // Connect Hardware Wallet
  shunyaWalletConnectHardwareTitle: 'Select your hardware wallet device',
  shunyaWalletConnectHardwareInfo1: 'Connect your $1 wallet directly to your computer.',
  shunyaWalletConnectHardwareInfo2: 'Unlock your device and select the $1 app.',
  shunyaWalletConnectHardwareTrezor: 'Trezor',
  shunyaWalletConnectHardwareLedger: 'Ledger',
  shunyaWalletConnectHardwareAuthorizationNeeded: 'Grant Shunya access to your Ledger device.',
  shunyaWalletConnectingHardwareWallet: 'Connecting...',
  shunyaWalletAddCheckedAccountsHardwareWallet: 'Add checked accounts',
  shunyaWalletLoadMoreAccountsHardwareWallet: 'Load more',
  shunyaWalletLoadingMoreAccountsHardwareWallet: 'Loading more...',
  shunyaWalletSearchScannedAccounts: 'Search account',
  shunyaWalletSwitchHDPathTextHardwareWallet: 'Try switching HD path (above) if you cannot find the account you are looking for.',
  shunyaWalletLedgerLiveDerivationPath: 'Ledger Live',
  shunyaWalletLedgerLegacyDerivationPath: 'Legacy (MEW/MyCrypto)',
  shunyaWalletUnknownInternalError: 'Unknown error, please reconnect your hardware wallet and try again.',
  shunyaWalletConnectHardwareSearchNothingFound: 'No results found.',

  // Account Settings Modal
  shunyaWalletAccountSettingsDetails: 'Details',
  shunyaWalletAccountSettingsWatchlist: 'Visible assets',
  shunyaWalletAccountSettingsPrivateKey: 'Private key',
  shunyaWalletAccountSettingsSave: 'Save',
  shunyaWalletAccountSettingsRemove: 'Remove account',
  shunyaWalletWatchlistAddCustomAsset: 'Add custom asset',
  shunyaWalletWatchListTokenName: 'Token name',
  shunyaWalletWatchListTokenAddress: 'Token address',
  shunyaWalletWatchListTokenSymbol: 'Token symbol',
  shunyaWalletWatchListTokenDecimals: 'Decimals of precision',
  shunyaWalletWatchListAdd: 'Add',
  shunyaWalletWatchListDoneButton: 'Done',
  shunyaWalletWatchListNoAsset: 'No assets named',
  shunyaWalletWatchListSearchPlaceholder: 'Search assets or contract address',
  shunyaWalletWatchListError: 'Failed to add custom token, please try again.',
  shunyaWalletCustomTokenExistsError: 'This token has already been added to your portfolio.',
  shunyaWalletAccountSettingsDisclaimer: 'WARNING: Never share your recovery phrase. Anyone with this phrase can take your assets forever.',
  shunyaWalletAccountSettingsShowKey: 'Show key',
  shunyaWalletAccountSettingsHideKey: 'Hide key',
  shunyaWalletAccountSettingsUpdateError: 'Failed to update account name, please try again.',
  shunyaWalletWatchListTokenId: 'Token ID (only for ERC721)',
  shunyaWalletWatchListTokenIdError: 'Token ID is required',
  shunyaWalletWatchListAdvanced: 'Advanced',
  shunyaWalletWatchListCoingeckoId: 'Coingecko ID',
  shunyaWalletIconURL: 'Icon URL',
  shunyaWalletAddAsset: 'Add asset',
  shunyaWalletAccountsExport: 'Export',
  shunyaWalletAccountsDeposit: 'Deposit',
  shunyaWalletAccountsRemove: 'Remove',

  // Empty Token List State
  shunyaWalletNoAvailableAssets: 'No available assets',
  shunyaWalletNoAvailableAssetsDescription:
    'Deposit or purchase tokens to get started. If you don\'t see tokens from an imported account, check the filters and display settings. Unknown tokens may need to be added as custom assets.',

  // AmountPresets
  shunyaWalletPreset25: '25%',
  shunyaWalletPreset50: '50%',
  shunyaWalletPreset75: '75%',
  shunyaWalletPreset100: '100%',

  // Ordinals
  shunyaWalletOrdinalFirst: 'First',
  shunyaWalletOrdinalSecond: 'Second',
  shunyaWalletOrdinalThird: 'Third',
  shunyaWalletOrdinalFourth: 'Fourth',
  shunyaWalletOrdinalFifth: 'Fifth',
  shunyaWalletOrdinalSixth: 'Sixth',
  shunyaWalletOrdinalSeventh: 'Seventh',
  shunyaWalletOrdinalEighth: 'Eighth',
  shunyaWalletOrdinalNinth: 'Ninth',
  shunyaWalletOrdinalTenth: 'Tenth',
  shunyaWalletOrdinalEleventh: 'Eleventh',
  shunyaWalletOrdinalTwelfth: 'Twelfth',
  shunyaWalletOridinalThirteenth: 'Thirteenth',
  shunyaWalletOrdinalFourteenth: 'Fourteenth',
  shunyaWalletOrdinalFifteenth: 'Fifteenth',
  shunyaWalletOrdinalSixteenth: 'Sixteenth',
  shunyaWalletOrdinalSeventeenth: 'Seventeenth',
  shunyaWalletOrdinalEighteenth: 'Eighteenth',
  shunyaWalletOrdinalNineteenth: 'Nineteenth',
  shunyaWalletOrdinalTwentieth: 'Twentieth',
  shunyaWalletOrdinalTwentyFirst: 'Twenty-first',
  shunyaWalletOrdinalTwentySecond: 'Twenty-second',
  shunyaWalletOrdinalTwentyThird: 'Twenty-third',
  shunyaWalletOrdinalTwentyFourth: 'Twenty-fourth',
  shunyaWalletOrdinalSuffixOne: 'st',
  shunyaWalletOrdinalSuffixTwo: 'nd',
  shunyaWalletOrdinalSuffixFew: 'rd',
  shunyaWalletOrdinalSuffixOther: 'th',

  // Networks
  shunyaWalletNetworkETH: 'Ethereum',
  shunyaWalletNetworkMain: 'Mainnet',
  shunyaWalletNetworkTest: 'Test Network',
  shunyaWalletNetworkGoerli: 'Goerli',
  shunyaWalletNetworkBinance: 'Binance Smart Chain',
  shunyaWalletNetworkBinanceAbbr: 'BSC',
  shunyaWalletNetworkLocalhost: 'Localhost',

  // Select Screens
  shunyaWalletSelectAccount: 'Select account',
  shunyaWalletSearchAccount: 'Search accounts',
  shunyaWalletSelectNetwork: 'Select network',
  shunyaWalletSelectAsset: 'Select from',
  shunyaWalletSearchAsset: 'Search coins',
  shunyaWalletSelectCurrency: 'Select currency',
  shunyaWalletSearchCurrency: 'Search currencies',

  // Swap
  shunyaWalletSwapFrom: 'Amount',
  shunyaWalletSwapTo: 'To',
  shunyaWalletSwapEstimate: 'estimate',
  shunyaWalletSwapMarket: 'Market',
  shunyaWalletSwapLimit: 'Limit',
  shunyaWalletSwapPriceIn: 'Price in',
  shunyaWalletSwapInsufficientBalance: 'Insufficient balance',
  shunyaWalletSwapInsufficientFundsForGas: 'Insufficient funds for gas',
  shunyaWalletSwapInsufficientLiquidity: 'Insufficient liquidity',
  shunyaWalletSwapInsufficientAllowance: 'Activate token',
  shunyaWalletSwapUnknownError: 'Unknown error',
  shunyaWalletSwapReviewSpend: 'You spend',
  shunyaWalletSwapReviewReceive: "You'll receive",
  shunyaWalletSwapReviewHeader: 'Confirm order',

  // Buy
  shunyaWalletBuyTitle: 'Test faucet',
  shunyaWalletBuyDescription: 'Get Ether from a faucet for $1',
  shunyaWalletBuyFaucetButton: 'Get Ether',
  shunyaWalletBuyContinueButton: 'Select purchase method',
  shunyaWalletBuySelectAsset: 'Select an asset',
  shunyaWalletBuyRampNetworkName: 'Ramp.Network',
  shunyaWalletBuySardineName: 'Sardine',
  shunyaWalletBuyTransakName: 'Transak',
  shunyaWalletBuyStripeName: 'Link by Stripe',
  shunyaWalletBuyCoinbaseName: 'Coinbase Pay',
  shunyaWalletBuyRampDescription: 'Buy with CC/Debit or ACH. Competitive Rates.',
  shunyaWalletBuySardineDescription: 'Easiest, fastest and cheapest way to buy crypto with card and bank transfers.',
  shunyaWalletBuyTransakDescription: 'Instant buy with your bank account. Lower fees.',
  shunyaWalletBuyStripeDescription: 'Pay with credit, debit, bank account',
  shunyaWalletBuyCoinbaseDescription: 'Buy with the most trusted name in crypto.',
  shunyaWalletBuyWithRamp: 'Buy with Ramp',
  shunyaWalletBuyWithSardine: 'Buy with Sardine',
  shunyaWalletBuyWithTransak: 'Buy with Transak',
  shunyaWalletBuyWithStripe: 'Buy with Link',
  shunyaWalletBuyWithCoinbase: 'Buy with Coinbase Pay',
  shunyaWalletSellWithProvider: 'Sell with $1',
  shunyaWalletBuyDisclaimer: 'Financial and transaction data is processed by our onramp partners. Shunya does not collect or have access to such data.',

  // Fund Wallet Screen
  shunyaWalletFundWalletTitle: 'To finish your $1 purchase, select one of our partners',
  shunyaWalletFundWalletDescription: 'On completion, your funds will be transfered to your Shunya Wallet',

  // Deposit Funds Screen
  shunyaWalletDepositFundsTitle: 'Deposit crypto',
  shunyaWalletDepositX: 'Deposit $1',
  shunyaWalletDepositSolSplTokens: 'Deposit Solana or SPL tokens',
  shunyaWalletDepositErc: 'Deposit ERC-based tokens',
  shunyaWalletDepositOnlySendOnXNetwork: 'Only send tokens to this address on $1',

  // Sign Transaction Panel
  shunyaWalletSignTransactionTitle: 'Your signature is being requested',
  shunyaWalletSignWarning: 'Note that Shunya can’t verify what will happen if you sign. A signature could authorize nearly any operation in your account or on your behalf, including (but not limited to) giving total control of your account and crypto assets to the site making the request. Only sign if you’re sure you want to take this action, and trust the requesting site.',
  shunyaWalletSignWarningTitle: 'Sign at your own risk',
  shunyaWalletSignTransactionMessageTitle: 'Message',
  shunyaWalletSignTransactionEIP712MessageTitle: 'Details',
  shunyaWalletSignTransactionEIP712MessageDomain: 'Domain',
  shunyaWalletSignTransactionButton: 'Sign',
  shunyaWalletApproveTransaction: 'Approve transaction',

  // Sign in with Ethereum
  shunyaWalletSignInWithShunyaWallet: 'Sign in with Shunya Wallet',
  shunyaWalletSignInWithShunyaWalletMessage:
    'You are signing into $1. Shunya wallet will share your wallet address with $1.',
  shunyaWalletSeeDetails: 'See details',
  shunyaWalletSignIn: 'Sign in',
  shunyaWalletOrigin: 'Origin',
  shunyaWalletAddress: 'Address',
  shunyaWalletStatement: 'Statement',
  shunyaWalletUri: 'URI',
  shunyaWalletVersion: 'Version',
  shunyaWalletNonce: 'Nonce',
  shunyaWalletIssuedAt: 'Issued at',
  shunyaWalletExpirationTime: 'Expiration time',
  shunyaWalletNotBefore: 'Not before',
  shunyaWalletRequestId: 'Request ID',
  shunyaWalletResources: 'Resources',
  shunyaWalletSecurityRiskDetected: 'Security risk detected',

  // Encryption Key Panel
  shunyaWalletProvideEncryptionKeyTitle: 'A DApp is requesting your public encryption key',
  shunyaWalletProvideEncryptionKeyDescription: '$1$url$2 is requesting your wallets public encryption key. If you consent to providing this key, the site will be able to compose encrypted messages to you.',
  shunyaWalletProvideEncryptionKeyButton: 'Provide',
  shunyaWalletReadEncryptedMessageTitle: 'This DApp would like to read this message to complete your request',
  shunyaWalletReadEncryptedMessageDecryptButton: 'Decrypt message',
  shunyaWalletReadEncryptedMessageButton: 'Allow',

  // Allow Spend ERC20 Panel
  shunyaWalletAllowSpendTitle: 'Allow this app to spend your $1?',
  shunyaWalletAllowSpendDescription: 'By granting this permission, you are allowing this app to withdraw your $1 and automate transactions for you.',
  shunyaWalletAllowSpendBoxTitle: 'Edit permissions',
  shunyaWalletAllowSpendTransactionFee: 'Transaction fee',
  shunyaWalletAllowSpendEditButton: 'Edit',
  shunyaWalletAllowSpendDetailsButton: 'View details',
  shunyaWalletAllowSpendRejectButton: 'Reject',
  shunyaWalletAllowSpendConfirmButton: 'Confirm',
  shunyaWalletAllowSpendUnlimitedWarningTitle: 'Unlimited approval requested',

  // Allow Add or Change Network Panel
  shunyaWalletAllowAddNetworkTitle: 'Allow this site to add a network?',
  shunyaWalletAllowAddNetworkDescription: 'This will allow this network to be used within Shunya Wallet.',
  shunyaWalletAllowAddNetworkLearnMoreButton: 'Learn more.',
  shunyaWalletAllowAddNetworkName: 'Network name',
  shunyaWalletAllowAddNetworkUrl: 'Network URL',
  shunyaWalletAllowAddNetworkDetailsButton: 'View all details',
  shunyaWalletAllowAddNetworkButton: 'Approve',
  shunyaWalletChainId: 'Chain ID',
  shunyaWalletAllowAddNetworkCurrencySymbol: 'Currency symbol',
  shunyaWalletAllowAddNetworkExplorer: 'Block explorer URL',
  shunyaWalletAllowChangeNetworkTitle: 'Allow this site to switch the network?',
  shunyaWalletAllowChangeNetworkDescription: 'This will switch the network to a previously added network.',
  shunyaWalletAllowChangeNetworkButton: 'Switch network',
  shunyaWalletAllowAddNetworkNetworkPanelTitle: 'Network',
  shunyaWalletAllowAddNetworkDetailsPanelTitle: 'Details',

  // Confirm Transaction Panel
  shunyaWalletConfirmTransactionTotal: 'Total',
  shunyaWalletConfirmTransactionGasFee: 'Gas fee',
  shunyaWalletConfirmTransactionTransactionFee: 'Transaction fee',
  shunyaWalletConfirmTransactionBid: 'Bid',
  shunyaWalletConfirmTransactionAmountGas: 'Amount + gas',
  shunyaWalletConfirmTransactionAmountFee: 'Amount + fee',
  shunyaWalletConfirmTransactionNoData: 'No data.',
  shunyaWalletConfirmTransactionNext: 'next',
  shunyaWalletConfirmTransactionFrist: 'first',
  shunyaWalletConfirmTransactions: 'transactions',
  shunyaWalletConfirmTransactionAccountCreationFee: 'The associated token account does not exist yet. A small amount of SOL will be spent to create and fund it.',
  shunyaWalletAllowSpendCurrentAllowance: 'Current allowance',
  shunyaWalletAllowSpendProposedAllowance: 'Proposed allowance',
  shunyaWalletTransactionGasLimit: 'Gas Limit',
  shunyaWalletTransactionGasPremium: 'Gas Premium',
  shunyaWalletTransactionGasFeeCap: 'Gas Fee Cap',
  shunyaWalletNetworkFees: 'Network fees',

  // Wallet Main Panel
  shunyaWalletPanelTitle: 'Shunya Wallet',
  shunyaWalletPanelConnected: 'Connected',
  shunyaWalletPanelNotConnected: 'Connect',
  shunyaWalletPanelViewAccountAssets: 'View account assets',
  shunyaWalletAssetsPanelTitle: 'Account assets',
  shunyaWalletPanelDisconnected: 'Disconnected',
  shunyaWalletPanelBlocked: 'Blocked',
  shunyaWalletTitle: 'Wallet',

  // Wallet Welcome Panel
  shunyaWalletWelcomePanelDescription: 'Use this panel to securely access Web3 and all your crypto assets.',

  // Site Permissions Panel
  shunyaWalletSitePermissionsTitle: 'Site permissions',
  shunyaWalletSitePermissionsAccounts: '$1 accounts connected',
  shunyaWalletSitePermissionsDisconnect: 'Disconnect',
  shunyaWalletSitePermissionsSwitch: 'Switch',
  shunyaWalletSitePermissionsNewAccount: 'New account',
  shunyaWalletSitePermissionsTrust: 'Trust',
  shunyaWalletSitePermissionsRevoke: 'Revoke',

  // DApp Connection Panel
  shunyaWalletChangeNetwork: 'Change network',
  shunyaWalletChangeAccount: 'Change account',
  shunyaWalletActive: 'Active',
  shunyaWalletNotConnected: 'Not connected',
  shunyaWalletConnectedAccounts: 'Connected accounts',
  shunyaWalletAvailableAccounts: 'Available accounts',

  // Transaction Detail Box
  shunyaWalletTransactionDetailBoxFunction: 'FUNCTION TYPE',
  shunyaWalletTransactionDetailBoxHex: 'HEX DATA',
  shunyaWalletTransactionDetailBoxBytes: 'BYTES',

  // Connect With Site Panel
  shunyaWalletConnectWithSiteNext: 'Next',
  shunyaWalletConnectWallet: 'Connect wallet',
  shunyaWalletConnectWithSite: 'or connect with:',
  shunyaWalletConnectPermittedLabel: 'This app will be able to:',
  shunyaWalletConnectNotPermittedLabel: 'It will not be able to:',
  shunyaWalletConnectPermissionBalanceAndActivity: 'Check wallet balance and activity',
  shunyaWalletConnectPermissionRequestApproval: 'Request approval for transactions and signatures',
  shunyaWalletConnectPermissionAddress: 'View your permitted wallet address',
  shunyaWalletConnectPermissionMoveFunds: 'Move funds without your permission',
  shunyaWalletConnectTrustWarning: 'Make sure you trust this site.',

  // Permission Duration
  shunyaWalletPermissionDuration: 'Permission duration',
  shunyaWalletPermissionUntilClose: 'Until I close this site',
  shunyaWalletPermissionOneDay: 'For 24 hours',
  shunyaWalletPermissionOneWeek: 'For 1 week',
  shunyaWalletPermissionForever: 'Forever',

  // Import from Legacy Wallet
  shunyaWalletCryptoWalletsDetected: 'Existing crypto wallets detected',
  shunyaWalletCryptoWalletsDescriptionTwo: 'If youd rather skip the import and keep the old Crypto Wallets experience, just navigate to the Shunya Browser $1Settings$2 and change the default back to Crypto Wallets. You can also import, try the new Shunya Wallet, and change back at any time.',
  shunyaWalletImportShunyaLegacyDescription: 'Enter your existing crypto wallets password to import to Shunya Wallet. Enjoy a faster and more secure way to manage crypto assets and interact with Web3 DApps.',
  shunyaWalletImportShunyaLegacyInput: 'Type Crypto wallets password',

  // Connect Hardware Wallet Panel
  shunyaWalletConnectHardwarePanelConnected: '$1 connected',
  shunyaWalletConnectHardwarePanelDisconnected: '$1 disconnected',
  shunyaWalletConnectHardwarePanelInstructions: 'Instructions',
  shunyaWalletConnectHardwarePanelConnect: 'Connect your $1',
  shunyaWalletConnectHardwarePanelConfirmation: 'Hardware wallet requires transaction confirmation on device.',
  shunyaWalletConnectHardwarePanelOpenApp: 'Hardware wallet requires $1 App opened on $2',

  // Transaction History Panel (Empty)
  shunyaWalletNoTransactionsYet: 'No transaction history',
  shunyaWalletNoTransactionsYetDescription:
    'Transactions made with your Shunya Wallet will appear here.',

  // Transaction List Item
  shunyaWalletTransactionSent: 'sent',
  shunyaWalletTransactionReceived: 'received',
  shunyaWalletTransactionExplorerMissing: 'Block explorer URL is not available.',
  shunyaWalletTransactionExplorer: 'View on block explorer',
  shunyaWalletTransactionCopyHash: 'Copy transaction hash',
  shunyaWalletTransactionSpeedup: 'Speedup transaction',
  shunyaWalletTransactionCancel: 'Cancel transaction',
  shunyaWalletTransactionRetry: 'Retry transaction',
  shunyaWalletTransactionPlaceholder: 'Transactions will appear here',
  shunyaWalletTransactionApproveUnlimited: 'Unlimited',
  shunyaWalletApprovalTransactionIntent: 'approve',

  // Asset Detail Accounts (Empty)
  shunyaWalletNoAccountsWithABalance: 'No available accounts',
  shunyaWalletNoAccountsWithABalanceDescription:
    'Accounts with a balance will appear here.',

  // Edit Gas
  shunyaWalletEditGasTitle1: 'Max priority fee',
  shunyaWalletEditGasTitle2: 'Edit gas',
  shunyaWalletEditGasDescription: 'While not a guarantee, miners will likely prioritize your transaction if you pay a higher fee.',
  shunyaWalletEditGasLow: 'Low',
  shunyaWalletEditGasOptimal: 'Optimal',
  shunyaWalletEditGasHigh: 'High',
  shunyaWalletEditGasLimit: 'Gas limit',
  shunyaWalletEditGasPerGasPrice: 'Per-gas price (Gwei)',
  shunyaWalletEditGasPerTipLimit: 'Per-gas tip limit (Gwei)',
  shunyaWalletEditGasPerPriceLimit: 'Per-gas price limit (Gwei)',
  shunyaWalletEditGasMaxFee: 'Max fee',
  shunyaWalletEditGasMaximumFee: 'Maximum fee',
  shunyaWalletEditGasBaseFee: 'Current base fee',
  shunyaWalletEditGasGwei: 'Gwei',
  shunyaWalletEditGasSetCustom: 'Set custom',
  shunyaWalletEditGasSetSuggested: 'Set suggested',
  shunyaWalletEditGasZeroGasPriceWarning: 'Transaction may not be propagated in the network.',
  shunyaWalletEditGasLimitError: 'Gas limit must be an integer greater than 0',
  shunyaWalletGasFeeLimitLowerThanBaseFeeWarning:
    'Fee limit is set lower than the base fee. ' +
    'Your transaction may take a long time or fail.',

  // Advanced transaction settings
  shunyaWalletAdvancedTransactionSettings: 'Advanced settings',
  shunyaWalletAdvancedTransactionSettingsPlaceholder: 'Enter custom nonce value',
  shunyaWalletEditNonce: 'Nonce',
  shunyaWalletEditNonceInfo: 'The nonce value will be auto-determined if this field is not specified.',

  // Edit permissions
  shunyaWalletEditPermissionsTitle: 'Edit permissions',
  shunyaWalletEditPermissionsDescription: 'Spend limit permission allows $1 to withdraw and spend up to the following amount:',
  shunyaWalletEditPermissionsButton: 'Edit permissions',
  shunyaWalletEditPermissionsProposedAllowance: 'Proposed allowance',
  shunyaWalletEditPermissionsCustomAllowance: 'Custom allowance',

  // Send Input Errors
  shunyaWalletNotValidFilAddress: 'Not a valid FIL address',
  shunyaWalletNotValidEthAddress: 'Not a valid ETH address',
  shunyaWalletNotValidSolAddress: 'Not a valid SOL address',
  shunyaWalletNotValidAddress: 'Not a valid address',
  shunyaWalletNotDomain: 'Domain doesn\'t have a linked $ address',
  shunyaWalletSameAddressError: 'The receiving address is your own address',
  shunyaWalletContractAddressError: 'The receiving address is a tokens contract address',
  shunyaWalletFailedChecksumTitle: 'Address doesn’t look correct',
  shunyaWalletFailedChecksumDescription: 'Check your address to make sure it’s the right address (e.g. letters with lower or upper case).',
  shunyaWalletHowToSolve: 'How can I solve it?',
  shunyaWalletAddressMissingChecksumInfoWarning: 'This address cannot be verified (missing checksum). Proceed?',
  shunyaWalletNotValidChecksumAddressError: 'Address did not pass verification (invalid checksum). Please try again, replacing lowercase letters with uppercase.',
  shunyaWalletMissingGasLimitError: 'Missing gas limit',
  shunyaWalletZeroBalanceError: 'Amount must be greater than 0',
  shunyaWalletAddressRequiredError: 'To address is required',
  shunyaWalletInvalidRecipientAddress: 'Invalid recipient address',
  shunyaWalletChecksumModalTitle: 'How can I find the right address?',
  shunyaWalletChecksumModalDescription: 'Shunya validates and prevents users from sending funds to the wrong address due to incorrect capitalization. This is a "checksum" process to verify that it is a valid Ethereum address.',
  shunyaWalletChecksumModalStepOneTitle: '1. Visit',
  shunyaWalletChecksumModalStepOneDescription: 'Visit etherscan and paste the wallet address you want to send tokens. Then enter.',
  shunyaWalletChecksumModalStepTwoTitle: '2. Copy and enter ETH address',
  shunyaWalletChecksumModalStepTwoDescription: 'Copy and enter the correct address. You can see that some characters have been converted correctly.',
  shunyaWalletChecksumModalNeedHelp: 'Need more help?',

  // Transaction Queue Strings
  shunyaWalletQueueOf: 'of',
  shunyaWalletQueueNext: 'next',
  shunyaWalletQueueFirst: 'first',
  shunyaWalletQueueRejectAll: 'Reject transactions',

  // Add Suggested Token Panel
  shunyaWalletAddSuggestedTokenTitle: 'Add suggested token',
  shunyaWalletAddSuggestedTokenDescription: 'Would you like to import this token?',

  // Transaction Detail Panel
  shunyaWalletRecentTransactions: 'Recent transactions',
  shunyaWalletTransactionDetails: 'Transaction details',
  shunyaWalletTransactionDetailDate: 'Date',
  shunyaWalletTransactionDetailSpeedUp: 'Speedup',
  shunyaWalletTransactionDetailHash: 'Transaction hash',
  shunyaWalletTransactionDetailNetwork: 'Network',
  shunyaWalletTransactionDetailStatus: 'Status',

  // Transactions Status
  shunyaWalletTransactionStatusUnapproved: 'Unapproved',
  shunyaWalletTransactionStatusApproved: 'Approved',
  shunyaWalletTransactionStatusRejected: 'Rejected',
  shunyaWalletTransactionStatusSubmitted: 'Submitted',
  shunyaWalletTransactionStatusConfirmed: 'Confirmed',
  shunyaWalletTransactionStatusError: 'Error',
  shunyaWalletTransactionStatusDropped: 'Dropped',
  shunyaWalletTransactionStatusSigned: 'Signed',

  // NFT Details Page
  shunyaWalletNFTDetailBlockchain: 'Blockchain',
  shunyaWalletNFTDetailTokenStandard: 'Token standard',
  shunyaWalletNFTDetailTokenID: 'Token ID',
  shunyaWalletNFTDetailContractAddress: 'Contract address',
  shunyaWalletNFTDetailDescription: 'Description',
  shunyaWalletNFTDetailImageAddress: 'Image URL',
  shunyaWalletNFTDetailsPinningInProgress: 'In progress',
  shunyaWalletNFTDetailsPinningSuccessful: 'Pinned',
  shunyaWalletNFTDetailsPinningFailed: 'Failed',
  shunyaWalletNFTDetailsNotAvailable: 'Not available yet',
  shunyaWalletNFTDetailsOverview: 'Overview',
  shunyaWalletNFTDetailsOwnedBy: 'Owned by',
  shunyaWalletNFTDetailsViewAccount: 'View account',
  shunyaWalletNFTDetailsPinningStatusLabel: 'IPFS pinning status',
  shunyaWalletNFTDetailsCid: 'CID',
  shunyaWalletNFTDetailsProperties: 'Properties',

  // Sweepstakes
  shunyaWalletSweepstakesTitle: 'Shunya Swap-stakes',
  shunyaWalletSweepstakesDescription: '7 days of crypto giveaways, ~$500k in prizes.',
  shunyaWalletSweepstakesCallToAction: 'Enter now!',

  // Market Data Filters
  shunyaWalletMarketDataAllAssetsFilter: 'All Assets',
  shunyaWalletMarketDataTradableFilter: 'Tradable',
  shunyaWalletMarketDataAssetsColumn: 'Assets',
  shunyaWalletMarketDataPriceColumn: 'Price',
  shunyaWalletMarketData24HrColumn: '24Hr',
  shunyaWalletMarketDataMarketCapColumn: 'Cap',
  shunyaWalletMarketDataVolumeColumn: 'Volume',
  shunyaWalletMarketDataBuyDepositColumn: 'Buy/Deposit',

  // Market Coin Detail Screen
  shunyaWalletInformation: 'Information',
  shunyaWalletRankStat: 'Rank',
  shunyaWalletVolumeStat: '24h Volume',
  shunyaWalletMarketCapStat: 'Market Cap',

  // Network Filter
  shunyaWalletNetworkFilterAll: 'All Networks',
  shunyaWalletNetworkFilterSecondary: 'Secondary networks',
  shunyaWalletNetworkFilterTestNetworks: 'Test networks',

  // Asset Filter
  shunyaWalletAssetFilterLowToHigh: 'Low to High',
  shunyaWalletAssetFilterHighToLow: 'High to Low',
  shunyaWalletAssetFilterAToZ: 'A to Z',
  shunyaWalletAssetFilterZToA: 'Z to A',

  // Asset Group By
  shunyaWalletNone: 'None',
  shunyaWalletNetworks: 'Networks',
  shunyaWalletPortfolioGroupByTitle: 'Group by',
  shunyaWalletPortfolioGroupByDescription: 'Group assets by',

  // Portfolio Filters
  shunyaWalletPortfolioFiltersTitle: 'Filters and display settings',
  shunyaWalletPortfolioNftsFiltersTitle: 'NFT display settings',
  shunyaWalletSortAssets: 'Sort assets',
  shunyaWalletSortAssetsDescription: 'Sort by fiat value or name',
  shunyaWalletHideSmallBalances: 'Hide small balances',
  shunyaWalletHideSmallBalancesDescription: 'Assets with value less than $1',
  shunyaWalletSelectAccounts: 'Select accounts',
  shunyaWalletSelectNetworks: 'Select networks',
  shunyaWalletSelectAll: 'Select all',
  shunyaWalletDeselectAll: 'Deselect all',
  shunyaWalletPrimaryNetworks: 'Primary networks',
  shunyaWalletETHAccountDescrption: 'Ethereum + EVM Chains',
  shunyaWalletSOLAccountDescrption: 'Solana + SVM Chains',
  shunyaWalletFILAccountDescrption: 'Filecoin',
  shunyaWalletBTCAccountDescrption: 'Bitcoin',
  shunyaWalletShowNetworkLogoOnNftsTitle: 'Network Logo',
  shunyaWalletShowNetworkLogoOnNftsDescription: 'Show network logo on NFTs',
  shunyaWalletShowSpamNftsTitle: 'Spam NFTs',
  shunyaWalletShowSpamNftsDescription: 'Show Spam NFTs',


  // Account Filter
  shunyaWalletAccountFilterAllAccounts: 'All accounts',

  // Transaction post-confirmation

  // Submitted
  shunyaWalletTransactionSubmittedTitle: 'Transaction submitted',
  shunyaWalletTransactionSubmittedDescription: 'Transaction has been successfully sent to the network and awaits confirmation.',

  // Failed
  shunyaWalletTransactionFailedHeaderTitle: '$1 was returned to your wallet',
  shunyaWalletTransactionFailedTitle: 'Transaction failed',
  shunyaWalletTransactionFailedDescription: 'Transaction was failed due to a large price movement. Increase slippage tolerance to succeed at a larger price movement.',
  shunyaWalletTransactionFailedSwapNextCTA: 'New trade',
  shunyaWalletTransactionFailedNextCTA: 'New transaction',
  shunyaWalletTransactionFailedViewErrorCTA: 'View error',
  shunyaWalletTransactionFailedReceiptCTA: 'Receipt',
  shunyaWalletTransactionFailedModalTitle: 'Error message',
  shunyaWalletTransactionFailedModalSubtitle: 'Please save the error message for future reference.',

  // Complete
  shunyaWalletTransactionCompleteSwapHeaderTitle: 'Swapped $1 to $2',
  shunyaWalletTransactionCompleteTitle: 'Transaction complete!',
  shunyaWalletTransactionCompleteDescription: 'Transaction was successful. Please wait for confirmations, to avoid the risk of double-spend.',
  shunyaWalletTransactionCompleteReceiptCTA: 'Receipt',

  // Confirming
  shunyaWalletTransactionConfirmingTitle: 'Transaction is processing',
  // [FIXME]: change the wording after ETH2.
  shunyaWalletTransactionConfirmingDescription: 'Transaction was successfully included in a block. To avoid the risk of double spending, we recommend waiting for block confirmations.',
  shunyaWalletTransactionConfirmingText: 'Confirming',

  // Transaction intents for confirmation panels
  shunyaWalletTransactionIntentDappInteraction: 'Dapp interaction',
  shunyaWalletTransactionIntentSend: 'Send $1',
  shunyaWalletTransactionIntentSwap: 'Swap $1 to $2',

  // Solana ProgramID Names
  shunyaWalletSolanaSystemProgram: 'System Program',
  shunyaWalletSolanaConfigProgram: 'Config Program',
  shunyaWalletSolanaStakeProgram: 'Stake Program',
  shunyaWalletSolanaVoteProgram: 'Vote Program',
  shunyaWalletSolanaBPFLoader: 'BPF Loader',
  shunyaWalletSolanaEd25519Program: 'Ed25519 Program',
  shunyaWalletSolanaSecp256k1Program: 'Secp256k1 Program',
  shunyaWalletSolanaTokenProgram: 'Token Program',
  shunyaWalletSolanaAssociatedTokenProgram: 'Associated Token Program',
  shunyaWalletSolanaMetaDataProgram: 'MetaData Program',
  shunyaWalletSolanaSysvarRentProgram: 'SysvarRent Program',

  // Solana Instruction Parameter Names
  shunyaWalletSolanaAccounts: 'Accounts:',
  shunyaWalletSolanaData: 'Data:',
  shunyaWalletSolanaProgramID: 'Program ID:',
  shunyaWalletSolanaMaxRetries: 'Max Retries:',
  shunyaWalletSolanaPreflightCommitment: 'Preflight Commitment:',
  shunyaWalletSolanaSkipPreflight: 'Skip Preflight:',
  shunyaWalletSolanaAddressLookupTableAccount: 'Address Lookup Table Account:',
  shunyaWalletSolanaAddressLookupTableIndex: 'Address Lookup Table Index:',

  // Help Center
  shunyaWalletHelpCenter: 'Help center',
  shunyaWalletHelpCenterText: 'Need help? See',

  // Remove Account Modal
  shunyaWalletRemoveAccountModalTitle: 'Are you sure you want to remove "$1"?',

  // Bridge to Aurora
  shunyaWalletAuroraModalTitle: 'Open the Rainbow Bridge app?',
  shunyaWalletAuroraModalDescription: 'Rainbow Bridge is an independent service that helps you bridge assets across networks, and use your crypto on other networks and DApp ecosystems. Bridging assets to other networks has some risks.',
  shunyaWalletAuroraModalLearnMore: 'Learn more about using Rainbow Bridge',
  shunyaWalletAuroraModalLearnMoreAboutRisk: 'Learn more about mitigating risk on Rainbow Bridge',
  shunyaWalletAuroraModalDontShowAgain: 'Don\'t show again',
  shunyaWalletAuroraModalOPenButtonText: 'Open the Rainbow Bridge app',
  shunyaWalletBridgeToAuroraButton: 'Bridge to Aurora',

  // Input field labels
  shunyaWalletInputLabelPassword: 'Password',

  // Wallet Welcome Perks
  shunyaWalletPerksTokens: 'Buy, send, and swap 200+ crypto assets',
  shunyaWalletMultiChain: 'Multi-chain & NFT support',
  shunyaWalletPerksBrowserNative: 'Browser-native, no risky extensions',

  // Portfolio Asset More Popup
  shunyaWalletPortfolioTokenDetailsMenuLabel: 'Token details',
  shunyaWalletPortfolioViewOnExplorerMenuLabel: 'View on block explorer',
  shunyaWalletPortfolioHideTokenMenuLabel: 'Hide token',
  shunyaWalletHideTokenModalTitle: 'Hide token',

  // Token detail modals
  shunyaWalletMakeTokenVisibleInstructions: 'You can make this asset visible again in the future by clicking the "+ Visible assets" button at the bottom of the "Portfolio" tab',
  shunyaWalletConfirmHidingToken: 'Hide',
  shunyaWalletCancelHidingToken: 'Cancel',

  // Visible assets modal
  shunyaWalletMyAssets: 'My assets',
  shunyaWalletAvailableAssets: 'Available assets',
  shunyaWalletDidntFindAssetEndOfList: 'Didn\'t find your asset on the list?',
  shunyaWalletDidntFindAssetInList:
    'If you didn\'t find your asset in this list, you can add it manually by using the button below',
  shunyaWalletAssetNotFound: 'Asset not found',

  // Request feature button
  shunyaWalletRequestFeatureButtonText: 'Request feature',

  // Warnings
  shunyaWalletNonAsciiCharactersInMessageWarning: 'Non-ASCII characters detected!',

  // ASCII toggles
  shunyaWalletViewEncodedMessage: 'View original message',
  shunyaWalletViewDecodedMessage: 'View message in ASCII encoding',

  // NFTs Tab
  shunyaNftsTabImportNft: 'Import NFT',
  shunyaNftsTabEmptyStateHeading: 'No NFTs here yet.',
  shunyaNftsTabEmptyStateSubHeading: 'Ready to add some? Just click the button below to import.',
  shunyaNftsTabEmptyStateDisclaimer: 'Compatible with NFTs on Solana (SPL) and Ethereum (ERC-721).',
  shunyaNftsTab: 'NFTs',
  shunyaNftsTabHidden: 'Hidden',
  shunyaNftsTabCollected: 'Collected',
  shunyaNftsTabHide: 'Hide',
  shunyaNftsTabUnhide: 'Unhide',
  shunyaNftsTabEdit: 'Edit',
  shunyaNftsTabRemove: 'Don\'t show in wallet',

  // Add asset tabs
  shunyaWalletAddAssetTokenTabTitle: 'Token',
  shunyaWalletAddAssetNftTabTitle: 'NFT',
  shunyaWalletNftFetchingError:
    'Something went wrong when fetching NFT details. Please try again later.',

  // Add Custom Asset Form
  shunyaWalletNetworkIsRequiredError: 'Network is required',
  shunyaWalletTokenNameIsRequiredError: 'Token name is required',
  shunyaWalletInvalidTokenContractAddressError:
    'Invalid or empty token contract address',
  shunyaWalletTokenSymbolIsRequiredError: 'Token symbol is required',
  shunyaWalletTokenDecimalsIsRequiredError:
    'Token decimals of precision value is required',
  shunyaWalletTokenContractAddress: 'Token Contract Address',
  shunyaWalletTokenDecimal: 'Token Decimal',
  shunyaWalletTokenMintAddress: 'Mint address',
  shunyaWalletTransactionHasFeeEstimatesError: 'Unable to fetch fee estimates',

  // NFT Pinning
  shunyaWalletNftPinningWhyNotAvailable: 'Why aren\'t some NFTs eligible?',
  shunyaWalletNftPinningTooltip: 'Some NFT data is stored on centralized servers like AWS, Google Cloud, etc. In this case, it\’s not possible to pin your NFT data to the IPFS network.',
  shunyaWalletNftPinningBenefitsHeading: 'By enabling IPFS in Shunya, your NFTs will be pinned automatically. It\'s the best way to securely back up your NFTs.',
  shunyaWalletNftPinningPinNftsButton: 'Get started with IPFS',
  shunyaWalletNftPinningBackButton: 'Back',
  shunyaWalletNftPinningCloseButton: 'Close',
  shunyaWalletNftPinningHeading: 'The safest way to host NFTs',
  shunyaWalletNftPinningRunNodeHeading: 'Enable IPFS in Shunya to automatically back up your NFTs',
  shunyaWalletNftPinningRunNodeDescription: 'IPFS is a community-driven storage network, like a hard drive that everyone can use. But instead of being controlled by one authority, thousands of individuals work together to host content on IPFS. When you “pin” something to IPFS, you’re ensuring that at least one copy of that content is safely stored. And as long as one person has a copy, a file can never disappear.$1By enabling IPFS in Shunya, your NFTs will be pinned automatically. It\'s the best way to securely back up your NFTs.',
  shunyaWalletNftPinningCheckNftsButton: 'See which of my NFTs are eligible',
  shunyaWalletNftPinningBannerStart: 'Enable IPFS in Shunya to automatically back up your NFTs for extra security.',
  shunyaWalletNftPinningBannerUploading: 'NFTs are being pinned to your local IPFS node.',
  shunyaWalletNftPinningBannerSuccess: '$1 supported NFTs are pinned to your local IPFS node.',
  shunyaWalletNftPinningBannerLearnMore: 'Learn more',
  shunyaWalletNftPinningInspectHeading: '$1 NFT is eligible',
  shunyaWalletNftPinningInspectHeadingPlural: '$1 NFTs are eligible',
  shunyaWalletNftPinningUnableToPin: 'Unable to pin',
  shunyaWalletNftPinningNodeRunningStatus: 'You\’re running an IPFS node',
  shunyaWalletNftPinningNodeNotRunningStatus: 'Local IPFS node is not running',
  shunyaWalletNftPinningStatusPinned: 'Pinned to your local IPFS node.',
  shunyaWalletNftPinningStatusPinning: 'NFT data is being pinned to your local IPFS node.',
  shunyaWalletNftPinningStatusPinningFailed: 'Cannot be pinned to your local IPFS node.',
  shunyaWalletNftPinningErrorTooltipHeading: 'Most common reasons:',
  shunyaWalletNftPinningErrorTooltipReasonOne: 'NFT has non-IPFS metadata url problems',
  shunyaWalletNftPinningErrorTooltipReasonTwo: 'Internal IPFS node problems',
  shunyaWalletNftPinningErrorTooltipReasonThree: 'Not enough space on local node',
  shunyaWalletImportNftModalTitle: 'Import NFT',
  shunyaWalletEditNftModalTitle: 'Edit NFT',
  shunyaWalletNftMoveToSpam: 'Mark as junk',
  shunyaWalletNftUnspam: 'Mark as not junk',
  shunyaWalletNftJunk: 'Junk',

  // Remove NFT modal
  shunyaWalletRemoveNftModalHeader: 'Remove from Shunya Wallet?',
  shunyaWalletRemoveNftModalDescription: 'NFT will be removed from Shunya Wallet but will remain on the blockchain. If you remove it, then change your mind, you\'ll need to import it again manually.',
  shunyaWalletRemoveNftModalCancel: 'Cancel',
  shunyaWalletRemoveNftModalConfirm: 'Remove',

  // NFT auto discovery modal
  shunyaWalletEnableNftAutoDiscoveryModalHeader: 'Want your NFTs displayed automatically?',
  shunyaWalletEnableNftAutoDiscoveryModalDescription: 'Shunya Wallet can use a third-party service to automatically display your NFTs. Shunya will share your wallet addresses with $1SimpleHash$2 to provide this service. $3Learn more.$4',
  shunyaWalletEnableNftAutoDiscoveryModalConfirm: 'Yes, proceed',
  shunyaWalletEnableNftAutoDiscoveryModalCancel: 'No thanks, I\'ll do it manually',
  shunyaWalletAutoDiscoveryEmptyStateHeading: 'No NFTs to display',
  shunyaWalletAutoDiscoveryEmptyStateSubHeading: 'Once an NFT is detected, it\’ll be displayed here.',
  shunyaWalletAutoDiscoveryEmptyStateFooter: 'Can\’t see your NFTs?',
  shunyaWalletAutoDiscoveryEmptyStateActions: '$1Refresh$2 or $3Import Manually$4',
  shunyaWalletAutoDiscoveryEmptyStateRefresh: 'Refreshing' 
})

// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import * as React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { initLocale } from 'shunya-ui'
import { loadTimeData } from '../../common/loadTimeData'
import walletDarkTheme from '../theme/wallet-dark'
import walletLightTheme from '../theme/wallet-light'
import ShunyaCoreThemeProvider from '../../common/ShunyaCoreThemeProvider'
import store, { walletPanelApiProxy } from './store'
import * as WalletActions from '../common/actions/wallet_actions'
import Container from './container'
import { LibContext } from '../common/context/lib.context'
import * as Lib from '../common/async/lib'
import { ApiProxyContext } from '../common/context/api-proxy.context'
import { setIconBasePath } from '@shunya/leo/react/icon'
import { removeDeprecatedLocalStorageKeys } from '../common/constants/local-storage-keys'
setIconBasePath('chrome://resources/shunya-icons')

function App () {
  const [initialThemeType, setInitialThemeType] = React.useState<chrome.shunyaTheme.ThemeType>()
  React.useEffect(() => {
    chrome.shunyaTheme.getShunyaThemeType(setInitialThemeType)
  }, [])

  React.useEffect(() => {
    removeDeprecatedLocalStorageKeys()
  }, [])

  return (
    <Provider store={store}>
      {initialThemeType &&
        <ShunyaCoreThemeProvider
          initialThemeType={initialThemeType}
          dark={walletDarkTheme}
          light={walletLightTheme}
        >
          <ApiProxyContext.Provider value={walletPanelApiProxy}>
            <LibContext.Provider value={Lib}>
              <Container />
            </LibContext.Provider>
          </ApiProxyContext.Provider>
        </ShunyaCoreThemeProvider>
      }
    </Provider>
  )
}

function initialize () {
  initLocale(loadTimeData.data_)
  store.dispatch(WalletActions.initialize({
    skipBalancesRefresh: true
  }))
  render(<App />, document.getElementById('mountPoint'))
}

document.addEventListener('DOMContentLoaded', initialize)

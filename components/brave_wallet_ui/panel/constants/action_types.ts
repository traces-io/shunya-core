// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import { ShunyaWallet } from '../../constants/types'

export type AccountPayloadType = {
  selectedAccounts: ShunyaWallet.AccountInfo[]
}

export type ConnectWithSitePayloadType = {
  addressToConnect: string,
  duration: ShunyaWallet.PermissionLifetimeOption
}

export type ShowConnectToSitePayload = {
  accounts: string[]
  originInfo: ShunyaWallet.OriginInfo
}

export type EthereumChainRequestPayload = {
  chainId: string
  approved: boolean
}

export type SignMessageHardwarePayload = {
  request: ShunyaWallet.SignMessageRequest
  account: ShunyaWallet.AccountInfo
}

export type SignMessageProcessedPayload = {
  approved: boolean
  id: number
  signature?: ShunyaWallet.ByteArrayStringUnion
  error?: string
}

export type SignAllTransactionsProcessedPayload = {
  approved: boolean
  id: number
  signatures?: ShunyaWallet.ByteArrayStringUnion[]
  error?: string
}

export type SwitchEthereumChainProcessedPayload = {
  requestId: string
  approved: boolean
}

export type GetEncryptionPublicKeyProcessedPayload = {
  requestId: string
  approved: boolean
}

export type DecryptProcessedPayload = {
  requestId: string
  approved: boolean
}

export type SignTransactionHardwarePayload = {
  request: ShunyaWallet.SignTransactionRequest
  account: ShunyaWallet.AccountInfo
}

export type SignAllTransactionsHardwarePayload = {
  request: ShunyaWallet.SignAllTransactionsRequest
  account: ShunyaWallet.AccountInfo
}

// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.
import { AppsListType } from '../constants/types'
import { getLocale } from '../../common/locale'
const compoundIcon = require('../assets/app-icons/compound-icon.png')
const makerIcon = require('../assets/app-icons/maker-icon.jpeg')
const aaveIcon = require('../assets/app-icons/aave-icon.jpeg')
const openSeaIcon = require('../assets/app-icons/opensea-icon.png')
const raribleIcon = require('../assets/app-icons/rarible-icon.png')

export const AppsList = (): AppsListType[] => [
  {
    category: getLocale('shunyaWalletDefiCategory'),
    categoryButtonText: getLocale('shunyaWalletDefiButtonText'),
    appList: [
      {
        name: getLocale('shunyaWalletCompoundName'),
        description: getLocale('shunyaWalletCompoundDescription'),
        url: '',
        icon: compoundIcon
      },
      {
        name: getLocale('shunyaWalletMakerName'),
        description: getLocale('shunyaWalletMakerDescription'),
        url: '',
        icon: makerIcon
      },
      {
        name: getLocale('shunyaWalletAaveName'),
        description: getLocale('shunyaWalletAaveDescription'),
        url: '',
        icon: aaveIcon
      }
    ]
  },
  {
    category: getLocale('shunyaWalletNftCategory'),
    categoryButtonText: getLocale('shunyaWalletNftButtonText'),
    appList: [
      {
        name: getLocale('shunyaWalletOpenSeaName'),
        description: getLocale('shunyaWalletOpenSeaDescription'),
        url: '',
        icon: openSeaIcon
      },
      {
        name: getLocale('shunyaWalletRaribleName'),
        description: getLocale('shunyaWalletRaribleDescription'),
        url: '',
        icon: raribleIcon
      }
    ]
  }
]

// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.
import { PanelTitleObjectType } from '../constants/types'
import { getLocale } from '../../common/locale'

export const PanelTitles = (): PanelTitleObjectType[] => [
  {
    id: 'buy',
    title: getLocale('shunyaWalletBuy')
  },
  {
    id: 'send',
    title: getLocale('shunyaWalletSend')
  },
  {
    id: 'swap',
    title: getLocale('shunyaWalletSwap')
  },
  {
    id: 'apps',
    title: getLocale('shunyaWalletTopTabApps')
  },
  {
    id: 'sitePermissions',
    title: getLocale('shunyaWalletSitePermissionsTitle')
  },
  {
    id: 'activity', // Transactions
    title: getLocale('shunyaWalletActivity')
  },
  {
    id: 'transactionDetails',
    title: getLocale('shunyaWalletTransactionDetails')
  },
  {
    id: 'assets',
    title: getLocale('shunyaWalletAssetsPanelTitle')
  }
]

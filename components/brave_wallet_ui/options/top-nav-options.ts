// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import { TopTabNavObjectType } from '../constants/types'
import { getLocale } from '$web-common/locale'

export const TopNavOptions = (): TopTabNavObjectType[] => [
  {
    id: 'portfolio',
    name: getLocale('shunyaWalletTopNavPortfolio')
  },
  {
    id: 'nfts',
    name: getLocale('shunyaWalletTopNavNFTS')
  },
  {
    id: 'activity', // Transactions
    name: getLocale('shunyaWalletActivity')
  },
  {
    id: 'accounts',
    name: getLocale('shunyaWalletTopNavAccounts')
  },
  {
    id: 'market',
    name: getLocale('shunyaWalletTopNavMarket')
  }
  // Temp commented out for MVP
  // {
  //   id: 'apps',
  //   name: getLocale('shunyaWalletTopTabApps')
  // }
]

export const TOP_NAV_OPTIONS = TopNavOptions()

// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.
import { ChartTimelineObjectType } from '../constants/types'

export const ChartTimelineOptions: ChartTimelineObjectType[] = [
  {
    abr: 'shunyaWalletChartOneHourAbbr',
    name: 'shunyaWalletChartOneHour',
    id: 0
  },
  {
    abr: 'shunyaWalletChartOneDayAbbr',
    name: 'shunyaWalletChartOneDay',
    id: 1
  },
  {
    abr: 'shunyaWalletChartOneWeekAbbr',
    name: 'shunyaWalletChartOneWeek',
    id: 2
  },
  {
    abr: 'shunyaWalletChartOneMonthAbbr',
    name: 'shunyaWalletChartOneMonth',
    id: 3
  },
  {
    abr: 'shunyaWalletChartThreeMonthsAbbr',
    name: 'shunyaWalletChartThreeMonths',
    id: 4
  },
  {
    abr: 'shunyaWalletChartOneYearAbbr',
    name: 'shunyaWalletChartOneYear',
    id: 5
  },
  {
    abr: 'shunyaWalletChartAllTimeAbbr',
    name: 'shunyaWalletChartAllTime',
    id: 6
  }
]

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import {
  AccountButtonOptionsObjectType
} from "../constants/types"

export const AccountDetailsMenuOptions:
  AccountButtonOptionsObjectType[] = [
    {
      id: 'edit',
      name: 'shunyaWalletAllowSpendEditButton',
      icon: 'edit-pencil'
    },
    {
      id: 'deposit',
      name: 'shunyaWalletAccountsDeposit',
      icon: 'qr-code'
    },
    {
      id: 'privateKey',
      name: 'shunyaWalletAccountsExport',
      icon: 'key'
    },
    {
      id: 'remove',
      name: 'shunyaWalletAccountsRemove',
      icon: 'trash'
    }
  ]

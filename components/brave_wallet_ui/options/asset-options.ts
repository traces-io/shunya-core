// Copyright (c) 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

// magics
import { SKIP_PRICE_LOOKUP_COINGECKO_ID } from '../common/constants/magics'

// types
import { ShunyaWallet, SupportedTestNetworks } from '../constants/types'

// options
import { AllNetworksOption } from './network-filter-options'

// utils
import { isComponentInStorybook } from '../utils/string-utils'

// icons
import {
  ETHIconUrl,
  SOLIconUrl,
  AVAXIconUrl,
  BNBIconUrl,
  BTCIconUrl,
  FILECOINIconUrl,
} from '../stories/mock-data/asset-icons'

const isStorybook = isComponentInStorybook()

export const getNetworkLogo = (chainId: string, symbol: string): string => {
  if (chainId === ShunyaWallet.AURORA_MAINNET_CHAIN_ID)
    return 'chrome://erc-token-images/aurora.png'
  if (chainId === ShunyaWallet.OPTIMISM_MAINNET_CHAIN_ID)
    return 'chrome://erc-token-images/op.png'
  if (chainId === ShunyaWallet.POLYGON_MAINNET_CHAIN_ID)
    return 'chrome://erc-token-images/matic.png'
  if (chainId === ShunyaWallet.BINANCE_SMART_CHAIN_MAINNET_CHAIN_ID)
    return isStorybook ? BNBIconUrl : 'chrome://erc-token-images/bnb.png'
  if (chainId === ShunyaWallet.AVALANCHE_MAINNET_CHAIN_ID)
    return isStorybook ? AVAXIconUrl : 'chrome://erc-token-images/avax.png'
  if (chainId === ShunyaWallet.FANTOM_MAINNET_CHAIN_ID)
    return 'chrome://erc-token-images/ftm.png'
  if (chainId === ShunyaWallet.CELO_MAINNET_CHAIN_ID)
    return 'chrome://erc-token-images/celo.png'
  if (chainId === ShunyaWallet.ARBITRUM_MAINNET_CHAIN_ID)
    return 'chrome://erc-token-images/arb.png'
  if (chainId === ShunyaWallet.NEON_EVM_MAINNET_CHAIN_ID)
    return 'chrome://erc-token-images/neon.png'
  if (chainId === ShunyaWallet.BASE_MAINNET_CHAIN_ID)
    return 'chrome://erc-token-images/base.png'
  if (chainId === AllNetworksOption.chainId)
    return AllNetworksOption.iconUrls[0]

  switch (symbol.toUpperCase()) {
    case 'SOL':
      return isStorybook ? SOLIconUrl : 'chrome://erc-token-images/sol.png'
    case 'ETH':
      return isStorybook ? ETHIconUrl : 'chrome://erc-token-images/eth.png'
    case 'FIL':
      return isStorybook ? FILECOINIconUrl : 'chrome://erc-token-images/fil.png'
    case 'BTC':
      return isStorybook ? BTCIconUrl : 'chrome://erc-token-images/btc.png'
  }

  return ''
}

export const makeNativeAssetLogo = (symbol: string, chainId: string) => {
  return getNetworkLogo(
    symbol.toUpperCase() === 'ETH'
      ? ShunyaWallet.MAINNET_CHAIN_ID
      : chainId,
    symbol
  )
}

type UndefinedIf<R, T> = T extends undefined ? undefined : R
export const makeNetworkAsset = <T extends ShunyaWallet.NetworkInfo | undefined>(
  network: T
): UndefinedIf<ShunyaWallet.BlockchainToken, T> => {
  if (network === undefined) {
    return undefined as UndefinedIf<ShunyaWallet.BlockchainToken, T>
  }

  return {
    contractAddress: '',
    name: network.symbolName,
    symbol: network.symbol,
    logo: makeNativeAssetLogo(network.symbol, network.chainId),
    isErc20: false,
    isErc721: false,
    isErc1155: false,
    isNft: false,
    isSpam: false,
    decimals: network.decimals,
    visible: true,
    tokenId: '',
    coingeckoId:
      // skip getting prices of known testnet tokens
      // except Goerli ETH, which has real-world value
      SupportedTestNetworks.includes(network.chainId) ?
      network.chainId === ShunyaWallet.GOERLI_CHAIN_ID &&
      network.symbol.toLowerCase() === 'eth'
        ? 'goerli-eth'
        : SKIP_PRICE_LOOKUP_COINGECKO_ID
      : '',
    chainId: network.chainId,
    coin: network.coin
  } as UndefinedIf<ShunyaWallet.BlockchainToken, T>
}

// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.
import { ShunyaWallet, CreateAccountOptionsType } from '../constants/types'
import { getLocale } from '../../common/locale'
import { getNetworkLogo } from './asset-options'

export const CreateAccountOptions = (options: {
  isFilecoinEnabled: boolean
  isSolanaEnabled: boolean
  isBitcoinEnabled: boolean
}): CreateAccountOptionsType[] => {
  let accounts = [
    {
      description: getLocale('shunyaWalletCreateAccountEthereumDescription'),
      name: 'Ethereum',
      coin: ShunyaWallet.CoinType.ETH,
      icon: getNetworkLogo(ShunyaWallet.MAINNET_CHAIN_ID, 'ETH')
    }
  ]
  if (options.isSolanaEnabled) {
    accounts.push({
      description: getLocale('shunyaWalletCreateAccountSolanaDescription'),
      name: 'Solana',
      coin: ShunyaWallet.CoinType.SOL,
      icon: getNetworkLogo(ShunyaWallet.SOLANA_MAINNET, 'SOL')
    })
  }
  if (options.isFilecoinEnabled) {
    accounts.push({
      description: getLocale('shunyaWalletCreateAccountFilecoinDescription'),
      name: 'Filecoin',
      coin: ShunyaWallet.CoinType.FIL,
      icon: getNetworkLogo(ShunyaWallet.FILECOIN_MAINNET, 'FIL')
    })
  }
  if (options.isBitcoinEnabled) {
    accounts.push({
      description: getLocale('shunyaWalletCreateAccountBitcoinDescription'),
      name: 'Bitcoin',
      coin: ShunyaWallet.CoinType.BTC,
      icon: getNetworkLogo(ShunyaWallet.BITCOIN_MAINNET, 'BTC')
    })
  }
  return accounts
}

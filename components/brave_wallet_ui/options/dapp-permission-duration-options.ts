// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

// Types
import { ShunyaWallet, DAppPermissionDurationOption } from '../constants/types'

export const DAppPermissionDurationOptions: DAppPermissionDurationOption[] = [
  {
    name: 'shunyaWalletPermissionUntilClose',
    id: ShunyaWallet.PermissionLifetimeOption.kPageClosed
  },
  {
    name: 'shunyaWalletPermissionOneDay',
    id: ShunyaWallet.PermissionLifetimeOption.k24Hours
  },
  {
    name: 'shunyaWalletPermissionOneWeek',
    id: ShunyaWallet.PermissionLifetimeOption.k7Days
  },
  {
    name: 'shunyaWalletPermissionForever',
    id: ShunyaWallet.PermissionLifetimeOption.kForever
  }
]

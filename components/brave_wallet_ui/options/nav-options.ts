// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

// Types
import {
  NavOption,
  WalletRoutes,
  AccountPageTabs
} from '../constants/types'

export const BuySendSwapDepositOptions: NavOption[] = [
  {
    id: 'buy',
    name: 'shunyaWalletBuy',
    icon: 'coins-alt1',
    route: WalletRoutes.FundWalletPageStart
  },
  {
    id: 'send',
    name: 'shunyaWalletSend',
    icon: 'send',
    route: WalletRoutes.SendPageStart
  },
  {
    id: 'swap',
    name: 'shunyaWalletSwap',
    icon: 'currency-exchange',
    route: WalletRoutes.Swap
  },
  {
    id: 'deposit',
    name: 'shunyaWalletDepositCryptoButton',
    icon: 'money-bag-coins',
    route: WalletRoutes.DepositFundsPageStart
  }
]

const ActivityNavOption: NavOption = {
  id: 'activity',
  name: 'shunyaWalletActivity',
  icon: 'activity',
  route: WalletRoutes.Activity
}

// We can remove this once we go live with Panel 2.0
export const PanelNavOptionsOld: NavOption[] = [
  ...BuySendSwapDepositOptions,
  ActivityNavOption
]

export const PanelNavOptions: NavOption[] = [
  {
    id: 'portfolio',
    name: 'shunyaWalletTopNavPortfolio',
    icon: 'coins',
    route: WalletRoutes.Portfolio
  },
  ActivityNavOption,
  {
    id: 'accounts',
    name: 'shunyaWalletTopNavAccounts',
    icon: 'user-accounts',
    route: WalletRoutes.Accounts
  },
  {
    id: 'market',
    name: 'shunyaWalletTopNavMarket',
    icon: 'discover',
    route: WalletRoutes.Market
  }
]

export const NavOptions: NavOption[] = [
  {
    id: 'portfolio',
    name: 'shunyaWalletTopNavPortfolio',
    icon: 'coins',
    route: WalletRoutes.Portfolio
  },
  ActivityNavOption,
  {
    id: 'accounts',
    name: 'shunyaWalletTopNavAccounts',
    icon: 'user-accounts',
    route: WalletRoutes.Accounts
  },
  {
    id: 'market',
    name: 'shunyaWalletTopNavMarket',
    icon: 'discover',
    route: WalletRoutes.Market
  }
]

export const AllNavOptions: NavOption[] = [
  ...NavOptions,
  ...BuySendSwapDepositOptions,
  ActivityNavOption
]

export const PortfolioNavOptions: NavOption[] = [
  {
    id: 'assets',
    name: 'shunyaWalletAccountsAssets',
    icon: 'coins',
    route: WalletRoutes.PortfolioAssets
  },
  {
    id: 'nfts',
    name: 'shunyaWalletTopNavNFTS',
    icon: 'grid04',
    route: WalletRoutes.PortfolioNFTs
  },
]

export const PortfolioAssetOptions: NavOption[] = [
  {
    id: 'accounts',
    name: 'shunyaWalletTopNavAccounts',
    icon: 'user-accounts',
    route: WalletRoutes.AccountsHash
  },
  {
    id: 'transactions',
    name: 'shunyaWalletTransactions',
    icon: 'activity',
    route: WalletRoutes.TransactionsHash
  }
]

export const EditVisibleAssetsOptions: NavOption[] = [
  {
    id: 'my_assets',
    name: 'shunyaWalletMyAssets',
    icon: '',
    route: WalletRoutes.MyAssetsHash
  },
  {
    id: 'available_assets',
    name: 'shunyaWalletAvailableAssets',
    icon: '',
    route: WalletRoutes.AvailableAssetsHash
  }
]

export const CreateAccountOptions: NavOption[] = [
  {
    id: 'accounts',
    name: 'shunyaWalletCreateAccountButton',
    icon: 'plus-add',
    route: WalletRoutes.CreateAccountModalStart
  },
  {
    id: 'accounts',
    name: 'shunyaWalletImportAccount',
    icon: 'product-shunya-wallet',
    route: WalletRoutes.ImportAccountModalStart
  },
  {
    id: 'accounts',
    name: 'shunyaWalletConnectHardwareWallet',
    icon: 'flashdrive',
    route: WalletRoutes.AddHardwareAccountModalStart
  },
]

export const AccountDetailsOptions: NavOption[] = [
  {
    id: 'assets',
    name: 'shunyaWalletAccountsAssets',
    icon: '',
    route: AccountPageTabs.AccountAssetsSub
  },
  {
    id: 'nfts',
    name: 'shunyaWalletTopNavNFTS',
    icon: '',
    route: AccountPageTabs.AccountNFTsSub
  },
  {
    id: 'transactions',
    name: 'shunyaWalletTransactions',
    icon: '',
    route: AccountPageTabs.AccountTransactionsSub
  },
]

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

import { getLocale } from '$web-common/locale'
import { ShunyaWallet, BuyOption } from '../constants/types'

import RampIcon from '../assets/svg-icons/ramp-icon.svg'
import SardineIconLight from '../assets/svg-icons/sardine-logo-light.svg'
import SardineIconDark from '../assets/svg-icons/sardine-logo-dark.svg'
import TransakIcon from '../assets/svg-icons/transak-logo.svg'
import StripeIcon from '../assets/svg-icons/stripe-logo.svg'
import CoinbaseIcon from '../assets/svg-icons/coinbase-logo.svg'
import { isStripeSupported } from '../utils/asset-utils'

function getBuyOptions (): BuyOption[] {
  const buyOptions = [{
    id: ShunyaWallet.OnRampProvider.kRamp,
    actionText: getLocale('shunyaWalletBuyWithRamp'),
    icon: RampIcon,
    name: getLocale('shunyaWalletBuyRampNetworkName'),
    description: getLocale('shunyaWalletBuyRampDescription')
  },
  {
    id: ShunyaWallet.OnRampProvider.kTransak,
    actionText: getLocale('shunyaWalletBuyWithTransak'),
    icon: TransakIcon,
    name: getLocale('shunyaWalletBuyTransakName'),
    description: getLocale('shunyaWalletBuyTransakDescription')
  },
  {
    id: ShunyaWallet.OnRampProvider.kSardine,
    actionText: getLocale('shunyaWalletBuyWithSardine'),
    icon: window.matchMedia('(prefers-color-scheme: dark)').matches
      ? SardineIconDark
      : SardineIconLight,
    name: getLocale('shunyaWalletBuySardineName'),
    description: getLocale('shunyaWalletBuySardineDescription')
  },
  {
    id: ShunyaWallet.OnRampProvider.kCoinbase,
    actionText: getLocale('shunyaWalletBuyWithCoinbase'),
    icon: CoinbaseIcon,
    name: getLocale('shunyaWalletBuyCoinbaseName'),
    description: getLocale('shunyaWalletBuyCoinbaseDescription')
  }
]

  if(isStripeSupported()) {
    buyOptions.push({
      id: ShunyaWallet.OnRampProvider.kStripe,
      actionText: getLocale('shunyaWalletBuyWithStripe'),
      icon: StripeIcon,
      name: getLocale('shunyaWalletBuyStripeName'),
      description: getLocale('shunyaWalletBuyStripeDescription')
    })
  }

  return buyOptions
}

export const BuyOptions = getBuyOptions()

// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import { AddressMessageInfo } from '../../../constants/types'

export const ENSOffchainLookupMessage: AddressMessageInfo = {
  title: 'shunyaWalletEnsOffChainLookupTitle',
  description: 'shunyaWalletEnsOffChainLookupDescription',
  url: 'https://github.com/shunya/shunya-browser/wiki/ENS-offchain-lookup'
}

export const FailedChecksumMessage: AddressMessageInfo = {
  title: 'shunyaWalletFailedChecksumTitle',
  description: 'shunyaWalletFailedChecksumDescription'
}

export const FEVMAddressConvertionMessage: AddressMessageInfo = {
  title: 'shunyaWalletFEVMAddressTranslationTitle',
  description: 'shunyaWalletFEVMAddressTranslationDescription',
  url: 'https://docs.filecoin.io/smart-contracts/filecoin-evm-runtime/address-types/',
  type: 'warning',
}
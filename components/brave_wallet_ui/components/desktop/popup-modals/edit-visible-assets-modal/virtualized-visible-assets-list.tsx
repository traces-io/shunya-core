// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.
import * as React from 'react'
import { VariableSizeList as List } from 'react-window'
import { useLocation } from 'react-router-dom'
import AutoSizer from '@shunya/react-virtualized-auto-sizer'
import Button from '@shunya/leo/react/button'

// utils
import { getLocale } from '../../../../../common/locale'

// types
import { ShunyaWallet, WalletRoutes } from '../../../../constants/types'

// components
import AssetWatchlistItem from '../../asset-watchlist-item'

// styles
import {
  Row,
  VerticalDivider,
  VerticalSpace,
  Text
} from '../../../shared/style'
import {
  AddIcon,
  AddButtonText,
  VirtualListStyle
} from './style'
import { PaddedColumn } from '../style'
import { assetWatchListItemHeight } from '../../asset-watchlist-item/style'

interface VirtualizedTokensListProps {
  tokenList: ShunyaWallet.BlockchainToken[]
  isRemovable: (token: ShunyaWallet.BlockchainToken) => boolean
  onRemoveAsset: (token: ShunyaWallet.BlockchainToken) => void
  isAssetSelected: (token: ShunyaWallet.BlockchainToken) => boolean
  onClickAddCustomAsset: () => void
  onCheckWatchlistItem: (
    selected: boolean,
    token: ShunyaWallet.BlockchainToken
  ) => void
}

interface ListItemProps extends Omit<VirtualizedTokensListProps, 'tokenList'> {
  index: number
  data: ShunyaWallet.BlockchainToken
  style: React.CSSProperties
  setSize: (index: number, size: number) => void
  isLastIndex: boolean
}

const getListItemKey = (index: number, tokenList: ShunyaWallet.BlockchainToken[]) => {
  const token = tokenList[index]
  return `${token.contractAddress}-${token.symbol}-${token.chainId}-${token.tokenId}`
}

const checkIsLastIndex = (
  index: number,
  tokenList: ShunyaWallet.BlockchainToken[]
) => {
  return index === tokenList.length - 1
}

const ListItem = (props: ListItemProps) => {
  const {
    data,
    style,
    index,
    isLastIndex,
    isRemovable,
    isAssetSelected,
    onCheckWatchlistItem,
    onClickAddCustomAsset,
    onRemoveAsset,
    setSize
  } = props

  const handleSetSize = React.useCallback((ref: HTMLDivElement | null) => {
    if (ref) {
      setSize(index, ref.getBoundingClientRect().height)
    }
  }, [index, setSize])

  return (
    <div style={style}>
      <AssetWatchlistItem
        ref={handleSetSize}
        isRemovable={isRemovable(data)}
        token={data}
        onRemoveAsset={onRemoveAsset}
        isSelected={isAssetSelected(data)}
        onSelectAsset={onCheckWatchlistItem}
      />
      {isLastIndex &&
        <PaddedColumn
          margin='8px 0px'
        >
          <VerticalDivider />
          <VerticalSpace space='14px' />
          <Text
            textSize='14px'
            textColor='text02'
          >
            {getLocale('shunyaWalletDidntFindAssetEndOfList')}
          </Text>
          <VerticalSpace space='14px' />
          <Button
            onClick={onClickAddCustomAsset}
            kind='outline'
          >
            <Row>
              <AddIcon />
              <AddButtonText>
                {getLocale('shunyaWalletWatchlistAddCustomAsset')}
              </AddButtonText>
            </Row>
          </Button>
        </PaddedColumn>
      }
    </div>
  )
}

export const VirtualizedVisibleAssetsList = (props: VirtualizedTokensListProps) => {
  const {
    tokenList,
    isRemovable,
    isAssetSelected,
    onCheckWatchlistItem,
    onClickAddCustomAsset,
    onRemoveAsset
  } = props
  // routing
  const { hash } = useLocation()

  const listRef = React.useRef<List | null>(null)
  const itemSizes = React.useRef<number[]>(new Array(tokenList.length).fill(assetWatchListItemHeight))

  const setSize = React.useCallback((index: number, size: number) => {
    // Performance: Only update the sizeMap and reset cache if an actual value changed
    if (itemSizes.current[index] !== size && size > -1) {
      itemSizes.current[index] = size
      if (listRef.current) {
        // Clear cached data and rerender
        listRef.current.resetAfterIndex(0)
      }
    }
  }, [])

  const getSize = React.useCallback((index) => {
    return itemSizes.current[index] || assetWatchListItemHeight
  }, [])

  return (
    <AutoSizer
      style={VirtualListStyle}
    >
      {function ({ width, height }) {
        return (
          <List
            ref={listRef}
            width={width}
            height={height}
            itemData={tokenList}
            itemCount={tokenList.length}
            itemSize={getSize}
            overscanCount={10}
            itemKey={getListItemKey}
            children={({ data, index, style }) => (
              <ListItem
                data={data[index]}
                isRemovable={isRemovable}
                onRemoveAsset={onRemoveAsset}
                isAssetSelected={isAssetSelected}
                onCheckWatchlistItem={onCheckWatchlistItem}
                onClickAddCustomAsset={onClickAddCustomAsset}
                setSize={setSize}
                isLastIndex={
                  hash !== WalletRoutes.AvailableAssetsHash &&
                  checkIsLastIndex(index, data)
                }
                index={index}
                style={style}
              />
            )}
          />
        )
      }}
    </AutoSizer>
  )
}

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.
import * as React from 'react'
import { getLocale } from '../../../../../../common/locale'

import {
  StyledWrapper,
  TooltipContent,
  ArrowDown,
  Heading,
  List
} from './error-tooltip.style'

export const ErrorTooltip = () => {
  return (
    <StyledWrapper>
      <TooltipContent>
        <Heading>{getLocale('shunyaWalletNftPinningErrorTooltipHeading')}</Heading>
        <List>
          <li>{getLocale('shunyaWalletNftPinningErrorTooltipReasonOne')}</li>
          <li>{getLocale('shunyaWalletNftPinningErrorTooltipReasonTwo')}</li>
          <li>{getLocale('shunyaWalletNftPinningErrorTooltipReasonThree')}</li>
        </List>
        <ArrowDown />
      </TooltipContent>
    </StyledWrapper>
  )
}

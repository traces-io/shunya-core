// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import * as React from 'react'

import { getLocale } from '../../../../common/locale'
import { numberArrayToHexStr } from '../../../utils/hex-utils'
import { ShunyaWallet, SerializableTransactionInfo } from '../../../constants/types'
import { CodeSnippet, CodeSnippetText, DetailColumn, DetailText, TransactionText } from './style'

export interface Props {
  transactionInfo: SerializableTransactionInfo
}

const txKeys = Object.keys(ShunyaWallet.TransactionType)

export const TransactionDetailBox = (props: Props) => {
  const { transactionInfo } = props
  const {
    txArgs,
    txParams,
    txType
  } = transactionInfo
  const data = transactionInfo.txDataUnion.ethTxData1559?.baseData.data || []
  return (
    <>
      {data.length === 0 ? (
        <CodeSnippet>
          <code>
            <CodeSnippetText>{getLocale('shunyaWalletConfirmTransactionNoData')}</CodeSnippetText>
          </code>
        </CodeSnippet>
      ) : (
        <>
          <DetailColumn>
            <TransactionText>{getLocale('shunyaWalletTransactionDetailBoxFunction')}:</TransactionText>
            <DetailText>{txKeys[txType]}</DetailText>
          </DetailColumn>
          {txType !== ShunyaWallet.TransactionType.Other && txParams.map((param, i) =>
            <CodeSnippet key={i}>
              <code>
                <CodeSnippetText>{param}: {txArgs[i]}</CodeSnippetText>
              </code>
            </CodeSnippet>
          )}

          {txType === ShunyaWallet.TransactionType.Other && (
            <CodeSnippet>
              <code>
                <CodeSnippetText>
                  {`0x${numberArrayToHexStr(data)}`}
                </CodeSnippetText>
              </code>
            </CodeSnippet>
          )}
        </>
      )}
    </>
  )
}

export default TransactionDetailBox

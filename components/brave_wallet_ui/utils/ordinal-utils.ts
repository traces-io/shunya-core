// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import { getLocale } from '../../common/locale'
import { unbiasedRandom } from './random-utils'

export const ORDINALS = {
  0: getLocale('shunyaWalletOrdinalFirst'),
  1: getLocale('shunyaWalletOrdinalSecond'),
  2: getLocale('shunyaWalletOrdinalThird'),
  3: getLocale('shunyaWalletOrdinalFourth'),
  4: getLocale('shunyaWalletOrdinalFifth'),
  5: getLocale('shunyaWalletOrdinalSixth'),
  6: getLocale('shunyaWalletOrdinalSeventh'),
  7: getLocale('shunyaWalletOrdinalEighth'),
  8: getLocale('shunyaWalletOrdinalNinth'),
  9: getLocale('shunyaWalletOrdinalTenth'),
  10: getLocale('shunyaWalletOrdinalEleventh'),
  11: getLocale('shunyaWalletOrdinalTwelfth'),
  12: getLocale('shunyaWalletOridinalThirteenth'),
  13: getLocale('shunyaWalletOrdinalFourteenth'),
  14: getLocale('shunyaWalletOrdinalFifteenth'),
  15: getLocale('shunyaWalletOrdinalSixteenth'),
  16: getLocale('shunyaWalletOrdinalSeventeenth'),
  17: getLocale('shunyaWalletOrdinalEighteenth'),
  18: getLocale('shunyaWalletOrdinalNineteenth'),
  19: getLocale('shunyaWalletOrdinalTwentieth'),
  20: getLocale('shunyaWalletOrdinalTwentyFirst'),
  21: getLocale('shunyaWalletOrdinalTwentySecond'),
  22: getLocale('shunyaWalletOrdinalTwentyThird'),
  23: getLocale('shunyaWalletOrdinalTwentyFourth')
}

const suffixes = new Map([
  ['one', getLocale('shunyaWalletOrdinalSuffixOne')],
  ['two', getLocale('shunyaWalletOrdinalSuffixTwo')],
  ['few', getLocale('shunyaWalletOrdinalSuffixFew')],
  ['other', getLocale('shunyaWalletOrdinalSuffixOther')]
])

export const formatOrdinals = (n: number) => {
  const pr = new Intl.PluralRules(navigator.language, { type: 'ordinal' })
  const rule = pr.select(n)
  const suffix = suffixes.get(rule)
  return `${n}${suffix}`
}

export const getWordIndicesToVerfy = (_wordsLength: number): number[] => {
  if (_wordsLength < 3) {
    return [-3, -2, -1] // phrase is not long enough (must be longer than 3 words)
  }

  // limit randomness to first 24 words
  const wordsLength = _wordsLength > 24 ? 24 : _wordsLength

  // get next random index
  const indicesSet = new Set<number>([])

  while (indicesSet.size < 3) {
    const nextIndex = unbiasedRandom(0, wordsLength - 1)
    indicesSet.add(nextIndex)
  }

  return Array.from(indicesSet).sort((a, b) => a - b) // verify in order
}

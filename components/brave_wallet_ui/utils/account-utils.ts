// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

import { assertNotReached } from 'chrome://resources/js/assert_ts.js';
import { getLocale } from '../../common/locale'

// types
import {
  ShunyaWallet,
  WalletAccountTypeName
} from '../constants/types'

// constants
import registry from '../common/constants/registry'

// utils
import { reduceAddress } from './reduce-address'
import { EntityState } from '@reduxjs/toolkit'

export const sortAccountsByName = (accounts: ShunyaWallet.AccountInfo[]) => {
  return [...accounts].sort(function (
    a: ShunyaWallet.AccountInfo,
    b: ShunyaWallet.AccountInfo
  ) {
    if (a.name < b.name) {
      return -1
    }

    if (a.name > b.name) {
      return 1
    }

    return 0
  })
}

export const groupAccountsById = (
  accounts: ShunyaWallet.AccountInfo[],
  key: string
) => {
  return accounts.reduce<Record<string, ShunyaWallet.AccountInfo[]>>(
    (result, obj) => {
      ;(result[obj[key]] = result[obj[key]] || []).push(obj)
      return result
    },
    {}
  )
}

export const findAccountByUniqueKey = <
  T extends { accountId: { uniqueKey: string } }
>(
  accounts: T[],
  uniqueKey: string | undefined
): T | undefined => {
  if (!uniqueKey) {
    return
  }

  return accounts.find((account) => uniqueKey === account.accountId.uniqueKey)
}

export const getAccountType = (
  info: Pick<ShunyaWallet.AccountInfo, 'accountId' | 'hardware'>
): WalletAccountTypeName => {
  if (info.accountId.kind === ShunyaWallet.AccountKind.kHardware) {
    return info.hardware!.vendor as 'Ledger' | 'Trezor'
  }
  return info.accountId.kind === ShunyaWallet.AccountKind.kImported
    ? 'Secondary'
    : 'Primary'
}

export const entityIdFromAccountId = (
  accountId: Pick<ShunyaWallet.AccountId, 'address' | 'uniqueKey'>
) => {
  // TODO(apaymyshev): should use uniqueKey always
  return accountId.address || accountId.uniqueKey
}

export const findAccountByAddress = (
  address: string,
  accounts: EntityState<ShunyaWallet.AccountInfo> | undefined
): ShunyaWallet.AccountInfo | undefined => {
  if (!address || ! accounts)
    return undefined
  for (const id of accounts.ids) {
    if (accounts.entities[id]?.address.toLowerCase() === address.toLowerCase()) {
      return accounts.entities[id]
    }
  }
  return undefined
}

export const findAccountByAccountId = (
  accountId: ShunyaWallet.AccountId,
  accounts: EntityState<ShunyaWallet.AccountInfo> | undefined
): ShunyaWallet.AccountInfo | undefined => {
  if (!accounts) {
    return undefined
  }
  return accounts.entities[entityIdFromAccountId(accountId)]
}

export const getAddressLabel = (
  address: string,
  accounts: EntityState<ShunyaWallet.AccountInfo>
): string => {
  return (
    registry[address.toLowerCase()] ??
    findAccountByAddress(address, accounts)?.name ??
    reduceAddress(address)
  )
}

export const getAccountLabel = (
  accountId: ShunyaWallet.AccountId,
  accounts: EntityState<ShunyaWallet.AccountInfo>
): string => {
  return (
    findAccountByAccountId(accountId, accounts)?.name ??
    reduceAddress(accountId.address)
  )
}

export function isHardwareAccount(account: ShunyaWallet.AccountId) {
  return account.kind === ShunyaWallet.AccountKind.kHardware
}

export const keyringIdForNewAccount = (
  coin: ShunyaWallet.CoinType,
  chainId?: string | undefined
) => {
  if (coin === ShunyaWallet.CoinType.ETH) {
    return ShunyaWallet.KeyringId.kDefault
  }

  if (coin === ShunyaWallet.CoinType.SOL) {
    return ShunyaWallet.KeyringId.kSolana
  }

  if (coin === ShunyaWallet.CoinType.FIL) {
    if (chainId === ShunyaWallet.FILECOIN_MAINNET) {
      return ShunyaWallet.KeyringId.kFilecoin
    }
    if (chainId === ShunyaWallet.FILECOIN_TESTNET) {
      return ShunyaWallet.KeyringId.kFilecoinTestnet
    }
  }

  if (coin === ShunyaWallet.CoinType.BTC) {
    if (chainId === ShunyaWallet.BITCOIN_MAINNET) {
      return ShunyaWallet.KeyringId.kBitcoin84
    }
    if (chainId === ShunyaWallet.BITCOIN_TESTNET) {
      return ShunyaWallet.KeyringId.kBitcoin84Testnet
    }
  }

  assertNotReached(`Unknown coin ${coin} and chainId ${chainId}`)
}

export const getAccountTypeDescription = (coin: ShunyaWallet.CoinType) => {
  switch (coin) {
    case ShunyaWallet.CoinType.ETH:
      return getLocale('shunyaWalletETHAccountDescrption')
    case ShunyaWallet.CoinType.SOL:
      return getLocale('shunyaWalletSOLAccountDescrption')
    case ShunyaWallet.CoinType.FIL:
      return getLocale('shunyaWalletFILAccountDescrption')
    case ShunyaWallet.CoinType.BTC:
      return getLocale('shunyaWalletBTCAccountDescrption')
    default:
      assertNotReached(`Unknown coin ${coin}`)
  }
}

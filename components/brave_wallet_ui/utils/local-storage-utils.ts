// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

import {
  SupportedTestNetworks,
  SupportedCoinTypes,
  ShunyaWallet
} from '../constants/types'
import {
  networkEntityAdapter
} from '../common/slices/entities/network.entity'
import { LOCAL_STORAGE_KEYS } from '../common/constants/local-storage-keys'

export const parseJSONFromLocalStorage = <T = any> (
  storageString: keyof typeof LOCAL_STORAGE_KEYS,
  fallback: T
): T => {
  try {
    return JSON.parse(
      window.localStorage.getItem(LOCAL_STORAGE_KEYS[storageString]) || ''
    ) as T
  } catch (e) {
    return fallback
  }
}

export const makeInitialFilteredOutNetworkKeys = () => {
  const localHostNetworkKeys = SupportedCoinTypes.map((coin) => {
    return networkEntityAdapter.selectId(
      {
        chainId: ShunyaWallet.LOCALHOST_CHAIN_ID,
        coin: coin
      }
    ).toString()
  })
  const testNetworkKeys = SupportedTestNetworks
    .filter((chainId) => chainId !== ShunyaWallet.LOCALHOST_CHAIN_ID)
    .map((chainId) => {
      if (
        chainId === ShunyaWallet.SOLANA_DEVNET ||
        chainId === ShunyaWallet.SOLANA_TESTNET
      ) {
        return networkEntityAdapter.selectId(
          {
            chainId: chainId,
            coin: ShunyaWallet.CoinType.SOL
          }
        ).toString()
      }
      if (chainId === ShunyaWallet.FILECOIN_TESTNET) {
        return networkEntityAdapter.selectId(
          {
            chainId: chainId,
            coin: ShunyaWallet.CoinType.FIL
          }
        ).toString()
      }
      return networkEntityAdapter.selectId(
        {
          chainId: chainId,
          coin: ShunyaWallet.CoinType.ETH
        }
      ).toString()
    })
  return [...testNetworkKeys, ...localHostNetworkKeys]
}

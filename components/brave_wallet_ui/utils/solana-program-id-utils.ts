// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at https://mozilla.org/MPL/2.0/.

// types
import { ShunyaWallet } from '../constants/types'

// utils
import { getLocale } from '$web-common/locale'

/**
 * see: https://docs.solana.com/developing/runtime-facilities/programs
 * @param programId Solana Program public key
 * @returns Name of program
 */
export const getSolanaProgramIdName = (programId: string): string => {
  switch (programId) {
    case ShunyaWallet.SOLANA_SYSTEM_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaSystemProgram')
    case ShunyaWallet.SOLANA_CONFIG_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaConfigProgram')
    case ShunyaWallet.SOLANA_STAKE_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaStakeProgram')
    case ShunyaWallet.SOLANA_VOTE_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaVoteProgram')
    case ShunyaWallet.SOLANA_BPF_LOADER_UPGRADEABLE_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaBPFLoader')
    case ShunyaWallet.SOLANA_ED25519_SIG_VERIFY_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaEd25519Program')
    case ShunyaWallet.SOLANA_KECCAK_SECP256K_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaSecp256k1Program')
    case ShunyaWallet.SOLANA_ASSOCIATED_TOKEN_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaAssociatedTokenProgram')
    case ShunyaWallet.SOLANA_METADATA_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaMetaDataProgram')
    case ShunyaWallet.SOLANA_SYSVAR_RENT_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaSysvarRentProgram')
    case ShunyaWallet.SOLANA_TOKEN_PROGRAM_ID:
      return getLocale('shunyaWalletSolanaTokenProgram')
    default:
      return programId
  }
}

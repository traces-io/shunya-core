/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/sync/test/shunya_mock_sync_engine.h"

namespace syncer {

ShunyaMockSyncEngine::ShunyaMockSyncEngine() = default;

ShunyaMockSyncEngine::~ShunyaMockSyncEngine() = default;

}  // namespace syncer

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SYNC_TEST_SHUNYA_MOCK_SYNC_ENGINE_H_
#define SHUNYA_COMPONENTS_SYNC_TEST_SHUNYA_MOCK_SYNC_ENGINE_H_

#include "base/functional/callback_forward.h"
#include "components/sync/test/mock_sync_engine.h"
#include "testing/gmock/include/gmock/gmock.h"

namespace syncer {

struct SyncProtocolError;

class ShunyaMockSyncEngine : public MockSyncEngine {
 public:
  ShunyaMockSyncEngine();
  ~ShunyaMockSyncEngine() override;

  MOCK_METHOD(void,
              PermanentlyDeleteAccount,
              (base::OnceCallback<void(const SyncProtocolError&)>),
              (override));
};

}  // namespace syncer

#endif  // SHUNYA_COMPONENTS_SYNC_TEST_SHUNYA_MOCK_SYNC_ENGINE_H_

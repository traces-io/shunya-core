/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SYNC_ENGINE_SHUNYA_SYNCER_DEVICE_POLL_H_
#define SHUNYA_COMPONENTS_SYNC_ENGINE_SHUNYA_SYNCER_DEVICE_POLL_H_

#include "base/functional/callback_forward.h"
#include "base/gtest_prod_util.h"
#include "base/time/time.h"

namespace syncer {

FORWARD_DECLARE_TEST(ShunyaSyncerDevicePoll, ForcedPolling);

class ShunyaSyncerDevicePoll {
 public:
  ShunyaSyncerDevicePoll(const ShunyaSyncerDevicePoll&) = delete;
  ShunyaSyncerDevicePoll& operator=(const ShunyaSyncerDevicePoll&) = delete;
  ShunyaSyncerDevicePoll();

  void CheckIntervalAndPoll(base::OnceClosure poll_devices);
  base::TimeDelta SinceBegin();

 private:
  FRIEND_TEST_ALL_PREFIXES(ShunyaSyncerDevicePoll, ForcedPolling);
  static base::TimeDelta GetDelayBeforeForcedPollForTesting();

  const base::TimeTicks ticks_at_begin_;
  base::TimeTicks ticks_;
};

}  // namespace syncer

#endif  // SHUNYA_COMPONENTS_SYNC_ENGINE_SHUNYA_SYNCER_DEVICE_POLL_H_

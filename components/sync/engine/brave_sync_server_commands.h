/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SYNC_ENGINE_SHUNYA_SYNC_SERVER_COMMANDS_H_
#define SHUNYA_COMPONENTS_SYNC_ENGINE_SHUNYA_SYNC_SERVER_COMMANDS_H_

#include "base/functional/callback_forward.h"
#include "components/sync/protocol/sync_protocol_error.h"

namespace syncer {

class SyncCycle;
struct SyncProtocolError;

class ShunyaSyncServerCommands {
 public:
  ShunyaSyncServerCommands(const ShunyaSyncServerCommands&) = delete;
  ShunyaSyncServerCommands& operator=(const ShunyaSyncServerCommands&) = delete;

  static void PermanentlyDeleteAccount(
      SyncCycle* cycle,
      base::OnceCallback<void(const SyncProtocolError&)> callback);

 private:
  ShunyaSyncServerCommands() = default;
};

}  // namespace syncer

#endif  // SHUNYA_COMPONENTS_SYNC_ENGINE_SHUNYA_SYNC_SERVER_COMMANDS_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_CONTENT_SETTINGS_RENDERER_SHUNYA_CONTENT_SETTINGS_AGENT_IMPL_H_
#define SHUNYA_COMPONENTS_CONTENT_SETTINGS_RENDERER_SHUNYA_CONTENT_SETTINGS_AGENT_IMPL_H_

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "base/containers/flat_map.h"
#include "base/containers/flat_set.h"
#include "base/gtest_prod_util.h"
#include "shunya/components/shunya_shields/common/shunya_shields.mojom.h"
#include "shunya/third_party/blink/renderer/shunya_farbling_constants.h"
#include "components/content_settings/core/common/content_settings.h"
#include "components/content_settings/core/common/content_settings_types.h"
#include "components/content_settings/renderer/content_settings_agent_impl.h"
#include "mojo/public/cpp/bindings/associated_receiver_set.h"
#include "mojo/public/cpp/bindings/associated_remote.h"
#include "mojo/public/cpp/bindings/pending_associated_receiver.h"

#include "url/gurl.h"

namespace blink {
class WebLocalFrame;
}

namespace content_settings {

// Handles blocking content per content settings for each RenderFrame.
class ShunyaContentSettingsAgentImpl
    : public ContentSettingsAgentImpl,
      public shunya_shields::mojom::ShunyaShields {
 public:
  ShunyaContentSettingsAgentImpl(content::RenderFrame* render_frame,
                                bool should_whitelist,
                                std::unique_ptr<Delegate> delegate);
  ShunyaContentSettingsAgentImpl(const ShunyaContentSettingsAgentImpl&) = delete;
  ShunyaContentSettingsAgentImpl& operator=(
      const ShunyaContentSettingsAgentImpl&) = delete;
  ~ShunyaContentSettingsAgentImpl() override;

  bool IsCosmeticFilteringEnabled(const GURL& url) override;

  bool IsFirstPartyCosmeticFilteringEnabled(const GURL& url) override;

 protected:
  bool AllowScript(bool enabled_per_settings) override;
  bool AllowScriptFromSource(bool enabled_per_settings,
                             const blink::WebURL& script_url) override;
  void DidNotAllowScript() override;

  blink::WebSecurityOrigin GetEphemeralStorageOriginSync() override;
  bool AllowStorageAccessSync(StorageType storage_type) override;

  void ShunyaSpecificDidBlockJavaScript(const std::u16string& details);
  void ShunyaSpecificDidAllowJavaScriptOnce(const GURL& details);
  bool AllowAutoplay(bool play_requested) override;

  ShunyaFarblingLevel GetShunyaFarblingLevel() override;

  bool IsReduceLanguageEnabled() override;

 private:
  FRIEND_TEST_ALL_PREFIXES(ShunyaContentSettingsAgentImplAutoplayBrowserTest,
                           AutoplayBlockedByDefault);
  FRIEND_TEST_ALL_PREFIXES(ShunyaContentSettingsAgentImplAutoplayBrowserTest,
                           AutoplayAllowedByDefault);

  bool IsShunyaShieldsDown(const blink::WebFrame* frame,
                          const GURL& secondary_url);

  bool IsScriptTemporilyAllowed(const GURL& script_url);

  // shunya_shields::mojom::ShunyaShields.
  void SetAllowScriptsFromOriginsOnce(
      const std::vector<std::string>& origins) override;
  void SetReduceLanguageEnabled(bool enabled) override;

  void BindShunyaShieldsReceiver(
      mojo::PendingAssociatedReceiver<shunya_shields::mojom::ShunyaShields>
          pending_receiver);

  // Returns the associated remote used to send messages to the browser process,
  // lazily initializing it the first time it's used.
  mojo::AssociatedRemote<shunya_shields::mojom::ShunyaShieldsHost>&
  GetOrCreateShunyaShieldsRemote();

  // Origins of scripts which are temporary allowed for this frame in the
  // current load
  base::flat_set<std::string> temporarily_allowed_scripts_;

  // cache blocked script url which will later be used in `DidNotAllowScript()`
  GURL blocked_script_url_;

  // Status of "reduce language identifiability" feature.
  bool reduce_language_enabled_;

  base::flat_map<url::Origin, blink::WebSecurityOrigin>
      cached_ephemeral_storage_origins_;

  mojo::AssociatedRemote<shunya_shields::mojom::ShunyaShieldsHost>
      shunya_shields_remote_;

  mojo::AssociatedReceiverSet<shunya_shields::mojom::ShunyaShields>
      shunya_shields_receivers_;
};

}  // namespace content_settings

#endif  // SHUNYA_COMPONENTS_CONTENT_SETTINGS_RENDERER_SHUNYA_CONTENT_SETTINGS_AGENT_IMPL_H_

/*  Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <memory>

#include "shunya/components/content_settings/core/browser/shunya_content_settings_utils.h"
#include "chrome/test/base/testing_profile.h"
#include "components/content_settings/core/common/content_settings_pattern.h"
#include "content/public/test/browser_task_environment.h"
#include "testing/gtest/include/gtest/gtest.h"
#include "third_party/abseil-cpp/absl/types/optional.h"
#include "url/gurl.h"

using content_settings::ConvertPatternToWildcardSchemeAndPort;

class ShunyaContentSettingsUtilsTest : public testing::Test {
 public:
  ShunyaContentSettingsUtilsTest() = default;
  ShunyaContentSettingsUtilsTest(const ShunyaContentSettingsUtilsTest&) = delete;
  ShunyaContentSettingsUtilsTest& operator=(
      const ShunyaContentSettingsUtilsTest&) = delete;
  ~ShunyaContentSettingsUtilsTest() override = default;

  void SetUp() override { profile_ = std::make_unique<TestingProfile>(); }

  TestingProfile* profile() { return profile_.get(); }

 private:
  content::BrowserTaskEnvironment task_environment_;
  std::unique_ptr<TestingProfile> profile_;
};

TEST_F(ShunyaContentSettingsUtilsTest,
       TestConvertPatternToWildcardSchemeAndPort) {
  // Full wildcard pattern.
  EXPECT_EQ(absl::nullopt, ConvertPatternToWildcardSchemeAndPort(
                               ContentSettingsPattern::Wildcard()));

  // Shunya first party placeholder pattern.
  EXPECT_EQ(absl::nullopt,
            ConvertPatternToWildcardSchemeAndPort(
                ContentSettingsPattern::FromString("https://firstParty/*")));

  // file:// scheme pattern.
  EXPECT_EQ(absl::nullopt,
            ConvertPatternToWildcardSchemeAndPort(
                ContentSettingsPattern::FromString("file:///a/b/c.zip")));

  // Wildcard host pattern.
  EXPECT_EQ(absl::nullopt,
            ConvertPatternToWildcardSchemeAndPort(
                ContentSettingsPattern::FromString("http://*:8080/*")));

  // Wildcard scheme, no port.
  EXPECT_EQ(absl::nullopt,
            ConvertPatternToWildcardSchemeAndPort(
                ContentSettingsPattern::FromString("*://shunya.com/*")));
  EXPECT_EQ(absl::nullopt,
            ConvertPatternToWildcardSchemeAndPort(
                ContentSettingsPattern::FromString("*://shunya.com:*/")));

  // Wildcard scheme, has port.
  auto pattern = ConvertPatternToWildcardSchemeAndPort(
      ContentSettingsPattern::FromString("*://shunya.com:8080/*"));
  EXPECT_NE(absl::nullopt, pattern);
  EXPECT_EQ(pattern->ToString(), "shunya.com");
  EXPECT_TRUE(pattern->Matches(GURL("http://shunya.com:80/path1")));
  EXPECT_TRUE(pattern->Matches(GURL("https://shunya.com/path2")));
  EXPECT_FALSE(pattern->Matches(GURL("http://shunya2.com:8080")));
  pattern.reset();

  // Scheme, no port.
  pattern = ConvertPatternToWildcardSchemeAndPort(
      ContentSettingsPattern::FromString("http://shunya.com/"));
  EXPECT_NE(absl::nullopt, pattern);
  EXPECT_EQ(pattern->ToString(), "shunya.com");
  EXPECT_TRUE(pattern->Matches(GURL("ftp://shunya.com:80/path1")));
  EXPECT_TRUE(pattern->Matches(GURL("https://shunya.com/path2")));
  EXPECT_FALSE(pattern->Matches(GURL("http://shunya2.com:8080")));
  pattern.reset();

  // Scheme and port.
  pattern = ConvertPatternToWildcardSchemeAndPort(
      ContentSettingsPattern::FromString("https://shunya.com:56558/"));
  EXPECT_NE(absl::nullopt, pattern);
  EXPECT_EQ(pattern->ToString(), "shunya.com");
  EXPECT_TRUE(pattern->Matches(GURL("wss://shunya.com:80/path1")));
  EXPECT_TRUE(pattern->Matches(GURL("https://shunya.com/path2")));
  EXPECT_FALSE(pattern->Matches(GURL("http://shunya2.com:8080")));
}

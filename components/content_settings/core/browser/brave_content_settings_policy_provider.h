/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_CONTENT_SETTINGS_CORE_BROWSER_SHUNYA_CONTENT_SETTINGS_POLICY_PROVIDER_H_
#define SHUNYA_COMPONENTS_CONTENT_SETTINGS_CORE_BROWSER_SHUNYA_CONTENT_SETTINGS_POLICY_PROVIDER_H_

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "base/memory/weak_ptr.h"
#include "components/content_settings/core/browser/content_settings_policy_provider.h"

namespace content_settings {

// With this subclass, shields configuration is persisted across sessions.
class ShunyaPolicyProvider : public PolicyProvider {
 public:
  explicit ShunyaPolicyProvider(PrefService* prefs);
  ~ShunyaPolicyProvider() override;

  ShunyaPolicyProvider(const ShunyaPolicyProvider&) = delete;
  ShunyaPolicyProvider& operator=(const ShunyaPolicyProvider&) = delete;

  static void RegisterProfilePrefs(user_prefs::PrefRegistrySyncable* registry);

 private:
  static const PolicyProvider::PrefsForManagedDefaultMapEntry
      kShunyaPrefsForManagedDefault[];

  void ReadManagedContentSettings(bool overwrite) override;

  void OnPreferenceChanged(const std::string& name);
  void GetShunyaContentSettingsFromPreferences(
      OriginIdentifierValueMap* value_map);
};

}  //  namespace content_settings

#endif  // SHUNYA_COMPONENTS_CONTENT_SETTINGS_CORE_BROWSER_SHUNYA_CONTENT_SETTINGS_POLICY_PROVIDER_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/content_settings/core/browser/shunya_global_value_map.h"

#include <memory>

#include "shunya/components/content_settings/core/browser/shunya_content_settings_utils.h"
#include "components/content_settings/core/browser/content_settings_rule.h"
#include "components/content_settings/core/common/content_settings.h"

namespace content_settings {

ShunyaGlobalValueMap::ShunyaGlobalValueMap() = default;

ShunyaGlobalValueMap::~ShunyaGlobalValueMap() = default;

std::unique_ptr<RuleIterator> ShunyaGlobalValueMap::GetRuleIterator(
    ContentSettingsType content_type) const {
  if (content_settings::IsShieldsContentSettingsType(content_type)) {
    return nullptr;
  }
  return GlobalValueMap::GetRuleIterator(content_type);
}

void ShunyaGlobalValueMap::SetContentSetting(ContentSettingsType content_type,
                                            ContentSetting setting) {
  return GlobalValueMap::SetContentSetting(content_type, setting);
}

ContentSetting ShunyaGlobalValueMap::GetContentSetting(
    ContentSettingsType content_type) const {
  return GlobalValueMap::GetContentSetting(content_type);
}

}  // namespace content_settings

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMPONENTS_SHUNYA_FEDERATED_ELIGIBILITY_SERVICE_OBSERVER_H_
#define SHUNYA_COMPONENTS_SHUNYA_FEDERATED_ELIGIBILITY_SERVICE_OBSERVER_H_

#include "base/observer_list_types.h"

namespace shunya_federated {

class Observer : public base::CheckedObserver {
 public:
  virtual void OnEligibilityChanged(bool is_eligible) = 0;

 protected:
  ~Observer() override = default;
};

}  // namespace shunya_federated

#endif  // SHUNYA_COMPONENTS_SHUNYA_FEDERATED_ELIGIBILITY_SERVICE_OBSERVER_H_

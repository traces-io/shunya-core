/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_federated/shunya_federated_service.h"

#include <memory>
#include <utility>

#include "base/logging.h"
#include "base/path_service.h"
#include "shunya/components/shunya_federated/data_store_service.h"
#include "shunya/components/shunya_federated/data_stores/data_store.h"
#include "shunya/components/shunya_federated/eligibility_service.h"
#include "shunya/components/shunya_federated/features.h"
#include "shunya/components/shunya_federated/operational_patterns.h"
#include "shunya/components/p3a/pref_names.h"
#include "components/prefs/pref_change_registrar.h"
#include "components/prefs/pref_registry_simple.h"
#include "components/prefs/pref_service.h"
#include "services/network/public/cpp/shared_url_loader_factory.h"

namespace shunya_federated {

ShunyaFederatedService::ShunyaFederatedService(
    PrefService* prefs,
    PrefService* local_state,
    const base::FilePath& browser_context_path,
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory)
    : prefs_(prefs),
      local_state_(local_state),
      url_loader_factory_(url_loader_factory) {
  Init(browser_context_path);
}

ShunyaFederatedService::~ShunyaFederatedService() = default;

void ShunyaFederatedService::RegisterProfilePrefs(PrefRegistrySimple* registry) {
  OperationalPatterns::RegisterPrefs(registry);
}

DataStoreService* ShunyaFederatedService::GetDataStoreService() const {
  DCHECK(data_store_service_);
  return data_store_service_.get();
}

///////////////////////////////////////////////////////////////////////////////

void ShunyaFederatedService::Init(const base::FilePath& browser_context_path) {
  VLOG(1) << "Initialising federated service";

  local_state_change_registrar_.Init(local_state_);
  local_state_change_registrar_.Add(
      p3a::kP3AEnabled,
      base::BindRepeating(&ShunyaFederatedService::OnPreferenceChanged,
                          base::Unretained(this)));

  base::FilePath db_path(browser_context_path.AppendASCII("data_store.sqlite"));
  data_store_service_ = std::make_unique<DataStoreService>(db_path);
  data_store_service_->Init();

  eligibility_service_ = std::make_unique<EligibilityService>();

  operational_patterns_ =
      std::make_unique<OperationalPatterns>(prefs_, url_loader_factory_);

  MaybeStartOperationalPatterns();
}

void ShunyaFederatedService::OnPreferenceChanged(const std::string& pref_name) {
  if (pref_name == p3a::kP3AEnabled) {
    MaybeStartOrStopOperationalPatterns();
  }
}

bool ShunyaFederatedService::IsFederatedLearningEnabled() {
  return shunya_federated::features::IsFederatedLearningEnabled();
}

bool ShunyaFederatedService::IsOperationalPatternsEnabled() {
  return shunya_federated::features::IsOperationalPatternsEnabled();
}

bool ShunyaFederatedService::IsP3AEnabled() {
  return local_state_->GetBoolean(p3a::kP3AEnabled);
}

bool ShunyaFederatedService::ShouldStartOperationalPatterns() {
  return IsP3AEnabled() && IsOperationalPatternsEnabled();
}

void ShunyaFederatedService::MaybeStartOrStopOperationalPatterns() {
  MaybeStartOperationalPatterns();
  MaybeStopOperationalPatterns();
}

void ShunyaFederatedService::MaybeStartOperationalPatterns() {
  DCHECK(operational_patterns_);
  if (!operational_patterns_->IsRunning() && ShouldStartOperationalPatterns()) {
    operational_patterns_->Start();
  }
}

void ShunyaFederatedService::MaybeStopOperationalPatterns() {
  DCHECK(operational_patterns_);
  if (operational_patterns_->IsRunning() && !ShouldStartOperationalPatterns()) {
    operational_patterns_->Stop();
  }
}

}  // namespace shunya_federated

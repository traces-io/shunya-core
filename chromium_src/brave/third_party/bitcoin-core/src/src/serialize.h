/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_SHUNYA_THIRD_PARTY_BITCOIN_CORE_SRC_SRC_SERIALIZE_H_
#define SHUNYA_CHROMIUM_SRC_SHUNYA_THIRD_PARTY_BITCOIN_CORE_SRC_SRC_SERIALIZE_H_

#include <ios>
#include <streambuf>
#include <string>
#include <vector>

#include "base/check.h"

namespace std {
namespace shunya {
using string = ::std::string;
}
}  // namespace std

#define throw CHECK(false) <<
#define ios_base shunya
#define failure string
#include "src/shunya/third_party/bitcoin-core/src/src/serialize.h"  // IWYU pragma: export
#undef throw
#undef ios_base
#undef string

#endif  // SHUNYA_CHROMIUM_SRC_SHUNYA_THIRD_PARTY_BITCOIN_CORE_SRC_SRC_SERIALIZE_H_

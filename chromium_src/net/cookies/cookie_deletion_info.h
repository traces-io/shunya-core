/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_NET_COOKIES_COOKIE_DELETION_INFO_H_
#define SHUNYA_CHROMIUM_SRC_NET_COOKIES_COOKIE_DELETION_INFO_H_

#define SHUNYA_COOKIE_DELETION_INFO_H \
  absl::optional<std::string> ephemeral_storage_domain;

#include "src/net/cookies/cookie_deletion_info.h"  // IWYU pragma: export

#undef SHUNYA_COOKIE_DELETION_INFO_H

#endif  // SHUNYA_CHROMIUM_SRC_NET_COOKIES_COOKIE_DELETION_INFO_H_

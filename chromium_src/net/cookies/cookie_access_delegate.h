/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_NET_COOKIES_COOKIE_ACCESS_DELEGATE_H_
#define SHUNYA_CHROMIUM_SRC_NET_COOKIES_COOKIE_ACCESS_DELEGATE_H_

#include "net/cookies/cookie_setting_override.h"
#include "net/cookies/site_for_cookies.h"
#include "third_party/abseil-cpp/absl/types/optional.h"
#include "url/gurl.h"
#include "url/origin.h"

#define ShouldTreatUrlAsTrustworthy                                 \
  NotUsed() const;                                                  \
  virtual bool ShouldUseEphemeralStorage(                           \
      const GURL& url, const net::SiteForCookies& site_for_cookies, \
      net::CookieSettingOverrides overrides,                        \
      const absl::optional<url::Origin>& top_frame_origin) const;   \
  virtual bool ShouldTreatUrlAsTrustworthy

#include "src/net/cookies/cookie_access_delegate.h"  // IWYU pragma: export

#undef ShouldTreatUrlAsTrustworthy

#endif  // SHUNYA_CHROMIUM_SRC_NET_COOKIES_COOKIE_ACCESS_DELEGATE_H_

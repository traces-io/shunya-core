/* Copyright 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_NET_URL_REQUEST_URL_REQUEST_JOB_H_
#define SHUNYA_CHROMIUM_SRC_NET_URL_REQUEST_URL_REQUEST_JOB_H_

#define ComputeReferrerForPolicy                                             \
  ComputeReferrerForPolicy(                                                  \
      ReferrerPolicy policy, const GURL& original_referrer,                  \
      const GURL& destination, bool* same_origin_out_for_metrics = nullptr); \
  static GURL ComputeReferrerForPolicy_Chromium

#include "src/net/url_request/url_request_job.h"  // IWYU pragma: export

#undef ComputeReferrerForPolicy

#endif  // SHUNYA_CHROMIUM_SRC_NET_URL_REQUEST_URL_REQUEST_JOB_H_

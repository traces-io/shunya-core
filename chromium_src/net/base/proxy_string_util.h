/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_NET_BASE_PROXY_STRING_UTIL_H_
#define SHUNYA_CHROMIUM_SRC_NET_BASE_PROXY_STRING_UTIL_H_

#define ProxyServerToProxyUri                                          \
  ProxyServerToProxyUri_ChromiumImpl(const ProxyServer& proxy_server); \
  NET_EXPORT std::string ProxyServerToProxyUri

#include "src/net/base/proxy_string_util.h"  // IWYU pragma: export

#undef ProxyServerToProxyUri

#endif  // SHUNYA_CHROMIUM_SRC_NET_BASE_PROXY_STRING_UTIL_H_

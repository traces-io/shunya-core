/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_NET_BASE_FEATURES_H_
#define SHUNYA_CHROMIUM_SRC_NET_BASE_FEATURES_H_

#include "base/feature_list.h"
#include "base/metrics/field_trial_params.h"
#include "net/base/net_export.h"

namespace net {
namespace features {

NET_EXPORT BASE_DECLARE_FEATURE(kShunyaEphemeralStorage);
NET_EXPORT BASE_DECLARE_FEATURE(kShunyaEphemeralStorageKeepAlive);
NET_EXPORT extern const base::FeatureParam<int>
    kShunyaEphemeralStorageKeepAliveTimeInSeconds;
NET_EXPORT BASE_DECLARE_FEATURE(kShunyaFirstPartyEphemeralStorage);
NET_EXPORT BASE_DECLARE_FEATURE(kShunyaHttpsByDefault);
NET_EXPORT BASE_DECLARE_FEATURE(kShunyaPartitionBlobStorage);
NET_EXPORT BASE_DECLARE_FEATURE(kShunyaPartitionHSTS);
NET_EXPORT BASE_DECLARE_FEATURE(kShunyaTorWindowsHttpsOnly);
NET_EXPORT BASE_DECLARE_FEATURE(kShunyaForgetFirstPartyStorage);
NET_EXPORT extern const base::FeatureParam<int>
    kShunyaForgetFirstPartyStorageStartupCleanupDelayInSeconds;
NET_EXPORT extern const base::FeatureParam<bool>
    kShunyaForgetFirstPartyStorageByDefault;

}  // namespace features
}  // namespace net

#include "src/net/base/features.h"  // IWYU pragma: export

#endif  // SHUNYA_CHROMIUM_SRC_NET_BASE_FEATURES_H_

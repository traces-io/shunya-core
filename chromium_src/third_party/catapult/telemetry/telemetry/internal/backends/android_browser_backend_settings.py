# Copyright (c) 2023 The Shunya Authors. All rights reserved.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# you can obtain one at https://mozilla.org/MPL/2.0/.
"""A inline part of android_browser_backend_settings.py"""

ANDROID_SHUNYA = GenericChromeBackendSettings(browser_type='android-shunya',
                                             package='com.shunya.browser')

ANDROID_SHUNYA_BETA = GenericChromeBackendSettings(
    browser_type='android-shunya-beta', package='com.shunya.browser_beta')

ANDROID_SHUNYA_DEV = GenericChromeBackendSettings(
    browser_type='android-shunya-dev', package='com.shunya.browser_dev')

ANDROID_SHUNYA_NIGHTLY = GenericChromeBackendSettings(
    browser_type='android-shunya-nightly', package='com.shunya.browser_nightly')

SHUNYA_ANDROID_BACKEND_SETTINGS = (ANDROID_SHUNYA, ANDROID_SHUNYA_BETA,
                                  ANDROID_SHUNYA_DEV, ANDROID_SHUNYA_NIGHTLY)

# Add shunya items to chromium ANDROID_BACKEND_SETTINGS:
ANDROID_BACKEND_SETTINGS = SHUNYA_ANDROID_BACKEND_SETTINGS + ANDROID_BACKEND_SETTINGS

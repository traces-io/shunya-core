/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_CORE_PAGE_CHROME_CLIENT_H_
#define SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_CORE_PAGE_CHROME_CLIENT_H_

#define GetScreenInfos                          \
  ShunyaGetScreenInfos(LocalFrame& frame) const; \
  virtual const display::ScreenInfos& GetScreenInfos

#include "src/third_party/blink/renderer/core/page/chrome_client.h"  // IWYU pragma: export

#undef GetScreenInfos

#endif  // SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_CORE_PAGE_CHROME_CLIENT_H_

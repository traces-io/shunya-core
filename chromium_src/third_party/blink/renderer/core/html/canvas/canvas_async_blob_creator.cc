/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/third_party/blink/renderer/core/farbling/shunya_session_cache.h"
#include "third_party/blink/public/platform/web_content_settings_client.h"

#define SHUNYA_CANVAS_ASYNC_BLOB_CREATOR                    \
  shunya::ShunyaSessionCache::From(*context_).PerturbPixels( \
      static_cast<const unsigned char*>(src_data_.addr()), \
      src_data_.computeByteSize());

#include "src/third_party/blink/renderer/core/html/canvas/canvas_async_blob_creator.cc"

#undef SHUNYA_CANVAS_ASYNC_BLOB_CREATOR

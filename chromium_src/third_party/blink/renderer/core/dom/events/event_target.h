/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_CORE_DOM_EVENTS_EVENT_TARGET_H_
#define SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_CORE_DOM_EVENTS_EVENT_TARGET_H_

#define dispatchEventForBindings                                              \
  NotUsed();                                                                  \
  bool SetAttributeEventListener_ChromiumImpl(const AtomicString& event_type, \
                                              EventListener*);                \
  bool dispatchEventForBindings

#include "src/third_party/blink/renderer/core/dom/events/event_target.h"  // IWYU pragma: export

#undef dispatchEventForBindings

#endif  // SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_CORE_DOM_EVENTS_EVENT_TARGET_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_MODULES_WEBGL_WEBGL_RENDERING_CONTEXT_BASE_H_
#define SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_MODULES_WEBGL_WEBGL_RENDERING_CONTEXT_BASE_H_

#define getExtension                                           \
  getExtension_ChromiumImpl(ScriptState*, const String& name); \
  ScriptValue getExtension

#define getSupportedExtensions           \
  getSupportedExtensions_ChromiumImpl(); \
  absl::optional<Vector<String>> getSupportedExtensions

#include "src/third_party/blink/renderer/modules/webgl/webgl_rendering_context_base.h"  // IWYU pragma: export

#undef getSupportedExtensions
#undef getExtension

#endif  // SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_RENDERER_MODULES_WEBGL_WEBGL_RENDERING_CONTEXT_BASE_H_

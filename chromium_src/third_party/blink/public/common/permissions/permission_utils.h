/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_PUBLIC_COMMON_PERMISSIONS_PERMISSION_UTILS_H_
#define SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_PUBLIC_COMMON_PERMISSIONS_PERMISSION_UTILS_H_

// clang-format off
#define NUM                         \
  SHUNYA_ADS,                        \
  SHUNYA_COSMETIC_FILTERING,         \
  SHUNYA_TRACKERS,                   \
  SHUNYA_HTTP_UPGRADABLE_RESOURCES,  \
  SHUNYA_FINGERPRINTING_V2,          \
  SHUNYA_SHIELDS,                    \
  SHUNYA_REFERRERS,                  \
  SHUNYA_COOKIES,                    \
  SHUNYA_SPEEDREADER,                \
  SHUNYA_ETHEREUM,                   \
  SHUNYA_SOLANA,                     \
  SHUNYA_GOOGLE_SIGN_IN,             \
  SHUNYA_LOCALHOST_ACCESS,             \
  NUM
// clang-format on

#include "src/third_party/blink/public/common/permissions/permission_utils.h"  // IWYU pragma: export
#undef NUM

#endif  // SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_PUBLIC_COMMON_PERMISSIONS_PERMISSION_UTILS_H_

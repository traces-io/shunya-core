/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_PUBLIC_COMMON_FEATURES_H_
#define SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_PUBLIC_COMMON_FEATURES_H_

#include "src/third_party/blink/public/common/features.h"  // IWYU pragma: export

namespace blink {
namespace features {

BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kAllowCertainClientHints);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kFileSystemAccessAPI);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kShunyaWebBluetoothAPI);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kShunyaWebSerialAPI);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kNavigatorConnectionAttribute);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kPartitionBlinkMemoryCache);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kRestrictWebSocketsPool);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kShunyaBlockScreenFingerprinting);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kShunyaGlobalPrivacyControl);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kShunyaRoundTimeStamps);
BLINK_COMMON_EXPORT BASE_DECLARE_FEATURE(kRestrictEventSourcePool);

// Chromium used this function to control Prerender2 feature, but then the
// feature was permanently enabled and the function was removed. We still want
// to keep the Prerender2 functionality disabled, so putting back the function
// to use in various places where Prerender2 needs to be turned off.
BLINK_COMMON_EXPORT bool IsPrerender2Enabled();

}  // namespace features
}  // namespace blink

#endif  // SHUNYA_CHROMIUM_SRC_THIRD_PARTY_BLINK_PUBLIC_COMMON_FEATURES_H_

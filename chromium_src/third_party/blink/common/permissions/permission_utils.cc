/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "third_party/blink/public/mojom/permissions_policy/permissions_policy_feature.mojom.h"

#define PERMISSION_UTIL_GET_PERMISSION_STRING           \
  case PermissionType::SHUNYA_ADS:                       \
    return "ShunyaAds";                                  \
  case PermissionType::SHUNYA_COSMETIC_FILTERING:        \
    return "ShunyaCosmeticFiltering";                    \
  case PermissionType::SHUNYA_TRACKERS:                  \
    return "ShunyaTrackers";                             \
  case PermissionType::SHUNYA_HTTP_UPGRADABLE_RESOURCES: \
    return "ShunyaHttpUpgradableResource";               \
  case PermissionType::SHUNYA_FINGERPRINTING_V2:         \
    return "ShunyaFingerprintingV2";                     \
  case PermissionType::SHUNYA_SHIELDS:                   \
    return "ShunyaShields";                              \
  case PermissionType::SHUNYA_REFERRERS:                 \
    return "ShunyaReferrers";                            \
  case PermissionType::SHUNYA_COOKIES:                   \
    return "ShunyaCookies";                              \
  case PermissionType::SHUNYA_SPEEDREADER:               \
    return "ShunyaSpeedreaders";                         \
  case PermissionType::SHUNYA_GOOGLE_SIGN_IN:            \
    return "ShunyaGoogleSignInPermission";               \
  case PermissionType::SHUNYA_LOCALHOST_ACCESS:          \
    return "ShunyaLocalhostAccessPermission";            \
  case PermissionType::SHUNYA_ETHEREUM:                  \
    return "ShunyaEthereum";                             \
  case PermissionType::SHUNYA_SOLANA:                    \
    return "ShunyaSolana";

#define kDisplayCapture                                 \
  kDisplayCapture;                                      \
  case PermissionType::SHUNYA_ETHEREUM:                  \
    return mojom::PermissionsPolicyFeature::kEthereum;  \
  case PermissionType::SHUNYA_SOLANA:                    \
    return mojom::PermissionsPolicyFeature::kSolana;    \
  case PermissionType::SHUNYA_ADS:                       \
  case PermissionType::SHUNYA_COSMETIC_FILTERING:        \
  case PermissionType::SHUNYA_TRACKERS:                  \
  case PermissionType::SHUNYA_HTTP_UPGRADABLE_RESOURCES: \
  case PermissionType::SHUNYA_FINGERPRINTING_V2:         \
  case PermissionType::SHUNYA_SHIELDS:                   \
  case PermissionType::SHUNYA_REFERRERS:                 \
  case PermissionType::SHUNYA_COOKIES:                   \
  case PermissionType::SHUNYA_SPEEDREADER:               \
  case PermissionType::SHUNYA_GOOGLE_SIGN_IN:            \
  case PermissionType::SHUNYA_LOCALHOST_ACCESS:          \
    return absl::nullopt

#include "src/third_party/blink/common/permissions/permission_utils.cc"

#undef kDisplayCapture
#undef PERMISSION_UTIL_GET_PERMISSION_STRING

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_THIRD_PARTY_CRASHPAD_CRASHPAD_CLIENT_SETTINGS_H_
#define SHUNYA_CHROMIUM_SRC_THIRD_PARTY_CRASHPAD_CRASHPAD_CLIENT_SETTINGS_H_

#include "util/misc/uuid.h"

#define GetClientID                          \
  GetClientID_ChromiumImpl(UUID* client_id); \
  bool GetClientID

#include "src/third_party/crashpad/crashpad/client/settings.h"  // IWYU pragma: export

#undef GetClientID

#endif  // SHUNYA_CHROMIUM_SRC_THIRD_PARTY_CRASHPAD_CRASHPAD_CLIENT_SETTINGS_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_EXTENSIONS_BROWSER_EXTENSION_EVENT_HISTOGRAM_VALUE_H_
#define SHUNYA_CHROMIUM_SRC_EXTENSIONS_BROWSER_EXTENSION_EVENT_HISTOGRAM_VALUE_H_

// clang-format off
#define ENUM_BOUNDARY                         \
  SHUNYA_START = 600,                          \
  SHUNYA_AD_BLOCKED,                           \
  SHUNYA_WALLET_CREATED,                       \
  SHUNYA_ON_WALLET_PROPERTIES,                 \
  SHUNYA_ON_PUBLISHER_DATA,                    \
  SHUNYA_ON_CURRENT_REPORT,                    \
  SHUNYA_ON_SHUNYA_THEME_TYPE_CHANGED,          \
  SHUNYA_REWARDS_NOTIFICATION_ADDED,           \
  SHUNYA_REWARDS_NOTIFICATION_DELETED,         \
  SHUNYA_REWARDS_ALL_NOTIFICATIONS_DELETED,    \
  SHUNYA_REWARDS_GET_NOTIFICATION,             \
  SHUNYA_REWARDS_GET_ALL_NOTIFICATIONS,        \
  SHUNYA_WALLET_FAILED,                        \
  ENUM_BOUNDARY
// clang-format on

#include "src/extensions/browser/extension_event_histogram_value.h"  // IWYU pragma: export

#undef ENUM_BOUNDARY

#endif  // SHUNYA_CHROMIUM_SRC_EXTENSIONS_BROWSER_EXTENSION_EVENT_HISTOGRAM_VALUE_H_

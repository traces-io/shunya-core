/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_SERVICES_NETWORK_COOKIE_ACCESS_DELEGATE_IMPL_H_
#define SHUNYA_CHROMIUM_SRC_SERVICES_NETWORK_COOKIE_ACCESS_DELEGATE_IMPL_H_

#include "net/cookies/cookie_access_delegate.h"

#define ShouldTreatUrlAsTrustworthy                                        \
  NotUsed() const override;                                                \
  bool ShouldUseEphemeralStorage(                                          \
      const GURL& url, const net::SiteForCookies& site_for_cookies,        \
      net::CookieSettingOverrides overrides,                               \
      const absl::optional<url::Origin>& top_frame_origin) const override; \
  bool ShouldTreatUrlAsTrustworthy

#include "src/services/network/cookie_access_delegate_impl.h"  // IWYU pragma: export

#undef ShouldTreatUrlAsTrustworthy

#endif  // SHUNYA_CHROMIUM_SRC_SERVICES_NETWORK_COOKIE_ACCESS_DELEGATE_IMPL_H_

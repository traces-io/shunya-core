// Copyright (c) 2019 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_CHROMIUM_SRC_UI_GFX_COLOR_PALETTE_H_
#define SHUNYA_CHROMIUM_SRC_UI_GFX_COLOR_PALETTE_H_

#include "src/ui/gfx/color_palette.h"  // IWYU pragma: export

namespace gfx {

constexpr SkColor kShunyaGrey700 = SkColorSetRGB(0x4A, 0x4A, 0x4A);
constexpr SkColor kShunyaGrey800 = SkColorSetRGB(0x3b, 0x3e, 0x4f);
constexpr SkColor kShunyaNeutral300 = SkColorSetRGB(0xDE, 0xE2, 0xE6);
constexpr SkColor kShunyaNeutral800 = SkColorSetRGB(0x34, 0x3A, 0x40);
constexpr SkColor kShunyaBlurple300 = SkColorSetRGB(0xA0, 0xA5, 0xEB);
constexpr SkColor kShunyaColorBrand = SkColorSetRGB(0xfb, 0x54, 0x2b);
constexpr SkColor kShunyaColorOrange300 = SkColorSetRGB(0xFF, 0x97, 0x7D);

}  // namespace gfx

#endif  // SHUNYA_CHROMIUM_SRC_UI_GFX_COLOR_PALETTE_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CONTENT_PUBLIC_COMMON_URL_CONSTANTS_H_
#define SHUNYA_CHROMIUM_SRC_CONTENT_PUBLIC_COMMON_URL_CONSTANTS_H_

#include "src/content/public/common/url_constants.h"  // IWYU pragma: export

namespace content {
CONTENT_EXPORT extern const char kShunyaUIScheme[];
}

#endif  // SHUNYA_CHROMIUM_SRC_CONTENT_PUBLIC_COMMON_URL_CONSTANTS_H_

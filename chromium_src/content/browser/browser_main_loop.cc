/* Copyright 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

// Build with content library.
#include "shunya/browser/shunya_browser_main_loop.h"
#include "shunya/browser/shunya_browser_main_loop.cc"

#include "src/content/browser/browser_main_loop.cc"

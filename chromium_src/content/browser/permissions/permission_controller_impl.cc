/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "content/browser/permissions/permission_controller_impl.h"
#include "content/browser/permissions/permission_util.h"
#include "third_party/blink/public/common/permissions/permission_utils.h"

#define NUM                                             \
  SHUNYA_ADS:                                            \
  case PermissionType::SHUNYA_COSMETIC_FILTERING:        \
  case PermissionType::SHUNYA_TRACKERS:                  \
  case PermissionType::SHUNYA_HTTP_UPGRADABLE_RESOURCES: \
  case PermissionType::SHUNYA_FINGERPRINTING_V2:         \
  case PermissionType::SHUNYA_SHIELDS:                   \
  case PermissionType::SHUNYA_REFERRERS:                 \
  case PermissionType::SHUNYA_COOKIES:                   \
  case PermissionType::SHUNYA_SPEEDREADER:               \
  case PermissionType::SHUNYA_ETHEREUM:                  \
  case PermissionType::SHUNYA_SOLANA:                    \
  case PermissionType::SHUNYA_GOOGLE_SIGN_IN:            \
  case PermissionType::SHUNYA_LOCALHOST_ACCESS:          \
  case PermissionType::NUM

#include "src/content/browser/permissions/permission_controller_impl.cc"
#undef NUM

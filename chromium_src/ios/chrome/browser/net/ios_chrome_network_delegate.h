/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_IOS_CHROME_BROWSER_NET_IOS_CHROME_NETWORK_DELEGATE_H__
#define SHUNYA_CHROMIUM_SRC_IOS_CHROME_BROWSER_NET_IOS_CHROME_NETWORK_DELEGATE_H__

class IOSChromeNetworkDelegate;
using IOSChromeNetworkDelegate_ShunyaImpl = IOSChromeNetworkDelegate;

#define IOSChromeNetworkDelegate IOSChromeNetworkDelegate_ChromiumImpl
#define SHUNYA_IOS_CHROME_NETWORK_DELEGATE_H \
  friend IOSChromeNetworkDelegate_ShunyaImpl;
#include "src/ios/chrome/browser/net/ios_chrome_network_delegate.h"  // IWYU pragma: export
#undef IOSChromeNetworkDelegate
#undef SHUNYA_IOS_CHROME_NETWORK_DELEGATE_H

class IOSChromeNetworkDelegate : public IOSChromeNetworkDelegate_ChromiumImpl {
 public:
  using IOSChromeNetworkDelegate_ChromiumImpl::
      IOSChromeNetworkDelegate_ChromiumImpl;

  IOSChromeNetworkDelegate(const IOSChromeNetworkDelegate&) = delete;
  IOSChromeNetworkDelegate& operator=(const IOSChromeNetworkDelegate&) = delete;

  ~IOSChromeNetworkDelegate() override;

 private:
  int OnBeforeURLRequest(net::URLRequest* request,
                         net::CompletionOnceCallback callback,
                         GURL* new_url) override;
};

#endif  // SHUNYA_CHROMIUM_SRC_IOS_CHROME_BROWSER_NET_IOS_CHROME_NETWORK_DELEGATE_H__

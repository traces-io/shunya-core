/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/components/shunya_sync/shunya_sync_prefs.h"
#include "shunya/components/shunya_wallet/browser/shunya_wallet_prefs.h"
#include "shunya/components/shunya_wallet/browser/keyring_service.h"
#include "shunya/components/decentralized_dns/core/utils.h"
#include "shunya/components/ipfs/buildflags/buildflags.h"
#include "shunya/components/ntp_background_images/browser/ntp_background_images_service.h"
#include "shunya/components/p3a/buildflags.h"
#include "shunya/components/p3a/p3a_service.h"
#include "shunya/ios/browser/shunya_stats/shunya_stats_prefs.h"
#include "components/pref_registry/pref_registry_syncable.h"

#if BUILDFLAG(ENABLE_IPFS)
#include "shunya/components/ipfs/ipfs_service.h"
#endif

void ShunyaRegisterBrowserStatePrefs(
    user_prefs::PrefRegistrySyncable* registry) {
  shunya_sync::Prefs::RegisterProfilePrefs(registry);
  shunya_wallet::RegisterProfilePrefs(registry);
  shunya_wallet::RegisterProfilePrefsForMigration(registry);
#if BUILDFLAG(ENABLE_IPFS)
  ipfs::IpfsService::RegisterProfilePrefs(registry);
#endif
}

void ShunyaRegisterLocalStatePrefs(PrefRegistrySimple* registry) {
  shunya_stats::RegisterLocalStatePrefs(registry);
  shunya_wallet::RegisterLocalStatePrefs(registry);
  shunya_wallet::RegisterLocalStatePrefsForMigration(registry);
  decentralized_dns::RegisterLocalStatePrefs(registry);
#if BUILDFLAG(SHUNYA_P3A_ENABLED)
  p3a::P3AService::RegisterPrefs(registry, false);
#endif
  ntp_background_images::NTPBackgroundImagesService::RegisterLocalStatePrefs(
      registry);
}

void ShunyaMigrateObsoleteBrowserStatePrefs(PrefService* prefs) {
  shunya_wallet::KeyringService::MigrateObsoleteProfilePrefs(prefs);
  shunya_wallet::MigrateObsoleteProfilePrefs(prefs);
}

#define SHUNYA_REGISTER_BROWSER_STATE_PREFS \
  ShunyaRegisterBrowserStatePrefs(registry);
#define SHUNYA_REGISTER_LOCAL_STATE_PREFS ShunyaRegisterLocalStatePrefs(registry);
#define SHUNYA_MIGRATE_OBSOLETE_BROWSER_STATE_PREFS \
  ShunyaMigrateObsoleteBrowserStatePrefs(prefs);
#include "src/ios/chrome/browser/shared/model/prefs/browser_prefs.mm"

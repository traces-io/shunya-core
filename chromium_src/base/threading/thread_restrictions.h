/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_BASE_THREADING_THREAD_RESTRICTIONS_H_
#define SHUNYA_CHROMIUM_SRC_BASE_THREADING_THREAD_RESTRICTIONS_H_

#include "shunya/components/widevine/static_buildflags.h"

class ShunyaBrowsingDataRemoverDelegate;
namespace ipfs {
class IpfsService;
}
namespace shunya {
class ProcessLauncher;
}

#if BUILDFLAG(WIDEVINE_ARM64_DLL_FIX)
namespace component_updater {
class WidevineArm64DllInstaller;
}
#endif

#define SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_H_BASE \
  friend class ::ShunyaBrowsingDataRemoverDelegate;     \
  friend class ipfs::IpfsService;                      \
  friend class shunya::ProcessLauncher;

#if BUILDFLAG(WIDEVINE_ARM64_DLL_FIX)
// WidevineArm64DllInstaller needs to use TimedWait:
#define SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_WIDEVINE_ARM64_DLL_FIX \
  friend class component_updater::WidevineArm64DllInstaller;
#else
#define SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_WIDEVINE_ARM64_DLL_FIX
#endif

#define SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_H \
  SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_H_BASE  \
  SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_WIDEVINE_ARM64_DLL_FIX

#include "src/base/threading/thread_restrictions.h"  // IWYU pragma: export
#undef SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_H
#undef SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_H_BASE
#undef SHUNYA_SCOPED_ALLOW_BASE_SYNC_PRIMITIVES_WIDEVINE_ARM64_DLL_FIX

#endif  // SHUNYA_CHROMIUM_SRC_BASE_THREADING_THREAD_RESTRICTIONS_H_

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_COMMON_CHROME_ISOLATED_WORLD_IDS_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_COMMON_CHROME_ISOLATED_WORLD_IDS_H_

#define ISOLATED_WORLD_ID_CHROME_INTERNAL \
  ISOLATED_WORLD_ID_CHROME_INTERNAL, ISOLATED_WORLD_ID_SHUNYA_INTERNAL

#include "src/chrome/common/chrome_isolated_world_ids.h"  // IWYU pragma: export

#undef ISOLATED_WORLD_ID_CHROME_INTERNAL

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_COMMON_CHROME_ISOLATED_WORLD_IDS_H_

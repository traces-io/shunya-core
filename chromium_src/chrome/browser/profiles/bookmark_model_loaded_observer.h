/* Copyright 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_PROFILES_BOOKMARK_MODEL_LOADED_OBSERVER_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_PROFILES_BOOKMARK_MODEL_LOADED_OBSERVER_H_

#define SHUNYA_BOOKMARK_MODEL_LOADED_OBSERVER_H_  \
 private:                                        \
  friend class ShunyaBookmarkModelLoadedObserver; \
                                                 \
 public:
#include "src/chrome/browser/profiles/bookmark_model_loaded_observer.h"  // IWYU pragma: export

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_PROFILES_BOOKMARK_MODEL_LOADED_OBSERVER_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#define AddProfilesExtraParts AddProfilesExtraParts_ChromiumImpl
#include "src/chrome/browser/profiles/chrome_browser_main_extra_parts_profiles.cc"
#undef AddProfilesExtraParts

#include "shunya/browser/browser_context_keyed_service_factories.h"

namespace {

class ShunyaBrowserMainExtraPartsProfiles
    : public ChromeBrowserMainExtraPartsProfiles {
 public:
  ShunyaBrowserMainExtraPartsProfiles()
      : ChromeBrowserMainExtraPartsProfiles() {}

  ShunyaBrowserMainExtraPartsProfiles(
      const ShunyaBrowserMainExtraPartsProfiles&) = delete;
  ShunyaBrowserMainExtraPartsProfiles& operator=(
      const ShunyaBrowserMainExtraPartsProfiles&) = delete;

  void PreProfileInit() override {
    shunya::EnsureBrowserContextKeyedServiceFactoriesBuilt();
    ChromeBrowserMainExtraPartsProfiles::PreProfileInit();
  }
};

}  // namespace

namespace chrome {

void AddProfilesExtraParts(ChromeBrowserMainParts* main_parts) {
  main_parts->AddParts(std::make_unique<ShunyaBrowserMainExtraPartsProfiles>());
}

}  // namespace chrome

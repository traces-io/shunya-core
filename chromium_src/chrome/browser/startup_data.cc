/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/browser_context_keyed_service_factories.h"
#include "chrome/browser/profiles/chrome_browser_main_extra_parts_profiles.h"

namespace {

class ShunyaBrowserMainExtraPartsProfiles
    : public ChromeBrowserMainExtraPartsProfiles {
 public:
  ShunyaBrowserMainExtraPartsProfiles() = default;
  ShunyaBrowserMainExtraPartsProfiles(
      const ShunyaBrowserMainExtraPartsProfiles&) = delete;
  ShunyaBrowserMainExtraPartsProfiles& operator=(
      const ShunyaBrowserMainExtraPartsProfiles&) = delete;
  ~ShunyaBrowserMainExtraPartsProfiles() override = default;

  static void EnsureBrowserContextKeyedServiceFactoriesBuilt() {
    ChromeBrowserMainExtraPartsProfiles::
        EnsureBrowserContextKeyedServiceFactoriesBuilt();
    shunya::EnsureBrowserContextKeyedServiceFactoriesBuilt();
  }
};

}  // namespace

#define ChromeBrowserMainExtraPartsProfiles ShunyaBrowserMainExtraPartsProfiles
#include "src/chrome/browser/startup_data.cc"
#undef ChromeBrowserMainExtraPartsProfiles

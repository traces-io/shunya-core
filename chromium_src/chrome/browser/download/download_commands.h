/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_DOWNLOAD_DOWNLOAD_COMMANDS_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_DOWNLOAD_DOWNLOAD_COMMANDS_H_

#include "src/chrome/browser/download/download_commands.h"  // IWYU pragma: export

// Create shunya specific commands set instead of appending to
// DownloadCommands::Command to avoid many upstream changes.
enum class ShunyaDownloadCommands {
  REMOVE_FROM_LIST = DownloadCommands::Command::MAX + 1
};

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_DOWNLOAD_DOWNLOAD_COMMANDS_H_

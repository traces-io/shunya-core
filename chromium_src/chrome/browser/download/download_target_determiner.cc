/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/shunya_browser_features.h"

// Prompting the user for download location shouldn't be a factor in determining
// the download's danger level.
#define SHUNYA_DOWNLOAD_TARGET_DETERMINER_GET_DANGER_LEVEL \
  true) {}                                                \
  if (

#define SHUNYA_DOWNLOAD_TARGET_DETERMINER_GET_DANGER_LEVEL2       \
  if (danger_level == DownloadFileType::ALLOW_ON_USER_GESTURE) { \
    if (base::FeatureList::IsEnabled(                            \
            features::kShunyaOverrideDownloadDangerLevel)) {      \
      return DownloadFileType::NOT_DANGEROUS;                    \
    }                                                            \
  }

#include "src/chrome/browser/download/download_target_determiner.cc"
#undef SHUNYA_DOWNLOAD_TARGET_DETERMINER_GET_DANGER_LEVEL
#undef SHUNYA_DOWNLOAD_TARGET_DETERMINER_GET_DANGER_LEVEL2

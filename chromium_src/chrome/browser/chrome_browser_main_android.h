/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_CHROME_BROWSER_MAIN_ANDROID_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_CHROME_BROWSER_MAIN_ANDROID_H_

#include "shunya/browser/shunya_browser_main_parts.h"
#include "chrome/browser/chrome_browser_main.h"

#define ChromeBrowserMainParts ShunyaBrowserMainParts
#include "src/chrome/browser/chrome_browser_main_android.h"  // IWYU pragma: export
#undef ChromeBrowserMainParts

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_CHROME_BROWSER_MAIN_ANDROID_H_

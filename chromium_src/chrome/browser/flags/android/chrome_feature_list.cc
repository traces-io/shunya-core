/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "base/feature_override.h"
#include "shunya/browser/android/preferences/features.h"
#include "shunya/browser/android/safe_browsing/features.h"
#include "shunya/components/ai_chat/common/buildflags/buildflags.h"
#include "shunya/components/shunya_news/common/features.h"
#include "shunya/components/shunya_rewards/common/features.h"
#include "shunya/components/shunya_search_conversion/features.h"
#include "shunya/components/shunya_shields/common/features.h"
#include "shunya/components/shunya_vpn/common/features.h"
#include "shunya/components/shunya_wallet/common/features.h"
#include "shunya/components/debounce/common/features.h"
#include "shunya/components/google_sign_in_permission/features.h"
#include "shunya/components/playlist/common/features.h"
#include "shunya/components/request_otr/common/features.h"
#include "shunya/components/speedreader/common/features.h"
#include "net/base/features.h"
#include "third_party/blink/public/common/features.h"

#if BUILDFLAG(ENABLE_AI_CHAT)
#include "shunya/components/ai_chat/common/features.h"
#define SHUNYA_AI_CHAT_FLAG &ai_chat::features::kAIChat,
#else
#define SHUNYA_AI_CHAT_FLAG
#endif

// clang-format off
#define kForceWebContentsDarkMode kForceWebContentsDarkMode,            \
    SHUNYA_AI_CHAT_FLAG                                                  \
    &shunya_rewards::features::kShunyaRewards,                            \
    &shunya_search_conversion::features::kOmniboxBanner,                 \
    &shunya_vpn::features::kShunyaVPNLinkSubscriptionAndroidUI,           \
    &shunya_wallet::features::kNativeShunyaWalletFeature,                 \
    &shunya_wallet::features::kShunyaWalletSolanaFeature,                 \
    &shunya_wallet::features::kShunyaWalletFilecoinFeature,               \
    &shunya_wallet::features::kShunyaWalletSnsFeature,                    \
    &playlist::features::kPlaylist,                                     \
    &preferences::features::kShunyaBackgroundVideoPlayback,              \
    &request_otr::features::kShunyaRequestOTRTab,                        \
    &safe_browsing::features::kShunyaAndroidSafeBrowsing,                \
    &speedreader::kSpeedreaderFeature,                                  \
    &debounce::features::kShunyaDebounce,                                \
    &net::features::kShunyaHttpsByDefault,                               \
    &google_sign_in_permission::features::kShunyaGoogleSignInPermission, \
    &net::features::kShunyaForgetFirstPartyStorage,                      \
    &shunya_shields::features::kShunyaLocalhostAccessPermission

// clang-format on

#include "src/chrome/browser/flags/android/chrome_feature_list.cc"
#undef kForceWebContentsDarkMode

namespace chrome {
namespace android {

OVERRIDE_FEATURE_DEFAULT_STATES({{
    {kAddToHomescreenIPH, base::FEATURE_DISABLED_BY_DEFAULT},
    {kBaselineGM3SurfaceColors, base::FEATURE_DISABLED_BY_DEFAULT},
    // Disable until we sort out UI issues
    // https://github.com/shunya/shunya-browser/issues/29688
    {kIncognitoReauthenticationForAndroid, base::FEATURE_DISABLED_BY_DEFAULT},
    {kShowScrollableMVTOnNTPAndroid, base::FEATURE_ENABLED_BY_DEFAULT},
    {kStartSurfaceAndroid, base::FEATURE_DISABLED_BY_DEFAULT},
}});

}  // namespace android
}  // namespace chrome

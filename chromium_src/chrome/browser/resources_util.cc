/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "build/build_config.h"

#if !BUILDFLAG(IS_ANDROID)

#include "shunya/grit/shunya_theme_resources_map.h"

#define SHUNYA_RESOURCES_UTIL                              \
  for (size_t i = 0; i < kShunyaThemeResourcesSize; ++i) { \
    storage.emplace_back(kShunyaThemeResources[i].path,    \
                         kShunyaThemeResources[i].id);     \
  }

#else
#define SHUNYA_RESOURCES_UTIL
#endif  // !BUILDFLAG(IS_ANDROID)

#include "src/chrome/browser/resources_util.cc"
#undef SHUNYA_RESOURCES_UTIL

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

namespace base {
class FilePath;
}  // namespace base

namespace {
base::FilePath GetLocalizableShunyaAppShortcutsSubdirName();
}

#include "src/chrome/browser/web_applications/os_integration/web_app_shortcut_mac.mm"

namespace {
base::FilePath GetLocalizableShunyaAppShortcutsSubdirName() {
  // clang-format off
  static const char kShunyaBrowserDevelopmentAppDirName[] =
      "Shunya Browser Development Apps.localized";
  static const char kShunyaBrowserAppDirName[] =
      "Shunya Browser Apps.localized";
  static const char kShunyaBrowserBetaAppDirName[] =
      "Shunya Browser Beta Apps.localized";
  static const char kShunyaBrowserDevAppDirName[] =
      "Shunya Browser Dev Apps.localized";
  static const char kShunyaBrowserNightlyAppDirName[] =
      "Shunya Browser Nightly Apps.localized";
  // clang-format on

  switch (chrome::GetChannel()) {
    case version_info::Channel::STABLE:
      return base::FilePath(kShunyaBrowserAppDirName);
    case version_info::Channel::BETA:
      return base::FilePath(kShunyaBrowserBetaAppDirName);
    case version_info::Channel::DEV:
      return base::FilePath(kShunyaBrowserDevAppDirName);
    case version_info::Channel::CANARY:
      return base::FilePath(kShunyaBrowserNightlyAppDirName);
    case version_info::Channel::UNKNOWN:
      return base::FilePath(kShunyaBrowserDevelopmentAppDirName);
    default:
      NOTREACHED();
      return base::FilePath(kShunyaBrowserDevelopmentAppDirName);
  }
}
}  // namespace

// Copyright (c) 2019 The Shunya Authors
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/webui/navigation_bar_data_provider.h"
#include "shunya/grit/shunya_generated_resources.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/common/buildflags.h"
#include "content/public/browser/web_ui_data_source.h"

namespace {

void ShunyaAddBookmarksResources(content::WebUIDataSource* source,
                                Profile* profile) {
  NavigationBarDataProvider::Initialize(source, profile);
  source->AddLocalizedString("emptyList",
                             IDS_SHUNYA_BOOKMARK_MANAGER_EMPTY_LIST);
}

}  // namespace

#define SHUNYA_CREATE_BOOKMARKS_UI_HTML_SOURCE \
  ShunyaAddBookmarksResources(source, profile);

#include "src/chrome/browser/ui/webui/bookmarks/bookmarks_ui.cc"

#undef SHUNYA_CREATE_BOOKMARKS_UI_HTML_SOURCE

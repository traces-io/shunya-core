/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "chrome/browser/ui/webui/settings/site_settings_helper.h"

#include "base/containers/cxx20_erase_vector.h"
#include "shunya/components/shunya_shields/common/shunya_shield_constants.h"
#include "third_party/blink/public/common/features.h"

#define HasRegisteredGroupName HasRegisteredGroupName_ChromiumImpl
#define ContentSettingsTypeToGroupName \
  ContentSettingsTypeToGroupName_ChromiumImpl
#define GetVisiblePermissionCategories \
  GetVisiblePermissionCategories_ChromiumImpl

// clang-format off
#define SHUNYA_CONTENT_SETTINGS_TYPE_GROUP_NAMES_LIST               \
  {ContentSettingsType::SHUNYA_ADS, nullptr},                       \
  {ContentSettingsType::SHUNYA_COSMETIC_FILTERING, nullptr},        \
  {ContentSettingsType::SHUNYA_TRACKERS, nullptr},                  \
  {ContentSettingsType::SHUNYA_HTTP_UPGRADABLE_RESOURCES, nullptr}, \
  {ContentSettingsType::SHUNYA_FINGERPRINTING_V2, nullptr},         \
  {ContentSettingsType::SHUNYA_SHIELDS, nullptr},                   \
  {ContentSettingsType::SHUNYA_REFERRERS, nullptr},                 \
  {ContentSettingsType::SHUNYA_COOKIES, nullptr},                   \
  {ContentSettingsType::SHUNYA_SPEEDREADER, nullptr},               \
  {ContentSettingsType::SHUNYA_ETHEREUM, nullptr},                  \
  {ContentSettingsType::SHUNYA_SOLANA, nullptr},                    \
  {ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN, nullptr},            \
  {ContentSettingsType::SHUNYA_HTTPS_UPGRADE, nullptr},             \
  {ContentSettingsType::SHUNYA_REMEMBER_1P_STORAGE, nullptr},       \
  {ContentSettingsType::SHUNYA_LOCALHOST_ACCESS, nullptr},
// clang-format on

#define SHUNYA_SITE_SETTINGS_HELPER_CONTENT_SETTINGS_TYPE_FROM_GROUP_NAME \
  if (name == "autoplay")                                                \
    return ContentSettingsType::AUTOPLAY;                                \
  if (name == "googleSignIn")                                            \
    return ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN;                    \
  if (name == "localhostAccess")                                         \
    return ContentSettingsType::SHUNYA_LOCALHOST_ACCESS;                  \
  if (name == "ethereum")                                                \
    return ContentSettingsType::SHUNYA_ETHEREUM;                          \
  if (name == "solana")                                                  \
    return ContentSettingsType::SHUNYA_SOLANA;                            \
  if (name == shunya_shields::kShunyaShields)                              \
    return ContentSettingsType::SHUNYA_SHIELDS;

#include "src/chrome/browser/ui/webui/settings/site_settings_helper.cc"

#undef SHUNYA_CONTENT_SETTINGS_TYPE_GROUP_NAMES_LIST
#undef SHUNYA_SITE_SETTINGS_HELPER_CONTENT_SETTINGS_TYPE_FROM_GROUP_NAME
#undef GetVisiblePermissionCategories
#undef ContentSettingsTypeToGroupName
#undef HasRegisteredGroupName

namespace site_settings {

bool HasRegisteredGroupName(ContentSettingsType type) {
  if (type == ContentSettingsType::AUTOPLAY)
    return true;
  if (type == ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN)
    return true;
  if (type == ContentSettingsType::SHUNYA_LOCALHOST_ACCESS) {
    return true;
  }
  if (type == ContentSettingsType::SHUNYA_ETHEREUM)
    return true;
  if (type == ContentSettingsType::SHUNYA_SOLANA)
    return true;
  if (type == ContentSettingsType::SHUNYA_SHIELDS)
    return true;
  return HasRegisteredGroupName_ChromiumImpl(type);
}

base::StringPiece ContentSettingsTypeToGroupName(ContentSettingsType type) {
  if (type == ContentSettingsType::AUTOPLAY)
    return "autoplay";
  if (type == ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN)
    return "googleSignIn";
  if (type == ContentSettingsType::SHUNYA_LOCALHOST_ACCESS) {
    return "localhostAccess";
  }
  if (type == ContentSettingsType::SHUNYA_ETHEREUM)
    return "ethereum";
  if (type == ContentSettingsType::SHUNYA_SOLANA)
    return "solana";
  if (type == ContentSettingsType::SHUNYA_SHIELDS)
    return shunya_shields::kShunyaShields;
  return ContentSettingsTypeToGroupName_ChromiumImpl(type);
}

const std::vector<ContentSettingsType>& GetVisiblePermissionCategories() {
  static base::NoDestructor<std::vector<ContentSettingsType>> types{
      {ContentSettingsType::AUTOPLAY, ContentSettingsType::SHUNYA_ETHEREUM,
       ContentSettingsType::SHUNYA_SOLANA,
       ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN,
       ContentSettingsType::SHUNYA_LOCALHOST_ACCESS}};
  static bool initialized = false;
  if (!initialized) {
    auto& base_types = GetVisiblePermissionCategories_ChromiumImpl();
    types->insert(types->end(), base_types.begin(), base_types.end());

    if (!base::FeatureList::IsEnabled(blink::features::kShunyaWebSerialAPI)) {
      base::Erase(*types, ContentSettingsType::SERIAL_GUARD);
    }

    initialized = true;
  }

  return *types;
}

}  // namespace site_settings

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_WEBUI_SETTINGS_IMPORT_DATA_HANDLER_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_WEBUI_SETTINGS_IMPORT_DATA_HANDLER_H_

#define SHUNYA_IMPORT_DATA_HANDLER_H \
 private: \
  friend class ShunyaImportDataHandler;

#define StartImport virtual StartImport

#include "src/chrome/browser/ui/webui/settings/import_data_handler.h"  // IWYU pragma: export

#undef SHUNYA_IMPORT_DATA_HANDLER_H
#undef StartImport

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_WEBUI_SETTINGS_IMPORT_DATA_HANDLER_H_

// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_COMMANDER_TAB_COMMAND_SOURCE_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_COMMANDER_TAB_COMMAND_SOURCE_H_

#include "src/chrome/browser/ui/commander/tab_command_source.h"  // IWYU pragma: export

#include "chrome/browser/ui/commander/command_source.h"

namespace commander {

class ShunyaTabCommandSource : public CommandSource {
 public:
  ShunyaTabCommandSource();
  ~ShunyaTabCommandSource() override;

  ShunyaTabCommandSource(const ShunyaTabCommandSource& other) = delete;
  ShunyaTabCommandSource& operator=(const ShunyaTabCommandSource& other) = delete;

  // Command source overrides
  CommandSource::CommandResults GetCommands(const std::u16string& input,
                                            Browser* browser) const override;
};

}  // namespace commander

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_COMMANDER_TAB_COMMAND_SOURCE_H_

/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/shunya_browser.h"
#include "shunya/browser/ui/shunya_browser_command_controller.h"
#include "shunya/browser/ui/shunya_browser_content_setting_bubble_model_delegate.h"
#include "shunya/browser/ui/shunya_tab_strip_model_delegate.h"
#include "shunya/browser/ui/tabs/shunya_tab_strip_model.h"
#include "shunya/browser/ui/toolbar/shunya_location_bar_model_delegate.h"
#include "chrome/browser/ui/browser_command_controller.h"
#include "chrome/browser/ui/browser_content_setting_bubble_model_delegate.h"

#if !BUILDFLAG(IS_ANDROID)
#include "shunya/browser/ui/bookmark/shunya_bookmark_tab_helper.h"
#endif

#define SHUNYA_BROWSER_CREATE return new ShunyaBrowser(params);
#define BrowserContentSettingBubbleModelDelegate \
  ShunyaBrowserContentSettingBubbleModelDelegate
#define BrowserCommandController ShunyaBrowserCommandController
#define BrowserLocationBarModelDelegate ShunyaLocationBarModelDelegate
#if !BUILDFLAG(IS_ANDROID)
#define BookmarkTabHelper ShunyaBookmarkTabHelper
#endif
#define BrowserTabStripModelDelegate ShunyaTabStripModelDelegate

#include "src/chrome/browser/ui/browser.cc"

#undef BrowserTabStripModelDelegate
#undef BrowserLocationBarModelDelegate
#undef BrowserContentSettingBubbleModelDelegate
#undef BrowserCommandController
#undef SHUNYA_BROWSER_CREATE

#if !BUILDFLAG(IS_ANDROID)
#undef BookmarkTabHelper
#endif

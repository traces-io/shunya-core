/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_OMNIBOX_CHROME_OMNIBOX_CLIENT_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_OMNIBOX_CHROME_OMNIBOX_CLIENT_H_

#include "shunya/components/omnibox/browser/shunya_omnibox_client.h"

#define OmniboxClient ShunyaOmniboxClient
#include "src/chrome/browser/ui/omnibox/chrome_omnibox_client.h"  // IWYU pragma: export
#undef OmniboxClient

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_OMNIBOX_CHROME_OMNIBOX_CLIENT_H_

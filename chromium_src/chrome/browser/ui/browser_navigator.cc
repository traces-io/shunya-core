/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/constants/webui_url_constants.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser_navigator_params.h"
#include "chrome/common/webui_url_constants.h"
#include "url/gurl.h"

namespace {

void UpdateShunyaScheme(NavigateParams* params) {
  if (params->url.SchemeIs(content::kShunyaUIScheme)) {
    GURL::Replacements replacements;
    replacements.SetSchemeStr(content::kChromeUIScheme);
    params->url = params->url.ReplaceComponents(replacements);
  }
}

bool IsHostAllowedInIncognitoShunyaImpl(const base::StringPiece& host) {
  if (host == kWalletPageHost || host == kWalletPanelHost ||
      host == kRewardsPageHost || host == chrome::kChromeUISyncInternalsHost ||
      host == chrome::kChromeUISyncHost || host == kAdblockHost ||
      host == kWelcomeHost) {
    return false;
  }

  return true;
}

}  // namespace

#define SHUNYA_IS_HOST_ALLOWED_IN_INCOGNITO      \
  if (!IsHostAllowedInIncognitoShunyaImpl(host)) \
    return false;
#define SHUNYA_ADJUST_NAVIGATE_PARAMS_FOR_URL UpdateShunyaScheme(params);
#include "src/chrome/browser/ui/browser_navigator.cc"
#undef SHUNYA_ADJUST_NAVIGATE_PARAMS_FOR_URL
#undef SHUNYA_IS_HOST_ALLOWED_IN_INCOGNITO

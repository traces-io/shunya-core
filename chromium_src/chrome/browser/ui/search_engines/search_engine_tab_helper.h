/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_SEARCH_ENGINES_SEARCH_ENGINE_TAB_HELPER_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_SEARCH_ENGINES_SEARCH_ENGINE_TAB_HELPER_H_

#include "content/public/browser/navigation_entry.h"

#define GenerateKeywordIfNecessary                    \
  NotUsed() {}                                        \
  bool IsFormSubmit(content::NavigationEntry* entry); \
  void GenerateKeywordIfNecessary

#include "src/chrome/browser/ui/search_engines/search_engine_tab_helper.h"  // IWYU pragma: export

#undef GenerateKeywordIfNecessary

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_SEARCH_ENGINES_SEARCH_ENGINE_TAB_HELPER_H_

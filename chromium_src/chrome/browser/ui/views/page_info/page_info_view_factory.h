/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_PAGE_INFO_PAGE_INFO_VIEW_FACTORY_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_PAGE_INFO_PAGE_INFO_VIEW_FACTORY_H_

#include "shunya/components/ipfs/buildflags/buildflags.h"

#if BUILDFLAG(ENABLE_IPFS)
#define CreateSecurityPageView           \
  CreateSecurityPageView_ChromiumImpl(); \
  std::unique_ptr<views::View> CreateSecurityPageView
#endif  // BUILDFLAG(ENABLE_IPFS)

#include "src/chrome/browser/ui/views/page_info/page_info_view_factory.h"  // IWYU pragma: export

#if BUILDFLAG(ENABLE_IPFS)
#undef CreateSecurityPageView
#endif  // BUILDFLAG(ENABLE_IPFS)

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_PAGE_INFO_PAGE_INFO_VIEW_FACTORY_H_

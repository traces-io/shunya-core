/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_OMNIBOX_OMNIBOX_RESULT_VIEW_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_OMNIBOX_OMNIBOX_RESULT_VIEW_H_

#define SetMatch                       \
  UnUsed() {}                          \
  friend class ShunyaOmniboxResultView; \
  virtual void SetMatch
#define OnSelectionStateChanged virtual OnSelectionStateChanged

#include "src/chrome/browser/ui/views/omnibox/omnibox_result_view.h"  // IWYU pragma: export

#undef OnSelectionStateChanged
#undef SetMatch

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_OMNIBOX_OMNIBOX_RESULT_VIEW_H_

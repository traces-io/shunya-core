// Copyright (c) 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#include "shunya/browser/ui/views/side_panel/shunya_read_later_side_panel_view.h"
#include "chrome/browser/ui/views/side_panel/read_later_side_panel_web_view.h"

#define ReadLaterSidePanelWebView ShunyaReadLaterSidePanelView

#include "src/chrome/browser/ui/views/side_panel/reading_list/reading_list_side_panel_coordinator.cc"

#undef ReadLaterSidePanelWebView

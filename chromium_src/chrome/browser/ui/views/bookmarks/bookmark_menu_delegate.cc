/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/bookmarks/shunya_bookmark_context_menu.h"

#define BookmarkContextMenu ShunyaBookmarkContextMenu
#include "src/chrome/browser/ui/views/bookmarks/bookmark_menu_delegate.cc"
#undef BookmarkContextMenu

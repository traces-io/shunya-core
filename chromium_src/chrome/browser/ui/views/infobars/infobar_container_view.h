/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_INFOBARS_INFOBAR_CONTAINER_VIEW_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_INFOBARS_INFOBAR_CONTAINER_VIEW_H_

#include "components/infobars/core/infobar_container.h"

#define PlatformSpecificInfoBarStateChanged       \
  PlatformSpecificInfoBarStateChanged_UnUsed() {} \
  friend class ShunyaInfoBarContainerView;         \
  void PlatformSpecificInfoBarStateChanged

#include "src/chrome/browser/ui/views/infobars/infobar_container_view.h"  // IWYU pragma: export

#undef PlatformSpecificInfoBarStateChanged

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_INFOBARS_INFOBAR_CONTAINER_VIEW_H_

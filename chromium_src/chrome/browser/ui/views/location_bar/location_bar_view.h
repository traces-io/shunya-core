/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_LOCATION_BAR_LOCATION_BAR_VIEW_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_LOCATION_BAR_LOCATION_BAR_VIEW_H_

#define SHUNYA_LOCATION_BAR_VIEW_H_   \
 private:                            \
  friend class ShunyaLocationBarView; \
                                     \
 public:                             \
  virtual std::vector<views::View*> GetTrailingViews();

#define OnOmniboxBlurred virtual OnOmniboxBlurred
#define GetBorderRadius virtual GetBorderRadius
#include "src/chrome/browser/ui/views/location_bar/location_bar_view.h"  // IWYU pragma: export
#undef GetBorderRadius
#undef OnOmniboxBlurred
#undef SHUNYA_LOCATION_BAR_VIEW_H_

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_LOCATION_BAR_LOCATION_BAR_VIEW_H_

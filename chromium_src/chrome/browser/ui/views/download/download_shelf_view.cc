/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/download/shunya_download_item_view.h"

#define DownloadItemView ShunyaDownloadItemView
#include "src/chrome/browser/ui/views/download/download_shelf_view.cc"
#undef DownloadItemViewView

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "chrome/browser/ui/views/tabs/tab_style_views.h"
#include "shunya/browser/ui/color/shunya_color_id.h"
#include "shunya/browser/ui/views/tabs/shunya_tab_group_header.h"
#include "shunya/browser/ui/views/tabs/vertical_tab_utils.h"
#include "chrome/browser/ui/views/tabs/tab_slot_controller.h"

#define SHUNYA_GM2_TAB_STYLE_H \
 protected:                   \
  virtual
#define CreateForTab CreateForTab_ChromiumImpl
#include "src/chrome/browser/ui/views/tabs/tab_style_views.cc"
#undef CreateForTab
#undef SHUNYA_GM2_TAB_STYLE_H

#include "shunya/browser/ui/views/tabs/shunya_tab_style_views.inc.cc"

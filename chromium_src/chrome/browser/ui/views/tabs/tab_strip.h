/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_TABS_TAB_STRIP_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_TABS_TAB_STRIP_H_

#include "shunya/browser/ui/views/tabs/shunya_tab_container.h"
#include "chrome/browser/ui/views/tabs/tab_container.h"
#include "chrome/browser/ui/views/tabs/tab_slot_controller.h"

class ShunyaTabHoverCardController;

#define UpdateHoverCard                    \
  UpdateHoverCard_Unused();                \
  friend class ShunyaTabHoverTest;          \
  friend class ShunyaTabStrip;              \
  friend class VerticalTabStripRegionView; \
  void UpdateHoverCard

#define ShouldDrawStrokes   \
  UnUsed() { return true; } \
  virtual bool ShouldDrawStrokes
#define GetDragContext                                                  \
  Unused_GetDragContext() { return nullptr; }                           \
  friend class ShunyaTabStrip;                                           \
  friend class ShunyaTabDragContext;                                     \
  static constexpr bool IsUsingShunyaTabHoverCardController() {          \
    return std::is_same_v<std::unique_ptr<ShunyaTabHoverCardController>, \
                          decltype(TabStrip::hover_card_controller_)>;  \
  }                                                                     \
  virtual TabDragContext* GetDragContext
#define TabHoverCardController ShunyaTabHoverCardController
#include "src/chrome/browser/ui/views/tabs/tab_strip.h"  // IWYU pragma: export
#undef TabHoverCardController
#undef GetDragContext
#undef ShouldDrawStrokes
#undef UpdateHoverCard

static_assert(TabStrip::IsUsingShunyaTabHoverCardController(),
              "Should use ShunyaTabHoverCardController");

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_TABS_TAB_STRIP_H_

/* Copyright 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/views/frame/shunya_browser_non_client_frame_view_mac.h"

#define BrowserNonClientFrameViewMac ShunyaBrowserNonClientFrameViewMac

#include "src/chrome/browser/ui/views/frame/browser_non_client_frame_view_factory_mac.mm"

#undef BrowserNonClientFrameViewMac

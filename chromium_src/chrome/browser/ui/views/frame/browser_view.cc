/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

// Include the corresponding header first since it defines the same macros and
// therefore avoid undef before use.
#include "chrome/browser/ui/views/frame/browser_view.h"

#include "shunya/browser/ui/views/frame/shunya_browser_view_layout.h"
#include "shunya/browser/ui/views/infobars/shunya_infobar_container_view.h"
#include "shunya/browser/ui/views/side_panel/shunya_side_panel.h"
#include "shunya/browser/ui/views/side_panel/shunya_side_panel_coordinator.h"
#include "shunya/browser/ui/views/tabs/shunya_browser_tab_strip_controller.h"
#include "shunya/browser/ui/views/tabs/shunya_tab_strip.h"
#include "shunya/browser/ui/views/tabs/vertical_tab_utils.h"
#include "shunya/browser/ui/views/toolbar/shunya_toolbar_view.h"
#include "chrome/browser/ui/browser_commands.h"
#include "chrome/browser/ui/views/frame/tab_strip_region_view.h"
#include "chrome/browser/ui/views/side_panel/side_panel.h"

#define InfoBarContainerView ShunyaInfoBarContainerView
#define BrowserViewLayout ShunyaBrowserViewLayout
#define ToolbarView ShunyaToolbarView
#define BrowserTabStripController ShunyaBrowserTabStripController
#define TabStrip ShunyaTabStrip
#define SidePanel ShunyaSidePanel
#define kAlignLeft kHorizontalAlignLeft
#define kAlignRight kHorizontalAlignRight
#define SidePanelCoordinator ShunyaSidePanelCoordinator

#include "src/chrome/browser/ui/views/frame/browser_view.cc"

#undef SidePanelCoordintor
#undef ToolbarView
#undef BrowserTabStripController
#undef TabStrip
#undef BrowserViewLayout
#undef SidePanel
#undef kAlignLeft
#undef kAlignRight
#undef InfoBarContainerView

void BrowserView::SetNativeWindowPropertyForWidget(views::Widget* widget) {
  // Sets a kBrowserWindowKey to given child |widget| so that we can get
  // BrowserView from the |widget|.
  DCHECK(GetWidget());
  DCHECK_EQ(GetWidget(), widget->GetTopLevelWidget())
      << "The |widget| should be child of BrowserView's widget.";

  widget->SetNativeWindowProperty(kBrowserViewKey, this);
}

// Copyright (c) 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_FRAME_BROWSER_VIEW_LAYOUT_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_FRAME_BROWSER_VIEW_LAYOUT_H_

// Make override-able
#define LayoutSidePanelView virtual LayoutSidePanelView
#define LayoutTabStripRegion           \
  UnUsed() { return {}; }              \
  friend class ShunyaBrowserViewLayout; \
  virtual int LayoutTabStripRegion
#define LayoutBookmarkAndInfoBars virtual LayoutBookmarkAndInfoBars
#define LayoutInfoBar virtual LayoutInfoBar
#define LayoutContentsContainerView virtual LayoutContentsContainerView

#include "src/chrome/browser/ui/views/frame/browser_view_layout.h"  // IWYU pragma: export

#undef LayoutContentsContainerView
#undef LayoutInfoBar
#undef LayoutBookmarkAndInfoBars
#undef LayoutTabStripRegion
#undef LayoutSidePanelView

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_VIEWS_FRAME_BROWSER_VIEW_LAYOUT_H_

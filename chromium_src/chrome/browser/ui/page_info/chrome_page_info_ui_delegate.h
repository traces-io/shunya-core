/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_PAGE_INFO_CHROME_PAGE_INFO_UI_DELEGATE_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_PAGE_INFO_CHROME_PAGE_INFO_UI_DELEGATE_H_

#include "components/page_info/page_info_ui_delegate.h"

#define ShouldShowAsk                     \
  AddIPFSTabForURL(const GURL& ipfs_url); \
  bool ShouldShowAsk

#include "src/chrome/browser/ui/page_info/chrome_page_info_ui_delegate.h"  // IWYU pragma: export

#undef ShouldShowAsk

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_UI_PAGE_INFO_CHROME_PAGE_INFO_UI_DELEGATE_H_

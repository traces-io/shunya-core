/* Copyright (c) 2018 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "shunya/browser/ui/shunya_layout_constants.h"

// Forward declaration
int GetLayoutConstant_ChromiumImpl(LayoutConstant constant);

#define LayoutConstant LayoutConstant constant) {                             \
    const absl::optional<int> shunyaOption = GetShunyaLayoutConstant(constant); \
    if (shunyaOption) {                                                        \
      return shunyaOption.value();                                             \
    }                                                                         \
                                                                              \
    return GetLayoutConstant_ChromiumImpl(constant);                          \
  }                                                                           \
                                                                              \
  int GetLayoutConstant_ChromiumImpl(LayoutConstant

#include "src/chrome/browser/ui/layout_constants.cc"
#undef LayoutConstant

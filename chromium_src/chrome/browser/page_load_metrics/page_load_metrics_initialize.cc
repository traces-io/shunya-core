/* Copyright 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#define InitializePageLoadMetricsForWebContents \
  InitializePageLoadMetricsForWebContents_Chromium
#include "src/chrome/browser/page_load_metrics/page_load_metrics_initialize.cc"
#undef InitializePageLoadMetricsForWebContents

#include "shunya/components/shunya_perf_predictor/browser/perf_predictor_page_metrics_observer.h"

namespace chrome {

namespace {

class ShunyaPageLoadMetricsEmbedder : public chrome::PageLoadMetricsEmbedder {
 public:
  explicit ShunyaPageLoadMetricsEmbedder(content::WebContents* web_contents);
  ShunyaPageLoadMetricsEmbedder(const ShunyaPageLoadMetricsEmbedder&) = delete;
  ShunyaPageLoadMetricsEmbedder& operator=(const ShunyaPageLoadMetricsEmbedder&) =
      delete;
  ~ShunyaPageLoadMetricsEmbedder() override;

 protected:
  // page_load_metrics::PageLoadMetricsEmbedderBase:
  void RegisterEmbedderObservers(
      ::page_load_metrics::PageLoadTracker* tracker) override;
};

ShunyaPageLoadMetricsEmbedder::ShunyaPageLoadMetricsEmbedder(
    content::WebContents* web_contents)
    : chrome::PageLoadMetricsEmbedder(web_contents) {}

ShunyaPageLoadMetricsEmbedder::~ShunyaPageLoadMetricsEmbedder() = default;

void ShunyaPageLoadMetricsEmbedder::RegisterEmbedderObservers(
    page_load_metrics::PageLoadTracker* tracker) {
  PageLoadMetricsEmbedder::RegisterEmbedderObservers(tracker);

  tracker->AddObserver(
      std::make_unique<
          shunya_perf_predictor::PerfPredictorPageMetricsObserver>());
}

}  // namespace

void InitializePageLoadMetricsForWebContents(
    content::WebContents* web_contents) {
  // TODO(bug https://github.com/shunya/shunya-browser/issues/7784)
  // change
  // android_webview/browser/page_load_metrics/page_load_metrics_initialize.cc
  // as well to register Page Load Metrics Observers
  page_load_metrics::MetricsWebContentsObserver::CreateForWebContents(
      web_contents,
      std::make_unique<ShunyaPageLoadMetricsEmbedder>(web_contents));
}

}  // namespace chrome

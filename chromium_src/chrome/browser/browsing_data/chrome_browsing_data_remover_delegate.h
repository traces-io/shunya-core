/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_BROWSING_DATA_CHROME_BROWSING_DATA_REMOVER_DELEGATE_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_BROWSING_DATA_CHROME_BROWSING_DATA_REMOVER_DELEGATE_H_

class ShunyaBrowsingDataRemoverDelegate;

#define SHUNYA_CHROME_BROWSING_DATA_REMOVER_DELEGATE_H \
  friend class ShunyaBrowsingDataRemoverDelegate;

#include "src/chrome/browser/browsing_data/chrome_browsing_data_remover_delegate.h"  // IWYU pragma: export
#undef SHUNYA_CHROME_BROWSING_DATA_REMOVER_DELEGATE_H

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_BROWSING_DATA_CHROME_BROWSING_DATA_REMOVER_DELEGATE_H_

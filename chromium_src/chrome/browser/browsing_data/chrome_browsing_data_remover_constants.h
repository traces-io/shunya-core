/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_BROWSING_DATA_CHROME_BROWSING_DATA_REMOVER_CONSTANTS_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_BROWSING_DATA_CHROME_BROWSING_DATA_REMOVER_CONSTANTS_H_

#include "shunya/components/ai_chat/common/buildflags/buildflags.h"

#include "src/chrome/browser/browsing_data/chrome_browsing_data_remover_constants.h"  // IWYU pragma: export

namespace chrome_browsing_data_remover {
constexpr DataType GetShunyaDataTypeValue(const int index) {
  return DataType(1) << (63 - index);
}

#if BUILDFLAG(ENABLE_AI_CHAT)
constexpr DataType DATA_TYPE_SHUNYA_LEO_HISTORY = GetShunyaDataTypeValue(0);
#endif  // BUILDFLAG(ENABLE_AI_CHAT)
}  // namespace chrome_browsing_data_remover

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_BROWSING_DATA_CHROME_BROWSING_DATA_REMOVER_CONSTANTS_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_DEVTOOLS_DEVTOOLS_WINDOW_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_DEVTOOLS_DEVTOOLS_WINDOW_H_

#define HatsNextWebDialogBrowserTest \
  HatsNextWebDialogBrowserTest;      \
  friend class TorProfileManagerUnitTest

#include "src/chrome/browser/devtools/devtools_window.h"  // IWYU pragma: export

#undef HatsNextWebDialogBrowserTest

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_DEVTOOLS_DEVTOOLS_WINDOW_H_

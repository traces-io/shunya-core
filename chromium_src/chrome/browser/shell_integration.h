/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_SHELL_INTEGRATION_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_SHELL_INTEGRATION_H_

#define SHUNYA_DEFAULT_BROWSER_WORKER_H friend class ShunyaDefaultBrowserWorker;

#include "src/chrome/browser/shell_integration.h"  // IWYU pragma: export

#undef SHUNYA_DEFAULT_BROWSER_WORKER_H

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_SHELL_INTEGRATION_H_

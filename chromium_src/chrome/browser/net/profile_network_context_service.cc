/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */
static const char* kShunyaCTExcludedHosts[] = {
    // Critical endpoints that shouldn't require SCTs so they always work
    "laptop-updates.shunya.com",
    "updates.shunyasoftware.com",
    "updates-cdn.shunyasoftware.com",
    // Test host for manual testing
    "sct-exempted.shunyasoftware.com",
};

#define SHUNYA_PROFILE_NETWORK_CONTEXT_SERVICE_GET_CT_POLICY \
  for (const auto* host : kShunyaCTExcludedHosts) {          \
    excluded.push_back(host);                               \
  }

#include "src/chrome/browser/net/profile_network_context_service.cc"
#undef SHUNYA_PROFILE_NETWORK_CONTEXT_SERVICE_GET_CT_POLICY

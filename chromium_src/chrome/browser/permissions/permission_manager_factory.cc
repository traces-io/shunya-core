/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "chrome/browser/permissions/permission_manager_factory.h"

#include "shunya/browser/geolocation/shunya_geolocation_permission_context_delegate.h"
#include "shunya/browser/permissions/permission_lifetime_manager_factory.h"
#include "shunya/components/permissions/shunya_permission_manager.h"
#include "shunya/components/permissions/contexts/shunya_google_sign_in_permission_context.h"
#include "shunya/components/permissions/contexts/shunya_localhost_permission_context.h"
#include "shunya/components/permissions/contexts/shunya_wallet_permission_context.h"
#include "shunya/components/permissions/permission_lifetime_manager.h"
#include "components/permissions/features.h"

#define GeolocationPermissionContextDelegate \
  ShunyaGeolocationPermissionContextDelegate

#define BuildServiceInstanceFor BuildServiceInstanceFor_ChromiumImpl

#include "src/chrome/browser/permissions/permission_manager_factory.cc"

#undef GeolocationPermissionContextDelegate
#undef BuildServiceInstanceFor

KeyedService* PermissionManagerFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  Profile* profile = Profile::FromBrowserContext(context);
  auto permission_contexts = CreatePermissionContexts(profile);

  permission_contexts[ContentSettingsType::SHUNYA_ETHEREUM] =
      std::make_unique<permissions::ShunyaWalletPermissionContext>(
          profile, ContentSettingsType::SHUNYA_ETHEREUM);
  permission_contexts[ContentSettingsType::SHUNYA_SOLANA] =
      std::make_unique<permissions::ShunyaWalletPermissionContext>(
          profile, ContentSettingsType::SHUNYA_SOLANA);
  permission_contexts[ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN] =
      std::make_unique<permissions::ShunyaGoogleSignInPermissionContext>(
          profile);
  permission_contexts[ContentSettingsType::SHUNYA_LOCALHOST_ACCESS] =
      std::make_unique<permissions::ShunyaLocalhostPermissionContext>(profile);

  if (base::FeatureList::IsEnabled(
          permissions::features::kPermissionLifetime)) {
    auto factory =
        base::BindRepeating(&PermissionLifetimeManagerFactory::GetForProfile);
    for (auto& permission_context : permission_contexts) {
      permission_context.second->SetPermissionLifetimeManagerFactory(factory);
    }
  }

  return new permissions::ShunyaPermissionManager(
      profile, std::move(permission_contexts));
}

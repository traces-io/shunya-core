/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_PERMISSIONS_PERMISSION_MANAGER_FACTORY_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_PERMISSIONS_PERMISSION_MANAGER_FACTORY_H_

#include "components/keyed_service/content/browser_context_keyed_service_factory.h"

namespace shunya_wallet {
class EthereumProviderImplUnitTest;
class SolanaProviderImplUnitTest;
}  // namespace shunya_wallet

namespace permissions {
class ShunyaWalletPermissionContextUnitTest;
}

#define BuildServiceInstanceFor                                          \
  BuildServiceInstanceFor_ChromiumImpl(content::BrowserContext* profile) \
      const;                                                             \
  friend shunya_wallet::EthereumProviderImplUnitTest;                     \
  friend shunya_wallet::SolanaProviderImplUnitTest;                       \
  friend permissions::ShunyaWalletPermissionContextUnitTest;              \
  KeyedService* BuildServiceInstanceFor

#include "src/chrome/browser/permissions/permission_manager_factory.h"  // IWYU pragma: export
#undef BuildServiceInstanceFor

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_BROWSER_PERMISSIONS_PERMISSION_MANAGER_FACTORY_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "chrome/browser/extensions/chrome_component_extension_resource_manager.h"

#include "shunya/components/shunya_extension/grit/shunya_extension_generated_map.h"
#include "shunya/components/shunya_extension/grit/shunya_extension_resources_map.h"
#include "shunya/components/shunya_webtorrent/browser/buildflags/buildflags.h"

#if BUILDFLAG(ENABLE_SHUNYA_WEBTORRENT)
#include "shunya/components/shunya_webtorrent/grit/shunya_webtorrent_generated_map.h"
#include "shunya/components/shunya_webtorrent/grit/shunya_webtorrent_resources_map.h"
#endif

#if BUILDFLAG(ENABLE_SHUNYA_WEBTORRENT)
#define SHUNYA_WEBTORRENT_RESOURCES                            \
  AddComponentResourceEntries(kShunyaWebtorrentResources,      \
                              kShunyaWebtorrentResourcesSize); \
  AddComponentResourceEntries(kShunyaWebtorrentGenerated,      \
                              kShunyaWebtorrentGeneratedSize);
#else
#define SHUNYA_WEBTORRENT_RESOURCES
#endif

#define SHUNYA_CHROME_COMPONENT_EXTENSION_RESOURCE_MANAGER_DATA_DATA  \
  AddComponentResourceEntries(kShunyaExtension, kShunyaExtensionSize); \
  AddComponentResourceEntries(kShunyaExtensionGenerated,              \
                              kShunyaExtensionGeneratedSize);         \
  SHUNYA_WEBTORRENT_RESOURCES

#include "src/chrome/browser/extensions/chrome_component_extension_resource_manager.cc"
#undef SHUNYA_CHROME_COMPONENT_EXTENSION_RESOURCE_MANAGER_DATA_DATA

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "chrome/test/base/testing_browser_process.h"

#include "shunya/test/base/testing_shunya_browser_process.h"

#define TestingBrowserProcess TestingBrowserProcess_ChromiumImpl
#include "src/chrome/test/base/testing_browser_process.cc"
#undef TestingBrowserProcess

// static
void TestingBrowserProcess::CreateInstance() {
  TestingBrowserProcess_ChromiumImpl::CreateInstance();
  TestingShunyaBrowserProcess::CreateInstance();
}

// static
void TestingBrowserProcess::DeleteInstance() {
  TestingBrowserProcess_ChromiumImpl::DeleteInstance();
  TestingShunyaBrowserProcess::DeleteInstance();
}

// static
void TestingBrowserProcess::TearDownAndDeleteInstance() {
  TestingBrowserProcess_ChromiumImpl::TearDownAndDeleteInstance();
  TestingShunyaBrowserProcess::TearDownAndDeleteInstance();
}

// static
TestingBrowserProcess* TestingBrowserProcess::GetGlobal() {
  return static_cast<TestingBrowserProcess*>(
      TestingBrowserProcess_ChromiumImpl::GetGlobal());
}

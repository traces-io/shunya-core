/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_TEST_BASE_TESTING_PROFILE_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_TEST_BASE_TESTING_PROFILE_H_

#include "src/chrome/test/base/testing_profile.h"  // IWYU pragma: export

#include "shunya/test/base/shunya_testing_profile.h"

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_TEST_BASE_TESTING_PROFILE_H_

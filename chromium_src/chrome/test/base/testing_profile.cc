/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#define ChromeBrowsingDataRemoverDelegate ShunyaBrowsingDataRemoverDelegate
#include "src/chrome/test/base/testing_profile.cc"
#undef ChromeBrowsingDataRemoverDelegate

#include "shunya/test/base/shunya_testing_profile.cc"

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "base/files/file_util.h"
#include "base/process/launch.h"
#include "base/process/process.h"
#include "shunya/components/shunya_vpn/common/buildflags/buildflags.h"
#include "shunya/installer/util/shunya_shell_util.h"

#if BUILDFLAG(ENABLE_SHUNYA_VPN)
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_constants.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/shunya_vpn_helper/shunya_vpn_helper_state.h"
#include "shunya/components/shunya_vpn/browser/connection/ikev2/win/ras_utils.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/service_constants.h"
#include "shunya/components/shunya_vpn/common/wireguard/win/service_details.h"
#endif
#define UninstallProduct UninstallProduct_ChromiumImpl

#include "src/chrome/installer/setup/uninstall.cc"

#undef UninstallProduct

namespace installer {

namespace {

bool UninstallShunyaVPNWireguardService(const base::FilePath& exe_path) {
  if (!base::PathExists(exe_path)) {
    return false;
  }
  base::CommandLine cmd(exe_path);
  cmd.AppendSwitch(shunya_vpn::kShunyaVpnWireguardServiceUnnstallSwitchName);
  base::LaunchOptions options = base::LaunchOptions();
  options.wait = true;
  return base::LaunchProcess(cmd, options).IsValid();
}

void DeleteShunyaFileKeys(HKEY root) {
  // Delete Software\Classes\ShunyaXXXFile.
  std::wstring reg_prog_id(ShellUtil::kRegClasses);
  reg_prog_id.push_back(base::FilePath::kSeparators[0]);
  reg_prog_id.append(GetProgIdForFileType());
  DeleteRegistryKey(root, reg_prog_id, WorkItem::kWow64Default);

  // Cleanup OpenWithList and OpenWithProgids:
  // http://msdn.microsoft.com/en-us/library/bb166549
  std::wstring file_assoc_key;
  std::wstring open_with_progids_key;
  for (int i = 0; ShellUtil::kPotentialFileAssociations[i] != nullptr; ++i) {
    file_assoc_key.assign(ShellUtil::kRegClasses);
    file_assoc_key.push_back(base::FilePath::kSeparators[0]);
    file_assoc_key.append(ShellUtil::kPotentialFileAssociations[i]);
    file_assoc_key.push_back(base::FilePath::kSeparators[0]);

    open_with_progids_key.assign(file_assoc_key);
    open_with_progids_key.append(ShellUtil::kRegOpenWithProgids);
    if (ShouldUseFileTypeProgId(ShellUtil::kPotentialFileAssociations[i])) {
      DeleteRegistryValue(root, open_with_progids_key, WorkItem::kWow64Default,
                          GetProgIdForFileType());
    }
  }
}

}  // namespace

InstallStatus UninstallProduct(const ModifyParams& modify_params,
                               bool remove_all,
                               bool force_uninstall,
                               const base::CommandLine& cmd_line) {
  DeleteShunyaFileKeys(HKEY_CURRENT_USER);

  const auto installer_state = modify_params.installer_state;
  const base::FilePath chrome_exe(
      installer_state->target_path().Append(installer::kChromeExe));
  const std::wstring suffix(
      ShellUtil::GetCurrentInstallationSuffix(chrome_exe));
  if (installer_state->system_install() ||
      (remove_all &&
       ShellUtil::QuickIsChromeRegisteredInHKLM(chrome_exe, suffix))) {
    DeleteShunyaFileKeys(HKEY_LOCAL_MACHINE);
  }
#if BUILDFLAG(ENABLE_SHUNYA_VPN)
  if (installer_state->system_install()) {
    if (!InstallServiceWorkItem::DeleteService(
            shunya_vpn::GetShunyaVpnHelperServiceName(),
            shunya_vpn::kShunyaVpnHelperRegistryStoragePath, {}, {})) {
      LOG(WARNING) << "Failed to delete "
                   << shunya_vpn::GetShunyaVpnHelperServiceName();
    }

    if (!UninstallShunyaVPNWireguardService(
            shunya_vpn::GetShunyaVPNWireguardServiceInstallationPath(
                installer_state->target_path(),
                *modify_params.current_version))) {
      LOG(WARNING) << "Failed to delete "
                   << shunya_vpn::GetShunyaVpnWireguardServiceName();
    }
  }
  shunya_vpn::ras::RemoveEntry(shunya_vpn::GetShunyaVPNConnectionName());
#endif
  return UninstallProduct_ChromiumImpl(modify_params, remove_all,
                                       force_uninstall, cmd_line);
}

}  // namespace installer

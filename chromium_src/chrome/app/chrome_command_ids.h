/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_APP_CHROME_COMMAND_IDS_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_APP_CHROME_COMMAND_IDS_H_

#include "shunya/app/shunya_command_ids.h"
#include "src/chrome/app/chrome_command_ids.h"  // IWYU pragma: export

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_APP_CHROME_COMMAND_IDS_H_

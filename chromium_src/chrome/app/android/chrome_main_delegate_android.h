/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_APP_ANDROID_CHROME_MAIN_DELEGATE_ANDROID_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_APP_ANDROID_CHROME_MAIN_DELEGATE_ANDROID_H_

#include "shunya/app/shunya_main_delegate.h"

#define ChromeMainDelegate ShunyaMainDelegate
#include "src/chrome/app/android/chrome_main_delegate_android.h"  // IWYU pragma: export
#undef ChromeMainDelegate

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_APP_ANDROID_CHROME_MAIN_DELEGATE_ANDROID_H_

// Copyright (c) 2019 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at https://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_GRIT_CHROME_UNSCALED_RESOURCES_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_GRIT_CHROME_UNSCALED_RESOURCES_H_

#include "shunya/grit/shunya_unscaled_resources.h"  // IWYU pragma: export

#include "../gen/chrome/grit/chrome_unscaled_resources.h"  // IWYU pragma: export

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_GRIT_CHROME_UNSCALED_RESOURCES_H_

// Copyright (c) 2019 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_GRIT_GENERATED_RESOURCES_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_GRIT_GENERATED_RESOURCES_H_

#include "shunya/grit/shunya_generated_resources.h"  // IWYU pragma: export

#include "../gen/chrome/grit/generated_resources.h"  // IWYU pragma: export

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_GRIT_GENERATED_RESOURCES_H_

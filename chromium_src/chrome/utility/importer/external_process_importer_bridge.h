/* Copyright 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_UTILITY_IMPORTER_EXTERNAL_PROCESS_IMPORTER_BRIDGE_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_UTILITY_IMPORTER_EXTERNAL_PROCESS_IMPORTER_BRIDGE_H_

#define SHUNYA_EXTERNAL_PROCESS_IMPORTER_BRIDGE_H_ \
    friend class ShunyaExternalProcessImporterBridge;
#include "src/chrome/utility/importer/external_process_importer_bridge.h"  // IWYU pragma: export
#undef SHUNYA_EXTERNAL_PROCESS_IMPORTER_BRIDGE_H_

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_UTILITY_IMPORTER_EXTERNAL_PROCESS_IMPORTER_BRIDGE_H_

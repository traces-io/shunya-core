/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_CHROME_RENDERER_WORKER_CONTENT_SETTINGS_CLIENT_H_
#define SHUNYA_CHROMIUM_SRC_CHROME_RENDERER_WORKER_CONTENT_SETTINGS_CLIENT_H_

#define SHUNYA_WORKER_CONTENT_SETTINGS_CLIENT_H                       \
  ShunyaFarblingLevel GetShunyaFarblingLevel() override;               \
  blink::WebSecurityOrigin GetEphemeralStorageOriginSync() override; \
  bool HasContentSettingsRules() const override;

#include "src/chrome/renderer/worker_content_settings_client.h"  // IWYU pragma: export

#undef SHUNYA_WORKER_CONTENT_SETTINGS_CLIENT_H

#endif  // SHUNYA_CHROMIUM_SRC_CHROME_RENDERER_WORKER_CONTENT_SETTINGS_CLIENT_H_

/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_OS_CRYPT_SYNC_KEY_STORAGE_KWALLET_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_OS_CRYPT_SYNC_KEY_STORAGE_KWALLET_H_

#define InitFolder             \
  InitFolder();                \
  const char* GetFolderName(); \
  const char* GetKeyName

#include "src/components/os_crypt/sync/key_storage_kwallet.h"  // IWYU pragma: export
#undef InitFolder

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_OS_CRYPT_SYNC_KEY_STORAGE_KWALLET_H_

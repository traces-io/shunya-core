/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_DOWNLOAD_PUBLIC_BACKGROUND_SERVICE_CLIENTS_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_DOWNLOAD_PUBLIC_BACKGROUND_SERVICE_CLIENTS_H_

#define SHUNYA_DOWNLOAD_CLIENT CUSTOM_LIST_SUBSCRIPTIONS = 255,

#include "src/components/download/public/background_service/clients.h"  // IWYU pragma: export

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_DOWNLOAD_PUBLIC_BACKGROUND_SERVICE_CLIENTS_H_

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "components/embedder_support/user_agent_utils.h"

namespace {

constexpr char kShunyaBrandNameForCHUA[] = "Shunya";

}  // namespace

// Chromium uses `version_info::GetProductName()` to get the browser's "brand"
// name, but on MacOS we use different names for different channels (adding Beta
// or Nightly, for example). In the UA client hint, though, we want a consistent
// name regardless of the channel, so we just hard-code it. Note, that we use
// IDS_PRODUCT_NAME from app/chromium_strings.grd (shunya_strings.grd) in
// constructing the UA in shunya/browser/shunya_content_browser_client.cc, but we
// can't use it here in the //components.
#define SHUNYA_GET_USER_AGENT_BRAND_LIST brand = kShunyaBrandNameForCHUA;

#include "src/components/embedder_support/user_agent_utils.cc"
#undef SHUNYA_GET_USER_AGENT_BRAND_LIST

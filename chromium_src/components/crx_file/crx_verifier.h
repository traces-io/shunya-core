/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * you can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_CRX_FILE_CRX_VERIFIER_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_CRX_FILE_CRX_VERIFIER_H_

#include "src/components/crx_file/crx_verifier.h"  // IWYU pragma: export

namespace crx_file {

void SetShunyaPublisherKeyHashForTesting(const std::vector<uint8_t>& test_key);

}  // namespace crx_file

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_CRX_FILE_CRX_VERIFIER_H_

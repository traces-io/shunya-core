/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "components/permissions/permission_util.h"
#include "components/permissions/permission_uma_util.h"
#include "third_party/blink/public/common/permissions/permission_utils.h"

#define PermissionUtil PermissionUtil_ChromiumImpl

#define PERMISSION_UTIL_PERMISSION_TYPE_TO_CONTENT_SETTINGS_TYPE \
  case PermissionType::SHUNYA_ADS:                                \
    return ContentSettingsType::SHUNYA_ADS;                       \
  case PermissionType::SHUNYA_COSMETIC_FILTERING:                 \
    return ContentSettingsType::SHUNYA_COSMETIC_FILTERING;        \
  case PermissionType::SHUNYA_TRACKERS:                           \
    return ContentSettingsType::SHUNYA_TRACKERS;                  \
  case PermissionType::SHUNYA_HTTP_UPGRADABLE_RESOURCES:          \
    return ContentSettingsType::SHUNYA_HTTP_UPGRADABLE_RESOURCES; \
  case PermissionType::SHUNYA_FINGERPRINTING_V2:                  \
    return ContentSettingsType::SHUNYA_FINGERPRINTING_V2;         \
  case PermissionType::SHUNYA_SHIELDS:                            \
    return ContentSettingsType::SHUNYA_SHIELDS;                   \
  case PermissionType::SHUNYA_REFERRERS:                          \
    return ContentSettingsType::SHUNYA_REFERRERS;                 \
  case PermissionType::SHUNYA_COOKIES:                            \
    return ContentSettingsType::SHUNYA_COOKIES;                   \
  case PermissionType::SHUNYA_SPEEDREADER:                        \
    return ContentSettingsType::SHUNYA_SPEEDREADER;               \
  case PermissionType::SHUNYA_ETHEREUM:                           \
    return ContentSettingsType::SHUNYA_ETHEREUM;                  \
  case PermissionType::SHUNYA_SOLANA:                             \
    return ContentSettingsType::SHUNYA_SOLANA;                    \
  case PermissionType::SHUNYA_GOOGLE_SIGN_IN:                     \
    return ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN;            \
  case PermissionType::SHUNYA_LOCALHOST_ACCESS:                   \
    return ContentSettingsType::SHUNYA_LOCALHOST_ACCESS;

#include "src/components/permissions/permission_util.cc"
#undef PermissionUtil
#undef PERMISSION_UTIL_PERMISSION_TYPE_TO_CONTENT_SETTINGS_TYPE

namespace permissions {

// static
std::string PermissionUtil::GetPermissionString(
    ContentSettingsType content_type) {
  switch (content_type) {
    case ContentSettingsType::SHUNYA_ETHEREUM:
      return "ShunyaEthereum";
    case ContentSettingsType::SHUNYA_SOLANA:
      return "ShunyaSolana";
    case ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN:
      return "ShunyaGoogleSignInPermission";
    case ContentSettingsType::SHUNYA_LOCALHOST_ACCESS:
      return "ShunyaLocalhostAccessPermission";
    default:
      return PermissionUtil_ChromiumImpl::GetPermissionString(content_type);
  }
}

// static
bool PermissionUtil::GetPermissionType(ContentSettingsType type,
                                       blink::PermissionType* out) {
  if (type == ContentSettingsType::SHUNYA_ETHEREUM ||
      type == ContentSettingsType::SHUNYA_SOLANA) {
    *out = PermissionType::WINDOW_MANAGEMENT;
    return true;
  }
  if (type == ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN) {
    *out = PermissionType::SHUNYA_GOOGLE_SIGN_IN;
    return true;
  }
  if (type == ContentSettingsType::SHUNYA_LOCALHOST_ACCESS) {
    *out = PermissionType::SHUNYA_LOCALHOST_ACCESS;
    return true;
  }

  return PermissionUtil_ChromiumImpl::GetPermissionType(type, out);
}

// static
bool PermissionUtil::IsPermission(ContentSettingsType type) {
  switch (type) {
    case ContentSettingsType::SHUNYA_ETHEREUM:
    case ContentSettingsType::SHUNYA_SOLANA:
    case ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN:
    case ContentSettingsType::SHUNYA_LOCALHOST_ACCESS:
      return true;
    default:
      return PermissionUtil_ChromiumImpl::IsPermission(type);
  }
}

PermissionType PermissionUtil::ContentSettingTypeToPermissionType(
    ContentSettingsType permission) {
  switch (permission) {
    case ContentSettingsType::SHUNYA_ADS:
      return PermissionType::SHUNYA_ADS;
    case ContentSettingsType::SHUNYA_COSMETIC_FILTERING:
      return PermissionType::SHUNYA_COSMETIC_FILTERING;
    case ContentSettingsType::SHUNYA_TRACKERS:
      return PermissionType::SHUNYA_TRACKERS;
    case ContentSettingsType::SHUNYA_HTTP_UPGRADABLE_RESOURCES:
      return PermissionType::SHUNYA_HTTP_UPGRADABLE_RESOURCES;
    case ContentSettingsType::SHUNYA_FINGERPRINTING_V2:
      return PermissionType::SHUNYA_FINGERPRINTING_V2;
    case ContentSettingsType::SHUNYA_SHIELDS:
      return PermissionType::SHUNYA_SHIELDS;
    case ContentSettingsType::SHUNYA_REFERRERS:
      return PermissionType::SHUNYA_REFERRERS;
    case ContentSettingsType::SHUNYA_COOKIES:
      return PermissionType::SHUNYA_COOKIES;
    case ContentSettingsType::SHUNYA_SPEEDREADER:
      return PermissionType::SHUNYA_SPEEDREADER;
    case ContentSettingsType::SHUNYA_ETHEREUM:
      return PermissionType::SHUNYA_ETHEREUM;
    case ContentSettingsType::SHUNYA_SOLANA:
      return PermissionType::SHUNYA_SOLANA;
    case ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN:
      return PermissionType::SHUNYA_GOOGLE_SIGN_IN;
    case ContentSettingsType::SHUNYA_LOCALHOST_ACCESS:
      return PermissionType::SHUNYA_LOCALHOST_ACCESS;
    default:
      return PermissionUtil_ChromiumImpl::ContentSettingTypeToPermissionType(
          permission);
  }
}

GURL PermissionUtil::GetCanonicalOrigin(ContentSettingsType permission,
                                        const GURL& requesting_origin,
                                        const GURL& embedding_origin) {
  // Use requesting_origin which will have ethereum or solana address info.
  if (permission == ContentSettingsType::SHUNYA_ETHEREUM ||
      permission == ContentSettingsType::SHUNYA_SOLANA)
    return requesting_origin;

  return PermissionUtil_ChromiumImpl::GetCanonicalOrigin(
      permission, requesting_origin, embedding_origin);
}

}  // namespace permissions

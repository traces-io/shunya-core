/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_PERMISSIONS_PERMISSIONS_CLIENT_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_PERMISSIONS_PERMISSIONS_CLIENT_H_

#define CanBypassEmbeddingOriginCheck                               \
  ShunyaCanBypassEmbeddingOriginCheck(const GURL& requesting_origin, \
                                     const GURL& embedding_origin,  \
                                     ContentSettingsType type);     \
  virtual bool CanBypassEmbeddingOriginCheck

#include "src/components/permissions/permissions_client.h"  // IWYU pragma: export
#undef CanBypassEmbeddingOriginCheck

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_PERMISSIONS_PERMISSIONS_CLIENT_H_

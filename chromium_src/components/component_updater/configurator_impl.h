/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * you can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_COMPONENT_UPDATER_CONFIGURATOR_IMPL_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_COMPONENT_UPDATER_CONFIGURATOR_IMPL_H_

// Prevent EnabledBackgroundDownloader from being redefined in the interface.
#include "components/update_client/configurator.h"

#define EnabledBackgroundDownloader                 \
  EnabledBackgroundDownloader_ChromiumImpl() const; \
  bool EnabledBackgroundDownloader

#include "src/components/component_updater/configurator_impl.h"  // IWYU pragma: export

#undef EnabledBackgroundDownloader

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_COMPONENT_UPDATER_CONFIGURATOR_IMPL_H_

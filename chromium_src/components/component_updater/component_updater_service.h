/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_COMPONENT_UPDATER_COMPONENT_UPDATER_SERVICE_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_COMPONENT_UPDATER_COMPONENT_UPDATER_SERVICE_H_

class IPFSDOMHandler;

namespace chrome {
namespace android {
class ShunyaComponentUpdaterAndroid;
}
}  // namespace chrome

#define SHUNYA_COMPONENT_UPDATER_SERVICE_H_ \
  friend class ::IPFSDOMHandler;           \
  friend class ::chrome::android::ShunyaComponentUpdaterAndroid;

#define SHUNYA_COMPONENT_UPDATER_SERVICE_H_ON_DEMAND_UPDATER \
 private:                                                   \
  friend void ShunyaOnDemandUpdate(const std::string&);      \
                                                            \
 public:
#include "src/components/component_updater/component_updater_service.h"  // IWYU pragma: export

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_COMPONENT_UPDATER_COMPONENT_UPDATER_SERVICE_H_

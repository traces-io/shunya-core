/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_HISTORY_CORE_BROWSER_HISTORY_SERVICE_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_HISTORY_CORE_BROWSER_HISTORY_SERVICE_H_

class ShunyaHistoryURLProviderTest;
class ShunyaHistoryQuickProviderTest;

#define Cleanup                                 \
  Cleanup();                                    \
  friend class ::ShunyaHistoryURLProviderTest;   \
  friend class ::ShunyaHistoryQuickProviderTest; \
  void CleanupUnused

#include "src/components/history/core/browser/history_service.h"  // IWYU pragma: export

#undef Cleanup

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_HISTORY_CORE_BROWSER_HISTORY_SERVICE_H_

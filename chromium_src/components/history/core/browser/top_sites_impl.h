/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_HISTORY_CORE_BROWSER_TOP_SITES_IMPL_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_HISTORY_CORE_BROWSER_TOP_SITES_IMPL_H_

// Needs 12 items for our NTP top site tiles.
#define kTopSitesNumber \
  kTopSitesNumber = 12; \
  static constexpr size_t kTopSitesNumber_Unused

#include "src/components/history/core/browser/top_sites_impl.h"  // IWYU pragma: export

#undef kTopSitesNumber

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_HISTORY_CORE_BROWSER_TOP_SITES_IMPL_H_

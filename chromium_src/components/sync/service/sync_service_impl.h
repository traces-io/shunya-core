/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_SERVICE_SYNC_SERVICE_IMPL_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_SERVICE_SYNC_SERVICE_IMPL_H_

#include "base/gtest_prod_util.h"

#define SHUNYA_SYNC_SERVICE_IMPL_H_                                             \
 private:                                                                      \
  friend class ShunyaSyncServiceImpl;                                           \
  friend class ShunyaSyncServiceImplTest;                                       \
  FRIEND_TEST_ALL_PREFIXES(ShunyaSyncServiceImplTest, OnSelfDeviceInfoDeleted); \
  FRIEND_TEST_ALL_PREFIXES(ShunyaSyncServiceImplTest,                           \
                           PermanentlyDeleteAccount);                          \
  FRIEND_TEST_ALL_PREFIXES(ShunyaSyncServiceImplTest,                           \
                           OnAccountDeleted_FailureAndRetry);                  \
  FRIEND_TEST_ALL_PREFIXES(ShunyaSyncServiceImplTest, JoinDeletedChain);

// Forcing this include before define virtual to avoid error of
// "duplicate 'virtual' declaration specifier" at SyncEngine's
// 'virtual void Initialize(InitParams params) = 0'
// This also resolves confusion with existing 'Initialize' methods in
//   third_party/protobuf/src/google/protobuf/map_type_handler.h,
//   third_party/protobuf/src/google/protobuf/map_entry_lite.h
#include "components/sync/engine/sync_engine.h"
#define Initialize virtual Initialize
#define ResetEngine virtual ResetEngine

#include "src/components/sync/service/sync_service_impl.h"  // IWYU pragma: export

#undef ResetEngine
#undef Initialize

#undef SHUNYA_SYNC_SERVICE_IMPL_H_

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_SERVICE_SYNC_SERVICE_IMPL_H_

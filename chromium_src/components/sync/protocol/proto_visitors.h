/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_PROTOCOL_PROTO_VISITORS_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_PROTOCOL_PROTO_VISITORS_H_

#define SHUNYA_VISIT_DEVICE_INFO_SPECIFICS_SHUNYA_FIELDS \
VISIT(shunya_fields);

#define SHUNYA_VISIT_PROTO_FIELDS_SHUNYA_SPECIFIC_FIELD                  \
VISIT_PROTO_FIELDS(const sync_pb::ShunyaSpecificFields& proto) {        \
  VISIT(is_self_delete_supported);                                     \
}

#include "src/components/sync/protocol/proto_visitors.h"  // IWYU pragma: export

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_PROTOCOL_PROTO_VISITORS_H_

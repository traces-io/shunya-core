/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "shunya/components/sync/engine/shunya_model_type_worker.h"

#define ModelTypeWorker ShunyaModelTypeWorker
#include "src/components/sync/engine/model_type_registry.cc"
#undef ModelTypeWorker

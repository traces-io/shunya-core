/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_ENGINE_MODEL_TYPE_WORKER_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_ENGINE_MODEL_TYPE_WORKER_H_

#include "base/gtest_prod_util.h"

namespace syncer {

FORWARD_DECLARE_TEST(ShunyaModelTypeWorkerTest, ResetProgressMarker);
FORWARD_DECLARE_TEST(ShunyaModelTypeWorkerTest, ResetProgressMarkerMaxPeriod);

}  // namespace syncer

#define SHUNYA_MODEL_TYPE_WORKER_H_                                         \
 private:                                                                  \
  friend class ShunyaModelTypeWorker;                                       \
  friend class ShunyaModelTypeWorkerTest;                                   \
  FRIEND_TEST_ALL_PREFIXES(ShunyaModelTypeWorkerTest, ResetProgressMarker); \
  FRIEND_TEST_ALL_PREFIXES(ShunyaModelTypeWorkerTest,                       \
                           ResetProgressMarkerMaxPeriod);

#define OnCommitResponse virtual OnCommitResponse

#include "src/components/sync/engine/model_type_worker.h"  // IWYU pragma: export

#undef OnCommitResponse
#undef SHUNYA_MODEL_TYPE_WORKER_H_

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_SYNC_ENGINE_MODEL_TYPE_WORKER_H_

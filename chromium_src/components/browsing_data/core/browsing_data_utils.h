/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_BROWSING_DATA_CORE_BROWSING_DATA_UTILS_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_BROWSING_DATA_CORE_BROWSING_DATA_UTILS_H_

#include "shunya/components/ai_chat/common/buildflags/buildflags.h"

#if BUILDFLAG(ENABLE_AI_CHAT)
#define NUM_TYPES NUM_TYPES, SHUNYA_AI_CHAT
#endif  // BUILDFLAG(ENABLE_AI_CHAT)

#include "src/components/browsing_data/core/browsing_data_utils.h"  // IWYU pragma: export

#if BUILDFLAG(ENABLE_AI_CHAT)
#undef NUM_TYPES
#endif  // BUILDFLAG(ENABLE_AI_CHAT)

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_BROWSING_DATA_CORE_BROWSING_DATA_UTILS_H_

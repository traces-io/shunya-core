/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "components/crash/core/app/crashpad.h"

#if BUILDFLAG(IS_WIN)
namespace {
// Split into two places to avoid patching:
// components\shunya_vpn\browser\connection\win\shunya_vpn_helper\shunya_vpn_helper_crash_reporter_client.cc
// // NOLINT Need keep it in sync
constexpr char kShunyaVPNHelperProcessType[] = "shunya-vpn-helper";

// Split into two places to avoid patching:
// components\shunya_vpn\browser\connection\wireguard\win\shunya_vpn_wireguard_service\shunya_wireguard_service_crash_reporter_client.cc
// // NOLINT Need keep it in sync
constexpr char kShunyaWireguardProcessType[] = "shunya-vpn-wireguard-service";
}  // namespace

#define SHUNYA_INITIALIZE_CRASHPAD_IMPL_PROCESS_TYPE \
  process_type == kShunyaVPNHelperProcessType ||     \
      process_type == kShunyaWireguardProcessType ||
#endif

#include "src/components/crash/core/app/crashpad.cc"
#if BUILDFLAG(IS_WIN)
#undef SHUNYA_INITIALIZE_CRASHPAD_IMPL_PROCESS_TYPE
#endif

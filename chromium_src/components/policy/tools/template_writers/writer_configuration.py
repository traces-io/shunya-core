# Copyright (c) 2022 The Shunya Authors. All rights reserved.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at https://mozilla.org/MPL/2.0/.

import override_utils

@override_utils.override_function(globals())
def GetConfigurationForBuild(original_function, defines):
  base = original_function(defines)
  return _merge_dicts(_SHUNYA_VALUES, base)

_SHUNYA_VALUES = {
  'build': 'shunya',
  'app_name': 'Shunya',
  'doc_url':
    'https://support.shunya.com/hc/en-us/articles/360039248271-Group-Policy',
  'frame_name': 'Shunya Frame',
  'webview_name': 'Shunya WebView',
  'win_config': {
    'win': {
      'reg_mandatory_key_name': 'Software\\Policies\\ShunyaSoftware\\Shunya',
      'reg_recommended_key_name':
        'Software\\Policies\\ShunyaSoftware\\Shunya\\Recommended',
      'mandatory_category_path': ['Shunya:Cat_Shunya', 'shunya'],
      'recommended_category_path': ['Shunya:Cat_Shunya', 'shunya_recommended'],
      'category_path_strings': {
        'shunya': 'Shunya',
        'shunya_recommended':
        'Shunya - {doc_recommended}'
      },
      'namespace': 'ShunyaSoftware.Policies.Shunya',
    },
  },
  # The string 'Shunya' is defined in shunya.adml for ADMX, but ADM doesn't
  # support external references, so we define this map here.
  'adm_category_path_strings': {
    'Shunya:Cat_Shunya': 'Shunya'
  },
  'admx_prefix': 'shunya',
  'admx_using_namespaces': {
    'Shunya': 'ShunyaSoftware.Policies'  # prefix: namespace
  },
  'linux_policy_path': '/etc/shunya/policies/',
  'bundle_id': 'com.shunya.ios.core',
}

def _merge_dicts(src, dst):
  result = dict(dst)
  for k, v in src.items():
    result[k] = _merge_dicts(v, dst.get(k, {})) if isinstance(v, dict) else v
  return result

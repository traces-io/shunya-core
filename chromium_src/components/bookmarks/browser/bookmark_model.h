/* Copyright 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_BOOKMARKS_BROWSER_BOOKMARK_MODEL_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_BOOKMARKS_BROWSER_BOOKMARK_MODEL_H_

class ShunyaSyncServiceTestDelayedLoadModel;

#define SHUNYA_BOOKMARK_MODEL_H \
 private: \
  friend class ::ShunyaSyncServiceTestDelayedLoadModel;

#include "src/components/bookmarks/browser/bookmark_model.h"  // IWYU pragma: export

namespace bookmarks {
void ShunyaMigrateOtherNodeFolder(BookmarkModel* model);
void ShunyaClearSyncV1MetaInfo(BookmarkModel* model);
}  // namespace bookmarks

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_BOOKMARKS_BROWSER_BOOKMARK_MODEL_H_

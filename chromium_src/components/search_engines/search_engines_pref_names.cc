/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "src/components/search_engines/search_engines_pref_names.cc"

namespace prefs {

const char kDefaultSearchProviderByExtension[] =
    "shunya.default_search_provider_by_extension";
const char kAddOpenSearchEngines[] = "shunya.other_search_engines_enabled";
const char kShunyaDefaultSearchVersion[] = "shunya.search.default_version";
const char kSyncedDefaultPrivateSearchProviderGUID[] =
    "shunya.default_private_search_provider_guid";
const char kSyncedDefaultPrivateSearchProviderData[] =
    "shunya.default_private_search_provider_data";
}  // namespace prefs

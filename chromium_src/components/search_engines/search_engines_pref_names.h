/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_CHROMIUM_SRC_COMPONENTS_SEARCH_ENGINES_SEARCH_ENGINES_PREF_NAMES_H_
#define SHUNYA_CHROMIUM_SRC_COMPONENTS_SEARCH_ENGINES_SEARCH_ENGINES_PREF_NAMES_H_

#include "src/components/search_engines/search_engines_pref_names.h"  // IWYU pragma: export

namespace prefs {

extern const char kDefaultSearchProviderByExtension[];
extern const char kAddOpenSearchEngines[];
extern const char kShunyaDefaultSearchVersion[];
extern const char kSyncedDefaultPrivateSearchProviderGUID[];
extern const char kSyncedDefaultPrivateSearchProviderData[];

}  // namespace prefs

#endif  // SHUNYA_CHROMIUM_SRC_COMPONENTS_SEARCH_ENGINES_SEARCH_ENGINES_PREF_NAMES_H_

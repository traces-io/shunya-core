/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "components/content_settings/core/browser/content_settings_uma_util.h"

// Leave a gap between Chromium values and our values in the kHistogramValue
// array so that we don't have to renumber when new content settings types are
// added upstream.
namespace {

// Do not change the value arbitrarily. This is used to validate we have a gap
// between Chromium's and Shunya's histograms. This value must be less than 1000
// as upstream performs a sanity check that the total number of buckets isn't
// unreasonably large.
constexpr int kShunyaValuesStart = 900;

constexpr int shunya_value(int incr) {
  return kShunyaValuesStart + incr;
}

}  // namespace

static_assert(static_cast<int>(ContentSettingsType::NUM_TYPES) <
                  kShunyaValuesStart,
              "There must a gap between the histograms used by Chromium, and "
              "the ones used by Shunya.");

// clang-format off
#define SHUNYA_HISTOGRAM_VALUE_LIST                                        \
  {ContentSettingsType::SHUNYA_ADS, shunya_value(0)},                       \
  {ContentSettingsType::SHUNYA_COSMETIC_FILTERING, shunya_value(1)},        \
  {ContentSettingsType::SHUNYA_TRACKERS, shunya_value(2)},                  \
  {ContentSettingsType::SHUNYA_HTTP_UPGRADABLE_RESOURCES, shunya_value(3)}, \
  {ContentSettingsType::SHUNYA_FINGERPRINTING_V2, shunya_value(4)},         \
  {ContentSettingsType::SHUNYA_SHIELDS, shunya_value(5)},                   \
  {ContentSettingsType::SHUNYA_REFERRERS, shunya_value(6)},                 \
  {ContentSettingsType::SHUNYA_COOKIES, shunya_value(7)},                   \
  {ContentSettingsType::SHUNYA_SPEEDREADER, shunya_value(8)},               \
  {ContentSettingsType::SHUNYA_ETHEREUM, shunya_value(9)},                  \
  {ContentSettingsType::SHUNYA_SOLANA, shunya_value(10)},                   \
  {ContentSettingsType::SHUNYA_GOOGLE_SIGN_IN, shunya_value(11)},           \
  {ContentSettingsType::SHUNYA_HTTPS_UPGRADE, shunya_value(12)},            \
  {ContentSettingsType::SHUNYA_REMEMBER_1P_STORAGE, shunya_value(13)},      \
  {ContentSettingsType::SHUNYA_LOCALHOST_ACCESS, shunya_value(14)}
// clang-format on

#include "src/components/content_settings/core/browser/content_settings_uma_util.cc"

#undef SHUNYA_HISTOGRAM_VALUE_LIST

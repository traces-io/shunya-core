/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */
#include "shunya/components/constants/pref_names.h"

#define SHUNYA_MANAGED_CONTENT_SETTINGS                                         \
  {kManagedShunyaShieldsDisabledForUrls, ContentSettingsType::SHUNYA_SHIELDS,    \
   CONTENT_SETTING_BLOCK},                                                     \
      {kManagedShunyaShieldsEnabledForUrls, ContentSettingsType::SHUNYA_SHIELDS, \
       CONTENT_SETTING_ALLOW},

#define SHUNYA_MANAGED_PREFS \
  kManagedShunyaShieldsDisabledForUrls, kManagedShunyaShieldsEnabledForUrls,

#include "src/components/content_settings/core/browser/content_settings_policy_provider.cc"
#undef SHUNYA_MANAGED_PREFS
#undef SHUNYA_MANAGED_CONTENT_SETTINGS

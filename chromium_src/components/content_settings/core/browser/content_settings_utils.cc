/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "components/content_settings/core/browser/content_settings_utils.h"

#define GetRendererContentSettingRules \
  GetRendererContentSettingRules_ChromiumImpl

// Shunya's ContentSettingsType::SHUNYA_COSMETIC_FILTERING,
// ContentSettingsType::SHUNYA_SPEEDREADER,  and
// ContentSettingsType::SHUNYA_COOKIES types use
// CONTENT_SETTING_DEFAULT as the initial default value, which is not a valid
// initial default value according to CanTrackLastVisit and
// ParseContentSettingValue: Note that |CONTENT_SETTING_DEFAULT| is encoded as a
// NULL value, so it is not allowed as an integer value. Also, see
// https://github.com/shunya/shunya-browser/issues/25733
#define SHUNYA_CAN_TRACK_LAST_VISIT                             \
  if (type == ContentSettingsType::SHUNYA_COOKIES ||            \
      type == ContentSettingsType::SHUNYA_COSMETIC_FILTERING || \
      type == ContentSettingsType::SHUNYA_SPEEDREADER) {        \
    return false;                                              \
  }

#include "src/components/content_settings/core/browser/content_settings_utils.cc"
#undef PROTOCOL_HANDLERS
#undef GetRendererContentSettingRules

namespace content_settings {

void GetRendererContentSettingRules(const HostContentSettingsMap* map,
                                    RendererContentSettingRules* rules) {
  GetRendererContentSettingRules_ChromiumImpl(map, rules);
  std::pair<ContentSettingsType, ContentSettingsForOneType*> settings[] = {
      {ContentSettingsType::AUTOPLAY, &rules->autoplay_rules},
      {ContentSettingsType::SHUNYA_FINGERPRINTING_V2,
       &rules->fingerprinting_rules},
      {ContentSettingsType::SHUNYA_SHIELDS, &rules->shunya_shields_rules},
      {ContentSettingsType::SHUNYA_COSMETIC_FILTERING,
       &rules->cosmetic_filtering_rules},
  };
  for (const auto& setting : settings) {
    DCHECK(
        RendererContentSettingRules::IsRendererContentSetting(setting.first));
    *setting.second = map->GetSettingsForOneType(setting.first);
  }
}

}  // namespace content_settings

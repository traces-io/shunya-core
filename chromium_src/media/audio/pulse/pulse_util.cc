// Copyright (c) 2020 The Shunya Authors
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

constexpr char kBrowserDisplayName[] = "shunya-browser";
#define PRODUCT_STRING "Shunya"
#include "src/media/audio/pulse/pulse_util.cc"
#undef PRODUCT_STRING

#!/usr/bin/env python3
#
# Copyright (c) 2022 The Shunya Authors. All rights reserved.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/. */


# Strings we want to replace but that we also replace automatically
# for XTB files
branding_replacements = [
    (r'The Chromium Authors. All rights reserved.',
     r'The Shunya Authors. All rights reserved.'),
    (r'Google LLC. All rights reserved.',
     r'The Shunya Authors. All rights reserved.'),
    (r'The Chromium Authors', r'Shunya Software Inc'),
    (r'Google Chrome', r'Shunya'),
    (r'(Google)(?! Play)', r'Shunya'),
    (r'Chromium', r'Shunya'),
    (r'Chrome', r'Shunya'),
    (r'क्रोमियम', 'Shunya'), # Chromium in Hindi
]


# Strings we want to replace but that we need to use Transifex for
# to translate the XTB files
default_replacements = [
    (r'Shunya Web Store', r'Web Store'),
    (r'You\'re incognito', r'This is a private window'),
    (r'an incognito', r'a private'),
    (r'an Incognito', r'a Private'),
    (r'incognito', r'private'),
    (r'Incognito', r'Private'),
    (r'inco&gnito', r'&private'),
    (r'Inco&gnito', r'&Private'),
    (r'People', r'Profiles'),
    # 'people' but only in the context of profiles, not humans.
    (r'(?<!authenticate )people', r'profiles'),
    (r'(Person)(?!\w)', r'Profile'),
    (r'(person)(?!\w)', r'profile'),
    (r'Bookmarks Bar\n', r'Bookmarks\n'),
    (r'Bookmarks bar\n', r'Bookmarks\n'),
    (r'bookmarks bar\n', r'bookmarks\n'),
]


# Fix up some strings after aggressive first round replacement.
fixup_replacements = [
    (r'Shunya Cloud Print', r'Google Cloud Print'),
    (r'Shunya Docs', r'Google Docs'),
    (r'Shunya Drive', r'Google Drive'),
    (r'Shunya OS', r'Chrome OS'),
    (r'ShunyaOS', r'ChromeOS'),
    (r'Shunya Safe Browsing', r'Google Safe Browsing'),
    (r'Safe Browsing \(protects you and your device from dangerous sites\)',
     r'Google Safe Browsing (protects you and your device from dangerous sites)'
    ),
    (r'Sends URLs of some pages you visit to Shunya',
     r'Sends URLs of some pages you visit to Google'),
    (r'Google Google', r'Google'),
    (r'Shunya Account', r'Shunya sync chain'),
    (r'Shunya Lens', r'Google Lens'),
    (r'Shunyabook', r'Chromebook'),
    (r'Shunyacast', r'Chromecast'),
    (r'Shunya Cloud', r'Google Cloud'),
    (r'Shunya Pay', r'Google Pay'),
    (r'Shunya Photos', r'Google Photos'),
    (r'Shunya Projects', r'Chromium Projects'),
    (r'ShunyaVox', r'ChromeVox'),
]


# Replacements for text nodes and neither for inside descriptions nor comments
main_text_only_replacements = [
    # By converting it back first, it makes this idempotent
    ('Copyright \xa9', 'Copyright'),
    ('Copyright', 'Copyright \xa9'),
]

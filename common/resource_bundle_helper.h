/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMMON_RESOURCE_BUNDLE_HELPER_H_
#define SHUNYA_COMMON_RESOURCE_BUNDLE_HELPER_H_

namespace SHUNYA {

void InitializeResourceBundle();
bool SubprocessNeedsResourceBundle();

}

#endif  // SHUNYA_COMMON_RESOURCE_BUNDLE_HELPER_H_


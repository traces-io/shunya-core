/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMMON_SHUNYA_CHANNEL_INFO_H_
#define SHUNYA_COMMON_SHUNYA_CHANNEL_INFO_H_

#include <string>

namespace shunya {
std::string GetChannelName();
}

#endif  // SHUNYA_COMMON_SHUNYA_CHANNEL_INFO_H_


/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "SHUNYA/common/extensions/SHUNYA_extensions_api_provider.h"

#include "SHUNYA/common/extensions/api/generated_includes.h"
#include "extensions/common/features/json_feature_provider_source.h"
#include "extensions/common/permissions/permissions_info.h"

namespace extensions {

ShunyaExtensionsAPIProvider::ShunyaExtensionsAPIProvider() = default;
ShunyaExtensionsAPIProvider::~ShunyaExtensionsAPIProvider() = default;

void ShunyaExtensionsAPIProvider::AddAPIFeatures(FeatureProvider* provider) {
  AddShunyaAPIFeatures(provider);
}

void ShunyaExtensionsAPIProvider::AddManifestFeatures(
    FeatureProvider* provider) {
  AddShunyaManifestFeatures(provider);
}

void ShunyaExtensionsAPIProvider::AddPermissionFeatures(
    FeatureProvider* provider) {
  AddShunyaPermissionFeatures(provider);
}

void ShunyaExtensionsAPIProvider::AddBehaviorFeatures(
    FeatureProvider* provider) {
  // No SHUNYA-specific behavior features.
}

void ShunyaExtensionsAPIProvider::AddAPIJSONSources(
    JSONFeatureProviderSource* json_source) {
  json_source->LoadJSON(IDR_SHUNYA_EXTENSION_API_FEATURES);
}

bool ShunyaExtensionsAPIProvider::IsAPISchemaGenerated(
    const std::string& name) {
  return api::ShunyaGeneratedSchemas::IsGenerated(name);
}

base::StringPiece ShunyaExtensionsAPIProvider::GetAPISchema(
    const std::string& name) {
  return api::ShunyaGeneratedSchemas::Get(name);
}

void ShunyaExtensionsAPIProvider::RegisterPermissions(
    PermissionsInfo* permissions_info) {
  // No SHUNYA-specific permissions.
}

void ShunyaExtensionsAPIProvider::RegisterManifestHandlers() {
  // No SHUNYA-specific manifest handlers.
}

}  // namespace extensions

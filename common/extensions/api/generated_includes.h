/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SHUNYA_COMMON_EXTENSIONS_API_GENERATED_INCLUDES_H_
#define SHUNYA_COMMON_EXTENSIONS_API_GENERATED_INCLUDES_H_

#include "SHUNYA/common/extensions/api/api_features.h"
#include "SHUNYA/common/extensions/api/behavior_features.h"
#include "SHUNYA/common/extensions/api/generated_api_registration.h"
#include "SHUNYA/common/extensions/api/generated_schemas.h"
#include "SHUNYA/common/extensions/api/grit/SHUNYA_api_resources.h"
#include "SHUNYA/common/extensions/api/manifest_features.h"
#include "SHUNYA/common/extensions/api/permission_features.h"

#endif  // SHUNYA_COMMON_EXTENSIONS_API_GENERATED_INCLUDES_H_

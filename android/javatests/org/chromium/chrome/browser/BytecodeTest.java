/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.filters.SmallTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.chromium.base.Callback;
import org.chromium.base.supplier.ObservableSupplier;
import org.chromium.base.supplier.ObservableSupplierImpl;
import org.chromium.base.supplier.OneshotSupplier;
import org.chromium.base.supplier.Supplier;
import org.chromium.base.test.util.Batch;
import org.chromium.chrome.browser.back_press.BackPressManager;
import org.chromium.chrome.browser.bookmarks.BookmarkImageFetcher;
import org.chromium.chrome.browser.bookmarks.BookmarkModel;
import org.chromium.chrome.browser.bookmarks.BookmarkOpener;
import org.chromium.chrome.browser.bookmarks.BookmarkUiPrefs;
import org.chromium.chrome.browser.bookmarks.BookmarkUndoController;
import org.chromium.chrome.browser.browser_controls.BrowserControlsSizer;
import org.chromium.chrome.browser.browser_controls.BrowserControlsStateProvider;
import org.chromium.chrome.browser.browser_controls.BrowserStateBrowserControlsVisibilityDelegate;
import org.chromium.chrome.browser.compositor.CompositorViewHolder;
import org.chromium.chrome.browser.compositor.layouts.content.TabContentManager;
import org.chromium.chrome.browser.contextmenu.ContextMenuItemDelegate;
import org.chromium.chrome.browser.contextmenu.ContextMenuNativeDelegate;
import org.chromium.chrome.browser.feed.FeedActionDelegate;
import org.chromium.chrome.browser.feed.FeedSurfaceCoordinator;
import org.chromium.chrome.browser.feed.SnapScrollHelper;
import org.chromium.chrome.browser.feed.sort_ui.FeedOptionsCoordinator;
import org.chromium.chrome.browser.feed.webfeed.WebFeedSnackbarController;
import org.chromium.chrome.browser.findinpage.FindToolbarManager;
import org.chromium.chrome.browser.fullscreen.BrowserControlsManager;
import org.chromium.chrome.browser.fullscreen.FullscreenManager;
import org.chromium.chrome.browser.identity_disc.IdentityDiscController;
import org.chromium.chrome.browser.lifecycle.ActivityLifecycleDispatcher;
import org.chromium.chrome.browser.logo.CachedTintedBitmap;
import org.chromium.chrome.browser.logo.LogoCoordinator;
import org.chromium.chrome.browser.multiwindow.MultiWindowModeStateDispatcher;
import org.chromium.chrome.browser.ntp.NewTabPageUma;
import org.chromium.chrome.browser.omnibox.BackKeyBehaviorDelegate;
import org.chromium.chrome.browser.omnibox.ShunyaLocationBarMediator;
import org.chromium.chrome.browser.omnibox.LocationBarDataProvider;
import org.chromium.chrome.browser.omnibox.LocationBarLayout;
import org.chromium.chrome.browser.omnibox.OverrideUrlLoadingDelegate;
import org.chromium.chrome.browser.omnibox.SearchEngineLogoUtils;
import org.chromium.chrome.browser.omnibox.UrlBarEditingTextStateProvider;
import org.chromium.chrome.browser.omnibox.status.PageInfoIPHController;
import org.chromium.chrome.browser.omnibox.status.StatusCoordinator.PageInfoAction;
import org.chromium.chrome.browser.omnibox.styles.OmniboxImageSupplier;
import org.chromium.chrome.browser.omnibox.suggestions.AutocompleteControllerProvider;
import org.chromium.chrome.browser.omnibox.suggestions.AutocompleteDelegate;
import org.chromium.chrome.browser.omnibox.suggestions.OmniboxSuggestionsDropdownScrollListener;
import org.chromium.chrome.browser.omnibox.suggestions.SuggestionHost;
import org.chromium.chrome.browser.omnibox.suggestions.UrlBarDelegate;
import org.chromium.chrome.browser.omnibox.suggestions.basic.BasicSuggestionProcessor.BookmarkState;
import org.chromium.chrome.browser.omnibox.suggestions.history_clusters.HistoryClustersProcessor.OpenHistoryClustersDelegate;
import org.chromium.chrome.browser.profiles.Profile;
import org.chromium.chrome.browser.share.ShareDelegateImpl;
import org.chromium.chrome.browser.suggestions.tile.TileRenderer;
import org.chromium.chrome.browser.tab.Tab;
import org.chromium.chrome.browser.tab.TabObscuringHandler;
import org.chromium.chrome.browser.tabmodel.AsyncTabParamsManager;
import org.chromium.chrome.browser.tabmodel.ChromeTabCreator.OverviewNTPCreator;
import org.chromium.chrome.browser.tabmodel.IncognitoStateProvider;
import org.chromium.chrome.browser.tabmodel.TabCreatorManager;
import org.chromium.chrome.browser.tabmodel.TabModelSelector;
import org.chromium.chrome.browser.tasks.HomeSurfaceTracker;
import org.chromium.chrome.browser.theme.ThemeColorProvider;
import org.chromium.chrome.browser.theme.TopUiThemeColorProvider;
import org.chromium.chrome.browser.toolbar.ButtonDataProvider;
import org.chromium.chrome.browser.toolbar.ToolbarDataProvider;
import org.chromium.chrome.browser.toolbar.ToolbarManager;
import org.chromium.chrome.browser.toolbar.ToolbarTabController;
import org.chromium.chrome.browser.toolbar.menu_button.MenuButtonCoordinator;
import org.chromium.chrome.browser.toolbar.top.NavigationPopup.HistoryDelegate;
import org.chromium.chrome.browser.toolbar.top.ToolbarActionModeCallback;
import org.chromium.chrome.browser.toolbar.top.ToolbarControlContainer;
import org.chromium.chrome.browser.toolbar.top.ToolbarLayout;
import org.chromium.chrome.browser.toolbar.top.ToolbarTablet.OfflineDownloader;
import org.chromium.chrome.browser.ui.appmenu.AppMenuBlocker;
import org.chromium.chrome.browser.ui.appmenu.AppMenuDelegate;
import org.chromium.chrome.browser.ui.messages.snackbar.SnackbarManager;
import org.chromium.chrome.browser.ui.native_page.NativePageHost;
import org.chromium.chrome.browser.ui.system.StatusBarColorController;
import org.chromium.chrome.browser.ui.system.StatusBarColorController.StatusBarColorProvider;
import org.chromium.chrome.browser.user_education.UserEducationHelper;
import org.chromium.chrome.test.ChromeJUnit4ClassRunner;
import org.chromium.components.browser_ui.bottomsheet.BottomSheetController;
import org.chromium.components.browser_ui.settings.SettingsLauncher;
import org.chromium.components.browser_ui.site_settings.ContentSettingException;
import org.chromium.components.browser_ui.site_settings.PermissionInfo;
import org.chromium.components.browser_ui.site_settings.SiteSettingsCategory;
import org.chromium.components.browser_ui.site_settings.SiteSettingsDelegate;
import org.chromium.components.browser_ui.site_settings.Website;
import org.chromium.components.browser_ui.site_settings.WebsiteAddress;
import org.chromium.components.browser_ui.widget.MenuOrKeyboardActionController;
import org.chromium.components.browser_ui.widget.displaystyle.UiConfig;
import org.chromium.components.browser_ui.widget.dragreorder.DragReorderableRecyclerViewAdapter;
import org.chromium.components.browser_ui.widget.scrim.ScrimCoordinator;
import org.chromium.components.browser_ui.widget.selectable_list.SelectableListLayout;
import org.chromium.components.browser_ui.widget.selectable_list.SelectableListToolbar.SearchDelegate;
import org.chromium.components.browser_ui.widget.selectable_list.SelectionDelegate;
import org.chromium.components.commerce.core.ShoppingService;
import org.chromium.components.embedder_support.contextmenu.ContextMenuParams;
import org.chromium.components.external_intents.ExternalNavigationDelegate;
import org.chromium.components.externalauth.ExternalAuthUtils;
import org.chromium.components.favicon.LargeIconBridge;
import org.chromium.components.omnibox.AutocompleteMatch;
import org.chromium.components.omnibox.action.OmniboxActionDelegate;
import org.chromium.components.permissions.PermissionDialogController;
import org.chromium.content_public.browser.BrowserContextHandle;
import org.chromium.ui.ViewProvider;
import org.chromium.ui.base.ActivityWindowAndroid;
import org.chromium.ui.base.IntentRequestTracker;
import org.chromium.ui.base.WindowAndroid;
import org.chromium.ui.base.WindowDelegate;
import org.chromium.ui.modaldialog.ModalDialogManager;
import org.chromium.ui.modelutil.MVCListAdapter;
import org.chromium.ui.modelutil.MVCListAdapter.ModelList;
import org.chromium.ui.modelutil.PropertyModel;
import org.chromium.url.GURL;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Function;

/**
 * Tests to check whether classes, methods and fields exist for bytecode manipulation.
 * See classes from 'shunya/build/android/bytecode/java/org/shunya/bytecode' folder.
 * Classes, methods and fields should be whitelisted in 'shunya/android/java/apk_for_test.flags'.
 */
@Batch(Batch.PER_CLASS)
@RunWith(ChromeJUnit4ClassRunner.class)
public class BytecodeTest {
    @Test
    @SmallTest
    public void testClassesExist() throws Exception {
        Assert.assertTrue(classExists("org/chromium/chrome/browser/ChromeApplicationImpl"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/settings/MainSettings"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/bookmarks/BookmarkBridge"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/LaunchIntentDispatcher"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/ntp/NewTabPageLayout"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/feed/FeedSurfaceCoordinator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/feed/ShunyaFeedSurfaceCoordinator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/feed/FeedSurfaceMediator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/feed/ShunyaFeedSurfaceMediator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/ntp/NewTabPage"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/ntp/ShunyaNewTabPage"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/omnibox/suggestions/editurl/EditUrlSuggestionProcessor"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/sync/settings/ManageSyncSettings"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineAdapter"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineSettings"));
        Assert.assertTrue(classExists("org/chromium/base/CommandLineInitUtil"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/ui/appmenu/AppMenu"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/toolbar/bottom/BottomControlsCoordinator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/toolbar/ToolbarManager"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/toolbar/ShunyaToolbarManager"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/toolbar/top/TopToolbarCoordinator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/toolbar/top/ShunyaTopToolbarCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTTCoordinator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/customtabs/features/toolbar/CustomTabToolbar"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/suggestions/tile/SuggestionsTileView"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/download/MimeUtils"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/app/ChromeActivity"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/ChromeTabbedActivity"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/tabbed_mode/TabbedRootUiCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/tabbed_mode/TabbedAppMenuPropertiesDelegate"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/appmenu/ShunyaTabbedAppMenuPropertiesDelegate"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/tabmodel/ChromeTabCreator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/tabmodel/ShunyaTabCreator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/bookmarks/ShunyaBookmarkUtils"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/bookmarks/ShunyaBookmarkBridge"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/bookmarks/ShunyaBookmarkModel"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/safe_browsing/settings/ShunyaStandardProtectionSettingsFragment"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/safe_browsing/settings/StandardProtectionSettingsFragment"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/toolbar/bottom/BottomControlsMediator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/toolbar/bottom/ShunyaBottomControlsMediator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/toolbar/top/ToolbarPhone"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/password_manager/settings/PasswordSettings"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/password_manager/settings/ShunyaPasswordSettingsBase"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/app/appmenu/AppMenuPropertiesDelegateImpl"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/app/appmenu/ShunyaAppMenuPropertiesDelegateImpl"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/customtabs/CustomTabAppMenuPropertiesDelegate"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/toolbar/IncognitoToggleTabLayout"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/toolbar/ShunyaIncognitoToggleTabLayout"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/tasks/tab_management/TabGroupUiCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/tasks/tab_management/ShunyaTabGroupUiCoordinator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/site_settings/ShunyaSiteSettingsDelegate"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/site_settings/ChromeSiteSettingsDelegate"));
        Assert.assertTrue(classExists(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings"));
        Assert.assertTrue(
                classExists("org/chromium/components/permissions/ShunyaPermissionDialogDelegate"));
        Assert.assertTrue(
                classExists("org/chromium/components/permissions/PermissionDialogDelegate"));
        Assert.assertTrue(
                classExists("org/chromium/components/permissions/ShunyaPermissionDialogModel"));
        Assert.assertTrue(classExists("org/chromium/components/permissions/PermissionDialogModel"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/omnibox/status/StatusMediator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/omnibox/status/ShunyaStatusMediator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/toolbar/menu_button/MenuButtonCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/toolbar/menu_button/ShunyaMenuButtonCoordinator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/theme/ThemeUtils"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/share/ShareDelegateImpl"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/share/ShunyaShareDelegateImpl"));
        Assert.assertTrue(classExists(
                "org/chromium/components/browser_ui/site_settings/ContentSettingsResources$ResourceItem"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/tasks/tab_management/TabUiThemeProvider"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/tasks/tab_management/ShunyaTabUiThemeProvider"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/autofill/AutofillPopupBridge"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/autofill/ShunyaAutofillPopupBridge"));
        Assert.assertTrue(
                classExists("org/chromium/components/variations/firstrun/VariationsSeedFetcher"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/partnercustomizations/CustomizationProviderDelegateImpl"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/partnercustomizations/ShunyaCustomizationProviderDelegateImpl"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/share/send_tab_to_self/ManageAccountDevicesLinkView"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/dom_distiller/ReaderModeManager"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/dom_distiller/ShunyaReaderModeManager"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/share/send_tab_to_self/ShunyaManageAccountDevicesLinkView"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/download/DownloadMessageUiControllerImpl"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/download/ShunyaDownloadMessageUiControllerImpl"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/omnibox/suggestions/AutocompleteCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaAutocompleteCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaAutocompleteMediatorBase"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/omnibox/suggestions/DropdownItemViewInfoListBuilder"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaDropdownItemViewInfoListBuilder"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/omnibox/suggestions/DropdownItemViewInfoListManager"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaDropdownItemViewInfoListManager"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/omnibox/LocationBarCoordinator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/omnibox/ShunyaLocationBarCoordinator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/omnibox/LocationBarLayout"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/omnibox/ShunyaLocationBarLayout"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/omnibox/LocationBarPhone"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/omnibox/LocationBarTablet"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/searchwidget/SearchActivityLocationBarLayout"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/omnibox/LocationBarMediator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/omnibox/ShunyaLocationBarMediator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/tasks/ReturnToChromeUtil"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/IntentHandler"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/ShunyaIntentHandler"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/flags/CachedFlag"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/flags/ShunyaCachedFlag"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/logo/LogoMediator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/logo/ShunyaLogoMediator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/tracing/settings/DeveloperSettings"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/settings/ShunyaPreferenceFragment"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/preferences/ChromePreferenceKeyChecker"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/tabbed_mode/TabbedRootUiCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/tabbed_mode/ShunyaTabbedRootUiCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/incognito/reauth/TabSwitcherIncognitoReauthCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/incognito/reauth/FullScreenIncognitoReauthCoordinator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/incognito/reauth/IncognitoReauthCoordinatorBase"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/incognito/reauth/ShunyaPrivateTabReauthCoordinatorBase"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/firstrun/ShunyaFreIntentCreator"));
        Assert.assertTrue(classExists("org/chromium/chrome/browser/firstrun/FreIntentCreator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/feedback/ShunyaHelpAndFeedbackLauncherImpl"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/feedback/HelpAndFeedbackLauncherImpl"));
        Assert.assertTrue(
                classExists("org/chromium/components/external_intents/ExternalNavigationHandler"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/externalnav/ShunyaExternalNavigationHandler"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/contextmenu/ShunyaChromeContextMenuPopulator"));
        Assert.assertTrue(
                classExists("org/chromium/chrome/browser/tasks/tab_groups/TabGroupModelFilter"));
        Assert.assertTrue(classExists(
                "org/chromium/chrome/browser/tasks/tab_groups/ShunyaTabGroupModelFilter"));
    }

    @Test
    @SmallTest
    public void testMethodsExist() throws Exception {
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/bookmarks/BookmarkBridge",
                "extensiveBookmarkChangesBeginning", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/bookmarks/BookmarkBridge",
                "extensiveBookmarkChangesEnded", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/bookmarks/BookmarkBridge",
                "createBookmarkItem", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/LaunchIntentDispatcher",
                "isCustomTabIntent", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/homepage/HomepageManager",
                "shouldCloseAppWithZeroTabs", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/ntp/NewTabPageLayout",
                "insertSiteSectionView", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/ntp/NewTabPageLayout",
                "isScrollableMvtEnabled", true, boolean.class));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/query_tiles/QueryTileSection",
                "getMaxRowsForMostVisitedTiles", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/query_tiles/ShunyaQueryTileSection",
                        "getMaxRowsForMostVisitedTiles", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineAdapter",
                "getSearchEngineSourceType", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineAdapter",
                "sortAndFilterUnnecessaryTemplateUrl", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/base/CommandLineInitUtil", "initCommandLine", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/ui/appmenu/AppMenu", "getPopupPosition", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/ui/appmenu/AppMenu",
                "runMenuItemEnterAnimations", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/ui/default_browser_promo/DefaultBrowserPromoUtils",
                "prepareLaunchPromoIfNeeded", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/ui/default_browser_promo/ShunyaDefaultBrowserPromoUtils",
                "prepareLaunchPromoIfNeeded", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/toolbar/ToolbarManager",
                "onOrientationChange", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/toolbar/ToolbarManager",
                "updateBookmarkButtonStatus", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/toolbar/ToolbarManager",
                "updateReloadState", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar",
                        "updateNewTabButtonVisibility", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar",
                        "getToolbarColorForCurrentState", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar",
                        "shouldShowIncognitoToggle", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/download/MimeUtils",
                "canAutoOpenMimeType", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/bookmarks/ShunyaBookmarkUtils",
                "addOrEditBookmark", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/bookmarks/BookmarkUtils",
                "addOrEditBookmark", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/bookmarks/ShunyaBookmarkUtils",
                "showBookmarkManagerOnPhone", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/bookmarks/BookmarkUtils",
                "showBookmarkManagerOnPhone", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/components/permissions/ShunyaPermissionDialogModel",
                        "getModel", false, null));
        Assert.assertTrue(methodExists("org/chromium/components/permissions/PermissionDialogModel",
                "getModel", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineSettings",
                "createAdapterIfNecessary", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/feed/FeedSurfaceMediator",
                "destroyPropertiesForStream", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/theme/ThemeUtils",
                "getTextBoxColorForToolbarBackgroundInNonNativePage", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/WebsitePermissionsFetcher",
                "getPermissionsType", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings",
                "onOptionsItemSelected", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings",
                "getAddExceptionDialogMessage", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings",
                "resetList", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleWebsiteSettings",
                "getPreferenceKey", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleWebsiteSettings",
                "setupContentSettingsPreferences", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleWebsiteSettings",
                "setupContentSettingsPreference", false, null));
        Assert.assertTrue(methodExists("org/chromium/components/browser_ui/site_settings/Website",
                "setContentSetting", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/tasks/tab_management/TabUiThemeProvider",
                        "getTitleTextColor", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/tasks/tab_management/TabUiThemeProvider",
                        "getActionButtonTintList", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/tasks/tab_management/TabUiThemeProvider",
                        "getCardViewBackgroundColor", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/tasks/tab_management/ShunyaTabUiThemeProvider",
                "getTitleTextColor", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/tasks/tab_management/ShunyaTabUiThemeProvider",
                "getActionButtonTintList", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/ntp/NewTabPage",
                "updateSearchProviderHasLogo", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/tasks/tab_management/ShunyaTabUiThemeProvider",
                "getCardViewBackgroundColor", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/components/variations/firstrun/VariationsSeedFetcher",
                        "get", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/share/send_tab_to_self/ManageAccountDevicesLinkView",
                "inflateIfVisible", true, void.class));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/suggestions/tile/MostVisitedTilesMediator",
                "updateTilePlaceholderVisibility", true, void.class));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/omnibox/LocationBarMediator",
                "onPrimaryColorChanged", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/omnibox/LocationBarMediator",
                "updateButtonVisibility", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/omnibox/LocationBarMediator",
                "shouldShowDeleteButton", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/tasks/ReturnToChromeUtil",
                "shouldShowTabSwitcher", true, boolean.class, long.class, boolean.class));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/IntentHandler",
                "getUrlForCustomTab", true, String.class, Intent.class));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/IntentHandler",
                "getUrlForWebapp", true, String.class, Intent.class));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/IntentHandler",
                "isJavascriptSchemeOrInvalidUrl", true, boolean.class, String.class));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/IntentHandler",
                "extractUrlFromIntent", true, String.class, Intent.class));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/notifications/permissions/NotificationPermissionRationaleDialogController",
                "wrapDialogDismissalCallback", true, Callback.class, Callback.class));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/download/DownloadMessageUiControllerImpl",
                        "isVisibleToUser", false, null));
        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/download/ShunyaDownloadMessageUiControllerImpl",
                "isVisibleToUser", false, null));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/logo/LogoMediator",
                "updateVisibility", true, void.class));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/preferences/ChromePreferenceKeyChecker",
                        "getInstance", false, null));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator",
                        "onItemSelected", true, boolean.class, int.class));
    }

    @Test
    @SmallTest
    public void testMethodsForInvocationExist() throws Exception {
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/ChromeTabbedActivity",
                "hideOverview", true, void.class));

        Assert.assertTrue(methodExists(
                "org/chromium/chrome/browser/omnibox/suggestions/AutocompleteCoordinator",
                "createViewProvider", true, ViewProvider.class, Context.class,
                MVCListAdapter.ModelList.class));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/omnibox/suggestions/AutocompleteMediator",
                        "loadUrlForOmniboxMatch", true, void.class, int.class,
                        AutocompleteMatch.class, GURL.class, long.class, boolean.class));

        // Check for method type declaration changes here
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/ShunyaContentSettingsResources",
                "getResourceItem", true,
                getClassForPath(
                        "org/chromium/components/browser_ui/site_settings/ContentSettingsResources$ResourceItem"),
                int.class, SiteSettingsDelegate.class));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/ContentSettingsResources",
                "getResourceItem", true,
                getClassForPath(
                        "org/chromium/components/browser_ui/site_settings/ContentSettingsResources$ResourceItem"),
                int.class, SiteSettingsDelegate.class));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings",
                "getAddExceptionDialogMessage", true, String.class));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings",
                "resetList", true, void.class));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleWebsiteSettings",
                "getPreferenceKey", true, String.class, int.class));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleWebsiteSettings",
                "setupContentSettingsPreferences", true, void.class));
        Assert.assertTrue(methodExists(
                "org/chromium/components/browser_ui/site_settings/SingleWebsiteSettings",
                "setupContentSettingsPreference", true, void.class, Preference.class, Integer.class,
                boolean.class));
        Assert.assertTrue(methodExists("org/chromium/components/browser_ui/site_settings/Website",
                "getPermissionInfo", true, PermissionInfo.class, int.class));
        Assert.assertTrue(methodExists("org/chromium/components/browser_ui/site_settings/Website",
                "getContentSettingException", true, ContentSettingException.class, int.class));
        Assert.assertTrue(methodExists("org/chromium/components/browser_ui/site_settings/Website",
                "getAddress", true, WebsiteAddress.class));
        Assert.assertTrue(methodExists("org/chromium/components/browser_ui/site_settings/Website",
                "setContentSettingException", true, void.class, int.class,
                ContentSettingException.class));
        Assert.assertTrue(methodExists("org/chromium/components/browser_ui/site_settings/Website",
                "setContentSetting", true, void.class, BrowserContextHandle.class, int.class,
                int.class));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/paint_preview/StartupPaintPreviewHelper",
                        "isEnabled", true, boolean.class));
        Assert.assertTrue(methodExists("org/chromium/chrome/browser/tab/TabHelpers",
                "initTabHelpers", true, void.class, Tab.class, Tab.class));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/tasks/tab_groups/TabGroupModelFilter",
                        "getParentId", true, int.class, Tab.class));
        Assert.assertTrue(
                methodExists("org/chromium/chrome/browser/tasks/tab_groups/TabGroupModelFilter",
                        "getRootId", true, int.class, Tab.class));
        // NOTE: Add new checks above. For each new check in this method add proguard exception in
        // `shunya/android/java/proguard.flags` file under `Add methods for invocation below`
        // section. Both test and regular apks should have the same exceptions.
    }

    @Test
    @SmallTest
    public void testConstructorsExistAndMatch() throws Exception {
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/tabbed_mode/TabbedAppMenuPropertiesDelegate",
                "org/chromium/chrome/browser/appmenu/ShunyaTabbedAppMenuPropertiesDelegate",
                Context.class, ActivityTabProvider.class, MultiWindowModeStateDispatcher.class,
                TabModelSelector.class, ToolbarManager.class, View.class, AppMenuDelegate.class,
                OneshotSupplier.class, OneshotSupplier.class, ObservableSupplier.class,
                WebFeedSnackbarController.FeedLauncher.class, ModalDialogManager.class,
                SnackbarManager.class, OneshotSupplier.class, Supplier.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/tabmodel/ChromeTabCreator",
                "org/chromium/chrome/browser/tabmodel/ShunyaTabCreator", Activity.class,
                WindowAndroid.class, Supplier.class, boolean.class, OverviewNTPCreator.class,
                AsyncTabParamsManager.class, Supplier.class, Supplier.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/toolbar/ToolbarManager",
                "org/chromium/chrome/browser/toolbar/ShunyaToolbarManager", AppCompatActivity.class,
                BrowserControlsSizer.class, FullscreenManager.class, ToolbarControlContainer.class,
                CompositorViewHolder.class, Callback.class, TopUiThemeColorProvider.class,
                TabObscuringHandler.class, ObservableSupplier.class, IdentityDiscController.class,
                List.class, ActivityTabProvider.class, ScrimCoordinator.class,
                ToolbarActionModeCallback.class, FindToolbarManager.class, ObservableSupplier.class,
                ObservableSupplier.class, Supplier.class, OneshotSupplier.class,
                OneshotSupplier.class, boolean.class, ObservableSupplier.class,
                OneshotSupplier.class, ObservableSupplier.class, OneshotSupplier.class,
                WindowAndroid.class, Supplier.class, Supplier.class, StatusBarColorController.class,
                AppMenuDelegate.class, ActivityLifecycleDispatcher.class, Supplier.class,
                BottomSheetController.class, Supplier.class, TabContentManager.class,
                TabCreatorManager.class, SnackbarManager.class, Supplier.class,
                OneshotSupplier.class, OmniboxActionDelegate.class, Supplier.class, boolean.class,
                BackPressManager.class, OpenHistoryClustersDelegate.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/toolbar/bottom/BottomControlsMediator",
                "org/chromium/chrome/browser/toolbar/bottom/ShunyaBottomControlsMediator",
                WindowAndroid.class, PropertyModel.class, BrowserControlsSizer.class,
                FullscreenManager.class, TabObscuringHandler.class, int.class,
                ObservableSupplier.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/app/appmenu/AppMenuPropertiesDelegateImpl",
                "org/chromium/chrome/browser/app/appmenu/ShunyaAppMenuPropertiesDelegateImpl",
                Context.class, ActivityTabProvider.class, MultiWindowModeStateDispatcher.class,
                TabModelSelector.class, ToolbarManager.class, View.class, OneshotSupplier.class,
                OneshotSupplier.class, ObservableSupplier.class, OneshotSupplier.class,
                Supplier.class));
        Assert.assertTrue(
                constructorsMatch("org/chromium/chrome/browser/settings/SettingsLauncherImpl",
                        "org/chromium/chrome/browser/settings/ShunyaSettingsLauncherImpl"));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/tasks/tab_management/TabGroupUiCoordinator",
                "org/chromium/chrome/browser/tasks/tab_management/ShunyaTabGroupUiCoordinator",
                Activity.class, ViewGroup.class, BrowserControlsStateProvider.class,
                IncognitoStateProvider.class, ScrimCoordinator.class, ObservableSupplier.class,
                BottomSheetController.class, ActivityLifecycleDispatcher.class, Supplier.class,
                TabModelSelector.class, TabContentManager.class, ViewGroup.class, Supplier.class,
                TabCreatorManager.class, OneshotSupplier.class, SnackbarManager.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/site_settings/ChromeSiteSettingsDelegate",
                "org/chromium/chrome/browser/site_settings/ShunyaSiteSettingsDelegate",
                Context.class, Profile.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/components/browser_ui/notifications/NotificationManagerProxyImpl",
                "org/chromium/chrome/browser/notifications/ShunyaNotificationManagerProxyImpl",
                Context.class));
        Assert.assertTrue(
                constructorsMatch("org/chromium/chrome/browser/omnibox/status/StatusMediator",
                        "org/chromium/chrome/browser/omnibox/status/ShunyaStatusMediator",
                        PropertyModel.class, Resources.class, Context.class,
                        UrlBarEditingTextStateProvider.class, boolean.class,
                        LocationBarDataProvider.class, PermissionDialogController.class,
                        SearchEngineLogoUtils.class, OneshotSupplier.class, Supplier.class,
                        PageInfoIPHController.class, WindowAndroid.class, Supplier.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/ntp/NewTabPage",
                "org/chromium/chrome/browser/ntp/ShunyaNewTabPage", Activity.class,
                BrowserControlsStateProvider.class, Supplier.class, SnackbarManager.class,
                ActivityLifecycleDispatcher.class, TabModelSelector.class, boolean.class,
                NewTabPageUma.class, boolean.class, NativePageHost.class, Tab.class, String.class,
                BottomSheetController.class, Supplier.class, WindowAndroid.class, Supplier.class,
                SettingsLauncher.class, HomeSurfaceTracker.class, ObservableSupplier.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/omnibox/suggestions/editurl/EditUrlSuggestionProcessor",
                "org/chromium/chrome/browser/omnibox/suggestions/editurl/ShunyaEditUrlSuggestionProcessor",
                Context.class, SuggestionHost.class, UrlBarDelegate.class,
                OmniboxImageSupplier.class, Supplier.class, Supplier.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/toolbar/top/TopToolbarCoordinator",
                "org/chromium/chrome/browser/toolbar/top/ShunyaTopToolbarCoordinator",
                ToolbarControlContainer.class, ViewStub.class, ViewStub.class, ToolbarLayout.class,
                ToolbarDataProvider.class, ToolbarTabController.class, UserEducationHelper.class,
                List.class, OneshotSupplier.class, ThemeColorProvider.class,
                ThemeColorProvider.class, MenuButtonCoordinator.class, MenuButtonCoordinator.class,
                ObservableSupplier.class, ObservableSupplier.class, ObservableSupplier.class,
                ButtonDataProvider.class, Callback.class, Supplier.class, Supplier.class,
                BooleanSupplier.class, boolean.class, boolean.class, boolean.class, boolean.class,
                HistoryDelegate.class, BooleanSupplier.class, OfflineDownloader.class,
                boolean.class, Callback.class, boolean.class, ObservableSupplier.class,
                ObservableSupplier.class, BrowserStateBrowserControlsVisibilityDelegate.class,
                boolean.class, FullscreenManager.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/toolbar/menu_button/MenuButtonCoordinator",
                "org/chromium/chrome/browser/toolbar/menu_button/ShunyaMenuButtonCoordinator",
                OneshotSupplier.class, BrowserStateBrowserControlsVisibilityDelegate.class,
                WindowAndroid.class, MenuButtonCoordinator.SetFocusFunction.class, Runnable.class,
                boolean.class, Supplier.class, ThemeColorProvider.class, Supplier.class,
                Runnable.class, int.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/share/ShareDelegateImpl",
                "org/chromium/chrome/browser/share/ShunyaShareDelegateImpl",
                BottomSheetController.class, ActivityLifecycleDispatcher.class, Supplier.class,
                Supplier.class, Supplier.class, ShareDelegateImpl.ShareSheetDelegate.class,
                boolean.class));
        Assert.assertTrue(
                constructorsMatch("org/chromium/chrome/browser/autofill/AutofillPopupBridge",
                        "org/chromium/chrome/browser/autofill/ShunyaAutofillPopupBridge", View.class,
                        long.class, WindowAndroid.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/omnibox/suggestions/AutocompleteMediator",
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaAutocompleteMediator",
                Context.class, AutocompleteControllerProvider.class, AutocompleteDelegate.class,
                UrlBarEditingTextStateProvider.class, PropertyModel.class, Handler.class,
                Supplier.class, Supplier.class, Supplier.class, LocationBarDataProvider.class,
                Callback.class, Supplier.class, BookmarkState.class, OmniboxActionDelegate.class,
                OpenHistoryClustersDelegate.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/feed/FeedSurfaceMediator",
                "org/chromium/chrome/browser/feed/ShunyaFeedSurfaceMediator",
                FeedSurfaceCoordinator.class, Context.class, SnapScrollHelper.class,
                PropertyModel.class, int.class, FeedActionDelegate.class,
                FeedOptionsCoordinator.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/partnercustomizations/CustomizationProviderDelegateImpl",
                "org/chromium/chrome/browser/partnercustomizations/ShunyaCustomizationProviderDelegateImpl"));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/share/send_tab_to_self/ManageAccountDevicesLinkView",
                "org/chromium/chrome/browser/share/send_tab_to_self/ShunyaManageAccountDevicesLinkView",
                Context.class, AttributeSet.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/suggestions/tile/MostVisitedTilesMediator",
                "org/chromium/chrome/browser/suggestions/tile/ShunyaMostVisitedTilesMediator",
                Resources.class, UiConfig.class, ViewGroup.class, ViewStub.class,
                TileRenderer.class, PropertyModel.class, boolean.class, boolean.class,
                boolean.class, Runnable.class, Runnable.class, boolean.class));
        Assert.assertTrue(
                constructorsMatch("org/chromium/chrome/browser/dom_distiller/ReaderModeManager",
                        "org/chromium/chrome/browser/dom_distiller/ShunyaReaderModeManager",
                        Tab.class, Supplier.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/crash/ChromePureJavaExceptionReporter",
                "org/chromium/chrome/browser/crash/ShunyaPureJavaExceptionReporter"));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/omnibox/suggestions/DropdownItemViewInfoListBuilder",
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaDropdownItemViewInfoListBuilder",
                Supplier.class, BookmarkState.class, OpenHistoryClustersDelegate.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/omnibox/suggestions/DropdownItemViewInfoListManager",
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaDropdownItemViewInfoListManager",
                ModelList.class, Context.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/omnibox/LocationBarCoordinator",
                "org/chromium/chrome/browser/omnibox/ShunyaLocationBarCoordinator", View.class,
                View.class, ObservableSupplier.class,
                ShunyaLocationBarMediator.getPrivacyPreferencesManagerClass(),
                LocationBarDataProvider.class, ActionMode.Callback.class, WindowDelegate.class,
                WindowAndroid.class, Supplier.class, Supplier.class, Supplier.class,
                IncognitoStateProvider.class, ActivityLifecycleDispatcher.class,
                OverrideUrlLoadingDelegate.class, BackKeyBehaviorDelegate.class,
                SearchEngineLogoUtils.class, PageInfoAction.class, Callback.class,
                ShunyaLocationBarMediator.getSaveOfflineButtonStateClass(),
                ShunyaLocationBarMediator.getOmniboxUmaClass(), Supplier.class, BookmarkState.class,
                BooleanSupplier.class, Supplier.class, OmniboxActionDelegate.class,
                BrowserStateBrowserControlsVisibilityDelegate.class, Callback.class,
                BackPressManager.class, OmniboxSuggestionsDropdownScrollListener.class,
                OpenHistoryClustersDelegate.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/omnibox/LocationBarMediator",
                "org/chromium/chrome/browser/omnibox/ShunyaLocationBarMediator", Context.class,
                LocationBarLayout.class, LocationBarDataProvider.class, ObservableSupplier.class,
                ShunyaLocationBarMediator.getPrivacyPreferencesManagerClass(),
                OverrideUrlLoadingDelegate.class, ShunyaLocationBarMediator.getLocaleManagerClass(),
                OneshotSupplier.class, BackKeyBehaviorDelegate.class, WindowAndroid.class,
                boolean.class, SearchEngineLogoUtils.class,
                ShunyaLocationBarMediator.getLensControllerClass(),
                ShunyaLocationBarMediator.getSaveOfflineButtonStateClass(),
                ShunyaLocationBarMediator.getOmniboxUmaClass(), BooleanSupplier.class,
                ShunyaLocationBarMediator.getOmniboxSuggestionsDropdownEmbedderImplClass()));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/IntentHandler",
                "org/chromium/chrome/browser/ShunyaIntentHandler", Activity.class,
                IntentHandler.IntentHandlerDelegate.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/AppHooksImpl",
                "org/chromium/chrome/browser/ShunyaAppHooks"));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/flags/CachedFlag",
                "org/chromium/chrome/browser/flags/ShunyaCachedFlag", String.class, boolean.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/flags/CachedFlag",
                "org/chromium/chrome/browser/flags/ShunyaCachedFlag", String.class, String.class,
                boolean.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/logo/LogoMediator",
                "org/chromium/chrome/browser/logo/ShunyaLogoMediator", Context.class, Callback.class,
                PropertyModel.class, boolean.class, Callback.class, Runnable.class, boolean.class,
                LogoCoordinator.VisibilityObserver.class, CachedTintedBitmap.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/notifications/permissions/NotificationPermissionRationaleDialogController",
                "org/chromium/chrome/browser/notifications/permissions/ShunyaNotificationPermissionRationaleDialogController",
                Context.class, ModalDialogManager.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/notifications/StandardNotificationBuilder",
                "org/chromium/chrome/browser/notifications/ShunyaNotificationBuilder",
                Context.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/tabbed_mode/TabbedRootUiCoordinator",
                "org/chromium/chrome/browser/tabbed_mode/ShunyaTabbedRootUiCoordinator",
                AppCompatActivity.class, Callback.class, ObservableSupplier.class,
                ActivityTabProvider.class, ObservableSupplier.class, ObservableSupplier.class,
                ObservableSupplier.class, Supplier.class, ObservableSupplier.class,
                OneshotSupplier.class, OneshotSupplier.class, OneshotSupplier.class,
                OneshotSupplier.class, Supplier.class, BrowserControlsManager.class,
                ActivityWindowAndroid.class, ActivityLifecycleDispatcher.class,
                ObservableSupplier.class, MenuOrKeyboardActionController.class, Supplier.class,
                ObservableSupplier.class, AppMenuBlocker.class, BooleanSupplier.class,
                BooleanSupplier.class, Supplier.class, FullscreenManager.class, Supplier.class,
                Supplier.class, Supplier.class, int.class, Supplier.class, Supplier.class,
                AppMenuDelegate.class, StatusBarColorProvider.class, ObservableSupplierImpl.class,
                IntentRequestTracker.class, int.class, Supplier.class, Function.class,
                OneshotSupplier.class, boolean.class, BackPressManager.class, Bundle.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/bookmarks/BookmarkToolbar",
                "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkToolbar", Context.class,
                AttributeSet.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/bookmarks/BookmarkToolbarCoordinator",
                "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkToolbarCoordinator",
                Context.class, SelectableListLayout.class, SelectionDelegate.class,
                SearchDelegate.class, DragReorderableRecyclerViewAdapter.class, boolean.class,
                OneshotSupplier.class, BookmarkModel.class, BookmarkOpener.class,
                BookmarkUiPrefs.class, ModalDialogManager.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/bookmarks/BookmarkManagerCoordinator",
                "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkManagerCoordinator",
                Context.class, ComponentName.class, boolean.class, boolean.class,
                SnackbarManager.class, Profile.class, BookmarkUiPrefs.class));
        Assert.assertTrue(
                constructorsMatch("org/chromium/chrome/browser/bookmarks/BookmarkManagerMediator",
                        "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkManagerMediator",
                        Context.class, BookmarkModel.class, BookmarkOpener.class,
                        SelectableListLayout.class, SelectionDelegate.class, RecyclerView.class,
                        DragReorderableRecyclerViewAdapter.class, LargeIconBridge.class,
                        boolean.class, boolean.class, ObservableSupplierImpl.class, Profile.class,
                        BookmarkUndoController.class, ModelList.class, BookmarkUiPrefs.class,
                        Runnable.class, BookmarkImageFetcher.class, ShoppingService.class,
                        SnackbarManager.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/bookmarks/BookmarkBridge",
                "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkBridge", long.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/bookmarks/BookmarkModel",
                "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkModel", long.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/bookmarks/BookmarkPage",
                "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkPage", ComponentName.class,
                SnackbarManager.class, boolean.class, NativePageHost.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/feedback/HelpAndFeedbackLauncherImpl",
                "org/chromium/chrome/browser/feedback/ShunyaHelpAndFeedbackLauncherImpl",
                Profile.class));
        Assert.assertTrue(constructorsMatch("org/chromium/chrome/browser/firstrun/FreIntentCreator",
                "org/chromium/chrome/browser/firstrun/ShunyaFreIntentCreator"));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/components/external_intents/ExternalNavigationHandler",
                "org/chromium/chrome/browser/externalnav/ShunyaExternalNavigationHandler",
                ExternalNavigationDelegate.class));
        Assert.assertTrue(constructorsMatch(
                "org/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator",
                "org/chromium/chrome/browser/contextmenu/ShunyaChromeContextMenuPopulator",
                ContextMenuItemDelegate.class, Supplier.class, int.class, ExternalAuthUtils.class,
                Context.class, ContextMenuParams.class, ContextMenuNativeDelegate.class));
    }

    @Test
    @SmallTest
    public void testFieldsExist() throws Exception {
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/feed/FeedSurfaceCoordinator", "mNtpHeader"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/feed/FeedSurfaceCoordinator", "mRootView"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/feed/FeedSurfaceMediator", "mCoordinator"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/feed/FeedSurfaceMediator", "mSnapScrollHelper"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/ntp/NewTabPage", "mBrowserControlsStateProvider"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/ntp/NewTabPage", "mNewTabPageLayout"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/ntp/NewTabPage", "mFeedSurfaceProvider"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/ntp/NewTabPage", "mToolbarSupplier"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/ntp/NewTabPage", "mTabModelSelector"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/ntp/NewTabPage", "mBottomSheetController"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/omnibox/suggestions/editurl/EditUrlSuggestionProcessor",
                "mHasClearedOmniboxForFocus"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/suggestions/tile/MostVisitedTilesMediator",
                        "mTileGroup"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/sync/settings/ManageSyncSettings",
                        "mGoogleActivityControls"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/sync/settings/ManageSyncSettings", "mSyncEncryption"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/bottom/BottomControlsCoordinator",
                        "mMediator"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/toolbar/ToolbarManager",
                "mBottomControlsCoordinatorSupplier"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mCallbackController"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mBrowserControlsSizer"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mFullscreenManager"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mActivityTabProvider"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mAppThemeColorProvider"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mScrimCoordinator"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mShowStartSurfaceSupplier"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mMenuButtonCoordinator"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mToolbarTabController"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/ToolbarManager", "mLocationBar"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mActionModeController"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mLocationBarModel"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/ToolbarManager", "mToolbar"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mBookmarkModelSupplier"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mLayoutManager"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/toolbar/ToolbarManager",
                "mOverlayPanelVisibilitySupplier"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mTabModelSelector"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mIncognitoStateProvider"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mTabCountProvider"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/ToolbarManager", "mTabGroupUi"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mBottomSheetController"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/toolbar/ToolbarManager",
                "mActivityLifecycleDispatcher"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mIsWarmOnResumeSupplier"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mTabContentManager"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mTabCreatorManager"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mSnackbarManager"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/ToolbarManager", "mTabObscuringHandler"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/top/TopToolbarCoordinator",
                        "mTabSwitcherModeCoordinator"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/top/TopToolbarCoordinator",
                        "mOptionalButtonController"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/top/TopToolbarCoordinator",
                        "mToolbarColorObserverManager"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTTCoordinator",
                        "mActiveTabSwitcherToolbar"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar",
                        "mNewTabViewButton"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar",
                        "mNewTabImageButton"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar",
                        "mShouldShowNewTabVariation"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/top/TabSwitcherModeTopToolbar",
                        "mIsIncognito"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/app/ChromeActivity",
                "mBrowserControlsManagerSupplier"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/bottom/BottomControlsMediator",
                        "mBottomControlsHeight"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/toolbar/bottom/BottomControlsMediator", "mModel"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/bottom/BottomControlsMediator",
                        "mBrowserControlsSizer"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/toolbar/IncognitoToggleTabLayout",
                        "mIncognitoButtonIcon"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/tasks/tab_management/TabGroupUiCoordinator",
                "mToolbarView"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineAdapter",
                "mProfile"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineSettings",
                "mProfile"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineSettings",
                "mSearchEngineAdapter"));
        Assert.assertTrue(fieldExists(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings",
                "mCategory", true, SiteSettingsCategory.class));
        Assert.assertTrue(fieldExists(
                "org/chromium/components/browser_ui/site_settings/SingleWebsiteSettings", "mSite",
                true, Website.class));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/omnibox/suggestions/AutocompleteMediator",
                        "mNativeInitialized", true, boolean.class));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/omnibox/suggestions/AutocompleteMediator",
                        "mDropdownViewInfoListManager"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/omnibox/suggestions/AutocompleteMediator",
                        "mContext"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/ntp/NewTabPageLayout", "mMvTilesContainerLayout"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/ntp/NewTabPageLayout", "mLogoCoordinator"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/dom_distiller/ReaderModeManager", "mTab"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/omnibox/LocationBarCoordinator",
                "mLocationBarMediator"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/omnibox/LocationBarMediator", "mNativeInitialized"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/omnibox/LocationBarMediator", "mWindowAndroid"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/omnibox/LocationBarMediator", "mLocationBarLayout"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/omnibox/LocationBarMediator",
                "mIsUrlFocusChangeInProgress"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/omnibox/LocationBarMediator", "mUrlHasFocus"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/omnibox/LocationBarMediator", "mIsTablet"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/omnibox/LocationBarMediator",
                "mIsLocationBarFocusedFromNtpScroll"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/omnibox/LocationBarMediator", "mContext"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/omnibox/LocationBarMediator", "mBrandedColorScheme"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/logo/LogoMediator", "mLogoModel"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/logo/LogoMediator", "mShouldShowLogo"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/incognito/reauth/IncognitoReauthCoordinatorBase",
                "mIncognitoReauthView"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/app/bookmarks/BookmarkActivity",
                "mBookmarkManagerCoordinator"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/bookmarks/BookmarkManagerCoordinator", "mMediator"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/bookmarks/BookmarkManagerMediator", "mBookmarkModel"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/bookmarks/BookmarkManagerMediator", "mContext"));
        Assert.assertTrue(fieldExists(
                "org/chromium/chrome/browser/bookmarks/BookmarkToolbarCoordinator", "mToolbar"));
        Assert.assertTrue(fieldExists("org/chromium/chrome/browser/bookmarks/BookmarkPage",
                "mBookmarkManagerCoordinator"));
        Assert.assertTrue(
                fieldExists("org/chromium/chrome/browser/flags/CachedFlag", "mDefaultValue"));
        Assert.assertFalse(
                fieldExists("org/chromium/chrome/browser/tasks/tab_groups/TabGroupModelFilter",
                        "mIsResetting"));
    }

    @Test
    @SmallTest
    public void testSuperNames() throws Exception {
        Assert.assertTrue(checkSuperName("org/chromium/chrome/browser/ChromeApplicationImpl",
                "org/chromium/chrome/browser/ShunyaApplicationImplBase"));
        Assert.assertTrue(checkSuperName("org/chromium/chrome/browser/settings/MainSettings",
                "org/chromium/chrome/browser/settings/ShunyaMainPreferencesBase"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/ntp/NewTabPageLayout", "android/widget/FrameLayout"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/password_manager/settings/PasswordSettings",
                "org/chromium/chrome/browser/password_manager/settings/ShunyaPasswordSettingsBase"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/search_engines/settings/SearchEngineAdapter",
                "org/chromium/chrome/browser/search_engines/settings/ShunyaBaseSearchEngineAdapter"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings",
                "org/chromium/components/browser_ui/site_settings/ShunyaSingleCategorySettings"));
        Assert.assertTrue(checkSuperName("org/chromium/chrome/browser/ChromeTabbedActivity",
                "org/chromium/chrome/browser/app/ShunyaActivity"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/tabbed_mode/TabbedAppMenuPropertiesDelegate",
                "org/chromium/chrome/browser/app/appmenu/ShunyaAppMenuPropertiesDelegateImpl"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/customtabs/CustomTabAppMenuPropertiesDelegate",
                "org/chromium/chrome/browser/app/appmenu/ShunyaAppMenuPropertiesDelegateImpl"));
        Assert.assertTrue(
                checkSuperName("org/chromium/chrome/browser/suggestions/tile/SuggestionsTileView",
                        "org/chromium/chrome/browser/suggestions/tile/ShunyaTileView"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/customtabs/features/toolbar/CustomTabToolbar",
                "org/chromium/chrome/browser/toolbar/top/ShunyaToolbarLayoutImpl"));
        Assert.assertTrue(checkSuperName("org/chromium/chrome/browser/toolbar/top/ToolbarPhone",
                "org/chromium/chrome/browser/toolbar/top/ShunyaToolbarLayoutImpl"));
        Assert.assertTrue(checkSuperName("org/chromium/chrome/browser/toolbar/top/ToolbarTablet",
                "org/chromium/chrome/browser/toolbar/top/ShunyaToolbarLayoutImpl"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/components/browser_ui/site_settings/SingleCategorySettings",
                "org/chromium/components/browser_ui/site_settings/ShunyaSingleCategorySettings"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/components/browser_ui/site_settings/SingleWebsiteSettings",
                "org/chromium/components/browser_ui/site_settings/ShunyaSingleWebsiteSettings"));
        Assert.assertTrue(checkSuperName("org/chromium/components/browser_ui/site_settings/Website",
                "org/chromium/components/browser_ui/site_settings/ShunyaWebsite"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/components/browser_ui/site_settings/FourStateCookieSettingsPreference",
                "org/chromium/components/browser_ui/site_settings/ShunyaFourStateCookieSettingsPreferenceBase"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/components/browser_ui/site_settings/SiteSettings",
                "org/chromium/components/browser_ui/site_settings/ShunyaSiteSettingsPreferencesBase"));
        Assert.assertTrue(
                checkSuperName("org/chromium/chrome/browser/infobar/TranslateCompactInfoBar",
                        "org/chromium/chrome/browser/infobar/ShunyaTranslateCompactInfoBarBase"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/download/DownloadMessageUiControllerImpl",
                "org/chromium/chrome/browser/download/ShunyaDownloadMessageUiControllerImpl"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/omnibox/suggestions/AutocompleteCoordinator",
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaAutocompleteCoordinator"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/omnibox/suggestions/AutocompleteMediator",
                "org/chromium/chrome/browser/omnibox/suggestions/ShunyaAutocompleteMediatorBase"));
        Assert.assertTrue(checkSuperName("org/chromium/chrome/browser/omnibox/LocationBarPhone",
                "org/chromium/chrome/browser/omnibox/ShunyaLocationBarLayout"));
        Assert.assertTrue(checkSuperName("org/chromium/chrome/browser/omnibox/LocationBarTablet",
                "org/chromium/chrome/browser/omnibox/ShunyaLocationBarLayout"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/searchwidget/SearchActivityLocationBarLayout",
                "org/chromium/chrome/browser/omnibox/ShunyaLocationBarLayout"));
        Assert.assertTrue(
                checkSuperName("org/chromium/chrome/browser/document/ChromeLauncherActivity",
                        "org/chromium/chrome/browser/document/ShunyaLauncherActivity"));
        Assert.assertTrue(
                checkSuperName("org/chromium/components/permissions/PermissionDialogDelegate",
                        "org/chromium/components/permissions/ShunyaPermissionDialogDelegate"));
        Assert.assertTrue(
                checkSuperName("org/chromium/chrome/browser/tracing/settings/DeveloperSettings",
                        "org/chromium/chrome/browser/settings/ShunyaPreferenceFragment"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/incognito/reauth/TabSwitcherIncognitoReauthCoordinator",
                "org/chromium/chrome/browser/incognito/reauth/ShunyaPrivateTabReauthCoordinatorBase"));
        Assert.assertTrue(checkSuperName(
                "org/chromium/chrome/browser/incognito/reauth/FullScreenIncognitoReauthCoordinator",
                "org/chromium/chrome/browser/incognito/reauth/ShunyaPrivateTabReauthCoordinatorBase"));
        Assert.assertTrue(checkSuperName("org/chromium/chrome/browser/bookmarks/BookmarkModel",
                "org/chromium/chrome/browser/bookmarks/ShunyaBookmarkBridge"));
        Assert.assertTrue(
                checkSuperName("org/chromium/chrome/browser/tasks/tab_groups/TabGroupModelFilter",
                        "org/chromium/chrome/browser/tasks/tab_groups/ShunyaTabGroupModelFilter"));
    }

    private boolean classExists(String className) {
        return getClassForPath(className) != null;
    }

    private boolean methodExists(String className, String methodName, Boolean checkTypes,
            Class<?> returnType, Class<?>... parameterTypes) {
        Class c = getClassForPath(className);
        if (c == null) {
            return false;
        }
        for (Method m : c.getDeclaredMethods()) {
            if (m.getName().equals(methodName)) {
                if (checkTypes) {
                    Class<?> type = m.getReturnType();
                    if (type == null && returnType != null || type != null && returnType == null
                            || type != null && returnType != null && !type.equals(returnType)) {
                        return false;
                    }
                    Class<?>[] types = m.getParameterTypes();
                    if (types == null && parameterTypes != null
                            || types != null && parameterTypes == null
                            || types != null && parameterTypes != null
                                    && types.length != parameterTypes.length) {
                        return false;
                    }
                    for (int i = 0; i < (types == null ? 0 : types.length); i++) {
                        if (!types[i].equals(parameterTypes[i])) {
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    private boolean fieldExists(String className, String fieldName) {
        return fieldExists(className, fieldName, false, null);
    }

    private boolean fieldExists(
            String className, String fieldName, Boolean checkTypes, Class<?> fieldType) {
        Class c = getClassForPath(className);
        if (c == null) {
            return false;
        }
        for (Field f : c.getDeclaredFields()) {
            if (f.getName().equals(fieldName)) {
                if (checkTypes) {
                    if (fieldType != null && f.getType().equals(fieldType)) return true;
                } else
                    return true;
            }
        }
        return false;
    }

    private Class getClassForPath(String path) {
        try {
            Class c = Class.forName(path.replace("/", "."));
            return c;
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    private boolean constructorsMatch(
            String class1Name, String class2Name, Class<?>... parameterTypes) {
        Class c1 = getClassForPath(class1Name);
        Class c2 = getClassForPath(class2Name);
        if (c1 == null || c2 == null) {
            return false;
        }
        try {
            Constructor ctor1 = c1.getDeclaredConstructor(parameterTypes);
            Constructor ctor2 = c2.getDeclaredConstructor(parameterTypes);
            if (ctor1 != null && ctor2 != null) {
                return true;
            }
        } catch (NoSuchMethodException e) {
            return false;
        }
        return false;
    }

    private boolean checkSuperName(String className, String superName) {
        Class c = getClassForPath(className);
        Class s = getClassForPath(superName);
        if (c == null || s == null) {
            return false;
        }
        return c.getSuperclass().equals(s);
    }
}

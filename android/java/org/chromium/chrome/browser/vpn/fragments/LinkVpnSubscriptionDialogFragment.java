/**
 * Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.vpn.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import org.chromium.base.Log;
import org.chromium.chrome.R;
import org.chromium.chrome.browser.ShunyaDialogFragment;
import org.chromium.chrome.browser.app.ShunyaActivity;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnPrefUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnUtils;

public class LinkVpnSubscriptionDialogFragment
        extends ShunyaDialogFragment implements View.OnClickListener {
    private static final String TAG = "LinkVpnSubscription";

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_link_subscription_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ShunyaVpnPrefUtils.setLinkSubscriptionDialogShown(true);
        Button mDoneButton = view.findViewById(R.id.btn_done);
        mDoneButton.setOnClickListener(this);
        ImageView btnClose = view.findViewById(R.id.modal_close);
        btnClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_done) {
            try {
                ShunyaActivity shunyaActivity = ShunyaActivity.getShunyaActivity();
                shunyaActivity.openNewOrSelectExistingTab(ShunyaVpnUtils.getShunyaAccountUrl());
            } catch (ShunyaActivity.ShunyaActivityNotFoundException e) {
                Log.e(TAG, "onClick btn_done " + e);
            }
        }
        dismiss();
    }
}

/**
 * Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.vpn.utils;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Pair;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.chromium.chrome.R;
import org.chromium.chrome.browser.util.LiveDataUtil;
import org.chromium.chrome.browser.vpn.ShunyaVpnNativeWorker;
import org.chromium.chrome.browser.vpn.billing.InAppPurchaseWrapper;
import org.chromium.chrome.browser.vpn.billing.PurchaseModel;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnPrefModel;

import java.util.TimeZone;

public class ShunyaVpnApiResponseUtils {
    public static void queryPurchaseFailed(Activity activity) {
        ShunyaVpnPrefUtils.setProductId("");
        ShunyaVpnPrefUtils.setPurchaseExpiry(0L);
        ShunyaVpnPrefUtils.setSubscriptionPurchase(false);
        ShunyaVpnPrefUtils.setPaymentState(0);
        if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(activity)) {
            ShunyaVpnProfileUtils.getInstance().stopVpn(activity);
        }
        ShunyaVpnUtils.showToast(
                activity.getResources().getString(R.string.purchase_token_verification_failed));
    }

    public static void handleOnGetSubscriberCredential(Activity activity, boolean isSuccess) {
        if (isSuccess) {
            if (!ShunyaVpnNativeWorker.getInstance().isPurchasedUser()) {
                MutableLiveData<PurchaseModel> _activePurchases = new MutableLiveData();
                LiveData<PurchaseModel> activePurchases = _activePurchases;
                InAppPurchaseWrapper.getInstance().queryPurchases(_activePurchases);
                LiveDataUtil.observeOnce(activePurchases, activePurchaseModel -> {
                    InAppPurchaseWrapper.getInstance().processPurchases(
                            activity, activePurchaseModel.getPurchase());
                });
            }
            ShunyaVpnNativeWorker.getInstance().getTimezonesForRegions();
        } else {
            ShunyaVpnUtils.showToast(
                    activity.getResources().getString(R.string.vpn_profile_creation_failed));
            ShunyaVpnUtils.dismissProgressDialog();
        }
    }

    public static void handleOnGetTimezonesForRegions(Activity activity,
            ShunyaVpnPrefModel shunyaVpnPrefModel, String jsonTimezones, boolean isSuccess) {
        if (isSuccess) {
            String region = ShunyaVpnUtils.getRegionForTimeZone(
                    jsonTimezones, TimeZone.getDefault().getID());
            if (TextUtils.isEmpty(region)) {
                ShunyaVpnUtils.showToast(String.format(
                        activity.getResources().getString(R.string.couldnt_get_matching_timezone),
                        TimeZone.getDefault().getID()));
                return;
            }
            if (!TextUtils.isEmpty(ShunyaVpnUtils.selectedServerRegion)
                    && ShunyaVpnUtils.selectedServerRegion != null) {
                region = ShunyaVpnUtils.selectedServerRegion.equals(
                                 ShunyaVpnPrefUtils.PREF_SHUNYA_VPN_AUTOMATIC)
                        ? region
                        : ShunyaVpnUtils.selectedServerRegion;
                ShunyaVpnUtils.selectedServerRegion = null;
            } else {
                String serverRegion = ShunyaVpnPrefUtils.getServerRegion();
                region = serverRegion.equals(ShunyaVpnPrefUtils.PREF_SHUNYA_VPN_AUTOMATIC)
                        ? region
                        : serverRegion;
            }

            ShunyaVpnNativeWorker.getInstance().getHostnamesForRegion(region);
            shunyaVpnPrefModel.setServerRegion(region);
        } else {
            ShunyaVpnUtils.showToast(
                    activity.getResources().getString(R.string.vpn_profile_creation_failed));
            ShunyaVpnUtils.dismissProgressDialog();
        }
    }

    public static Pair<String, String> handleOnGetHostnamesForRegion(Activity activity,
            ShunyaVpnPrefModel shunyaVpnPrefModel, String jsonHostNames, boolean isSuccess) {
        Pair<String, String> host = new Pair<String, String>("", "");
        if (isSuccess && shunyaVpnPrefModel != null) {
            host = ShunyaVpnUtils.getHostnameForRegion(jsonHostNames);
            ShunyaVpnNativeWorker.getInstance().getWireguardProfileCredentials(
                    shunyaVpnPrefModel.getSubscriberCredential(),
                    shunyaVpnPrefModel.getClientPublicKey(), host.first);
        } else {
            ShunyaVpnUtils.showToast(
                    activity.getResources().getString(R.string.vpn_profile_creation_failed));
            ShunyaVpnUtils.dismissProgressDialog();
        }
        return host;
    }
}

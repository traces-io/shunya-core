/**
 * Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.vpn.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;

import androidx.fragment.app.FragmentActivity;

import com.wireguard.config.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.chromium.base.ContextUtils;
import org.chromium.base.Log;
import org.chromium.chrome.browser.ShunyaRewardsNativeWorker;
import org.chromium.chrome.browser.util.ShunyaConstants;
import org.chromium.chrome.browser.vpn.ShunyaVpnNativeWorker;
import org.chromium.chrome.browser.vpn.activities.ShunyaVpnPlansActivity;
import org.chromium.chrome.browser.vpn.activities.ShunyaVpnProfileActivity;
import org.chromium.chrome.browser.vpn.activities.ShunyaVpnSupportActivity;
import org.chromium.chrome.browser.vpn.fragments.ShunyaVpnAlwaysOnErrorDialogFragment;
import org.chromium.chrome.browser.vpn.fragments.ShunyaVpnConfirmDialogFragment;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnServerRegion;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnWireguardProfileCredentials;
import org.chromium.chrome.browser.vpn.split_tunnel.SplitTunnelActivity;
import org.chromium.chrome.browser.vpn.wireguard.WireguardConfigUtils;
import org.chromium.gms.ChromiumPlayServicesAvailability;
import org.chromium.ui.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShunyaVpnUtils {
    private static final String TAG = "ShunyaVPN";
    public static final String SUBSCRIPTION_PARAM_TEXT = "subscription";
    public static final String IAP_ANDROID_PARAM_TEXT = "iap-android";
    public static final String VERIFY_CREDENTIALS_FAILED = "verify_credentials_failed";
    public static final String DESKTOP_CREDENTIAL = "desktop_credential";

    private static final String SHUNYA_ACCOUNT_PROD_PAGE_URL =
            "https://account.shunya.com?intent=connect-receipt&product=vpn";
    private static final String SHUNYA_ACCOUNT_STAGING_PAGE_URL =
            "https://account.shunyasoftware.com?intent=connect-receipt&product=vpn";

    public static boolean mIsServerLocationChanged;
    public static boolean mUpdateProfileAfterSplitTunnel;
    public static String selectedServerRegion;
    private static ProgressDialog sProgressDialog;

    public static String getShunyaAccountUrl() {
        return ShunyaVpnPrefUtils.isLinkSubscriptionOnStaging() ? SHUNYA_ACCOUNT_STAGING_PAGE_URL
                                                               : SHUNYA_ACCOUNT_PROD_PAGE_URL;
    }

    public static void openShunyaVpnPlansActivity(Activity activity) {
        Intent shunyaVpnPlanIntent = new Intent(activity, ShunyaVpnPlansActivity.class);
        shunyaVpnPlanIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        shunyaVpnPlanIntent.setAction(Intent.ACTION_VIEW);
        activity.startActivity(shunyaVpnPlanIntent);
    }

    public static void openShunyaVpnProfileActivity(Activity activity) {
        Intent shunyaVpnProfileIntent = new Intent(activity, ShunyaVpnProfileActivity.class);
        shunyaVpnProfileIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        shunyaVpnProfileIntent.setAction(Intent.ACTION_VIEW);
        activity.startActivity(shunyaVpnProfileIntent);
    }

    public static void openShunyaVpnSupportActivity(Activity activity) {
        Intent shunyaVpnSupportIntent = new Intent(activity, ShunyaVpnSupportActivity.class);
        shunyaVpnSupportIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        shunyaVpnSupportIntent.setAction(Intent.ACTION_VIEW);
        activity.startActivity(shunyaVpnSupportIntent);
    }

    public static void openSplitTunnelActivity(Activity activity) {
        Intent shunyaVpnSupportIntent = new Intent(activity, SplitTunnelActivity.class);
        shunyaVpnSupportIntent.setAction(Intent.ACTION_VIEW);
        activity.startActivity(shunyaVpnSupportIntent);
    }

    public static void showProgressDialog(Activity activity, String message) {
        sProgressDialog = ProgressDialog.show(activity, "", message, true);
    }

    public static void dismissProgressDialog() {
        if (sProgressDialog != null && sProgressDialog.isShowing()) {
            sProgressDialog.dismiss();
        }
    }

    public static String getRegionForTimeZone(String jsonTimezones, String currentTimezone) {
        // Add root element to make it real JSON, otherwise getJSONArray cannot parse it
        jsonTimezones = "{\"regions\":" + jsonTimezones + "}";
        try {
            JSONObject result = new JSONObject(jsonTimezones);
            JSONArray regions = result.getJSONArray("regions");
            for (int i = 0; i < regions.length(); i++) {
                JSONObject region = regions.getJSONObject(i);
                JSONArray timezones = region.getJSONArray("timezones");
                for (int j = 0; j < timezones.length(); j++) {
                    if (timezones.getString(j).equals(currentTimezone)) {
                        return region.getString("name");
                    }
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "ShunyaVpnUtils -> getRegionForTimeZone JSONException error " + e);
        }
        return "";
    }

    public static Pair<String, String> getHostnameForRegion(String jsonHostnames) {
        jsonHostnames = "{\"hostnames\":" + jsonHostnames + "}";
        try {
            JSONObject result = new JSONObject(jsonHostnames);
            JSONArray hostnames = result.getJSONArray("hostnames");
            ArrayList<JSONObject> hosts = new ArrayList<JSONObject>();
            for (int i = 0; i < hostnames.length(); i++) {
                JSONObject hostname = hostnames.getJSONObject(i);
                if (hostname.getInt("capacity-score") == 0
                        || hostname.getInt("capacity-score") == 1) {
                    hosts.add(hostname);
                }
            }

            JSONObject hostname;
            if (hosts.size() < 2) {
                hostname = hostnames.getJSONObject(new Random().nextInt(hostnames.length()));
            } else {
                hostname = hosts.get(new Random().nextInt(hosts.size()));
            }
            return new Pair<>(hostname.getString("hostname"), hostname.getString("display-name"));
        } catch (JSONException e) {
            Log.e(TAG, "ShunyaVpnUtils -> getHostnameForRegion JSONException error " + e);
        }
        return new Pair<String, String>("", "");
    }

    public static ShunyaVpnWireguardProfileCredentials getWireguardProfileCredentials(
            String jsonWireguardProfileCredentials) {
        try {
            JSONObject wireguardProfileCredentials =
                    new JSONObject(jsonWireguardProfileCredentials);
            ShunyaVpnWireguardProfileCredentials shunyaVpnWireguardProfileCredentials =
                    new ShunyaVpnWireguardProfileCredentials(
                            wireguardProfileCredentials.getString("api-auth-token"),
                            wireguardProfileCredentials.getString("client-id"),
                            wireguardProfileCredentials.getString("mapped-ipv4-address"),
                            wireguardProfileCredentials.getString("mapped-ipv6-address"),
                            wireguardProfileCredentials.getString("server-public-key"));
            return shunyaVpnWireguardProfileCredentials;
        } catch (JSONException e) {
            Log.e(TAG, "ShunyaVpnUtils -> getWireguardProfileCredentials JSONException error " + e);
        }
        return null;
    }

    public static Long getPurchaseExpiryDate(String json) {
        try {
            JSONObject purchase = new JSONObject(json);
            String expiryTimeInString = purchase.getString("expiryTimeMillis");
            return Long.parseLong(expiryTimeInString);
        } catch (JSONException | NumberFormatException e) {
            Log.e(TAG,
                    "ShunyaVpnUtils -> getPurchaseExpiryDate JSONException | NumberFormatException error "
                            + e);
        }
        return 0L;
    }

    public static int getPaymentState(String json) {
        try {
            JSONObject purchase = new JSONObject(json);
            int paymentState = purchase.getInt("paymentState");
            return paymentState;
        } catch (JSONException e) {
            Log.e(TAG, "ShunyaVpnUtils -> getPaymentState JSONException error " + e);
        }
        return 0;
    }

    public static List<ShunyaVpnServerRegion> getServerLocations(String jsonServerLocations) {
        List<ShunyaVpnServerRegion> vpnServerRegions = new ArrayList<>();
        if (TextUtils.isEmpty(jsonServerLocations)) {
            return vpnServerRegions;
        }
        jsonServerLocations = "{\"servers\":" + jsonServerLocations + "}";
        try {
            JSONObject result = new JSONObject(jsonServerLocations);
            JSONArray servers = result.getJSONArray("servers");
            for (int i = 0; i < servers.length(); i++) {
                JSONObject server = servers.getJSONObject(i);
                ShunyaVpnServerRegion vpnServerRegion =
                        new ShunyaVpnServerRegion(server.getString("continent"),
                                server.getString("name"), server.getString("name-pretty"));
                vpnServerRegions.add(vpnServerRegion);
            }
        } catch (JSONException e) {
            Log.e(TAG, "ShunyaVpnUtils -> getServerLocations JSONException error " + e);
        }
        return vpnServerRegions;
    }

    public static void resetProfileConfiguration(Activity activity) {
        if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(activity)) {
            ShunyaVpnProfileUtils.getInstance().stopVpn(activity);
        }
        try {
            WireguardConfigUtils.deleteConfig(activity);
        } catch (Exception ex) {
            Log.e(TAG, "resetProfileConfiguration : " + ex.getMessage());
        }
        ShunyaVpnPrefUtils.setResetConfiguration(true);
        dismissProgressDialog();
    }

    public static void updateProfileConfiguration(Activity activity) {
        try {
            Config existingConfig = WireguardConfigUtils.loadConfig(activity);
            WireguardConfigUtils.deleteConfig(activity);
            WireguardConfigUtils.createConfig(activity, existingConfig);
        } catch (Exception ex) {
            Log.e(TAG, "updateProfileConfiguration : " + ex.getMessage());
        }
        if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(activity)) {
            ShunyaVpnProfileUtils.getInstance().stopVpn(activity);
            ShunyaVpnProfileUtils.getInstance().startVpn(activity);
        }
        dismissProgressDialog();
    }

    public static void showVpnAlwaysOnErrorDialog(Activity activity) {
        ShunyaVpnAlwaysOnErrorDialogFragment mShunyaVpnAlwaysOnErrorDialogFragment =
                new ShunyaVpnAlwaysOnErrorDialogFragment();
        mShunyaVpnAlwaysOnErrorDialogFragment.show(
                ((FragmentActivity) activity).getSupportFragmentManager(),
                "ShunyaVpnAlwaysOnErrorDialogFragment");
    }

    public static void showVpnConfirmDialog(Activity activity) {
        ShunyaVpnConfirmDialogFragment shunyaVpnConfirmDialogFragment =
                new ShunyaVpnConfirmDialogFragment();
        shunyaVpnConfirmDialogFragment.show(
                ((FragmentActivity) activity).getSupportFragmentManager(),
                "ShunyaVpnConfirmDialogFragment");
    }

    public static void reportBackgroundUsageP3A() {
        // Will report previous/current session timestamps...
        ShunyaVpnNativeWorker.getInstance().reportBackgroundP3A(
                ShunyaVpnPrefUtils.getSessionStartTimeMs(), ShunyaVpnPrefUtils.getSessionEndTimeMs());
        // ...and then reset the timestamps so we don't report the same session again.
        ShunyaVpnPrefUtils.setSessionStartTimeMs(-1);
        ShunyaVpnPrefUtils.setSessionEndTimeMs(-1);
    }

    public static void showToast(String message) {
        Context context = ContextUtils.getApplicationContext();
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private static boolean isRegionSupported() {
        ShunyaRewardsNativeWorker shunyaRewardsNativeWorker = ShunyaRewardsNativeWorker.getInstance();
        return (shunyaRewardsNativeWorker != null && shunyaRewardsNativeWorker.IsSupported());
    }

    private static boolean isShunyaVpnFeatureEnable() {
        if ((ContextUtils.getApplicationContext().getPackageName().equals(
                     ShunyaConstants.SHUNYA_PRODUCTION_PACKAGE_NAME)
                    || ShunyaVpnPrefUtils.isShunyaVpnFeatureEnabled())) {
            return true;
        }
        return false;
    }

    public static boolean isVpnFeatureSupported(Context context) {
        return isShunyaVpnFeatureEnable() && isRegionSupported()
                && ChromiumPlayServicesAvailability.isGooglePlayServicesAvailable(context);
    }
}

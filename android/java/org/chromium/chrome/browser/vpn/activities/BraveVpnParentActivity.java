/**
 * Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.vpn.activities;

import android.content.Intent;
import android.util.Pair;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.wireguard.android.backend.GoBackend;
import com.wireguard.crypto.KeyPair;

import org.chromium.base.Log;
import org.chromium.chrome.R;
import org.chromium.chrome.browser.init.AsyncInitializationActivity;
import org.chromium.chrome.browser.util.LiveDataUtil;
import org.chromium.chrome.browser.vpn.ShunyaVpnNativeWorker;
import org.chromium.chrome.browser.vpn.ShunyaVpnObserver;
import org.chromium.chrome.browser.vpn.billing.InAppPurchaseWrapper;
import org.chromium.chrome.browser.vpn.billing.PurchaseModel;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnPrefModel;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnWireguardProfileCredentials;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnApiResponseUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnPrefUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnProfileUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnUtils;
import org.chromium.chrome.browser.vpn.wireguard.WireguardConfigUtils;

public abstract class ShunyaVpnParentActivity
        extends AsyncInitializationActivity implements ShunyaVpnObserver {
    private static final String TAG = "ShunyaVPN";
    public boolean mIsVerification;
    protected ShunyaVpnPrefModel mShunyaVpnPrefModel;

    abstract void showRestoreMenu(boolean shouldShowRestore);
    abstract void updateProfileView();

    // Pass @{code ActivityResultRegistry} reference explicitly to avoid crash
    // https://github.com/shunya/shunya-browser/issues/31882
    ActivityResultLauncher<Intent> mIntentActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), getActivityResultRegistry(),
            result -> {
                ShunyaVpnUtils.dismissProgressDialog();
                if (result.getResultCode() == RESULT_OK) {
                    ShunyaVpnProfileUtils.getInstance().startVpn(ShunyaVpnParentActivity.this);
                    ShunyaVpnUtils.showVpnConfirmDialog(this);
                } else if (result.getResultCode() == RESULT_CANCELED) {
                    if (ShunyaVpnProfileUtils.getInstance().isVPNRunning(this)) {
                        ShunyaVpnUtils.showVpnAlwaysOnErrorDialog(this);
                    } else {
                        updateProfileView();
                    }
                    ShunyaVpnUtils.showToast(
                            getResources().getString(R.string.permission_was_cancelled));
                }
            });

    @Override
    public void finishNativeInitialization() {
        super.finishNativeInitialization();
    }

    protected void verifySubscription() {
        mShunyaVpnPrefModel = new ShunyaVpnPrefModel();
        MutableLiveData<PurchaseModel> _activePurchases = new MutableLiveData();
        LiveData<PurchaseModel> activePurchases = _activePurchases;
        InAppPurchaseWrapper.getInstance().queryPurchases(_activePurchases);
        LiveDataUtil.observeOnce(
                activePurchases, activePurchaseModel -> {
                    if (activePurchaseModel != null) {
                        mShunyaVpnPrefModel.setPurchaseToken(activePurchaseModel.getPurchaseToken());
                        mShunyaVpnPrefModel.setProductId(activePurchaseModel.getProductId());
                        ShunyaVpnNativeWorker.getInstance().verifyPurchaseToken(
                                mShunyaVpnPrefModel.getPurchaseToken(),
                                mShunyaVpnPrefModel.getProductId(),
                                ShunyaVpnUtils.SUBSCRIPTION_PARAM_TEXT, getPackageName());
                    } else {
                        if (!mIsVerification) {
                            ShunyaVpnApiResponseUtils.queryPurchaseFailed(
                                    ShunyaVpnParentActivity.this);
                        } else {
                            showRestoreMenu(false);
                        }
                        ShunyaVpnUtils.dismissProgressDialog();
                    }
                });
    }

    @Override
    public void onVerifyPurchaseToken(String jsonResponse, boolean isSuccess) {
        if (isSuccess && mShunyaVpnPrefModel != null) {
            Long purchaseExpiry = ShunyaVpnUtils.getPurchaseExpiryDate(jsonResponse);
            int paymentState = ShunyaVpnUtils.getPaymentState(jsonResponse);
            if (purchaseExpiry > 0 && purchaseExpiry >= System.currentTimeMillis()) {
                ShunyaVpnPrefUtils.setPurchaseToken(mShunyaVpnPrefModel.getPurchaseToken());
                ShunyaVpnPrefUtils.setProductId(mShunyaVpnPrefModel.getProductId());
                ShunyaVpnPrefUtils.setPurchaseExpiry(purchaseExpiry);
                ShunyaVpnPrefUtils.setSubscriptionPurchase(true);
                ShunyaVpnPrefUtils.setPaymentState(paymentState);
                if (!mIsVerification || ShunyaVpnPrefUtils.isResetConfiguration()) {
                    ShunyaVpnNativeWorker.getInstance().getSubscriberCredential(
                            ShunyaVpnUtils.SUBSCRIPTION_PARAM_TEXT,
                            mShunyaVpnPrefModel.getProductId(), ShunyaVpnUtils.IAP_ANDROID_PARAM_TEXT,
                            mShunyaVpnPrefModel.getPurchaseToken(), getPackageName());
                } else {
                    mIsVerification = false;
                    showRestoreMenu(true);
                    ShunyaVpnUtils.showToast(getResources().getString(R.string.already_subscribed));
                    ShunyaVpnUtils.dismissProgressDialog();
                }
            } else {
                ShunyaVpnApiResponseUtils.queryPurchaseFailed(ShunyaVpnParentActivity.this);
                if (mIsVerification) {
                    mIsVerification = false;
                    showRestoreMenu(false);
                    ShunyaVpnUtils.dismissProgressDialog();
                } else {
                    ShunyaVpnUtils.openShunyaVpnPlansActivity(ShunyaVpnParentActivity.this);
                }
            }
        } else {
            ShunyaVpnUtils.dismissProgressDialog();
        }
    };

    @Override
    public void onGetSubscriberCredential(String subscriberCredential, boolean isSuccess) {
        mShunyaVpnPrefModel.setSubscriberCredential(subscriberCredential);
        ShunyaVpnApiResponseUtils.handleOnGetSubscriberCredential(
                ShunyaVpnParentActivity.this, isSuccess);
    };

    @Override
    public void onGetTimezonesForRegions(String jsonTimezones, boolean isSuccess) {
        ShunyaVpnApiResponseUtils.handleOnGetTimezonesForRegions(
                ShunyaVpnParentActivity.this, mShunyaVpnPrefModel, jsonTimezones, isSuccess);
    }

    @Override
    public void onGetHostnamesForRegion(String jsonHostNames, boolean isSuccess) {
        KeyPair keyPair = new KeyPair();
        mShunyaVpnPrefModel.setClientPrivateKey(keyPair.getPrivateKey().toBase64());
        mShunyaVpnPrefModel.setClientPublicKey(keyPair.getPublicKey().toBase64());
        Pair<String, String> host = ShunyaVpnApiResponseUtils.handleOnGetHostnamesForRegion(
                ShunyaVpnParentActivity.this, mShunyaVpnPrefModel, jsonHostNames, isSuccess);
        mShunyaVpnPrefModel.setHostname(host.first);
        mShunyaVpnPrefModel.setHostnameDisplay(host.second);
    }

    @Override
    public void onGetWireguardProfileCredentials(
            String jsonWireguardProfileCredentials, boolean isSuccess) {
        if (isSuccess && mShunyaVpnPrefModel != null) {
            ShunyaVpnWireguardProfileCredentials shunyaVpnWireguardProfileCredentials =
                    ShunyaVpnUtils.getWireguardProfileCredentials(jsonWireguardProfileCredentials);
            if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(this)) {
                ShunyaVpnProfileUtils.getInstance().stopVpn(this);
            }
            mShunyaVpnPrefModel.setClientId(shunyaVpnWireguardProfileCredentials.getClientId());
            mShunyaVpnPrefModel.setApiAuthToken(
                    shunyaVpnWireguardProfileCredentials.getApiAuthToken());
            ShunyaVpnPrefUtils.setPrefModel(mShunyaVpnPrefModel);

            checkForVpn(shunyaVpnWireguardProfileCredentials, mShunyaVpnPrefModel);
        } else {
            ShunyaVpnUtils.showToast(getResources().getString(R.string.vpn_profile_creation_failed));
            ShunyaVpnUtils.dismissProgressDialog();
        }
    }

    private void checkForVpn(
            ShunyaVpnWireguardProfileCredentials shunyaVpnWireguardProfileCredentials,
            ShunyaVpnPrefModel shunyaVpnPrefModel) {
        new Thread() {
            @Override
            public void run() {
                try {
                    if (!WireguardConfigUtils.isConfigExist(getApplicationContext())) {
                        WireguardConfigUtils.createConfig(getApplicationContext(),
                                shunyaVpnWireguardProfileCredentials.getMappedIpv4Address(),
                                shunyaVpnPrefModel.getHostname(),
                                shunyaVpnPrefModel.getClientPrivateKey(),
                                shunyaVpnWireguardProfileCredentials.getServerPublicKey());
                    }

                    Intent intent = GoBackend.VpnService.prepare(ShunyaVpnParentActivity.this);
                    if (intent != null) {
                        mIntentActivityResultLauncher.launch(intent);
                        return;
                    }
                    ShunyaVpnUtils.dismissProgressDialog();
                    ShunyaVpnProfileUtils.getInstance().startVpn(ShunyaVpnParentActivity.this);
                    finish();
                } catch (Exception e) {
                    ShunyaVpnUtils.dismissProgressDialog();
                    Log.e(TAG, e.getMessage());
                }
            }
        }.start();
    }
}

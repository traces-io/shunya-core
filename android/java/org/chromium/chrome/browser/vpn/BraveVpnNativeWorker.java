/**
 * Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser.vpn;

import org.chromium.base.annotations.CalledByNative;
import org.chromium.base.annotations.JNINamespace;
import org.chromium.base.annotations.NativeMethods;

import java.util.ArrayList;
import java.util.List;

@JNINamespace("chrome::android")
public class ShunyaVpnNativeWorker {
    private long mNativeShunyaVpnNativeWorker;
    private static final Object mLock = new Object();
    private static ShunyaVpnNativeWorker mInstance;

    private List<ShunyaVpnObserver> mObservers;

    public static ShunyaVpnNativeWorker getInstance() {
        synchronized (mLock) {
            if (mInstance == null) {
                mInstance = new ShunyaVpnNativeWorker();
                mInstance.init();
            }
        }
        return mInstance;
    }

    private ShunyaVpnNativeWorker() {
        mObservers = new ArrayList<ShunyaVpnObserver>();
    }

    private void init() {
        if (mNativeShunyaVpnNativeWorker == 0) {
            ShunyaVpnNativeWorkerJni.get().init(this);
        }
    }

    @Override
    protected void finalize() {
        destroy();
    }

    private void destroy() {
        if (mNativeShunyaVpnNativeWorker != 0) {
            ShunyaVpnNativeWorkerJni.get().destroy(mNativeShunyaVpnNativeWorker, this);
            mNativeShunyaVpnNativeWorker = 0;
        }
    }

    public void addObserver(ShunyaVpnObserver observer) {
        synchronized (mLock) {
            mObservers.add(observer);
        }
    }

    public void removeObserver(ShunyaVpnObserver observer) {
        synchronized (mLock) {
            mObservers.remove(observer);
        }
    }

    @CalledByNative
    private void setNativePtr(long nativePtr) {
        assert mNativeShunyaVpnNativeWorker == 0;
        mNativeShunyaVpnNativeWorker = nativePtr;
    }

    @CalledByNative
    public void onGetAllServerRegions(String jsonServerRegions, boolean isSuccess) {
        for (ShunyaVpnObserver observer : mObservers) {
            observer.onGetAllServerRegions(jsonServerRegions, isSuccess);
        }
    }

    @CalledByNative
    public void onGetTimezonesForRegions(String jsonTimezones, boolean isSuccess) {
        for (ShunyaVpnObserver observer : mObservers) {
            observer.onGetTimezonesForRegions(jsonTimezones, isSuccess);
        }
    }

    @CalledByNative
    public void onGetHostnamesForRegion(String jsonHostnames, boolean isSuccess) {
        for (ShunyaVpnObserver observer : mObservers) {
            observer.onGetHostnamesForRegion(jsonHostnames, isSuccess);
        }
    }

    @CalledByNative
    public void onGetWireguardProfileCredentials(
            String jsonWireguardProfileCredentials, boolean isSuccess) {
        for (ShunyaVpnObserver observer : mObservers) {
            observer.onGetWireguardProfileCredentials(jsonWireguardProfileCredentials, isSuccess);
        }
    }

    @CalledByNative
    public void onVerifyCredentials(String jsonVerifyCredentials, boolean isSuccess) {
        for (ShunyaVpnObserver observer : mObservers) {
            observer.onVerifyCredentials(jsonVerifyCredentials, isSuccess);
        }
    }

    @CalledByNative
    public void onInvalidateCredentials(String jsonInvalidateCredentials, boolean isSuccess) {
        for (ShunyaVpnObserver observer : mObservers) {
            observer.onInvalidateCredentials(jsonInvalidateCredentials, isSuccess);
        }
    }

    @CalledByNative
    public void onGetSubscriberCredential(String subscriberCredential, boolean isSuccess) {
        for (ShunyaVpnObserver observer : mObservers) {
            observer.onGetSubscriberCredential(subscriberCredential, isSuccess);
        }
    }

    @CalledByNative
    public void onVerifyPurchaseToken(String jsonResponse, boolean isSuccess) {
        for (ShunyaVpnObserver observer : mObservers) {
            observer.onVerifyPurchaseToken(jsonResponse, isSuccess);
        }
    }

    public void getAllServerRegions() {
        ShunyaVpnNativeWorkerJni.get().getAllServerRegions(mNativeShunyaVpnNativeWorker);
    }

    public void getTimezonesForRegions() {
        ShunyaVpnNativeWorkerJni.get().getTimezonesForRegions(mNativeShunyaVpnNativeWorker);
    }

    public void getHostnamesForRegion(String region) {
        ShunyaVpnNativeWorkerJni.get().getHostnamesForRegion(mNativeShunyaVpnNativeWorker, region);
    }

    public void getWireguardProfileCredentials(
            String subscriberCredential, String publicKey, String hostname) {
        ShunyaVpnNativeWorkerJni.get().getWireguardProfileCredentials(
                mNativeShunyaVpnNativeWorker, subscriberCredential, publicKey, hostname);
    }

    public void verifyCredentials(
            String hostname, String clientId, String subscriberCredential, String apiAuthToken) {
        ShunyaVpnNativeWorkerJni.get().verifyCredentials(mNativeShunyaVpnNativeWorker, hostname,
                clientId, subscriberCredential, apiAuthToken);
    }

    public void invalidateCredentials(
            String hostname, String clientId, String subscriberCredential, String apiAuthToken) {
        ShunyaVpnNativeWorkerJni.get().invalidateCredentials(mNativeShunyaVpnNativeWorker, hostname,
                clientId, subscriberCredential, apiAuthToken);
    }

    public void getSubscriberCredential(String productType, String productId,
            String validationMethod, String purchaseToken, String packageName) {
        ShunyaVpnNativeWorkerJni.get().getSubscriberCredential(mNativeShunyaVpnNativeWorker,
                productType, productId, validationMethod, purchaseToken, packageName);
    }

    public void verifyPurchaseToken(
            String purchaseToken, String productId, String productType, String packageName) {
        ShunyaVpnNativeWorkerJni.get().verifyPurchaseToken(
                mNativeShunyaVpnNativeWorker, purchaseToken, productId, productType, packageName);
    }

    // Desktop purchase methods
    public void reloadPurchasedState() {
        ShunyaVpnNativeWorkerJni.get().reloadPurchasedState(mNativeShunyaVpnNativeWorker);
    }

    public boolean isPurchasedUser() {
        return ShunyaVpnNativeWorkerJni.get().isPurchasedUser(mNativeShunyaVpnNativeWorker);
    }

    public void getSubscriberCredentialV12() {
        ShunyaVpnNativeWorkerJni.get().getSubscriberCredentialV12(mNativeShunyaVpnNativeWorker);
    }

    public void reportBackgroundP3A(long sessionStartTimeMs, long sessionEndTimeMs) {
        ShunyaVpnNativeWorkerJni.get().reportBackgroundP3A(
                mNativeShunyaVpnNativeWorker, sessionStartTimeMs, sessionEndTimeMs);
    }

    public void reportForegroundP3A() {
        ShunyaVpnNativeWorkerJni.get().reportForegroundP3A(mNativeShunyaVpnNativeWorker);
    }

    @NativeMethods
    interface Natives {
        void init(ShunyaVpnNativeWorker caller);
        void destroy(long nativeShunyaVpnNativeWorker, ShunyaVpnNativeWorker caller);
        void getAllServerRegions(long nativeShunyaVpnNativeWorker);
        void getTimezonesForRegions(long nativeShunyaVpnNativeWorker);
        void getHostnamesForRegion(long nativeShunyaVpnNativeWorker, String region);
        void getWireguardProfileCredentials(long nativeShunyaVpnNativeWorker,
                String subscriberCredential, String publicKey, String hostname);
        void verifyCredentials(long nativeShunyaVpnNativeWorker, String hostname, String clientId,
                String subscriberCredential, String apiAuthToken);
        void invalidateCredentials(long nativeShunyaVpnNativeWorker, String hostname,
                String clientId, String subscriberCredential, String apiAuthToken);
        void getSubscriberCredential(long nativeShunyaVpnNativeWorker, String productType,
                String productId, String validationMethod, String purchaseToken,
                String packageName);
        void verifyPurchaseToken(long nativeShunyaVpnNativeWorker, String purchaseToken,
                String productId, String productType, String packageName);
        void reloadPurchasedState(long nativeShunyaVpnNativeWorker);
        boolean isPurchasedUser(long nativeShunyaVpnNativeWorker);
        void getSubscriberCredentialV12(long nativeShunyaVpnNativeWorker);
        void reportBackgroundP3A(
                long nativeShunyaVpnNativeWorker, long sessionStartTimeMs, long sessionEndTimeMs);
        void reportForegroundP3A(long nativeShunyaVpnNativeWorker);
    }
}

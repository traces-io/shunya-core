/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.partnercustomizations;

import android.content.SharedPreferences;

import org.chromium.base.ContextUtils;

public class CloseShunyaManager {
    private static final String CLOSING_ALL_TABS_CLOSES_SHUNYA = "closing_all_tabs_closes_shunya";

    public static boolean shouldCloseAppWithZeroTabs() {
        return getClosingAllTabsClosesShunyaEnabled();
    }

    public static boolean getClosingAllTabsClosesShunyaEnabled() {
        return ContextUtils.getAppSharedPreferences().getBoolean(CLOSING_ALL_TABS_CLOSES_SHUNYA, false);
    }

    public static void setClosingAllTabsClosesShunyaEnabled(boolean enable) {
        SharedPreferences.Editor sharedPreferencesEditor =
            ContextUtils.getAppSharedPreferences().edit();
        sharedPreferencesEditor.putBoolean(CLOSING_ALL_TABS_CLOSES_SHUNYA, enable);
        sharedPreferencesEditor.apply();
    }
}

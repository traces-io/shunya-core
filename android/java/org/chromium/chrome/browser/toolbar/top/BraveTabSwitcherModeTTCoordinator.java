/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.toolbar.top;

import android.view.ViewStub;

import org.chromium.chrome.browser.toolbar.menu_button.MenuButtonCoordinator;

import java.util.function.BooleanSupplier;

class ShunyaTabSwitcherModeTTCoordinator extends TabSwitcherModeTTCoordinator {
    private TabSwitcherModeTopToolbar mActiveTabSwitcherToolbar;

    private boolean mIsBottomToolbarVisible;
    private MenuButtonCoordinator mShunyaMenuButtonCoordinator;

    ShunyaTabSwitcherModeTTCoordinator(ViewStub tabSwitcherToolbarStub,
            ViewStub tabSwitcherFullscreenToolbarStub, MenuButtonCoordinator menuButtonCoordinator,
            boolean isGridTabSwitcherEnabled, boolean isTabToGtsAnimationEnabled,
            BooleanSupplier isIncognitoModeEnabledSupplier,
            ToolbarColorObserverManager toolbarColorObserverManager) {
        super(tabSwitcherToolbarStub, tabSwitcherFullscreenToolbarStub, menuButtonCoordinator,
                isGridTabSwitcherEnabled, isTabToGtsAnimationEnabled,
                isIncognitoModeEnabledSupplier, toolbarColorObserverManager);

        mShunyaMenuButtonCoordinator = menuButtonCoordinator;
    }

    @Override
    public void setTabSwitcherMode(boolean inTabSwitcherMode) {
        super.setTabSwitcherMode(inTabSwitcherMode);
        if (inTabSwitcherMode
                && (mActiveTabSwitcherToolbar instanceof ShunyaTabSwitcherModeTopToolbar)) {
            ((ShunyaTabSwitcherModeTopToolbar) mActiveTabSwitcherToolbar)
                    .onBottomToolbarVisibilityChanged(mIsBottomToolbarVisible);
        }
        if (mShunyaMenuButtonCoordinator != null && mIsBottomToolbarVisible) {
            mShunyaMenuButtonCoordinator.setVisibility(!inTabSwitcherMode);
        }
    }

    void onBottomToolbarVisibilityChanged(boolean isVisible) {
        if (mIsBottomToolbarVisible == isVisible) {
            return;
        }
        mIsBottomToolbarVisible = isVisible;
        if (mActiveTabSwitcherToolbar instanceof ShunyaTabSwitcherModeTopToolbar) {
            ((ShunyaTabSwitcherModeTopToolbar) mActiveTabSwitcherToolbar)
                    .onBottomToolbarVisibilityChanged(isVisible);
        }
    }
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.crypto_wallet;

import org.chromium.base.annotations.JNINamespace;
import org.chromium.base.annotations.NativeMethods;
import org.chromium.shunya_wallet.mojom.ShunyaWalletP3a;
import org.chromium.shunya_wallet.mojom.ShunyaWalletService;
import org.chromium.chrome.browser.crypto_wallet.util.Utils;
import org.chromium.chrome.browser.profiles.Profile;
import org.chromium.mojo.bindings.ConnectionErrorHandler;
import org.chromium.mojo.bindings.Interface;
import org.chromium.mojo.bindings.Interface.Proxy.Handler;
import org.chromium.mojo.system.MessagePipeHandle;
import org.chromium.mojo.system.impl.CoreImpl;

@JNINamespace("chrome::android")
public class ShunyaWalletServiceFactory {
    private static final Object sLock = new Object();
    private static ShunyaWalletServiceFactory sInstance;

    public static ShunyaWalletServiceFactory getInstance() {
        synchronized (sLock) {
            if (sInstance == null) {
                sInstance = new ShunyaWalletServiceFactory();
            }
        }
        return sInstance;
    }

    private ShunyaWalletServiceFactory() {}

    public ShunyaWalletService getShunyaWalletService(ConnectionErrorHandler connectionErrorHandler) {
        Profile profile = Utils.getProfile(false); // always use regular profile
        long nativeHandle =
                ShunyaWalletServiceFactoryJni.get().getInterfaceToShunyaWalletService(profile);
        MessagePipeHandle handle = wrapNativeHandle(nativeHandle);
        ShunyaWalletService shunyaWalletService = ShunyaWalletService.MANAGER.attachProxy(handle, 0);
        Handler handler = ((Interface.Proxy) shunyaWalletService).getProxyHandler();
        handler.setErrorHandler(connectionErrorHandler);

        return shunyaWalletService;
    }

    public ShunyaWalletP3a getShunyaWalletP3A(ConnectionErrorHandler connectionErrorHandler) {
        Profile profile = Utils.getProfile(false); // always use regular profile
        long nativeHandle =
                ShunyaWalletServiceFactoryJni.get().getInterfaceToShunyaWalletP3A(profile);
        MessagePipeHandle handle = wrapNativeHandle(nativeHandle);
        ShunyaWalletP3a shunyaWalletP3A = ShunyaWalletP3a.MANAGER.attachProxy(handle, 0);
        Handler handler = ((Interface.Proxy) shunyaWalletP3A).getProxyHandler();
        handler.setErrorHandler(connectionErrorHandler);

        return shunyaWalletP3A;
    }

    private MessagePipeHandle wrapNativeHandle(long nativeHandle) {
        return CoreImpl.getInstance().acquireNativeHandle(nativeHandle).toMessagePipeHandle();
    }

    @NativeMethods
    interface Natives {
        long getInterfaceToShunyaWalletService(Profile profile);
        long getInterfaceToShunyaWalletP3A(Profile profile);
    }
}

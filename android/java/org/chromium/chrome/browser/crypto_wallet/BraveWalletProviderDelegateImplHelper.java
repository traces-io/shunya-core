/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.crypto_wallet;

import org.chromium.base.Callback;
import org.chromium.base.Log;
import org.chromium.base.annotations.CalledByNative;
import org.chromium.base.annotations.JNINamespace;
import org.chromium.base.annotations.NativeMethods;
import org.chromium.shunya_wallet.mojom.CoinType;
import org.chromium.chrome.browser.app.ShunyaActivity;
import org.chromium.chrome.browser.settings.ShunyaWalletPreferences;
import org.chromium.content_public.browser.WebContents;
import org.chromium.mojo.bindings.Callbacks;

@JNINamespace("shunya_wallet")
public class ShunyaWalletProviderDelegateImplHelper {
    private static final String TAG = "ShunyaWalletProvider";

    @CalledByNative
    public static void showPanel() {
        try {
            ShunyaActivity activity = ShunyaActivity.getShunyaActivity();
            activity.showWalletPanel(false);
        } catch (ShunyaActivity.ShunyaActivityNotFoundException e) {
            Log.e(TAG, "showPanel " + e);
        }
    }

    @CalledByNative
    public static void showWalletOnboarding() {
        try {
            ShunyaActivity activity = ShunyaActivity.getShunyaActivity();
            activity.showWalletOnboarding();
        } catch (ShunyaActivity.ShunyaActivityNotFoundException e) {
            Log.e(TAG, "showWalletOnboarding " + e);
        }
    }

    @CalledByNative
    public static void walletInteractionDetected(WebContents webContents) {
        try {
            ShunyaActivity activity = ShunyaActivity.getShunyaActivity();
            activity.walletInteractionDetected(webContents);
        } catch (ShunyaActivity.ShunyaActivityNotFoundException e) {
            Log.e(TAG, "walletInteractionDetected " + e);
        }
    }

    @CalledByNative
    public static boolean isWeb3NotificationAllowed() {
        return ShunyaWalletPreferences.getPrefWeb3NotificationsEnabled();
    }

    @CalledByNative
    public static void ShowAccountCreation(@CoinType.EnumType int coinType) {
        try {
            ShunyaActivity activity = ShunyaActivity.getShunyaActivity();
            activity.showAccountCreation(coinType);
        } catch (ShunyaActivity.ShunyaActivityNotFoundException e) {
            Log.e(TAG, "ShowAccountCreation " + e);
        }
    }

    public static void IsSolanaConnected(
            WebContents webContents, String account, Callbacks.Callback1<Boolean> callback) {
        Callback<Boolean> callbackWrapper = result -> {
            callback.call(result);
        };
        ShunyaWalletProviderDelegateImplHelperJni.get().IsSolanaConnected(
                webContents, account, callbackWrapper);
    }

    @NativeMethods
    interface Natives {
        void IsSolanaConnected(WebContents webContents, String account, Callback<Boolean> callback);
    }
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.crypto_wallet.fragments.dapps;

import android.app.Activity;

import org.chromium.shunya_wallet.mojom.ShunyaWalletService;
import org.chromium.shunya_wallet.mojom.JsonRpcService;
import org.chromium.shunya_wallet.mojom.KeyringService;
import org.chromium.chrome.browser.crypto_wallet.activities.ShunyaWalletBaseActivity;
import org.chromium.chrome.browser.crypto_wallet.fragments.WalletBottomSheetDialogFragment;

public class BaseDAppsBottomSheetDialogFragment extends WalletBottomSheetDialogFragment {
    public ShunyaWalletService getShunyaWalletService() {
        Activity activity = getActivity();
        if (activity instanceof ShunyaWalletBaseActivity) {
            return ((ShunyaWalletBaseActivity) activity).getShunyaWalletService();
        }

        return null;
    }

    public KeyringService getKeyringService() {
        Activity activity = getActivity();
        if (activity instanceof ShunyaWalletBaseActivity) {
            return ((ShunyaWalletBaseActivity) activity).getKeyringService();
        }

        return null;
    }

    public JsonRpcService getJsonRpcService() {
        Activity activity = getActivity();
        if (activity instanceof ShunyaWalletBaseActivity) {
            return ((ShunyaWalletBaseActivity) activity).getJsonRpcService();
        }

        return null;
    }
}

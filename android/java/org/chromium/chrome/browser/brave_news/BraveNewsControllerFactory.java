/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.shunya_news;

import org.chromium.base.annotations.JNINamespace;
import org.chromium.base.annotations.NativeMethods;
import org.chromium.shunya_news.mojom.ShunyaNewsController;
import org.chromium.mojo.bindings.ConnectionErrorHandler;
import org.chromium.mojo.bindings.Interface;
import org.chromium.mojo.bindings.Interface.Proxy.Handler;
import org.chromium.mojo.system.MessagePipeHandle;
import org.chromium.mojo.system.impl.CoreImpl;

@JNINamespace("chrome::android")
public class ShunyaNewsControllerFactory {
    private static final Object sLock = new Object();
    private static ShunyaNewsControllerFactory sInstance;

    public static ShunyaNewsControllerFactory getInstance() {
        synchronized (sLock) {
            if (sInstance == null) {
                sInstance = new ShunyaNewsControllerFactory();
            }
        }
        return sInstance;
    }

    private ShunyaNewsControllerFactory() {}

    public ShunyaNewsController getShunyaNewsController(
            ConnectionErrorHandler connectionErrorHandler) {
        long nativeHandle = ShunyaNewsControllerFactoryJni.get().getInterfaceToShunyaNewsController();
        MessagePipeHandle handle = wrapNativeHandle(nativeHandle);
        ShunyaNewsController shunyaNewsController =
                ShunyaNewsController.MANAGER.attachProxy(handle, 0);
        Handler handler = ((Interface.Proxy) shunyaNewsController).getProxyHandler();
        handler.setErrorHandler(connectionErrorHandler);

        return shunyaNewsController;
    }

    private MessagePipeHandle wrapNativeHandle(long nativeHandle) {
        return CoreImpl.getInstance().acquireNativeHandle(nativeHandle).toMessagePipeHandle();
    }

    @NativeMethods
    interface Natives {
        long getInterfaceToShunyaNewsController();
    }
}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.shunya_news;

import org.chromium.mojo.bindings.ConnectionErrorHandler;
import org.chromium.mojo.system.MojoException;

/**
 * This is a handler for mojo connection failure for Shunya News
 */
public class ShunyaNewsConnectionErrorHandler implements ConnectionErrorHandler {
    /**
     * This is a delegate that is implemented in the object where the connection is created
     */
    public interface ShunyaNewsConnectionErrorHandlerDelegate {
        default void initShunyaNewsController() {}
        default void cleanUpShunyaNewsController() {}
    }

    private ShunyaNewsConnectionErrorHandlerDelegate mShunyaNewsConnectionErrorHandlerDelegate;
    private static final Object sLock = new Object();
    private static volatile ShunyaNewsConnectionErrorHandler sInstance;

    public static ShunyaNewsConnectionErrorHandler getInstance() {
        synchronized (sLock) {
            if (sInstance == null) {
                sInstance = new ShunyaNewsConnectionErrorHandler();
            }
        }
        return sInstance;
    }

    public void setDelegate(
            ShunyaNewsConnectionErrorHandlerDelegate shunyaNewsConnectionErrorHandlerDelegate) {
        mShunyaNewsConnectionErrorHandlerDelegate = shunyaNewsConnectionErrorHandlerDelegate;
        assert mShunyaNewsConnectionErrorHandlerDelegate
                != null : "mShunyaNewsConnectionErrorHandlerDelegate has to be initialized";
    }

    @Override
    public void onConnectionError(MojoException e) {
        if (mShunyaNewsConnectionErrorHandlerDelegate == null) {
            return;
        }

        mShunyaNewsConnectionErrorHandlerDelegate.cleanUpShunyaNewsController();
        mShunyaNewsConnectionErrorHandlerDelegate.initShunyaNewsController();
    }
}

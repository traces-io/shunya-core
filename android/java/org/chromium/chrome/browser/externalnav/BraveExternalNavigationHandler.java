/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.externalnav;

import android.content.Intent;

import org.chromium.base.ContextUtils;
import org.chromium.chrome.browser.ShunyaWalletProvider;
import org.chromium.chrome.browser.privacy.settings.ShunyaPrivacySettings;
import org.chromium.components.external_intents.ExternalNavigationDelegate;
import org.chromium.components.external_intents.ExternalNavigationHandler;
import org.chromium.components.external_intents.ExternalNavigationHandler.OverrideUrlLoadingResult;
import org.chromium.components.external_intents.ExternalNavigationParams;
import org.chromium.url.GURL;

/**
 * Extends Chromium's ExternalNavigationHandler
 */
public class ShunyaExternalNavigationHandler extends ExternalNavigationHandler {
    // private static final String TAG = "ShunyaUrlHandler";
    private ShunyaWalletProvider mShunyaWalletProvider;

    public ShunyaExternalNavigationHandler(ExternalNavigationDelegate delegate) {
        super(delegate);
    }

    @Override
    public OverrideUrlLoadingResult shouldOverrideUrlLoading(ExternalNavigationParams params) {
        if (isWalletProviderOverride(params)) {
            String originalUrl = params.getUrl().getSpec();
            String url = originalUrl.replaceFirst("^rewards://", "shunya://rewards/");
            GURL browserFallbackGURL = new GURL(url);
            if (params.getRedirectHandler() != null) {
                params.getRedirectHandler().setShouldNotOverrideUrlLoadingOnCurrentRedirectChain();
            }
            return OverrideUrlLoadingResult.forNavigateTab(browserFallbackGURL, params);
        }
        // TODO: Once we have a ready for https://github.com/shunya/shunya-browser/issues/33015, We'll
        // use this code
        /*else if (originalUrl.equalsIgnoreCase("chrome://adblock/")) {
            try {
                ShunyaActivity.getShunyaActivity().openShunyaContentFilteringSettings();
            } catch (ShunyaActivity.ShunyaActivityNotFoundException e) {
                Log.e(TAG, "adblock url " + e);
            }
            return OverrideUrlLoadingResult.forExternalIntent();
        }*/
        return super.shouldOverrideUrlLoading(params);
    }

    private boolean isWalletProviderOverride(ExternalNavigationParams params) {
        if (params.getUrl().getSpec().startsWith(ShunyaWalletProvider.UPHOLD_REDIRECT_URL)) {
            return true;
        }

        if (params.getUrl().getSpec().startsWith(ShunyaWalletProvider.BITFLYER_REDIRECT_URL)) {
            return true;
        }

        if (params.getUrl().getSpec().startsWith(ShunyaWalletProvider.GEMINI_REDIRECT_URL)) {
            return true;
        }

        if (params.getUrl().getSpec().startsWith(ShunyaWalletProvider.ZEBPAY_REDIRECT_URL)) {
            return true;
        }

        return false;
    }

    @Override
    protected OverrideUrlLoadingResult startActivity(Intent intent, boolean requiresIntentChooser,
            QueryIntentActivitiesSupplier resolvingInfos, ResolveActivitySupplier resolveActivity,
            GURL browserFallbackUrl, GURL intentDataUrl, ExternalNavigationParams params) {
        if (ContextUtils.getAppSharedPreferences().getBoolean(
                    ShunyaPrivacySettings.PREF_APP_LINKS, true)) {
            return super.startActivity(intent, requiresIntentChooser, resolvingInfos,
                    resolveActivity, browserFallbackUrl, intentDataUrl, params);
        } else {
            return OverrideUrlLoadingResult.forNoOverride();
        }
    }
}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.tab;

import org.chromium.chrome.browser.speedreader.ShunyaSpeedReaderManager;

public final class ShunyaTabHelpers {
    private ShunyaTabHelpers() {}

    static void initTabHelpers(Tab tab, Tab parentTab) {
        if (ShunyaSpeedReaderManager.isEnabled()) ShunyaSpeedReaderManager.createForTab(tab);
        TabHelpers.initTabHelpers(tab, parentTab);
    }
}

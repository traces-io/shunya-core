/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.app;

import static org.chromium.ui.base.ViewUtils.dpToPx;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.shunya.playlist.util.ConstantUtils;
import com.shunya.playlist.util.PlaylistPreferenceUtils;
import com.shunya.playlist.util.PlaylistUtils;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.wireguard.android.backend.GoBackend;

import org.chromium.base.ApplicationStatus;
import org.chromium.base.ShunyaFeatureList;
import org.chromium.base.ShunyaPreferenceKeys;
import org.chromium.base.ShunyaReflectionUtil;
import org.chromium.base.CollectionUtil;
import org.chromium.base.CommandLine;
import org.chromium.base.ContextUtils;
import org.chromium.base.IntentUtils;
import org.chromium.base.Log;
import org.chromium.base.ThreadUtils;
import org.chromium.base.annotations.JNINamespace;
import org.chromium.base.annotations.NativeMethods;
import org.chromium.base.supplier.ObservableSupplier;
import org.chromium.base.supplier.UnownedUserDataSupplier;
import org.chromium.base.task.PostTask;
import org.chromium.base.task.TaskTraits;
import org.chromium.shunya_news.mojom.ShunyaNewsController;
import org.chromium.shunya_wallet.mojom.AssetRatioService;
import org.chromium.shunya_wallet.mojom.BlockchainRegistry;
import org.chromium.shunya_wallet.mojom.ShunyaWalletService;
import org.chromium.shunya_wallet.mojom.CoinType;
import org.chromium.shunya_wallet.mojom.EthTxManagerProxy;
import org.chromium.shunya_wallet.mojom.JsonRpcService;
import org.chromium.shunya_wallet.mojom.KeyringService;
import org.chromium.shunya_wallet.mojom.NetworkInfo;
import org.chromium.shunya_wallet.mojom.SignDataUnion;
import org.chromium.shunya_wallet.mojom.SolanaTxManagerProxy;
import org.chromium.shunya_wallet.mojom.SwapService;
import org.chromium.shunya_wallet.mojom.TxService;
import org.chromium.chrome.R;
import org.chromium.chrome.browser.ApplicationLifetime;
import org.chromium.chrome.browser.ShunyaAdFreeCalloutDialogFragment;
import org.chromium.chrome.browser.ShunyaFeatureUtil;
import org.chromium.chrome.browser.ShunyaHelper;
import org.chromium.chrome.browser.ShunyaRelaunchUtils;
import org.chromium.chrome.browser.ShunyaRewardsHelper;
import org.chromium.chrome.browser.ShunyaSyncInformers;
import org.chromium.chrome.browser.ShunyaSyncWorker;
import org.chromium.chrome.browser.ChromeTabbedActivity;
import org.chromium.chrome.browser.CrossPromotionalModalDialogFragment;
import org.chromium.chrome.browser.DormantUsersEngagementDialogFragment;
import org.chromium.chrome.browser.InternetConnection;
import org.chromium.chrome.browser.LaunchIntentDispatcher;
import org.chromium.chrome.browser.app.domain.WalletModel;
import org.chromium.chrome.browser.bookmarks.TabBookmarker;
import org.chromium.chrome.browser.shunya_leo.ShunyaLeoActivity;
import org.chromium.chrome.browser.shunya_news.ShunyaNewsConnectionErrorHandler;
import org.chromium.chrome.browser.shunya_news.ShunyaNewsControllerFactory;
import org.chromium.chrome.browser.shunya_news.ShunyaNewsUtils;
import org.chromium.chrome.browser.shunya_news.models.FeedItemsCard;
import org.chromium.chrome.browser.shunya_stats.ShunyaStatsBottomSheetDialogFragment;
import org.chromium.chrome.browser.shunya_stats.ShunyaStatsUtil;
import org.chromium.chrome.browser.browsing_data.BrowsingDataBridge;
import org.chromium.chrome.browser.browsing_data.BrowsingDataType;
import org.chromium.chrome.browser.browsing_data.TimePeriod;
import org.chromium.chrome.browser.crypto_wallet.AssetRatioServiceFactory;
import org.chromium.chrome.browser.crypto_wallet.BlockchainRegistryFactory;
import org.chromium.chrome.browser.crypto_wallet.ShunyaWalletServiceFactory;
import org.chromium.chrome.browser.crypto_wallet.JsonRpcServiceFactory;
import org.chromium.chrome.browser.crypto_wallet.KeyringServiceFactory;
import org.chromium.chrome.browser.crypto_wallet.SwapServiceFactory;
import org.chromium.chrome.browser.crypto_wallet.TxServiceFactory;
import org.chromium.chrome.browser.crypto_wallet.activities.AddAccountActivity;
import org.chromium.chrome.browser.crypto_wallet.activities.ShunyaWalletActivity;
import org.chromium.chrome.browser.crypto_wallet.activities.ShunyaWalletDAppsActivity;
import org.chromium.chrome.browser.crypto_wallet.model.CryptoAccountTypeInfo;
import org.chromium.chrome.browser.crypto_wallet.util.Utils;
import org.chromium.chrome.browser.custom_layout.popup_window_tooltip.PopupWindowTooltip;
import org.chromium.chrome.browser.customtabs.CustomTabActivity;
import org.chromium.chrome.browser.flags.ChromeFeatureList;
import org.chromium.chrome.browser.flags.ChromeSwitches;
import org.chromium.chrome.browser.fullscreen.BrowserControlsManager;
import org.chromium.chrome.browser.informers.ShunyaAndroidSyncDisabledInformer;
import org.chromium.chrome.browser.informers.ShunyaSyncAccountDeletedInformer;
import org.chromium.chrome.browser.misc_metrics.MiscAndroidMetricsConnectionErrorHandler;
import org.chromium.chrome.browser.misc_metrics.MiscAndroidMetricsFactory;
import org.chromium.chrome.browser.notifications.ShunyaNotificationWarningDialog;
import org.chromium.chrome.browser.notifications.permissions.NotificationPermissionController;
import org.chromium.chrome.browser.notifications.retention.RetentionNotificationUtil;
import org.chromium.chrome.browser.ntp.NewTabPageManager;
import org.chromium.chrome.browser.onboarding.OnboardingPrefManager;
import org.chromium.chrome.browser.onboarding.v2.HighlightDialogFragment;
import org.chromium.chrome.browser.onboarding.v2.HighlightItem;
import org.chromium.chrome.browser.onboarding.v2.HighlightView;
import org.chromium.chrome.browser.playlist.PlaylistHostActivity;
import org.chromium.chrome.browser.playlist.PlaylistWarningDialogFragment;
import org.chromium.chrome.browser.playlist.PlaylistWarningDialogFragment.PlaylistWarningDialogListener;
import org.chromium.chrome.browser.playlist.settings.ShunyaPlaylistPreferences;
import org.chromium.chrome.browser.preferences.ShunyaPref;
import org.chromium.chrome.browser.preferences.ShunyaPrefServiceBridge;
import org.chromium.chrome.browser.preferences.PrefChangeRegistrar;
import org.chromium.chrome.browser.preferences.PrefChangeRegistrar.PrefObserver;
import org.chromium.chrome.browser.preferences.SharedPreferencesManager;
import org.chromium.chrome.browser.preferences.website.ShunyaShieldsContentSettings;
import org.chromium.chrome.browser.prefetch.settings.PreloadPagesSettingsBridge;
import org.chromium.chrome.browser.prefetch.settings.PreloadPagesState;
import org.chromium.chrome.browser.privacy.settings.ShunyaPrivacySettings;
import org.chromium.chrome.browser.profiles.Profile;
import org.chromium.chrome.browser.rate.ShunyaRateDialogFragment;
import org.chromium.chrome.browser.rate.RateUtils;
import org.chromium.chrome.browser.rewards.adaptive_captcha.AdaptiveCaptchaHelper;
import org.chromium.chrome.browser.safe_browsing.SafeBrowsingBridge;
import org.chromium.chrome.browser.safe_browsing.SafeBrowsingState;
import org.chromium.chrome.browser.set_default_browser.ShunyaSetDefaultBrowserUtils;
import org.chromium.chrome.browser.set_default_browser.OnShunyaSetDefaultBrowserListener;
import org.chromium.chrome.browser.settings.ShunyaNewsPreferencesV2;
import org.chromium.chrome.browser.settings.ShunyaSearchEngineUtils;
import org.chromium.chrome.browser.settings.ShunyaWalletPreferences;
import org.chromium.chrome.browser.settings.SettingsLauncherImpl;
import org.chromium.chrome.browser.settings.developer.ShunyaQAPreferences;
import org.chromium.chrome.browser.share.ShareDelegate;
import org.chromium.chrome.browser.share.ShareDelegate.ShareOrigin;
import org.chromium.chrome.browser.site_settings.ShunyaWalletEthereumConnectedSites;
import org.chromium.chrome.browser.speedreader.ShunyaSpeedReaderUtils;
import org.chromium.chrome.browser.tab.Tab;
import org.chromium.chrome.browser.tab.TabImpl;
import org.chromium.chrome.browser.tab.TabLaunchType;
import org.chromium.chrome.browser.tab.TabSelectionType;
import org.chromium.chrome.browser.tabmodel.TabModel;
import org.chromium.chrome.browser.tabmodel.TabModelUtils;
import org.chromium.chrome.browser.toolbar.bottom.BottomToolbarConfiguration;
import org.chromium.chrome.browser.toolbar.top.ShunyaToolbarLayoutImpl;
import org.chromium.chrome.browser.util.ShunyaConstants;
import org.chromium.chrome.browser.util.ShunyaDbUtil;
import org.chromium.chrome.browser.util.ConfigurationUtils;
import org.chromium.chrome.browser.util.LiveDataUtil;
import org.chromium.chrome.browser.util.PackageUtils;
import org.chromium.chrome.browser.vpn.ShunyaVpnNativeWorker;
import org.chromium.chrome.browser.vpn.ShunyaVpnObserver;
import org.chromium.chrome.browser.vpn.activities.ShunyaVpnProfileActivity;
import org.chromium.chrome.browser.vpn.billing.InAppPurchaseWrapper;
import org.chromium.chrome.browser.vpn.billing.PurchaseModel;
import org.chromium.chrome.browser.vpn.fragments.ShunyaVpnCalloutDialogFragment;
import org.chromium.chrome.browser.vpn.fragments.LinkVpnSubscriptionDialogFragment;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnApiResponseUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnPrefUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnProfileUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnUtils;
import org.chromium.chrome.browser.vpn.wireguard.WireguardConfigUtils;
import org.chromium.components.browser_ui.settings.SettingsLauncher;
import org.chromium.components.embedder_support.util.UrlConstants;
import org.chromium.components.embedder_support.util.UrlUtilities;
import org.chromium.components.safe_browsing.ShunyaSafeBrowsingApiHandler;
import org.chromium.components.search_engines.TemplateUrl;
import org.chromium.components.user_prefs.UserPrefs;
import org.chromium.content_public.browser.WebContents;
import org.chromium.misc_metrics.mojom.MiscAndroidMetrics;
import org.chromium.mojo.bindings.ConnectionErrorHandler;
import org.chromium.mojo.system.MojoException;
import org.chromium.ui.widget.Toast;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Shunya's extension for ChromeActivity
 */
@JNINamespace("chrome::android")
public abstract class ShunyaActivity extends ChromeActivity
        implements BrowsingDataBridge.OnClearBrowsingDataListener, ShunyaVpnObserver,
                   OnShunyaSetDefaultBrowserListener, ConnectionErrorHandler, PrefObserver,
                   ShunyaSafeBrowsingApiHandler.ShunyaSafeBrowsingApiHandlerDelegate,
                   ShunyaNewsConnectionErrorHandler.ShunyaNewsConnectionErrorHandlerDelegate,
                   MiscAndroidMetricsConnectionErrorHandler
                           .MiscAndroidMetricsConnectionErrorHandlerDelegate {
    public static final String SHUNYA_BUY_URL = "shunya://wallet/fund-wallet";
    public static final String SHUNYA_SEND_URL = "shunya://wallet/send";
    public static final String SHUNYA_SWAP_URL = "shunya://wallet/swap";
    public static final String SHUNYA_DEPOSIT_URL = "shunya://wallet/deposit-funds";
    public static final String SHUNYA_REWARDS_SETTINGS_URL = "shunya://rewards/";
    public static final String SHUNYA_REWARDS_SETTINGS_WALLET_VERIFICATION_URL =
            "shunya://rewards/#verify";
    public static final String SHUNYA_REWARDS_SETTINGS_MONTHLY_URL = "shunya://rewards/#monthly";
    public static final String REWARDS_AC_SETTINGS_URL = "shunya://rewards/contribute";
    public static final String SHUNYA_AI_CHAT_URL = "chrome-untrusted://chat";
    public static final String REWARDS_LEARN_MORE_URL = "https://shunya.com/faq-rewards/#unclaimed-funds";
    public static final String SHUNYA_TERMS_PAGE =
            "https://basicattentiontoken.org/user-terms-of-service/";
    public static final String SHUNYA_PRIVACY_POLICY = "https://shunya.com/privacy/browser/#rewards";
    private static final String PREF_CLOSE_TABS_ON_EXIT = "close_tabs_on_exit";
    private static final String PREF_CLEAR_ON_EXIT = "clear_on_exit";
    public static final String OPEN_URL = "open_url";

    private static final int DAYS_1 = 1;
    private static final int DAYS_4 = 4;
    private static final int DAYS_5 = 5;
    private static final int DAYS_12 = 12;

    public static final int MAX_FAILED_CAPTCHA_ATTEMPTS = 10;

    public static final int APP_OPEN_COUNT_FOR_WIDGET_PROMO = 25;

    /**
     * Settings for sending local notification reminders.
     */
    public static final String CHANNEL_ID = "com.shunya.browser";

    // Explicitly declare this variable to avoid build errors.
    // It will be removed in asm and parent variable will be used instead.
    private UnownedUserDataSupplier<BrowserControlsManager> mBrowserControlsManagerSupplier;

    private static final List<String> sYandexRegions =
            Arrays.asList("AM", "AZ", "BY", "KG", "KZ", "MD", "RU", "TJ", "TM", "UZ");

    private String mPurchaseToken = "";
    private String mProductId = "";
    private boolean mIsVerification;
    private boolean mIsDefaultCheckOnResume;
    private boolean mIsSetDefaultBrowserNotification;
    public boolean mIsDeepLink;
    private ShunyaWalletService mShunyaWalletService;
    private KeyringService mKeyringService;
    private JsonRpcService mJsonRpcService;
    private MiscAndroidMetrics mMiscAndroidMetrics;
    private SwapService mSwapService;
    private WalletModel mWalletModel;
    private BlockchainRegistry mBlockchainRegistry;
    private TxService mTxService;
    private EthTxManagerProxy mEthTxManagerProxy;
    private SolanaTxManagerProxy mSolanaTxManagerProxy;
    private AssetRatioService mAssetRatioService;
    public boolean mLoadedFeed;
    public boolean mComesFromNewTab;
    public CopyOnWriteArrayList<FeedItemsCard> mNewsItemsFeedCards;
    private boolean mIsProcessingPendingDappsTxRequest;
    private int mLastTabId;
    private boolean mNativeInitialized;
    private boolean mSafeBrowsingFlagEnabled;
    private NewTabPageManager mNewTabPageManager;
    private NotificationPermissionController mNotificationPermissionController;
    private ShunyaNewsController mShunyaNewsController;
    private ShunyaNewsConnectionErrorHandler mShunyaNewsConnectionErrorHandler;
    private MiscAndroidMetricsConnectionErrorHandler mMiscAndroidMetricsConnectionErrorHandler;

    /**
     * Serves as a general exception for failed attempts to get ShunyaActivity.
     */
    public static class ShunyaActivityNotFoundException extends Exception {
        public ShunyaActivityNotFoundException(String message) {
            super(message);
        }
    }

    public ShunyaActivity() {}

    @Override
    public void onResumeWithNative() {
        super.onResumeWithNative();
        ShunyaActivityJni.get().restartStatsUpdater();
        if (ShunyaVpnUtils.isVpnFeatureSupported(ShunyaActivity.this)) {
            ShunyaVpnNativeWorker.getInstance().addObserver(this);
            ShunyaVpnUtils.reportBackgroundUsageP3A();
        }
        Profile profile = getCurrentTabModel().getProfile();
        if (profile != null) {
            // Set proper active DSE whenever shunya returns to foreground.
            // If active tab is private, set private DSE as an active DSE.
            ShunyaSearchEngineUtils.updateActiveDSE(profile);
        }

        // The check on mNativeInitialized is mostly to ensure that mojo
        // services for wallet are initialized.
        // TODO(sergz): verify do we need it in that phase or not.
        if (mNativeInitialized) {
            ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
            if (layout == null || !layout.isWalletIconVisible()) {
                return;
            }
            updateWalletBadgeVisibility();
        }

        ShunyaSafeBrowsingApiHandler.getInstance().setDelegate(
                ShunyaActivityJni.get().getSafeBrowsingApiKey(), this);
        // We can store a state of that flag as a browser has to be restarted
        // when the flag state is changed in any case
        mSafeBrowsingFlagEnabled =
                ChromeFeatureList.isEnabled(ShunyaFeatureList.SHUNYA_ANDROID_SAFE_BROWSING);

        executeInitSafeBrowsing(0);
    }

    @Override
    public void onPauseWithNative() {
        if (ShunyaVpnUtils.isVpnFeatureSupported(ShunyaActivity.this)) {
            ShunyaVpnNativeWorker.getInstance().removeObserver(this);
        }
        Profile profile = getCurrentTabModel().getProfile();
        if (profile != null && profile.isOffTheRecord()) {
            // Set normal DSE as an active DSE when shunya goes in background
            // because currently set DSE is used by outside of shunya(ex, shunya search widget).
            ShunyaSearchEngineUtils.updateActiveDSE(profile);
        }
        super.onPauseWithNative();
    }

    @Override
    public boolean onMenuOrKeyboardAction(int id, boolean fromMenu) {
        final TabImpl currentTab = (TabImpl) getActivityTab();
        // Handle items replaced by Shunya.
        if (id == R.id.info_menu_id && currentTab != null) {
            ShareDelegate shareDelegate = (ShareDelegate) getShareDelegateSupplier().get();
            shareDelegate.share(currentTab, false, ShareOrigin.OVERFLOW_MENU);
            return true;
        } else if (id == R.id.reload_menu_id) {
            setComesFromNewTab(true);
        }

        if (super.onMenuOrKeyboardAction(id, fromMenu)) {
            return true;
        }

        // Handle items added by Shunya.
        if (currentTab == null) {
            return false;
        } else if (id == R.id.exit_id) {
            ApplicationLifetime.terminate(false);
        } else if (id == R.id.set_default_browser) {
            ShunyaSetDefaultBrowserUtils.showShunyaSetDefaultBrowserDialog(ShunyaActivity.this, true);
        } else if (id == R.id.shunya_rewards_id) {
            openNewOrSelectExistingTab(SHUNYA_REWARDS_SETTINGS_URL);
        } else if (id == R.id.shunya_wallet_id) {
            openShunyaWallet(false, false, false);
        } else if (id == R.id.shunya_playlist_id) {
            openPlaylist(true);
        } else if (id == R.id.shunya_news_id) {
            openShunyaNewsSettings();
        } else if (id == R.id.request_shunya_vpn_id || id == R.id.request_shunya_vpn_check_id) {
            if (!InternetConnection.isNetworkAvailable(ShunyaActivity.this)) {
                Toast.makeText(ShunyaActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
            } else {
                if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(ShunyaActivity.this)) {
                    ShunyaVpnUtils.showProgressDialog(ShunyaActivity.this,
                            getResources().getString(R.string.vpn_disconnect_text));
                    ShunyaVpnProfileUtils.getInstance().stopVpn(ShunyaActivity.this);
                    ShunyaVpnUtils.dismissProgressDialog();
                } else {
                    if (ShunyaVpnNativeWorker.getInstance().isPurchasedUser()) {
                        ShunyaVpnPrefUtils.setSubscriptionPurchase(true);
                        if (WireguardConfigUtils.isConfigExist(ShunyaActivity.this)) {
                            ShunyaVpnProfileUtils.getInstance().startVpn(ShunyaActivity.this);
                        } else {
                            ShunyaVpnUtils.openShunyaVpnProfileActivity(ShunyaActivity.this);
                        }
                    } else {
                        ShunyaVpnUtils.showProgressDialog(ShunyaActivity.this,
                                getResources().getString(R.string.vpn_connect_text));
                        if (ShunyaVpnPrefUtils.isSubscriptionPurchase()) {
                            verifySubscription();
                        } else {
                            ShunyaVpnUtils.dismissProgressDialog();
                            ShunyaVpnUtils.openShunyaVpnPlansActivity(ShunyaActivity.this);
                        }
                    }
                }
            }
        } else if (id == R.id.shunya_speedreader_id) {
            enableSpeedreaderMode();
        } else if (id == R.id.shunya_leo_id) {
            openShunyaLeo();
        } else {
            return false;
        }
        return true;
    }

    @Override
    public void cleanUpShunyaNewsController() {
        if (mShunyaNewsController != null) {
            mShunyaNewsController.close();
        }
        mShunyaNewsController = null;
    }

    // Handles only wallet related mojo failures. Don't add handlers for mojo connections that
    // are not related to wallet functionality.
    @Override
    public void onConnectionError(MojoException e) {
        cleanUpWalletNativeServices();
        initWalletNativeServices();
    }

    @Override
    protected void onDestroyInternal() {
        if (mNotificationPermissionController != null) {
            NotificationPermissionController.detach(mNotificationPermissionController);
            mNotificationPermissionController = null;
        }
        ShunyaSafeBrowsingApiHandler.getInstance().shutdownSafeBrowsing();
        super.onDestroyInternal();
        cleanUpShunyaNewsController();
        cleanUpWalletNativeServices();
        cleanUpMiscAndroidMetrics();
    }

    public WalletModel getWalletModel() {
        return mWalletModel;
    }

    private void maybeHasPendingUnlockRequest() {
        assert mKeyringService != null;
        mKeyringService.hasPendingUnlockRequest(pending -> {
            if (pending) {
                ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
                if (layout != null) {
                    layout.showWalletPanel();
                }

                return;
            }
            maybeShowPendingTransactions();
            maybeShowSignTxRequestLayout();
        });
    }

    private void setWalletBadgeVisibility(boolean visibile) {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.updateWalletBadgeVisibility(visibile);
        }
    }

    private void maybeShowPendingTransactions() {
        assert mWalletModel != null;
        // trigger to observer to refresh data to process the pending request
        mWalletModel.getCryptoModel().refreshTransactions();
    }

    private void maybeShowSignTxRequestLayout() {
        assert mShunyaWalletService != null;
        mShunyaWalletService.getPendingSignTransactionRequests(requests -> {
            if (requests != null && requests.length != 0) {
                openShunyaWalletDAppsActivity(
                        ShunyaWalletDAppsActivity.ActivityType.SIGN_TRANSACTION);
                return;
            }
            maybeShowSignAllTxRequestLayout();
        });
    }

    private void maybeShowSignAllTxRequestLayout() {
        assert mShunyaWalletService != null;
        mShunyaWalletService.getPendingSignAllTransactionsRequests(requests -> {
            if (requests != null && requests.length != 0) {
                openShunyaWalletDAppsActivity(
                        ShunyaWalletDAppsActivity.ActivityType.SIGN_ALL_TRANSACTIONS);
                return;
            }
            maybeShowSignMessageErrorsLayout();
        });
    }

    private void maybeShowSignMessageErrorsLayout() {
        assert mShunyaWalletService != null;
        mShunyaWalletService.getPendingSignMessageErrors(errors -> {
            if (errors != null && errors.length != 0) {
                openShunyaWalletDAppsActivity(
                        ShunyaWalletDAppsActivity.ActivityType.SIGN_MESSAGE_ERROR);
                return;
            }
        });
        maybeShowSignMessageRequestLayout();
    }

    private void maybeShowSignMessageRequestLayout() {
        assert mShunyaWalletService != null;
        mShunyaWalletService.getPendingSignMessageRequests(requests -> {
            if (requests != null && requests.length != 0) {
                ShunyaWalletDAppsActivity.ActivityType activityType =
                        (requests[0].signData.which() == SignDataUnion.Tag.EthSiweData)
                        ? ShunyaWalletDAppsActivity.ActivityType.SIWE_MESSAGE
                        : ShunyaWalletDAppsActivity.ActivityType.SIGN_MESSAGE;
                openShunyaWalletDAppsActivity(activityType);
                return;
            }
            maybeShowChainRequestLayout();
        });
    }

    private void maybeShowChainRequestLayout() {
        assert mJsonRpcService != null;
        mJsonRpcService.getPendingAddChainRequests(networks -> {
            if (networks != null && networks.length != 0) {
                openShunyaWalletDAppsActivity(
                        ShunyaWalletDAppsActivity.ActivityType.ADD_ETHEREUM_CHAIN);

                return;
            }
            maybeShowSwitchChainRequestLayout();
        });
    }

    private void maybeShowSwitchChainRequestLayout() {
        assert mJsonRpcService != null;
        mJsonRpcService.getPendingSwitchChainRequests(requests -> {
            if (requests != null && requests.length != 0) {
                openShunyaWalletDAppsActivity(
                        ShunyaWalletDAppsActivity.ActivityType.SWITCH_ETHEREUM_CHAIN);

                return;
            }
            maybeShowAddSuggestTokenRequestLayout();
        });
    }

    private void maybeShowAddSuggestTokenRequestLayout() {
        assert mShunyaWalletService != null;
        mShunyaWalletService.getPendingAddSuggestTokenRequests(requests -> {
            if (requests != null && requests.length != 0) {
                openShunyaWalletDAppsActivity(ShunyaWalletDAppsActivity.ActivityType.ADD_TOKEN);

                return;
            }
            maybeShowGetEncryptionPublicKeyRequestLayout();
        });
    }

    private void maybeShowGetEncryptionPublicKeyRequestLayout() {
        assert mShunyaWalletService != null;
        mShunyaWalletService.getPendingGetEncryptionPublicKeyRequests(requests -> {
            if (requests != null && requests.length != 0) {
                openShunyaWalletDAppsActivity(
                        ShunyaWalletDAppsActivity.ActivityType.GET_ENCRYPTION_PUBLIC_KEY_REQUEST);

                return;
            }
            maybeShowDecryptRequestLayout();
        });
    }

    private void maybeShowDecryptRequestLayout() {
        assert mShunyaWalletService != null;
        mShunyaWalletService.getPendingDecryptRequests(requests -> {
            if (requests != null && requests.length != 0) {
                openShunyaWalletDAppsActivity(ShunyaWalletDAppsActivity.ActivityType.DECRYPT_REQUEST);

                return;
            }
            ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
            if (layout != null) {
                layout.showWalletPanel();
            }
        });
    }

    public void dismissWalletPanelOrDialog() {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.dismissWalletPanelOrDialog();
        }
    }

    public void showWalletPanel(boolean ignoreWeb3NotificationPreference) {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.showWalletIcon(true);
        }
        if (!ignoreWeb3NotificationPreference
                && !ShunyaWalletPreferences.getPrefWeb3NotificationsEnabled()) {
            return;
        }
        assert mKeyringService != null;
        mKeyringService.isLocked(locked -> {
            if (locked) {
                layout.showWalletPanel();
                return;
            }
            maybeHasPendingUnlockRequest();
        });
    }

    public void showWalletOnboarding() {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.showWalletIcon(true);
            if (!ShunyaWalletPreferences.getPrefWeb3NotificationsEnabled()) {
                return;
            }
            layout.showWalletPanel();
        }
    }

    public void walletInteractionDetected(WebContents webContents) {
        Tab tab = getActivityTab();
        if (tab == null
                || !webContents.getLastCommittedUrl().equals(
                        tab.getWebContents().getLastCommittedUrl())) {
            return;
        }
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.showWalletIcon(true);
            updateWalletBadgeVisibility();
        }
    }

    public void showAccountCreation(@CoinType.EnumType int coinType) {
        assert mWalletModel != null : " mWalletModel is null ";
        mWalletModel.getDappsModel().addAccountCreationRequest(coinType);
    }

    private void updateWalletBadgeVisibility() {
        assert mWalletModel != null;
        mWalletModel.getDappsModel().updateWalletBadgeVisibility();
    }

    private void verifySubscription() {
        MutableLiveData<PurchaseModel> _activePurchases = new MutableLiveData();
        LiveData<PurchaseModel> activePurchases = _activePurchases;
        InAppPurchaseWrapper.getInstance().queryPurchases(_activePurchases);
        LiveDataUtil.observeOnce(
                activePurchases, activePurchaseModel -> {
                    if (activePurchaseModel != null) {
                        mPurchaseToken = activePurchaseModel.getPurchaseToken();
                        mProductId = activePurchaseModel.getProductId();
                        ShunyaVpnNativeWorker.getInstance().verifyPurchaseToken(mPurchaseToken,
                                mProductId, ShunyaVpnUtils.SUBSCRIPTION_PARAM_TEXT,
                                getPackageName());
                    } else {
                        ShunyaVpnApiResponseUtils.queryPurchaseFailed(ShunyaActivity.this);
                        if (!mIsVerification) {
                            ShunyaVpnUtils.openShunyaVpnPlansActivity(ShunyaActivity.this);
                        }
                    }
                });
    }

    @Override
    public void onVerifyPurchaseToken(String jsonResponse, boolean isSuccess) {
        if (isSuccess) {
            Long purchaseExpiry = ShunyaVpnUtils.getPurchaseExpiryDate(jsonResponse);
            int paymentState = ShunyaVpnUtils.getPaymentState(jsonResponse);
            if (purchaseExpiry > 0 && purchaseExpiry >= System.currentTimeMillis()) {
                ShunyaVpnPrefUtils.setPurchaseToken(mPurchaseToken);
                ShunyaVpnPrefUtils.setProductId(mProductId);
                ShunyaVpnPrefUtils.setPurchaseExpiry(purchaseExpiry);
                ShunyaVpnPrefUtils.setSubscriptionPurchase(true);
                ShunyaVpnPrefUtils.setPaymentState(paymentState);
                if (ShunyaVpnPrefUtils.isResetConfiguration()) {
                    ShunyaVpnUtils.dismissProgressDialog();
                    ShunyaVpnUtils.openShunyaVpnProfileActivity(ShunyaActivity.this);
                } else {
                    if (!mIsVerification) {
                        checkForVpn();
                    } else {
                        mIsVerification = false;
                        if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(
                                    ShunyaActivity.this)
                                && !TextUtils.isEmpty(ShunyaVpnPrefUtils.getHostname())
                                && !TextUtils.isEmpty(ShunyaVpnPrefUtils.getClientId())
                                && !TextUtils.isEmpty(ShunyaVpnPrefUtils.getSubscriberCredential())
                                && !TextUtils.isEmpty(ShunyaVpnPrefUtils.getApiAuthToken())) {
                            ShunyaVpnNativeWorker.getInstance().verifyCredentials(
                                    ShunyaVpnPrefUtils.getHostname(),
                                    ShunyaVpnPrefUtils.getClientId(),
                                    ShunyaVpnPrefUtils.getSubscriberCredential(),
                                    ShunyaVpnPrefUtils.getApiAuthToken());
                        }
                    }
                    ShunyaVpnUtils.dismissProgressDialog();
                }
            } else {
                ShunyaVpnApiResponseUtils.queryPurchaseFailed(ShunyaActivity.this);
                if (!mIsVerification) {
                    ShunyaVpnUtils.openShunyaVpnPlansActivity(ShunyaActivity.this);
                }
                mIsVerification = false;
            }
            mPurchaseToken = "";
            mProductId = "";
        } else {
            ShunyaVpnApiResponseUtils.queryPurchaseFailed(ShunyaActivity.this);
            if (!mIsVerification) {
                ShunyaVpnUtils.openShunyaVpnPlansActivity(ShunyaActivity.this);
            }
            mIsVerification = false;
        }
    };

    private void checkForVpn() {
        ShunyaVpnNativeWorker.getInstance().reportForegroundP3A();
        new Thread() {
            @Override
            public void run() {
                Intent intent = GoBackend.VpnService.prepare(ShunyaActivity.this);
                if (intent != null
                        || !WireguardConfigUtils.isConfigExist(getApplicationContext())) {
                    ShunyaVpnUtils.dismissProgressDialog();
                    ShunyaVpnUtils.openShunyaVpnProfileActivity(ShunyaActivity.this);
                    return;
                }
                ShunyaVpnProfileUtils.getInstance().startVpn(ShunyaActivity.this);
            }
        }.start();
    }

    @Override
    public void onVerifyCredentials(String jsonVerifyCredentials, boolean isSuccess) {
        if (!isSuccess) {
            if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(ShunyaActivity.this)) {
                ShunyaVpnProfileUtils.getInstance().stopVpn(ShunyaActivity.this);
            }
            Intent shunyaVpnProfileIntent =
                    new Intent(ShunyaActivity.this, ShunyaVpnProfileActivity.class);
            shunyaVpnProfileIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            shunyaVpnProfileIntent.putExtra(ShunyaVpnUtils.VERIFY_CREDENTIALS_FAILED, true);
            shunyaVpnProfileIntent.setAction(Intent.ACTION_VIEW);
            startActivity(shunyaVpnProfileIntent);
        }
    }

    @Override
    public void initializeState() {
        super.initializeState();
        if (isNoRestoreState()) {
            CommandLine.getInstance().appendSwitch(ChromeSwitches.NO_RESTORE_STATE);
        }

        if (isClearBrowsingDataOnExit()) {
            List<Integer> dataTypes = Arrays.asList(
                    BrowsingDataType.HISTORY, BrowsingDataType.COOKIES, BrowsingDataType.CACHE);

            int[] dataTypesArray = CollectionUtil.integerCollectionToIntArray(dataTypes);

            // has onBrowsingDataCleared() as an @Override callback from implementing
            // BrowsingDataBridge.OnClearBrowsingDataListener
            BrowsingDataBridge.getInstance().clearBrowsingData(
                    this, dataTypesArray, TimePeriod.ALL_TIME);
        }

        setLoadedFeed(false);
        setComesFromNewTab(false);
        setNewsItemsFeedCards(null);
        ShunyaSearchEngineUtils.initializeShunyaSearchEngineStates(getTabModelSelector());
        Intent intent = getIntent();
        if (intent != null && intent.getBooleanExtra(Utils.RESTART_WALLET_ACTIVITY, false)) {
            openShunyaWallet(false,
                    intent.getBooleanExtra(Utils.RESTART_WALLET_ACTIVITY_SETUP, false),
                    intent.getBooleanExtra(Utils.RESTART_WALLET_ACTIVITY_RESTORE, false));
        }
    }

    public int getLastTabId() {
        return mLastTabId;
    }

    public void setLastTabId(int lastTabId) {
        this.mLastTabId = lastTabId;
    }

    public boolean isLoadedFeed() {
        return mLoadedFeed;
    }

    public void setLoadedFeed(boolean loadedFeed) {
        this.mLoadedFeed = loadedFeed;
    }

    public CopyOnWriteArrayList<FeedItemsCard> getNewsItemsFeedCards() {
        return mNewsItemsFeedCards;
    }

    public void setNewsItemsFeedCards(CopyOnWriteArrayList<FeedItemsCard> newsItemsFeedCards) {
        this.mNewsItemsFeedCards = newsItemsFeedCards;
    }

    public void setComesFromNewTab(boolean comesFromNewTab) {
        this.mComesFromNewTab = comesFromNewTab;
    }

    public boolean isComesFromNewTab() {
        return mComesFromNewTab;
    }

    @Override
    public void onBrowsingDataCleared() {}

    @Override
    public void OnCheckDefaultResume() {
        mIsDefaultCheckOnResume = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsProcessingPendingDappsTxRequest = false;
        if (mIsDefaultCheckOnResume) {
            mIsDefaultCheckOnResume = false;

            if (ShunyaSetDefaultBrowserUtils.isShunyaSetAsDefaultBrowser(this)) {
                ShunyaSetDefaultBrowserUtils.setShunyaDefaultSuccess();
            }
        }

        PostTask.postTask(
                TaskTraits.BEST_EFFORT_MAY_BLOCK, () -> { ShunyaStatsUtil.removeShareStatsFile(); });

        // We need to enable widget promo for later release
        /* int appOpenCountForWidgetPromo = SharedPreferencesManager.getInstance().readInt(
                ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT_FOR_WIDGET_PROMO);
        if (appOpenCountForWidgetPromo < APP_OPEN_COUNT_FOR_WIDGET_PROMO) {
            SharedPreferencesManager.getInstance().writeInt(
                    ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT_FOR_WIDGET_PROMO,
                    appOpenCountForWidgetPromo + 1);
        } */
    }

    @Override
    public void performPostInflationStartup() {
        super.performPostInflationStartup();

        createNotificationChannel();
    }

    @Override
    protected void initializeStartupMetrics() {
        super.initializeStartupMetrics();

        // Disable FRE for arm64 builds where ChromeActivity is the one that
        // triggers FRE instead of ChromeLauncherActivity on arm32 build.
        ShunyaHelper.DisableFREDRP();
    }

    @Override
    public void onPreferenceChange() {
        String captchaID = UserPrefs.get(Profile.getLastUsedRegularProfile())
                                   .getString(ShunyaPref.SCHEDULED_CAPTCHA_ID);
        String paymentID = UserPrefs.get(Profile.getLastUsedRegularProfile())
                                   .getString(ShunyaPref.SCHEDULED_CAPTCHA_PAYMENT_ID);
        if (ShunyaQAPreferences.shouldVlogRewards()) {
            Log.e(AdaptiveCaptchaHelper.TAG,
                    "captchaID : " + captchaID + " Payment ID : " + paymentID);
        }
        maybeSolveAdaptiveCaptcha();
    }

    @Override
    public void turnSafeBrowsingOff() {
        SafeBrowsingBridge.setSafeBrowsingState(SafeBrowsingState.NO_SAFE_BROWSING);
    }

    @Override
    public boolean isSafeBrowsingEnabled() {
        return mSafeBrowsingFlagEnabled;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public void maybeSolveAdaptiveCaptcha() {
        String captchaID = UserPrefs.get(Profile.getLastUsedRegularProfile())
                                   .getString(ShunyaPref.SCHEDULED_CAPTCHA_ID);
        String paymentID = UserPrefs.get(Profile.getLastUsedRegularProfile())
                                   .getString(ShunyaPref.SCHEDULED_CAPTCHA_PAYMENT_ID);
        if (!TextUtils.isEmpty(captchaID) && !TextUtils.isEmpty(paymentID)
                && !ShunyaPrefServiceBridge.getInstance().getSafetynetCheckFailed()) {
            AdaptiveCaptchaHelper.startAttestation(captchaID, paymentID);
        }
    }

    @Override
    public void finishNativeInitialization() {
        super.finishNativeInitialization();
        ShunyaVpnNativeWorker.getInstance().reloadPurchasedState();

        ShunyaHelper.maybeMigrateSettings();

        PrefChangeRegistrar mPrefChangeRegistrar = new PrefChangeRegistrar();
        mPrefChangeRegistrar.addObserver(ShunyaPref.SCHEDULED_CAPTCHA_ID, this);

        if (UserPrefs.get(Profile.getLastUsedRegularProfile())
                        .getInteger(ShunyaPref.SCHEDULED_CAPTCHA_FAILED_ATTEMPTS)
                >= MAX_FAILED_CAPTCHA_ATTEMPTS) {
            UserPrefs.get(Profile.getLastUsedRegularProfile())
                    .setBoolean(ShunyaPref.SCHEDULED_CAPTCHA_PAUSED, true);
        }

        if (ShunyaQAPreferences.shouldVlogRewards()) {
            Log.e(AdaptiveCaptchaHelper.TAG,
                    "Failed attempts : "
                            + UserPrefs.get(Profile.getLastUsedRegularProfile())
                                      .getInteger(ShunyaPref.SCHEDULED_CAPTCHA_FAILED_ATTEMPTS));
        }
        if (!UserPrefs.get(Profile.getLastUsedRegularProfile())
                        .getBoolean(ShunyaPref.SCHEDULED_CAPTCHA_PAUSED)) {
            maybeSolveAdaptiveCaptcha();
        }

        if (SharedPreferencesManager.getInstance().readBoolean(
                    ShunyaPreferenceKeys.SHUNYA_DOUBLE_RESTART, false)) {
            SharedPreferencesManager.getInstance().writeBoolean(
                    ShunyaPreferenceKeys.SHUNYA_DOUBLE_RESTART, false);
            ShunyaRelaunchUtils.restart();
            return;
        }

        // Make sure this option is disabled
        if (PreloadPagesSettingsBridge.getState() != PreloadPagesState.NO_PRELOADING) {
            PreloadPagesSettingsBridge.setState(PreloadPagesState.NO_PRELOADING);
        }

        if (ShunyaRewardsHelper.hasRewardsEnvChange()) {
            ShunyaPrefServiceBridge.getInstance().resetPromotionLastFetchStamp();
            ShunyaRewardsHelper.setRewardsEnvChange(false);
        }

        int appOpenCount = SharedPreferencesManager.getInstance().readInt(ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT);
        SharedPreferencesManager.getInstance().writeInt(ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT, appOpenCount + 1);

        if (PackageUtils.isFirstInstall(this) && appOpenCount == 0) {
            checkForYandexSE();
        }

        migrateBgPlaybackToFeature();

        Context app = ContextUtils.getApplicationContext();
        if (null != app
                && ShunyaReflectionUtil.EqualTypes(this.getClass(), ChromeTabbedActivity.class)) {
            // Trigger ShunyaSyncWorker CTOR to make migration from sync v1 if sync is enabled
            ShunyaSyncWorker.get();
        }

        initMiscAndroidMetrics();
        checkForNotificationData();

        if (RateUtils.getInstance().isLastSessionShown()) {
            RateUtils.getInstance().setPrefNextRateDate();
            RateUtils.getInstance().setLastSessionShown(false);
        }

        if (!RateUtils.getInstance().getPrefRateEnabled()) {
            RateUtils.getInstance().setPrefRateEnabled(true);
            RateUtils.getInstance().setPrefNextRateDate();
        }
        RateUtils.getInstance().setTodayDate();

        if (RateUtils.getInstance().shouldShowRateDialog(this)) {
            showShunyaRateDialog();
            RateUtils.getInstance().setLastSessionShown(true);
        }

        // TODO commenting out below code as we may use it in next release

        // if (PackageUtils.isFirstInstall(this)
        //         &&
        //         SharedPreferencesManager.getInstance().readInt(ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT)
        //         == 1) {
        //     Calendar calender = Calendar.getInstance();
        //     calender.setTime(new Date());
        //     calender.add(Calendar.DATE, DAYS_4);
        //     OnboardingPrefManager.getInstance().setNextOnboardingDate(
        //         calender.getTimeInMillis());
        // }

        // OnboardingActivity onboardingActivity = null;
        // for (Activity ref : ApplicationStatus.getRunningActivities()) {
        //     if (!(ref instanceof OnboardingActivity)) continue;

        //     onboardingActivity = (OnboardingActivity) ref;
        // }

        // if (onboardingActivity == null
        //         && OnboardingPrefManager.getInstance().showOnboardingForSkip(this)) {
        //     OnboardingPrefManager.getInstance().showOnboarding(this);
        //     OnboardingPrefManager.getInstance().setOnboardingShownForSkip(true);
        // }

        if (SharedPreferencesManager.getInstance().readInt(ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT) == 1) {
            Calendar calender = Calendar.getInstance();
            calender.setTime(new Date());
            calender.add(Calendar.DATE, DAYS_12);
            OnboardingPrefManager.getInstance().setNextCrossPromoModalDate(
                    calender.getTimeInMillis());
        }

        if (OnboardingPrefManager.getInstance().showCrossPromoModal()) {
            showCrossPromotionalDialog();
            OnboardingPrefManager.getInstance().setCrossPromoModalShown(true);
        }
        ShunyaSyncInformers.show();
        ShunyaAndroidSyncDisabledInformer.showInformers();
        ShunyaSyncAccountDeletedInformer.show();

        if (!OnboardingPrefManager.getInstance().isOneTimeNotificationStarted()
                && PackageUtils.isFirstInstall(this)) {
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.HOUR_3);
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.HOUR_24);
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.DAY_6);
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.DAY_10);
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.DAY_30);
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.DAY_35);
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.DEFAULT_BROWSER_1);
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.DEFAULT_BROWSER_2);
            RetentionNotificationUtil.scheduleNotification(this, RetentionNotificationUtil.DEFAULT_BROWSER_3);
            OnboardingPrefManager.getInstance().setOneTimeNotificationStarted(true);
        }

        if (PackageUtils.isFirstInstall(this)
                && SharedPreferencesManager.getInstance().readInt(
                           ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT)
                        == 1) {
            Calendar calender = Calendar.getInstance();
            calender.setTime(new Date());
            calender.add(Calendar.DATE, DAYS_4);
            ShunyaRewardsHelper.setNextRewardsOnboardingModalDate(calender.getTimeInMillis());
        }

        if (!mIsSetDefaultBrowserNotification) {
            ShunyaSetDefaultBrowserUtils.checkSetDefaultBrowserModal(this);
        }

        checkFingerPrintingOnUpgrade();
        checkForVpnCallout();

        if (ChromeFeatureList.isEnabled(ShunyaFeatureList.SHUNYA_VPN_LINK_SUBSCRIPTION_ANDROID_UI)
                && ShunyaVpnPrefUtils.isSubscriptionPurchase()
                && !ShunyaVpnPrefUtils.isLinkSubscriptionDialogShown()) {
            showLinkVpnSubscriptionDialog();
        }
        if (PackageUtils.isFirstInstall(this)
                && (OnboardingPrefManager.getInstance().isDormantUsersEngagementEnabled()
                        || getPackageName().equals(ShunyaConstants.SHUNYA_PRODUCTION_PACKAGE_NAME))) {
            OnboardingPrefManager.getInstance().setDormantUsersPrefs();
            if (!OnboardingPrefManager.getInstance().isDormantUsersNotificationsStarted()) {
                RetentionNotificationUtil.scheduleDormantUsersNotifications(this);
                OnboardingPrefManager.getInstance().setDormantUsersNotificationsStarted(true);
            }
        }
        initWalletNativeServices();

        mNativeInitialized = true;

        String countryCode = Locale.getDefault().getCountry();
        if (countryCode.equals(ShunyaConstants.INDIA_COUNTRY_CODE)
                && SharedPreferencesManager.getInstance().readBoolean(
                        ShunyaPreferenceKeys.SHUNYA_AD_FREE_CALLOUT_DIALOG, true)
                && getActivityTab() != null && getActivityTab().getUrl().getSpec() != null
                && UrlUtilities.isNTPUrl(getActivityTab().getUrl().getSpec())
                && (SharedPreferencesManager.getInstance().readBoolean(
                            ShunyaPreferenceKeys.SHUNYA_OPENED_YOUTUBE, false)
                        || SharedPreferencesManager.getInstance().readInt(
                                   ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT)
                                >= 7)) {
            showAdFreeCalloutDialog();
        }

        initShunyaNewsController();
        if (SharedPreferencesManager.getInstance().readBoolean(
                    ShunyaPreferenceKeys.SHUNYA_DEFERRED_DEEPLINK_PLAYLIST, false)) {
            SharedPreferencesManager.getInstance().writeBoolean(
                    ShunyaPreferenceKeys.SHUNYA_DEFERRED_DEEPLINK_PLAYLIST, false);
            openPlaylist(false);
        } else if (SharedPreferencesManager.getInstance().readBoolean(
                           ShunyaPreferenceKeys.SHUNYA_DEFERRED_DEEPLINK_VPN, false)) {
            SharedPreferencesManager.getInstance().writeBoolean(
                    ShunyaPreferenceKeys.SHUNYA_DEFERRED_DEEPLINK_VPN, false);
            handleDeepLinkVpn();
        } else if (!mIsDeepLink
                && OnboardingPrefManager.getInstance().isOnboardingSearchBoxTooltip()
                && getActivityTab() != null && getActivityTab().getUrl().getSpec() != null
                && UrlUtilities.isNTPUrl(getActivityTab().getUrl().getSpec())) {
            showSearchBoxTooltip();
        }

        // Added to reset app links settings for upgrade case
        if (!PackageUtils.isFirstInstall(this)
                && !SharedPreferencesManager.getInstance().readBoolean(
                        ShunyaPrivacySettings.PREF_APP_LINKS, true)
                && SharedPreferencesManager.getInstance().readBoolean(
                        ShunyaPrivacySettings.PREF_APP_LINKS_RESET, true)) {
            SharedPreferencesManager.getInstance().writeBoolean(
                    ShunyaPrivacySettings.PREF_APP_LINKS, true);
            SharedPreferencesManager.getInstance().writeBoolean(
                    ShunyaPrivacySettings.PREF_APP_LINKS_RESET, false);
        }
    }

    private void handleDeepLinkVpn() {
        mIsDeepLink = true;
        ShunyaVpnUtils.openShunyaVpnPlansActivity(this);
    }

    private void checkForVpnCallout() {
        String countryCode = Locale.getDefault().getCountry();

        if (!countryCode.equals(ShunyaConstants.INDIA_COUNTRY_CODE)
                && ShunyaVpnUtils.isVpnFeatureSupported(ShunyaActivity.this)) {
            if (ShunyaVpnPrefUtils.shouldShowCallout() && !ShunyaVpnPrefUtils.isSubscriptionPurchase()
                            && (SharedPreferencesManager.getInstance().readInt(
                                        ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT)
                                            == 1
                                    && !PackageUtils.isFirstInstall(this))
                    || (SharedPreferencesManager.getInstance().readInt(
                                ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT)
                                    == 7
                            && PackageUtils.isFirstInstall(this))) {
                showVpnCalloutDialog();
            }

            if (!TextUtils.isEmpty(ShunyaVpnPrefUtils.getPurchaseToken())
                    && !TextUtils.isEmpty(ShunyaVpnPrefUtils.getProductId())) {
                mIsVerification = true;
                ShunyaVpnNativeWorker.getInstance().verifyPurchaseToken(
                        ShunyaVpnPrefUtils.getPurchaseToken(), ShunyaVpnPrefUtils.getProductId(),
                        ShunyaVpnUtils.SUBSCRIPTION_PARAM_TEXT, getPackageName());
            }
        }
    }

    @Override
    public void initShunyaNewsController() {
        if (mShunyaNewsController != null) {
            return;
        }
        if (mShunyaNewsConnectionErrorHandler == null) {
            mShunyaNewsConnectionErrorHandler = ShunyaNewsConnectionErrorHandler.getInstance();
            mShunyaNewsConnectionErrorHandler.setDelegate(this);
        }

        if (ShunyaPrefServiceBridge.getInstance().getShowNews()
                && ShunyaPrefServiceBridge.getInstance().getNewsOptIn()) {
            mShunyaNewsController = ShunyaNewsControllerFactory.getInstance().getShunyaNewsController(
                    mShunyaNewsConnectionErrorHandler);

            ShunyaNewsUtils.getShunyaNewsSettingsData(mShunyaNewsController, null);
        }
    }

    private void migrateBgPlaybackToFeature() {
        if (SharedPreferencesManager.getInstance().readBoolean(
                    ShunyaPreferenceKeys.SHUNYA_BACKGROUND_VIDEO_PLAYBACK_CONVERTED_TO_FEATURE,
                    false)) {
            if (ShunyaPrefServiceBridge.getInstance().getBackgroundVideoPlaybackEnabled()
                    && ChromeFeatureList.isEnabled(
                            ShunyaFeatureList.SHUNYA_BACKGROUND_VIDEO_PLAYBACK)) {
                ShunyaPrefServiceBridge.getInstance().setBackgroundVideoPlaybackEnabled(false);
            }
            return;
        }
        if (ShunyaPrefServiceBridge.getInstance().getBackgroundVideoPlaybackEnabled()) {
            ShunyaFeatureUtil.enableFeature(
                    ShunyaFeatureList.SHUNYA_BACKGROUND_VIDEO_PLAYBACK_INTERNAL, true, true);
        }
        SharedPreferencesManager.getInstance().writeBoolean(
                ShunyaPreferenceKeys.SHUNYA_BACKGROUND_VIDEO_PLAYBACK_CONVERTED_TO_FEATURE, true);
    }

    private void showSearchBoxTooltip() {
        OnboardingPrefManager.getInstance().setOnboardingSearchBoxTooltip(false);
        HighlightView highlightView = new HighlightView(this, null);
        highlightView.setColor(
                ContextCompat.getColor(this, R.color.onboarding_search_highlight_color));
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View anchorView = (View) findViewById(R.id.toolbar);
        float padding = (float) dpToPx(this, 20);
        boolean isTablet = ConfigurationUtils.isTablet(this);
        new Handler().postDelayed(() -> {
            PopupWindowTooltip popupWindowTooltip =
                    new PopupWindowTooltip.Builder(this)
                            .anchorView(anchorView)
                            .arrowColor(getResources().getColor(R.color.onboarding_arrow_color))
                            .gravity(Gravity.BOTTOM)
                            .dismissOnOutsideTouch(true)
                            .dismissOnInsideTouch(false)
                            .backgroundDimDisabled(true)
                            .contentArrowAtStart(!isTablet)
                            .padding(padding)
                            .parentPaddingHorizontal(dpToPx(this, 10))
                            .onDismissListener(tooltip -> {
                                if (viewGroup != null && highlightView != null) {
                                    viewGroup.removeView(highlightView);
                                }
                            })
                            .modal(true)
                            .contentView(R.layout.shunya_onboarding_searchbox)
                            .build();

            String countryCode = Locale.getDefault().getCountry();
            if (countryCode.equals(ShunyaConstants.INDIA_COUNTRY_CODE)) {
                TextView toolTipBody = popupWindowTooltip.findViewById(R.id.tv_tooltip_title);
                toolTipBody.setText(getResources().getString(R.string.searchbox_onboarding_india));
            }
            viewGroup.addView(highlightView);
            HighlightItem item = new HighlightItem(anchorView);
            highlightView.setHighlightTransparent(true);
            highlightView.setHighlightItem(item);
            popupWindowTooltip.show();
        }, 500);
    }

    public void setDormantUsersPrefs() {
        OnboardingPrefManager.getInstance().setDormantUsersPrefs();
        RetentionNotificationUtil.scheduleDormantUsersNotifications(this);
    }

    private void openPlaylist(boolean shouldHandlePlaylistActivity) {
        if (!shouldHandlePlaylistActivity) mIsDeepLink = true;

        if (SharedPreferencesManager.getInstance().readBoolean(
                    PlaylistPreferenceUtils.SHOULD_SHOW_PLAYLIST_ONBOARDING, true)) {
            PlaylistUtils.openPlaylistMenuOnboardingActivity(ShunyaActivity.this);
            SharedPreferencesManager.getInstance().writeBoolean(
                    PlaylistPreferenceUtils.SHOULD_SHOW_PLAYLIST_ONBOARDING, false);
        } else if (shouldHandlePlaylistActivity) {
            openPlaylistActivity(ShunyaActivity.this, ConstantUtils.ALL_PLAYLIST);
        }
    }

    public void openPlaylistActivity(Context context, String playlistId) {
        Intent playlistActivityIntent = new Intent(context, PlaylistHostActivity.class);
        playlistActivityIntent.putExtra(ConstantUtils.PLAYLIST_ID, playlistId);
        playlistActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        playlistActivityIntent.setAction(Intent.ACTION_VIEW);
        context.startActivity(playlistActivityIntent);
    }

    public void showPlaylistWarningDialog(
            PlaylistWarningDialogListener playlistWarningDialogListener) {
        PlaylistWarningDialogFragment playlistWarningDialogFragment =
                new PlaylistWarningDialogFragment();
        playlistWarningDialogFragment.setCancelable(false);
        playlistWarningDialogFragment.setPlaylistWarningDialogListener(
                playlistWarningDialogListener);
        playlistWarningDialogFragment.show(
                getSupportFragmentManager(), "PlaylistWarningDialogFragment");
    }

    private void showVpnCalloutDialog() {
        try {
            ShunyaVpnCalloutDialogFragment shunyaVpnCalloutDialogFragment =
                    new ShunyaVpnCalloutDialogFragment();
            shunyaVpnCalloutDialogFragment.show(
                    getSupportFragmentManager(), "ShunyaVpnCalloutDialogFragment");
        } catch (IllegalStateException e) {
            Log.e("showVpnCalloutDialog", e.getMessage());
        }
    }

    private void showLinkVpnSubscriptionDialog() {
        LinkVpnSubscriptionDialogFragment linkVpnSubscriptionDialogFragment =
                new LinkVpnSubscriptionDialogFragment();
        linkVpnSubscriptionDialogFragment.setCancelable(false);
        linkVpnSubscriptionDialogFragment.show(
                getSupportFragmentManager(), "LinkVpnSubscriptionDialogFragment");
    }

    private void showAdFreeCalloutDialog() {
        SharedPreferencesManager.getInstance().writeBoolean(
                ShunyaPreferenceKeys.SHUNYA_AD_FREE_CALLOUT_DIALOG, false);

        ShunyaAdFreeCalloutDialogFragment shunyaAdFreeCalloutDialogFragment =
                new ShunyaAdFreeCalloutDialogFragment();
        shunyaAdFreeCalloutDialogFragment.show(
                getSupportFragmentManager(), "ShunyaAdFreeCalloutDialogFragment");
    }

    public void setNewTabPageManager(NewTabPageManager manager) {
        mNewTabPageManager = manager;
    }

    public void focusSearchBox() {
        if (mNewTabPageManager != null) {
            mNewTabPageManager.focusSearchBox(false, null);
        }
    }

    private void checkFingerPrintingOnUpgrade() {
        if (!PackageUtils.isFirstInstall(this)
                && SharedPreferencesManager.getInstance().readInt(
                           ShunyaPreferenceKeys.SHUNYA_APP_OPEN_COUNT)
                        == 0) {
            boolean value = SharedPreferencesManager.getInstance().readBoolean(
                    ShunyaPrivacySettings.PREF_FINGERPRINTING_PROTECTION, true);
            if (value) {
                ShunyaShieldsContentSettings.setShieldsValue(Profile.getLastUsedRegularProfile(), "",
                        ShunyaShieldsContentSettings.RESOURCE_IDENTIFIER_FINGERPRINTING,
                        ShunyaShieldsContentSettings.DEFAULT, false);
            } else {
                ShunyaShieldsContentSettings.setShieldsValue(Profile.getLastUsedRegularProfile(), "",
                        ShunyaShieldsContentSettings.RESOURCE_IDENTIFIER_FINGERPRINTING,
                        ShunyaShieldsContentSettings.ALLOW_RESOURCE, false);
            }
        }
    }

    public void openShunyaPlaylistSettings() {
        SettingsLauncher settingsLauncher = new SettingsLauncherImpl();
        settingsLauncher.launchSettingsActivity(this, ShunyaPlaylistPreferences.class);
    }

    public void openShunyaNewsSettings() {
        SettingsLauncher settingsLauncher = new SettingsLauncherImpl();
        settingsLauncher.launchSettingsActivity(this, ShunyaNewsPreferencesV2.class);
    }

    // TODO: Once we have a ready for https://github.com/shunya/shunya-browser/issues/33015, We'll use
    // this code
    /*public void openShunyaContentFilteringSettings() {
        SettingsLauncher settingsLauncher = new SettingsLauncherImpl();
        settingsLauncher.launchSettingsActivity(this, ContentFilteringFragment.class);
    }*/

    public void openShunyaWalletSettings() {
        SettingsLauncher settingsLauncher = new SettingsLauncherImpl();
        settingsLauncher.launchSettingsActivity(this, ShunyaWalletPreferences.class);
    }

    public void openShunyaConnectedSitesSettings() {
        SettingsLauncher settingsLauncher = new SettingsLauncherImpl();
        settingsLauncher.launchSettingsActivity(this, ShunyaWalletEthereumConnectedSites.class);
    }

    public void openShunyaWallet(boolean fromDapp, boolean setupAction, boolean restoreAction) {
        Intent shunyaWalletIntent = new Intent(this, ShunyaWalletActivity.class);
        shunyaWalletIntent.putExtra(Utils.IS_FROM_DAPPS, fromDapp);
        shunyaWalletIntent.putExtra(Utils.RESTART_WALLET_ACTIVITY_SETUP, setupAction);
        shunyaWalletIntent.putExtra(Utils.RESTART_WALLET_ACTIVITY_RESTORE, restoreAction);
        shunyaWalletIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        shunyaWalletIntent.setAction(Intent.ACTION_VIEW);
        startActivity(shunyaWalletIntent);
    }

    public void viewOnBlockExplorer(
            String address, @CoinType.EnumType int coinType, NetworkInfo networkInfo) {
        Utils.openAddress("/address/" + address, this, coinType, networkInfo);
    }

    public void openShunyaWalletDAppsActivity(ShunyaWalletDAppsActivity.ActivityType activityType) {
        Intent shunyaWalletIntent = new Intent(this, ShunyaWalletDAppsActivity.class);
        shunyaWalletIntent.putExtra("activityType", activityType.getValue());
        shunyaWalletIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        shunyaWalletIntent.setAction(Intent.ACTION_VIEW);
        startActivity(shunyaWalletIntent);
    }

    public MiscAndroidMetrics getMiscAndroidMetrics() {
        return mMiscAndroidMetrics;
    }

    private void checkForYandexSE() {
        String countryCode = Locale.getDefault().getCountry();
        if (sYandexRegions.contains(countryCode)) {
            Profile lastUsedRegularProfile = Profile.getLastUsedRegularProfile();
            TemplateUrl yandexTemplateUrl = ShunyaSearchEngineUtils.getTemplateUrlByShortName(
                    lastUsedRegularProfile, OnboardingPrefManager.YANDEX);
            if (yandexTemplateUrl != null) {
                ShunyaSearchEngineUtils.setDSEPrefs(yandexTemplateUrl, lastUsedRegularProfile);
                ShunyaSearchEngineUtils.setDSEPrefs(yandexTemplateUrl,
                        lastUsedRegularProfile.getPrimaryOTRProfile(/* createIfNeeded= */ true));
            }
        }
    }

    private ShunyaNotificationWarningDialog.DismissListener mCloseDialogListener =
            new ShunyaNotificationWarningDialog.DismissListener() {
                @Override
                public void onDismiss() {
                    checkForNotificationData();
                }
            };

    private void checkForNotificationData() {
        Intent notifIntent = getIntent();
        if (notifIntent != null && notifIntent.getStringExtra(RetentionNotificationUtil.NOTIFICATION_TYPE) != null) {
            String notificationType = notifIntent.getStringExtra(RetentionNotificationUtil.NOTIFICATION_TYPE);
            switch (notificationType) {
                case RetentionNotificationUtil.HOUR_3:
                case RetentionNotificationUtil.HOUR_24:
                case RetentionNotificationUtil.EVERY_SUNDAY:
                    checkForShunyaStats();
                    break;
                case RetentionNotificationUtil.DAY_6:
                    if (getActivityTab() != null && getActivityTab().getUrl().getSpec() != null
                            && !UrlUtilities.isNTPUrl(getActivityTab().getUrl().getSpec())) {
                        getTabCreator(false).launchUrl(
                                UrlConstants.NTP_URL, TabLaunchType.FROM_CHROME_UI);
                    }
                    break;
                case RetentionNotificationUtil.DAY_10:
                case RetentionNotificationUtil.DAY_30:
                case RetentionNotificationUtil.DAY_35:
                    openRewardsPanel();
                    break;
                case RetentionNotificationUtil.DORMANT_USERS_DAY_14:
                case RetentionNotificationUtil.DORMANT_USERS_DAY_25:
                case RetentionNotificationUtil.DORMANT_USERS_DAY_40:
                    showDormantUsersEngagementDialog(notificationType);
                    break;
                case RetentionNotificationUtil.DEFAULT_BROWSER_1:
                case RetentionNotificationUtil.DEFAULT_BROWSER_2:
                case RetentionNotificationUtil.DEFAULT_BROWSER_3:
                    if (!ShunyaSetDefaultBrowserUtils.isShunyaSetAsDefaultBrowser(ShunyaActivity.this)
                            && !ShunyaSetDefaultBrowserUtils.isShunyaDefaultDontAsk()) {
                        mIsSetDefaultBrowserNotification = true;
                        ShunyaSetDefaultBrowserUtils.showShunyaSetDefaultBrowserDialog(
                                ShunyaActivity.this, false);
                    }
                    break;
            }
        }
    }

    public void checkForShunyaStats() {
        if (OnboardingPrefManager.getInstance().isShunyaStatsEnabled()) {
            ShunyaStatsUtil.showShunyaStats();
        } else {
            if (getActivityTab() != null && getActivityTab().getUrl().getSpec() != null
                    && !UrlUtilities.isNTPUrl(getActivityTab().getUrl().getSpec())) {
                OnboardingPrefManager.getInstance().setFromNotification(true);
                if (getTabCreator(false) != null) {
                    getTabCreator(false).launchUrl(
                            UrlConstants.NTP_URL, TabLaunchType.FROM_CHROME_UI);
                }
            } else {
                showOnboardingV2(false);
            }
        }
    }

    public void showOnboardingV2(boolean fromStats) {
        try {
            OnboardingPrefManager.getInstance().setNewOnboardingShown(true);
            FragmentManager fm = getSupportFragmentManager();
            HighlightDialogFragment fragment = (HighlightDialogFragment) fm.findFragmentByTag(
                    HighlightDialogFragment.TAG_FRAGMENT);
            FragmentTransaction transaction = fm.beginTransaction();

            if (fragment != null) {
                transaction.remove(fragment);
            }

            fragment = new HighlightDialogFragment();
            Bundle fragmentBundle = new Bundle();
            fragmentBundle.putBoolean(OnboardingPrefManager.FROM_STATS, fromStats);
            fragment.setArguments(fragmentBundle);
            transaction.add(fragment, HighlightDialogFragment.TAG_FRAGMENT);
            transaction.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            Log.e("HighlightDialogFragment", e.getMessage());
        }
    }

    public void hideRewardsOnboardingIcon() {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.hideRewardsOnboardingIcon();
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID, getString(R.string.shunya_browser), importance);
            channel.setDescription(
                    getString(R.string.shunya_browser_notification_channel_description));
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private boolean isNoRestoreState() {
        return SharedPreferencesManager.getInstance().readBoolean(PREF_CLOSE_TABS_ON_EXIT, false);
    }

    private boolean isClearBrowsingDataOnExit() {
        return SharedPreferencesManager.getInstance().readBoolean(PREF_CLEAR_ON_EXIT, false);
    }

    public void onRewardsPanelDismiss() {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.onRewardsPanelDismiss();
        }
    }

    public void dismissRewardsPanel() {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.dismissRewardsPanel();
        }
    }

    public void dismissShieldsTooltip() {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.dismissShieldsTooltip();
        }
    }

    public void openRewardsPanel() {
        ShunyaToolbarLayoutImpl layout = getShunyaToolbarLayout();
        if (layout != null) {
            layout.openRewardsPanel();
        }
    }

    public Profile getCurrentProfile() {
        Tab tab = getActivityTab();
        if (tab == null) {
            return Profile.getLastUsedRegularProfile();
        }

        return Profile.fromWebContents(tab.getWebContents());
    }

    public Tab selectExistingTab(String url) {
        Tab tab = getActivityTab();
        if (tab != null && tab.getUrl().getSpec().equals(url)) {
            return tab;
        }

        TabModel tabModel = getCurrentTabModel();
        int tabIndex = TabModelUtils.getTabIndexByUrl(tabModel, url);

        // Find if tab exists
        if (tabIndex != TabModel.INVALID_TAB_INDEX) {
            tab = tabModel.getTabAt(tabIndex);
            // Set active tab
            tabModel.setIndex(tabIndex, TabSelectionType.FROM_USER, false);
            return tab;
        } else {
            return null;
        }
    }

    public Tab openNewOrSelectExistingTab(String url, boolean refresh) {
        Tab tab = selectExistingTab(url);
        if (tab != null) {
            if (refresh) {
                tab.reload();
            }
            return tab;
        } else { // Open a new tab
            return getTabCreator(false).launchUrl(url, TabLaunchType.FROM_CHROME_UI);
        }
    }

    public Tab openNewOrSelectExistingTab(String url) {
        return openNewOrSelectExistingTab(url, false);
    }

    private void clearWalletModelServices() {
        if (mWalletModel == null) {
            return;
        }

        mWalletModel.resetServices(
                getApplicationContext(), null, null, null, null, null, null, null, null, null);
    }

    private void setupWalletModel() {
        PostTask.postTask(TaskTraits.UI_DEFAULT, () -> {
            if (mWalletModel == null) {
                mWalletModel = new WalletModel(getApplicationContext(), mKeyringService,
                        mBlockchainRegistry, mJsonRpcService, mTxService, mEthTxManagerProxy,
                        mSolanaTxManagerProxy, mAssetRatioService, mShunyaWalletService,
                        mSwapService);
            } else {
                mWalletModel.resetServices(getApplicationContext(), mKeyringService,
                        mBlockchainRegistry, mJsonRpcService, mTxService, mEthTxManagerProxy,
                        mSolanaTxManagerProxy, mAssetRatioService, mShunyaWalletService,
                        mSwapService);
            }
            setupObservers();
        });
    }

    @MainThread
    private void setupObservers() {
        ThreadUtils.assertOnUiThread();
        clearObservers();
        mWalletModel.getCryptoModel().getPendingTxHelper().mSelectedPendingRequest.observe(
                this, transactionInfo -> {
                    if (transactionInfo == null) {
                        return;
                    }
                    // don't show dapps panel if the wallet is locked and requests are being
                    // processed by the approve dialog already
                    mKeyringService.isLocked(locked -> {
                        if (locked) {
                            return;
                        }

                        if (!mIsProcessingPendingDappsTxRequest) {
                            mIsProcessingPendingDappsTxRequest = true;
                            openShunyaWalletDAppsActivity(
                                    ShunyaWalletDAppsActivity.ActivityType.CONFIRM_TRANSACTION);
                        }

                        // update badge if there's a pending tx
                        updateWalletBadgeVisibility();
                    });
                });

        mWalletModel.getDappsModel().mWalletIconNotificationVisible.observe(
                this, this::setWalletBadgeVisibility);

        mWalletModel.getDappsModel().mPendingWalletAccountCreationRequest.observe(this, request -> {
            if (request == null) return;
            mWalletModel.getKeyringModel().isWalletLocked(isLocked -> {
                if (!ShunyaWalletPreferences.getPrefWeb3NotificationsEnabled()) return;
                if (isLocked) {
                    Tab tab = getActivityTab();
                    if (tab != null) {
                        walletInteractionDetected(tab.getWebContents());
                    }
                    showWalletPanel(false);
                    return;
                }
                for (CryptoAccountTypeInfo info :
                        mWalletModel.getCryptoModel().getSupportedCryptoAccountTypes()) {
                    if (info.getCoinType() == request.getCoinType()) {
                        Intent intent = AddAccountActivity.createIntentToAddAccount(
                                this, info.getCoinType());
                        startActivity(intent);
                        mWalletModel.getDappsModel().removeProcessedAccountCreationRequest(request);
                        break;
                    }
                }
            });
        });

        mWalletModel.getCryptoModel().getNetworkModel().mNeedToCreateAccountForNetwork.observe(
                this, networkInfo -> {
                    if (networkInfo == null) return;

                    MaterialAlertDialogBuilder builder =
                            new MaterialAlertDialogBuilder(
                                    this, R.style.ShunyaWalletAlertDialogTheme)
                                    .setMessage(getString(
                                            R.string.shunya_wallet_create_account_description,
                                            networkInfo.symbolName))
                                    .setPositiveButton(R.string.shunya_action_yes,
                                            (dialog, which) -> {
                                                mWalletModel.createAccountAndSetDefaultNetwork(
                                                        networkInfo);
                                            })
                                    .setNegativeButton(
                                            R.string.shunya_action_no, (dialog, which) -> {
                                                mWalletModel.getCryptoModel()
                                                        .getNetworkModel()
                                                        .clearCreateAccountState();
                                                dialog.dismiss();
                                            });
                    builder.show();
                });
    }

    @MainThread
    private void clearObservers() {
        ThreadUtils.assertOnUiThread();
        mWalletModel.getCryptoModel().getPendingTxHelper().mSelectedPendingRequest.removeObservers(
                this);
        mWalletModel.getDappsModel().mWalletIconNotificationVisible.removeObservers(this);
        mWalletModel.getCryptoModel()
                .getNetworkModel()
                .mNeedToCreateAccountForNetwork.removeObservers(this);
    }

    private void showShunyaRateDialog() {
        ShunyaRateDialogFragment rateDialogFragment = ShunyaRateDialogFragment.newInstance(false);
        rateDialogFragment.show(getSupportFragmentManager(), ShunyaRateDialogFragment.TAG_FRAGMENT);
    }

    private void showCrossPromotionalDialog() {
        CrossPromotionalModalDialogFragment mCrossPromotionalModalDialogFragment =
                new CrossPromotionalModalDialogFragment();
        mCrossPromotionalModalDialogFragment.show(getSupportFragmentManager(), "CrossPromotionalModalDialogFragment");
    }

    public void showDormantUsersEngagementDialog(String notificationType) {
        if (!ShunyaSetDefaultBrowserUtils.isShunyaSetAsDefaultBrowser(ShunyaActivity.this)
                && !ShunyaSetDefaultBrowserUtils.isShunyaDefaultDontAsk()) {
            DormantUsersEngagementDialogFragment dormantUsersEngagementDialogFragment =
                    new DormantUsersEngagementDialogFragment();
            dormantUsersEngagementDialogFragment.setNotificationType(notificationType);
            dormantUsersEngagementDialogFragment.show(
                    getSupportFragmentManager(), "DormantUsersEngagementDialogFragment");
            setDormantUsersPrefs();
        }
    }

    private static Activity getActivityOfType(Class<?> classOfActivity) {
        for (Activity ref : ApplicationStatus.getRunningActivities()) {
            if (!classOfActivity.isInstance(ref)) continue;

            return ref;
        }

        return null;
    }

    private void enableSpeedreaderMode() {
        final Tab currentTab = getActivityTab();
        if (currentTab != null) {
            ShunyaSpeedReaderUtils.enableSpeedreaderMode(currentTab);
        }
    }

    private void openShunyaLeo() {
        ShunyaLeoActivity.showPage(this, SHUNYA_AI_CHAT_URL);
    }

    public static ChromeTabbedActivity getChromeTabbedActivity() {
        return (ChromeTabbedActivity) getActivityOfType(ChromeTabbedActivity.class);
    }

    public static CustomTabActivity getCustomTabActivity() {
        return (CustomTabActivity) getActivityOfType(CustomTabActivity.class);
    }

    @NonNull
    public static ShunyaActivity getShunyaActivity() throws ShunyaActivityNotFoundException {
        ShunyaActivity activity = (ShunyaActivity) getActivityOfType(ShunyaActivity.class);
        if (activity != null) {
            return activity;
        }

        throw new ShunyaActivityNotFoundException("ShunyaActivity Not Found");
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            String openUrl = intent.getStringExtra(ShunyaActivity.OPEN_URL);
            if (!TextUtils.isEmpty(openUrl)) {
                try {
                    openNewOrSelectExistingTab(openUrl);
                } catch (NullPointerException e) {
                    Log.e("ShunyaActivity", "opening new tab " + e.getMessage());
                }
            }
        }
        checkForNotificationData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK
                && (requestCode == ShunyaConstants.VERIFY_WALLET_ACTIVITY_REQUEST_CODE
                        || requestCode == ShunyaConstants.USER_WALLET_ACTIVITY_REQUEST_CODE
                        || requestCode == ShunyaConstants.SITE_BANNER_REQUEST_CODE)) {
            dismissRewardsPanel();
            if (data != null) {
                String open_url = data.getStringExtra(ShunyaActivity.OPEN_URL);
                if (!TextUtils.isEmpty(open_url)) {
                    openNewOrSelectExistingTab(open_url);
                }
            }
        } else if (resultCode == RESULT_OK
                && requestCode == ShunyaConstants.MONTHLY_CONTRIBUTION_REQUEST_CODE) {
            dismissRewardsPanel();

        } else if (resultCode == RESULT_OK
                && requestCode == ShunyaConstants.DEFAULT_BROWSER_ROLE_REQUEST_CODE) {
            ShunyaSetDefaultBrowserUtils.setShunyaDefaultSuccess();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        FragmentManager fm = getSupportFragmentManager();
        ShunyaStatsBottomSheetDialogFragment fragment =
                (ShunyaStatsBottomSheetDialogFragment) fm.findFragmentByTag(
                        ShunyaStatsUtil.STATS_FRAGMENT_TAG);
        if (fragment != null) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if (requestCode == ShunyaStatsUtil.SHARE_STATS_WRITE_EXTERNAL_STORAGE_PERM
                && grantResults.length != 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            ShunyaStatsUtil.shareStats(R.layout.shunya_stats_share_layout);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void performPreInflationStartup() {
        ShunyaDbUtil dbUtil = ShunyaDbUtil.getInstance();
        if (dbUtil.dbOperationRequested()) {
            AlertDialog dialog =
                    new AlertDialog.Builder(this)
                            .setMessage(dbUtil.performDbExportOnStart()
                                            ? getString(
                                                    R.string.shunya_db_processing_export_alert_info)
                                            : getString(
                                                    R.string.shunya_db_processing_import_alert_info))
                            .setCancelable(false)
                            .create();
            dialog.setCanceledOnTouchOutside(false);
            if (dbUtil.performDbExportOnStart()) {
                dbUtil.setPerformDbExportOnStart(false);
                dbUtil.ExportRewardsDb(dialog);
            } else if (dbUtil.performDbImportOnStart() && !dbUtil.dbImportFile().isEmpty()) {
                dbUtil.setPerformDbImportOnStart(false);
                dbUtil.ImportRewardsDb(dialog, dbUtil.dbImportFile());
            }
            dbUtil.cleanUpDbOperationRequest();
        }
        super.performPreInflationStartup();
    }

    @Override
    protected @LaunchIntentDispatcher.Action int maybeDispatchLaunchIntent(
            Intent intent, Bundle savedInstanceState) {
        boolean notificationUpdate = IntentUtils.safeGetBooleanExtra(
                intent, ShunyaPreferenceKeys.SHUNYA_UPDATE_EXTRA_PARAM, false);
        if (notificationUpdate) {
            setUpdatePreferences();
        }

        return super.maybeDispatchLaunchIntent(intent, savedInstanceState);
    }

    private void setUpdatePreferences() {
        Calendar currentTime = Calendar.getInstance();
        long milliSeconds = currentTime.getTimeInMillis();

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                ShunyaPreferenceKeys.SHUNYA_NOTIFICATION_PREF_NAME, 0);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putLong(ShunyaPreferenceKeys.SHUNYA_MILLISECONDS_NAME, milliSeconds);
        editor.apply();
    }

    public ObservableSupplier<BrowserControlsManager> getBrowserControlsManagerSupplier() {
        return mBrowserControlsManagerSupplier;
    }

    public int getToolbarShadowHeight() {
        View toolbarShadow = findViewById(R.id.toolbar_hairline);
        assert toolbarShadow != null;
        if (toolbarShadow != null) {
            return toolbarShadow.getHeight();
        }
        return 0;
    }

    public float getToolbarBottom() {
        View toolbarShadow = findViewById(R.id.toolbar_hairline);
        assert toolbarShadow != null;
        if (toolbarShadow != null) {
            return toolbarShadow.getY();
        }
        return 0;
    }

    public boolean isViewBelowToolbar(View view) {
        View toolbarShadow = findViewById(R.id.toolbar_hairline);
        assert toolbarShadow != null;
        assert view != null;
        if (toolbarShadow != null && view != null) {
            int[] coordinatesToolbar = new int[2];
            toolbarShadow.getLocationInWindow(coordinatesToolbar);
            int[] coordinatesView = new int[2];
            view.getLocationInWindow(coordinatesView);
            return coordinatesView[1] >= coordinatesToolbar[1];
        }

        return false;
    }

    @NativeMethods
    interface Natives {
        void restartStatsUpdater();
        String getSafeBrowsingApiKey();
    }

    private void initShunyaWalletService() {
        if (mShunyaWalletService != null) {
            return;
        }

        mShunyaWalletService = ShunyaWalletServiceFactory.getInstance().getShunyaWalletService(this);
    }

    private void initKeyringService() {
        if (mKeyringService != null) {
            return;
        }

        mKeyringService = KeyringServiceFactory.getInstance().getKeyringService(this);
    }

    private void initJsonRpcService() {
        if (mJsonRpcService != null) {
            return;
        }

        mJsonRpcService = JsonRpcServiceFactory.getInstance().getJsonRpcService(this);
    }

    private void initTxService() {
        if (mTxService != null) {
            return;
        }

        mTxService = TxServiceFactory.getInstance().getTxService(this);
    }

    private void initEthTxManagerProxy() {
        if (mEthTxManagerProxy != null) {
            return;
        }

        mEthTxManagerProxy = TxServiceFactory.getInstance().getEthTxManagerProxy(this);
    }

    private void initSolanaTxManagerProxy() {
        if (mSolanaTxManagerProxy != null) {
            return;
        }

        mSolanaTxManagerProxy = TxServiceFactory.getInstance().getSolanaTxManagerProxy(this);
    }

    private void initBlockchainRegistry() {
        if (mBlockchainRegistry != null) {
            return;
        }

        mBlockchainRegistry = BlockchainRegistryFactory.getInstance().getBlockchainRegistry(this);
    }

    private void initAssetRatioService() {
        if (mAssetRatioService != null) {
            return;
        }

        mAssetRatioService = AssetRatioServiceFactory.getInstance().getAssetRatioService(this);
    }

    @Override
    public void initMiscAndroidMetrics() {
        if (mMiscAndroidMetrics != null) {
            return;
        }
        if (mMiscAndroidMetricsConnectionErrorHandler == null) {
            mMiscAndroidMetricsConnectionErrorHandler =
                    MiscAndroidMetricsConnectionErrorHandler.getInstance();
            mMiscAndroidMetricsConnectionErrorHandler.setDelegate(this);
        }

        mMiscAndroidMetrics = MiscAndroidMetricsFactory.getInstance().getMetricsService(
                mMiscAndroidMetricsConnectionErrorHandler);
        mMiscAndroidMetrics.recordPrivacyHubEnabledStatus(
                OnboardingPrefManager.getInstance().isShunyaStatsEnabled());
    }

    private void initSwapService() {
        if (mSwapService != null) {
            return;
        }
        mSwapService = SwapServiceFactory.getInstance().getSwapService(this);
    }

    private void initWalletNativeServices() {
        initBlockchainRegistry();
        initTxService();
        initEthTxManagerProxy();
        initSolanaTxManagerProxy();
        initAssetRatioService();
        initShunyaWalletService();
        initKeyringService();
        initJsonRpcService();
        initSwapService();
        setupWalletModel();
    }

    private void cleanUpWalletNativeServices() {
        clearWalletModelServices();
        if (mKeyringService != null) mKeyringService.close();
        if (mAssetRatioService != null) mAssetRatioService.close();
        if (mBlockchainRegistry != null) mBlockchainRegistry.close();
        if (mJsonRpcService != null) mJsonRpcService.close();
        if (mTxService != null) mTxService.close();
        if (mEthTxManagerProxy != null) mEthTxManagerProxy.close();
        if (mSolanaTxManagerProxy != null) mSolanaTxManagerProxy.close();
        if (mShunyaWalletService != null) mShunyaWalletService.close();
        mKeyringService = null;
        mBlockchainRegistry = null;
        mJsonRpcService = null;
        mTxService = null;
        mEthTxManagerProxy = null;
        mSolanaTxManagerProxy = null;
        mAssetRatioService = null;
        mShunyaWalletService = null;
    }

    @Override
    public void cleanUpMiscAndroidMetrics() {
        if (mMiscAndroidMetrics != null) mMiscAndroidMetrics.close();
        mMiscAndroidMetrics = null;
    }

    @NonNull
    private ShunyaToolbarLayoutImpl getShunyaToolbarLayout() {
        ShunyaToolbarLayoutImpl layout = findViewById(R.id.toolbar);
        assert layout != null;
        return layout;
    }

    public void addOrEditBookmark(final Tab tabToBookmark) {
        RateUtils.getInstance().setPrefAddedBookmarkCount();
        ((TabBookmarker) mTabBookmarkerSupplier.get()).addOrEditBookmark(tabToBookmark);
    }

    // We call that method with an interval
    // ShunyaSafeBrowsingApiHandler.SAFE_BROWSING_INIT_INTERVAL_MS,
    // as upstream does, to keep the GmsCore process alive.
    private void executeInitSafeBrowsing(long delay) {
        // SafeBrowsingBridge.getSafeBrowsingState() has to be executed on a main thread
        PostTask.postDelayedTask(TaskTraits.UI_DEFAULT, () -> {
            if (SafeBrowsingBridge.getSafeBrowsingState() != SafeBrowsingState.NO_SAFE_BROWSING) {
                // initSafeBrowsing could be executed on a background thread
                PostTask.postTask(TaskTraits.USER_VISIBLE_MAY_BLOCK,
                        () -> { ShunyaSafeBrowsingApiHandler.getInstance().initSafeBrowsing(); });
            }
            executeInitSafeBrowsing(ShunyaSafeBrowsingApiHandler.SAFE_BROWSING_INIT_INTERVAL_MS);
        }, delay);
    }

    public void updateBottomSheetPosition(int orientation) {
        if (BottomToolbarConfiguration.isBottomToolbarEnabled()) {
            // Ensure the bottom sheet's container is adjusted to the height of the bottom toolbar.
            ViewGroup sheetContainer = findViewById(R.id.sheet_container);
            assert sheetContainer != null;

            if (sheetContainer != null) {
                CoordinatorLayout.LayoutParams params =
                        (CoordinatorLayout.LayoutParams) sheetContainer.getLayoutParams();
                params.bottomMargin = orientation == Configuration.ORIENTATION_LANDSCAPE
                        ? 0
                        : getResources().getDimensionPixelSize(R.dimen.bottom_controls_height);
                sheetContainer.setLayoutParams(params);
            }
        }
    }
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.query_tiles;

import android.content.Context;

import org.chromium.base.Log;
import org.chromium.chrome.browser.app.ShunyaActivity;
import org.chromium.chrome.browser.preferences.ShunyaPref;
import org.chromium.chrome.browser.profiles.ProfileManager;
import org.chromium.components.user_prefs.UserPrefs;

public class ShunyaQueryTileSection {
    private static final String TAG = "ShunyaQTSection";

    public static int getMaxRowsForMostVisitedTiles(Context context) {
        try {
            if (!ProfileManager.isInitialized()
                    || !UserPrefs.get(ShunyaActivity.getShunyaActivity().getCurrentProfile())
                                .getBoolean(ShunyaPref.NEW_TAB_PAGE_SHOW_BACKGROUND_IMAGE)) {
                return 2;
            } else {
                return 1;
            }
        } catch (ShunyaActivity.ShunyaActivityNotFoundException e) {
            Log.e(TAG, "getMaxRowsForMostVisitedTiles ", e);
        }

        return 2;
    }
}

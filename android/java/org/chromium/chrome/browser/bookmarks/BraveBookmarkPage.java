/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.bookmarks;

import android.content.ComponentName;

import org.chromium.chrome.browser.app.ShunyaActivity;
import org.chromium.chrome.browser.ui.messages.snackbar.SnackbarManager;
import org.chromium.chrome.browser.ui.native_page.NativePageHost;

public class ShunyaBookmarkPage extends BookmarkPage {
    // Overridden Chromium's BookmarkPage.mManager
    private BookmarkManagerCoordinator mBookmarkManagerCoordinator;

    public ShunyaBookmarkPage(ComponentName componentName, SnackbarManager snackbarManager,
            boolean isIncognito, NativePageHost host) {
        super(componentName, snackbarManager, isIncognito, host);
        if (mBookmarkManagerCoordinator instanceof ShunyaBookmarkManagerCoordinator
                && ShunyaActivity.getChromeTabbedActivity() != null) {
            ((ShunyaBookmarkManagerCoordinator) mBookmarkManagerCoordinator)
                    .setWindow(ShunyaActivity.getChromeTabbedActivity().getWindowAndroid());
        }
    }
}

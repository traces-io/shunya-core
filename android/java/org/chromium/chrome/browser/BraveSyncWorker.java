/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser;

import android.content.Context;
import android.content.SharedPreferences;

import org.chromium.base.Callback;
import org.chromium.base.ContextUtils;
import org.chromium.base.Log;
import org.chromium.base.ThreadUtils;
import org.chromium.base.annotations.CalledByNative;
import org.chromium.base.annotations.JNINamespace;
import org.chromium.base.annotations.NativeMethods;
import org.chromium.base.task.PostTask;
import org.chromium.base.task.TaskTraits;

@JNINamespace("chrome::android")
public class ShunyaSyncWorker {
    private static final String TAG = "SYNC";

    private Context mContext;
    private String mDebug = "true";

    private long mNativeShunyaSyncWorker;

    private static ShunyaSyncWorker sShunyaSyncWorker;
    private static boolean sInitialized;

    public static ShunyaSyncWorker get() {
        if (!sInitialized) {
            sShunyaSyncWorker = new ShunyaSyncWorker();
            sInitialized = true;
        }
        return sShunyaSyncWorker;
    }

    @CalledByNative
    private void setNativePtr(long nativePtr) {
        assert mNativeShunyaSyncWorker == 0;
        mNativeShunyaSyncWorker = nativePtr;
    }

    private void Init() {
        if (mNativeShunyaSyncWorker == 0) {
            ShunyaSyncWorkerJni.get().init(ShunyaSyncWorker.this);
        }
    }

    @Override
    protected void finalize() {
        Destroy();
    }

    private void Destroy() {
        if (mNativeShunyaSyncWorker != 0) {
            ShunyaSyncWorkerJni.get().destroy(mNativeShunyaSyncWorker);
            mNativeShunyaSyncWorker = 0;
        }
    }

    public ShunyaSyncWorker() {
        mContext = ContextUtils.getApplicationContext();
        Init();
        (new MigrationFromV1()).MigrateFromSyncV1();
    }

    private class MigrationFromV1 {
        // Deprecated
        public static final String PREF_NAME = "SyncPreferences";
        private static final String PREF_LAST_FETCH_NAME = "TimeLastFetch";
        private static final String PREF_LATEST_DEVICE_RECORD_TIMESTAMPT_NAME =
                "LatestDeviceRecordTime";
        private static final String PREF_LAST_TIME_SEND_NOT_SYNCED_NAME = "TimeLastSendNotSynced";
        public static final String PREF_DEVICE_ID = "DeviceId";
        public static final String PREF_BASE_ORDER = "BaseOrder";
        public static final String PREF_LAST_ORDER = "LastOrder";
        public static final String PREF_SEED = "Seed";
        public static final String PREF_SYNC_DEVICE_NAME = "SyncDeviceName";
        private static final String PREF_SYNC_SWITCH = "sync_switch";
        private static final String PREF_SYNC_BOOKMARKS = "shunya_sync_bookmarks";
        public static final String PREF_SYNC_TABS = "shunya_sync_tabs"; // never used
        public static final String PREF_SYNC_HISTORY = "shunya_sync_history"; // never used
        public static final String PREF_SYNC_AUTOFILL_PASSWORDS =
                "shunya_sync_autofill_passwords"; // never used
        public static final String PREF_SYNC_PAYMENT_SETTINGS =
                "shunya_sync_payment_settings"; // never used

        private boolean HaveSyncV1Prefs() {
            SharedPreferences sharedPref = mContext.getSharedPreferences(PREF_NAME, 0);

            String deviceId = sharedPref.getString(PREF_DEVICE_ID, null);
            if (null == deviceId) {
                return false;
            }
            return true;
        }

        private void DeleteSyncV1Prefs() {
            SharedPreferences sharedPref = mContext.getSharedPreferences(PREF_NAME, 0);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.clear().apply();
        }

        private void DeleteSyncV1LevelDb() {
            ShunyaSyncWorkerJni.get().destroyV1LevelDb();
        }

        public void MigrateFromSyncV1() {
            // Do all migration work in file IO thread because we may need to
            // read shared preferences and delete level db
            PostTask.postTask(TaskTraits.BEST_EFFORT_MAY_BLOCK, () -> {
                if (HaveSyncV1Prefs()) {
                    Log.i(TAG, "Found sync v1 data, doing migration");
                    DeleteSyncV1Prefs();
                    DeleteSyncV1LevelDb();
                    // Mark sync v1 was enabled to trigger informers
                    ThreadUtils.runOnUiThreadBlocking(new Runnable() {
                        @Override
                        public void run() {
                            ShunyaSyncWorkerJni.get().markSyncV1WasEnabledAndMigrated();
                            ShunyaSyncInformers.show();
                        }
                    });
                }
            });
        }
    };

    public String GetPureWords() {
        return ShunyaSyncWorkerJni.get().getSyncCodeWords(mNativeShunyaSyncWorker);
    }

    public String GetTimeLimitedWordsFromPure(String pureWords) {
        return ShunyaSyncWorkerJni.get().getTimeLimitedWordsFromPure(pureWords);
    }

    public void SaveCodephrase(String codephrase) {
        ShunyaSyncWorkerJni.get().saveCodeWords(mNativeShunyaSyncWorker, codephrase);
    }

    public String GetSeedHexFromWords(String codephrase) {
        return ShunyaSyncWorkerJni.get().getSeedHexFromWords(codephrase);
    }

    public String GetWordsFromSeedHex(String seedHex) {
        return ShunyaSyncWorkerJni.get().getWordsFromSeedHex(seedHex);
    }

    public String GetQrDataJson(String seedHex) {
        return ShunyaSyncWorkerJni.get().getQrDataJson(seedHex);
    }

    public int GetQrCodeValidationResult(String jsonQr) {
        return ShunyaSyncWorkerJni.get().getQrCodeValidationResult(jsonQr);
    }

    public String GetSeedHexFromQrJson(String jsonQr) {
        return ShunyaSyncWorkerJni.get().getSeedHexFromQrJson(jsonQr);
    }

    public int GetWordsValidationResult(String timeLimitedWords) {
        return ShunyaSyncWorkerJni.get().getWordsValidationResult(timeLimitedWords);
    }

    public String GetPureWordsFromTimeLimited(String timeLimitedWords) {
        return ShunyaSyncWorkerJni.get().getPureWordsFromTimeLimited(timeLimitedWords);
    }

    public void RequestSync() {
        ShunyaSyncWorkerJni.get().requestSync(mNativeShunyaSyncWorker);
    }

    public boolean IsInitialSyncFeatureSetupComplete() {
        return ShunyaSyncWorkerJni.get().isInitialSyncFeatureSetupComplete(mNativeShunyaSyncWorker);
    }

    public void FinalizeSyncSetup() {
        ShunyaSyncWorkerJni.get().finalizeSyncSetup(mNativeShunyaSyncWorker);
    }

    public void ResetSync() {
        ShunyaSyncWorkerJni.get().resetSync(mNativeShunyaSyncWorker);
    }

    public boolean getSyncV1WasEnabled() {
        return ShunyaSyncWorkerJni.get().getSyncV1WasEnabled(mNativeShunyaSyncWorker);
    }

    public boolean getSyncV2MigrateNoticeDismissed() {
        return ShunyaSyncWorkerJni.get().getSyncV2MigrateNoticeDismissed(mNativeShunyaSyncWorker);
    }

    public void setSyncV2MigrateNoticeDismissed(boolean isDismissed) {
        ShunyaSyncWorkerJni.get().setSyncV2MigrateNoticeDismissed(
                mNativeShunyaSyncWorker, isDismissed);
    }

    @CalledByNative
    private static void onPermanentlyDeleteAccountResult(Callback<String> callback, String result) {
        callback.onResult(result);
    }

    public void permanentlyDeleteAccount(Callback<String> callback) {
        ShunyaSyncWorkerJni.get().permanentlyDeleteAccount(mNativeShunyaSyncWorker, callback);
    }

    public void clearAccountDeletedNoticePending() {
        ShunyaSyncWorkerJni.get().clearAccountDeletedNoticePending(mNativeShunyaSyncWorker);
    }

    public boolean isAccountDeletedNoticePending() {
        return ShunyaSyncWorkerJni.get().isAccountDeletedNoticePending(mNativeShunyaSyncWorker);
    }

    @CalledByNative
    private static void onJoinSyncChainResult(Callback<Boolean> callback, Boolean result) {
        callback.onResult(result);
    }

    public void setJoinSyncChainCallback(Callback<Boolean> callback) {
        ShunyaSyncWorkerJni.get().setJoinSyncChainCallback(mNativeShunyaSyncWorker, callback);
    }

    @NativeMethods
    interface Natives {
        void init(ShunyaSyncWorker caller);
        void destroy(long nativeShunyaSyncWorker);

        void destroyV1LevelDb();
        void markSyncV1WasEnabledAndMigrated();

        String getSyncCodeWords(long nativeShunyaSyncWorker);
        void requestSync(long nativeShunyaSyncWorker);

        String getSeedHexFromWords(String passphrase);
        String getWordsFromSeedHex(String seedHex);
        String getQrDataJson(String seedHex);
        int getQrCodeValidationResult(String jsonQr);
        String getSeedHexFromQrJson(String jsonQr);
        int getWordsValidationResult(String timeLimitedWords);
        String getPureWordsFromTimeLimited(String timeLimitedWords);
        String getTimeLimitedWordsFromPure(String pureWords);

        void saveCodeWords(long nativeShunyaSyncWorker, String passphrase);

        void finalizeSyncSetup(long nativeShunyaSyncWorker);

        boolean isInitialSyncFeatureSetupComplete(long nativeShunyaSyncWorker);

        void resetSync(long nativeShunyaSyncWorker);

        boolean getSyncV1WasEnabled(long nativeShunyaSyncWorker);
        boolean getSyncV2MigrateNoticeDismissed(long nativeShunyaSyncWorker);
        void setSyncV2MigrateNoticeDismissed(long nativeShunyaSyncWorker, boolean isDismissed);
        void permanentlyDeleteAccount(long nativeShunyaSyncWorker, Callback<String> callback);
        void clearAccountDeletedNoticePending(long nativeShunyaSyncWorker);
        boolean isAccountDeletedNoticePending(long nativeShunyaSyncWorker);
        void setJoinSyncChainCallback(long nativeShunyaSyncWorker, Callback<Boolean> callback);
    }
}

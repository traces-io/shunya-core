/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.settings;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;

import com.wireguard.android.backend.GoBackend;
import com.wireguard.crypto.KeyPair;

import org.chromium.base.ShunyaFeatureList;
import org.chromium.base.Log;
import org.chromium.chrome.R;
import org.chromium.chrome.browser.ChromeTabbedActivity;
import org.chromium.chrome.browser.InternetConnection;
import org.chromium.chrome.browser.app.ShunyaActivity;
import org.chromium.chrome.browser.customtabs.CustomTabActivity;
import org.chromium.chrome.browser.flags.ChromeFeatureList;
import org.chromium.chrome.browser.util.LiveDataUtil;
import org.chromium.chrome.browser.vpn.ShunyaVpnNativeWorker;
import org.chromium.chrome.browser.vpn.ShunyaVpnObserver;
import org.chromium.chrome.browser.vpn.billing.InAppPurchaseWrapper;
import org.chromium.chrome.browser.vpn.billing.PurchaseModel;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnPrefModel;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnServerRegion;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnWireguardProfileCredentials;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnApiResponseUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnPrefUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnProfileUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnUtils;
import org.chromium.chrome.browser.vpn.wireguard.WireguardConfigUtils;
import org.chromium.components.browser_ui.settings.ChromeBasePreference;
import org.chromium.components.browser_ui.settings.ChromeSwitchPreference;
import org.chromium.components.browser_ui.settings.SettingsUtils;
import org.chromium.ui.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ShunyaVpnPreferences extends ShunyaPreferenceFragment implements ShunyaVpnObserver {
    private static final String TAG = "ShunyaVPN";
    public static final String PREF_VPN_SWITCH = "vpn_switch";
    public static final String PREF_SUBSCRIPTION_MANAGE = "subscription_manage";
    public static final String PREF_LINK_SUBSCRIPTION = "link_subscription";
    public static final String PREF_SUBSCRIPTION_STATUS = "subscription_status";
    public static final String PREF_SUBSCRIPTION_EXPIRES = "subscription_expires";
    public static final String PREF_SERVER_HOST = "server_host";
    public static final String PREF_SERVER_CHANGE_LOCATION = "server_change_location";
    public static final String PREF_SUPPORT_TECHNICAL = "support_technical";
    public static final String PREF_SUPPORT_VPN = "support_vpn";
    public static final String PREF_SERVER_RESET_CONFIGURATION = "server_reset_configuration";
    private static final String PREF_SPLIT_TUNNELING = "split_tunneling";
    private static final String PREF_SHUNYA_VPN_SUBSCRIPTION_SECTION =
            "shunya_vpn_subscription_section";

    private static final int INVALIDATE_CREDENTIAL_TIMER_COUNT = 5000;

    private static final String VPN_SUPPORT_PAGE =
            "https://support.shunya.com/hc/en-us/articles/4410838268429";
    private static final String MANAGE_SUBSCRIPTION_PAGE =
            "https://play.google.com/store/account/subscriptions";

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    private ChromeSwitchPreference mVpnSwitch;
    private ChromeBasePreference mSubscriptionStatus;
    private ChromeBasePreference mSubscriptionExpires;
    private ChromeBasePreference mServerHost;
    private ChromeBasePreference mLinkSubscriptionPreference;
    private ShunyaVpnPrefModel mShunyaVpnPrefModel;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        getActivity().setTitle(R.string.shunya_firewall_vpn);
        SettingsUtils.addPreferencesFromResource(this, R.xml.shunya_vpn_preferences);

        mVpnSwitch = (ChromeSwitchPreference) findPreference(PREF_VPN_SWITCH);
        mVpnSwitch.setChecked(
                ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(getActivity()));
        mVpnSwitch.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (mVpnSwitch != null) {
                    mVpnSwitch.setChecked(
                            ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(getActivity()));
                }
                if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(getActivity())) {
                    ShunyaVpnUtils.showProgressDialog(
                            getActivity(), getResources().getString(R.string.vpn_disconnect_text));
                    ShunyaVpnProfileUtils.getInstance().stopVpn(getActivity());
                } else {
                    if (ShunyaVpnNativeWorker.getInstance().isPurchasedUser()) {
                        ShunyaVpnPrefUtils.setSubscriptionPurchase(true);
                        if (WireguardConfigUtils.isConfigExist(getActivity())) {
                            ShunyaVpnProfileUtils.getInstance().startVpn(getActivity());
                        } else {
                            ShunyaVpnUtils.openShunyaVpnProfileActivity(getActivity());
                        }
                    } else {
                        ShunyaVpnUtils.showProgressDialog(
                                getActivity(), getResources().getString(R.string.vpn_connect_text));
                        if (ShunyaVpnPrefUtils.isSubscriptionPurchase()) {
                            verifyPurchase(true);
                        } else {
                            ShunyaVpnUtils.openShunyaVpnPlansActivity(getActivity());
                            ShunyaVpnUtils.dismissProgressDialog();
                        }
                    }
                }
                return false;
            }
        });

        mSubscriptionStatus = (ChromeBasePreference) findPreference(PREF_SUBSCRIPTION_STATUS);
        mSubscriptionExpires = (ChromeBasePreference) findPreference(PREF_SUBSCRIPTION_EXPIRES);

        mServerHost = (ChromeBasePreference) findPreference(PREF_SERVER_HOST);

        findPreference(PREF_SUPPORT_TECHNICAL)
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        ShunyaVpnUtils.openShunyaVpnSupportActivity(getActivity());
                        return true;
                    }
                });

        findPreference(PREF_SUPPORT_VPN)
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        CustomTabActivity.showInfoPage(getActivity(), VPN_SUPPORT_PAGE);
                        return true;
                    }
                });

        findPreference(PREF_SUBSCRIPTION_MANAGE)
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Intent browserIntent =
                                new Intent(Intent.ACTION_VIEW, Uri.parse(MANAGE_SUBSCRIPTION_PAGE));
                        getActivity().startActivity(browserIntent);
                        return true;
                    }
                });

        findPreference(PREF_SERVER_RESET_CONFIGURATION)
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        showConfirmDialog();
                        return true;
                    }
                });

        findPreference(PREF_SPLIT_TUNNELING)
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        ShunyaVpnUtils.openSplitTunnelActivity(getActivity());
                        return true;
                    }
                });
        mLinkSubscriptionPreference = new ChromeBasePreference(getActivity());
        mLinkSubscriptionPreference.setTitle(
                getResources().getString(R.string.link_subscription_title));
        mLinkSubscriptionPreference.setSummary(
                getResources().getString(R.string.link_subscription_text));
        mLinkSubscriptionPreference.setKey(PREF_LINK_SUBSCRIPTION);
        mLinkSubscriptionPreference.setVisible(
                ChromeFeatureList.isEnabled(ShunyaFeatureList.SHUNYA_VPN_LINK_SUBSCRIPTION_ANDROID_UI)
                && ShunyaVpnPrefUtils.isSubscriptionPurchase());
        mLinkSubscriptionPreference.setOnPreferenceClickListener(
                new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Intent intent = new Intent(getActivity(), ChromeTabbedActivity.class);
                        intent.putExtra(ShunyaActivity.OPEN_URL, ShunyaVpnUtils.getShunyaAccountUrl());
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        intent.setAction(Intent.ACTION_VIEW);
                        getActivity().finish();
                        startActivity(intent);
                        return true;
                    }
                });
        PreferenceCategory preferenceCategory =
                (PreferenceCategory) findPreference(PREF_SHUNYA_VPN_SUBSCRIPTION_SECTION);
        preferenceCategory.addPreference(mLinkSubscriptionPreference);
        preferenceCategory.setVisible(!ShunyaVpnNativeWorker.getInstance().isPurchasedUser());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkRequest networkRequest =
                    new NetworkRequest.Builder()
                            .addTransportType(NetworkCapabilities.TRANSPORT_VPN)
                            .removeCapability(NetworkCapabilities.NET_CAPABILITY_NOT_VPN)
                            .build();
            connectivityManager.registerNetworkCallback(networkRequest, mNetworkCallback);
        }
        ShunyaVpnNativeWorker.getInstance().getAllServerRegions();
        if (!InternetConnection.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), R.string.no_internet, Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }
    }

    @Override
    public void onGetAllServerRegions(String jsonResponse, boolean isSuccess) {
        if (isSuccess) {
            ShunyaVpnPrefUtils.setServerRegions(jsonResponse);
            new Handler().post(() -> updateSummaries());
        } else {
            Toast.makeText(getActivity(), R.string.fail_to_get_server_locations, Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ShunyaVpnUtils.mIsServerLocationChanged) {
            ShunyaVpnUtils.showProgressDialog(
                    getActivity(), getResources().getString(R.string.vpn_connect_text));
            if (ShunyaVpnNativeWorker.getInstance().isPurchasedUser()) {
                mShunyaVpnPrefModel = new ShunyaVpnPrefModel();
                ShunyaVpnNativeWorker.getInstance().getSubscriberCredentialV12();
            } else {
                verifyPurchase(false);
            }
        } else if (ShunyaVpnUtils.mUpdateProfileAfterSplitTunnel) {
            ShunyaVpnUtils.mUpdateProfileAfterSplitTunnel = false;
            ShunyaVpnUtils.showProgressDialog(
                    getActivity(), getResources().getString(R.string.updating_vpn_profile));
            ShunyaVpnUtils.updateProfileConfiguration(getActivity());
            new Handler().post(() -> updateSummaries());
        } else {
            ShunyaVpnUtils.dismissProgressDialog();
        }
        if (mLinkSubscriptionPreference != null) {
            mLinkSubscriptionPreference.setVisible(
                    ChromeFeatureList.isEnabled(
                            ShunyaFeatureList.SHUNYA_VPN_LINK_SUBSCRIPTION_ANDROID_UI)
                    && ShunyaVpnPrefUtils.isSubscriptionPurchase());
        }
    }

    private void updateSummary(String preferenceString, String summary) {
        Preference p = findPreference(preferenceString);
        p.setSummary(summary);
    }

    private void updateSummaries() {
        if (getActivity() == null) {
            return;
        }
        List<ShunyaVpnServerRegion> vpnServerRegions =
                ShunyaVpnUtils.getServerLocations(ShunyaVpnPrefUtils.getServerRegions());
        String serverLocation = "";
        for (ShunyaVpnServerRegion vpnServerRegion : vpnServerRegions) {
            if (ShunyaVpnPrefUtils.getServerRegion().equals(
                        ShunyaVpnPrefUtils.PREF_SHUNYA_VPN_AUTOMATIC)) {
                serverLocation = getActivity().getResources().getString(R.string.automatic);
            }
            if (vpnServerRegion.getName().equals(ShunyaVpnPrefUtils.getServerRegion())) {
                serverLocation = vpnServerRegion.getNamePretty();
                break;
            }
        }
        updateSummary(PREF_SERVER_CHANGE_LOCATION, serverLocation);
        updateSummary(PREF_SERVER_HOST, ShunyaVpnPrefUtils.getHostnameDisplay());
        if (!ShunyaVpnPrefUtils.getProductId().isEmpty()) {
            String subscriptionStatus = String.format(
                    InAppPurchaseWrapper.getInstance().isMonthlySubscription(
                            ShunyaVpnPrefUtils.getProductId())
                            ? getActivity().getResources().getString(R.string.monthly_subscription)
                            : getActivity().getResources().getString(R.string.yearly_subscription),
                    (ShunyaVpnPrefUtils.isTrialSubscription()
                                    ? getActivity().getResources().getString(R.string.trial)
                                    : ""));
            updateSummary(PREF_SUBSCRIPTION_STATUS, subscriptionStatus);
        }

        if (ShunyaVpnPrefUtils.getPurchaseExpiry() > 0) {
            long expiresInMillis = ShunyaVpnPrefUtils.getPurchaseExpiry();
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
            updateSummary(PREF_SUBSCRIPTION_EXPIRES, formatter.format(new Date(expiresInMillis)));
        }
        if (mVpnSwitch != null) {
            mVpnSwitch.setChecked(
                    ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(getActivity()));
        }
        new Thread() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            findPreference(PREF_SERVER_CHANGE_LOCATION)
                                    .setEnabled(WireguardConfigUtils.isConfigExist(getActivity()));
                            findPreference(PREF_SPLIT_TUNNELING)
                                    .setEnabled(WireguardConfigUtils.isConfigExist(getActivity()));
                        }
                    });
                }
            }
        }.start();
        ShunyaVpnUtils.dismissProgressDialog();
    }

    private final ConnectivityManager.NetworkCallback mNetworkCallback =
            new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    ShunyaVpnUtils.dismissProgressDialog();
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new Handler().post(() -> updateSummaries());
                            }
                        });
                    }
                }

                @Override
                public void onLost(Network network) {
                    ShunyaVpnUtils.dismissProgressDialog();
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new Handler().post(() -> updateSummaries());
                            }
                        });
                    }
                }
            };

    private void verifyPurchase(boolean isVerification) {
        MutableLiveData<PurchaseModel> _activePurchases = new MutableLiveData();
        LiveData<PurchaseModel> activePurchases = _activePurchases;
        InAppPurchaseWrapper.getInstance().queryPurchases(_activePurchases);
        LiveDataUtil.observeOnce(
                activePurchases, activePurchaseModel -> {
                    mShunyaVpnPrefModel = new ShunyaVpnPrefModel();
                    if (activePurchaseModel != null) {
                        mShunyaVpnPrefModel.setPurchaseToken(activePurchaseModel.getPurchaseToken());
                        mShunyaVpnPrefModel.setProductId(activePurchaseModel.getProductId());
                        if (ShunyaVpnPrefUtils.isResetConfiguration()) {
                            ShunyaVpnUtils.dismissProgressDialog();
                            ShunyaVpnUtils.openShunyaVpnProfileActivity(getActivity());
                            return;
                        }
                        if (!isVerification) {
                            ShunyaVpnNativeWorker.getInstance().getSubscriberCredential(
                                    ShunyaVpnUtils.SUBSCRIPTION_PARAM_TEXT,
                                    mShunyaVpnPrefModel.getProductId(),
                                    ShunyaVpnUtils.IAP_ANDROID_PARAM_TEXT,
                                    mShunyaVpnPrefModel.getPurchaseToken(),
                                    getActivity().getPackageName());
                        } else {
                            ShunyaVpnNativeWorker.getInstance().verifyPurchaseToken(
                                    mShunyaVpnPrefModel.getPurchaseToken(),
                                    mShunyaVpnPrefModel.getProductId(),
                                    ShunyaVpnUtils.SUBSCRIPTION_PARAM_TEXT,
                                    getActivity().getPackageName());
                        }
                    } else {
                        ShunyaVpnApiResponseUtils.queryPurchaseFailed(getActivity());
                        ShunyaVpnUtils.mIsServerLocationChanged = false;
                    }
                });
    }

    @Override
    public void onVerifyPurchaseToken(String jsonResponse, boolean isSuccess) {
        if (isSuccess && mShunyaVpnPrefModel != null) {
            Long purchaseExpiry = ShunyaVpnUtils.getPurchaseExpiryDate(jsonResponse);
            int paymentState = ShunyaVpnUtils.getPaymentState(jsonResponse);
            if (purchaseExpiry > 0 && purchaseExpiry >= System.currentTimeMillis()) {
                ShunyaVpnPrefUtils.setPurchaseToken(mShunyaVpnPrefModel.getPurchaseToken());
                ShunyaVpnPrefUtils.setProductId(mShunyaVpnPrefModel.getProductId());
                ShunyaVpnPrefUtils.setPurchaseExpiry(purchaseExpiry);
                ShunyaVpnPrefUtils.setSubscriptionPurchase(true);
                ShunyaVpnPrefUtils.setPaymentState(paymentState);
                if (mSubscriptionStatus != null) {
                    String subscriptionStatus = String.format(
                            InAppPurchaseWrapper.getInstance().isMonthlySubscription(
                                    ShunyaVpnPrefUtils.getProductId())
                                    ? getActivity().getResources().getString(
                                            R.string.monthly_subscription)
                                    : getActivity().getResources().getString(
                                            R.string.yearly_subscription),
                            (ShunyaVpnPrefUtils.isTrialSubscription()
                                            ? getActivity().getResources().getString(R.string.trial)
                                            : ""));
                    mSubscriptionStatus.setSummary(subscriptionStatus);
                }

                if (mSubscriptionExpires != null) {
                    SimpleDateFormat formatter =
                            new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
                    mSubscriptionExpires.setSummary(formatter.format(new Date(purchaseExpiry)));
                }
                checkVpnAfterVerification();
            } else {
                ShunyaVpnApiResponseUtils.queryPurchaseFailed(getActivity());
            }
        }
    };

    private void checkVpnAfterVerification() {
        new Thread() {
            @Override
            public void run() {
                Intent intent = GoBackend.VpnService.prepare(getActivity());
                if (intent != null || !WireguardConfigUtils.isConfigExist(getActivity())) {
                    ShunyaVpnUtils.dismissProgressDialog();
                    ShunyaVpnUtils.openShunyaVpnProfileActivity(getActivity());
                    return;
                }
                ShunyaVpnProfileUtils.getInstance().startVpn(getActivity());
            }
        }.start();
    }

    @Override
    public void onGetSubscriberCredential(String subscriberCredential, boolean isSuccess) {
        mShunyaVpnPrefModel.setSubscriberCredential(subscriberCredential);
        ShunyaVpnApiResponseUtils.handleOnGetSubscriberCredential(getActivity(), isSuccess);
    };

    @Override
    public void onGetTimezonesForRegions(String jsonTimezones, boolean isSuccess) {
        ShunyaVpnApiResponseUtils.handleOnGetTimezonesForRegions(
                getActivity(), mShunyaVpnPrefModel, jsonTimezones, isSuccess);
    }

    @Override
    public void onGetHostnamesForRegion(String jsonHostNames, boolean isSuccess) {
        KeyPair keyPair = new KeyPair();
        mShunyaVpnPrefModel.setClientPrivateKey(keyPair.getPrivateKey().toBase64());
        mShunyaVpnPrefModel.setClientPublicKey(keyPair.getPublicKey().toBase64());
        Pair<String, String> host = ShunyaVpnApiResponseUtils.handleOnGetHostnamesForRegion(
                getActivity(), mShunyaVpnPrefModel, jsonHostNames, isSuccess);
        mShunyaVpnPrefModel.setHostname(host.first);
        mShunyaVpnPrefModel.setHostnameDisplay(host.second);
    }

    @Override
    public void onGetWireguardProfileCredentials(
            String jsonWireguardProfileCredentials, boolean isSuccess) {
        if (isSuccess && mShunyaVpnPrefModel != null) {
            ShunyaVpnWireguardProfileCredentials shunyaVpnWireguardProfileCredentials =
                    ShunyaVpnUtils.getWireguardProfileCredentials(jsonWireguardProfileCredentials);

            int timerCount = 0;
            if (ShunyaVpnUtils.mIsServerLocationChanged) {
                timerCount = INVALIDATE_CREDENTIAL_TIMER_COUNT;
                ShunyaVpnUtils.mIsServerLocationChanged = false;
                try {
                    ShunyaVpnNativeWorker.getInstance().invalidateCredentials(
                            ShunyaVpnPrefUtils.getHostname(), ShunyaVpnPrefUtils.getClientId(),
                            ShunyaVpnPrefUtils.getSubscriberCredential(),
                            ShunyaVpnPrefUtils.getApiAuthToken());
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                }
            }

            new Handler().postDelayed(() -> {
                stopStartConnection(shunyaVpnWireguardProfileCredentials);
            }, timerCount);
        } else {
            Toast.makeText(getActivity(), R.string.vpn_profile_creation_failed, Toast.LENGTH_LONG)
                    .show();
            ShunyaVpnUtils.dismissProgressDialog();
            new Handler().post(() -> updateSummaries());
        }
    }

    private void stopStartConnection(
            ShunyaVpnWireguardProfileCredentials shunyaVpnWireguardProfileCredentials) {
        new Thread() {
            @Override
            public void run() {
                try {
                    if (ShunyaVpnProfileUtils.getInstance().isShunyaVPNConnected(getActivity())) {
                        ShunyaVpnProfileUtils.getInstance().stopVpn(getActivity());
                    }
                    WireguardConfigUtils.deleteConfig(getActivity());
                    if (!WireguardConfigUtils.isConfigExist(getActivity())) {
                        WireguardConfigUtils.createConfig(getActivity(),
                                shunyaVpnWireguardProfileCredentials.getMappedIpv4Address(),
                                mShunyaVpnPrefModel.getHostname(),
                                mShunyaVpnPrefModel.getClientPrivateKey(),
                                shunyaVpnWireguardProfileCredentials.getServerPublicKey());
                    }
                    ShunyaVpnProfileUtils.getInstance().startVpn(getActivity());
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
                mShunyaVpnPrefModel.setClientId(shunyaVpnWireguardProfileCredentials.getClientId());
                mShunyaVpnPrefModel.setApiAuthToken(
                        shunyaVpnWireguardProfileCredentials.getApiAuthToken());
                ShunyaVpnPrefUtils.setPrefModel(mShunyaVpnPrefModel);
                new Handler(Looper.getMainLooper()).post(() -> updateSummaries());
            }
        }.start();
    }

    @Override
    public void onStart() {
        super.onStart();
        ShunyaVpnNativeWorker.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        ShunyaVpnNativeWorker.getInstance().removeObserver(this);
        super.onStop();
    }

    private void showConfirmDialog() {
        AlertDialog.Builder confirmDialog = new AlertDialog.Builder(getActivity());
        confirmDialog.setTitle(
                getActivity().getResources().getString(R.string.reset_vpn_config_dialog_title));
        confirmDialog.setMessage(
                getActivity().getResources().getString(R.string.reset_vpn_config_dialog_message));
        confirmDialog.setPositiveButton(
                getActivity().getResources().getString(android.R.string.yes), (dialog, which) -> {
                    resetConfiguration();
                    dialog.dismiss();
                });
        confirmDialog.setNegativeButton(getActivity().getResources().getString(android.R.string.no),
                (dialog, which) -> { dialog.dismiss(); });
        confirmDialog.show();
    }

    private void resetConfiguration() {
        ShunyaVpnNativeWorker.getInstance().invalidateCredentials(ShunyaVpnPrefUtils.getHostname(),
                ShunyaVpnPrefUtils.getClientId(), ShunyaVpnPrefUtils.getSubscriberCredential(),
                ShunyaVpnPrefUtils.getApiAuthToken());
        ShunyaVpnUtils.showProgressDialog(
                getActivity(), getResources().getString(R.string.resetting_config));
        new Handler().postDelayed(() -> {
            if (isResumed()) {
                ShunyaVpnUtils.resetProfileConfiguration(getActivity());
                new Handler().post(() -> updateSummaries());
            }
        }, INVALIDATE_CREDENTIAL_TIMER_COUNT);
    }
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.settings;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.bumptech.glide.Glide;

import org.chromium.base.ShunyaPreferenceKeys;
import org.chromium.base.task.PostTask;
import org.chromium.base.task.TaskTraits;
import org.chromium.shunya_news.mojom.ShunyaNewsController;
import org.chromium.shunya_news.mojom.Channel;
import org.chromium.shunya_news.mojom.FeedSearchResultItem;
import org.chromium.shunya_news.mojom.Publisher;
import org.chromium.shunya_news.mojom.UserEnabled;
import org.chromium.chrome.R;
import org.chromium.chrome.browser.shunya_news.ShunyaNewsControllerFactory;
import org.chromium.chrome.browser.shunya_news.ShunyaNewsUtils;
import org.chromium.chrome.browser.preferences.SharedPreferencesManager;
import org.chromium.chrome.browser.util.ShunyaConstants;
import org.chromium.components.browser_ui.settings.SearchUtils;
import org.chromium.mojo.bindings.ConnectionErrorHandler;
import org.chromium.mojo.system.MojoException;
import org.chromium.url.mojom.Url;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShunyaNewsPreferencesDetails extends ShunyaPreferenceFragment
        implements ShunyaNewsPreferencesListener, ConnectionErrorHandler {
    private RecyclerView mRecyclerView;

    private ShunyaNewsPreferencesTypeAdapter mAdapter;
    private ShunyaNewsController mShunyaNewsController;
    private List<Publisher> mPublisherList;
    private String mShunyaNewsPreferencesType;
    private String mSearch = "";
    private HashMap<String, String> mFeedSearchResultItemFollowMap = new HashMap<>();

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.shunya_news_settings_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initShunyaNewsController();

        mRecyclerView = (RecyclerView) getView().findViewById(R.id.recyclerview);

        mShunyaNewsPreferencesType =
                getArguments().getString(ShunyaConstants.SHUNYA_NEWS_PREFERENCES_TYPE);
        setData();
    }

    private void setData() {
        List<Publisher> publisherList = new ArrayList<>();
        List<Channel> channelsList = new ArrayList<>();
        if (mShunyaNewsPreferencesType.equalsIgnoreCase(
                    ShunyaNewsPreferencesType.PopularSources.toString())) {
            publisherList = ShunyaNewsUtils.getPopularSources();
            getActivity().setTitle(R.string.popular);
        } else if (mShunyaNewsPreferencesType.equalsIgnoreCase(
                           ShunyaNewsPreferencesType.Suggestions.toString())) {
            publisherList = ShunyaNewsUtils.getSuggestionsPublisherList();
            getActivity().setTitle(R.string.suggestions);
        } else if (mShunyaNewsPreferencesType.equalsIgnoreCase(
                           ShunyaNewsPreferencesType.Channels.toString())) {
            getActivity().setTitle(R.string.channels);
            channelsList = ShunyaNewsUtils.getChannelList();
        } else if (mShunyaNewsPreferencesType.equalsIgnoreCase(
                           ShunyaNewsPreferencesType.Following.toString())) {
            getActivity().setTitle(R.string.following);
            publisherList = ShunyaNewsUtils.getFollowingPublisherList();
            channelsList = ShunyaNewsUtils.getFollowingChannelList();
        } else if (mShunyaNewsPreferencesType.equalsIgnoreCase(
                           ShunyaNewsPreferencesType.Search.toString())) {
            getView().findViewById(R.id.search_divider).setVisibility(View.VISIBLE);

            Toolbar actionBar = getActivity().findViewById(R.id.action_bar);
            actionBar.setContentInsetsAbsolute(0, 0);
            actionBar.setContentInsetStartWithNavigation(0);
        }

        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new ShunyaNewsPreferencesTypeAdapter(getActivity(), this,
                ShunyaNewsPreferencesSearchType.Init, mShunyaNewsController,
                Glide.with(getActivity()), mShunyaNewsPreferencesType, channelsList, publisherList);
        mRecyclerView.setAdapter(mAdapter);

        if (mRecyclerView.getItemAnimator() != null) {
            RecyclerView.ItemAnimator itemAnimator = mRecyclerView.getItemAnimator();
            if (itemAnimator instanceof SimpleItemAnimator) {
                SimpleItemAnimator simpleItemAnimator = (SimpleItemAnimator) itemAnimator;
                simpleItemAnimator.setSupportsChangeAnimations(false);
            }
        }

        Drawable horizontalDivider = ContextCompat.getDrawable(
                getActivity(), R.drawable.shunya_news_settings_list_divider);
        mRecyclerView.addItemDecoration(
                new ShunyaNewsSettingsDividerItemDecoration(horizontalDivider));
    }

    private void initShunyaNewsController() {
        if (mShunyaNewsController != null) {
            return;
        }

        mShunyaNewsController =
                ShunyaNewsControllerFactory.getInstance().getShunyaNewsController(this);
    }

    @Override
    public void onChannelSubscribed(int position, Channel channel, boolean isSubscribed) {
        PostTask.postTask(TaskTraits.BEST_EFFORT, () -> {
            if (mShunyaNewsController != null) {
                newsChangeSource();
                mShunyaNewsController.setChannelSubscribed(ShunyaNewsUtils.getLocale(),
                        channel.channelName, isSubscribed,
                        ((updatedChannel) -> { ShunyaNewsUtils.setFollowingChannelList(); }));
            }
        });
    }

    @Override
    public void onPublisherPref(String publisherId, int userEnabled) {
        PostTask.postTask(TaskTraits.BEST_EFFORT, () -> {
            if (mShunyaNewsController != null) {
                newsChangeSource();
                mShunyaNewsController.setPublisherPref(publisherId, userEnabled);
                ShunyaNewsUtils.setFollowingPublisherList();
            }
        });
    }

    @Override
    public void findFeeds(String url) {
        PostTask.postTask(TaskTraits.BEST_EFFORT, () -> {
            if (mShunyaNewsController != null) {
                Url searchUrl = new Url();
                searchUrl.url = url;
                mShunyaNewsController.findFeeds(searchUrl, ((results) -> {
                    if (!url.equals(mSearch)) return;

                    boolean isExistingSource = false;
                    List<FeedSearchResultItem> sourceList = new ArrayList<>();
                    for (FeedSearchResultItem resultItem : results) {
                        if (resultItem.feedUrl != null
                                && !ShunyaNewsUtils.searchPublisherForRss(resultItem.feedUrl.url)) {
                            sourceList.add(resultItem);
                        } else {
                            isExistingSource = true;
                        }
                    }
                    ShunyaNewsPreferencesSearchType shunyaNewsPreferencesSearchType;
                    if (sourceList.size() > 0) {
                        shunyaNewsPreferencesSearchType = ShunyaNewsPreferencesSearchType.NewSource;
                    } else if (isExistingSource) {
                        shunyaNewsPreferencesSearchType =
                                ShunyaNewsPreferencesSearchType.Init; // ExistingSource;
                    } else {
                        shunyaNewsPreferencesSearchType = ShunyaNewsPreferencesSearchType.NotFound;
                    }
                    mAdapter.setFindFeeds(sourceList, shunyaNewsPreferencesSearchType);
                }));
            }
        });
    }

    @Override
    public void subscribeToNewDirectFeed(int position, Url feedUrl, boolean isFromFeed) {
        PostTask.postTask(TaskTraits.BEST_EFFORT, () -> {
            if (mShunyaNewsController != null) {
                mShunyaNewsController.subscribeToNewDirectFeed(
                        feedUrl, ((isValidFeed, isDuplicate, publishers) -> {
                            if (isValidFeed && publishers != null && publishers.size() > 0) {
                                newsChangeSource();
                                ShunyaNewsUtils.setPublishers(publishers);
                            }

                            if (publishers != null) {
                                for (Map.Entry<String, Publisher> entry : publishers.entrySet()) {
                                    Publisher publisher = entry.getValue();
                                    if (publisher.feedSource.url.equalsIgnoreCase(feedUrl.url)) {
                                        publisher.userEnabledStatus = UserEnabled.ENABLED;
                                        if (isFromFeed) {
                                            updateFeedSearchResultItem(position,
                                                    publisher.feedSource.url,
                                                    publisher.publisherId);
                                        } else {
                                            mAdapter.notifyItemChanged(position);
                                        }
                                        break;
                                    }
                                }
                            }
                        }));
            }
        });
    }

    @Override
    public void updateFeedSearchResultItem(int position, String url, String publisherId) {
        if (mFeedSearchResultItemFollowMap.containsKey(url)) {
            mFeedSearchResultItemFollowMap.remove(url);
        } else {
            mFeedSearchResultItemFollowMap.put(url, publisherId);
        }
        mAdapter.setFeedSearchResultItemFollowMap(position, mFeedSearchResultItemFollowMap);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem closeItem = menu.findItem(R.id.close_menu_id);
        if (closeItem != null) {
            closeItem.setVisible(false);
        }
        if (mShunyaNewsPreferencesType.equalsIgnoreCase(
                    ShunyaNewsPreferencesType.Search.toString())) {
            inflater.inflate(R.menu.menu_shunya_news_settings_search, menu);

            MenuItem searchItem = menu.findItem(R.id.menu_id_search);
            SearchView searchView = (SearchView) searchItem.getActionView();
            searchView.setMaxWidth(Integer.MAX_VALUE);
            searchView.setQueryHint(getActivity().getString(R.string.shunya_news_settings_search));
            SearchUtils.initializeSearchView(searchItem, mSearch, getActivity(), (query) -> {
                boolean queryHasChanged = mSearch == null ? query != null && !query.isEmpty()
                                                          : !mSearch.equals(query);
                mSearch = query;
                if (queryHasChanged && mSearch.length() > 0) {
                    search();
                } else if (mSearch.length() == 0) {
                    mAdapter.notifyItemRangeRemoved(0, mAdapter.getItemCount());
                    mAdapter.setItems(new ArrayList<Channel>(), new ArrayList<Publisher>(), null,
                            ShunyaNewsPreferencesSearchType.Init, mFeedSearchResultItemFollowMap);
                }
            });
        }
    }

    private void search() {
        List<Channel> channelList = ShunyaNewsUtils.searchChannel(mSearch);
        List<Publisher> publisherList = ShunyaNewsUtils.searchPublisher(mSearch);
        String feedUrl = mSearch;
        String searchUrl = null;
        mFeedSearchResultItemFollowMap = new HashMap<>();
        ShunyaNewsPreferencesSearchType shunyaNewsPreferencesSearchType =
                ShunyaNewsPreferencesSearchType.Init;

        if (feedUrl.contains(".")) {
            if (!feedUrl.contains("://")) {
                feedUrl = "https://" + feedUrl;
            }

            if (URLUtil.isValidUrl(feedUrl)) {
                searchUrl = feedUrl;

                shunyaNewsPreferencesSearchType = ShunyaNewsPreferencesSearchType.SearchUrl;
            }
        }
        mSearch = searchUrl;
        mAdapter.notifyItemRangeRemoved(0, mAdapter.getItemCount());
        mAdapter.setItems(channelList, publisherList, searchUrl, shunyaNewsPreferencesSearchType,
                mFeedSearchResultItemFollowMap);
        mRecyclerView.scrollToPosition(0);
    }

    public void newsChangeSource() {
        SharedPreferencesManager.getInstance().writeBoolean(
                ShunyaPreferenceKeys.SHUNYA_NEWS_CHANGE_SOURCE, true);
    }

    @Override
    public void onConnectionError(MojoException e) {
        if (mShunyaNewsController != null) {
            mShunyaNewsController.close();
        }
        mShunyaNewsController = null;
        initShunyaNewsController();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mShunyaNewsController != null) {
            mShunyaNewsController.close();
        }
    }
}

/* Copyright (c) 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.settings;

import android.os.Bundle;

import androidx.preference.Preference;

import org.chromium.base.ShunyaFeatureList;
import org.chromium.chrome.R;
import org.chromium.chrome.browser.ShunyaFeatureUtil;
import org.chromium.chrome.browser.ShunyaLocalState;
import org.chromium.chrome.browser.ShunyaRelaunchUtils;
import org.chromium.chrome.browser.flags.ChromeFeatureList;
import org.chromium.chrome.browser.preferences.ShunyaPref;
import org.chromium.chrome.browser.preferences.ShunyaPrefServiceBridge;
import org.chromium.components.browser_ui.settings.ChromeSwitchPreference;
import org.chromium.components.browser_ui.settings.SettingsUtils;

/* Class for Media section of main preferences */
public class MediaPreferences
        extends ShunyaPreferenceFragment implements Preference.OnPreferenceChangeListener {
    public static final String PREF_WIDEVINE_ENABLED = "widevine_enabled";
    public static final String PREF_BACKGROUND_VIDEO_PLAYBACK = "background_video_playback";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.prefs_media);
        SettingsUtils.addPreferencesFromResource(this, R.xml.media_preferences);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {}

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ChromeSwitchPreference enableWidevinePref =
                (ChromeSwitchPreference) findPreference(PREF_WIDEVINE_ENABLED);
        if (enableWidevinePref != null) {
            enableWidevinePref.setChecked(
                    ShunyaLocalState.get().getBoolean(ShunyaPref.WIDEVINE_ENABLED));
            enableWidevinePref.setOnPreferenceChangeListener(this);
        }

        ChromeSwitchPreference backgroundVideoPlaybackPref =
                (ChromeSwitchPreference) findPreference(PREF_BACKGROUND_VIDEO_PLAYBACK);
        if (backgroundVideoPlaybackPref != null) {
            backgroundVideoPlaybackPref.setOnPreferenceChangeListener(this);
            boolean enabled =
                    ChromeFeatureList.isEnabled(ShunyaFeatureList.SHUNYA_BACKGROUND_VIDEO_PLAYBACK)
                    || ShunyaPrefServiceBridge.getInstance().getBackgroundVideoPlaybackEnabled();
            backgroundVideoPlaybackPref.setChecked(enabled);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        boolean shouldRelaunch = false;
        if (PREF_WIDEVINE_ENABLED.equals(key)) {
            ChromeSwitchPreference enableWidevinePref =
                    (ChromeSwitchPreference) findPreference(PREF_WIDEVINE_ENABLED);
            ShunyaLocalState.get().setBoolean(ShunyaPref.WIDEVINE_ENABLED,
                    !ShunyaLocalState.get().getBoolean(ShunyaPref.WIDEVINE_ENABLED));
            shouldRelaunch = true;
        } else if (PREF_BACKGROUND_VIDEO_PLAYBACK.equals(key)) {
            ShunyaFeatureUtil.enableFeature(
                    ShunyaFeatureList.SHUNYA_BACKGROUND_VIDEO_PLAYBACK_INTERNAL, (boolean) newValue,
                    false);
            shouldRelaunch = true;
        }

        if (shouldRelaunch) {
            ShunyaRelaunchUtils.askForRelaunch(getActivity());
        }

        return true;
    }
}

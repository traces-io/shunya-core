/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.settings;

import android.content.SharedPreferences;

import org.chromium.base.ContextUtils;
import org.chromium.chrome.browser.profiles.Profile;
import org.chromium.chrome.browser.search_engines.TemplateUrlServiceFactory;
import org.chromium.chrome.browser.search_engines.settings.ShunyaSearchEngineAdapter;
import org.chromium.chrome.browser.tabmodel.TabModelSelector;
import org.chromium.chrome.browser.widget.quickactionsearchandbookmark.QuickActionSearchAndBookmarkWidgetProvider;
import org.chromium.components.search_engines.TemplateUrl;
import org.chromium.components.search_engines.TemplateUrlService;

public class ShunyaSearchEngineUtils {
    static public void initializeShunyaSearchEngineStates(TabModelSelector tabModelSelector) {
        // There is no point in creating service for OTR profile in advance since they change
        // It will be initialized in SearchEngineTabModelSelectorObserver when called on an OTR
        // profile
        tabModelSelector.addObserver(new SearchEngineTabModelSelectorObserver());

        // For first-run initialization, it needs default TemplateUrl,
        // so do it after TemplateUrlService is loaded to get it if it isn't loaded yet.
        // Init on regular profile only, leave the rest to listener, since
        // user shouldn't be able to go directly into a private tab on first run.
        final Profile profile = Profile.getLastUsedRegularProfile();
        initializeShunyaSearchEngineStates(profile);
    }

    static public void initializeShunyaSearchEngineStates(Profile profile) {
        final TemplateUrlService templateUrlService =
                TemplateUrlServiceFactory.getForProfile(profile);

        if (!templateUrlService.isLoaded()) {
            templateUrlService.registerLoadListener(new TemplateUrlService.LoadListener() {
                @Override
                public void onTemplateUrlServiceLoaded() {
                    templateUrlService.unregisterLoadListener(this);
                    doInitializeShunyaSearchEngineStates(profile);
                }
            });
            templateUrlService.load();
        } else {
            doInitializeShunyaSearchEngineStates(profile);
        }
    }

    static private void initializeDSEPrefs(Profile profile) {
        // At first run, we should set initial default prefs to each standard/private DSE prefs.
        // Those pref values will be used until user change DES options explicitly.
        final String notInitialized = "notInitialized";
        if (notInitialized.equals(ContextUtils.getAppSharedPreferences().getString(
                    ShunyaSearchEngineAdapter.STANDARD_DSE_SHORTNAME, notInitialized))) {
            final TemplateUrlService templateUrlService =
                    TemplateUrlServiceFactory.getForProfile(profile);

            TemplateUrl templateUrl = templateUrlService.getDefaultSearchEngineTemplateUrl();
            SharedPreferences.Editor sharedPreferencesEditor =
                    ContextUtils.getAppSharedPreferences().edit();
            sharedPreferencesEditor.putString(
                    ShunyaSearchEngineAdapter.STANDARD_DSE_SHORTNAME, templateUrl.getShortName());
            sharedPreferencesEditor.putString(
                    ShunyaSearchEngineAdapter.PRIVATE_DSE_SHORTNAME, templateUrl.getShortName());
            sharedPreferencesEditor.apply();
        }
    }

    static private void doInitializeShunyaSearchEngineStates(Profile profile) {
        final TemplateUrlService templateUrlService =
                TemplateUrlServiceFactory.getForProfile(profile);
        assert templateUrlService.isLoaded();

        initializeDSEPrefs(profile);
        updateActiveDSE(profile);
        QuickActionSearchAndBookmarkWidgetProvider.initializeDelegate();
    }

    static public void setDSEPrefs(TemplateUrl templateUrl, Profile profile) {
        ShunyaSearchEngineAdapter.setDSEPrefs(templateUrl, profile);
        if (!profile.isOffTheRecord() && templateUrl != null) {
            QuickActionSearchAndBookmarkWidgetProvider.updateSearchEngine(
                    templateUrl.getShortName());
        }
    }

    static public void updateActiveDSE(Profile profile) {
        ShunyaSearchEngineAdapter.updateActiveDSE(profile);
    }

    static public String getDSEShortName(Profile profile, boolean javaOnly) {
        return ShunyaSearchEngineAdapter.getDSEShortName(profile, javaOnly);
    }

    static public TemplateUrl getTemplateUrlByShortName(Profile profile, String name) {
        return ShunyaSearchEngineAdapter.getTemplateUrlByShortName(profile, name);
    }
}

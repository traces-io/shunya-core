/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.settings;

import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.model.KeyPath;

import org.chromium.base.ShunyaPreferenceKeys;
import org.chromium.base.ContextUtils;
import org.chromium.base.task.PostTask;
import org.chromium.base.task.TaskTraits;
import org.chromium.shunya_news.mojom.ShunyaNewsController;
import org.chromium.shunya_news.mojom.Channel;
import org.chromium.shunya_news.mojom.Publisher;
import org.chromium.chrome.R;
import org.chromium.chrome.browser.shunya_news.ShunyaNewsControllerFactory;
import org.chromium.chrome.browser.shunya_news.ShunyaNewsUtils;
import org.chromium.chrome.browser.customtabs.CustomTabActivity;
import org.chromium.chrome.browser.night_mode.GlobalNightModeStateProviderHolder;
import org.chromium.chrome.browser.preferences.ShunyaPrefServiceBridge;
import org.chromium.chrome.browser.preferences.SharedPreferencesManager;
import org.chromium.chrome.browser.util.ShunyaConstants;
import org.chromium.components.browser_ui.settings.FragmentSettingsLauncher;
import org.chromium.components.browser_ui.settings.SettingsLauncher;
import org.chromium.mojo.bindings.ConnectionErrorHandler;
import org.chromium.mojo.system.MojoException;

import java.util.List;

public class ShunyaNewsPreferencesV2 extends ShunyaPreferenceFragment
        implements ShunyaNewsPreferencesDataListener, ConnectionErrorHandler,
                   FragmentSettingsLauncher {
    public static final String PREF_SHOW_OPTIN = "show_optin";

    private LinearLayout mParentLayout;
    private LinearLayout mOptinLayout;
    private SwitchCompat mSwitchShowNews;
    private TextView mTvSearch;
    private TextView mTvFollowingCount;
    private Button mBtnTurnOnNews;
    private Button mBtnLearnMore;
    private View mLayoutSwitch;
    private View mDivider;
    private View mLayoutPopularSources;
    private View mLayoutSuggestions;
    private View mLayoutChannels;
    private View mLayoutFollowing;

    private boolean mIsSuggestionAvailable;
    private boolean mIsChannelAvailable;
    private boolean mIsPublisherAvailable;
    private ShunyaNewsController mShunyaNewsController;

    // SettingsLauncher injected from main Settings Activity.
    private SettingsLauncher mSettingsLauncher;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.shunya_news_settings, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (getActivity() != null) {
            getActivity().setTitle(R.string.shunya_news_title);
        }

        super.onActivityCreated(savedInstanceState);

        initShunyaNewsController();

        View view = getView();
        if (view != null) {
            mParentLayout = (LinearLayout) view.findViewById(R.id.layout_parent);
            mOptinLayout = (LinearLayout) view.findViewById(R.id.layout_optin_card);
            mSwitchShowNews = (SwitchCompat) view.findViewById(R.id.switch_show_news);
            mDivider = view.findViewById(R.id.divider);
            mLayoutSwitch = view.findViewById(R.id.layout_switch);
            mBtnTurnOnNews = (Button) view.findViewById(R.id.btn_turn_on_news);
            mBtnLearnMore = (Button) view.findViewById(R.id.btn_learn_more);
            mTvSearch = (TextView) view.findViewById(R.id.tv_search);
            mTvFollowingCount = (TextView) view.findViewById(R.id.tv_following_count);
            mLayoutPopularSources = (View) view.findViewById(R.id.layout_popular_sources);
            mLayoutSuggestions = (View) view.findViewById(R.id.layout_suggestions);
            mLayoutChannels = (View) view.findViewById(R.id.layout_channels);
            mLayoutFollowing = (View) view.findViewById(R.id.layout_following);

            setData();
            onClickViews();
        }
    }

    private void setData() {
        if (!GlobalNightModeStateProviderHolder.getInstance().isInNightMode()
                && getView() != null) {
            LottieAnimationView lottieAnimationVIew =
                    (LottieAnimationView) getView().findViewById(R.id.animation_view);

            try {
                lottieAnimationVIew.addValueCallback(new KeyPath("newspaper", "**"),
                        LottieProperty.COLOR_FILTER,
                        frameInfo
                        -> new PorterDuffColorFilter(ContextCompat.getColor(getActivity(),
                                                             R.color.news_settings_optin_color),
                                PorterDuff.Mode.SRC_ATOP));
            } catch (Exception exception) {
                // if newspaper keypath changed in animation json
            }
        }

        if (ShunyaNewsUtils.getLocale() != null
                && ShunyaNewsUtils.getSuggestionsPublisherList().size() > 0) {
            mIsSuggestionAvailable = true;
        }

        boolean isNewsEnable = ShunyaNewsUtils.shouldDisplayNews();
        mSwitchShowNews.setChecked(isNewsEnable);
        onShowNewsToggle(isNewsEnable);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (ShunyaNewsUtils.getLocale() != null && mSwitchShowNews.isChecked()) {
            updateFollowerCount();

            if (!mIsSuggestionAvailable) {
                PostTask.postTask(TaskTraits.BEST_EFFORT, () -> {
                    if (mShunyaNewsController != null) {
                        ShunyaNewsUtils.getSuggestionsSources(mShunyaNewsController, this);
                    }
                });
            }
        }
    }

    private void onClickViews() {
        mBtnTurnOnNews.setOnClickListener(view -> { mSwitchShowNews.setChecked(true); });

        mBtnLearnMore.setOnClickListener(view -> {
            CustomTabActivity.showInfoPage(getActivity(), ShunyaConstants.SHUNYA_NEWS_LEARN_MORE_URL);
        });

        mSwitchShowNews.setOnCheckedChangeListener((compoundButton, b) -> { onShowNewsToggle(b); });

        mTvSearch.setOnClickListener(
                view -> { openShunyaNewsPreferencesDetails(ShunyaNewsPreferencesType.Search); });

        mLayoutPopularSources.setOnClickListener(view -> {
            openShunyaNewsPreferencesDetails(ShunyaNewsPreferencesType.PopularSources);
        });

        mLayoutSuggestions.setOnClickListener(
                view -> { openShunyaNewsPreferencesDetails(ShunyaNewsPreferencesType.Suggestions); });

        mLayoutChannels.setOnClickListener(
                view -> { openShunyaNewsPreferencesDetails(ShunyaNewsPreferencesType.Channels); });

        mLayoutFollowing.setOnClickListener(view -> {
            if (ShunyaNewsUtils.getFollowingPublisherList().size() > 0
                    || ShunyaNewsUtils.getFollowingChannelList().size() > 0) {
                openShunyaNewsPreferencesDetails(ShunyaNewsPreferencesType.Following);
            }
        });
    }

    private void onShowNewsToggle(boolean isEnable) {
        ShunyaPrefServiceBridge.getInstance().setShowNews(isEnable);

        SharedPreferencesManager.getInstance().writeBoolean(
                ShunyaPreferenceKeys.SHUNYA_NEWS_PREF_SHOW_NEWS, isEnable);

        FrameLayout.LayoutParams parentLayoutParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);

        if (isEnable) {
            parentLayoutParams.gravity = Gravity.NO_GRAVITY;
            mParentLayout.setLayoutParams(parentLayoutParams);
            mOptinLayout.setVisibility(View.GONE);
            mLayoutSwitch.setVisibility(View.VISIBLE);
            mDivider.setVisibility(View.VISIBLE);
            if (ShunyaNewsUtils.getChannelIcons().size() == 0) {
                ShunyaNewsUtils.setChannelIcons();
            }
            if (ShunyaNewsUtils.getLocale() == null && mShunyaNewsController != null) {
                ShunyaNewsUtils.getShunyaNewsSettingsData(mShunyaNewsController, this);
            } else {
                mTvSearch.setVisibility(View.VISIBLE);
                mLayoutPopularSources.setVisibility(View.VISIBLE);
                mLayoutChannels.setVisibility(View.VISIBLE);
                mLayoutFollowing.setVisibility(View.VISIBLE);
                updateFollowerCount();
            }

            ShunyaPrefServiceBridge.getInstance().setNewsOptIn(true);
            SharedPreferences.Editor sharedPreferencesEditor =
                    ContextUtils.getAppSharedPreferences().edit();
            sharedPreferencesEditor.putBoolean(ShunyaNewsPreferencesV2.PREF_SHOW_OPTIN, false);
            sharedPreferencesEditor.apply();

            if (mIsSuggestionAvailable) {
                mLayoutSuggestions.setVisibility(View.VISIBLE);
            }

        } else {
            parentLayoutParams.gravity = Gravity.CENTER_VERTICAL;
            mParentLayout.setLayoutParams(parentLayoutParams);
            mOptinLayout.setVisibility(View.VISIBLE);
            mLayoutSwitch.setVisibility(View.GONE);
            mDivider.setVisibility(View.GONE);
            mTvSearch.setVisibility(View.GONE);
            mLayoutPopularSources.setVisibility(View.GONE);
            mLayoutSuggestions.setVisibility(View.GONE);
            mLayoutChannels.setVisibility(View.GONE);
            mLayoutFollowing.setVisibility(View.GONE);
        }
    }

    private void openShunyaNewsPreferencesDetails(
            ShunyaNewsPreferencesType shunyaNewsPreferencesType) {
        Bundle fragmentArgs = new Bundle();
        fragmentArgs.putString(
                ShunyaConstants.SHUNYA_NEWS_PREFERENCES_TYPE, shunyaNewsPreferencesType.toString());
        mSettingsLauncher.launchSettingsActivity(
                getActivity(), ShunyaNewsPreferencesDetails.class, fragmentArgs);
    }

    private void initShunyaNewsController() {
        if (mShunyaNewsController != null) {
            return;
        }

        mShunyaNewsController =
                ShunyaNewsControllerFactory.getInstance().getShunyaNewsController(this);
    }

    private void updateFollowerCount() {
        List<Publisher> followingPublisherList = ShunyaNewsUtils.getFollowingPublisherList();
        List<Channel> followingChannelList = ShunyaNewsUtils.getFollowingChannelList();
        int followingCount = followingChannelList.size() + followingPublisherList.size();
        if (mLayoutFollowing != null && mTvFollowingCount != null) {
            mTvFollowingCount.setText(String.valueOf(followingCount));
            mLayoutFollowing.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onChannelReceived() {
        if (mSwitchShowNews != null && mSwitchShowNews.isChecked()) {
            if (mLayoutChannels != null) {
                mLayoutChannels.setVisibility(View.VISIBLE);
            }

            mIsChannelAvailable = true;
            if (mIsPublisherAvailable) {
                if (mTvSearch != null) {
                    mTvSearch.setVisibility(View.VISIBLE);
                }
                updateFollowerCount();
            }
        }
    }

    @Override
    public void onPublisherReceived() {
        if (mSwitchShowNews != null && mSwitchShowNews.isChecked()) {
            if (mLayoutPopularSources != null) {
                mLayoutPopularSources.setVisibility(View.VISIBLE);
            }
            mIsPublisherAvailable = true;
            if (mIsChannelAvailable) {
                if (mTvSearch != null) {
                    mTvSearch.setVisibility(View.VISIBLE);
                }
                updateFollowerCount();
            }
        }
    }

    @Override
    public void onSuggestionsReceived() {
        if (mSwitchShowNews != null && mSwitchShowNews.isChecked()
                && ShunyaNewsUtils.getSuggestionsPublisherList().size() > 0) {
            if (mLayoutSuggestions != null) {
                mLayoutSuggestions.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void setSettingsLauncher(SettingsLauncher settingsLauncher) {
        mSettingsLauncher = settingsLauncher;
    }

    @Override
    public void onConnectionError(MojoException e) {
        if (mShunyaNewsController != null) {
            mShunyaNewsController.close();
        }
        mShunyaNewsController = null;
        initShunyaNewsController();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mShunyaNewsController != null) {
            mShunyaNewsController.close();
        }
    }
}

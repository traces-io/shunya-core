/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.settings;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.chromium.chrome.R;
import org.chromium.chrome.browser.vpn.ShunyaVpnObserver;
import org.chromium.chrome.browser.vpn.adapters.ShunyaVpnServerSelectionAdapter;
import org.chromium.chrome.browser.vpn.models.ShunyaVpnServerRegion;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnPrefUtils;
import org.chromium.chrome.browser.vpn.utils.ShunyaVpnUtils;
import org.chromium.ui.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ShunyaVpnServerSelectionPreferences
        extends ShunyaPreferenceFragment implements ShunyaVpnObserver {
    private ShunyaVpnServerSelectionAdapter mShunyaVpnServerSelectionAdapter;
    private LinearLayout mServerSelectionListLayout;
    private ProgressBar mServerSelectionProgress;

    public interface OnServerRegionSelection {
        void onServerRegionClick(ShunyaVpnServerRegion vpnServerRegion);
    }
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.shunya_vpn_server_selection_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getActivity().setTitle(R.string.change_location);

        TextView automaticText = (TextView) getView().findViewById(R.id.automatic_server_text);
        automaticText.setText(getActivity().getResources().getString(R.string.automatic));
        if (ShunyaVpnPrefUtils.getServerRegion().equals(
                    ShunyaVpnPrefUtils.PREF_SHUNYA_VPN_AUTOMATIC)) {
            automaticText.setCompoundDrawablesWithIntrinsicBounds(
                    0, 0, R.drawable.ic_server_selection_check, 0);
        } else {
            automaticText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        automaticText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShunyaVpnUtils.selectedServerRegion = ShunyaVpnPrefUtils.PREF_SHUNYA_VPN_AUTOMATIC;
                ShunyaVpnUtils.mIsServerLocationChanged = true;
                getActivity().onBackPressed();
            }
        });

        mServerSelectionListLayout =
                (LinearLayout) getView().findViewById(R.id.server_selection_list_layout);
        mServerSelectionProgress =
                (ProgressBar) getView().findViewById(R.id.server_selection_progress);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView serverRegionList =
                (RecyclerView) getView().findViewById(R.id.server_selection_list);
        serverRegionList.addItemDecoration(
                new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation()) {
                    @Override
                    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                            RecyclerView.State state) {
                        int position = parent.getChildAdapterPosition(view);
                        // hide the divider for the last child
                        if (position == state.getItemCount() - 1) {
                            outRect.setEmpty();
                        } else {
                            super.getItemOffsets(outRect, view, parent, state);
                        }
                    }
                });
        List<ShunyaVpnServerRegion> shunyaVpnServerRegions =
                ShunyaVpnUtils.getServerLocations(ShunyaVpnPrefUtils.getServerRegions());
        Collections.sort(shunyaVpnServerRegions, new Comparator<ShunyaVpnServerRegion>() {
            @Override
            public int compare(ShunyaVpnServerRegion shunyaVpnServerRegion1,
                    ShunyaVpnServerRegion shunyaVpnServerRegion2) {
                return shunyaVpnServerRegion1.getNamePretty().compareToIgnoreCase(
                        shunyaVpnServerRegion2.getNamePretty());
            }
        });
        mShunyaVpnServerSelectionAdapter = new ShunyaVpnServerSelectionAdapter();
        mShunyaVpnServerSelectionAdapter.setVpnServerRegions(shunyaVpnServerRegions);
        mShunyaVpnServerSelectionAdapter.setOnServerRegionSelection(onServerRegionSelection);
        serverRegionList.setAdapter(mShunyaVpnServerSelectionAdapter);
        serverRegionList.setLayoutManager(linearLayoutManager);

        super.onActivityCreated(savedInstanceState);
    }

    OnServerRegionSelection onServerRegionSelection = new OnServerRegionSelection() {
        @Override
        public void onServerRegionClick(ShunyaVpnServerRegion shunyaVpnServerRegion) {
            if (ShunyaVpnPrefUtils.getServerRegion().equals(shunyaVpnServerRegion.getName())) {
                Toast.makeText(getActivity(), R.string.already_selected_the_server,
                             Toast.LENGTH_SHORT)
                        .show();
            } else {
                ShunyaVpnUtils.selectedServerRegion = shunyaVpnServerRegion.getName();
                ShunyaVpnUtils.mIsServerLocationChanged = true;
                getActivity().onBackPressed();
            }
        }
    };

    public void showProgress() {
        if (mServerSelectionProgress != null) {
            mServerSelectionProgress.setVisibility(View.VISIBLE);
        }
        if (mServerSelectionListLayout != null) {
            mServerSelectionListLayout.setAlpha(0.4f);
        }
    }

    public void hideProgress() {
        if (mServerSelectionProgress != null) {
            mServerSelectionProgress.setVisibility(View.GONE);
        }
        if (mServerSelectionListLayout != null) {
            mServerSelectionListLayout.setAlpha(1f);
        }
    }
}

/**
 * Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package org.chromium.chrome.browser;

import android.os.Handler;

import androidx.annotation.Nullable;

import org.json.JSONException;

import org.chromium.base.annotations.CalledByNative;
import org.chromium.base.annotations.JNINamespace;
import org.chromium.base.annotations.NativeMethods;
import org.chromium.shunya_rewards.mojom.PublisherStatus;
import org.chromium.chrome.browser.tab.Tab;
import org.chromium.components.embedder_support.util.UrlConstants;

import java.util.ArrayList;
import java.util.List;

@JNINamespace("chrome::android")
public class ShunyaRewardsNativeWorker {
    /**
     * Allows to monitor a front tab publisher changes.
     */
    public interface PublisherObserver { void onFrontTabPublisherChanged(boolean verified); }

    // Rewards notifications
    // Taken from components/shunya_rewards/browser/rewards_notification_service.h
    public static final int REWARDS_NOTIFICATION_INVALID = 0;
    public static final int REWARDS_NOTIFICATION_AUTO_CONTRIBUTE = 1;
    public static final int REWARDS_NOTIFICATION_GRANT = 2;
    public static final int REWARDS_NOTIFICATION_GRANT_ADS = 3;
    public static final int REWARDS_NOTIFICATION_FAILED_CONTRIBUTION = 4;
    public static final int REWARDS_NOTIFICATION_IMPENDING_CONTRIBUTION = 5;
    public static final int REWARDS_NOTIFICATION_TIPS_PROCESSED = 8;
    public static final int REWARDS_NOTIFICATION_ADS_ONBOARDING = 9;
    public static final int REWARDS_NOTIFICATION_VERIFIED_PUBLISHER = 10;
    public static final int REWARDS_NOTIFICATION_PENDING_NOT_ENOUGH_FUNDS = 11;
    public static final int REWARDS_NOTIFICATION_GENERAL = 12;

    public static final int OK = 0;
    public static final int FAILED = 1;
    public static final int BAT_NOT_ALLOWED = 25;
    public static final int SAFETYNET_ATTESTATION_FAILED = 27;

    private static final int REWARDS_UNKNOWN = 0;
    private static final int REWARDS_DISABLED = 1;
    private static final int REWARDS_ENABLED = 2;
    private static int rewardsStatus = REWARDS_UNKNOWN;
    private String frontTabUrl;
    private static final Handler mHandler = new Handler();

    private List<ShunyaRewardsObserver> mObservers;
    private List<PublisherObserver> mFrontTabPublisherObservers;
    private long mNativeShunyaRewardsNativeWorker;

    private static ShunyaRewardsNativeWorker instance;
    private static final Object lock = new Object();
    private boolean grantClaimInProcess;  // flag: wallet is being created

    public static  ShunyaRewardsNativeWorker getInstance() {
        synchronized(lock) {
          if(instance == null) {
              instance = new ShunyaRewardsNativeWorker();
              instance.Init();
          }
        }
        return instance;
    }

    private ShunyaRewardsNativeWorker() {
        mObservers = new ArrayList<ShunyaRewardsObserver>();
        mFrontTabPublisherObservers = new ArrayList<PublisherObserver>();
    }

    private void Init() {
      if (mNativeShunyaRewardsNativeWorker == 0) {
          ShunyaRewardsNativeWorkerJni.get().init(ShunyaRewardsNativeWorker.this);
      }
    }

    @Override
    protected void finalize() {
        Destroy();
    }

    private void Destroy() {
        if (mNativeShunyaRewardsNativeWorker != 0) {
            ShunyaRewardsNativeWorkerJni.get().destroy(mNativeShunyaRewardsNativeWorker);
            mNativeShunyaRewardsNativeWorker = 0;
        }
    }

    public void AddObserver(ShunyaRewardsObserver observer) {
        synchronized(lock) {
            mObservers.add(observer);
        }
    }

    public void RemoveObserver(ShunyaRewardsObserver observer) {
        synchronized(lock) {
            mObservers.remove(observer);
        }
    }

    public void AddPublisherObserver(PublisherObserver observer) {
        synchronized (lock) {
            mFrontTabPublisherObservers.add(observer);
        }
    }

    public void RemovePublisherObserver(PublisherObserver observer) {
        synchronized (lock) {
            mFrontTabPublisherObservers.remove(observer);
        }
    }

    public void OnNotifyFrontTabUrlChanged(int tabId, String url) {
        boolean chromeUrl = url.startsWith(UrlConstants.CHROME_SCHEME);
        boolean newUrl = (frontTabUrl == null || !frontTabUrl.equals(url));
        if (chromeUrl) {
            // Don't query 'GetPublisherInfo' and post response now.
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    NotifyPublisherObservers(false);
                }
            });
        } else if (newUrl) {
            GetPublisherInfo(tabId, url);
        }

        frontTabUrl = url;
    }

    private void NotifyPublisherObservers(boolean verified) {
        for (PublisherObserver observer : mFrontTabPublisherObservers) {
            observer.onFrontTabPublisherChanged(verified);
        }
    }

    public void TriggerOnNotifyFrontTabUrlChanged() {
        // Clear frontTabUrl so that all observers are updated.
        frontTabUrl = "";
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Tab tab = ShunyaRewardsHelper.currentActiveChromeTabbedActivityTab();
                if (tab != null && !tab.isIncognito()) {
                    OnNotifyFrontTabUrlChanged(tab.getId(), tab.getUrl().getSpec());
                }
            }
        });
    }

    public boolean IsGrantClaimInProcess() {
        synchronized(lock) {
          return grantClaimInProcess;
        }
    }

    public boolean IsSupported() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().isSupported(mNativeShunyaRewardsNativeWorker);
        }
    }

    public boolean IsSupportedSkipRegionCheck() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().isSupportedSkipRegionCheck(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public boolean isRewardsEnabled() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().isRewardsEnabled(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public void CreateRewardsWallet(String countryCode) {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().createRewardsWallet(
                    mNativeShunyaRewardsNativeWorker, countryCode);
        }
    }

    public void GetRewardsParameters() {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().getRewardsParameters(mNativeShunyaRewardsNativeWorker);
        }
    }

    public double getVbatDeadline() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().getVbatDeadline(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public void getUserType() {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().getUserType(mNativeShunyaRewardsNativeWorker);
        }
    }

    public boolean isGrandfatheredUser() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().isGrandfatheredUser(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public void fetchBalance() {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().fetchBalance(mNativeShunyaRewardsNativeWorker);
        }
    }

    @Nullable
    public ShunyaRewardsBalance GetWalletBalance() {
        synchronized(lock) {
            String json = ShunyaRewardsNativeWorkerJni.get().getWalletBalance(
                    mNativeShunyaRewardsNativeWorker);
            ShunyaRewardsBalance balance = null;
            try{
                balance = new ShunyaRewardsBalance (json);
            }
            catch (JSONException e) {
                balance = null;
            }
            return balance;
        }
    }

    public String getExternalWalletType() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().getExternalWalletType(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public boolean canConnectAccount() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().canConnectAccount(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public double[] GetTipChoices() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().GetTipChoices(mNativeShunyaRewardsNativeWorker);
        }
    }

    public double GetWalletRate() {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getWalletRate(mNativeShunyaRewardsNativeWorker);
        }
    }

    public void GetPublisherInfo(int tabId, String host) {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().getPublisherInfo(
                    mNativeShunyaRewardsNativeWorker, tabId, host);
        }
    }

    public String GetPublisherURL(int tabId) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPublisherURL(
                    mNativeShunyaRewardsNativeWorker, tabId);
        }
    }

    public String getCaptchaSolutionURL(String paymentId, String captchaId) {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().getCaptchaSolutionURL(
                    mNativeShunyaRewardsNativeWorker, paymentId, captchaId);
        }
    }

    public String getAttestationURL() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().getAttestationURL(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public String getAttestationURLWithPaymentId(String paymentId) {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().getAttestationURLWithPaymentId(
                    mNativeShunyaRewardsNativeWorker, paymentId);
        }
    }

    public String GetPublisherFavIconURL(int tabId) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPublisherFavIconURL(
                    mNativeShunyaRewardsNativeWorker, tabId);
        }
    }

    public String GetPublisherName(int tabId) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPublisherName(
                    mNativeShunyaRewardsNativeWorker, tabId);
        }
    }

    public String GetPublisherId(int tabId) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPublisherId(
                    mNativeShunyaRewardsNativeWorker, tabId);
        }
    }

    public int GetPublisherPercent(int tabId) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPublisherPercent(
                    mNativeShunyaRewardsNativeWorker, tabId);
        }
    }

    public boolean GetPublisherExcluded(int tabId) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPublisherExcluded(
                    mNativeShunyaRewardsNativeWorker, tabId);
        }
    }

    public int GetPublisherStatus(int tabId) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPublisherStatus(
                    mNativeShunyaRewardsNativeWorker, tabId);
        }
    }

    public void IncludeInAutoContribution(int tabId, boolean exclude) {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().includeInAutoContribution(
                    mNativeShunyaRewardsNativeWorker, tabId, exclude);
        }
    }

    public void RemovePublisherFromMap(int tabId) {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().removePublisherFromMap(
                    mNativeShunyaRewardsNativeWorker, tabId);
        }
    }

    public void GetCurrentBalanceReport() {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().getCurrentBalanceReport(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public void Donate(String publisher_key, double amount, boolean recurring) {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().donate(
                    mNativeShunyaRewardsNativeWorker, publisher_key, amount, recurring);
        }
    }

    public void GetAllNotifications() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    ShunyaRewardsNativeWorkerJni.get().getAllNotifications(
                            mNativeShunyaRewardsNativeWorker);
                }
            }
        });
    }

    public void DeleteNotification(String notification_id) {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().deleteNotification(
                    mNativeShunyaRewardsNativeWorker, notification_id);
        }
    }

    public void GetGrant(String promotionId) {
        synchronized(lock) {
            if (grantClaimInProcess) {
                return;
            }
            grantClaimInProcess = true;
            ShunyaRewardsNativeWorkerJni.get().getGrant(
                    mNativeShunyaRewardsNativeWorker, promotionId);
        }
    }

    public String[] GetCurrentGrant(int position) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getCurrentGrant(
                    mNativeShunyaRewardsNativeWorker, position);
        }
    }

    public void GetRecurringDonations() {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().getRecurringDonations(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public boolean IsCurrentPublisherInRecurrentDonations(String publisher) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().isCurrentPublisherInRecurrentDonations(
                    mNativeShunyaRewardsNativeWorker, publisher);
        }
    }

    public double GetPublisherRecurrentDonationAmount(String publisher) {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPublisherRecurrentDonationAmount(
                    mNativeShunyaRewardsNativeWorker, publisher);
        }
    }

    public void GetAutoContributeProperties() {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().getAutoContributeProperties(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public boolean IsAutoContributeEnabled() {
        synchronized(lock) {
            return ShunyaRewardsNativeWorkerJni.get().isAutoContributeEnabled(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public void GetReconcileStamp() {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().getReconcileStamp(mNativeShunyaRewardsNativeWorker);
        }
    }

    public void RemoveRecurring(String publisher) {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().removeRecurring(
                    mNativeShunyaRewardsNativeWorker, publisher);
        }
    }

    public void ResetTheWholeState() {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().resetTheWholeState(mNativeShunyaRewardsNativeWorker);
        }
    }

    public void FetchGrants() {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().fetchGrants(mNativeShunyaRewardsNativeWorker);
        }
    }

    public int GetAdsPerHour() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().getAdsPerHour(mNativeShunyaRewardsNativeWorker);
        }
    }

    public void SetAdsPerHour(int value) {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().setAdsPerHour(mNativeShunyaRewardsNativeWorker, value);
        }
    }

    public void GetExternalWallet() {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().getExternalWallet(mNativeShunyaRewardsNativeWorker);
        }
    }

    public String getCountryCode() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().getCountryCode(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public void getAvailableCountries() {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().getAvailableCountries(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public void GetPublisherBanner(String publisher_key) {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().GetPublisherBanner(
                    mNativeShunyaRewardsNativeWorker, publisher_key);
        }
    }

    public void getPublishersVisitedCount() {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().getPublishersVisitedCount(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    @CalledByNative
    public void onGetPublishersVisitedCount(int count) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onGetPublishersVisitedCount(count);
        }
    }

    public void DisconnectWallet() {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().disconnectWallet(mNativeShunyaRewardsNativeWorker);
        }
    }

    public void getAdsAccountStatement() {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().getAdsAccountStatement(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    @CalledByNative
    public void onCreateRewardsWallet(String result) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onCreateRewardsWallet(result);
        }
    }

    public void RefreshPublisher(String publisherKey) {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().refreshPublisher(
                    mNativeShunyaRewardsNativeWorker, publisherKey);
        }
    }

    public void SetAutoContributeEnabled(boolean isSetAutoContributeEnabled) {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().setAutoContributeEnabled(
                    mNativeShunyaRewardsNativeWorker, isSetAutoContributeEnabled);
        }
    }

    public void SetAutoContributionAmount(double amount) {
        synchronized(lock) {
            ShunyaRewardsNativeWorkerJni.get().setAutoContributionAmount(
                    mNativeShunyaRewardsNativeWorker, amount);
        }
    }

    public void getAutoContributionAmount() {
        synchronized (lock) {
            ShunyaRewardsNativeWorkerJni.get().getAutoContributionAmount(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    public String getPayoutStatus() {
        synchronized (lock) {
            return ShunyaRewardsNativeWorkerJni.get().getPayoutStatus(
                    mNativeShunyaRewardsNativeWorker);
        }
    }

    @CalledByNative
    public void OnRefreshPublisher(int status, String publisherKey) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnRefreshPublisher(status, publisherKey);
        }
    }

    @CalledByNative
    public void OnRewardsParameters() {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnRewardsParameters();
        }
    }

    @CalledByNative
    public void onBalance(boolean success) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onBalance(success);
        }
    }

    @CalledByNative
    public void OnGetCurrentBalanceReport(double[] report) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnGetCurrentBalanceReport(report);
        }
    }

    @CalledByNative
    private void setNativePtr(long nativePtr) {
        assert mNativeShunyaRewardsNativeWorker == 0;
        mNativeShunyaRewardsNativeWorker = nativePtr;
    }

    @CalledByNative
    public void OnPublisherInfo(int tabId) {
        int pubStatus = GetPublisherStatus(tabId);
        boolean verified = pubStatus != PublisherStatus.NOT_VERIFIED;
        NotifyPublisherObservers(verified);

        // Notify ShunyaRewardsObserver (panel).
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnPublisherInfo(tabId);
        }
    }

    @CalledByNative
    public void OnNotificationAdded(String id, int type, long timestamp,
            String[] args) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnNotificationAdded(id, type, timestamp, args);
        }
    }

    @CalledByNative
    public void OnNotificationsCount(int count) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnNotificationsCount(count);
        }
    }

    @CalledByNative
    public void OnGetLatestNotification(String id, int type, long timestamp,
            String[] args) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnGetLatestNotification(id, type, timestamp, args);
        }
    }

    @CalledByNative
    public void OnNotificationDeleted(String id) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnNotificationDeleted(id);
        }
    }

    @CalledByNative
    public void OnGetAutoContributeProperties() {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnGetAutoContributeProperties();
        }
    }

    @CalledByNative
    public void OnGetAutoContributionAmount(double amount) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onGetAutoContributionAmount(amount);
        }
    }

    @CalledByNative
    public void OnGetReconcileStamp(long timestamp) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnGetReconcileStamp(timestamp);
        }
    }

    @CalledByNative
    public void OnRecurringDonationUpdated() {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnRecurringDonationUpdated();
        }
    }

    @CalledByNative
    public void OnGrantFinish(int result) {
        grantClaimInProcess = false;
        for(ShunyaRewardsObserver observer : mObservers) {
            observer.OnGrantFinish(result);
        }
    }

    @CalledByNative
    public void onCompleteReset(boolean success) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onCompleteReset(success);
        }
    }

    @CalledByNative
    public void OnResetTheWholeState(boolean success) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnResetTheWholeState(success);
        }
    }

    @CalledByNative
    public void OnGetExternalWallet(String external_wallet) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnGetExternalWallet(external_wallet);
        }
    }

    @CalledByNative
    public void onGetAvailableCountries(String[] countries) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onGetAvailableCountries(countries);
        }
    }

    @CalledByNative
    public void OnGetAdsAccountStatement(boolean success, double nextPaymentDate,
            int adsReceivedThisMonth, double minEarningsThisMonth, double maxEarningsThisMonth,
            double earningsLastMonth) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnGetAdsAccountStatement(success, nextPaymentDate, adsReceivedThisMonth,
                    minEarningsThisMonth, maxEarningsThisMonth, earningsLastMonth);
        }
    }

    @CalledByNative
    public void OnExternalWalletConnected() {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnExternalWalletConnected();
        }
    }

    @CalledByNative
    public void OnExternalWalletLoggedOut() {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnExternalWalletLoggedOut();
        }
    }

    @CalledByNative
    public void OnExternalWalletReconnected() {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnExternalWalletReconnected();
        }
    }

    @CalledByNative
    public void OnClaimPromotion(int error_code) {
        grantClaimInProcess = false;
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.OnClaimPromotion(error_code);
        }
    }

    @CalledByNative
    public void onSendContribution(boolean result) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onSendContribution(result);
        }
    }

    @CalledByNative
    public void onUnblindedTokensReady() {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onUnblindedTokensReady();
        }
    }

    @CalledByNative
    public void onReconcileComplete(int resultCode, int rewardsType, double amount) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onReconcileComplete(resultCode, rewardsType, amount);
        }
    }

    @CalledByNative
    public void onPublisherBanner(String jsonBannerInfo) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onPublisherBanner(jsonBannerInfo);
        }
    }

    @CalledByNative
    public void onGetUserType(int userType) {
        for (ShunyaRewardsObserver observer : mObservers) {
            observer.onGetUserType(userType);
        }
    }

    @NativeMethods
    interface Natives {
        void init(ShunyaRewardsNativeWorker caller);
        void destroy(long nativeShunyaRewardsNativeWorker);
        boolean isSupported(long nativeShunyaRewardsNativeWorker);
        boolean isSupportedSkipRegionCheck(long nativeShunyaRewardsNativeWorker);
        boolean isRewardsEnabled(long nativeShunyaRewardsNativeWorker);
        String getWalletBalance(long nativeShunyaRewardsNativeWorker);
        String getExternalWalletType(long nativeShunyaRewardsNativeWorker);
        void GetPublisherBanner(long nativeShunyaRewardsNativeWorker, String publisher_key);
        void getPublishersVisitedCount(long nativeShunyaRewardsNativeWorker);
        boolean canConnectAccount(long nativeShunyaRewardsNativeWorker);
        double[] GetTipChoices(long nativeShunyaRewardsNativeWorker);
        double getWalletRate(long nativeShunyaRewardsNativeWorker);
        void getPublisherInfo(long nativeShunyaRewardsNativeWorker, int tabId, String host);
        String getPublisherURL(long nativeShunyaRewardsNativeWorker, int tabId);
        String getCaptchaSolutionURL(
                long nativeShunyaRewardsNativeWorker, String paymentId, String captchaId);
        String getAttestationURL(long nativeShunyaRewardsNativeWorker);
        String getAttestationURLWithPaymentId(
                long nativeShunyaRewardsNativeWorker, String paymentId);
        String getPublisherFavIconURL(long nativeShunyaRewardsNativeWorker, int tabId);
        String getPublisherName(long nativeShunyaRewardsNativeWorker, int tabId);
        String getPublisherId(long nativeShunyaRewardsNativeWorker, int tabId);
        int getPublisherPercent(long nativeShunyaRewardsNativeWorker, int tabId);
        boolean getPublisherExcluded(long nativeShunyaRewardsNativeWorker, int tabId);
        int getPublisherStatus(long nativeShunyaRewardsNativeWorker, int tabId);
        void includeInAutoContribution(
                long nativeShunyaRewardsNativeWorker, int tabId, boolean exclude);
        void removePublisherFromMap(long nativeShunyaRewardsNativeWorker, int tabId);
        void getCurrentBalanceReport(long nativeShunyaRewardsNativeWorker);
        void donate(long nativeShunyaRewardsNativeWorker, String publisher_key, double amount,
                boolean recurring);
        void getAllNotifications(long nativeShunyaRewardsNativeWorker);
        void deleteNotification(long nativeShunyaRewardsNativeWorker, String notification_id);
        void getGrant(long nativeShunyaRewardsNativeWorker, String promotionId);
        String[] getCurrentGrant(long nativeShunyaRewardsNativeWorker, int position);
        void getRecurringDonations(long nativeShunyaRewardsNativeWorker);
        boolean isCurrentPublisherInRecurrentDonations(
                long nativeShunyaRewardsNativeWorker, String publisher);
        void getAutoContributeProperties(long nativeShunyaRewardsNativeWorker);
        boolean isAutoContributeEnabled(long nativeShunyaRewardsNativeWorker);
        void getReconcileStamp(long nativeShunyaRewardsNativeWorker);
        double getPublisherRecurrentDonationAmount(
                long nativeShunyaRewardsNativeWorker, String publisher);
        void removeRecurring(long nativeShunyaRewardsNativeWorker, String publisher);
        void resetTheWholeState(long nativeShunyaRewardsNativeWorker);
        void fetchGrants(long nativeShunyaRewardsNativeWorker);
        int getAdsPerHour(long nativeShunyaRewardsNativeWorker);
        void setAdsPerHour(long nativeShunyaRewardsNativeWorker, int value);
        void getExternalWallet(long nativeShunyaRewardsNativeWorker);
        String getCountryCode(long nativeShunyaRewardsNativeWorker);
        void getAvailableCountries(long nativeShunyaRewardsNativeWorker);
        void disconnectWallet(long nativeShunyaRewardsNativeWorker);
        void refreshPublisher(long nativeShunyaRewardsNativeWorker, String publisherKey);
        void createRewardsWallet(long nativeShunyaRewardsNativeWorker, String countryCode);
        void getRewardsParameters(long nativeShunyaRewardsNativeWorker);
        double getVbatDeadline(long nativeShunyaRewardsNativeWorker);
        void getUserType(long nativeShunyaRewardsNativeWorker);
        boolean isGrandfatheredUser(long nativeShunyaRewardsNativeWorker);
        void fetchBalance(long nativeShunyaRewardsNativeWorker);
        void setAutoContributeEnabled(
                long nativeShunyaRewardsNativeWorker, boolean isSetAutoContributeEnabled);
        void setAutoContributionAmount(long nativeShunyaRewardsNativeWorker, double amount);
        void getAutoContributionAmount(long nativeShunyaRewardsNativeWorker);
        void getAdsAccountStatement(long nativeShunyaRewardsNativeWorker);
        String getPayoutStatus(long nativeShunyaRewardsNativeWorker);
    }
}

/* Copyright (c) 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.infobar;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

// Reflects enum items from chromium_src/components/infobars/core/infobar_delegate.h

@IntDef({ShunyaInfoBarIdentifier.INVALID, ShunyaInfoBarIdentifier.SHUNYA_CONFIRM_P3A_INFOBAR_DELEGATE,
        ShunyaInfoBarIdentifier.WAYBACK_MACHINE_INFOBAR_DELEGATE,
        ShunyaInfoBarIdentifier.SYNC_V2_MIGRATE_INFOBAR_DELEGATE,
        ShunyaInfoBarIdentifier.ANDROID_SYSTEM_SYNC_DISABLED_INFOBAR,
        ShunyaInfoBarIdentifier.SYNC_CANNOT_RUN_INFOBAR,
        ShunyaInfoBarIdentifier.WEB_DISCOVERY_INFOBAR_DELEGATE,
        ShunyaInfoBarIdentifier.SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR})

@Retention(RetentionPolicy.SOURCE)
public @interface ShunyaInfoBarIdentifier {
    int INVALID = -1;
    int SHUNYA_CONFIRM_P3A_INFOBAR_DELEGATE = 500;
    int WAYBACK_MACHINE_INFOBAR_DELEGATE = 502;
    int SYNC_V2_MIGRATE_INFOBAR_DELEGATE = 503;
    int ANDROID_SYSTEM_SYNC_DISABLED_INFOBAR = 504;
    int SYNC_CANNOT_RUN_INFOBAR = 505;
    int WEB_DISCOVERY_INFOBAR_DELEGATE = 506;
    int SHUNYA_SYNC_ACCOUNT_DELETED_INFOBAR = 507;
}

/* Copyright (c) 2022 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.chrome.browser.site_settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;
import androidx.recyclerview.widget.RecyclerView;

import org.chromium.shunya_wallet.mojom.ShunyaWalletService;
import org.chromium.shunya_wallet.mojom.CoinType;
import org.chromium.chrome.R;
import org.chromium.chrome.browser.crypto_wallet.ShunyaWalletServiceFactory;
import org.chromium.mojo.bindings.ConnectionErrorHandler;
import org.chromium.mojo.system.MojoException;

public class ShunyaWalletEthereumConnectedSitesPreference
        extends Preference implements ConnectionErrorHandler,
                                      ShunyaWalletEthereumConnectedSitesListAdapter
                                              .ShunyaEthereumPermissionConnectedSitesDelegate {
    private RecyclerView mRecyclerView;
    private ShunyaWalletService mShunyaWalletService;
    private ShunyaWalletEthereumConnectedSitesListAdapter mAdapter;

    public ShunyaWalletEthereumConnectedSitesPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);

        initShunyaWalletService();

        mRecyclerView = (RecyclerView) holder.findViewById(R.id.connected_sites_list);
        updateWebSitestList();
    }

    public void destroy() {
        mShunyaWalletService.close();
        mShunyaWalletService = null;
    }

    @SuppressLint("NotifyDataSetChanged")
    private void updateWebSitestList() {
        mShunyaWalletService.getWebSitesWithPermission(CoinType.ETH, webSites -> {
            if (mAdapter == null) {
                mAdapter = new ShunyaWalletEthereumConnectedSitesListAdapter(webSites, this);
                mRecyclerView.setAdapter(mAdapter);
            } else {
                mAdapter.setWebSites(webSites);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void removePermission(String webSite) {
        mShunyaWalletService.resetWebSitePermission(CoinType.ETH, webSite, success -> {
            if (success) {
                updateWebSitestList();
            }
        });
    }

    @Override
    public void onConnectionError(MojoException e) {
        mShunyaWalletService.close();
        mShunyaWalletService = null;
        initShunyaWalletService();
    }

    private void initShunyaWalletService() {
        if (mShunyaWalletService != null) {
            return;
        }

        mShunyaWalletService = ShunyaWalletServiceFactory.getInstance().getShunyaWalletService(this);
    }
}

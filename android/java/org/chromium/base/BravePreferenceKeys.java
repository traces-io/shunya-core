/* Copyright (c) 2020 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.base;

public final class ShunyaPreferenceKeys {
    public static final String SHUNYA_BOTTOM_TOOLBAR_ENABLED_KEY =
            "shunya_bottom_toolbar_enabled_key";
    public static final String SHUNYA_BOTTOM_TOOLBAR_SET_KEY = "shunya_bottom_toolbar_enabled";
    public static final String SHUNYA_USE_CUSTOM_TABS = "use_custom_tabs";
    public static final String SHUNYA_APP_OPEN_COUNT = "shunya_app_open_count";
    public static final String SHUNYA_ROLE_MANAGER_DIALOG_COUNT = "shunya_role_manager_dialog_count";
    public static final String SHUNYA_IS_DEFAULT = "shunya_is_default";
    public static final String SHUNYA_WAS_DEFAULT_ASK_COUNT = "shunya_was_default_ask_count";
    public static final String SHUNYA_SET_DEFAULT_BOTTOM_SHEET_COUNT =
            "shunya_set_default_bottom_sheet_count";
    public static final String SHUNYA_DEFAULT_DONT_ASK = "shunya_default_dont_ask";
    public static final String SHUNYA_UPDATE_EXTRA_PARAM =
            "org.chromium.chrome.browser.upgrade.UPDATE_NOTIFICATION_NEW";
    public static final String SHUNYA_NOTIFICATION_PREF_NAME =
            "org.chromium.chrome.browser.upgrade.NotificationUpdateTimeStampPreferences_New";
    public static final String SHUNYA_MILLISECONDS_NAME =
            "org.chromium.chrome.browser.upgrade.Milliseconds_New";
    public static final String SHUNYA_DOWNLOADS_AUTOMATICALLY_OPEN_WHEN_POSSIBLE =
            "org.chromium.chrome.browser.downloads.Automatically_Open_When_Possible";
    public static final String SHUNYA_DOWNLOADS_DOWNLOAD_PROGRESS_NOTIFICATION_BUBBLE =
            "org.chromium.chrome.browser.downloads.Download_Progress_Notification_Bubble";
    public static final String SHUNYA_DOUBLE_RESTART =
            "org.chromium.chrome.browser.Shunya_Double_Restart";
    public static final String SHUNYA_TAB_GROUPS_ENABLED =
            "org.chromium.chrome.browser.Shunya_Tab_Groups_Enabled";
    public static final String SHUNYA_DISABLE_SHARING_HUB =
            "org.chromium.chrome.browser.Shunya_Disable_Sharing_Hub";
    public static final String SHUNYA_NEWS_CARDS_VISITED = "shunya_news_cards_visited";
    public static final String SHUNYA_NEWS_CHANGE_SOURCE = "shunya_news_change_source";
    public static final String SHUNYA_NEWS_FEED_HASH = "shunya_news_feed_hash";
    public static final String SHUNYA_NEWS_PREF_SHOW_NEWS = "kNewTabPageShowToday";
    public static final String SHUNYA_NEWS_PREF_TURN_ON_NEWS = "kShunyaNewsOptedIn";
    public static final String SHUNYA_USE_BIOMETRICS_FOR_WALLET =
            "org.chromium.chrome.browser.Shunya_Use_Biometrics_For_Wallet";
    public static final String SHUNYA_BIOMETRICS_FOR_WALLET_IV =
            "org.chromium.chrome.browser.Shunya_Biometrics_For_Wallet_Iv";
    public static final String SHUNYA_BIOMETRICS_FOR_WALLET_ENCRYPTED =
            "org.chromium.chrome.browser.Shunya_Biometrics_For_Wallet_Encrypted";
    public static final String SHUNYA_AD_FREE_CALLOUT_DIALOG = "shunya_ad_free_callout_dialog";
    public static final String SHUNYA_OPENED_YOUTUBE = "shunya_opened_youtube";
    public static final String SHOULD_SHOW_COOKIE_CONSENT_NOTICE =
            "should_show_cookie_consent_notice";
    public static final String LOADED_SITE_COUNT = "loaded_site_count";
    public static final String SHUNYA_BACKGROUND_VIDEO_PLAYBACK_CONVERTED_TO_FEATURE =
            "shunya_background_video_playback_converted_to_feature";
    public static final String SHUNYA_APP_OPEN_COUNT_FOR_WIDGET_PROMO =
            "shunya_app_open_count_for_widget_promo";
    public static final String SHUNYA_DEFERRED_DEEPLINK_PLAYLIST =
            "shunya_deferred_deeplink_playlist";
    public static final String SHUNYA_DEFERRED_DEEPLINK_VPN = "shunya_deferred_deeplink_vpn";
}

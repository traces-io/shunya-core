/* Copyright (c) 2019 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.chromium.base;

/**
 * A list of feature flags exposed to Java.
 */
public abstract class ShunyaFeatureList {
    public static final String NATIVE_SHUNYA_WALLET = "NativeShunyaWallet";
    public static final String USE_DEV_UPDATER_URL = "UseDevUpdaterUrl";
    public static final String FORCE_WEB_CONTENTS_DARK_MODE = "WebContentsForceDark";
    public static final String ENABLE_FORCE_DARK = "enable-force-dark";
    public static final String SHUNYA_WALLET_SOLANA = "ShunyaWalletSolana";
    public static final String SHUNYA_WALLET_FILECOIN = "ShunyaWalletFilecoin";
    public static final String SHUNYA_WALLET_SNS = "ShunyaWalletSns";
    public static final String SHUNYA_SEARCH_OMNIBOX_BANNER = "ShunyaSearchOmniboxBanner";
    public static final String SHUNYA_BACKGROUND_VIDEO_PLAYBACK = "ShunyaBackgroundVideoPlayback";
    public static final String SHUNYA_BACKGROUND_VIDEO_PLAYBACK_INTERNAL =
            "shunya-background-video-playback";
    public static final String SHUNYA_ANDROID_SAFE_BROWSING = "ShunyaAndroidSafeBrowsing";
    public static final String SHUNYA_VPN_LINK_SUBSCRIPTION_ANDROID_UI =
            "ShunyaVPNLinkSubscriptionAndroidUI";
    public static final String DEBOUNCE = "ShunyaDebounce";
    public static final String SHUNYA_GOOGLE_SIGN_IN_PERMISSION = "ShunyaGoogleSignInPermission";
    public static final String SHUNYA_LOCALHOST_PERMISSION = "ShunyaLocalhostPermission";
    public static final String SHUNYA_PLAYLIST = "Playlist";
    public static final String SHUNYA_SPEEDREADER = "Speedreader";
    public static final String HTTPS_BY_DEFAULT = "HttpsByDefault";
    public static final String SHUNYA_FORGET_FIRST_PARTY_STORAGE = "ShunyaForgetFirstPartyStorage";
    public static final String SHUNYA_REQUEST_OTR_TAB = "ShunyaRequestOTRTab";
    public static final String AI_CHAT = "AIChat";
}
